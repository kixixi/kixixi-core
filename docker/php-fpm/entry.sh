#!/bin/bash

HOSTGUID=$(stat -c '%g' .)
HOSTUID=$(stat -c '%u' .)

if [ "$HOSTGUID" != 0 ]; then
	userdel www-data
	addgroup --gid `stat -c '%g' .` www-data
	adduser --uid `stat -c '%u' .` --gid `stat -c '%g' .` --disabled-password --gecos '' www-data
	adduser www-data sudo
        chown -R www-data. /code

        if [ "$1" != 'php-fpm' ]; then
            sudo -u www-data "$@"
        else
            set -e

            # first arg is `-f` or `--some-option`
            if [ "${1#-}" != "$1" ]; then
                    set -- php-fpm "$@"
            fi

            exec "$@"
        fi
else
    set -e

    # first arg is `-f` or `--some-option`
    if [ "${1#-}" != "$1" ]; then
            set -- php-fpm "$@"
    fi

    exec "$@"
fi

