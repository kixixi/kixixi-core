$(document).ready(function() {
    $('[data-toggle=offcanvas]').click(function() {
        var el = $(this).attr("data-target");
        $(el).toggleClass('collapse');
    });
});