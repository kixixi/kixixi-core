{HEADER}
<!-- BEGIN PANEL -->
<div class="hidden-md-up" id="sidebar-nav-sx-toggler">
    <div type="button" data-toggle="offcanvas" data-target="#sidebar-nav-sx">
        <i class="fa fa-chevron-right"></i>
    </div>
</div>
<div class="collapse navbar-dark" aria-expanded="false" id="sidebar-nav-sx">
    <div class="container-fluid">
        <div class="row row-eq-height">
            <div class="col1 col-xs-10">
                <ul class="nav sidebar-nav">
                    <!-- BEGIN MENU -->
                    <!-- BEGIN ONELEVEL -->
                    <li class="colored {PANEL.MENU.active}">
                        <a href="{PANEL.MENU.href}"><i class="{PANEL.MENU.iClass}"></i> {PANEL.MENU.label}</a>
                    </li>
                    <!-- END ONELEVEL -->
                    <!-- BEGIN MULTILEVEL -->
                    <li class="{PANEL.MENU.MULTILEVEL.active}">
                        <a href="{PANEL.MENU.MULTILEVEL.href}"><i class="{PANEL.MENU.MULTILEVEL.iClass}"></i> {PANEL.MENU.MULTILEVEL.label}</a>
                        <ul>
                            <!-- BEGIN IN -->
                            <li class="sidebar-nav-in colored {PANEL.MENU.MULTILEVEL.IN.active}">
                                <a href="{PANEL.MENU.MULTILEVEL.IN.href}"><i class="{PANEL.MENU.MULTILEVEL.IN.iClass}"></i> {PANEL.MENU.MULTILEVEL.IN.label}</a>
                            </li>
                            <!-- END IN -->
                        </ul>
                    </li>
                    <!-- END MULTILEVEL -->
                    <!-- END MENU -->
                </ul>
            </div>
            <div class="col2 col-xs-2 relative">
                <div class="closeNav bg-warning">
                    <div class="hidden-lg-up bg-warning" type="button" data-toggle="offcanvas" data-target="#sidebar-nav-sx">
                        <i class="fa fa-chevron-left"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid" id="container">
    <div class="row row-eq-height p-t-1">
        <nav role="navigation" id="sidebar-nav" class="col-sm-4 col-md-4 col-lg-3 col-xl-2 p-t-1 p-b-1 hidden-sm-down">

            <ul class="nav sidebar-nav">
                <!-- BEGIN BLOCKWEBEDITING -->
                <li id="editingWebSite" class="nav-item">
                    <div class="dropdown">
                        <a class="nav-link dropdown-toggle" role="button" id="topBarFlagCur" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                            <!-- BEGIN CURWEBEDITING -->
                            {PANEL.BLOCKWEBEDITING.CURWEBEDITING.webName}
                            <!-- END CURWEBEDITING -->
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="topBarFlagCur">
                            <!-- BEGIN WEBEDITING -->
                            <a class="dropdown-item" href="#" onclick="KXJS.setEditingWebSite('{PANEL.BLOCKWEBEDITING.WEBEDITING.webCode}');">{PANEL.BLOCKWEBEDITING.WEBEDITING.webName}</a>
                            <!-- END WEBEDITING -->
                        </div>
                    </div>
                </li>
                <!-- END BLOCKWEBEDITING -->
                <!-- BEGIN MENU -->
                <!-- BEGIN ONELEVEL -->
                <li class="colored {PANEL.MENU.active}">
                    <a href="{PANEL.MENU.href}"><i class="{PANEL.MENU.iClass}"></i> {PANEL.MENU.label}</a>
                </li>
                <!-- END ONELEVEL -->
                <!-- BEGIN MULTILEVEL -->
                <li class="{PANEL.MENU.MULTILEVEL.active}">
                    <a href="{PANEL.MENU.MULTILEVEL.href}"><i class="{PANEL.MENU.MULTILEVEL.iClass}"></i> {PANEL.MENU.MULTILEVEL.label}</a>
                    <ul>
                        <!-- BEGIN IN -->
                        <li class="sidebar-nav-in colored {PANEL.MENU.MULTILEVEL.IN.active}">
                            <a href="{PANEL.MENU.MULTILEVEL.IN.href}"><i class="{PANEL.MENU.MULTILEVEL.IN.iClass}"></i> {PANEL.MENU.MULTILEVEL.IN.label}</a>
                        </li>
                        <!-- END IN -->
                    </ul>
                </li>
                <!-- END MULTILEVEL -->
                <!-- END MENU -->
            </ul>
        </nav>
        <div id="columnRX" class="{columnRXClass} p-t-1" style="min-height: 700px;">
            <div class="text-left text-muted">
                <h1 class="titlePage">{TITLEPAGE}</h1>
            </div>
            <div id="panelBlock"></div>
        </div>
    </div>
</div>
<!-- END PANEL -->
{FOOTER}


