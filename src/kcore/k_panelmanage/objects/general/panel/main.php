<?
/**
 * KIXIXI panelmanage (panel)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_panelmanage
 * @subpackage objects\panel
 * @since 1.00
 */
namespace k_panelmanage\objects\general\panel;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";


    /**
     * Get Panel Object
     *
     * @param array $arrayVariable array of variables
     * No allowed variables<br>
     *
     *
     * @return this
     */
    public function get($arrayVariable = [])
    {



/*
        $a = ["pippo"];
        echo "R= ". $a["pippo"] . "-" . $a[0];
        $a = ["pippo" => "pluto"];
        echo "R= ". $a["pippo"] . "-" . $a[0];

        $this->kxdb->showSQL();
        $this->kxdb->prepare("SELECT * FROM k_registry_registry WHERE ana_code IN (:pippo)", ["pippo" => ["kx_type_number" => [1,1]]]);
        $this->kxdb->execute();
        $d = $this->kxdb->fetch();
        echo "NOME = " . $d->ana_first_name;
        die("OK");
*/

        //include objects
        $objApiWebManager = $this->K_COMMON->getPluginObject("k_webmanager", "api");
        $objApiUsefulData = $this->K_COMMON->getPluginObject("k_usefuldata", "api");

        $this->addExternalPlugin("waitingfor");
        $template = $this->getTemplateLayout("panel");

        //set variable
        $currentURL = $this->K_COMMON->getCurrentURI();
        $defaultPanelUrl = $this->getPanelManageURL();

        //check if we are in Panel
        if (strpos($currentURL, $defaultPanelUrl) !== 0) //not start with
        {
            //Check if eventually we are on modify template
            if ($this->K_COMMON->getVarRequest("KCA") != "templateEditor")
                die("Error Security");
        }



        $currentURL = str_replace($defaultPanelUrl,"/manage",$currentURL);



        //set others variables
        $splitURL = explode('/', $currentURL);
        $panelUrlName = $defaultPanelUrl;
        $actualURLPanelPluginName = (isset($splitURL[2]) ? $splitURL[2] : "");
        $actualURLPanelPluginObject = (isset($splitURL[3]) ? $splitURL[3] : "");
        $panelUrlBase =  $panelUrlName . "/";
        $panelUrlPluginBase =  $panelUrlName . "/" . $actualURLPanelPluginName . "/";
        $panelUrlActual =  $panelUrlName . "/" . $actualURLPanelPluginName . "/" . $actualURLPanelPluginObject . "/";
        $anaCode = $this->K_COMMON->getCurrentUser();
        $userLevel = $this->K_COMMON->getCurrentUserLevelType();
        $foundMenu = false; //true if we found an object to show authorized
        $getPluginName = ""; //plugin name to be taken
        $getMenuTemplateCode = ""; //plugin Template to Laod
        $getPluginObject = ""; //plugin object to be taken
        $titlePageCustom = "";
        $pluginMenuName = "";
        $urlExtraParam = [];
        $crumbsPanelLabel = "";
        $crumbsPanelUrl = "";
        $editingWebReloadUrl = $panelUrlPluginBase;
        $webCodeToGet = $this->K_COMMON->getCurrentWeb();


        //search type of panel
        if ($actualURLPanelPluginName == "")
            return $this->getDashboard($arrayVariable, $template);

        if (isset($splitURL[4]))
            $arrayVariable["pluginMethod"] = $splitURL[4];
        if (isset($splitURL[5]))
            $arrayVariable["pluginParam"] = $splitURL[5];
        if (count($splitURL) > 5) {

            unset($splitURL[0]);
            unset($splitURL[1]);
            unset($splitURL[2]);
            unset($splitURL[3]);
            $splitURL = $this->K_COMMON->arrayToAssociative($splitURL);

            $urlExtraParam = $splitURL;

        }
        $arrayVariable["panelUrlActual"] = $panelUrlActual;
        $arrayVariable["panelUrlPluginBase"] = $panelUrlPluginBase;

        //set variables
        $pluginMethod = $this->K_COMMON->getVarArray($arrayVariable, "pluginMethod");
        $pluginParam = $this->K_COMMON->getVarArray($arrayVariable, "pluginParam");
        $urlExtraParam[$pluginMethod] = $pluginParam;
        $arrayVariable["urlExtraParam"] = $urlExtraParam;


        //Check if user logged
        if (!$this->K_SECAUTH->verifyUserLoggedGoLogin()) return;

        //Get Variables
        $columnRXClass = $this->K_COMMON->getVarArray($arrayVariable, "columnRXClass");
        $pageTitle = $this->K_COMMON->getVarArray($arrayVariable, "pageTitle");

        //Check Variable
        if ($columnRXClass == "")
            $columnRXClass = "col-xs-12 col-sm-8 col-md-8 col-lg-9 col-xl-offset-1 col-xl-10 p-t-md p-b-lg";
        if ($pageTitle == "")
            $pageTitle = "Panel";

        //Assign CSS
        $this->addHeadFile([$this->getPathFileCSS("panel")]);
        $this->addHeadFile([$this->getPathFileJS("panel")]);

        //Assign Template Variable
        $template->assign_vars(array("columnRXClass" => $columnRXClass));

        //Build Template
        $template->assign_block_vars("PANEL", array());

        //Add Menu
        $template->assign_block_vars("PANEL.MENU", array());

        //Read Menu from DB
        $table = $this->getTable("menu");
        $query = "SELECT 
                    menu.pmn_plugin_object object 
                    ,menu.pmn_menu_code code 
                    ,menu.pmn_menu_up_code codeup
                    ,menu.pmn_menu_ref_code codeRef
                    ,menu.pmn_label label 
                    ,menu.pmn_icon icon 
                    ,menu.pmn_authorization auth
                    , sty.code_form 
                    , menu.pmn_url url_menu
                    , (SELECT GROUP_CONCAT(pmn_url SEPARATOR ',') 
                        FROM k_panelmanage_menu 
                        WHERE pmn_plugin_name = menu.pmn_plugin_name 
                              
                              AND (pmn_menu_ref_code = menu.pmn_menu_code OR (pmn_plugin_object = menu.pmn_plugin_object AND pmn_menu_code = menu.pmn_menu_code))
                      ) url_all
                    , menu.pmn_owner_plugin_name ownerPluginName
                    , menu.pmn_owner_plugin_object ownerPluginObject
                    , menu.pmn_get_plugin_name getPluginName
                    , menu.pmn_get_plugin_object getPluginObject
                    , menu.pmn_get_plugin_method getPluginMethod
                    , dashboard.dmn_label, menu.pmn_tpl_code getTemplateCode
                                FROM k_panelmanage_menu menu
                                LEFT JOIN k_panelmanage_dashboard dashboard on menu.pmn_plugin_name = dashboard.dmn_plugin_name
                                LEFT JOIN k_panelmanage_menu menuup on menu.pmn_plugin_name = menuup.pmn_plugin_name AND menu.pmn_menu_up_code = menuup.pmn_menu_code AND menuup.pmn_plugin_object = ''
                                LEFT JOIN (
                                          SELECT plugin_name, code_form
                                          FROM k_secauth_permission
                                          WHERE (level_type_code = '" . $userLevel . "' or employee_person_code = '" . $anaCode . "')
                                            AND (web_code = '".$webCodeToGet."')
                                          GROUP BY plugin_name, code_form
                                          ) sty
                                    on sty.plugin_name = menu.pmn_authorization_plugin_name
                                        and sty.code_form = menu.pmn_authorization
                                        
                    WHERE menu.pmn_plugin_name = '" . $actualURLPanelPluginName . "'
                                    and (menu.pmn_authorization = '' or menu.pmn_authorization = sty.code_form)
                               ORDER BY 
                                 CONVERT(if(menuup.pmn_order <> \"\",menuup.pmn_order,menu.pmn_order),DECIMAL(10,7))
                                ,CONVERT(if(menuup.pmn_order <> \"\",menu.pmn_order+1,0),DECIMAL(10,7))
        ";

        $table->prepare($query);

        $table->execute();

        //Add Menu Dashboard
        $template->assign_block_vars("PANEL.MENU.MULTILEVEL"
            , array("href" => $panelUrlName, "label" => "DASHBOARD", "iClass" => "fa fa-dashboard "
            , ""));

        //Write menu on template
        $dataMNU = true;
        $readData = true;

        while ($dataMNU) {
            if ($readData)
                $dataMNU = $table->fetch();
            if ($dataMNU) {



                $menuCode = $dataMNU->code;
                $menuIcon = $dataMNU->icon;
                $url_menu = $dataMNU->url_menu;
                $url_all = $dataMNU->url_all;
                $url_all_array = explode(",",$url_all);



                $readData = true;
                if ($dataMNU->label != "")
                    $label = $this->getDictionaryWord($dataMNU->label, $dataMNU->ownerPluginName, $dataMNU->ownerPluginObject);
                else
                    $label = "";

                $dmnLabel = $dataMNU->dmn_label;
                if ($dmnLabel != "")
                    $pluginMenuName = $this->getDictionaryWord($dmnLabel, $dataMNU->ownerPluginName, "");



                if ($dataMNU->codeup == "" && $dataMNU->codeRef == "") {


                    //Check if there are sub menu
                    $dataMNU_NEXT = $table->fetch();
                    if ($dataMNU_NEXT) {
                        if ($dataMNU_NEXT->codeup == $menuCode) {
                            //Write menu
                            $template->assign_block_vars("PANEL.MENU.MULTILEVEL"
                                , array("href" => $panelUrlPluginBase . $url_menu, "label" => $label, "iClass" => "fa " . $menuIcon
                                ));
                        }
                    }

                    //reset $dataMNU
                    $dataMNU = $dataMNU_NEXT;
                    $readData = false;

                } else {


                    if (!$foundMenu) {

                        if ($actualURLPanelPluginObject == $url_menu)
                        {

                            $foundMenu =  true;

                        }

                        $getPluginName = $dataMNU->getPluginName;
                        $getPluginObject = $dataMNU->getPluginObject;
                        $getMenuTemplateCode = $dataMNU->getTemplateCode;

                    }

                    $menuActive = false;



                    //Search if this menu is to be highlight - may be that another panel highlight this menu
                    foreach ($url_all_array as $url_item)
                    {

                        if ($actualURLPanelPluginObject == $url_item)
                            $menuActive =  true;

                    }



                    if ($dataMNU->codeRef == "")
                    {


                        if ($menuActive)
                        {
                            $crumbsPanelLabel = $label;
                            $crumbsPanelUrl = $panelUrlPluginBase . $url_menu . "/" . $dataMNU->getPluginMethod;
                            $editingWebReloadUrl = $crumbsPanelUrl;
                        }
                        $template->assign_block_vars("PANEL.MENU.MULTILEVEL.IN"
                            , array("href" => $panelUrlPluginBase . $url_menu . "/" . $dataMNU->getPluginMethod, "label" => $label, "iClass" => "fa " . $dataMNU->icon
                            , "active" => ($menuActive ? "active" : "")));
                    }

                }
            }

        }


        //Set Crumbs
        $this->pageCrumbs["Dashboard"] = $panelUrlName;
        $this->pageCrumbs[$crumbsPanelLabel] = $crumbsPanelUrl;


        //Set Column B
        if (($getPluginName != "") && ($foundMenu)) {




            //get html of object requested
            if ($getMenuTemplateCode == "")
            {

                $obj = $this->K_COMMON->getPluginObjectPanel($getPluginName, $getPluginObject);
                $objRet = $obj->getPanelObject($arrayVariable);

            }
            else
            {

                $objectsVariables = [
                    "global" =>
                        [
                            "baseURL" => $panelUrlActual
                            ,"urlParam" => $urlExtraParam
                        ]
                ];

                $objRet = $this->getWebTemplate("",$getMenuTemplateCode,$objectsVariables,$getPluginName,$getPluginObject);
            }





            $html = $objRet->getHTML();
            $this->mergeObjectVariables($objRet);

            //check if custom title page
            $titlePageCustom = $objRet->getSEOMetaTag("title");

        } else
            $html = "";

        //Insert Current Web Site Editing
        $template->assign_block_vars("PANEL.BLOCKWEBEDITING",[]);
        $template->assign_block_vars("PANEL.BLOCKWEBEDITING.CURWEBEDITING"
            , [
                "webCode" => $this->K_COMMON->getEditingWeb()
                , "webName" => $objApiWebManager->getWebName(["webCode" => $this->K_COMMON->getEditingWeb()])
            ]);

        //Create Combo Web Sites Editing
        $webSites = $objApiUsefulData->getWebSites(["OM" => "TABLE"]);
        while ($dataWebSites = $webSites->fetch()) {
            $template->assign_block_vars("PANEL.BLOCKWEBEDITING.WEBEDITING"
                , [
                    "webCode" => $dataWebSites->value
                    , "webName" => $dataWebSites->text
                ]
            );
        }


        //Assign SEO Meta Tag
        $this->addSEOMetaTag(
            [
                "title" => $pageTitle . ($titlePageCustom != "" ? " (" . $titlePageCustom . ")" : " (" . $pluginMenuName . ")")
            ]
        );


        //Show Panel on COLUMN 2
        $template->assign_vars(array("COLUMN2" => ""));
        $PParse = $template->pparse('body');
        $this->appendHTML("", $PParse);



        //Get Crumbs
        $this->appendWO("panelBlock", "k_htmlcomponents", "web\objects\w_breadcrumbs",
            [
                "crumbs" => $this->pageCrumbs
            ]
        );

        //Append Object Panel
        $this->appendHTML("panelBlock", $html);


        $JSToInclude = '<script>
            KXJS.panelUrlBase = "' . $panelUrlBase . '";
            KXJS.panelUrlActual = "' . $panelUrlActual . '";
            KXJS.panelUrlPluginBase = "' . $panelUrlPluginBase . '";
            KXJS.panelURLParam = "' . $pluginParam . '";
            KXJS.editingWebReloadUrl = "' . $editingWebReloadUrl . '";
            </script>';
        $this->addFooterJS($JSToInclude);



        return $this;

    }

    /**
     * Get Panel Dashboard Object
     *
     * @param array $arrayVariable string to be escaped
     * The following directives can be used in the array variables:<br>
     *    *    columnRXClass: class for RX column
     *    *    pageTitle: title of page
     *
     * @param template $template template of page
     *
     *
     * @return this
     */
    protected function getDashboard($arrayVariable = array(), $template)
    {

        //set variable
        $panelUrlName = $this->getPanelManageURL();
        $anaCode = $this->K_COMMON->getCurrentUser();
        $userLevel = $this->K_COMMON->getCurrentUserLevelType();
        $webCodeToGet = $this->K_COMMON->getCurrentWeb();

        //Check if user logged
        if (!$this->K_SECAUTH->verifyUserLoggedGoLogin()) return;

        //Get Variables
        $columnRXClass = $this->K_COMMON->getVarArray($arrayVariable, "columnRXClass");
        $pageTitle = $this->K_COMMON->getVarArray($arrayVariable, "pageTitle");

        //Check Variable
        if ($columnRXClass == "")
            $columnRXClass = "col-xs-12 col-sm-8 col-md-8 col-lg-9 col-xl-offset-1 col-xl-10 p-t-md p-b-lg";
        if ($pageTitle == "")
            $pageTitle = "Panel Manage";

        //Assign CSS
        $this->addHeadFile([$this->getPathFileCSS("panel")]);
        $this->addHeadFile([$this->getPathFileJS("panel")]);

        //Assign Template Variable
        $template->assign_vars(array("columnRXClass" => $columnRXClass));

        //Assign SEO Meta Tag
        $this->addSEOMetaTag(
            [
                "title" => $pageTitle
            ]
        );

        //Build Template
        $template->assign_block_vars("PANEL", array());

        //Add Menu
        $template->assign_block_vars("PANEL.MENU", array());

        //Read Menu from DB
        $table = $this->getTable("menu");
        $table->prepare("SELECT menu.pmn_plugin_name plugin, dashboard.dmn_label
						    FROM k_panelmanage_menu menu
						    LEFT JOIN k_panelmanage_dashboard dashboard on menu.pmn_plugin_name = dashboard.dmn_plugin_name AND dmn_type = 'menu' 
                            LEFT JOIN k_panelmanage_menu menuin on menu.pmn_plugin_name = menuin.pmn_plugin_name AND menuin.pmn_menu_up_code <> ''
                            LEFT JOIN (
						          SELECT plugin_name, code_form
						          FROM k_secauth_permission
						          WHERE (level_type_code = '" . $userLevel . "' or employee_person_code = '" . $anaCode . "')
						            AND (web_code = '".$webCodeToGet."')
						          GROUP BY plugin_name, code_form
						          ) sty
						          on sty.plugin_name = menu.pmn_authorization_plugin_name
								and sty.code_form = menu.pmn_authorization
                            WHERE (menuin.pmn_authorization = '' or menuin.pmn_authorization = sty.code_form)
 
                            GROUP BY menu.pmn_plugin_name
	                        ORDER BY replace(menu.pmn_plugin_name,\"k_\",\"kcore_\")					");

        $table->execute();

        //Write menu on template
        $moduleTypePrec = "";
        while ($row = $table->fetch()) {
            //get Module Type
            $moduleType = $this->K_COMMON->getPluginType($row->plugin);
            $pluginName = $row->plugin;
            $dmnLabel = $row->dmn_label;

            //search type of plugin
            switch ($moduleType) {
                case "kcore":
                    $pluginLabelName = "K-CORE";
                    break;
                case "kplugin":
                    $pluginLabelName = "K-APP";
                    break;
                case "ktemplate":
                    $pluginLabelName = "K-TEMPLATE";
                    break;

            }

            //Create First Level Menu
            if ($moduleType != $moduleTypePrec)
                $template->assign_block_vars("PANEL.MENU.MULTILEVEL"
                    , array("href" => "#", "label" => $pluginLabelName, "iClass" => ""
                    , "active" => ""));

            //Create label Menu
            if ($dmnLabel != "")
                $pluginMenuName = $this->getDictionaryWord($dmnLabel, $pluginName, "");
            else {
                $pluginMenuName = $pluginName;
                $pluginMenuName = str_replace("k_", "", $pluginMenuName);
                $pluginMenuName = str_replace("kplugin_", "", $pluginMenuName);
                $pluginMenuName = str_replace("_", " ", $pluginMenuName);
                $pluginMenuName = ucfirst($pluginMenuName);
            }

            $template->assign_block_vars("PANEL.MENU.MULTILEVEL.IN"
                , array("href" => $panelUrlName . "/" . $row->plugin, "label" => $pluginMenuName, "iClass" => ""
                , "active" => ""));

            $moduleTypePrec = $moduleType;
        }


        //Set Column B
        $html = "";

        $html = '<div id="panelBlock">' . $html . '</div>';
        $template->assign_vars(array("COLUMN2" => $html));

        $PParse = $template->pparse('body');


        $this->appendHTML("", $PParse);

        return $this;
    }

}

?>