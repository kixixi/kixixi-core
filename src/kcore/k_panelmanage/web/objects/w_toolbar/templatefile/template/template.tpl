<!-- BEGIN NAVBAR -->
<nav id="navbar-top" class="navbar navbar-static-top navbar navbar-dark m-l-1 m-r-1 m-t-1" style="display: block;{NAVBAR.style}">
    <!-- BEGIN LOGO -->
    <a href="" class="navbar-brand pull-left"><img border="0" height="30" src="{NAVBAR.LOGO.logo}"></a>
    <!-- END LOGO -->
    <button style="color:white;" data-target=".navbar-collapse" data-toggle="collapse" type="button"
            class="navbar-toggler hidden-md-up bg-warning pull-right m-l">
        <span class="fa fa-align-justify"></span>
    </button>
    <div style="" aria-expanded="true" class="collapse navbar-toggleable-sm">

        <ul class="nav navbar-nav pull-left">
            

            <!-- BEGIN ITEML -->
            <li class="nav-item {NAVBAR.ITEML.class}">
                <a class="nav-link {NAVBAR.ITEML.class}" href="{NAVBAR.ITEML.link}" style="{NAVBAR.ITEML.style}" {NAVBAR.ITEML.attributes}>{NAVBAR.ITEML.label}</a>
            </li>
            <!-- END ITEML -->
        </ul>
        <ul class="nav navbar-nav pull-right">



            <!-- BEGIN ITEMR -->
            <li class="nav-item {NAVBAR.ITEMR.class}">
                <a class="nav-link" href="{NAVBAR.ITEMR.link} {NAVBAR.ITEMR.class}" style="{NAVBAR.ITEMR.style}" {NAVBAR.ITEMR.attributes}>{NAVBAR.ITEMR.label}</a>
            </li>
            <!-- END ITEMR -->




            <!-- BEGIN BLOCKLANGUAGES -->
            <li class="nav-item">
                <div class="dropdown">
                    <a class="nav-link dropdown-toggle" role="button" id="topBarFlagCur" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false">
                        <!-- BEGIN CURLANGUAGE -->
                        <img src="{NAVBAR.BLOCKLANGUAGES.CURLANGUAGE.languageImage}">
                        <!-- END CURLANGUAGE -->
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="topBarFlagCur">
                        <!-- BEGIN LANGUAGES -->
                        <a class="dropdown-item" href="#" onclick="KXJS.setLanguage('{NAVBAR.BLOCKLANGUAGES.LANGUAGES.languageJS}');"><img src="{NAVBAR.BLOCKLANGUAGES.LANGUAGES.languageImage}"></a>
                        <!-- END LANGUAGES -->
                    </div>
                </div>
            </li>
            <!-- END BLOCKLANGUAGES -->

        </ul>
    </div>
</nav>
<!-- END NAVBAR -->
<!-- BEGIN NAVBARSM -->
<div id="navbar-top-sm" style="" aria-expanded="false" class="navbar-collapse collapse navbar navbar-dark">
    <div id="main-menu">
        <div class="visible-xs">
            <div class="container-fluid">
                <div class="nav navbar-nav">
                    <!-- BEGIN ITEM -->
                    <div class="row">
                        <div class="col-md-12">
                            <a class="nav-link {NAVBAR.ITEM.class}" href="{NAVBARSM.ITEM.link}" {NAVBAR.ITEM.attributes}>{NAVBARSM.ITEM.label}</a>
                        </div>
                    </div>
                    <!-- END ITEM -->
                </div>
            </div>
        </div>


    </div>
</div>
<!-- END NAVBARSM -->
