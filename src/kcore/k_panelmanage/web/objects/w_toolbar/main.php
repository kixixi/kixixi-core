<?
/**
 * KIXIXI panelmanage (w_toolbar)
 * web object of panel toolbar
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_panelmanage
 * @subpackage web\objects\w_toolbar
 * @since 1.00
 */
namespace k_panelmanage\web\objects\w_toolbar;
class obj extends \k_webmanager\objects\general\web_object\obj
{
    var $ver = "1.0";

    /**
     * Constructor
     *
     */
    public function __construct($AV =[])
    {
        parent::__construct();
    }


    /**
     * Get Panel Toolbar Object
     *
     * @return this
     */
    public function get($ArrayVariable = [])
    {


        //set Variables
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $logo = $this->K_COMMON->getVarArray($ArrayVariable, "logo");
        $BGColor = $this->K_COMMON->getVarArray($ArrayVariable, "BGColor");
        $textColor = $this->K_COMMON->getVarArray($ArrayVariable, "textColor");
        $showLogo = $this->K_COMMON->getVarArray($ArrayVariable, "showLogo");
        $showName = $this->K_COMMON->getVarArray($ArrayVariable, "showName");
        $showLogin = $this->K_COMMON->getVarArray($ArrayVariable, "showLogin");
        $showLang = $this->K_COMMON->getVarArray($ArrayVariable, "showLang");
        $arrayName = [];
        $arrayLogin = [];
        $navBarArray = [];
        $navBarItemStyle = "";

        // location and language
        $lang = $this->K_COMMON->language;

        //Load Base Template
        $template = $this->getTemplateLayout("template");

        if($showName == "1"){
            $arrayName = ["label" => $this->K_COMMON->getCurrentUserNameComplete()
                , "link" => "#"
                , "target" => "_self"
                , "class" => "label-info label-pill"
                , "attributes" => ""];

        }
        if($showLogin  == "1"){
            $arrayLogin = ["label" => "Logout"
                , "link" => "#"
                , "target" => "_self"
                , "class" => ""
                , "attributes" => 'onclick="KXJS.logout();"'];
        }
        //Set Menu Data
        $arrMenu = [$arrayName,$arrayLogin];

        //Check background color
        if ($BGColor != "")
            $this->K_COMMON->concatVarArray($navBarArray, "style", "background-color:" . $BGColor . ";");

        //Check color
        if ($textColor != "")
            $navBarItemStyle .= "color:" . $textColor . ";";

        //Create blocks
        $template->assign_block_vars("NAVBAR", $navBarArray);
        $template->assign_block_vars("NAVBARSM", []);

        //write logo
        if ($showLogo == "1"){
            if($logo == "")
                $logo = $this->getParamValue("WBSLogo", "", "k_webmanager","");
            else{
                 $urlBase = $this->K_COMMON->getStaticContentURLWeb($webCode);
                $logo = $urlBase.$logo;
            }
            $template->assign_block_vars("NAVBAR.LOGO", ["logo" =>$logo]);

        }

        foreach ($arrMenu as $prog => $menu) {
                if(!empty($menu)) {

                    //write on template
                    $template->assign_block_vars("NAVBAR.ITEMR"
                        , [
                            "label" => $menu["label"]
                            , "link" => $menu["link"]
                            , "target" => $menu["target"]
                            , "attributes" => $menu["attributes"]
                            , "class" => $menu["class"]
                            , "style" => $navBarItemStyle
                        ]
                    );
                    $template->assign_block_vars("NAVBARSM.ITEM"
                        , [
                            "label" => $menu["label"]
                            , "link" => $menu["link"]
                            , "target" => $menu["target"]
                            , "class" => $menu["class"]
                            , "attributes" => $menu["attributes"]
                        ]
                    );
                }
        }
        $pathImages = $this->getMyObjectPath();

        if($showLang == "1"){
            $template->assign_block_vars("NAVBAR.BLOCKLANGUAGES",[]);
            //Languages
            $arLang = array("IT","FR","ES","DE","EN");
            if (in_array($lang, $arLang))
                $template->assign_block_vars("NAVBAR.BLOCKLANGUAGES.CURLANGUAGE"
                    , [
                        "languageImage" => $pathImages . "/images/flags/flag-" . strtolower($lang) . ".png"
                        ,"languageJS" => strtoupper($lang)
                    ]);
            foreach ($arLang as $val) {
            if ($val != $lang)
                $template->assign_block_vars("NAVBAR.BLOCKLANGUAGES.LANGUAGES"
                    , [
                        "languageImage" => $pathImages . "/images/flags/flag-" . strtolower($val) . ".png"
                        ,"languageJS" => strtoupper($val)
                        ]
                );
            }
        }



        //Parse Html
        $html = $template->pparse("body");

        $this->appendHTML("",$html);

        //Return
        return $this;
    }

    /**
     * get Parameterization Form
     *
     * @param array $objParameters array with variables
     *
     * @return webobject
     */

    public function parameterizationGetForm($objParameters,$objExtraParameters)
    {
        //set variables
        $webCode = $this->K_COMMON->getVarArray($objExtraParameters, "webCode");
        $arrayFormField = $objParameters;
        $tmplPluginName = $this->K_COMMON->getVarArray($objExtraParameters, "tmplPluginName");

        //append logo
        $label = "Logo";
        $name = "logo";
        $value = $this->K_COMMON->getVarArray($arrayFormField, $name);
        $this->appendInput(["column"=>6,"row"=>true], ["type" => "serverFile", "getFolderByPlugin" => $tmplPluginName, "labelInput" => $label, "id" => $name,"name" => $name, "value" => $value, "webCode" => $webCode]);

        //append BG
        $label = "Back Ground Color";
        $name = "BGColor";
        $value = $this->K_COMMON->getVarArray($arrayFormField, $name);
        $this->appendInput(["column"=>6], ["type" => "colorPicker", "labelInput" => $label, "id" => $name, "name" => $name, "value"=> $value]);

        // append Txt color
        $label = "Text Color";
        $name = "textColor";
        $value = $this->K_COMMON->getVarArray($arrayFormField, $name);
        $this->appendInput(["column"=>6,"row"=>true], ["type" => "colorPicker", "labelInput" => $label, "id" => $name, "name" => $name, "value"=> $value]);

        //append Name
        $label = "Show Logo";
        $name = "showLogo";
        $value = $this->K_COMMON->getVarArray($arrayFormField, $name);
        $this->appendInput(["column"=>6,"row"=>true], ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value"=> "1","checked" => $value]);

        //append Name
        $label = "Show Name";
        $name = "showName";
        $value = $this->K_COMMON->getVarArray($arrayFormField, $name);
        $this->appendInput(["column"=>6], ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value"=> "1","checked" => $value]);


        //append login/logout
        $label = "Show Login/Logout";
        $name = "showLogin";
        $value = $this->K_COMMON->getVarArray($arrayFormField, $name);
        $this->appendInput(["column"=>6,"row"=>true], ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value"=> "1","checked" => $value]);

        // append language
        $label = "Show Language";
        $name = "showLang";
        $value = $this->K_COMMON->getVarArray($arrayFormField, $name);
        $this->appendInput(["column"=>6], ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value"=> "1","checked" => $value]);
        return $this;

    }

    /**
     * save Parameterization Data
     *
     * @param array $AV array with variables
     *
     * @return webobject
     */
    public function parameterizationSaveData($AV)
    {
        $logo = $this->K_COMMON->getVarArray($AV, "logo");
        $BGColor = $this->K_COMMON->getVarArray($AV, "BGColor");
        $textColor = $this->K_COMMON->getVarArray($AV, "textColor");
        $showLogo = $this->K_COMMON->getVarArray($AV, "showLogo");
        $showName = $this->K_COMMON->getVarArray($AV, "showName");
        $showLogin = $this->K_COMMON->getVarArray($AV, "showLogin");
        $showLang = $this->K_COMMON->getVarArray($AV, "showLang");


        return ["logo"=>$logo
            ,"BGColor"=>$BGColor
            ,"textColor"=>$textColor
            ,"showLogo"=>$showLogo
            ,"showName"=>$showName
            ,"showLogin"=>$showLogin
            ,"showLang"=>$showLang
        ];
    }


}

?>