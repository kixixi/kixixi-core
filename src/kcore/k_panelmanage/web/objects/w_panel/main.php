<?
/**
 * KIXIXI panelmanage (w_panel)
 * web object of panel
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_panelmanage
 * @subpackage web\objects\w_panel
 * @since 1.00
 */
namespace k_panelmanage\web\objects\w_panel;
class obj extends \k_webmanager\objects\general\web_object\obj
{
    var $dictionary;

    /**
     * Get Object
     *
     * @param array $arrayVariable array of variables for panel
     *
     *
     * @return this
     */
    function get($arrayVariable = [])
    {
        $panel = $this->K_COMMON->getPluginObject("k_panelmanage","panel");

        return $panel->get($arrayVariable);

    }

   
}

?>