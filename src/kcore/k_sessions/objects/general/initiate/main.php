<?
/**
 * KIXIXI k_sessions (kixixi session initiate)
 * Start kixixi Session
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_sessions
 * @subpackage objects\initiate
 * @since 1.00
 */
namespace k_sessions\objects\general\initiate;
class obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";


    /**
     * Initiate Kixixi Session
     * Load the first files
     *
     * @return boolean
     */
    function initiate()
    {

        //set global variables
        global $K_COMMON;

        //include base object
        include_once($_SERVER['DOCUMENT_ROOT'] . "/kcore/k_common/objects/general/base/main.php");
        include_once($_SERVER['DOCUMENT_ROOT'] . "/kcore/k_common/objects/general/common/main.php");


        //set global variable
        $K_COMMON = new \k_common\objects\general\common\obj();
        $K_COMMON->pathCstSystem = $_SERVER['DOCUMENT_ROOT'];
        $K_COMMON->pathKixixi = $K_COMMON->pathCstSystem;

        //Set Local Temp Dir
        $K_COMMON->pathTmp = $K_COMMON->pathKixixi . "/ktmp/";
        ini_set("session.save_path", $K_COMMON->pathTmp);

        //include auto loader object
        include_once($_SERVER['DOCUMENT_ROOT'] . "/kcore/k_common/objects/general/autoloader/main.php");
        new \k_common\objects\general\autoloader\obj();

        //Load Configuration File
        include_once($_SERVER['DOCUMENT_ROOT'] . "/ksettings/settings.php");

        //Define Variables for stati content
        $K_COMMON->setStaticContentVariables();

        //Load main common plugin
        include_once($_SERVER['DOCUMENT_ROOT'] . "/kcore/k_sessions/objects/general/handler/main.php");

        //start session
        $objStart = new \k_sessions\objects\general\start\obj();
        $objStart->start();

        return true;


    }





}

?>