<?
/**
 * KIXIXI k_sessions (session handler)
 * Manage Sessions Data
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_sessions
 * @subpackage objects\handler
 * @since 1.00
 */
namespace k_sessions\objects\general\handler;
class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Array with session fields
     *
     * @since 1.00
     * @access public
     * @var array
     */
    protected $fieldarray = array();

    /**
     * table session
     *
     * @since 1.00
     * @access public
     * @var object
     */
    protected $tableSES;

    /**
     * Constructor
     *
     */
    public function __construct()
    {

        parent::__construct();
        $this->tableSES = $this->getTable("sessions");
        $this->fieldarray['session_id'] = "";
        $this->fieldarray['user_id'] = "";
        $this->fieldarray['date_created'] = "";
        $this->fieldarray['last_updated'] = "";
        $this->fieldarray['session_data'] = '';
    }

    /**
     * destruct
     *
     */
    public function __destruct()

    {
        @session_write_close();

    }


    /**
     * Open Session
     *
     * @return boolean
     */
    public function open($save_path, $session_name)
    {
        return TRUE;
    }

    /**
     * close session
     *
     * @return boolean
     */
    public function close()
    {
        if (!empty($this->fieldarray)) {
            $result = $this->gc(ini_get('session.gc_maxlifetime'));
            return $result;
        }
        return FALSE;
    }

    /**
     * read session
     *
     * @param string $session_id id of session
     *
     * @return string
     */
    public function read($session_id)
    {

        $this->tableSES->get("*", ["session_id" => $session_id]);
        $data = $this->tableSES->fetch();
        if (!$data)
            return "";
        else {
            $this->fieldarray['session_id'] = $data->session_id;
            $this->fieldarray['user_id'] = $data->user_id;
            $this->fieldarray['date_created'] = $data->date_created;
            $this->fieldarray['last_updated'] = $data->last_updated;
            $this->fieldarray['session_data'] = '';
            return $data->session_data;
        }

    }

    /**
     * write session data
     *
     * @param string $session_id id of session
     * @param string $session_data session data to write
     *
     * @return boolean
     */
    public function write($session_id, $session_data)
    {


        if ($this->fieldarray['session_id'] != "") {
            if ($this->fieldarray['session_id'] != $session_id) {
                // user is starting a new session with previous data
                $this->fieldarray['session_id'] = "";
            } // if
        } // if

        if (isset($_SESSION["setSessionNotExpire"])) $not_expire = $_SESSION["setSessionNotExpire"];
        else $not_expire = 0;

        if ($this->fieldarray['session_id'] == "") {
            // create new record

            $AVFields = [
                'session_id' => $session_id
                , 'date_created' => date('Y-m-d H:i:s')
                , 'last_updated' => date('Y-m-d H:i:s')
                , 'session_data' => $session_data
                , 'not_expire' => $not_expire
            ];
            if (isset($_SESSION['KSESSION_Logon_User']))
                $AVFields['user_id'] = $_SESSION['KSESSION_Logon_User'];
                $this->tableSES->put($AVFields);


        } else {

            $AVFields = [
                'last_updated' => date('Y-m-d H:i:s')
                , 'session_data' => $session_data
                , 'not_expire' => $not_expire
            ];



            if (isset($_SESSION['KSESSION_Logon_User']))
                $AVFields['user_id'] = $_SESSION['KSESSION_Logon_User'];

            $this->tableSES->put($AVFields, ["session_id" => $session_id]);


        } // if

        return TRUE;

    }

    /**
     * destroy session
     *
     * @param string $session_id id of session
     *
     * @return boolean
     */
    public function destroy($session_id)
    {

        $this->tableSES->delete(["session_id" => $session_id]);

        return TRUE;

    }

    /**
     * gc
     *
     * @param string $max_lifetime session life time (seconds)
     *
     * @return boolean
     */
    public function gc($max_lifetime)
    {

        $real_now = date('Y-m-d H:i:s');
        $dt1 = strtotime("$real_now -$max_lifetime seconds");
        $dt2 = date('Y-m-d H:i:s', $dt1);

        $this->tableSES->delete(["last_updated < '$dt2' AND not_expire = 0"]);

        return TRUE;

    }


    /**
     * Set actual session as not expire
     *
     * @param integer $ne 1 if not expiry session
     *
     */
    public function setSessionNotExpire($ne = 1)
    {

        $table = $this->getTable("sessions");
        $table->put(["not_expire" => $ne], ["session_id" => session_id()]);

        $_SESSION["setSessionNotExpire"] = $ne;

    }



}

?>