<?
/**
 * KIXIXI k_session (api)
 * functions for manage login
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_session
 * @subpackage objects\api
 * @since 1.00
 */
namespace k_sessions\objects\general\api;
class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";



    /**
     * reset session variable
     *
     *
     */
    public function resetSessionVariables()
    {

        //Include Module
        $tableREG = $this->K_COMMON->getPluginDataObject("k_registry", "registry");
        $tableLGN = $this->K_COMMON->getPluginDataObject("k_secauth", "login");

        //set variables
        $anaCode = $this->K_COMMON->getCurrentUser();
        $loginWebCodeParam = $this->getParamValue("loginWebCode", $this->webCode, "k_secauth");

        if ($loginWebCodeParam != "")
            $webToLogin = $loginWebCodeParam;
        else
            $webToLogin = $this->K_COMMON->getCurrentWeb();

        //read data
        $tableREG->get("", ["ana_code" => $anaCode]);
        $dataREG = $tableREG->fetch();


        if ($dataREG) {

            //load login data
            $tableLGN->get("", ["lgn_web_code" => $webToLogin, "lgn_user_code" => $anaCode]);
            $dataLGN = $tableLGN->fetch();

            //Set Variable Session
            $_SESSION["KSESSION_User_FirstName"] = $dataREG->ana_first_name;
            $_SESSION["KSESSION_User_LastName"] = $dataREG->ana_last_name;
            $_SESSION["KSESSION_Logon_User"] = $dataREG->ana_code;
            $_SESSION["KSESSION_Level_Typ_Code"] = $dataLGN->lgn_level_code;

            //Get Language
            $Language = $dataREG->ana_language;
            $this->K_COMMON->language = strtoupper($Language);
            $_SESSION["KSESSION_language"] = strtoupper($this->K_COMMON->language);


            return array("success" => "true");

        }
        else
            return array("success" => "false", "reason" => "nouser", "message" => "User Not Found");


    }


}

?>