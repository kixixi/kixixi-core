<?
/**
 * KIXIXI k_sessions (session start)
 * Start kixixi Session
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_sessions
 * @subpackage objects\start
 * @since 1.00
 */

namespace k_sessions\objects\general\start;
class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Start Kixixi Session
     * Set global variables
     *
     * @return boolean
     */
    function start()
    {

        //set global variables
        global $K_COMMON;
        global $K_SECAUTH;
        global $K_CUSTOMMANAGER;
        global $kxDBManager;
        $actLanguage = $K_COMMON->getVarSession("KSESSION_language");

        //Set security variable
        $K_SECAUTH = new \k_secauth\objects\general\api\obj;

        //Connection to DB
        $kxDBManager = new \k_dbmanager\objects\general\manager\obj(constant("DATABASE_USER"), constant("DATABASE_PWD"), constant("DATABASE_NAME"), constant("KX_DBHOST"), constant("KX_DBPORT"));
        $K_COMMON->kxdb = &$kxDBManager;

        //Start PHP Session
        $sessionHandler = new \k_sessions\objects\general\handler\obj();
        session_set_save_handler(array(&$sessionHandler, 'open'), array(&$sessionHandler, 'close'),
            array(&$sessionHandler, 'read'),
            array(&$sessionHandler, 'write'),
            array(&$sessionHandler, 'destroy'),
            array(&$sessionHandler, 'gc'));
        register_shutdown_function('session_write_close');

        //set others global variables
        $K_CUSTOMMANAGER = new \k_custommanager\objects\general\customization\obj;

        //Load dictionary for common
        $K_COMMON->dictionary = $K_COMMON->getPluginDictionary("k_common", "objects\general\common");

        //Verify Referrer
        $url_site = $_SERVER['SERVER_NAME'];

        $url_site_referer = "";
        if (isset($_SERVER['HTTP_REFERER'])) {
            $parts = parse_url($_SERVER['HTTP_REFERER']);
            if (isset($parts['host']))
                $url_site_referer = $parts['host'];
        }
        $K_COMMON->URLReferrer = $url_site_referer;

        //Check if web site exist on DB
        $tableWS = $K_COMMON->getPluginDataObject("k_webmanager", "web_sites");

        $tableWS->get("*", ["web_name" => $url_site]);
        if ($tableWS->getNumRows() == 0)
            die("This Web Site is not recognized!");
        $dataWS = $tableWS->fetch();


        //set web site variables
        $K_COMMON->webCode = $dataWS->web_code;
        if ($K_COMMON->editingWebCode == "")
            $K_COMMON->editingWebCode = $dataWS->web_code;
        $K_COMMON->pathCstWeb = $K_COMMON->pathCstSystem . "/kwebsites/" . $K_COMMON->webCode;

        //Verify if exist custom webconfig.php for this web site
        if (file_exists($K_COMMON->pathCstWeb . "/webconfig.php")) {

            include_once($K_COMMON->pathCstWeb . "/webconfig.php");

            //Overwrite Variables for static content
            $K_COMMON->setStaticContentVariables();

        }


        //Verify Cookie Session
        if (isset($_COOKIE["kscid"]))
            session_id($_COOKIE["kscid"]);

        //Start Session
        $tokenTest = $K_COMMON->getVarRequest("token");
        if ($tokenTest != "")
            session_id($tokenTest); //The token have priority on cookie

        @session_start();

        if (session_id() == "undefined") //check for security app reason
            session_regenerate_id(true);

        $kscid = session_id();
        setcookie("kscid", $kscid, time() + (365 * 24 * 60 * 60 * 10), "/"); //kscid = Web Code Session ID

        //Set Variables
        $isLogged = $K_SECAUTH->verifyUserLogged();


        //Web Site Editing
        if (isset($_COOKIE["kcews"]))
            $setEditingWebSite = $_COOKIE["kcews"];
        else
            $setEditingWebSite = $K_COMMON->getCurrentWeb();

        $K_COMMON->setEditingWeb($setEditingWebSite);
        setcookie("kcews", $K_COMMON->editingWebCode, time() + (365 * 24 * 60 * 60 * 10), "/", NULL); //kcews = Web Site code Editing

        //Language & Location
        $setLanguage = "";
        if ($K_COMMON->getVarRequest("setLanguage") != "")
            $setLanguage = strtoupper($K_COMMON->getVarRequest("setLanguage"));
        else
        {
            if (isset($_COOKIE["kclng"]))
            {
                $setLanguage = $_COOKIE["kclng"];
            }
            else {

                if ($isLogged)
                    $setLanguage = strtoupper($this->K_COMMON->getUserLanguage($K_COMMON->getCurrentUser()));
                else
                {
                    $languageWS = $this->getParamValue("LanguageWS", "", "k_webmanager","");
                    if ($languageWS != "")
                        $setLanguage = $languageWS; //Check if Language web site force
                    //else
                    //    $setLanguage = strtoupper($K_COMMON->getVarRequest("setLanguage"));
                }


            }
        }

        $server_language = isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : "EN-EN";
        $K_COMMON->location = strtoupper(substr($server_language, 3, 2));
        $_SESSION["KSESSION_location"] = $K_COMMON->location;

        if ($setLanguage == "") {
            if ($K_COMMON->getVarSession("KSESSION_language") == "") {


                if ($isLogged)
                    $K_COMMON->language = strtoupper($this->K_COMMON->getUserLanguage($K_COMMON->getCurrentUser()));
                elseif (isset($_COOKIE["kclng"]))
                    $K_COMMON->language = strtoupper($_COOKIE["kclng"]);
                else
                    $K_COMMON->language = strtoupper(substr($server_language, 0, 2));

                $_SESSION["KSESSION_language"] = strtoupper($K_COMMON->language);

            } else {

                $K_COMMON->language = strtoupper($_SESSION["KSESSION_language"]);
                $K_COMMON->location = $_SESSION["KSESSION_location"];

            }
        } else {

            if ($actLanguage != $setLanguage)
                if ($isLogged)
                    $this->K_COMMON->setUserLanguage($K_COMMON->getCurrentUser(), $setLanguage);
            $K_COMMON->language = strtoupper($setLanguage);
            $_SESSION["KSESSION_language"] = strtoupper($setLanguage);
            $_SESSION["KSESSION_location"] = $K_COMMON->location;


        }

        setcookie("kclng", $K_COMMON->language, time() + (365 * 24 * 60 * 60 * 10), "/"); //kclng = Web Site code language

        //Search for ref code (ana_ref_code)
        if ($K_COMMON->getVarRequest("friendCode") != "")
            $_SESSION["KSESSION_friendCode"] = $K_COMMON->getVarRequest("friendCode");

        //Set decimal and thousand separator
        if (($K_COMMON->location == "EN") || ($K_COMMON->location == "IE")) {
            $K_COMMON->decimalSep = ".";
            $K_COMMON->thousandsSep = ",";
        }


        //Get Real URL Web Site with parameters
        $K_COMMON->URLActual = $_SERVER['REQUEST_URI'];
        if (substr($K_COMMON->URLActual, strlen($K_COMMON->URLActual) - 1, 1) == "/")
            $K_COMMON->URLActual = substr($K_COMMON->URLActual, 0, strlen($K_COMMON->URLActual) - 1);


        //return
        return true;


    }


}

?>