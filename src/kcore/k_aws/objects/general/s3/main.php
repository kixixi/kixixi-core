<?
/**
 * KIXIXI AWS S3
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_common
 * @subpackage objects\common
 * @since 1.00
 */
namespace k_aws\objects\general\s3;
class obj extends \k_common\objects\general\base\obj
{
    var $ver = "1.0";

    /**
     * Calculate the HMAC SHA1 hash of a string.
     *
     * @param string $key The key to hash against
     * @param string $data The data to hash
     * @param int $blocksize Optional blocksize
     * @return string HMAC SHA1
     */
    function cryptoData($key, $data, $blocksize = 64) 
    {
        
        if (strlen($key) > $blocksize) $key = pack('H*', sha1($key));
        $key = str_pad($key, $blocksize, chr(0x00));
        $ipad = str_repeat(chr(0x36), $blocksize);
        $opad = str_repeat(chr(0x5c), $blocksize);
        $hmac = pack('H*', sha1(
            ($key ^ $opad) . pack('H*', sha1(
                ($key ^ $ipad) . $data
            ))
        ));
        return base64_encode($hmac);
    }

    /**
     * Delete a file from S3
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    bucketName: bucket name
     *    *    fileName: file to delete (with correct path)
     *    *    awsAccessKey: bucket access key
     *    *    awsSecretKey: bucket secret key
     *
     * @return boolean
     */
    function deleteFile($arrayVariable)
    {
        $pathSDKAWS = $this->K_COMMON->getPathSDK(["name" => "aws"]);
        include_once($pathSDKAWS . '/s3/S3.php');


        //Load Variable
        $bucketName = $this->K_COMMON->getVarArray($arrayVariable, "bucketName");
        $fileName = $this->K_COMMON->getVarArray($arrayVariable, "fileName");
        $awsAccessKey = $this->K_COMMON->getVarArray($arrayVariable, "awsAccessKey");
        $awsSecretKey = $this->K_COMMON->getVarArray($arrayVariable, "awsSecretKey");

        //Start S3
        $s3 = new \S3($awsAccessKey, $awsSecretKey);
        $s3->setEndpoint("s3-eu-west-1.amazonaws.com");


        return $s3->deleteObject($bucketName, $fileName);


    }

    /**
     * Delete a folder from S3
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    bucketName: bucket name
     *    *    folderName: folder to delete (with correct path)
     *    *    awsAccessKey: bucket access key
     *    *    awsSecretKey: bucket secret key
     *
     * @return boolean
     */
    function deleteFolder($arrayVariable)
    {
        //Include SDK
        $pathSDKAWS = $this->K_COMMON->getPathSDK(["name" => "aws"]);
        include_once($pathSDKAWS . '/aws-v3/aws.phar');


        //Load Variable
        $bucketName = $this->K_COMMON->getVarArray($arrayVariable, "bucketName");
        $folderName = $this->K_COMMON->getVarArray($arrayVariable, "folderName");
        $awsAccessKey = $this->K_COMMON->getVarArray($arrayVariable, "awsAccessKey");
        $awsSecretKey = $this->K_COMMON->getVarArray($arrayVariable, "awsSecretKey");

        // Create an S3 client
        $s3 = new \Aws\S3\S3Client([
            'region'  => 'eu-west-1',
            'version' => '2006-03-01',
            'credentials' => [
                'key'    => $awsAccessKey,
                'secret' => $awsSecretKey,
            ],
        ]);


        //List the objects and get the keys.
        $results = $s3->ListObjectsV2 ([
            'Bucket' => $bucketName,
            'Prefix' => $folderName
        ]);



        if (isset($results['Contents'])) {
            foreach ($results['Contents'] as $result) {
                $s3->deleteObject([
                    'Bucket' => $bucketName,
                    'Key' => $result['Key']
                ]);
            }
        }

        return true;


    }

    /**
     * Upload file on S3
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    sourceFile: file to be upload with complete path
     *    *    targetDir: directory where upload file
     *    *    targetFile: the file name on S3
     *    *    bucketName: bucket name
     *    *    awsAccessKey: bucket access key
     *    *    awsSecretKey: bucket secret key
     *    *    authRead: default "public-read"
     *    *    deleteSource: remove file from local 
     * @return array result of upload (Ex: ["success" = "true"]
     */
    function uploadFile($arrayVariable)
    {
        $pathSDKAWS = $this->K_COMMON->getPathSDK(["name" => "aws"]);
        include_once($pathSDKAWS . '/s3/S3.php');

        //set Variables
        $sourceFile = $this->K_COMMON->getVarArray($arrayVariable, "sourceFile");
        $targetDir = $this->K_COMMON->getVarArray($arrayVariable, "targetDir");
        $targetFile = $this->K_COMMON->getVarArray($arrayVariable, "targetFile");
        $bucketName = $this->K_COMMON->getVarArray($arrayVariable, "bucketName");
        $awsAccessKey = $this->K_COMMON->getVarArray($arrayVariable, "awsAccessKey");
        $awsSecretKey = $this->K_COMMON->getVarArray($arrayVariable, "awsSecretKey");
        $authRead = $this->K_COMMON->getVarArray($arrayVariable, "authRead");
        $deleteSource = $this->K_COMMON->getVarArray($arrayVariable, "deleteSource"); //Delete Original Local File after upload
        $fileUploaded = false;

        //check variables
        if ($authRead == "")
            $authRead = "public-read";
        if ($deleteSource == "")
            $deleteSource = false;
        if ($targetDir != "")
            $targetFile = (substr($targetDir, -1) != "/" ? $targetDir . "/" : $targetDir) . $targetFile;

        //Start S3
        $s3 = new \S3($awsAccessKey, $awsSecretKey);

        //Get File Data
        $s3->setEndpoint("s3-eu-west-1.amazonaws.com");
        if ($s3->putObjectFile($sourceFile, $bucketName, $targetFile, $authRead)) {
            $fileUploaded = true;
            if ($deleteSource)
                unlink($sourceFile);
        }

        return ["success" => $fileUploaded];


    }


    /**
     * Create temporary URLs to your protected Amazon S3 files.
     *
     * @param string $accessKey Your Amazon S3 access key
     * @param string $secretKey Your Amazon S3 secret key
     * @param string $bucket The bucket (bucket.s3.amazonaws.com)
     * @param string $path The target file path
     * @param int $expires In minutes
     * @return string Temporary Amazon S3 URL
     * @see http://awsdocs.s3.amazonaws.com/S3/20060301/s3-dg-20060301.pdf
     */
    function getTemporaryLink($accessKey, $secretKey, $bucket, $path, $expires = 5)
    {


        // Calculate expiry time
        $expires = time() + intval(floatval($expires) * 60);
        // Fix the path; encode and sanitize
        $path = str_replace('%2F', '/', rawurlencode($path = ltrim($path, '/')));
        // Path for signature starts with the bucket
        $signpath = '/' . $bucket . '/' . $path;
        // S3 friendly string to sign
        $signsz = implode("\n", $pieces = array('GET', null, null, $expires, $signpath));
        // Calculate the hash
        $signature = $this->cryptoData($secretKey, $signsz);
        // Glue the URL ...
        //$url = sprintf('http://%s.s3.amazonaws.com/%s', $bucket, $path);
        $url = sprintf('https://s3-eu-west-1.amazonaws.com/%s/%s', $bucket, $path);
        // ... to the query string ...
        $qs = http_build_query($pieces = array(
            'AWSAccessKeyId' => $accessKey,
            'Expires' => $expires,
            'Signature' => $signature,
        ));
        // ... and return the URL!
        //return $url;
        return $url . '?' . $qs;
    }

    /**
     * load file names and directory names of a folder
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    bucketName: bucket name
     *    *    awsAccessKey: bucket access key
     *    *    awsSecretKey: bucket secret key
     *    *    dir: dir to read
     * @return array results of read (Ex: ["folders" => ["folder1", "folder2"], "files" => ["f1","f2","f3"]]
     */
    function loadFolderContent($arrayVariable)
    {

        //load sdk
        $pathSDKAWS = $this->K_COMMON->getPathSDK(["name" => "aws"]);
        include_once($pathSDKAWS . '/s3/S3.php');

        //set variables
        $bucketName = $this->K_COMMON->getVarArray($arrayVariable, "bucketName");
        $awsAccessKey = $this->K_COMMON->getVarArray($arrayVariable, "awsAccessKey");
        $awsSecretKey = $this->K_COMMON->getVarArray($arrayVariable, "awsSecretKey");
        $dir = $this->K_COMMON->getVarArray($arrayVariable, "dir");
        $delimiter = "/";
        $result = [];

        //Start S3
        $s3 = new \S3($awsAccessKey, $awsSecretKey);
        $s3->setEndpoint("s3-eu-west-1.amazonaws.com");

        //read folder
        $contents = $s3->getBucket($bucketName, $dir, "", "", $delimiter, true);

        $n = 1;
        $folder_numb = 0;
        $file_numb = 0;
        $folder = [];
        $files = [];
        foreach ($contents as $p => $v)
        {
            $p = str_replace($dir, "", $p);
            if ($p != "")
                if (substr($p, "-1") == "/") {
                    $folder[$folder_numb] = substr($p, 0, -1);
                    $folder_numb++;
                } else {
                    $files[$file_numb] = $p;
                    $file_numb++;

                }
            $n++;
        }

        $result["folders"] = $folder;
        $result["files"] = $files;

        return $result;
    }

    /**
     * craete dir
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    bucketName: bucket name
     *    *    awsAccessKey: bucket access key
     *    *    awsSecretKey: bucket secret key
     *    *    dir: dir where crete new folder
     *    *    newFolderName: folder to create
     * @return array results of read (Ex: ["folders" => ["folder1", "folder2"], "files" => ["f1","f2","f3"]]
     */
    function mkDir($arrayVariable)
    {
        $pathSDKAWS = $this->K_COMMON->getPathSDK(["name" => "aws"]);
        include_once($pathSDKAWS . '/s3/S3.php');


        //Load Variable
        $bucketName = $this->K_COMMON->getVarArray($arrayVariable, "bucketName");
        $dir = $this->K_COMMON->getVarArray($arrayVariable, "dir");
        $newFolderName = $this->K_COMMON->getVarArray($arrayVariable, "newFolderName");
        $awsAccessKey = $this->K_COMMON->getVarArray($arrayVariable, "awsAccessKey");
        $awsSecretKey = $this->K_COMMON->getVarArray($arrayVariable, "awsSecretKey");

        //new Folder Name
        $newFolderName = strtolower($newFolderName);
        $newFolderName = str_replace(" ", "_", $newFolderName);

        //Start S3
        $s3 = new \S3($awsAccessKey, $awsSecretKey);
        $s3->setEndpoint("s3-eu-west-1.amazonaws.com");

        $s3->putObject("", $bucketName, $dir . $newFolderName . "/", "public-read");



    }

    /**
     * Image Resize
     *
     *
     * @since 1.00
     * @access public
     *
     * * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *
     * *    filename    : file of image to resize
     * *    newfilename    : file of image resized (if empty same of original)
     * *    width   : pixel or percentage
     * *    height  : pixel or percentage
     * *    checkWH : if true check if initial image width > $width (same for height) otherwise not resize it
     * *    compression: compression (default 75)
     *
     * @return array Ex: ["success" => true, "width" => "NEW WIDTH", "height" => "NEW HEIGHT"]
     *
     */
    function imageResize($ArrayVariable)
    {
        //include
        $pathSDKAWS = $this->K_COMMON->getPathSDK(["name" => "aws"]);
        include_once($pathSDKAWS . '/s3/S3.php');

        //set variables
        $filename = $this->K_COMMON->getVarArray($ArrayVariable, "filename");
        $newfilename = $this->K_COMMON->getVarArray($ArrayVariable, "newfilename");
        $width = $this->K_COMMON->getVarArray($ArrayVariable, "width");
        $height = $this->K_COMMON->getVarArray($ArrayVariable, "height");
        $checkWH = $this->K_COMMON->getVarArray($ArrayVariable, "checkWH");
        $compression = $this->K_COMMON->getVarArray($ArrayVariable, "compression");
        $bucketName = $this->K_COMMON->getVarArray($ArrayVariable, "bucketName");
        $awsAccessKey = $this->K_COMMON->getVarArray($ArrayVariable, "awsAccessKey");
        $awsSecretKey = $this->K_COMMON->getVarArray($ArrayVariable, "awsSecretKey");
        if ($newfilename == "")
            $newfilename = $filename;


        //include image api
        $apiImage = $this->K_COMMON->getPluginObject("k_imagemanager","api");

        //Start S3
        $s3 = new \S3($awsAccessKey, $awsSecretKey);


        //get file
        $fileNameTmp = $this->K_COMMON->getPathTmp() . "tmp_" . time();
        $s3->setEndpoint("s3-eu-west-1.amazonaws.com");
        $s3->getObject($bucketName, $filename, $fileNameTmp);


        //resize
        $res = $apiImage->imageResize(
            [
                "filename" => $fileNameTmp
                , "newfilename" => $fileNameTmp
                , "width" => $width
                , "height" => $height
                , "checkWH" => $checkWH
                , "compression" => $compression
            ]
        );

        //upload on S3
        $ParamFile = array();
        $ParamFile["sourceFile"] = $fileNameTmp;
        $ParamFile["targetFile"] = $newfilename;
        $ParamFile["deleteOri"] = true;
        $ParamFile["awsAccessKey"] = $awsAccessKey;
        $ParamFile["awsSecretKey"] = $awsSecretKey;
        $ParamFile["bucketName"] = $bucketName;
        $this->uploadFile($ParamFile);

        return $res;

    }

    /**
     * Image rotate
     *
     * @since 1.00
     * @access public
     *
     * * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *
     * *    filename    : file of image to rotate
     * *    rotang    : angle of rotation
     *
     * @return array
     */
    function imageRotate($ArrayVariable)
    {

        //include
        $pathSDKAWS = $this->K_COMMON->getPathSDK(["name" => "aws"]);
        include_once($pathSDKAWS . '/s3/S3.php');

        //set variable
        $filename = $this->K_COMMON->getVarArray($ArrayVariable, "filename");
        $rotang = $this->K_COMMON->getVarArray($ArrayVariable, "rotang");
        $filename = (substr($filename, 0,1) == "/" ? substr($filename ,1): $filename);
        $bucketName = $this->K_COMMON->getVarArray($ArrayVariable, "bucketName");
        $awsAccessKey = $this->K_COMMON->getVarArray($ArrayVariable, "awsAccessKey");
        $awsSecretKey = $this->K_COMMON->getVarArray($ArrayVariable, "awsSecretKey");

        //include image api
        $apiImage = $this->K_COMMON->getPluginObject("k_imagemanager","api");

        //Start S3
        $s3 = new \S3($awsAccessKey, $awsSecretKey);


        //get file
        $newfilename = $this->K_COMMON->getPathTmp() . "/tmp_" . time();
        $s3->setEndpoint("s3-eu-west-1.amazonaws.com");
        $s3->getObject($bucketName, $filename, $newfilename);

        //resize
        $apiImage->imageRotate(
            [
                "filename" => $newfilename
                , "newfilename" => $newfilename
                , "rotang" => $rotang
            ]
        );

        //upload on S3
        $ParamFile = [];
        $ParamFile["sourceFile"] = $newfilename;
        $ParamFile["targetFile"] = $filename;
        $ParamFile["deleteOri"] = true;
        $ParamFile["awsAccessKey"] = $awsAccessKey;
        $ParamFile["awsSecretKey"] = $awsSecretKey;
        $ParamFile["bucketName"] = $bucketName;
        $this->uploadFile($ParamFile);


        return array("success" => "true");



    }

    /**
     * Copy Folder From S3 To Local
     *
     * @since 1.00
     * @access public
     *
     * * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *
     * *    bucketDir    : dir of bucket
     * *    localDir    : local dir
     *
     */
    function downloadFolderFromS3($arrayVariable)
    {

        //set variables
        $bucketDir = $this->K_COMMON->getVarArray($arrayVariable, "bucketDir");
        $localDir = $this->K_COMMON->getVarArray($arrayVariable, "localDir");
        $bucketName = $this->K_COMMON->getVarArray($arrayVariable, "bucketName");

        $sourceDir = "s3://" . $bucketName . "/" . $bucketDir;

        //Include SDK
        $pathSDKAWS = $this->K_COMMON->getPathSDK(["name" => "aws"]);
        include_once($pathSDKAWS . '/aws-v3/aws.phar');


        // Create an S3 client
        $s3 = new \Aws\S3\S3Client([
            'region' => 'eu-west-1',
            'version' => '2006-03-01',
        ]);


        $manager = new \Aws\S3\Transfer($s3, $sourceDir, $localDir);
        $manager->transfer();
    }

    /**
     * Copy Folder From Local To S3 or From S3 To Local or S3 To S3
     *
     * @since 1.00
     * @access public
     *
     * * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *
     * *    bucketDir    : dir of bucket
     * *    localDir    : local dir
     *
     */
    function uploadFolderToS3($arrayVariable)
    {

        //set variables
        $bucketDir = $this->K_COMMON->getVarArray($arrayVariable, "bucketDir");
        $localDir = $this->K_COMMON->getVarArray($arrayVariable, "localDir");
        $bucketName = $this->K_COMMON->getVarArray($arrayVariable, "bucketName");

        $targetDir = "s3://" . $bucketName . "/" . $bucketDir;

        //Include SDK
        $pathSDKAWS = $this->K_COMMON->getPathSDK(["name" => "aws"]);
        include_once($pathSDKAWS . '/aws-v3/aws.phar');


        // Create an S3 client
        $s3 = new \Aws\S3\S3Client([
            'region' => 'eu-west-1',
            'version' => '2006-03-01',
        ]);

        $manager = new \Aws\S3\Transfer($s3, $localDir, $targetDir,[
            'before' => function (\Aws\Command $command) {
                $command['CacheControl'] = 'max-age=3600';
                $command['ACL'] = 'public-read';
            },
        ]);
        $manager->transfer();
    }

    /**
     * duplicate Folder From S3 To S3
     *
     * @since 1.00
     * @access public
     *
     * * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *
     * *    sourceDir    : file of image to rotate
     * *    targetDir    : angle of rotation
     *
     */
    function duplicateFolderS3($arrayVariable)
    {

        //set variables
        $sourceDir = $this->K_COMMON->getVarArray($arrayVariable, "sourceDir");
        $targetDir = $this->K_COMMON->getVarArray($arrayVariable, "targetDir");
        $bucketName = $this->K_COMMON->getVarArray($arrayVariable, "bucketName");


        //Include SDK
        $pathSDKAWS = $this->K_COMMON->getPathSDK(["name" => "aws"]);
        include_once($pathSDKAWS . '/aws-v3/aws.phar');


        // Create an S3 client
        $s3 = new \Aws\S3\S3Client([
            'region' => 'eu-west-1',
            'version' => '2006-03-01',
        ]);


        $objects = $s3->getIterator('ListObjects', array('Bucket' => $bucketName, 'Prefix' => $sourceDir));
        foreach ($objects as $object) {

            $newPath = str_replace($sourceDir, "", $object['Key']);

            $s3->copyObject(array(
                'Bucket'     => $bucketName,
                'Key'        => $targetDir . $newPath,
                'CopySource' => "{$bucketName}/{$object['Key']}"
            ));

        }




    }




}

?>