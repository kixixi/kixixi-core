<?
/**
 * KIXIXI HTML Components (tag)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage web\objects\w_tablelist
 * @since 1.00
 */
namespace k_htmlcomponents\web\objects\w_tablelist;

class obj extends \k_webmanager\objects\general\web_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";


    public function get($arrayParam = [])
    {


        //get variable

        $objData = $this->K_COMMON->getVarArray($arrayParam, "objData");
        $isModify = $this->K_COMMON->getVarArray($objData, "isModify");

        $obj = $this->getObject("tablelist");
        $obj->get($arrayParam);

        $this->appendObject("", $obj);
        if ($isModify == "1") {

            //include script
            $this->addHeadFile([$this->getPathFileJS("editor")]);

            //include object
            $objTag = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tag");
            $objTag->get(["tag" => "div", "id" => "hiddenContainerAddCol", "class" => "collapse", "content" => ""]);
            $objTag->get(["tag" => "div", "id" => "hiddenContainerEditCol", "class" => "collapse", "content" => ""]);
            $objTag->get(["tag" => "div", "id" => "hiddenContainerEditField", "class" => "collapse", "content" => ""]);
            $objTag->get(["tag" => "div", "id" => "hiddenContainerAddBtn", "class" => "collapse", "content" => ""]);
            $objTag->get(["tag" => "div", "id" => "hiddenContainerDeleteCol", "class" => "collapse", "content" => ""]);
            $objTag->get(["tag" => "div", "id" => "hiddenContainerMoveLCol", "class" => "collapse", "content" => ""]);
            $objTag->get(["tag" => "div", "id" => "hiddenContainerMoveRCol", "class" => "collapse", "content" => ""]);
            $this->appendObject("", $objTag);

            $this->appendInput("hiddenContainerAddCol",
                [
                    "type" => "btn"
                    , "id" => "btnAddCol"
                    , "class" => "btnTableListModify btnWebObject btnAddWebObjectOnStatic fa fa-plus"
                    , "semanticColor" => "warning"
                    , "value" => "Add Column"
                    , "title" => "Add Column"
                    , "size" => "sm"
                ]);
            $this->appendInput("hiddenContainerEditCol",
                [
                    "type" => "btn"
                    , "id" => "btnEditCol"
                    , "class" => "btnTableListModify btnWebObject btnAddWebObjectOnStatic fa fa-list-alt"
                    , "semanticColor" => "warning"
                    , "title" => "Set Label Column"
                    , "size" => "sm"
                ]);
            $this->appendInput("hiddenContainerEditField",
                [
                    "type" => "btn"
                    , "id" => "btnEditField"
                    , "class" => "btnTableListModify btnWebObject btnAddWebObjectOnStatic fa fa-database"
                    , "semanticColor" => "warning"
                    , "title" => "Set Field"
                    , "size" => "sm"
                ]);
            $this->appendInput("hiddenContainerAddBtn",
                [
                    "type" => "btn"
                    , "id" => "btnAddBtn"
                    , "class" => "btnTableListModify btnWebObject btnAddWebObjectOnStatic fa fa-hand-pointer-o"
                    , "semanticColor" => "secondary"
                    , "title" => "Add Button"
                    , "size" => "sm"
                ]);
            $this->appendInput("hiddenContainerDeleteCol",
                [
                    "type" => "btn"
                    , "id" => "btnDeleteCol"
                    , "class" => "btnTableListModify btnWebObject btnAddWebObjectOnStatic fa fa-remove"
                    , "semanticColor" => "danger"
                    , "title" => "Delete Column"
                    , "size" => "sm"
                ]);
            $this->appendInput("hiddenContainerMoveLCol",
                [
                    "type" => "btn"
                    , "id" => "btnMoveLCol"
                    , "class" => "btnTableListModify btnWebObject btnAddWebObjectOnStatic fa fa-arrow-left"
                    , "semanticColor" => "primary"
                    , "title" => "Move column"
                    , "size" => "sm"
                ]);
            $this->appendInput("hiddenContainerMoveRCol",
                [
                    "type" => "btn"
                    , "id" => "btnMoveRCol"
                    , "class" => "btnTableListModify btnWebObject btnAddWebObjectOnStatic fa fa-arrow-right"
                    , "semanticColor" => "primary"
                    , "title" => "Move column"
                    , "size" => "sm"
                ]);

        }

        return $this;
    }

    public function objectRequest($ArrayVariable)
    {
        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");
        //get template
        switch ($KCA) {
            case "getModalAddField":
                return json_encode($this->getModalAddField($ArrayVariable));
                break;
            case "deleteField":
                return json_encode($this->deleteField($ArrayVariable));
                break;
            case "saveDataField":
                return json_encode($this->saveDataField($ArrayVariable));
                break;
            case "getModalEditTable" :
                return json_encode($this->getModalEditTable($ArrayVariable));
                break;


        }
    }

    /** ****************************************************************************************** **/
    /** ******************************** EXTRA FUNCTIONS FOR PARAMETRIZATION ********************* **/
    /** ****************************************************************************************** **/

    protected function getModalEditTable($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");
        $tableID = $this->K_COMMON->getVarArray($ArrayVariable, "tableID");
        $refid = $this->K_COMMON->getVarArray($ArrayVariable, "refid");
        $type = $this->K_COMMON->getVarArray($ArrayVariable, "type");
        $position = $this->K_COMMON->getVarArray($ArrayVariable, "position");
        $value_label = "";
        $value_queryField = "";

        //include obj
        $kwebmanagerApi = $this->K_COMMON->getPluginObject("k_webmanager", "api");
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        //load value for edit
        $objUsedData = $kwebmanagerApi->getObjUsed(["keyID" => $refid, "OM" => "table"]);
        if ($data = $objUsedData->fetch()) {
            $parameter = json_decode($data->tpo_wo_parameters, true);
            $fields = $this->K_COMMON->getVarArray($parameter, "fields");
            if (is_array($fields)) {
                if (isset($fileds["field"])) // in old version was managed with field
                    $newFields = $fields["field"];
                else
                    $newFields = $fields;

                $field = $this->K_COMMON->getVarArray($newFields, $position);
                $value_label = $this->K_COMMON->getVarArray($field, "label");
                $value_queryField = $this->K_COMMON->getVarArray($field, "qryField");
                $value_buttons = $this->K_COMMON->getVarArray($field, "buttons");
            }
        }
        //create modal
        $webModal->newModal(["id" => $idModal, "title" => "", "type" => "normal"]);

        //create form
        $webModal->appendForm($webModal->modalBody,
            [
                "id" => "editTableForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveDataField"
                , "reloadTableListWithHead" => $tableID
                , "closeModal" => $idModal
            ]
        );

        //Hidden field
        $name = "refid";
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $refid]);

        $name = "type";
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $type]);

        $name = "position";
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $position]);

        if ($type == "label") {
            //label
            $webModal->appendRow($webModal->lastForm);
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
            $label = "Label";
            $name = "label";
            $webModal->appendInput($webModal->lastCol, ["type" => "text", "labelInput" =>
                $label, "id" => $name, "name" => $name, "value" => $value_label]);


        }
        if ($type == "queryField") {
            //query field
            $webModal->appendRow($webModal->lastForm);
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
            $label = "Query Field";
            $name = "queryField";
            $webModal->appendInput($webModal->lastCol, ["type" => "text", "labelInput" =>
                $label, "id" => $name, "name" => $name, "value" => $value_queryField]);

        }
        if ($type == "button") {

            //$value_buttons è valorizzato da qua prndere i valori per le variabili successive
            $value_titleBtn = "";
            $value_iconBtn = "";
            $value_actionBtn = "";
            $value_showIF = "";
            $value_qryAttributes = "";
            $value_size = "sm"; // sm per default da leggere nel bd il dato modificato
            $value_kcp = "";
            $value_kco = "";
            $value_kca = "";
            $value_triggerFunction = "";


            //create tab
            $tabsArray[] = ["id" => "tabGeneral", "label" => "General", "active" => "1"];
            $tabsArray[] = ["id" => "tabConditions", "label" => "Conditions"];
            $tabsArray[] = ["id" => "tabAttributes", "label" => "Attributes"];
            $tabsArray[] = ["id" => "tabAjax", "label" => "Ajax"];

            $webModal->appendTab($webModal->lastForm,
                [
                    "id" => "tabBtnCustomization"
                    , "tabs" => $tabsArray
                ]
            );


            //*******************************TAB GENERAL******************************************************/


            //title
            $webModal->appendRow("tabGeneral");
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
            $label = "Title";
            $name = "title";
            $webModal->appendInput($webModal->lastCol, ["type" => "text", "labelInput" =>
                $label, "id" => $name, "name" => $name, "value" => $value_titleBtn]);


            //action
            $webModal->appendRow("tabGeneral");
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
            $label = "Action";
            $name = "action";
            $actionOption = [
                "edit" => "edit"
                , "remove" => "remove"
                , "link" => "link"
                , "lock" => "lock"
                , "unlock" => "unlock"
                , "copy" => "copy"
                , "modal" => "modal"
                , "editModal" => "editModal"
            ];
            $webModal->appendInput($webModal->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $actionOption, "labelInput" =>
                $label, "id" => $name, "name" => $name, "value" => $value_actionBtn]);

            //icon
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
            $label = "Icon";
            $name = "icon";
            $webModal->appendInput($webModal->lastCol, ["type" => "text", "labelInput" =>
                $label, "id" => $name, "name" => $name, "value" => $value_iconBtn]);

            //size
            $webModal->appendRow("tabGeneral");
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
            $label = "Size";
            $name = "size";
            $sizeOption = [
                "" => "Normal"
                , "sm" => "Small"
            ];
            $webModal->appendInput($webModal->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $sizeOption, "noEmptyOption" => "1", "labelInput" =>
                $label, "id" => $name, "name" => $name, "value" => $value_size]);

            //size
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
            $label = "Sematic Color";
            $name = "semanticColor";
            $webModal->appendWO($webModal->lastCol, "k_usefulobjects", "web\objects\w_semanticcolors", ["labelInput" => $label, "name" => $name, "id" => $name, "value" => $value_size]);


            //*******************************TAB CONDITIONS******************************************************/


            //Heading show IF
            $webModal->appendRow("tabConditions");
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
            $webModal->appendWO($webModal->lastCol, "k_htmlcomponents", "web\objects\w_hr");
            $webModal->appendWO($webModal->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "Display Conditions", "importance" => "5"]);
            $webModal->appendHTML($webModal->lastCol, "<small>Add Field conditions to show the button</small>");

            //show IF
            $textHelpID = "Data Field To Check";
            $textHelpVal = "Value For Check";
            $name = "showIF";
            $value_showIFDecoded = json_decode($value_showIF, true);
            $webModal->appendRow("tabConditions");
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
            $webModal->appendListGroup($webModal->lastCol,
                [
                    "id" => $name
                    , "attributes" => 'style="height:176; overflow: scroll; overflow-x: hidden;"'
                    , "values" => $value_showIFDecoded
                    , "addingValues" =>
                    [
                        "valuesHName" => $name
                        , "associative" => "1"
                        , "fields" => [
                        ["type" => "text", "textHelp" => $textHelpID, "labelInput" => "Field", "name" => "field"]
                        , ["type" => "text", "textHelp" => $textHelpVal, "labelInput" => "Value", "name" => "value"]
                    ]
                    ]
                ]
            );

            //*******************************TAB ATTRIBUTES******************************************************/


            //Heading Query Attributes
            $webModal->appendRow("tabAttributes");
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
            $webModal->appendWO($webModal->lastCol, "k_htmlcomponents", "web\objects\w_hr");
            $webModal->appendWO($webModal->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "Query Attributes", "importance" => "5"]);
            $webModal->appendHTML($webModal->lastCol, "<small>Fields to be sent when the button is clicked</small>");

            //Query Attributes
            $textHelpID = "Attribute Name";
            $textHelpVal = "Data Field Name";
            $name = "qryAttributes";
            $value_qryAttributesDecoded = json_decode($value_qryAttributes, true);
            $webModal->appendRow("tabAttributes");
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
            $webModal->appendListGroup($webModal->lastCol,
                [
                    "id" => $name
                    , "attributes" => 'style="height:176; overflow: scroll; overflow-x: hidden;"'
                    , "values" => $value_qryAttributesDecoded
                    , "addingValues" =>
                    [
                        "valuesHName" => $name
                        , "associative" => "1"
                        , "fields" => [
                        ["type" => "text", "textHelp" => $textHelpID, "labelInput" => "Field", "name" => "field"]
                        , ["type" => "text", "textHelp" => $textHelpVal, "labelInput" => "Value", "name" => "value"]
                    ]
                    ]
                ]
            );

            //*******************************TAB AJAX******************************************************/

            //Heading show IF
            $webModal->appendRow("tabAjax");
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
            $webModal->appendWO($webModal->lastCol, "k_htmlcomponents", "web\objects\w_hr");
            $webModal->appendWO($webModal->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "Ajax Call", "importance" => "5"]);
            $webModal->appendHTML($webModal->lastCol, "<small>This parameters must be completed for these actions: Modal, Edit Modal, Remove, Lock and UnLock</small>");


            //KCP
            $label = "Plugin";
            $name = "kcp";
            $webModal->appendRow("tabAjax");
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
            $webModal->appendWO($webModal->lastCol, "k_usefulobjects", "web\objects\w_comboplugin", ["labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_kcp]);


            //KCO
            $label = "Object";
            $name = "kco";
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
            $webModal->appendWO($webModal->lastCol, "k_usefulobjects", "web\objects\w_combopluginobjects", ["labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_kco, "loadTriggerRef" => "#kcp"]);

            //KCA
            $label = "Action";
            $name = "kca";
            $webModal->appendRow("tabAjax");
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
            $webModal->appendInput($webModal->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_kca]);


            //KCA
            $label = "Trigger Function";
            $name = "triggerFunction";
            $textHelp = "Javascript function executed on Ajax success";
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
            $webModal->appendInput($webModal->lastCol, ["type" => "text", "textHelp" => "Javascript function executed on Ajax success", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_triggerFunction]);


        }
        if ($type == "delete") {
            $webModal->appendHTML($webModal->lastForm, "Save for delete this column");
        }
        if ($type == "moveR") {
            $webModal->appendHTML($webModal->lastForm, "Save for move right this column");
        } elseif ($type == "moveL") {
            $webModal->appendHTML($webModal->lastForm, "Save for move left this column");
        }


        //button Save
        $webModal->appendRow($webModal->lastForm);
        $label = "Save";
        $name = "btnSaveField";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 3]);
        $webModal->appendInput($webModal->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        //return
        $htmlResult = $webModal->getHTMLWithCSSJS();
        return ["success" => "true", "html" => $htmlResult];
    }

    protected function saveDataField($ArrayVariable)
    {


        //set variable
        $refid = $this->K_COMMON->getVarArray($ArrayVariable, "refid");
        $type = $this->K_COMMON->getVarArray($ArrayVariable, "type");
        $label = $this->K_COMMON->getVarArray($ArrayVariable, "label");
        $queryField = $this->K_COMMON->getVarArray($ArrayVariable, "queryField");
        $position = $this->K_COMMON->getVarArray($ArrayVariable, "position");

        $action = $this->K_COMMON->getVarArray($ArrayVariable, "action");
        $title = $this->K_COMMON->getVarArray($ArrayVariable, "title");
        $icon = $this->K_COMMON->getVarArray($ArrayVariable, "icon");
        $showIF = $this->K_COMMON->getVarArray($ArrayVariable, "showIF");
        $qryAttributes = $this->K_COMMON->getVarArray($ArrayVariable, "qryAttributes");
        $size = $this->K_COMMON->getVarArray($ArrayVariable, "size");
        $semanticColor = $this->K_COMMON->getVarArray($ArrayVariable, "semanticColor");

        $kcp = $this->K_COMMON->getVarArray($ArrayVariable, "kcp");
        $kco = $this->K_COMMON->getVarArray($ArrayVariable, "kco");
        $kca = $this->K_COMMON->getVarArray($ArrayVariable, "kca");
        $triggerFunction = $this->K_COMMON->getVarArray($ArrayVariable, "triggerFunction");

        $newFields = [];
        $parameter = [];

        //check variable
        if ($refid == "")
            return ["success" => "false"];

        //include obj
        $kwebmanagerApi = $this->K_COMMON->getPluginObject("k_webmanager", "api");

        //load parameter of obj Used and get ID
        $objUsedData = $kwebmanagerApi->getObjUsed(["keyID" => $refid, "OM" => "table"]);
        if ($data = $objUsedData->fetch()) {
            $parameter = json_decode($data->tpo_wo_parameters, true);
            $fields = $this->K_COMMON->getVarArray($parameter, "fields");
            if (is_array($fields)) {
                if (isset($fileds["field"])) // in old version was managed with field
                    $newFields = $fields["field"];
                else
                    $newFields = $fields;

            }
            if ($position == "")
                $position = count($newFields);

            if ($type == "label") $newFields[$position]["label"] = $label;
            if ($type == "queryField") $newFields[$position]["qryField"] = $queryField;
            if ($type == "delete") array_splice($newFields, $position, 1);
            if ($type == "moveR" && isset($newFields[$position + 1])) {
                $temp = $newFields[$position];
                $newFields[$position] = $newFields[$position + 1];
                $newFields[$position + 1] = $temp;
            }
            if ($type == "moveL" && isset($newFields[$position - 1])) {
                $temp = $newFields[$position];
                $newFields[$position] = $newFields[$position - 1];
                $newFields[$position - 1] = $temp;
            }
            if ($type == "button") {
                $newFields[$position]["buttons"][] = [
                    "title" => $title
                    , "action" => $action
                    , "icon" => $icon
                    , "showIF" => $showIF
                    , "qryAttributes" => $qryAttributes
                    , "size" => $size
                    , "semanticColor" => $semanticColor
                    , "ajax" => [
                        "kcp" => $kcp
                        , "kco" => $kco
                        , "kca" => $kca
                        , "triggerFunction" => $triggerFunction
                    ]
                ];

            }
            $parameter["fields"] = $newFields;
        }
        $parameter = json_encode($parameter);
        $table = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_templates_content");
        $table->put(["tpo_wo_parameters" => $parameter], ["id_web_template_content" => $refid]);

        return ["success" => "true"];
    }

    /** ****************************************************************************************** **/
    /** ******************************** PARAMETRIZATION FUNCTIONS    **************************** **/
    /** ****************************************************************************************** **/
    protected function appendEditGeneric($param)
    {
        $this->appendEditTableClass($param, true);
        $this->appendEditTriggerFunction($param, true);
        $this->appendEditData($param, false);


        $this->appendEditDictionary($param, true);


        //combo for load data
        $this->appendRow("");
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "Data to Load", "importance" => "5"]);
        $this->appendHTML($this->lastCol, "<small>" . "Select plugin data where to load the data. " . "</small>");
        $this->appendEditDataPluginName($param, true);
        $this->appendEditDataPluginData($param, false);
        $this->appendEditFieldsToGet($param, true);

        //ajax section
        $this->appendRow("");
        $this->appendColumn($this->lastRow);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
    }

    protected function appendEditTableClass($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Table Class";
        $name = "tableClass";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditTriggerFunction($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Trigger Function";
        $name = "triggerFunction";
        $textHelp = "Function called after loaded table";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "textHelp" => $textHelp, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditData($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Data";
        $name = "data";
        $textHelp = "Data array for table (results of get db data as array)";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "textarea", "labelInput" => $label, "textHelp" => $textHelp, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditDictionary($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Dictionary DA VEDERE COME CARICARLO"; // stavo pensando ad la possibilità di fare una combo con tutti i dizionari disponibili
        $name = "dictionary";
        $textHelp = "dictionary for label transaction";
        $options = [];
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "array", "typeLoad" => "array", "options" => $options, "labelInput" => $label, "textHelp" => $textHelp, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditDataPluginName($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Plugin";
        $name = "dataPluginName";
        $dataLoad = $this->K_COMMON->getVarArray($param, "dataLoad");
        $value = $this->K_COMMON->getVarArray($dataLoad, "pluginName");
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_comboplugin", ["labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditDataPluginData($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Plugin Data";
        $name = "dataPluginData";
        $dataLoad = $this->K_COMMON->getVarArray($param, "dataLoad");
        $value = $this->K_COMMON->getVarArray($dataLoad, "pluginData");

        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_comboplugindata"
            , [
                "labelInput" => $label,
                "id" => $name,
                "name" => $name,
                "value" => $value, "loadTriggerRef" => "#dataPluginName"
            ]
        );

    }

    protected function appendEditFieldsToGet($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Fields To Get";
        $textHelpID = "ID field to get from page when refresh table";
        $textHelpVal = "Parameter name for post";
        $name = "fieldsPageToGet";
        $value = json_decode($this->K_COMMON->getVarArray($param, "fieldsPageToGet"), true);

        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendListGroup($this->lastForm,
            [
                "id" => $name
                , "attributes" => 'style="height:176; overflow: scroll; overflow-x: hidden;"'
                , "values" => $value
                , "addingValues" =>
                [
                    "valuesHName" => $name
                    , "fields" => [
                    ["type" => "text", "textHelp" => $textHelpVal, "labelInput" => "Ajax Parameter", "name" => "parameter"]
                    , ["type" => "text", "textHelp" => $textHelpID, "labelInput" => "Html ID", "name" => "htmlid"]
                ]
                ]
            ]
        );

    }

    /**
     * get Parameterization Form
     *
     * @param array $objParameters array with variables
     *
     * @return webobject
     */
    public function parameterizationGetForm($objParametersSaved)
    {
        $arrayFormField = $objParametersSaved;

        $this->appendEditGeneric($arrayFormField);


        return $this;

    }

    /**
     * save Parameterization Save Data
     *
     * @param array $AV array with variables
     *
     * @return webobject
     */
    public function parameterizationSaveData($AV)
    {

        
        //include obj
        $kwebmanagerApi = $this->K_COMMON->getPluginObject("k_webmanager", "api");

        //set variables
        $parameter = [];
        $tableClass = $this->K_COMMON->getVarArray($AV, "tableClass");
        $triggerFunction = $this->K_COMMON->getVarArray($AV, "triggerFunction");
        $data = $this->K_COMMON->getVarArray($AV, "data");
        $dictionary = $this->K_COMMON->getVarArray($AV, "dictionary");

        $tplCNTID = $this->K_COMMON->getVarArray($AV, "tplCNTID");
        $dataPluginName = $this->K_COMMON->getVarArray($AV, "dataPluginName");
        $dataPluginData = $this->K_COMMON->getVarArray($AV, "dataPluginData");
        $fieldsPageToGet = $this->K_COMMON->getVarArray($AV, "fieldsPageToGet"); //ID field to get from page when refresh table

        //load fields for save
        $objUsedData = $kwebmanagerApi->getObjUsed(["keyID" => $tplCNTID, "OM" => "table"]);
        if ($dataTable = $objUsedData->fetch()) {
            $parameter = json_decode($dataTable->tpo_wo_parameters, true);
        }

        $parameter["tableClass"] = $tableClass;
        $parameter["triggerFunction"] = $triggerFunction;
        $parameter["data"] = $data;
        $parameter["dictionary"] = $dictionary;
        $parameter["dataLoad"] = ["pluginName" => $dataPluginName, "pluginData" => $dataPluginData];
        $parameter["fieldsPageToGet"] = $fieldsPageToGet;


        return $parameter;
    }


}

?>