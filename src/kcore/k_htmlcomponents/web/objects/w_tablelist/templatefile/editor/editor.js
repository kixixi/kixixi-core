// create namespace
KXJS.namespace("k_htmlcomponents.web.objects.w_tablelist.editor");

// create method of plugin
(function () {
    var t = this;
    this.init = function () {
        $(document).ready(t.ready);
    };


    t.setButtons = function () {

        var refid = $(t.currentTableList).attr("data-tplcntid");
        var tableID = $(t.currentTableList).attr("data-htmlid");

        $("#hiddenContainerAddCol button").attr("data-refid", refid);
        $("#hiddenContainerEditCol button").attr("data-refid", refid);
        $("#hiddenContainerEditField button").attr("data-refid", refid);
        $("#hiddenContainerAddBtn button").attr("data-refid", refid);
        $("#hiddenContainerDeleteCol button").attr("data-refid", refid);
        $("#hiddenContainerMoveLCol button").attr("data-refid", refid);
        $("#hiddenContainerMoveRCol button").attr("data-refid", refid);


        $("#hiddenContainerAddCol button").attr("data-tableID", tableID);
        $("#hiddenContainerEditCol button").attr("data-tableID", tableID);
        $("#hiddenContainerEditField button").attr("data-tableID", tableID);
        $("#hiddenContainerAddBtn button").attr("data-tableID", tableID);
        $("#hiddenContainerDeleteCol button").attr("data-tableID", tableID);
        $("#hiddenContainerMoveLCol button").attr("data-tableID", tableID);
        $("#hiddenContainerMoveRCol button").attr("data-tableID", tableID);


        var btnAddCol = $("#hiddenContainerAddCol").html();
        var btnEditCol = $("#hiddenContainerEditCol").html();
        var btnEditField = $("#hiddenContainerEditField").html();
        var btnAddButton = $("#hiddenContainerAddBtn").html();
        var btnDeleteCol = $("#hiddenContainerDeleteCol").html();
        var btnMoveLCol = $("#hiddenContainerMoveLCol").html();
        var btnMoveRCol = $("#hiddenContainerMoveRCol").html();

        var totalColumn = $(t.currentTableList).find('table th').length;
        var newID = KXJS.getNewDomId();
        var btnAddCol = btnAddCol.replace("btnAddCol", "btnAddCol_" + newID);
        var count = 0;


        //append btn and assign trigger
        if ($(t.currentTableList).find("table thead tr").length == 0)
            $(t.currentTableList).append("<div class='webEditorCustomization'>" + btnAddCol + "</div>");
        else
            $(t.currentTableList).find("table thead tr").append("<div class='webEditorCustomization'>" + btnAddCol + "</div>");

        $(t.currentTableList).find("#btnAddCol_" + newID).click(t.btnAddColClick);

        $(t.currentTableList).find('table th').each(function () {


            newID = KXJS.getNewDomId();
            var btnEditColTmp = btnEditCol.replace("btnEditCol", "btnEditCol_" + newID);
            var btnEditFieldTmp = btnEditField.replace("btnEditField", "btnEditField_" + newID);
            var btnAddButtonTmp = btnAddButton.replace("btnAddBtn", "btnAddBtn_" + newID);
            var btnDeleteColTmp = btnDeleteCol.replace("btnDeleteCol", "btnDeleteCol_" + newID);
            var btnMoveLColTmp = btnMoveLCol.replace("btnMoveLCol", "btnMoveLCol_" + newID);
            var btnMoveRColTmp = btnMoveRCol.replace("btnMoveRCol", "btnMoveRCol_" + newID);

            //$(this).append("<div class='webEditorCustomization'></div>");

            $(this).append(
                '<div class="btn-group">' +
                '<button type="button" class="btn btn-sm btn-secondary fa fa-gear dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
                '</button>' +
                '<div class="dropdown-menu gearButton" style="text-align: center"></div>' +
                '</div>'
            );

            $(this).find(".gearButton").append(btnEditColTmp);
            $(this).find(".gearButton").append(btnEditFieldTmp);
            $(this).find(".gearButton").append(btnAddButtonTmp);
            if (count > 0)
                $(this).find(".gearButton").append(btnMoveLColTmp);
            if (count < totalColumn-1)
                $(this).find(".gearButton").append(btnMoveRColTmp);
            $(this).find(".gearButton").append(btnDeleteColTmp);


            $(t.currentTableList).find("#btnEditCol_" + newID).click(t.btnEditColClick);
            $(t.currentTableList).find("#btnEditField_" + newID).click(t.btnEditFieldClick);
            $(t.currentTableList).find("#btnAddBtn_" + newID).click(t.btnAddBtnClick);
            $(t.currentTableList).find("#btnDeleteCol_" + newID).click(t.btnDeleteColTmp);
            $(t.currentTableList).find("#btnMoveLCol_" + newID).click(t.btnMoveLColTmp);
            $(t.currentTableList).find("#btnMoveRCol_" + newID).click(t.btnMoveRColTmp);

            //add numebr of element
            $(this).find(".btnTableListModify").attr("data-position", count);
            count++;
        });


    }
    t.btnAddColClick = function () {
        var idModal = "modalEditTable";
        var refid = $(this).attr("data-refid");
        var tableID = $(this).attr("data-tableID");
        var btnType = "label";
        t.openModalEdit(idModal, tableID, refid, btnType);
    }
    t.btnEditColClick = function () {
        var idModal = "modalEditTable";
        var refid = $(this).attr("data-refid");
        var tableID = $(this).attr("data-tableID");
        var btnType = "label";
        var position = $(this).attr("data-position");
        t.openModalEdit(idModal, tableID, refid, btnType, position);
    }
    t.btnEditFieldClick = function () {
        var idModal = "modalEditTable";
        var refid = $(this).attr("data-refid");
        var tableID = $(this).attr("data-tableID");
        var btnType = "queryField";
        var position = $(this).attr("data-position");
        t.openModalEdit(idModal, tableID, refid, btnType, position);
    }
    t.btnAddBtnClick = function () {
        var idModal = "modalEditTable";
        var refid = $(this).attr("data-refid");
        var tableID = $(this).attr("data-tableID");
        var btnType = "button";
        var position = $(this).attr("data-position");
        t.openModalEdit(idModal, tableID, refid, btnType, position);
    }
    t.btnDeleteColTmp = function () {
        var idModal = "modalEditTable";
        var refid = $(this).attr("data-refid");
        var tableID = $(this).attr("data-tableID");
        var btnType = "delete";
        var position = $(this).attr("data-position");
        t.openModalEdit(idModal, tableID, refid, btnType, position);
    }
    t.btnMoveLColTmp = function () {
        var idModal = "modalEditTable";
        var refid = $(this).attr("data-refid");
        var tableID = $(this).attr("data-tableID");
        var btnType = "moveL";
        var position = $(this).attr("data-position");
        t.openModalEdit(idModal, tableID, refid, btnType, position);
    }
    t.btnMoveRColTmp = function () {
        var idModal = "modalEditTable";
        var refid = $(this).attr("data-refid");
        var tableID = $(this).attr("data-tableID");
        var btnType = "moveR";
        var position = $(this).attr("data-position");
        t.openModalEdit(idModal, tableID, refid, btnType, position);
    }
    t.openModalEdit = function (idModal, tableID, refid, btnType, position) {
        dialogModal = waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'warning'});

        $.ajax({
            url: KXJS.buildAPICallUrl("KXCJS", "k_htmlcomponents", "web\\objects\\w_tablelist", "getModalEditTable"),
            data: {
                idModal: idModal,
                refid: refid,
                tableID: tableID,
                type: btnType,
                position: position
            }
        }).done(function (data) {
            r = $.parseJSON(data);
            waitingDialog.hide();
            if (r.success == "true") {
                $(r.html).appendTo('body');
                $('#' + idModal).modal("show");

                $('#' + idModal).on('hidden.bs.modal', function (e) {
                    $('#' + idModal).remove();
                    location.reload();
                });
            }
        });
    }
    t.currentTableList = "";
    t.ready = function () {
        $('.kwebobject[data-namespace="k_htmlcomponents_web_objects_w_tablelist"]').each(function () {
            t.currentTableList = this;
            t.setButtons();
        })
    }
    this.init();

}).apply(KXNS.k_htmlcomponents.web.objects.w_tablelist.editor);

