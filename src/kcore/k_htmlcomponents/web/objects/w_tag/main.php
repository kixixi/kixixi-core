<?
/**
 * KIXIXI HTML Components (tag)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage web\objects\w_tag
 * @since 1.00
 */
namespace k_htmlcomponents\web\objects\w_tag;

class obj extends \k_webmanager\objects\general\web_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";


    public function get($arrayParam = []){


        $obj = $this->getObject("tag");
        $obj->get($arrayParam);
        return $obj;
    }

    /**
     * get Parameterization Form
     *
     * @param array $objParameters array with variables
     *
     * @return webobject
     */
    public function parameterizationGetForm($objParametersSaved)
    {
        $arrayFormField = $objParametersSaved;

        //set variables

        $tag = $this->K_COMMON->getVarArray($arrayFormField, "tag");


        //Logo Link
        $label = "Tag HTML";
        $name = "tagCode";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $tag]);


        return $this;

    }

    /**
     * save Parameterization Save Data
     *
     * @param array $AV array with variables
     *
     * @return webobject
     */
    public function parameterizationSaveData($AV)
    {


        $tag = $this->K_COMMON->getVarArray($AV, "tagCode");
        if ($tag == "")
            $tag = "div";

        return
            [
                "tag" => $tag

            ];

    }



}

?>