<?
/**
 * KIXIXI HTML Components (hr)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage web\objects\w_hr
 * @since 1.00
 */
namespace k_htmlcomponents\web\objects\w_tab;

class obj extends \k_webmanager\objects\general\web_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";


    public function get($arrayParam = []){
        $obj = $this->getObject("tab");
        $obj->get($arrayParam);
        return $obj;
    }



}

?>