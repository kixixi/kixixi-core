<?
/**
 * KIXIXI HTML Components (form)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage web\objects\w_form
 * @since 1.00
 */
namespace k_htmlcomponents\web\objects\w_form;

class obj extends \k_webmanager\objects\general\web_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();

    }

    public function get($arrayParam = [])
    {
        $objInput = $this->getObject("form");
        $objInput->get($arrayParam);
        return $objInput;
    }

    /** ****************************************************************************************** **/
    /** ******************************** PARAMETRIZATION FUNCTIONS    **************************** **/
    /** ****************************************************************************************** **/

    protected function appendEditGeneric($param)
    {




        //create tab
        $tabsArray[] = ["id" => "tabObjForm", "label" => "Form", "active" => "1"];
        $tabsArray[] = ["id" => "tabObjSubmit", "label" => "Submit"];
        $tabsArray[] = ["id" => "tabObjOther", "label" => "Other"];
        $tabsArray[] = ["id" => "tabObjAjax", "label" => "Ajax"];

        $this->appendTab("",
            [
                "id" => "tabObjCustomization"
                , "tabs" => $tabsArray
            ]
        );

        //TAB FORM
        $this->appendRow("tabObjForm");
        $this->appendEditClass($param, false);
        $this->appendEditAction($param, false);
        $this->appendEditAttributes($param, "tabObjForm",12 );
        $this->appendEditHtml($param, "tabObjForm", 12);
        $this->appendEditInLine($param, "tabObjForm");
        $this->appendEditResetOnSuccess($param, false);
        $this->appendEditReloadPage($param, "tabObjForm");
        $this->appendEditShowFeedback($param, false);
        $this->appendEditShowSuccess($param, "tabObjForm");
        $this->appendEditShowErrors($param, false);

        //TAB SUBMIT
        $this->appendRow("tabObjSubmit");
        $this->appendEditFunctionPreSubmit($param, "tabObjSubmit", 12);
        $this->appendEditFunctionSubmit($param, "tabObjSubmit", 12);
        $this->appendEditFunctionPostSubmit($param, "tabObjSubmit", 12);

        //TAB OTHER
        $this->appendRow("tabObjOther");
        $this->appendEditCloseModal($param, "tabObjOther", 12);
        $this->appendEditReloadTableList($param, "tabObjOther");
        $this->appendEditReloadMethod($param, false);
        $this->appendEditReloadPageURL($param, "tabObjOther");
        $this->appendEditFormMethod($param, false);
        $this->appendEditReloadPageCheckCode($param, "tabObjOther");
        $this->appendEditReloadPageGetCode($param, false);
        $this->appendEditSetSavedMessage($param, "tabObjOther");
        $this->appendEditReloadPageActual($param, false);

        //TAB AJAX
        $this->appendRow("tabObjAjax");
        $this->appendEditKcp($param, "tabObjAjax", 12);
        $this->appendEditKco($param, "tabObjAjax", 12);
        $this->appendEditKca($param, "tabObjAjax", 12);

    }

    protected function appendEditClass($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Class";
        $name = "class";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditAction($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Action";
        $name = "action";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditInLine($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "In Line Form";
        $name = "inLine";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditAttributes($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Default Attributes";
        $name = "attributes";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditHtml($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Optional Html";
        $name = "html";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "textarea", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditFunctionPostSubmit($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "JS Function Post Submit";
        $name = "functionPostSubmit";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditFunctionPreSubmit($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "JS Function Pre Submit";
        $name = "functionPreSubmit";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditFunctionSubmit($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "JS Function Submit";
        $name = "functionSubmit";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditSetSavedMessage($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Set Saved Message";
        $name = "setSavedMessage";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditReloadPageActual($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Reload Page Actual";
        $name = "reloadPageActual";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditCloseModal($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Close Modal";
        $name = "closeModal";
        $placeholder = "Modal ID";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelPlaceholder" => $placeholder, "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditReloadTableList($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Reload Table List";
        $name = "reloadTableList";
        $placeholder = "Table List ID";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelPlaceholder" => $placeholder, "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditResetOnSuccess($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Reset Form On Success";
        $name = "resetOnSuccess";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditReloadPage($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Reload Page On Success";
        $name = "reloadPage";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditReloadMethod($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Reload Method";
        $name = "reloadMethod";
        $options = ["REST" => "REST", "GET" => "GET"];
        $value = $this->K_COMMON->getVarArray($param, $name);
        if ($value == "")
            $value = "REST";
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $options, "noEmptyOption" => "1", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditReloadPageURL($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "URL to load on success";
        $name = "reloadPageURL";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditReloadPageCheckCode($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Check Code";
        $textHelp = "Optional Code to check if we are in modify or not";
        $name = "reloadPageCheckCode";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "textHelp" => $textHelp, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditReloadPageGetCode($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Get Code";
        $textHelp = "Optional Code to get from answer for URL GET";
        $name = "reloadPageGetCode";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "textHelp" => $textHelp, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditFormMethod($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Form Method";
        $name = "formMethod";
        $options = ["post" => "POST", "get" => "GET"];
        $value = $this->K_COMMON->getVarArray($param, $name);
        if ($value == "")
            $value = "get";
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $options, "noEmptyOption" => "1", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditShowFeedback($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Show Feedback";
        $name = "showFeedback";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditShowSuccess($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Show Success";
        $name = "showSuccess";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditShowErrors($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Show Errors";
        $name = "showErrors";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditKcp($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Plugin";
        $name = "kcp";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_comboplugin", ["labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditKco($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Object";
        $name = "kco";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_combopluginobjects", ["labelInput" => $label, "id" => $name, "name" => $name, "value" => $value, "loadTriggerRef" => "#kcp"]);

    }

    protected function appendEditKca($param = [], $newRow = "", $nCol = 6)
    {

        if ($newRow !== false) {
            //NEW ROW
            $this->appendRow($newRow);
        }

        //labelInput
        $label = "Action";
        $name = "kca";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    /**
     * get Parameterization Form
     *
     * @param array $objParameters array with variables
     *
     * @return webobject
     */
    public function parameterizationGetForm($objParametersSaved)
    {
        $arrayFormField = $objParametersSaved;

        $this->appendEditGeneric($arrayFormField);

        return $this;

    }

    /**
     * save Parameterization Save Data
     *
     * @param array $AV array with variables
     *
     * @return webobject
     */
    public function parameterizationSaveData($AV)
    {
        //set variables
        $action = $this->K_COMMON->getVarArray($AV, "action");
        $class = $this->K_COMMON->getVarArray($AV, "class");
        $inLine = $this->K_COMMON->getVarArray($AV, "inLine");
        $attributes = $this->K_COMMON->getVarArray($AV, "attributes");
        $html = $this->K_COMMON->getVarArray($AV, "html");
        $functionPostSubmit = $this->K_COMMON->getVarArray($AV, "functionPostSubmit");
        $functionPreSubmit = $this->K_COMMON->getVarArray($AV, "functionPreSubmit");
        $functionSubmit = $this->K_COMMON->getVarArray($AV, "functionSubmit");
        $setSavedMessage = $this->K_COMMON->getVarArray($AV, "setSavedMessage");
        $reloadPageActual = $this->K_COMMON->getVarArray($AV, "reloadPageActual");
        $kcp = $this->K_COMMON->getVarArray($AV, "kcp");
        $kco = $this->K_COMMON->getVarArray($AV, "kco");
        $kca = $this->K_COMMON->getVarArray($AV, "kca");
        $closeModal = $this->K_COMMON->getVarArray($AV, "closeModal");
        $reloadTableList = $this->K_COMMON->getVarArray($AV, "reloadTableList");
        $resetOnSuccess = $this->K_COMMON->getVarArray($AV, "resetOnSuccess");
        $nextStep = $this->K_COMMON->getVarArray($AV, "nextStep");// preparare l'edit
        $reloadPage = $this->K_COMMON->getVarArray($AV, "reloadPage");
        $reloadMethod = $this->K_COMMON->getVarArray($AV, "reloadMethod");
        $reloadPageURL = $this->K_COMMON->getVarArray($AV, "reloadPageURL");
        $reloadPageCheckCode = $this->K_COMMON->getVarArray($AV, "reloadPageCheckCode");
        $reloadPageGetCode = $this->K_COMMON->getVarArray($AV, "reloadPageGetCode");
        $formMethod = $this->K_COMMON->getVarArray($AV, "formMethod");
        $showFeedback = $this->K_COMMON->getVarArray($AV, "showFeedback");
        $showSuccess = $this->K_COMMON->getVarArray($AV, "showSuccess");
        $showErrors = $this->K_COMMON->getVarArray($AV, "showErrors");

        return [
            "action" => $action
            , "class" => $class
            , "inLine" => $inLine
            , "attributes" => $attributes
            , "html" => $html
            , "functionPostSubmit" => $functionPostSubmit
            , "functionPreSubmit" => $functionPreSubmit
            , "functionSubmit" => $functionSubmit
            , "setSavedMessage" => $setSavedMessage
            , "reloadPageActual" => $reloadPageActual
            , "kcp" => $kcp
            , "kco" => $kco
            , "kca" => $kca
            , "closeModal" => $closeModal
            , "reloadTableList" => $reloadTableList
            , "resetOnSuccess" => $resetOnSuccess
            , "nextStep" => $nextStep
            , "reloadPage" => $reloadPage
            , "reloadMethod" => $reloadMethod
            , "reloadPageURL" => $reloadPageURL
            , "reloadPageCheckCode" => $reloadPageCheckCode
            , "reloadPageGetCode" => $reloadPageGetCode
            , "formMethod" => $formMethod
            , "showFeedback" => $showFeedback
            , "showSuccess" => $showSuccess
            , "showErrors" => $showErrors
        ];
    }

}

?>