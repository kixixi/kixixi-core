<?
/**
 * KIXIXI HTML Components (input)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage web\objects\w_buttons
 * @since 1.00
 */
namespace k_htmlcomponents\web\objects\w_buttons;

class obj extends \k_webmanager\objects\general\web_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();

    }

    public function get($arrayParam = [])
    {
        $objInput = $this->getObject("input");
        $objInput->get($arrayParam);
        return $objInput;
    }

    /** ****************************************************************************************** **/
    /** ******************************** PARAMETRIZATION FUNCTIONS    **************************** **/
    /** ****************************************************************************************** **/

    protected function appendEditGeneric($param, $link = true)
    {
        $this->appendEditName($param, true);
        $this->appendEditValue($param, false);
        $this->appendEditClass($param, true);
        $this->appendEditAttributes($param, true, 12);
        $this->appendEditSize($param, true);
        $this->appendEditSemanticColor($param, false);
        $this->appendEditOutlineColor($param, true, 4);
        $this->appendEditBlocked($param, false, 4);
        $this->appendEditTitle($param, true);
        $this->appendEditIcon($param, false);

        if ($link)
            $this->appendEditLink($param, true, 12);

        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");

        $this->appendEditOpenModal($param, true, 6);
        $this->appendEditKcp($param, true, 4);
        $this->appendEditKco($param, false, 4);
        $this->appendEditKca($param, false, 4);

    }

    protected function appendEditFileButton($param, $link = true)
    {
        $this->appendEditName($param, true);
        $this->appendEditValue($param, false);
        $this->appendEditClass($param, true);
        $this->appendEditAttributes($param, true, 12);
        $this->appendEditSize($param, true);
        $this->appendEditSemanticColor($param, false);
        $this->appendEditOutlineColor($param, true, 4);
        $this->appendEditBlocked($param, false, 4);
        $this->appendEditTitle($param, true);
        $this->appendEditIcon($param, false);

        $this->appendEditFunctionOnChange($param,true);
        $this->appendEditIFileName($param, false);

    }

    protected function appendEditName($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Name";
        $name = "name";
        $value_name = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_name]);

    }

    protected function appendEditClass($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Class";
        $name = "class";
        $value_class = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_class]);

    }

    protected function appendEditValue($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Text btn";
        $name = "value";
        $value_value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_value]);

    }

    protected function appendEditAttributes($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Default Attributes";
        $name = "attributes";
        $value_attributes = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_attributes]);

    }

    protected function appendEditSize($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Size";
        $name = "size";
        $value_size = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "id" => $name
                , "name" => $name
                , "value" => $value_size
                , "required" => "0"
                , "typeLoad" => "array"
                , "options" => ["sm" => "small"]
            ]
        );

    }

    protected function appendEditBlocked($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "With 100%";
        $name = "blocked";
        $value_blocked = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value_blocked]);

    }

    protected function appendEditSemanticColor($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Semantic Color";
        $name = "semanticColor";
        $value_semanticColor = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "id" => $name
                , "name" => $name
                , "value" => $value_semanticColor
                , "required" => "0"
                , "typeLoad" => "array"
                , "options" => ["primary" => "primary", "secondary" => "secondary", "success" => "success", "info" => "info", "warning" => "warning", "danger" => "danger", "link" => "link"]
            ]
        );

    }

    protected function appendEditOutlineColor($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Outline Color";
        $name = "outlineColor";
        $value_outlineColor = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value_outlineColor]);

    }

    protected function appendEditTitle($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Title";
        $name = "title";
        $value_text = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_text]);
    }

    protected function appendEditIcon($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Icon";
        $name = "icon";
        $value_icon = $this->K_COMMON->getVarArray($param, $name);
        $textHelp = $this->dictionary["LabelTextHelpIcon"];
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_icon, "textHelp" => $textHelp]);

    }

    protected function appendEditOpenModal($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Modal ID";
        $name = "openModal";
        $textHelp = $this->dictionary["LabelTextHelpOpenModal"];
        $value_openModal = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_openModal, "textHelp" => $textHelp]);

    }

    protected function appendEditKcp($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Plugin";
        $name = "kcp";
        $value_kcp = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_comboplugin", ["labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_kcp]);

    }

    protected function appendEditKco($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Object";
        $name = "kco";
        $value_kco = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_combopluginobjects", ["labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_kco, "loadTriggerRef" => "#kcp"]);

    }

    protected function appendEditKca($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Action";
        $name = "kca";
        $value_kca = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_kca]);

    }

    protected function appendEditFunctionOnChange($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Function On File Change";
        $name = "functionOnChange";
        $value_text = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_text]);
    }

    protected function appendEditIFileName($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Input File Name";
        $name = "iFileName";
        $value_text = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_text]);
    }

    protected function appendEditLink($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Link";
        $name = "link";
        $value_link = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_link]);

    }

    /**
     * get Parameterization Form
     *
     * @param array $objParameters array with variables
     *
     * @return webobject
     */
    public function parameterizationGetForm($objParametersSaved)
    {
        $arrayFormField = $objParametersSaved;

        //set variables
        $type = $this->K_COMMON->getVarArray($arrayFormField, "type");

        //append hidden field type
        $name = "type";
        $this->appendInput("", ["type" => "hidden", "id" => $name, "name" => $name, "value" => $type]);


        //get correct html parametrization for type
        switch ($type) {
            case "btn":
                $this->appendEditGeneric($arrayFormField, true);
                break;
            case "btnSubmit":
                $this->appendEditGeneric($arrayFormField, false);
                break;
            case "fileButton":
                $this->appendEditFileButton($arrayFormField);
                break;
        }

        return $this;

    }

    /**
     * save Parameterization Save Data
     *
     * @param array $AV array with variables
     *
     * @return webobject
     */
    public function parameterizationSaveData($AV)
    {
        //set variables
        $type = $this->K_COMMON->getVarArray($AV, "type");
        $name = $this->K_COMMON->getVarArray($AV, "name");
        $class = $this->K_COMMON->getVarArray($AV, "class");
        $value = $this->K_COMMON->getVarArray($AV, "value");
        $attributes = $this->K_COMMON->getVarArray($AV, "attributes");
        $size = $this->K_COMMON->getVarArray($AV, "size");

        $semanticColor = $this->K_COMMON->getVarArray($AV, "semanticColor");
        $title = $this->K_COMMON->getVarArray($AV, "title");
        $icon = $this->K_COMMON->getVarArray($AV, "icon");
        $blocked = $this->K_COMMON->getVarArray($AV, "blocked");
        $outlineColor = $this->K_COMMON->getVarArray($AV, "outlineColor");

        $openModal = $this->K_COMMON->getVarArray($AV, "openModal");
        $kcp = $this->K_COMMON->getVarArray($AV, "kcp");
        $kco = $this->K_COMMON->getVarArray($AV, "kco");
        $kca = $this->K_COMMON->getVarArray($AV, "kca");

        $link = $this->K_COMMON->getVarArray($AV, "link");
        $functionOnChange = $this->K_COMMON->getVarArray($AV, "functionOnChange");
        $iFileName = $this->K_COMMON->getVarArray($AV, "iFileName");

        return [
            "type" => $type
            , "name" => $name
            , "class" => $class
            , "value" => $value
            , "attributes" => $attributes
            , "size" => $size
            , "semanticColor" => $semanticColor
            , "title" => $title
            , "icon" => $icon
            , "blocked" => $blocked
            , "outlineColor" => $outlineColor
            , "openModal" => $openModal
            , "kcp" => $kcp
            , "kco" => $kco
            , "kca" => $kca

            , "link" => $link
            , "functionOnChange" => $functionOnChange
            , "iFileName" => $iFileName
        ];
    }

}

?>