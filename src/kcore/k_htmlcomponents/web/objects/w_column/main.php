<?
/**
 * KIXIXI HTML Components (col)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage web\objects\w_col
 * @since 1.00
 */
namespace k_htmlcomponents\web\objects\w_column;

class obj extends \k_webmanager\objects\general\web_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    var $standardFormat = ["xs","sm","md","lg","xl"];



    public function get($arrayParam = []){
        $obj = $this->getObject("column");
        $obj->get($arrayParam);
        return $obj;
    }

    /**
     * get Parameterization Form
     *
     * @param array $objParameters array with variables
     *
     * @return webobject
     */
    public function parameterizationGetForm($objParameters)
    {
        $arrayFormField = $objParameters;

        //set variables
        $nCol = (array)$this->K_COMMON->getVarArray($arrayFormField, "nCol");
        $offset = (array)$this->K_COMMON->getVarArray($arrayFormField, "offset");

        foreach($this->standardFormat as $format){
            //NEW ROW
            $this->appendRow("");

            //Column
            $label = "Column ".$format;
            $name = "nCol".$format;
            $value = $this->K_COMMON->getVarArray($nCol, $format);
            $this->appendColumn($this->lastRow, ["nCol" => 6]);
            $this->appendInput($this->lastCol, ["type" => "spinner", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

            //Offset
            $label = "Offset ".$format;
            $name = "offset".$format;
            $value = $this->K_COMMON->getVarArray($offset, $format);
            $this->appendColumn($this->lastRow, ["nCol" => 6]);
            $this->appendInput($this->lastCol, ["type" => "spinner", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);
        }

        return $this;

    }

    /**
     * save Parameterization Save Data
     *
     * @param array $AV array with variables
     *
     * @return webobject
     */
    public function parameterizationSaveData($AV)
    {
        $nColArray = [];
        $offsetArray = [];
        foreach($this->standardFormat as $format) {

            //set variables
            $nCol = $this->K_COMMON->getVarArray($AV, "nCol".$format);
            $offset = $this->K_COMMON->getVarArray($AV, "offset".$format);
            $nColArray[$format] = $nCol;
            $offsetArray[$format] = $offset;
        }
        return ["nCol"=>$nColArray,"offset"=>$offsetArray];
    }



}

?>