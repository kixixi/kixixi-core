<?
/**
 * KIXIXI HTML Components (input)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage web\objects\w_input
 * @since 1.00
 */
namespace k_htmlcomponents\web\objects\w_input;

class obj extends \k_webmanager\objects\general\web_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * Constructor
     *
     */
    public function __construct()
    {

        parent::__construct();
        $this->dictionary = $this->getDictionary();


    }


    public function get($arrayParam = [])
    {
        $objInput = $this->getObject("input");
        $objInput->get($arrayParam);
        return $objInput;
    }

    /** ****************************************************************************************** **/
    /** ******************************** PARAMETRIZATION FUNCTIONS    **************************** **/
    /** ****************************************************************************************** **/
    protected function appendEditGeneric($param)
    {
        $this->appendEditContainerClass($param, true);

        $this->appendEditName($param, false);
        $this->appendEditClass($param, true);
        $this->appendEditAttributes($param, false);
        $this->appendEditSize($param, true);
        $this->appendEditAddIcon($param, false);

        $this->appendEditLabelInput($param, true);
        $this->appendEditLabelPlaceholder($param, false);
        $this->appendEditTextHelp($param, true);
        $this->appendEditValue($param, false);

        $this->appendEditRequired($param, true, 3);
        $this->appendEditMultiple($param, false, 3);
        $this->appendEditNoContainer($param, true, 3);
        $this->appendEditNoFieldSet($param, false, 3);
        $this->appendEditNoLabel($param, false, 3);
    }

    protected function appendEditTextArea($param)
    {
        $this->appendEditContainerClass($param, true);
        $this->appendEditName($param, false);

        $this->appendEditClass($param, true);
        $this->appendEditSize($param, false);

        $this->appendEditLabelInput($param, true);
        $this->appendEditLabelPlaceholder($param, false);

        $this->appendEditTextHelp($param, true);
        $this->appendEditValue($param, false);

        $this->appendEditRequired($param, true, 3);
        $this->appendEditMultiple($param, false, 3);

        $this->appendEditNoContainer($param, true, 3);
        $this->appendEditNoFieldSet($param, false, 3);
        $this->appendEditNoLabel($param, false, 3);
    }

    protected function appendEditLabelInput($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Label";
        $name = "labelInput";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditName($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Name";
        $name = "name";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditLabelPlaceholder($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Placeholder";
        $name = "labelPlaceholder";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditClass($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Class";
        $name = "class";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditValue($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Default Value";
        $name = "valueFiled";
        $value = $this->K_COMMON->getVarArray($param, "value");
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditPrefixValue($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Default Prefix Code";
        $name = "prefixValue";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditAttributes($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Default Attributes";
        $name = "attributes";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditTextHelp($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Text Help";
        $name = "textHelp";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditNoLabel($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Hide Label";
        $name = "noLabel";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditContainerClass($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Class Container";
        $name = "containerClass";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditRequired($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Required";
        $name = "required";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditAddIcon($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Add Icon";
        $name = "addIcon";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    } // pensare se farla come combo

    protected function appendEditSize($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Size";
        $name = "size";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "id" => $name
                , "name" => $name
                , "value" => $value
                , "required" => "0"
                , "typeLoad" => "array"
                , "options" => ["sm" => "small"]
            ]
        );

    }

    protected function appendEditMultiple($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Multiple Field";
        $name = "multiple";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditNoFieldSet($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "No FieldSet";
        $name = "noFieldSet";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditNoContainer($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "No Container";
        $name = "noContainer";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditTextareaNCol($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Number Of Columns";
        $name = "ncol";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditTextareaNRow($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Number Of Rows";
        $name = "nrow";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }


    protected function appendEditGenericDataPicker($param)
    {

        $this->appendEditContainerClass($param, true);
        $this->appendEditName($param, false);

        $this->appendEditClass($param, true);
        $this->appendEditAttributes($param, false);


        $this->appendEditLabelInput($param, true);
        $this->appendEditLabelPlaceholder($param, false);
        $this->appendEditTextHelp($param, true);
        $this->appendEditValue($param, false);

        $this->appendEditRequired($param, true, 3);
        $this->appendEditMultiple($param, false, 3);
        $this->appendEditNoContainer($param, true, 3);
        $this->appendEditNoFieldSet($param, false, 3);
        $this->appendEditNoLabel($param, false, 3);

        $this->appendEditFormatData($param, true);
        $this->appendEditStartView($param, false);
        $this->appendEditMinView($param, true);
        $this->appendEditMaxView($param, false);

        $this->appendEditPickerPosition($param, true);


    }

    protected function appendEditGenericDataPickerMulti($param)
    {

        $this->appendEditContainerClass($param, true);
        $this->appendEditName($param, false);

        $this->appendEditClass($param, true);
        $this->appendEditAttributes($param, false);


        $this->appendEditLabelInput($param, true);
        $this->appendEditLabelPlaceholder($param, false);
        $this->appendEditTextHelp($param, true);
        $this->appendEditValue($param, false);

        $this->appendEditRequired($param, true, 3);
        $this->appendEditMultiple($param, false, 3);
        $this->appendEditNoContainer($param, true, 3);
        $this->appendEditNoFieldSet($param, false, 3);
        $this->appendEditNoLabel($param, false, 3);

        $this->appendEditFormatData($param, true);
        $this->appendEditStartView($param, false);
        $this->appendEditMinView($param, true);
        $this->appendEditMaxView($param, false);

        $this->appendEditOpened($param,true);
        $this->appendEditMultiDate($param,false);




    }

    protected function appendEditOpened($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Calendar Opened";
        $name = "opened";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditMultiDate($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Multi Date";
        $name = "multidate";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditMinView($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Min View";
        $name = "minView";
        $value = $this->K_COMMON->getVarArray($param, $name);
        if ($value == "")
            $value = "0";
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "id" => $name
                , "name" => $name
                , "value" => $value
                , "required" => "0"
                , "typeLoad" => "array"
                , "options" => ["0" => "hour", "1" => "day", "2" => "month", "3" => "year", "4" => "decade"]
            ]
        );

    }

    protected function appendEditMaxView($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Max View";
        $name = "maxView";
        $value = $this->K_COMMON->getVarArray($param, $name);
        if ($value == "")
            $value = "4";
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "id" => $name
                , "name" => $name
                , "value" => $value
                , "required" => "0"
                , "typeLoad" => "array"
                , "options" => ["0" => "hour", "1" => "day", "2" => "month", "3" => "year", "4" => "decade"]
            ]
        );

    }

    protected function appendEditStartView($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Start View";
        $name = "startView";
        $value = $this->K_COMMON->getVarArray($param, $name);
        if ($value == "")
            $value = "2";
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "id" => $name
                , "name" => $name
                , "value" => $value
                , "required" => "0"
                , "typeLoad" => "array"
                , "options" => ["0" => "hour", "1" => "day", "2" => "month", "3" => "year", "4" => "decade"]
            ]
        );

    }

    protected function appendEditFormatData($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Date Format";
        $name = "formatDate";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditPickerPosition($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Picker Position";
        $name = "pickerPosition";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "id" => $name
                , "name" => $name
                , "value" => $value
                , "required" => "0"
                , "typeLoad" => "array"
                , "options" => ["bottom-left" => "bottom-left", "top-left" => "top-left", "2" => "bottom-right", "bottom-right" => "top-right", "4" => "top-right"]
            ]
        );

    }

    protected function appendEditChecked($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Checked";
        $name = "checked";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditTextEditor($param)
    {
        $this->appendEditContainerClass($param, true);

        $this->appendEditName($param, false);
        $this->appendEditClass($param, true);
        $this->appendEditAttributes($param, false);
        $this->appendEditLabelInput($param, true);
        $this->appendEditTextHelp($param, true);
        $this->appendEditValue($param, false);

        $this->appendEditRequired($param, true, 3);
        $this->appendEditMultiple($param, false, 3);
        $this->appendEditNoFieldSet($param, false, 3);
        $this->appendEditNoLabel($param, false, 3);
    }

    protected function appendEditOptionsPhonePrefix($param = [])
    {

        //set variables
        $optionValues = $this->K_COMMON->getVarArray($param, "options");

        //Heading
        $this->appendRow("");
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->dictionary["labelHeadPhone"], "importance" => "5"]);
        $this->appendHTML($this->lastCol, "<small>".$this->dictionary["labelHeadSubPhone"]."</small>");
       
        //Fields for insert new value
        $this->appendRow("");

        //labelInput
        $name = "options";

        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendListGroup($this->lastCol,
            [
                "id" => $name
                , "attributes" => 'style="height:135px; overflow: scroll; overflow-x: hidden;"'
                , "values" => $optionValues
                , "addingValues" =>
                    [
                        "valuesHName" => "options"
                    ]
            ]
        );


    }

    protected function appendEditRadioOptions($param = [], $newRow = true)
    {
        $this->appendRow("");
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->dictionary["labelHeadRadioOption"], "importance" => "5"]);
        $this->appendHTML($this->lastCol, "<small>".$this->dictionary["labelHeadSubRadioOption"]."</small>");


        //set variables
        $optionValues = $this->K_COMMON->getVarArray($param, "options");


        //New ROW
        $this->appendRow("");


        //Fields for insert new value
        $this->appendRow("");

        //labelInput
        $name = "radioOptions";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendListGroup($this->lastCol,
            [
                "id" => $name
                , "attributes" => 'style="height:135px; overflow: scroll; overflow-x: hidden;"'
                , "values" => $optionValues
                , "addingValues" =>
                [
                    "valuesHName" => "options"
                ]
            ]
        );


    }

    /**
     * get Parameterization Form
     *
     * @param array $objParameters array with variables
     *
     * @return webobject
     */
    public function parameterizationGetForm($objParametersSaved)
    {
        $arrayFormField = $objParametersSaved;

        //set variables
        $type = $this->K_COMMON->getVarArray($arrayFormField, "type");

        //append hidden field type
        $name = "type";
        $this->appendInput("", ["type" => "hidden", "id" => $name, "name" => $name, "value" => $type]);


        //get correct html parametrization for type
        switch ($type) {
            case "text":
                $this->appendEditGeneric($arrayFormField);
                break;
            case "numb":
                $this->appendEditGeneric($arrayFormField);
                break;
            case "spinner":
                $this->appendEditGeneric($arrayFormField);
                break;
            case "password":
                $this->appendEditGeneric($arrayFormField);
                break;
            case "email":
                $this->appendEditGeneric($arrayFormField);
                break;
            case "serverFile":
                $this->appendEditGeneric($arrayFormField);
                break;
            case "file":
                $this->appendEditGeneric($arrayFormField);
                break;
            case "phone":
                $this->appendEditContainerClass($arrayFormField, true);

                $this->appendEditName($arrayFormField, false);
                $this->appendEditClass($arrayFormField, true);
                $this->appendEditAttributes($arrayFormField, false);
                $this->appendEditSize($arrayFormField, true);
                $this->appendEditAddIcon($arrayFormField, false);

                $this->appendEditLabelInput($arrayFormField, true);
                $this->appendEditLabelPlaceholder($arrayFormField, false);
                $this->appendEditTextHelp($arrayFormField, true);
                $this->appendEditPrefixValue($arrayFormField, false);

                $this->appendEditRequired($arrayFormField, true, 3);
                $this->appendEditMultiple($arrayFormField, false, 3);
                $this->appendEditNoContainer($arrayFormField, true, 3);
                $this->appendEditNoFieldSet($arrayFormField, false, 3);
                $this->appendEditNoLabel($arrayFormField, false, 3);
                $this->appendEditOptionsPhonePrefix($arrayFormField);
                break;
            case "checkbox" :
                $this->appendEditGeneric($arrayFormField);
                $this->appendEditChecked($arrayFormField, true);
                break;
            case "radio" :
                $this->appendEditGeneric($arrayFormField);
                $this->appendEditRadioOptions($arrayFormField);
                break;
            case "textarea":
                $this->appendEditTextArea($arrayFormField);
                $this->appendEditTextareaNCol($arrayFormField, true);
                $this->appendEditTextareaNRow($arrayFormField, true);


                break;
            case "colorPicker":
                $this->appendEditGeneric($arrayFormField);
                break;
            case "datePicker":
                $this->appendEditGenericDataPicker($arrayFormField);

                break;
            case "datePickerMulti":
                $this->appendEditGenericDataPickerMulti($arrayFormField);
                break;
            case "hidden":
                $this->appendEditName($arrayFormField, true);
                $this->appendEditValue($arrayFormField, false);
                break;
            case "label":
                $this->appendEditValue($arrayFormField, true);
                break;
            case "textEditor":
                $this->appendEditTextEditor($arrayFormField);
                break;

        }

        return $this;

    }

    /**
     * save Parameterization Save Data
     *
     * @param array $AV array with variables
     *
     * @return webobject
     */
    public function parameterizationSaveData($AV)
    {
        //set variables
        $type = $this->K_COMMON->getVarArray($AV, "type");
        $labelInput = $this->K_COMMON->getVarArray($AV, "labelInput");
        $name = $this->K_COMMON->getVarArray($AV, "name");
        $labelPlaceholder = $this->K_COMMON->getVarArray($AV, "labelPlaceholder");
        $class = $this->K_COMMON->getVarArray($AV, "class");
        $value = $this->K_COMMON->getVarArray($AV, "valueFiled");
        $prefixValue = $this->K_COMMON->getVarArray($AV, "prefixValue");
        $attributes = $this->K_COMMON->getVarArray($AV, "attributes");
        $textHelp = $this->K_COMMON->getVarArray($AV, "textHelp");
        $noLabel = $this->K_COMMON->getVarArray($AV, "noLabel");
        $containerClass = $this->K_COMMON->getVarArray($AV, "containerClass");
        $required = $this->K_COMMON->getVarArray($AV, "required");
        $addIcon = $this->K_COMMON->getVarArray($AV, "addIcon");
        $size = $this->K_COMMON->getVarArray($AV, "size");
        $multiple = $this->K_COMMON->getVarArray($AV, "multiple");
        $noFieldSet = $this->K_COMMON->getVarArray($AV, "noFieldSet");
        $noContainer = $this->K_COMMON->getVarArray($AV, "noContainer");
        $checked = $this->K_COMMON->getVarArray($AV, "checked");

        $multidate = $this->K_COMMON->getVarArray($AV, "multidate");
        $opened = $this->K_COMMON->getVarArray($AV, "opened");
        $minView = $this->K_COMMON->getVarArray($AV, "minView");
        $maxView = $this->K_COMMON->getVarArray($AV, "maxView");
        $startView = $this->K_COMMON->getVarArray($AV, "startView");
        $formatDate = $this->K_COMMON->getVarArray($AV, "formatDate");
        $pickerPosition = $this->K_COMMON->getVarArray($AV, "pickerPosition");
        $options = $this->K_COMMON->getVarArray($AV, "options");
        $ncol = $this->K_COMMON->getVarArray($AV, "ncol");
        $nrow = $this->K_COMMON->getVarArray($AV, "nrow");




        return [
            "type" => $type
            , "labelInput" => $labelInput
            , "name" => $name
            , "labelPlaceholder" => $labelPlaceholder
            , "class" => $class
            , "value" => $value
            , "prefixValue" => $prefixValue
            , "attributes" => $attributes
            , "textHelp" => $textHelp
            , "noLabel" => $noLabel
            , "containerClass" => $containerClass
            , "required" => $required
            , "addIcon" => $addIcon
            , "size" => $size
            , "multiple" => $multiple
            , "noFieldSet" => $noFieldSet
            , "noContainer" => $noContainer
            , "options" => $options
            , "opened" => $opened
            , "minView" => $minView
            , "maxView" => $maxView
            , "startView" => $startView
            , "formatDate" => $formatDate
            , "pickerPosition" => $pickerPosition
            , "checked" => $checked
            , "ncol" => $ncol
            , "nrow" => $nrow
            , "multidate" => $multidate
            ,

        ];
    }

}

?>