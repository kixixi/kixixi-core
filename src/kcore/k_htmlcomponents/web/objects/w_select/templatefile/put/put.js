// create namespace
KXJS.namespace("k_htmlcomponents.objects.w_select.put");

// create method of plugin
(function () {
    var t = this;
    t.urlBase = "";
    t.actualDir = "";

    t.init = function () {
        $(document).ready(t.ready);
    };

    //Options
    t.recalculateOptionValuesH = function () {

        var arrayOptions = $('#options button').map(function () {
            var optionCode = $(this).attr("optioncode");
            var optiontext = $(this).attr("optiontext");
            var o = new Object();
            o.value = optionCode;
            o.text =  optiontext;
            return JSON.stringify(o);

        }).get();

        $("#optionValuesH").val("[" + arrayOptions + "]");
    }
    t.optionMoveUp = function () {
        $(this).insertBefore($(this).prev());
        t.recalculateOptionValuesH();
    }
    t.optionRemove = function (e) {
        e.preventDefault();
        $(this).remove();
        t.recalculateOptionValuesH();

    }
    t.optionAdd = function () {
        var optionCode = $("#optionCode").val();
        var optiontext = $("#optionText").val();
        var textToAppend = '<button type="button" class="list-group-item list-group-item-action small" optionCode="' + optionCode + '" optiontext="' + optiontext + '">' + optionCode + " - " + optiontext + '</button>';
        $("#options").append(textToAppend);
        currentOption = $('#options button').last();
        currentOption.click(t.optionMoveUp);
        currentOption.on("contextmenu", t.optionRemove);

        t.recalculateOptionValuesH();

    }

    //ajax Parameter
    t.recalculateAjaxParValuesH = function () {

        var arrayOptions = $('#ajaxParameters button').map(function () {
            var ajaxParCode = $(this).attr("ajaxparcode");
            var ajaxPartext = $(this).attr("ajaxpartext");
            return '"'+ajaxParCode+'":"'+ajaxPartext+'"';

        }).get();

        $("#ajaxParametersH").val("{" + arrayOptions + "}");
    }
    t.ajaxParMoveUp = function () {
        $(this).insertBefore($(this).prev());
        t.recalculateAjaxParValuesH();
    }
    t.ajaxParRemove = function (e) {
        e.preventDefault();
        $(this).remove();
        t.recalculateAjaxParValuesH();

    }
    t.ajaxParAdd = function () {
        var ajaxParCode = $("#ajaxParCode").val();
        var ajaxPartext = $("#ajaxParText").val();
        var textToAppend = '<button type="button" class="list-group-item list-group-item-action small" ajaxParCode="' + ajaxParCode + '" ajaxPartext="' + ajaxPartext + '">' + ajaxParCode + " - " + ajaxPartext + '</button>';
        $("#ajaxParameters").append(textToAppend);
        currentAjaxPar = $('#ajaxParameters button').last();
        currentAjaxPar.click(t.ajaxParMoveUp);
        currentAjaxPar.on("contextmenu", t.ajaxParRemove);

        t.recalculateAjaxParValuesH();

    }


    t.ready = function () {
        if ($("#optionAdd").length > 0)
        {
            $("#optionAdd").click(t.optionAdd);
            $('#options button').click(t.optionMoveUp);
            $('#options button').on("contextmenu", t.optionRemove);
        }

        if ($("#ajaxParAdd").length > 0)
        {
            $("#ajaxParAdd").click(t.ajaxParAdd);
            $('#ajaxParameters button').click(t.ajaxParMoveUp);
            $('#ajaxParameters button').on("contextmenu", t.ajaxParRemove);
        }


    }

    t.init();

}).apply(KXNS.k_htmlcomponents.objects.w_select.put);

