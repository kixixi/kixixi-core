<?
/**
 * KIXIXI HTML Components (input)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage web\objects\w_select
 * @since 1.00
 */
namespace k_htmlcomponents\web\objects\w_select;

class obj extends \k_webmanager\objects\general\web_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * Constructor
     *
     */
    public function __construct()
    {

        parent::__construct();
        $this->dictionary = $this->getDictionary();


    }


    public function get($arrayParam = [])
    {
        $objInput = $this->getObject("input");
        $objInput->get($arrayParam);
        return $objInput;
    }

    /** ****************************************************************************************** **/
    /** ******************************** PARAMETRIZATION FUNCTIONS    **************************** **/
    /** ****************************************************************************************** **/

    protected function appendEditSelectAjax($param)
    {
        $this->appendRow("");
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->dictionary["labelHeadSelectAjax"], "importance" => "5"]);
        $this->appendHTML($this->lastCol, "<small>".$this->dictionary["labelHeadSubSelectAjax"]."</small>");


        $this->appendEditKcp($param, true, 6);
        $this->appendEditKco($param, false, 6);
        $this->appendEditKca($param, true, 12);

    }

    protected function appendEditGeneric($param)
    {

        $typeLoad = $this->K_COMMON->getVarArray($param, "typeLoad");
        switch ($typeLoad) {
            case "array":

                $this->appendRow("");
                $this->appendColumn($this->lastRow, ["nCol" => 12]);
                $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "Select by Array", "importance" => "5"]);

                break;
            case "function":

                $this->appendRow("");
                $this->appendColumn($this->lastRow, ["nCol" => 12]);
                $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "Select by Function", "importance" => "5"]);

                break;
            case "data":

                $this->appendRow("");
                $this->appendColumn($this->lastRow, ["nCol" => 12]);
                $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "Select by Data", "importance" => "5"]);
                
                break;
            case "ajax":

                $this->appendRow("");
                $this->appendColumn($this->lastRow, ["nCol" => 12]);
                $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "Select by Ajax", "importance" => "5"]);


                break;
        }


        $this->appendEditContainerClass($param, true);
        $this->appendEditClass($param, false);

        $this->appendEditLabelInput($param, true);
        $this->appendEditName($param, false);

        $this->appendEditTextHelp($param, true);
        $this->appendEditValue($param, false);


        $this->appendEditAttributes($param, true);
        $this->appendEditSize($param, false);


        $this->appendEditRequired($param, true, 3);
        $this->appendEditMultiple($param, false, 3);
        $this->appendEditNoFieldSet($param, false, 3);

        $this->appendEditNoLabel($param, true, 3);
        $this->appendEditDisabled($param, false, 3);


    }

    protected function appendEditSelect($param)
    {
        $this->appendEditContainerClass($param, true);

        $this->appendEditName($param, false);
        $this->appendEditClass($param, true);
        $this->appendEditAttributes($param, false);
        $this->appendEditSize($param, true);

        $this->appendEditLabelInput($param, true);
        $this->appendEditTextHelp($param, true);
        $this->appendEditValue($param, false);

        $this->appendEditRequired($param, true, 3);
        $this->appendEditMultiple($param, false, 3);
        $this->appendEditNoFieldSet($param, false, 3);
        $this->appendEditNoLabel($param, false, 3);

        $this->appendEditDisabled($param, true, 3);


    }

    protected function appendEditLabelInput($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Label";
        $name = "labelInput";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditName($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Name";
        $name = "name";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditClass($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Class Input";
        $name = "class";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditValue($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Default Value";
        $name = "valueField";
        $value = $this->K_COMMON->getVarArray($param, "value");
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditAttributes($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Default Attributes";
        $name = "attributes";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditTextHelp($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Text Help";
        $name = "textHelp";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditNoLabel($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Hide Label";
        $name = "noLabel";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditContainerClass($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Class Container";
        $name = "containerClass";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditRequired($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Required";
        $name = "required";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditSize($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Size";
        $name = "size";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "id" => $name
                , "name" => $name
                , "value" => $value
                , "required" => "0"
                , "typeLoad" => "array"
                , "options" => ["sm" => "small"]
            ]
        );

    }

    protected function appendEditMultiple($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Multiple Field";
        $name = "multiple";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditNoFieldSet($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "No FieldSet";
        $name = "noFieldSet";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditNoContainer($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "No Container";
        $name = "noContainer";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditDisabled($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Disabled";
        $name = "disabled";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditKcp($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Plugin";
        $name = "kcp";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendWO($this->lastCol,"k_usefulobjects","web\objects\w_comboplugin",[ "labelInput" => $label, "id" => $name, "name" => $name, "value"=>$value]);

    }

    protected function appendEditKco($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Plugin Object";
        $name = "kco";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendWO($this->lastCol,"k_usefulobjects","web\objects\w_combopluginobjects",[ "labelInput" => $label, "id" => $name, "name" => $name, "value"=>$value,"loadTriggerRef"=>"#kcp"]);

    }

    protected function appendEditKca($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Plugin Action";
        $name = "kca";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditLoadSelect($param = [], $newRow = true, $nCol = 6)
    {
        $typeLoad = $this->K_COMMON->getVarArray($param, "typeLoad");

        $this->appendEditTypeLoad($param, false);

        switch ($typeLoad) {
            case "array":

                $this->appendRow("");
                $this->appendColumn($this->lastRow, ["nCol" => 12]);
                $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
                $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->dictionary["labelHeadOption"], "importance" => "5"]);
                $this->appendHTML($this->lastCol, "<small>".$this->dictionary["labelHeadSubOption"]."</small>");

                $this->appendEditOptions($param, true, 12);


                break;
            case "function":

                $this->appendRow("");
                $this->appendColumn($this->lastRow, ["nCol" => 12]);
                $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
                $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->dictionary["labelHeadFunctionType"], "importance" => "5"]);
                $this->appendHTML($this->lastCol, "<small>".$this->dictionary["labelHeadSubFunctionType"]."</small>");

                $this->appendEditPluginName($param, true);
                $this->appendEditPluginObject($param, false);
                $this->appendEditFunctionName($param, true);


                break;
            case "data":

                $this->appendRow("");
                $this->appendColumn($this->lastRow, ["nCol" => 12]);
                $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
                $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->dictionary["labelHeadDataType"], "importance" => "5"]);
                $this->appendHTML($this->lastCol, "<small>".$this->dictionary["labelHeadSubDataType"]."</small>");
                
                $this->appendEditPluginName($param, true);
                $this->appendEditPluginData($param, false);
                $this->appendEditDataOrder($param, true);
                $this->appendEditDataPrototype($param, false);

                break;
            case "ajax":

                $this->appendRow("");
                $this->appendColumn($this->lastRow, ["nCol" => 12]);
                $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
                $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->dictionary["labelHeadAjaxType"], "importance" => "5"]);
                $this->appendHTML($this->lastCol, "<small>".$this->dictionary["labelHeadSubAjaxType"]."</small>");

                $this->appendEditPluginName($param, true);
                $this->appendEditPluginObject($param, false);
                $this->appendEditPluginAction($param, true);
                $this->appendEditValueSelected($param, false);

                $this->appendEditarameterRef($param, true);
                $this->appendEditFirstValueCall($param, false);

                $this->appendRow("");
                $this->appendColumn($this->lastRow, ["nCol" => 12]);
                $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->dictionary["labelHeadAjaxParam"], "importance" => "5"]);
                $this->appendHTML($this->lastCol, "<small>".$this->dictionary["labelHeadSubAjaxParam"]."</small>");


                $this->appendEditAjaxParameters($param, true, 12);

                $this->appendEditLoadTrigger($param, true);
                $this->appendEditLoadTriggerRef($param, false);




                break;
        }


        $this->appendRow("");
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->dictionary["labelHeadEvents"], "importance" => "5"]);

        $this->appendEditTriggerEvent($param, true);
        $this->appendEditTriggerFunction($param, false);

        $this->appendEditNoEmptyOption($param, true);
        $this->appendEditAutoSelect($param, false);


    }

    protected function appendEditPluginName($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Plugin";
        $name = "pluginName";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendWO($this->lastCol,"k_usefulobjects","web\objects\w_comboplugin",[ "labelInput" => $label, "id" => $name, "name" => $name, "value"=>$value]);

    }

    protected function appendEditPluginObject($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Plugin Object";
        $name = "pluginObject";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendWO($this->lastCol,"k_usefulobjects","web\objects\w_combopluginobjects",[ "labelInput" => $label, "id" => $name, "name" => $name, "value"=>$value,"loadTriggerRef"=>"#pluginName"]);

    }

    protected function appendEditPluginData($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Plugin Data";
        $name = "pluginData";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendWO($this->lastCol,"k_usefulobjects","web\objects\w_comboplugindata",[ "labelInput" => $label, "id" => $name, "name" => $name, "value"=>$value,"loadTriggerRef"=>"#pluginName"]);

    }

    protected function appendEditFunctionName($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Function";
        $name = "functionName";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditFunctionVariables($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Function";
        $name = "functionVariables";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditOptions($param = [], $newRow = true)
    {

        //set variables
        $optionValues = $this->K_COMMON->getVarArray($param, "options");


        //New ROW
        $this->appendRow("");

        //Column Code
        $label = "Code";
        $name = "optionCode";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => ""]);

        //Column Value
        $label = "Text";
        $name = "optionText";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => ""]);

        //Column Button
        $name = "optionAdd";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btn", "value" => '<i class="fa fa-plus"></i>', "label" => "", "id" => $name, "name" => $name, "semanticColor" => "primary", "size" => "sm", "emptyLabel" => "1"]);

        //Hidden field
        $name = "optionValuesH";
        $this->appendInput($this->lastCol, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $optionValues]);


        //Fields for insert new value
        $this->appendRow("");

        //labelInput
        $name = "options";
        $options = json_decode($optionValues, true);
        if (!empty($optionValues))
            $options = json_decode($optionValues, true);
        else
            $options = [];
        $itemsField = [];
        foreach ($options as $item) {
            $itemsField[] = ["type" => "button", "class" => "small", "value" => $item["value"] . " - " . $item["text"], "attributes" => 'optioncode="' . $item["value"] . '" optiontext="' . $item["text"] . '"'];
        }
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendListGroup($this->lastCol,
            [
                "id" => $name
                , "attributes" => 'style="height:135px; overflow: scroll; overflow-x: hidden;"'
                , "items" => $itemsField
            ]
        );


    }

    protected function appendEditAjaxParameters($param = [], $newRow = true)
    {

        //set variables
        $optionValues = $this->K_COMMON->getVarArray($param, "ajaxParameters");


        //New ROW
        $this->appendRow("");

        //Column Code
        $label = "Code";
        $name = "ajaxParCode";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => ""]);

        //Column Value
        $label = "Text";
        $name = "ajaxParText";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => ""]);

        //Column Button
        $name = "ajaxParAdd";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btn", "value" => '<i class="fa fa-plus"></i>', "label" => "", "id" => $name, "name" => $name, "semanticColor" => "primary", "size" => "sm", "emptyLabel" => "1"]);

        //Hidden field
        $name = "ajaxParametersH";
        $this->appendInput($this->lastCol, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $optionValues]);


        //Fields for insert new value
        $this->appendRow("");

        //labelInput
        $name = "ajaxParameters";
        if (!empty($optionValues))
            $options = json_decode($optionValues, true);
        else
            $options = [];
        $itemsField = [];
        foreach ($options as $key => $value) {
            $itemsField[] = ["type" => "button", "class" => "small", "value" => $key . " - " . $value, "attributes" => 'ajaxparcode="' . $key . '" ajaxpartext="' . $value . '"'];
        }
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendListGroup($this->lastCol,
            [
                "id" => $name
                , "attributes" => 'style="height:135px; overflow: scroll; overflow-x: hidden;"'
                , "items" => $itemsField
            ]
        );


    }

    protected function appendEditTypeLoad($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $name = "typeLoad";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditFirstValueCall($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "First Parameter Value";
        $name = "firstValueCall";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditValueSelected($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Value";
        $name = "valueField";
        $value = $this->K_COMMON->getVarArray($param, "value");
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditAutoSelect($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Auto Select if only one element";
        $name = "autoSelect";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditTriggerEvent($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Trigger For Event";
        $name = "triggerEvent";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "id" => $name
                , "name" => $name
                , "value" => $value
                , "required" => "0"
                , "typeLoad" => "array"
                , "options" => ["change" => "change", "click" => "click"]
            ]
        );
    }

    protected function appendEditTriggerFunction($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Event Function";
        $name = "triggerFunction";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditNoEmptyOption($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Not show first \"empty value\"";
        $name = "noEmptyOption";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditDataOrder($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Order Data";
        $name = "dataOrder";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditDataPrototype($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Data Prototype Name";
        $name = "dataPrototype";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditPluginAction($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Plugin Action";
        $name = "pluginAction";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditarameterRef($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Parameter referer";
        $name = "parameterRef";
        $textHelp = $this->dictionary["labelTHAjaxParRef"];
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value, "textHelp"=>$textHelp]);

    }

    protected function appendEditLoadTrigger($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW 
            $this->appendRow("");
        }

        //labelInput
        $label = "Trigger For load";
        $name = "loadTrigger";
        $textHelp = $this->dictionary["labelTHAjaxTriggerLoad"];

        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "id" => $name
                , "name" => $name
                , "value" => $value
                , "required" => "0"
                , "typeLoad" => "array"
                , "options" => ["change" => "change", "click" => "click"]
                , "textHelp" => $textHelp
            ]
        );
    }

    protected function appendEditLoadTriggerRef($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "ID of Trigger";
        $name = "loadTriggerRef";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    /**
     * get Parameterization Form
     *
     * @param array $objParameters array with variables
     *
     * @return webobject
     */
    public function parameterizationGetForm($objParametersSaved)
    {

        //add files to include
        $this->addHeadFile([$this->getPathFileJS("put")]);



        $arrayFormField = $objParametersSaved;

        //set variables
        $type = $this->K_COMMON->getVarArray($arrayFormField, "type");

        //append hidden field type
        $name = "type";
        $this->appendInput("", ["type" => "hidden", "id" => $name, "name" => $name, "value" => $type]);


        //get correct html parametrization for type
        switch ($type) {
            case "select":
                $this->appendEditGeneric($arrayFormField);
                $this->appendEditLoadSelect($arrayFormField);
                break;
            case "selectAjax":
                $this->appendEditGeneric($arrayFormField);
                $this->appendEditSelectAjax($arrayFormField, true);
                break;
        }

        return $this;

    }

    /**
     * save Parameterization Save Data
     *
     * @param array $AV array with variables
     *
     * @return webobject
     */
    public function parameterizationSaveData($AV)
    {
        //set variables
        $type = $this->K_COMMON->getVarArray($AV, "type");
        $labelInput = $this->K_COMMON->getVarArray($AV, "labelInput");
        $name = $this->K_COMMON->getVarArray($AV, "name");
        $class = $this->K_COMMON->getVarArray($AV, "class");
        $value = $this->K_COMMON->getVarArray($AV, "valueField");
        $attributes = $this->K_COMMON->getVarArray($AV, "attributes");
        $textHelp = $this->K_COMMON->getVarArray($AV, "textHelp");
        $noLabel = $this->K_COMMON->getVarArray($AV, "noLabel");
        $containerClass = $this->K_COMMON->getVarArray($AV, "containerClass");
        $required = $this->K_COMMON->getVarArray($AV, "required");
        $size = $this->K_COMMON->getVarArray($AV, "size");
        $multiple = $this->K_COMMON->getVarArray($AV, "multiple");
        $noFieldSet = $this->K_COMMON->getVarArray($AV, "noFieldSet");
        $noContainer = $this->K_COMMON->getVarArray($AV, "noContainer");

        // for select
        $disabled = $this->K_COMMON->getVarArray($AV, "disabled");
        $kcp = $this->K_COMMON->getVarArray($AV, "kcp");
        $kco = $this->K_COMMON->getVarArray($AV, "kco");
        $kca = $this->K_COMMON->getVarArray($AV, "kca");
        $pluginName = $this->K_COMMON->getVarArray($AV, "pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($AV, "pluginObject");
        $functionName = $this->K_COMMON->getVarArray($AV, "functionName");
        $functionVariables = $this->K_COMMON->getVarArray($AV, "functionVariables");
        $typeLoad = $this->K_COMMON->getVarArray($AV, "typeLoad");
        $options = $this->K_COMMON->getVarArray($AV, "optionValuesH");
        $firstValueCall = $this->K_COMMON->getVarArray($AV, "firstValueCall");
        $autoSelect = $this->K_COMMON->getVarArray($AV, "autoSelect");
        $triggerEvent = $this->K_COMMON->getVarArray($AV, "triggerEvent");
        $triggerFunction = $this->K_COMMON->getVarArray($AV, "triggerFunction");
        $noEmptyOption = $this->K_COMMON->getVarArray($AV, "noEmptyOption");
        $pluginData = $this->K_COMMON->getVarArray($AV, "pluginData");
        $dataOrder = $this->K_COMMON->getVarArray($AV, "dataOrder");
        $dataPrototype = $this->K_COMMON->getVarArray($AV, "dataPrototype");
        $pluginAction = $this->K_COMMON->getVarArray($AV, "pluginAction");
        $ajaxParameters = $this->K_COMMON->getVarArray($AV, "ajaxParametersH");
        $parameterRef = $this->K_COMMON->getVarArray($AV, "parameterRef");
        $loadTrigger = $this->K_COMMON->getVarArray($AV, "loadTrigger");
        $loadTriggerRef = $this->K_COMMON->getVarArray($AV, "loadTriggerRef");

        return [
            "type" => $type
            , "labelInput" => $labelInput
            , "name" => $name
            , "class" => $class
            , "value" => $value
            , "attributes" => $attributes
            , "textHelp" => $textHelp
            , "noLabel" => $noLabel
            , "containerClass" => $containerClass
            , "required" => $required
            , "size" => $size
            , "multiple" => $multiple
            , "noFieldSet" => $noFieldSet
            , "noContainer" => $noContainer
            // for select
            , "typeLoad" => $typeLoad
            , "options" => $options
            , "disabled" => $disabled
            , "kcp" => $kcp
            , "kco" => $kco
            , "kca" => $kca

            , "pluginName" => $pluginName
            , "pluginObject" => $pluginObject
            , "functionName" => $functionName
            , "functionVariables" => $functionVariables
            , "firstValueCall" => $firstValueCall
            , "autoSelect" => $autoSelect
            , "triggerEvent" => $triggerEvent
            , "triggerFunction" => $triggerFunction
            , "noEmptyOption" => $noEmptyOption
            , "pluginData" => $pluginData
            , "dataOrder" => $dataOrder
            , "dataPrototype" => $dataPrototype
            , "pluginAction" => $pluginAction
            , "ajaxParameters" => $ajaxParameters
            , "parameterRef" => $parameterRef
            , "loadTrigger" => $loadTrigger
            , "loadTriggerRef" => $loadTriggerRef

        ];
    }

}

?>