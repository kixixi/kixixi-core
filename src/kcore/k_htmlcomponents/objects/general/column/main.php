<?
/**
 * KIXIXI HTML Components (column)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage objects\column
 * @since 1.00
 */
namespace k_htmlcomponents\objects\general\column;

class obj extends \k_webmanager\objects\general\general_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * Get HTML of a column well formatted, using bootstrap .
     *
     * @param array $arrayVariable array of parameters
     * The following parameters are accepted:<br>
     *	*	id (optional): the id of object
     *	*	class (optional): class of the object
     *	*	attributes (optional): additional attributes
     *	*	html (optional): content of the column
     *	*	wo (optional): web object to insert in column
     *	*	nCol (optional): n° columns on "md" (put array for more options)
     *	*	offset (optional): offset on "md" (put array for more options)
     *	*	align (optional): "center", "left", "right"
     *
     * @return array with html.
     */
    public function get($arrayVariable = array())
    {

        //set variables
        $id = $this->K_COMMON->getVarArray($arrayVariable, "id");
        $class = $this->K_COMMON->getVarArray($arrayVariable, "class");
        $style = $this->K_COMMON->getVarArray($arrayVariable, "style");
        $attributes = $this->K_COMMON->getVarArray($arrayVariable, "attributes");
        $html = $this->K_COMMON->getVarArray($arrayVariable, "html");
        $wo = $this->K_COMMON->getVarArray($arrayVariable, "wo");
        $nCol = $this->K_COMMON->getVarArray($arrayVariable, "nCol"); //n° columns
        $offset = $this->K_COMMON->getVarArray($arrayVariable, "offset"); //offset
        $align = $this->K_COMMON->getVarArray($arrayVariable, "align");

        
        //check variables
        if($id == ""){
            $id = "column_".uniqid();
        }

        //N Columns
        if (is_array($nCol))
        {
            foreach ($nCol as $format => $qty)
            {
                if ($qty != "")
                {
                    $class .= " col-" . $format . "-".$qty;
                    if ($align != "")
                        $class .= " text-" . $format . "-" . $align;
                }
            }
        }
        else
        {
            $class .= ($nCol != "" ? " col-md-" . $nCol : "");
            if ($align != "")
                $class .= " text-md-" . $align;
        }

        //Offset
        if (is_array($offset))
        {
            foreach ($offset as $format => $qty)
            {
                if ($qty != "")
                    $class .= " offset-" . $format . "-".$qty;
            }
        }
        else
            $class .= ($offset != "" ? " offset-md-" . $offset : "");

        //style
        if ($style != "")
            $attributes .= 'style = "'.$style.'"';

        // create html string
        $this->appendHTML("", '<div id="' . $id . '" class="column ' . $class . '"' . $attributes . '>'
                   . $html
                   . '</div>');

        //set default HTML ID
        $this->setIDObject($id);
        $this->setIDContent($id);

        //Append web object
        $this->appendObject($id,$wo);

        //return
        return $this;
    }


   
}

?>

