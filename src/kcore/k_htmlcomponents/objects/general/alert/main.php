<?
/**
 * KIXIXI HTML Components (alert)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage objects\alert
 * @since 1.00
 */
namespace k_htmlcomponents\objects\general\alert;

class obj extends \k_webmanager\objects\general\general_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";


    /**
     * get an HTML Alert
     *
     * @since 1.00
     * @access public
     *
     * @param array $arrayVariable
     * The following directives can be used in the array variables:<br>
     *    *    id: id of html object
     *    *    message: message to write
     *    *    semanticColor: optional set color of button by bootstrap semantic purpose (primary, secondary, success, info, warning, danger, link)
     *
     * @return object
     */
    public function get($arrayVariable = [])
    {


        //set variables
        $id = $this->K_COMMON->getVarArray($arrayVariable, "id");
        $message = $this->K_COMMON->getVarArray($arrayVariable, "message");
        $semanticColor = $this->K_COMMON->getVarArray($arrayVariable, "semanticColor");

        //check variables
        if ($id == "")
            $id = "alert_" . uniqid();


        // create html string
        $this->appendHTML("", '<div id="'.$id.'" class="alert alert-'.$semanticColor.'" role="alert">'.$message.'</div>');


        // return
        return $this;
    }

}

?>

