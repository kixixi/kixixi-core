<?
/**
 * KIXIXI HTML Components (trigger manager)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage objects\triggers
 * @since 1.00
 */
namespace k_htmlcomponents\objects\general\triggers;

class obj extends \k_webmanager\objects\general\general_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";


    /**
     * get an HR
     *
     * @since 1.00
     * @access public
     *
     * @return webobject
     */
    public function get($arrayParam = [])
    {


        $this->addTriggers($arrayParam);


        // return
        return $this;
    }

    /**
     * add triggers
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    triggers: array of trigger
     *    *    id (optional): if specified will be used for all triggers
     *
     * * Example: $OBJTrigger->addTriggers(
     *  [
     *    [   "id"=>"obj1"
     *      , "event" => "click"
     *      , "function" => "addObject"
     *    ]
     *   ,[   "id"=>"obj2"
     *      , "event" => "click"
     *      , "function" => "deleteObject"
     *    ]
     *  [
     *
     * );
     *
     *
     *
     * @return object this object
     */
    public function addTriggers($arrayVariable = array())
    {

        $triggers = $this->K_COMMON->getVarArray($arrayVariable, "triggers");
        $commonID = $this->K_COMMON->getVarArray($arrayVariable, "id");

        foreach ($triggers as $trigger) {

            $id = $this->K_COMMON->getVarArray($trigger, "id");
            $event = $this->K_COMMON->getVarArray($trigger, "event");
            $function = $this->K_COMMON->getVarArray($trigger, "function");

            if ($id == "")
                $id = $commonID;

            $this->addFooterJS('<script>'
                . '$(document).ready(function(){
								$("#' . $id . '").on("'.$event.'", function (e) {
									if (e.isDefaultPrevented()) {
										} else {
											e.preventDefault();
											' . $function . '();
										}
									})
							});
						</script>');
        }


        return $this;
    }

}

?>

