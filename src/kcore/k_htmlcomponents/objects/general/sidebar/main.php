<?
namespace k_htmlcomponents\objects\general\sidebar;
class obj extends \k_common\objects\general\base\obj{
	var $ver = "1.0";
	var $namePlugin = "k_htmlcomponents";
	var $nameObject = "sidebar";
	var $dictionary;
	var $sideBar = false;
	var $template;
	
	public function __construct($AV) {
		parent::__construct();

		$this->template = $this->getTemplateLayout("sidebar");

		//get Table Classes
		$class = $this->K_COMMON->getVarArray($AV,"class");
		$id = $this->K_COMMON->getVarArray($AV,"id");
		if($id != "")
			$this->addSideBar (array("id"=>$id,"class"=>$class));
		
	}

	public function addSideBar ($ArrayVariable) {
		$class = $this->K_COMMON->getVarArray($ArrayVariable,"class");
		$id = $this->K_COMMON->getVarArray($ArrayVariable,"id");
		if (!$this->sideBar) {
			$this->template->assign_block_vars("SIDEBAR",array("id"=>$id,"class"=>$class));
			$this->sideBar = true;
		}		
	}
	public function addItem($arrayVariable){


		//setVariable
		$title = $this->K_COMMON->getVarArray($arrayVariable,"title");
		$contentID = $this->K_COMMON->getVarArray($arrayVariable,"contentID");
		$contentClass = $this->K_COMMON->getVarArray($arrayVariable,"contentClass");
		$content = $this->K_COMMON->getVarArray($arrayVariable,"content");
		$arraySet = array();
		$arraySet["title"] = $title;
		$arraySet["contentID"] = $contentID;
		$arraySet["contentClass"] = $contentClass;
		$arraySet["content"] = $content;
		$this->template->assign_block_vars("ITEM",$arraySet);
			
	}	
	public function parse() {
		return $this->template->pparse("body");
	}
}
?>

