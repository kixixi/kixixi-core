<!-- BEGIN SIDEBAR -->
<div class="sidebar-nav {SIDEBAR.class}" id = "{SIDEBAR.id}">
<!-- END SIDEBAR -->
   <!-- BEGIN ITEM -->
	<ul class="nav nav-list ">
        <li class="nav-header">
            {ITEM.title}
        </li>
        <li id="{ITEM.contentID}" class="{ITEM.contentClass}">
			{ITEM.content}
        </li>
    </ul>   
      
   <!-- END ITEM -->
<!-- BEGIN SIDEBAR -->
</div>
<!-- END SIDEBAR -->


					
		