<?
/**
 * KIXIXI HTML Components (breadcrumbs)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage objects\breadcrumbs
 * @since 1.00
 */
namespace k_htmlcomponents\objects\general\breadcrumbs;

class obj extends \k_webmanager\objects\general\general_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";


    /**
     * get an HR
     *
     * @since 1.00
     * @access public
     *
     * @param array $arrayParam array of parameters
     * The following parameters are accepted:<br>
     *    *    id (optional): the id of object
     *    *    crumbs : array of crumbs (["label" => "link"]) - Ex: ["Home" => "/", "Products" => ""]
     *
     *
     * @return webobject
     */
    public function get($arrayParam = [])
    {

        //set variables
        $id = $this->K_COMMON->getVarArray($arrayParam, "id");
        $crumbs = $this->K_COMMON->getVarArray($arrayParam, "crumbs");

        //check variables
        if ($id == "")
            $id = "breadcrumbs_" . uniqid();
        if (empty($crumbs))
            $crumbs = [];

        $html = '<ol id="' . $id . '" class="breadcrumb">';

        $nCrumbs = count($crumbs);
        $i = 1;
        foreach ($crumbs as $label => $link)
        {
            if ($i == $nCrumbs)
                $html .= '<li class="breadcrumb-item active">' .  $label . '</li>';
            else
                $html .= '<li class="breadcrumb-item">' . '<a href="'.($link == "" ? "#" : $link).'">' . $label . '</a></li>';
            $i++;
        }

        $html .= '</ol>';

        // create html string
        $this->appendHTML("", $html);


        // return
        return $this;
    }

}

?>

