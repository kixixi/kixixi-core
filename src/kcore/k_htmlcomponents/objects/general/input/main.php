<?
/**
 * KIXIXI HTML Components (input)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage objects\input
 * @since 1.00
 */
namespace k_htmlcomponents\objects\general\input;

class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * get an input (web object)
     * This function is used from template manager,
     * otherwise you can use single function for get html input.
     *
     * @param array $arrayParam array for input paraeters.<br>
     * Please refer to each input for correct parameters.<br>
     * If you use on template, you can pass the parameters on "lns_form_variable" variable.
     *
     *  Mandatory Parameters: <br>
     *
     * *    type    : the type of field
     *   *    text
     *   *    numb
     *   *    password
     *   *    email
     *   *    serverFile
     *   *    file
     *   *    fileButton
     *   *    select
     *   *    selectAjax
     *   *    phone
     *   *    checkbox
     *   *    radio
     *   *    textarea
     *   *    colorPicker
     *   *    datePicker
     *   *    datePickerMulti
     *   *    hidden
     *   *    label
     *   *    btn
     *   *    btnSubmit
     *   *    textEditor
     *   *    usefulobject
     *
     * @return this object
     */
    public function get($arrayParam = [])
    {
        //set variable
        $type = $this->K_COMMON->getVarArray($arrayParam, "type");
        $ID = $this->K_COMMON->getVarArray($arrayParam, "id");
        $triggers = $this->K_COMMON->getVarArray($arrayParam, "triggers");

        // check ID
        if ($ID == "")
            $arrayParam["id"] = $type . "_" . uniqid();

        //get html of input requested
        switch ($type) {
            case "text":
            case "":
                $this->appendInputText("", $arrayParam);
                break;
            case "numb":
                $this->appendInputNumber("", $arrayParam);
                break;
            case "spinner":
                $this->appendInputSpinner("", $arrayParam);
                break;
            case "password":
                $this->appendInputPassword("", $arrayParam);
                break;
            case "email":
                $this->appendInputEmail("", $arrayParam);
                break;
            case "serverFile":
                $this->appendInputServerFile("", $arrayParam);
                break;
            case "file":
                $this->appendInputFile("", $arrayParam);
                break;
            case "fileButton":
                $this->appendInputFileButton("", $arrayParam);
                break;
            case "select":
                $this->appendInputSelect("", $arrayParam);
                break;
            case "selectAjax":
                $this->appendInputSelectAjax("", $arrayParam);
                break;
            case "phone":
                $this->appendInputPhone("", $arrayParam);
                break;
            case "checkbox" :
                $this->appendInputCheckBox("", $arrayParam);
                break;
            case "radio" :
                $this->appendInputRadio("", $arrayParam);
                break;
            case "textarea":
                $this->appendInputTextArea("", $arrayParam);
                break;
            case "colorPicker":
                $this->appendColorPicker("", $arrayParam);
                break;
            case "datePicker":
                $this->appendDatePicker("", $arrayParam);
                break;
            case "datePickerMulti":
                $this->appendDatePickerMulti("", $arrayParam);
                break;
            case "hidden":
                $this->appendInputHidden("", $arrayParam);
                break;
            case "label":
                $this->appendLabel("", $arrayParam);
                break;
            case "btn":
                $this->appendBtn("", $arrayParam);
                break;
            case "btnSubmit":
                $this->appendBtnSubmit("", $arrayParam);
                break;
            case "textEditor":
                $this->appendTextEditor("", $arrayParam);
                break;
            case "combodictionary":
                $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_combodictionary", $arrayParam);
                break;

        }

        //check if there are triggers
        if (is_array($triggers))
        {

            $this->appendWO("", "k_htmlcomponents", "web\objects\w_triggers", ["id" => $arrayParam["id"],"triggers" => $triggers]);
        }


        //return
        return $this;
    }

    /**
     * append input text
     *
     */
    protected function appendInputText($idToAppend, $arrayParam = array())
    {

        $arrayParam["type"] = "text";
        $this->appendFieldContainer($idToAppend, $arrayParam);
        $this->appendGeneralInput($this->lastObject, $arrayParam);

    }

    /**
     * append input text that accept number
     *
     *
     */
    protected function appendInputNumber($idToAppend, $arrayParam = array())
    {

        $arrayParam["type"] = "text";
        $arrayParam["attributes"] = ' pattern="^\d*\.?\d*$"';
        $this->appendFieldContainer($idToAppend, $arrayParam);
        $this->appendGeneralInput($this->lastObject, $arrayParam);

    }

    /**
     * append input text that accept number
     *
     *
     */
    protected function appendInputSpinner($idToAppend, $arrayParam = array())
    {
        $arrayParam["type"] = "number";
        $this->appendFieldContainer($idToAppend, $arrayParam);
        $this->appendGeneralInput($this->lastObject, $arrayParam);

    }

    /**
     * append input password
     *
     *
     */
    protected function appendInputPassword($idToAppend, $arrayParam = array())
    {
        $arrayParam["type"] = "password";
        $this->appendFieldContainer($idToAppend, $arrayParam);
        $this->appendGeneralInput($this->lastObject, $arrayParam);
    }

    /**
     * append input email
     *
     * @param array $arrayParam array of input parameters.<br>
     * see appendGeneralInput() for supported parameters
     *
     * @see ::appendGeneralInput()
     *
     */
    protected function appendInputEmail($idToAppend, $arrayParam = array())
    {
        $arrayParam["type"] = "email";
        $this->appendFieldContainer($idToAppend, $arrayParam);
        $this->appendGeneralInput($this->lastObject, $arrayParam);
    }

    /**
     * append input hidden
     *
     * @param array $arrayParam array of input parameters.<br>
     * see appendGeneralInput() for supported parameters
     *
     * @see ::appendGeneralInput()
     *
     */
    protected function appendInputHidden($idToAppend, $arrayParam = array())
    {
        $arrayParam["type"] = "hidden";
        $this->appendGeneralInput($idToAppend, $arrayParam);
    }

    /**
     * append Text Editor
     *
     * @param array $arrayParam array of input parameters.<br>
     *  Parameters: <br>
     *
     * *    webCode    : web code for text editor images
     *
     * @see ::appendGeneralInput() for basic parameters
     * @see ::getFieldContainer object for extra parameters
     *
     */
    protected function appendTextEditor($idToAppend, $arrayParam = array())
    {
        //set variables
        $id = $this->K_COMMON->getVarArray($arrayParam, "id");
        $folderToGet = $this->K_COMMON->getVarArray($arrayParam, "folderToGet");
        $getFolderByPlugin = $this->K_COMMON->getVarArray($arrayParam, "getFolderByPlugin");
        $webCode = $this->K_COMMON->getVarArray($arrayParam, "webCode");
        if ($getFolderByPlugin != "")
            $folderToGet = "/" . $this->K_COMMON->getPluginType($getFolderByPlugin) . "/" . $getFolderByPlugin ;

        //$this->appendFieldContainer($idToAppend, $arrayParam);
        $this->appendInputTextArea($idToAppend, $arrayParam);

        //Add JS
        $this->addExternalPlugin(["tinymce4"]);
        $jsUniqueID = "kx_input_text_editor_" . uniqid();
        $this->addFooterJS('<script>
			' . $jsUniqueID . ' = {};
			(function() {
				var t = this;
				this.init = function(){
					$(document).ready(t.ready);
				};
				this.ready = function(){
                    var  edSetting = jQuery.extend(true, {}, KXNS.tinyMCE.settingFull);
                    edSetting.height = 400;
                    edSetting.selector = "#' . $id . '";
                    
                    edSetting.file_browser_callback = function(field_name, url, type, win) {
                        var webCode = "' . $webCode . '";
                        if (url === "")
                            url = "'.$folderToGet.'";
                        if (webCode!="")
                            KXJS.showFileManager(webCode,field_name,url,type,1);
                    };
                    edSetting.setup = function(ed) {
                        ed.on("change", function(e) {
                            $("#' . $id . '").html(tinyMCE.get("' . $id . '").getContent());
                        });
}
                    tinymce.init(edSetting);
                    
				}

				this.init();

			}).apply(' . $jsUniqueID . ');

		</script>');

    }

    /**
     * append input with icon "folder" for select file from server side
     *
     * @param array $arrayParam array of input parameters.<br>
     *  Parameters: <br>
     *
     * *    webCode    : Static web code
     * *    webCodeID    : ID of html object to get web code
     * *    folderToGet: folder to open
     * *    getFolderByPlugin; plugin Name to get Folder
     *
     * see ::appendGeneralInput() for supported parameters
     *
     * @see ::appendGeneralInput() for basic parameters
     * @see ::getFieldContainer object for extra parameters
     *
     */
    protected function appendInputServerFile($idToAppend, $arrayParam = array())
    {

        //set variables
        $addIcon = $this->K_COMMON->getVarArray($arrayParam, "addIcon");
        $folderToGet = $this->K_COMMON->getVarArray($arrayParam, "folderToGet");
        $getFolderByPlugin = $this->K_COMMON->getVarArray($arrayParam, "getFolderByPlugin");
        $id = $this->K_COMMON->getVarArray($arrayParam, "id");
        $webCode = $this->K_COMMON->getVarArray($arrayParam, "webCode");
        $webCodeID = $this->K_COMMON->getVarArray($arrayParam, "webCodeID");
        if ($getFolderByPlugin != "")
            $folderToGet = "/" . $this->K_COMMON->getPluginType($getFolderByPlugin) . "/" . $getFolderByPlugin ;

        //check variable
        if ($addIcon == "")
            $arrayParam["addIcon"] = "fa-folder-open";

        $arrayParam["type"] = "text";
        $this->appendFieldContainer($idToAppend, $arrayParam);
        $this->appendGeneralInput($this->lastObject, $arrayParam);

        //Add JS
        $jsUniqueID = "kx_input_file_server_btn_" . uniqid();
        $this->addFooterJS('<script>
			' . $jsUniqueID . ' = {};
			(function() {
				var t = this;
				this.init = function(){
					$(document).ready(t.ready);
				};
				this.ready = function(){
                    $("#addonGroup_' . $id . ' i").click(function(){
                        var webCode = ' . ($webCodeID != "" ? '$("#'.$webCodeID.'").val()' : '"' . $webCode . '"') . ';
                        KXJS.showFileManager(webCode,"' . $id . '","'.$folderToGet.'","image",0);
                    });
				}

				this.init();

			}).apply(' . $jsUniqueID . ');

		</script>');

    }

    /**
     * append input file
     *
     * @param array $arrayParam array of input parameters.<br>
     * see ::appendGeneralInput() for supported parameters
     *
     * @see ::appendGeneralInput() for basic parameters
     * @see ::getFieldContainer object for extra parameters
     *
     */
    protected function appendInputFile($idToAppend, $arrayParam = array())
    {
        $arrayParam["type"] = "file";
        $this->appendFieldContainer($idToAppend, $arrayParam);
        $this->appendGeneralInput($this->lastObject, $arrayParam);
    }

    /**
     * append input file hidden in button with js
     *
     * @param array $arrayParam array of button parameters.<br>
     *
     * @see ::getButton() for basic parameters
     * @see ::getFieldContainer object for extra parameters
     *
     */
    protected function appendInputFileButton($idToAppend, $arrayParam = array())
    {

        //set variables
        $ID = $this->K_COMMON->getVarArray($arrayParam, "id");
        $functionOnChange = $this->K_COMMON->getVarArray($arrayParam, "functionOnChange");
        $iFileName = $this->K_COMMON->getVarArray($arrayParam, "iFileName");


        //append button
        $arrayParam["link"] = "#";
        $arrayParam["style"] = "position:relative;";
        $this->appendBtn($idToAppend, $arrayParam);

        //create JS
        $jsUniqueID = "kx_input_file_btn_" . uniqid();
        $this->addFooterJS('<script>
			' . $jsUniqueID . ' = {};
			(function() {
				var t = this;
				this.init = function(){
					$(document).ready(t.ready);
				};
				this.ready = function(){
                    KXJS.Form.createSingleButtonFileForm({
                        "appendTo": "#' . $ID . '",
                        ' . ($functionOnChange != "" ? ' "onFileChange": ' . $functionOnChange . ', ' : '') . '
                        "iFileName": "' . $iFileName . '"
                    });
				}

				this.init();

			}).apply(' . $jsUniqueID . ');

		</script>');
    }

    /**
     * append select Ajax
     *
     * @param array $arrayParam array of input parameters.<br>
     *
     *  Parameters: <br>
     *
     * *    name    : name of input
     * *    id    : optional id of input
     * *    value    : optional value of the input
     * *    class    : optional class of the input
     * *    attributes    : optional attributes of the input
     * *    multiple    : set the name of field as array.
     * *    required    : set input as required
     * *    size    : "sm" for small
     * *    kcp    : required plugin name (for ajax call)
     * *    kco    : required plugin objecet name (for ajax call)
     * *    kca    : required plugin action code (for ajax call)
     * *    disabled: optional 1 or 0
     * *    parameters: Parameters to send on ajax call
     *
     * @see ::getFieldContainer object for extra parameters
     *
     */
    protected function appendInputSelectAjax($idToAppend, $arrayParam = array())
    {
        //get variables
        $name = $this->K_COMMON->getVarArray($arrayParam, "name");
        $ID = $this->K_COMMON->getVarArray($arrayParam, "id");
        $value = $this->K_COMMON->getVarArray($arrayParam, "value");
        $class = $this->K_COMMON->getVarArray($arrayParam, "class");
        $attributes = $this->K_COMMON->getVarArray($arrayParam, "attributes");
        $multiple = $this->K_COMMON->getVarArray($arrayParam, "multiple");
        $required = $this->K_COMMON->getVarArray($arrayParam, "required");
        $size = $this->K_COMMON->getVarArray($arrayParam, "size");
        $kcp = $this->K_COMMON->getVarArray($arrayParam, "kcp");
        $kco = $this->K_COMMON->getVarArray($arrayParam, "kco");
        $kca = $this->K_COMMON->getVarArray($arrayParam, "kca");
        $disabled = $this->K_COMMON->getVarArray($arrayParam, "disabled");
        $parametersToSend = $this->K_COMMON->getVarArray($arrayParam, "parameters");
        $parametersToSendString  = "";


        //replace "\" on kco with double "\" for javascript
        $kco = str_replace("\\", "\\\\", $kco);

        //Include Files for Head
        $this->addExternalPlugin("comboAjax");

        // if is set "multiple" add array at input name
        if ($multiple == "1") {
            $ID .= "[]";
            $arrayParam["id"] = $ID;
            if ($name != "") {
                $name .= "[]";
                $arrayParam["name"] = $name;
            }

        }

        // check size
        if ($size == "sm")
            $class .= " form-control-sm";

        // check if required
        if ($required == "1")
            $attributes .= ' required="" ';

        //check if disabled
        if ($disabled == "1")
            $attributes .= ' disabled="disabled" ';


        // create final html of select
        $objHTML =
            '<input type="text" id="' . $ID . '" name="' . $name . '" class="form-control ' . $class . '"'
            . ' value="' . $value . '"' . $attributes . ' />';

        //Search for extra parameters to send
        if (is_array($parametersToSend)) {
            foreach ($parametersToSend as $key => $val) {
                $parametersToSendString .= "&$key=$val";

            }

            $parametersToSendString = substr($parametersToSendString, 1);
        }

        // set javascript
        $this->addFooterJS('<script>
				$(document).ready(function(){
					$("' . "#" . $ID . '").ajaxComboBox(
                      KXJS.buildAPICallUrl("KXCJS","' . $kcp . '","' . $kco . '","' . $kca . '","'.$parametersToSendString.'"),
                      {
                        lang: "en"
                        ' . ($value != "" ? ",init_record:'" . $value . "'" : "") . '
                        ' . ($disabled == "1" ? ",plugin_type: 'simple'" : "") . '
                      }
                    );
				});
			</script>');

        // append object of select, with container
        $this->appendFieldContainer($idToAppend, $arrayParam);
        $this->appendObject($this->lastObject, $objHTML);
    }

    /**
     * append select
     *
     * @param array $arrayParam array of input parameters.<br>
     *
     *  Parameters: <br>
     *
     * *    name    : name of input
     * *    id    : optional id of input
     * *    value    : optional value of the input
     * *    class    : optional class of the input
     * *    attributes    : optional attributes of the input
     * *    multiple    : set the name of field as array.
     * *    required    : set input as required
     * *    size    : "sm" for small
     * *    disabled: optional 1 or 0
     *
     * @see ::getFieldContainer object for extra parameters
     *
     */
    protected function appendInputSelect($idToAppend, $arrayParam = array())
    {
        //get variables
        $name = $this->K_COMMON->getVarArray($arrayParam, "name");
        $ID = $this->K_COMMON->getVarArray($arrayParam, "id");
        $value = $this->K_COMMON->getVarArray($arrayParam, "value");
        $class = $this->K_COMMON->getVarArray($arrayParam, "class");
        $attributes = $this->K_COMMON->getVarArray($arrayParam, "attributes");
        $multiple = $this->K_COMMON->getVarArray($arrayParam, "multiple");
        $required = $this->K_COMMON->getVarArray($arrayParam, "required");
        $size = $this->K_COMMON->getVarArray($arrayParam, "size");
        $disabled = $this->K_COMMON->getVarArray($arrayParam, "disabled");

        // if is set "multiple" add array at input name
        if ($multiple == "1") {
            $ID .= "[]";
            $arrayParam["id"] = $ID;
            if ($name != "") {
                $name .= "[]";
                $arrayParam["name"] = $name;
            }

        }

        //check if disabled
        if ($disabled == "1")
            $attributes .= ' disabled="disabled" ';

        // check size
        if ($size == "sm")
            $class .= " form-control-sm";

        // check if required
        if ($required == "1")
            $attributes .= ' required="" ';

        // load options of select
        $objHTMLOption = "";
        $arrayLoad = $this->loadOptionSelect($arrayParam);
        if ($arrayLoad["success"] == "true") {
            if ($arrayLoad["html"] != "")
                $objHTMLOption = $arrayLoad["html"];

        }

        // create final html of select
        $objHTML = '<select id="' . $ID . '" init-value = "' . $value . '" class="form-control ' . $class . '" name="' . $name . '" ' . $attributes . '>' . $objHTMLOption . '</select>';

        // append object of select, with container
        $this->appendFieldContainer($idToAppend, $arrayParam);
        $this->appendObject($this->lastObject, $objHTML);
    }

    /**
     * append input phone (combo with prefix + input phone number)
     *
     * @param array $arrayParam array of input parameters.<br>
     *
     *  Parameters: <br>
     *
     * *    name    : name of input (it's used for create input hidden)
     * *    id    : optional id of input
     * *    options    : optional array of prefix (Ex: array("+44" => "EN", "+39" => "IT"))
     * *    prefixValue    : value of prefix
     *
     * @see ::appendGeneralInput() for extra parameters of input phone
     * @see ::appendInputSelect() for extra parameters of select prefix
     *
     */
    protected function appendInputPhone($idToAppend, $arrayParam = array())
    {

        //get variables
        $name = $this->K_COMMON->getVarArray($arrayParam, "name");
        $ID = $this->K_COMMON->getVarArray($arrayParam, "id");
        $phonePrefixArray = $this->K_COMMON->getVarArray($arrayParam, "options");
        $prefixValue = $this->K_COMMON->getVarArray($arrayParam, "prefixValue");
        $rowContainerClass = $this->K_COMMON->getVarArray($arrayParam, "rowContainerClass");

        // create prefix array if not get by parameters
        if ($phonePrefixArray == "")
            $phonePrefixArray = array("+39" => "IT", "+33" => "FR", "+34" => "ES", "+44" => "EN", "+49" => "DE", "+353" => "IE");

        //create row
        $this->appendRow($idToAppend, ["class" => $rowContainerClass]);

        // build combo prefix
        $this->appendColumn($this->lastRow, array("class" => "col-md-3"));
        $comboParam = $arrayParam;
        $comboParam["typeLoad"] = "array";
        $comboParam["options"] = $phonePrefixArray;
        if ($name != "")
            $comboParam["name"] = "prefix_" . $name;
        $comboParam["id"] = "prefix_" . $ID;
        if ($prefixValue != "")
            $comboParam["value"] = $prefixValue;
        $comboParam["textHelp"] = "";
        $this->appendInputSelect($this->lastCol, $comboParam);

        // build input phone
        $this->appendColumn($this->lastRow, array("class" => "col-md-9"));
        $inputParam = $arrayParam;
        $inputParam["id"] = $ID;
        $inputParam["labelInput"] = "";
        $inputParam["attributes"] = ' pattern="^[0-9]{1,}$"';
        $this->appendInputText($this->lastCol, $inputParam);

    }

    /**
     * append checkbox
     *
     * @param array $arrayParam array of input parameters.<br>
     *
     *  Parameters: <br>
     *
     * *    name    : name of input
     * *    id    : optional id of input
     * *    value    : optional value of the input
     * *    labelInput : label of input
     * *    labelPos : label position (SX or DX as default)
     * *    class    : optional class of the input
     * *    attributes    : optional attributes of the input
     * *    required    : set input as required
     * *    size    : "sm" for small
     * *    toggle    : enable toggle
     * *    triggerFunction    : this function will be triggered on click, or after ajax call
     * *    ajax    : array for data connection by ajax (on click trigger)
     *   *    kcp : name of plugin
     *   *    kco : name of object
     *   *    kca : function name (return an array of data db results)
     *   *    parameters : parameters to send
     *
     * @see ::getFieldContainer object for extra parameters
     *
     */
    protected function appendInputCheckBox($idToAppend, $arrayParam = array())
    {
        // get variables
        $name = $this->K_COMMON->getVarArray($arrayParam, "name");
        $ID = $this->K_COMMON->getVarArray($arrayParam, "id");
        $value = $this->K_COMMON->getVarArray($arrayParam, "value");
        $checked = $this->K_COMMON->getVarArray($arrayParam, "checked");
        $labelInput = $this->K_COMMON->getVarArray($arrayParam, "labelInput");
        $labelPos = $this->K_COMMON->getVarArray($arrayParam, "labelPos");
        $class = $this->K_COMMON->getVarArray($arrayParam, "class");
        $style = $this->K_COMMON->getVarArray($arrayParam, "style");
        $attributes = $this->K_COMMON->getVarArray($arrayParam, "attributes");
        $required = $this->K_COMMON->getVarArray($arrayParam, "required");
        $size = $this->K_COMMON->getVarArray($arrayParam, "size");
        $ajax = $this->K_COMMON->getVarArray($arrayParam, "ajax");
        $triggerFunction = $this->K_COMMON->getVarArray($arrayParam, "triggerFunction");
        $toggle = $this->K_COMMON->getVarArray($arrayParam, "toggle");


        // set variables
        $arrayParam["containerClass"] = "checkbox-inline";

        //Connection
        if (is_array($ajax)) {
            $btnkcp = $this->K_COMMON->getVarArray($ajax, "kcp");
            $btnkco = $this->K_COMMON->getVarArray($ajax, "kco");
            $btnkca = $this->K_COMMON->getVarArray($ajax, "kca");

            //replace "\" on kco with double "\" for javascript
            $btnkco = str_replace("\\", "\\\\", $btnkco);

        } else {
            $btnkcp = "";
            $btnkco = "";
            $btnkca = "";
        }

        // check size
        if ($size == "sm")
            $class .= " form-control-sm";

        // check required
        if ($required == "1")
            $attributes .= ' required="" ';

        // check checked
        if ($checked == "1")
            $attributes .= ' checked="checked" ';

        //check toggle
        if ($toggle == "1")
            {
                $this->addExternalPlugin("bootstrap-toggle");
                $attributes .= ' data-toggle="toggle"  ';
                $IDForTrigger = "fieldset_" . $ID . " .toggle";
            }
        else
            $IDForTrigger = $ID;

        //check Label Pos
        if ($labelPos == "SX")
        {
            $labelInputSX = $labelInput . " ";
            $labelInputDX = "";
        }
        elseif ($labelPos == "UP")
        {
            $labelInputSX = "";
            $labelInputDX = "";
            $class .= " form-control";
            $style .= " width: 30%; margin-top: 10px;";
        }
        else
        {
            $labelInputSX = "";
            $labelInputDX = " " . $labelInput;
        }


        // create html
        if ($labelInput != "")
            {
                if ($labelPos == "UP")
                    $objHTML = '<label for="' . $ID . '">'
                        . $labelInput
                        .'  </label>'
                        .'<input type="checkbox" id="' . $ID . '" name="' . $name . '" value="' . $value . '" class="' . $class . '" style="' . $style. '" ' . $attributes . ' />';

                else
                    $objHTML = '<label for="' . $ID . '">'
                        . $labelInputSX
                        .'<input type="checkbox" id="' . $ID . '" name="' . $name . '" value="' . $value . '" class="' . $class . '" ' . $attributes . ' />'
                        . $labelInputDX . '
                           </label>';
            }
        else
            $objHTML = '<input type="checkbox" id="' . $ID . '" name="' . $name . '" value="' . $value . '" class="' . $class . '" ' . $attributes . ' />';




        //check if write script
        if (is_array($ajax)) {

            $JSButtonAttributesString  = "";
            $parametersToSend = $this->K_COMMON->getVarArray($ajax, "parameters");
            if (is_array($parametersToSend)) {
                foreach ($parametersToSend as $key => $val) {
                    if ($JSButtonAttributesString != "")
                        $JSButtonAttributesString .= ', ' . $key . ':"' . $val.'"';
                    else
                        $JSButtonAttributesString .= ' ' . $key . ':"' . $val.'"';

                }
            }



            $jsUniqueID = "kx_input_checkbox_" . uniqid();
            $this->addFooterJS('<script>
			' . $jsUniqueID . ' = {};
			(function() {
			
				var t = this;
				this.init = function(){
					$(document).ready(t.ready);
				};
				this.trigger = function(){
				    var val = 0;
                    if ($(this).is(":checked")) val = 1;
				    waitingDialog.show("Loading...", {dialogSize: "sm", progressType: "warning"});
                    $.ajax({
                          url: KXJS.buildAPICallUrl("KXCJS","' . $btnkcp . '","' . $btnkco . '","' . $btnkca . '","checked="+val),
                        data: {' . $JSButtonAttributesString . '}
                    }).done(function(data) {
                        r = $.parseJSON(data);
                        waitingDialog.hide();
                        ' . ($triggerFunction != "" ? $triggerFunction . '({checked: val, result:r});' : '') . '
                        
                    });
				}
				this.ready = function(){
                    $("#' . $IDForTrigger . '").click(t.trigger);
				}

				this.init();

			}).apply(' . $jsUniqueID . ');

		</script>');
        }
        else
        {
            if ($triggerFunction != "")
            {
                $jsUniqueID = "kx_input_checkbox_" . uniqid();
                $this->addFooterJS('<script>
			' . $jsUniqueID . ' = {};
			(function() {
			
				var t = this;
				this.init = function(){
					$(document).ready(t.ready);
				};
				this.trigger = function(){
				var val = 0;
                    if ($("#' . $ID . '").is(":checked")) val = 1;
                    ' . ($triggerFunction != "" ? $triggerFunction . '({checked: val});' : '') . '
				}
				this.ready = function(){
                    $("#' . $IDForTrigger . '").click(t.trigger);
				}

				this.init();

			}).apply(' . $jsUniqueID . ');

		</script>');
            }
        }

        // return
        $arrayParam["noLabel"] = "1";
        $this->appendFieldContainer($idToAppend, $arrayParam);
        $this->appendObject($this->lastObject, $objHTML);



    }

    /**
     * append radio
     *
     * @param array $arrayParam array of input parameters.<br>
     *
     *  Parameters: <br>
     *
     * *    name    : name of input
     * *    value    : optional value of the input (ex: "1")
     * *    options : value and label of input n(ex: array("1" => "Red", "2" => "Yellow") )
     * *    class    : optional class of the input
     * *    attributes    : optional attributes of the input
     * *    required    : set input as required
     * *    size    : "sm" for small
     *
     * @see ::getFieldContainer object for extra parameters
     *
     */
    protected function appendInputRadio($idToAppend, $arrayParam = array())
    {
        //get variables
        $name = $this->K_COMMON->getVarArray($arrayParam, "name");
        $value = $this->K_COMMON->getVarArray($arrayParam, "value");
        $options = $this->K_COMMON->getVarArray($arrayParam, "options");
        $class = $this->K_COMMON->getVarArray($arrayParam, "class");
        $attributes = $this->K_COMMON->getVarArray($arrayParam, "attribute");
        $required = $this->K_COMMON->getVarArray($arrayParam, "required");
        $size = $this->K_COMMON->getVarArray($arrayParam, "size");

        // check size
        if ($size == "sm")
            $class .= " form-control-sm";

        // check required
        if ($required == "1")
            $attributes .= ' required="" ';

        //create html
        $this->appendFieldContainer($idToAppend, $arrayParam);
        $idToAppend = $this->lastObject;
        if (!is_array($options))
            if (is_string($options))
                $options = json_decode($options, true);
        if (is_array($options)) {
            if (is_array(current($options)))
            {
                foreach ($options as $option)
                {
                    $valueAct = $option["value"];
                    $label = $option["text"];

                    $arrayRadio = array(
                        "value" => $valueAct
                    , "label" => $label
                    , "class" => $class
                    , "name" => $name
                    , "attributes" => ($valueAct == $value ? 'checked="checked" ' . $attributes : $attributes)
                    , "noLabel" => "1"
                    );

                    $this->getRadioOption($idToAppend,$arrayRadio);

                }
            }
            else
            {
                foreach ($options as $valueAct => $label) {
                    $arrayRadio = array(
                        "value" => $valueAct
                    , "label" => $label
                    , "class" => $class
                    , "name" => $name
                    , "attributes" => ($valueAct == $value ? 'checked="checked" ' . $attributes : $attributes)
                    , "noLabel" => "1"
                    );
                    $this->getRadioOption($idToAppend,$arrayRadio);
                }
            }


        }

        // append object
        //$arrayParam["noLabel"] = "1";
        //$this->appendFieldContainer($idToAppend, $arrayParam);
        //$this->appendObject($this->lastObject, $objHTML);
    }

    /**
     * create single html option for Radio
     *
     * @param array $arrayParam array of input parameters.<br>
     *
     *  Parameters: <br>
     *
     * *    name    : name of input
     * *    label    : label of radio
     * *    value    : value of radio
     * *    class    : optional class of the input
     * *    attributes    : optional attributes of the input
     *
     *
     */
    protected function getRadioOption($idToAppend, $arrayParam = array())
    {
        // get variables
        $name = $this->K_COMMON->getVarArray($arrayParam, "name");
        $label = $this->K_COMMON->getVarArray($arrayParam, "label");
        $value = $this->K_COMMON->getVarArray($arrayParam, "value");
        $class = $this->K_COMMON->getVarArray($arrayParam, "class");
        $attributes = $this->K_COMMON->getVarArray($arrayParam, "attributes");

        // set variables
        $ID = "radio_" . uniqid();

        // create html
        $objHTML = '<label for="' . $ID . '" class="radio-inline m-r-1">
                        	<input type="radio" id="' . $ID . '" name="' . $name . '" value="' . $value . '" class="' . $class . '" ' . $attributes . ' />
                        	' . $label . '
                    </label>';

        // append object

        $this->appendObject($idToAppend, $objHTML);
    }

    /**
     * append text area
     *
     * @param array $arrayParam array of input parameters.<br>
     *
     *  Parameters: <br>
     *
     * *    name    : name of input
     * *    id    : optional id of the input
     * *    labelPlaceholder    : optional label of the input
     * *    value    : value of textarea
     * *    class    : optional class of the textarea
     * *    required    : set textarea as required
     * *    size    : "sm" for small
     * *    multiple    : set the name of field as array.
     * *    ncol    : number of column
     * *    nrow    : number of row
     *
     * @see ::getFieldContainer object for extra parameters
     *
     */
    protected function appendInputTextArea($idToAppend, $arrayParam = array())
    {
        // get variable
        $name = $this->K_COMMON->getVarArray($arrayParam, "name");
        $ID = $this->K_COMMON->getVarArray($arrayParam, "id");
        $value = $this->K_COMMON->getVarArray($arrayParam, "value");
        $class = $this->K_COMMON->getVarArray($arrayParam, "class");
        $required = $this->K_COMMON->getVarArray($arrayParam, "required");
        $size = $this->K_COMMON->getVarArray($arrayParam, "size");
        $labelPlaceholder = $this->K_COMMON->getVarArray($arrayParam, "labelPlaceholder");
        $multiple = $this->K_COMMON->getVarArray($arrayParam, "multiple");
        $ncol = $this->K_COMMON->getVarArray($arrayParam, "ncol");
        $nrow = $this->K_COMMON->getVarArray($arrayParam, "nrow");


        // check size
        if ($size == "sm")
            $class .= " form-control-sm";

        // if is set "multiple" add array at input name
        if ($multiple == "1") {
            $ID .= "[]";
            if ($name != "") {
                $name .= "[]";
            }
        }


        // create element
        $dom = new \DOMDocument("1.0", 'UTF-8');
        $dom->formatOutput = true;
        $element = $dom->createElement('textarea');

        //set Value
        $textNode = $dom->createTextNode($value);
        $element->appendChild($textNode);


        //set Attributes
        $element->setAttribute("id", $ID);
        $element->setAttribute("name", $name);
        $element->setAttribute("class", "form-control " . $class);

        //check row col
        if ($nrow != "")
            $element->setAttribute("rows", $nrow);
        if ($ncol != "")
            $element->setAttribute("cols", $ncol);

        // set attribute "placeholder"
        if ($labelPlaceholder != "")
            $element->setAttribute("placeholder", $labelPlaceholder);


        //set others attributes
        // check required
        if ($required == "1")
            $element->setAttribute("required", "");

        //Append Element
        $dom->appendChild($element);

        //get html
        $objHTML = $dom->saveHTML();

        // append object
        $this->appendFieldContainer($idToAppend, $arrayParam);
        $this->appendObject($this->lastObject, $objHTML);
    }

    /**
     * append color picker
     *
     * @param array $arrayParam array of input parameters.<br>
     *
     *  Parameters: <br>
     *
     * *    name    : name of input
     * *    id    : optional id of the input
     * *    value    : optional value of date
     *
     * @see ::appendGeneralInput() for extra parameters
     * @see ::getFieldContainer object for extra parameters
     *
     */
    protected function appendColorPicker($idToAppend, $arrayParam = array())
    {
        // get variable
        $ID = $this->K_COMMON->getVarArray($arrayParam, "id");

        //Include Files for Head
        $this->addExternalPlugin("colorpicker");

        // set color picker javascript
        $this->addFooterJS('<script>
				$(document).ready(function(){
					$("#addonGroup_' . $ID . '").colorpicker();
				});
			</script>');


        // append object
        $this->appendFieldContainer($idToAppend, $arrayParam);
        //$this->appendObject($this->lastObject, $inputHTML);

        //append Input
        $arrayParam["type"] = "text";
        $arrayParam["addGroup"] = '<i></i>';
        $arrayParam["id"] = $ID;
        $this->appendGeneralInput($this->lastObject, $arrayParam);


    }


    /**
     * append date picker
     *
     * @param array $arrayParam array of input parameters.<br>
     *
     *  Parameters: <br>
     *
     * *    name    : name of input
     * *    id    : optional id of the input
     * *    value    : optional value of date
     * *    format    : optional date format (Ex: yyyy/mm/dd)
     * *    startView    : optional First to Show
     * *    minuteStep : optional step minutes (default 30)
     * * * 0 or 'hour' for the hour view
     * * * 1 or 'day' for the day view
     * * * 2 or 'month' for month view (the default)
     * * * 3 or 'year' for the 12-month overview
     * * * 4 or 'decade' for the 10-year overview. Useful for date-of-birth datetimepickers
     * * *  minView    : optional Min View of datepicker to show (see startView)
     * * * maxView    : optional Min View of datepicker to show (see startView)
     *
     * @see ::appendGeneralInput() for extra parameters
     * @see ::getFieldContainer object for extra parameters
     *
     */
    protected function appendDatePicker($idToAppend, $arrayParam = array())
    {
        // get variable 
        $ID = $this->K_COMMON->getVarArray($arrayParam, "id");
        $format = $this->K_COMMON->getVarArray($arrayParam, "formatDate");
        $value = $this->K_COMMON->getVarArray($arrayParam, "value");
        $startView = $this->K_COMMON->getVarArray($arrayParam, "startView");
        $minView = $this->K_COMMON->getVarArray($arrayParam, "minView");
        $maxView = $this->K_COMMON->getVarArray($arrayParam, "maxView");
        $minuteStep = $this->K_COMMON->getVarArray($arrayParam, "minuteStep");
        $pickerPosition = $this->K_COMMON->getVarArray($arrayParam, "pickerPosition");

        //Include Files for Head
        $this->addExternalPlugin("datetimepicker");


        //check variables
        if ($startView == "")
            $startView = "2";
        if ($minView == "")
            $minView = "hour";
        if ($maxView == "")
            $maxView = "decade";
        if ($pickerPosition == "")
            $pickerPosition = "bottom-left";

        // check attributes
        if (isset($arrayParam["attributes"]))
            $arrayParam["attributes"] .= ' readonly="" ';
        else
            $arrayParam["attributes"] = ' readonly="" ';

        // check format
        if ($format == "")
            $format = "yyyy-mm-dd";

        //check minuteStep
        if ($minuteStep == "")
            $minuteStep = 30;


        // set date picker javascript
        $this->addFooterJS('<script>
				$(document).ready(function(){
					$("#' . $ID . '").datetimepicker({
						format: "' . $format . '",
						autoclose: true,
						pickerPosition: "' . $pickerPosition . '",
						todayHighlight: 1,
						weekStart: 1,
						todayBtn:  1,
						minuteStep : '.$minuteStep.',
						todayHighlight: 1,
						startView: ' . (is_numeric($startView) ? $startView : '"' . $startView . '"') . ',
						minView: ' . (is_numeric($minView) ? $minView : '"' . $minView . '"') . ',
						maxView: ' . (is_numeric($maxView) ? $maxView : '"' . $maxView . '"') . ',
						forceParse: 0,
						fontAwesome: 1
					});
				});
			</script>');

        // create html

        $inputHTML =
            '<div id="' . $ID . '" class="input-group date form_datetime" data-link-format="' . $format . '"'
            . ' data-date-format="' . $format . '" data-date="' . $value . '" data-link-field="' . $ID . '">'
            . '</div>';


        // append object
        $this->appendFieldContainer($idToAppend, $arrayParam);
        $this->appendObject($this->lastObject, $inputHTML);

        //append Input
        $arrayParam["type"] = "text";
        $arrayParam["id"] = "dateTime_" . $ID;
        $this->appendGeneralInput($ID, $arrayParam);

        //append Icon
        $this->appendHTML($ID, '<span class="input-group-addon"><i class="fa fa-calendar"></i></span>');


    }


    /**
     * append date picker Multi Select
     *
     * @param array $arrayParam array of input parameters.<br>
     *
     *  Parameters: <br>
     *
     * *    name    : name of input
     * *    id    : optional id of the input
     * *    value    : optional value of date
     * *    format    : optional date format (Ex: yyyy/mm/dd)
     * *    startView    : optional First to Show
     * * * 0 or 'hour' for the hour view
     * * * 1 or 'day' for the day view
     * * * 2 or 'month' for month view (the default)
     * * * 3 or 'year' for the 12-month overview
     * * * 4 or 'decade' for the 10-year overview. Useful for date-of-birth datetimepickers
     * *    minView    : optional Min View of datepicker to show (see startView)
     * *    maxView    : optional Min View of datepicker to show (see startView)
     * *    opened    : optional 1 if you want opened calendar
     * *    multidate    : optional 1 if you want select more that one day
     *
     * @see ::appendGeneralInput() for extra parameters
     * @see ::getFieldContainer object for extra parameters
     *
     */
    protected function appendDatePickerMulti($idToAppend, $arrayParam = array())
    {
        // get variable
        $ID = $this->K_COMMON->getVarArray($arrayParam, "id");
        $format = $this->K_COMMON->getVarArray($arrayParam, "formatDate");
        $value = $this->K_COMMON->getVarArray($arrayParam, "value");
        $startView = $this->K_COMMON->getVarArray($arrayParam, "startView");
        $minView = $this->K_COMMON->getVarArray($arrayParam, "minView");
        $maxView = $this->K_COMMON->getVarArray($arrayParam, "maxView");
        $opened = $this->K_COMMON->getVarArray($arrayParam, "opened");
        $multidate = $this->K_COMMON->getVarArray($arrayParam, "multidate");
        $prefixID = "dateTime_";

        //Include Files for Head
        $this->addExternalPlugin("datepickermulti");


        //check variables
        if ($startView == "")
            $startView = "2";
        if ($minView == "")
            $minView = "hour";
        if ($maxView == "")
            $maxView = "decade";

        // check attributes
        if (isset($arrayParam["attributes"]))
            $arrayParam["attributes"] .= ' readonly="" ';
        else
            $arrayParam["attributes"] = ' readonly="" ';

        // check format
        if ($format == "")
            $format = "yyyy/mm/dd";

        if ($opened == "1")
        {
            $startView = "";
            $minView = "";
            $maxView = "";
        }


        // set date picker javascript
        $this->addFooterJS('<script>
				$(document).ready(function(){
					$("' . "#" . ($opened == "1" ? $ID : $prefixID . $ID) . '").datepicker({
						format: "' . $format . '",
						language: KXJS.getLanguage().toLowerCase(),
						multidate: '.($multidate == "1" ? "true" : "false").',
                        weekStart: 1,   
                        daysOfWeekHighlighted: "0,6",
                        todayHighlight: true,
                        calendarWeeks: true,
                        clearBtn: true,
						startView: ' . (is_numeric($startView) ? $startView : '"' . $startView . '"') . ',
						minView: ' . (is_numeric($minView) ? $minView : '"' . $minView . '"') . ',
						maxView: ' . (is_numeric($maxView) ? $maxView : '"' . $maxView . '"') . '
					});
				});
			</script>');


        //check if want opened calendar

        if ($opened == "1") {
            $IDCalendarDIV = $ID;
            $inputHTML =
                '<div id="' . $IDCalendarDIV . '"  class="input-group date form_datetime" data-link-format="' . $format . '"'
                . ' data-date-format="' . $format . '" data-date="' . $value . '" data-link-field="' . $ID . '">'
                . ' </div>';
        } else {
            // create input html
            $IDCalendarDIV = ($opened == "1" ? $ID : $prefixID . $ID);
            $inputHTML =
                '<div id="' . $IDCalendarDIV . '" class="input-group date form_datetime" data-link-format="' . $format . '"'
                . ' data-date-format="' . $format . '" data-date="' . $value . '" data-link-field="' . $ID . '">'
                . ' </div>';
        }

        // append object
        $this->appendFieldContainer($idToAppend, $arrayParam);
        $this->appendObject($this->lastObject, $inputHTML);

        if ($opened != "1") {
            //append Input
            $arrayParam["type"] = "text";
            $arrayParam["id"] = $prefixID . $ID;
            $this->appendGeneralInput($IDCalendarDIV, $arrayParam);

            //append Icon
            $this->appendHTML($IDCalendarDIV, '<span class="input-group-addon"><i class="fa fa-calendar"></i></span>');
        }
    }


    /**
     * append normal button
     *
     * @param array $arrayParam array of input parameters.<br>
     * see ::getButton() for supported parameters
     *
     * @see ::getButton()
     *
     */
    protected function appendBtn($idToAppend, $arrayParam = array())
    {

        $arrayParam["type"] = "button";
        $inputData = $this->getButton($arrayParam);
        $this->appendObject($idToAppend, $inputData["html"]);

        $arrayParam["id"] = $inputData["id"];
        $this->createButtonsScript($arrayParam);

    }

    /**
     * append submit button
     *
     * @param array $arrayParam array of input parameters.<br>
     * see ::getButton() for supported parameters
     *
     * @see ::getButton()
     *
     */
    protected function appendBtnSubmit($idToAppend, $arrayParam = array())
    {
        $arrayParam["type"] = "submit";
        $inputData = $this->getButton($arrayParam);
        $this->appendObject($idToAppend, $inputData["html"]);

        $arrayParam["id"] = $inputData["id"];
        $this->createButtonsScript($arrayParam);
    }



    /**
     * get a Button Script
     *
     * @since 1.00
     * @access public
     *
     * @param array $arrayParam array with parameters for create button
     *
     *  The array of button support this values<br>
     *
     * *    id    : id of the button
     * *    onClick: "function", "ajax", "general"
     * *    functionSubmit    : optional javascript function name to call on click button
     * *    functionPreSubmit    : optional javascript function name to call before submit
     * *    functionPostSubmit    : optional javascript function name to call after submit
     * *    ajax: array of parameters for open model (on success = true), all results will be sent to the modal
     * *    ajax    : array for data connection by ajax
     *   *    kcp : name of plugin
     *   *    kco : name of object
     *   *    kca : action code
     * *    dialogMessage    : optional Message for dialog on submit button (default "Please Wait...")
     * *    closeModal    : optional the ID of modal to close after submit button (on success)
     * *    closeModalNow    : optional the ID of modal to close immediatly, without check success
     * *    openModal: array of parameters for open model (on success = true), all results will be sent to the modal
     * *        idModal: id of modal
     * *        kcp: kcp of ajax
     * *        kco: kco of ajax
     * *        kca: kca of ajax
     * *        parameters: parameters for ajax
     * *        parametersRef: parameters for ajax to get from page by ID
     * *        onClose: function to call on close modal
     * *    reloadTableList    : optional the ID of table to reload (on success)
     * *    reloadTableListParam    : array optional param for "loadTableList" of table lsi
     * *    reloadTableListWithHead    : optional the ID of table to reload (on success)
     * *    reloadPageActual: optional if 1 reload actual page with same URL
     * *    reloadPage: optional if 1 reload page with new ID or new Code EX of URL: get/17
     * *    reloadPageURL: url for reload page (if different from actual
     * *    reloadMethod: REST or GET
     * *    reloadPluginMethod: method for plugin, "get", "put" or your custom method (default "get") - replace "put" method on url
     * *    reloadPageCheckCode: optional Code to check if we are in modify or not
     * *    reloadPageGetCode: optional Code to get from answer for URL GET
     * *    locationURL: optional URL if success after submit
     * *    nextStep: if used "pagesteps" object you can specify the next step on success
     * *    staticAttributes: array of attributes to send in ajax call (Ex: ["a" => "1", "b" => "3"])
     * *    dynamicAttributes: array of attributes to get from button to send in ajax call (Ex: ["a" => "1", "b" => "3"])
 *
     * @return webobject
     */
    protected function createButtonsScript( $arrayParam) {



        //set variables
        $id = $this->K_COMMON->getVarArray($arrayParam, "id");
        $onClick = $this->K_COMMON->getVarArray($arrayParam, "onClick");
        $functionPostSubmit = $this->K_COMMON->getVarArray($arrayParam, "functionPostSubmit");
        $functionPreSubmit = $this->K_COMMON->getVarArray($arrayParam, "functionPreSubmit");
        $functionSubmit = $this->K_COMMON->getVarArray($arrayParam, "functionSubmit");
        $btnAjax = $this->K_COMMON->getVarArray($arrayParam, "ajax");
        $disableIF = $this->K_COMMON->getVarArray($arrayParam, "disableIF");

        if (is_string($btnAjax))
            $btnAjax = json_decode($btnAjax, true);

        //Connection
        if (is_array($btnAjax)) {
            $kcall = $this->K_COMMON->getVarArray($btnAjax, "kcall");
            $kcp = $this->K_COMMON->getVarArray($btnAjax, "kcp");
            $kco = $this->K_COMMON->getVarArray($btnAjax, "kco");
            $kca = $this->K_COMMON->getVarArray($btnAjax, "kca");

            //replace "\" on kco with double "\" for javascript
            $kco = str_replace("\\", "\\\\", $kco);


        } else
        {
            $kcall = "";
            $kcp = "";
            $kco = "";
            $kca = "";
        };


        $dialogMessage = $this->K_COMMON->getVarArray($arrayParam, "dialogMessage");
        $closeModal = $this->K_COMMON->getVarArray($arrayParam, "closeModal");
        $closeModalNow = $this->K_COMMON->getVarArray($arrayParam, "closeModalNow");
        $openModal = $this->K_COMMON->getVarArray($arrayParam, "openModal");
        $reloadTableList = $this->K_COMMON->getVarArray($arrayParam, "reloadTableList");
        $reloadTableListParam = $this->K_COMMON->getVarArray($arrayParam, "reloadTableListParam");
        $reloadTableListWithHead = $this->K_COMMON->getVarArray($arrayParam, "reloadTableListWithHead");

        $nextStep = $this->K_COMMON->getVarArray($arrayParam, "nextStep");
        $reloadPageActual = $this->K_COMMON->getVarArray($arrayParam, "reloadPageActual");
        $reloadPage = $this->K_COMMON->getVarArray($arrayParam, "reloadPage");
        $reloadMethod = $this->K_COMMON->getVarArray($arrayParam, "reloadMethod");
        $reloadPluginMethod = $this->K_COMMON->getVarArray($arrayParam, "reloadPluginMethod");
        $reloadPageURL = $this->K_COMMON->getVarArray($arrayParam, "reloadPageURL");
        $locationURL = $this->K_COMMON->getVarArray($arrayParam, "locationURL");
        $reloadPageCheckCode = $this->K_COMMON->getVarArray($arrayParam, "reloadPageCheckCode");
        $reloadPageGetCode = $this->K_COMMON->getVarArray($arrayParam, "reloadPageGetCode");

        $staticAttributes = $this->K_COMMON->getVarArray($arrayParam, "staticAttributes");
        $dynamicAttributes = $this->K_COMMON->getVarArray($arrayParam, "dynamicAttributes");



        //set dialog message
        if ($dialogMessage == "")
            $dialogMessage = "Please Wait...";


        //check variables
        if ($reloadMethod == "")
            $reloadMethod = "REST";
        if ($reloadPage == "1") {
            if ($reloadPageCheckCode == "")
                $reloadPageCheckCode = "keyID";
            if ($reloadPageGetCode == "")
                $reloadPageGetCode = "keyID";
            if ($reloadPageURL == "")
                $reloadPageURL = $this->K_COMMON->getCurrentURI();

            if ($reloadMethod == "REST") {
                $posPut = strpos($reloadPageURL, "/put"); //used for panel
                if ($posPut > 0)
                {
                    $reloadPageURL = substr($reloadPageURL, 0, $posPut);
                }
                else
                {
                    $posPut = strpos($reloadPageURL, "/get"); //used for panel
                    if ($posPut > 0)
                    {
                        $reloadPageURL = substr($reloadPageURL, 0, $posPut);
                    }
                }
                if (substr($reloadPageURL, -1, 1) != "/")
                    $reloadPageURL .= "/";

            } else {
                if (strpos($reloadPageURL, "?") === false)
                    $reloadPageURL .= "?";
            }
        }

        if (is_array($reloadTableListParam))
            $reloadTableListParamJSON = json_encode($reloadTableListParam);
        else
            $reloadTableListParamJSON = "";


        //Check for Open Modal
        if (is_array($openModal)) {
            $idModal = $this->K_COMMON->getVarArray($openModal, "idModal");
            $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");
            $webModal->newModalJS($openModal);
            $this->mergeObjectVariables($webModal);
        }


        //check query attributes for ajax call
        if (is_string($staticAttributes))
            $staticAttributes = json_decode($staticAttributes, true);



        //crate parameter for ajax call
        $JSButtonAttributesString = "";
        if (is_array($staticAttributes)) {
            foreach ($staticAttributes as $key => $val) {
                if ($JSButtonAttributesString != "")
                    $JSButtonAttributesString .= ', ' . $key . ':"' . $val.'"';
                else
                    $JSButtonAttributesString .= ' ' . $key . ':"' . $val.'"';

            }
        }
        if (is_array($dynamicAttributes)) {
            foreach ($dynamicAttributes as $key => $val) {
                $JSButtonAttributesString .= ', ' . $key . ': btnClicked.attr("' . $key . '")';
            }
        }


        //Set Reload Table Script
        $reloadTableListScript = "";
        if (is_array($reloadTableList)) {
            foreach ($reloadTableList as $rt) {
                $reloadTableListScript .= "kx_tablelist_script_" . $rt . ".loadTableList(" . $reloadTableListParamJSON . ");";
            }
        } elseif ($reloadTableList != "")
            $reloadTableListScript .= "kx_tablelist_script_" . $reloadTableList . ".loadTableList(" . $reloadTableListParamJSON . ");";

        if (is_array($reloadTableListWithHead)) {
            foreach ($reloadTableListWithHead as $rt) {
                $reloadTableListScript .= "kx_tablelist_script_" . $rt . ".loadTableList({onlyRow:0});";
            }
        } elseif ($reloadTableListWithHead != "")
            $reloadTableListScript .= "kx_tablelist_script_" . $reloadTableListWithHead . ".loadTableList({onlyRow:0});";

        //Set JS Common
        $JSCommonPostSubmit =
            ($functionPostSubmit != "" ? $functionPostSubmit . "(r,'" . $id . "');" : "")
            . ($nextStep != "" ?
                'if(r.success == "true"){
                        $("#pageStep_'. ($nextStep - 1). '").hide();
                        $("#pageStep_'. $nextStep . '").show();
                    }'
                : "")
            . ($reloadPageActual == "1" ?
                'if(r.success == "true"){
                    location.reload();
                                             }'
                : "")
            . ($reloadPage == "1" ?
                'if(r.success == "true"){
                                                if($("#' . $reloadPageCheckCode . '").val() == "")
                                                    {'
                . (
                $reloadMethod != "REST" ? 'location.href = "' . $reloadPageURL . '" + "&' . $reloadPageGetCode . '="+r.' . $reloadPageGetCode . ';' : 'location.href = "' . $reloadPageURL . '" + "' . $reloadPluginMethod . '/"+r.' . $reloadPageGetCode . ';'
                )
                . '}
                                             }'
                : "")
            . ($locationURL != "" ?
                'if(r.success == "true"){'
                . (
                    'location.href = "' . $locationURL . '";'
                )
                . '}'
                : "")
            . ($closeModal != "" ?
                '
                                            if(r.success == "true"){
                                                dialogModal.on("hidden.bs.modal"
                                                        , function () {'
                .$reloadTableListScript
                . ($openModal != "" ?
                    '
                                                                            if(r.success == "true"){
                                                                                var extraParamToSend = r;
                                                                                
                                                                               '.$idModal.'OpenModal({extraParam: extraParamToSend});'.'
                                                                            }
                                                                           
                                                                            '
                    : ''
                )
                . '}
                                                );
                                                
                                                $("#' . $closeModal . '").modal("hide");
                                                waitingDialog.hide();
                                            }
                                            else{
                                                waitingDialog.hide();
                                            }
                                            '
                : (
                    'if(r.success == "true") { '
                    .$reloadTableListScript
                    . '}'

                )
            )
            . ($closeModalNow != "" ?
                '    
                                                $("#' . $closeModalNow . '").modal("hide");
                '
                : ''
            )
            . ($openModal != "" && $closeModal == "" ?
                '
                                            if(r.success == "true"){
                                                var extraParamToSend = r;
                                                
                                               '.$idModal.'OpenModal({extraParam: extraParamToSend});'.'
                                            }
                                           
                                            '
                : ''
            )
        ;

        // set javascript and css to be included on page
        $this->addExternalPlugin(["validator", "waitingfor"]);

        //Check if manage onClick
        switch ($onClick){

            case "function":
                    if ($functionSubmit != "")
                        $this->addFooterJS('<script>'
                            . '$(document).ready(function(){
                                    
                                    $("#' . $id . '").click(function () {
                                            ' . ($functionPreSubmit != "" ? $functionPreSubmit . "();" : "") . '
                                            ' . $functionSubmit . '();
                                            ' . $JSCommonPostSubmit . '
                                        });
                                });
                            </script>');
                break;

                case "ajax":
                    if ($kcp != "") {

                        if ($kcall == "")
                            $kcall = "KXCJS";


                        $this->addFooterJS('<script>'
                            . 'var ' . $id . '_functionSubmit = function(btnClicked){
                       ' . ($functionPreSubmit != "" ? $functionPreSubmit . "();" : "") . '
                                dialogModal = waitingDialog.show("' . $dialogMessage . '", {dialogSize: "sm", progressType: "warning"});
                                var dataToSend = {' . $JSButtonAttributesString . '};
                                
                                $.post(KXJS.buildAPICallUrl("' . $kcall . '","' . $kcp . '","' . $kco . '","' . $kca . '"), 
                                    dataToSend,
                                    function(data) {
                                        waitingDialog.hide();
                                        var r = $.parseJSON(data);
                                        
                                        ' . $JSCommonPostSubmit  . '

                                    }
                                 );
                                };
							$(document).ready(function(){
							
							    $("#' . $id . '").click(function () {
									    ' . $id . '_functionSubmit($(this));
									});

							});
						</script>');


                    }
                break;


                case "general":
                    $this->addFooterJS('<script>'
                        . '$(document).ready(function(){
								
								$("#' . $id . '").click(function () {
								        var r = {"success" : "true"};
									    ' . ($functionPreSubmit != "" ? $functionPreSubmit . "();" : "") . '
								        ' . $JSCommonPostSubmit . '
									});
							});
						</script>');
                break;
        }



        //Check for disable button IF
        if (is_array($disableIF))
        {
            $type = $this->K_COMMON->getVarArray($disableIF,"type");
            $field = $this->K_COMMON->getVarArray($disableIF,"field");
            $word = $this->K_COMMON->getVarArray($disableIF,"word");
            $scriptString = "";
            $triggerString = "";

            if ($type == "different")
            {
                $scriptString .= '
                            $("#' . $id . '").attr("disabled","disabled");
                            t.buttonCheck_'.$id.' = function(){
                               if($(this).val() == "'.$word.'")
                                        $("#' . $id . '").removeAttr("disabled");
                                    else
                                        $("#' . $id . '").attr("disabled","disabled");
                            }
                            
                           
                        ';

                $triggerString .= '$("#'.$field.'").keyup(t.buttonCheck_'.$id.');';
            }


            $jsUniqueID = "kx_btncheck_script_" . $id;
            $this->addFooterJS('
                    <script>
                    ' . $jsUniqueID . ' = {};
                    (function() {
                        var t = this;
                        
                        this.init = function(){
                            $(document).ready(t.ready);
                        };
                        this.ready = function(){
                            t.assignTrigger();
                        }
                                            
                        ' . $scriptString . '
                        
                        
                        t.assignTrigger = function(){
                                             
                                                        
                            ' . $triggerString .'
                        }
        
                        this.init();
        
                    }).apply(' . $jsUniqueID . ');
                    </script>
                ');

        }





    }


    /**
     * create a button (button or button submit)
     *
     * @param array $arrayParam array of input parameters.<br>
     *
     *  Parameters: <br>
     *
     * *    type    : type of button
     * *    name    : name of button
     * *    id    : optional id of button
     * *    value    : value of the button (label)
     * *    title    : title of the button
     * *    class    : optional class of the button
     * *    style   : optional style of button
     * *    attributes    : optional attributes of the button
     * *    link    : optional href of the button
     * *    linkTarget: optional target for link (default _self)
     * *    required    : optional, if == 1 set input as required
     * *    size    : optional "sm" for small, "lg" for big, empty for normal
     * *    icon    : optional font icon
     * *    label    : optional upper button label
     * *    emptyLabel: optional 1 for set an empty label
     * *    semanticColor    : optional set color of button by bootstrap semantic purpose (primary, secondary, success, info, warning, danger, link)
     * *    outlineColor    : optional if == 1 create an outline button color
     * *    dictionary: array of dictionary
     * *    toggle    : enable toggle
     * *    onoff   : 1 for on (for now is used only for toggle)
     * *    openModal: array of parameters for open model
     * *        idModal: id of modal
     * *        kcp: kcp of ajax
     * *        kco: kco of ajax
     * *        kca: kca of ajax
     * *        parameters: parameters for ajax
     * *        parametersRef: parameters for ajax to get from page by ID
     * *        onClose: function to call on close modal
     *
     * @return html generated
     */
    protected function getButton($arrayParam = array())
    {
        // get variables
        $type = $this->K_COMMON->getVarArray($arrayParam, "type");
        $baseURL = $this->K_COMMON->getVarArray($arrayParam, "baseURL");
        $name = $this->K_COMMON->getVarArray($arrayParam, "name");
        $ID = $this->K_COMMON->getVarArray($arrayParam, "id");
        $value = $this->K_COMMON->getVarArray($arrayParam, "value");
        $title = $this->K_COMMON->getVarArray($arrayParam, "title");
        $class = $this->K_COMMON->getVarArray($arrayParam, "class");
        $style = $this->K_COMMON->getVarArray($arrayParam, "style");
        $attributes = $this->K_COMMON->getVarArray($arrayParam, "attributes");
        $link = $this->K_COMMON->getVarArray($arrayParam, "link");
        $linkTarget = $this->K_COMMON->getVarArray($arrayParam, "linkTarget");
        $required = $this->K_COMMON->getVarArray($arrayParam, "required");
        $size = $this->K_COMMON->getVarArray($arrayParam, "size");
        $icon = $this->K_COMMON->getVarArray($arrayParam, "icon");
        $blocked = $this->K_COMMON->getVarArray($arrayParam, "blocked");
        $label = $this->K_COMMON->getVarArray($arrayParam, "label"); 
        $emptyLabel = $this->K_COMMON->getVarArray($arrayParam, "emptyLabel");
        $semanticColor = $this->K_COMMON->getVarArray($arrayParam, "semanticColor");
        $outlineColor = $this->K_COMMON->getVarArray($arrayParam, "outlineColor");//if want color only line and text of btn
        $dictionary = $this->K_COMMON->getVarArray($arrayParam, "dictionary");
        $openModal = $this->K_COMMON->getVarArray($arrayParam, "openModal");
        $showAuth = $this->K_COMMON->getVarArray($arrayParam, "showAuth");
        $toggle = $this->K_COMMON->getVarArray($arrayParam, "toggle");
        $onoff = $this->K_COMMON->getVarArray($arrayParam, "onoff");
        $attributesString = "";

        // check ID
        if ($ID == "")
            $ID = uniqid();

        //check authorization
        if (is_array($showAuth))
            if (!$this->isAuthorizedCheck($showAuth))
                return "";
        
        //Translate Value dictionary
        $value = $this->convertLabelDictionary($value, $dictionary);

        //check attributes
        if (is_array($attributes)) {
            foreach ($attributes as $key => $val) {
                $attributesString .= " " . $key . "='" . $val . "''";
            }
        } else
            $attributesString = $attributes;

        //check toggle
        if ($toggle == "1")
        {
            $this->addExternalPlugin("button-toggle");
            $ariaPressed = ($onoff == "1" ? "true" : "false");
            $class .= ($onoff == "1" ? " active" : "");
            $attributesString .= ' data-toggle="button" aria-pressed="' . $ariaPressed . '" autocomplete="off"  ';
            $class .= " btn-toggle";
            $value = '<div class="handle"></div>';
        }

        // set button size
        switch ($size) {
            case "sm":
                $class .= " btn-sm";
                break;
            case "lg":
                $class .= " btn-lg";
                break;
        }

        //style
        if ($style != "")
            $attributesString .= ' style = "'.$style.'"';

        // set semantic button color
        if ($semanticColor != "") {
            $class .= " btn-" . ($outlineColor == "1" ? "outline-" : "") . $semanticColor;
        }

        // check required parameter
        if ($required == "1")
            $attributesString .= ' required="" ';

        // check for icon
        if ($icon != "")
            $value = '<i class="fa ' . $icon . '"></i>' . chr(32) . $value;

        //check for block button
        if ($blocked == "1")
            $class .= " btn-block";

        //create script for modal if required

        if (is_array($openModal)) {
            $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");
            $openModal["triggerID"] = $ID;
            $openModal["triggerEvent"] = "click";
            $webModal->newModalJS($openModal);
            $this->mergeObjectVariables($webModal);
        }

        //check for label
        if ($emptyLabel == "1")
            $label = "&nbsp;";


        // create html of button
        if ($link == "")
            $objHTML = '<button
						type="' . $type . '"
						id="' . $ID . '"
						title="' . $title . '"
						class="btn ' . $class . '"
						name="' . $name . '"
						' . $attributesString . '>
							' . $value . '
					</button>' . chr(13);
        else {
            $link = str_replace("[baseURL]/", $baseURL, $link);
            $objHTML = '<a id="' . $ID . '" title="' . $title . '" name="' . $name . '" href="' . $link . '" class="btn ' . $class . '" ' . $attributesString . ' target="'.($linkTarget != "" ? $linkTarget : "_self").'">' . $value . '</a> ';
        }

        //check for label
        if ($label != "")
            $objHTML = '<label for="' . $ID . '">' . $label . '</label>' . '<div>' . $objHTML . '</div>';

        // return html generated
        return ["html" => $objHTML, "id" => $ID];
    }


    /**
     * create label for input
     *
     * @param array $arrayParam array of input parameters.<br>
     *
     *  Parameters: <br>
     *
     * *    for    : id of input refer
     * *    value    : optional value of label
     *
     *
     * @return html generated
     */
    protected function getLabel($arrayParam = array())
    {
        // get variables
        $for = $this->K_COMMON->getVarArray($arrayParam, "for");
        $value = $this->K_COMMON->getVarArray($arrayParam, "value");

        // check if value = ""
        if ($value == "")
            $value = "&nbsp;";


        // create html label
        $objHTML = "<label for=\"" . $for . "\">" . $value . "</label>";
        return $objHTML;


    }

    /**
     * append label
     *
     */
    protected function appendLabel($idToAppend, $arrayParam = array())
    {
        // create html label
        $objHTML = $this->getLabel($arrayParam);
        $this->appendHTML($idToAppend, $objHTML);


    }


    /**
     * create options of an input select
     *
     * @param array $arrayParam array of input parameters.<br>
     *
     *  Parameters (general): <br>
     *
     * *    id    : id of the combo
     * *    optionAttributes    : extra attributes for the options
     * *    typeLoad    : type of load (array, function,  ajax, data)
     * *    value    : optional default value
     * *    firstValueCall: optional the value for first ajax call - may be an array
     * *    autoSelect: optional 1 or 0 if 1 select the first element if only 1 element in the list
     * *    noEmptyOption: if 1 not show first option (empty option)
     *
     * Parameters (type load "array"): <br>
     *
     * *    options    : array with value/label option (Ex: array("1" => "Red", "2" => "Yellow") )
     *
     *  Parameters (type load "table") - DEPRECATED: <br>
     *
     * *    pluginName    : name of plugin that load data
     * *    pluginObject    : name of plugin object that load data
     * *    functionName    : function name to call
     * *    optionValue    : optional name of table field
     * *    optionText    : optional name of table field
     * *    triggerEvent    : optional event for trigger function
     * *    triggerFunction    : optional js function to call if trigger event
     * *    triggerFunctionBefore    : optional js function executed before trigger
     * *    triggerFunctionAfter    : optional js function executed After trigger
     *
     *  Parameters (type load "function"): <br>
     * *    pluginName    : name of plugin that load data
     * *    pluginObject    : name of plugin object that load data
     * *    functionName    : function name to call
     *
     *  Parameters (type load "data"): <br>
     * *    pluginName    : name of plugin that load data
     * *    pluginData    : name of plugin data
     * *    functionName    : function name to call
     *
     * <br>
     * The function must return an array so formatted:<br>
     * <pre>
     *    [
     *        [0] => ["value" => "IT", "text" => "Italia"]
     *        [1] => ["value" => "GB", "text" => "United Kingdom"]
     *  ]
     * </pre>
     * <br>
     * You can add others fields, they will be added as option attributes
     *
     * Parameters (type load "ajax"): <br>
     *
     * *    pluginName    : name of plugin that load data
     * *    pluginObject    : name of plugin object that load data
     * *    pluginAction    : name of plugin action that load data
     * *    ajaxParameters    : optional json parameters for ajax call
     * *    optionValue    : optional name of field
     * *    optionText    : optional name of field
     *
     * Parameters (type load "ajax"): <br>
     * Extra paramameter if you want load data when change data on other select
     *
     * *    loadTrigger    : optional the trigger action (ex: "change")
     * *    loadTriggerRef    : optional the dom id of select where keep the value to send in ajax call - may be an array
     * *    parameterRef    : name of parameter to send in ajax call with value selected (ex: "code") - may be an array
     * *    reloadOnFocus : if 1 execute ajax call when click on combo
     *
     * @return html generated
     */
    protected function loadOptionSelect($arrayParam = array())
    {
        // get general variables
        $ID = $this->K_COMMON->getVarArray($arrayParam, "id");
        $typeLoad = $this->K_COMMON->getVarArray($arrayParam, "typeLoad");
        $attributes = $this->K_COMMON->getVarArray($arrayParam, "optionAttributes");
        $valueSelected = $this->K_COMMON->getVarArray($arrayParam, "value");
        $firstValueCall = $this->K_COMMON->getVarArray($arrayParam, "firstValueCall");
        $autoSelect = $this->K_COMMON->getVarArray($arrayParam, "autoSelect");
        $triggerEvent = $this->K_COMMON->getVarArray($arrayParam, "triggerEvent");
        $triggerFunction = $this->K_COMMON->getVarArray($arrayParam, "triggerFunction");
        $triggerFunctionBefore = $this->K_COMMON->getVarArray($arrayParam, "triggerFunctionBefore");
        $triggerFunctionAfter = $this->K_COMMON->getVarArray($arrayParam, "triggerFunctionAfter");
        $noEmptyOption = $this->K_COMMON->getVarArray($arrayParam, "noEmptyOption");
        $showEmptyOption = true;

        //check variables
        if ($noEmptyOption == "1")
            $showEmptyOption = false;

        // get load by "array" variables
        $options = $this->K_COMMON->getVarArray($arrayParam, "options");
        //$options = json_decode($options);

        // get load by "table"/"ajax"/"function"/"data" common variables
        $pluginName = $this->K_COMMON->getVarArray($arrayParam, "pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($arrayParam, "pluginObject");


        // get load by "table"/"function" common variables
        $functionName = $this->K_COMMON->getVarArray($arrayParam, "functionName");

        // get load by "function" common variables
        $functionVariables = $this->K_COMMON->getVarArray($arrayParam, "functionVariables");

        // get load by "data" common variables
        $pluginData = $this->K_COMMON->getVarArray($arrayParam, "pluginData");
        $dataOrder = $this->K_COMMON->getVarArray($arrayParam, "dataOrder");
        $dataPrototype = $this->K_COMMON->getVarArray($arrayParam, "dataPrototype");

        // get load by "table" variables
        $optionValue = $this->K_COMMON->getVarArray($arrayParam, "optionValue");
        $optionText = $this->K_COMMON->getVarArray($arrayParam, "optionText");

        // get load by "ajax" variables
        $pluginAction = $this->K_COMMON->getVarArray($arrayParam, "pluginAction");
        $ajaxParameters = $this->K_COMMON->getVarArray($arrayParam, "ajaxParameters");
        $parameterRef = $this->K_COMMON->getVarArray($arrayParam, "parameterRef");
        $loadTrigger = $this->K_COMMON->getVarArray($arrayParam, "loadTrigger");
        $loadTriggerRef = $this->K_COMMON->getVarArray($arrayParam, "loadTriggerRef");
        $reloadOnFocus = $this->K_COMMON->getVarArray($arrayParam, "reloadOnFocus");

        // set variables
        $objHTML = "";
        $scriptLoader = "";
        $triggerJSFunction = "";
        $triggerJSFunctionClick = "";
        $triggerScript = "";
        $triggerScriptClick = "";

        // check variables
        if ($typeLoad == "")
            return array("success" => "false");

        //check for attributes
        if ($typeLoad != "ajax")
            if (is_array($attributes))
            {
                $attributesString = "";
                foreach ($attributes as $k => $v)
                    $attributesString .= $k . '="'.$v.'"';
                $attributes = $attributesString;
            }

        // switch by typeLoad
        switch ($typeLoad) {
            case "array":

                // check if options is array
                if (!is_array($options))
                    if (is_string($options))
                        $options = json_decode($options, true);
                    else
                        break;

                // create first empty option
                if ($showEmptyOption)
                    $objHTML .= $this->getSelectOption(array("value" => "", "option" => "", "attributes" => $attributes));

                //add options to select
                $objHTML .= $this->getSelectOptionByArray($options, $valueSelected, $autoSelect, $attributes);

                break;
            case "function":

                $arrayOptions = [];
                if (!($pluginName == "" || $functionName == "")) {
                    // get plugin where load data
                    $objPlugin = $this->K_COMMON->getPluginObject($pluginName, $pluginObject);

                    if (!$objPlugin === false)
                    {
                        //check variables
                        if (!is_array($functionVariables))
                            $functionVariables = [];
                        $functionVariables["OM"] = "array";

                        //load data
                        $arrayOptions = $objPlugin->$functionName($functionVariables);

                        // create first empty option
                        if ($showEmptyOption)
                            $objHTML .= $this->getSelectOption(array("value" => "", "option" => "", "attributes" => $attributes));
                    }


                }

                //add options to select
                $objHTML .= $this->getSelectOptionByArray($arrayOptions, $valueSelected, $autoSelect, $attributes);

                break;
            case "data":

                $arrayOptions = [];
                if (!($pluginName == "" || $pluginData == "")) {
                    //get plugin data
                    $objPlugin = $this->K_COMMON->getPluginDataObject($pluginName, $pluginData);

                    //check variables
                    if (!is_array($functionVariables))
                        $functionVariables = [];
                    $functionVariables["OM"] = "array";
                    if (empty($dataOrder))
                        $dataOrder = [];
                    if (empty($dataPrototype))
                        $dataPrototype = "";

                    // load data

                    if ($dataPrototype == "")
                        $objPlugin->get(["value" => $optionValue, "text" => $optionText], "", "", "", $dataOrder);
                    else
                        $objPlugin->getByQuery($dataPrototype, "", "", "", "", $dataOrder);
                    $arrayOptions = $objPlugin->getResults("array");

                    // create first empty option
                    if ($showEmptyOption)
                        $objHTML .= $this->getSelectOption(array("value" => "", "option" => "", "attributes" => $attributes));
                }
                //add options to select
                $objHTML .= $this->getSelectOptionByArray($arrayOptions, $valueSelected, $autoSelect, $attributes);

                break;
            case "ajax":

                if (is_array($attributes))
                {
                    $attributesString = "";
                    foreach ($attributes as $k)
                        {
                            if ($attributesString == "")
                                $attributesString .= '"'.$k.'"';
                            else
                                $attributesString .= ', "'.$k.'"';
                        }
                    $attributes = ($attributesString != "" ? "[" . $attributesString . "]" : "");
                }

                if (!is_array($ajaxParameters))
                {
                    if (is_string($ajaxParameters) && (!empty($ajaxParameters)))
                    {
                        $ajaxParameters = (array)json_decode($ajaxParameters, true);
                        $ajaxParameters = json_encode($ajaxParameters);
                    }
                    else
                        $ajaxParameters = "{}";

                }
                else
                    $ajaxParameters = json_encode($ajaxParameters);


                //check name of fields
                if ($optionValue == "")
                    $optionValue = "value";
                if ($optionText == "")
                    $optionText = "text";
                if ($loadTrigger == "")
                    $loadTrigger = "change";
                $scriptStringValue = "";
                $scriptStringParameters = "";
                $scriptStringFirstValueCall = "";
                $scriptStringFirstValueCallCheck = "";

                if (is_array($loadTriggerRef))
                {
                    $k = 0;
                    foreach ($loadTriggerRef as $t)
                    {
                        $k++;
                        $scriptStringValue .= 'var value'.$k.' = $("' . $t . '").val();';
                        $triggerJSFunction .= '$("' . $t . '").' . $loadTrigger . '(t.trigger);';
                    }
                    $k = 0;
                    foreach ($parameterRef as $p)
                    {
                        $k++;
                        $scriptStringParameters .= 'parameters.' . $p . ' = value'.$k.';';
                    }
                    if (!empty($firstValueCall))
                    {
                        $k = 0;
                        foreach ($firstValueCall as $v)
                        {
                            $k++;
                            $scriptStringFirstValueCall .= 'value'.$k.' = "' . $v . '";';
                            $scriptStringFirstValueCallCheck .= " || value" . $k . ' == ""  || typeof(value'.$k.') == "undefined" || !value'.$k;
                        }
                    }



                }
                else
                {
                    if ($loadTriggerRef != "") {
                        $scriptStringValue = 'var value1 = $("' . $loadTriggerRef . '").val();';
                        $scriptStringParameters = 'parameters.' . $parameterRef . ' = value1;';
                        $scriptStringFirstValueCall = 'value1 = "' . $firstValueCall . '";';
                        $triggerJSFunction = '$("' . $loadTriggerRef . '").' . $loadTrigger . '(t.trigger);';
                    }


                }
                if ($reloadOnFocus == "1")
                {
                    $triggerJSFunctionClick = '$("#' . $ID . '").focus(t.triggerClick);';
                    $triggerScriptClick = '
                    
               
                t.trigger();
                    
                    ';


                }


                //replace "\" on kco with double "\" for javascript
                $pluginObjectJS = str_replace("\\", "\\\\", $pluginObject);
                $triggerScript = '
            
							var parameters = ' . $ajaxParameters . ';
							'. $scriptStringValue .'
							
							var valueToSet = "";
							

                            if (t.startValueToSet != "") {
                            
								valueToSet = t.startValueToSet;
								
							}
							
							
							'.($scriptStringFirstValueCall != "" ? '
							if (t.firstCall == 1) 
							{
                                if ((value1 == "") || (typeof(value1) == "undefined") || !value1'.$scriptStringFirstValueCallCheck.')
                                        {
                                        '.$scriptStringFirstValueCall.'
                                        }
							}
							' :'').'
							
							
							'.($loadTriggerRef != "" ? '
                                if ((value1 == "") || (typeof(value1) == "undefined") || !value1){
                                    $("#' . $ID . '").empty();
                                    return;
                                }
                                ' : '')
                    .'
							
							
							
							
                               '.$scriptStringParameters.'                            
							
							t.firstCall = 0;
							t.startValueToSet = "";
							
							KXJS.loadDataCombo({
								plugin:"' . $pluginName . '"
								,pluginObject:"' . $pluginObjectJS . '"
								,pluginAction:"' . $pluginAction . '"
								,parameters: parameters
								,comboID:"#' . $ID . '"
								,optionValue:"' . $optionValue . '"
								,optionText:"' . $optionText . '"
								,optionSelected: valueToSet
								'.
                    ($triggerFunctionAfter != "" ? ',done: ' . $triggerFunctionAfter : '')
                    .($attributes != "" ? ',optionAttributes: ' . $attributes : '')
                    .'
								
							});
							';





                break;
        }



        //check if trigger to manage
        if ($triggerEvent != "") {
            $triggerJSFunction = '
				$("#' . $ID . '").' . $triggerEvent . '(t.trigger);';

        }

        if ($triggerFunction != "")
            $triggerScript = $triggerFunction . '();';

        if ($triggerFunctionBefore != "")
            $triggerScript = $triggerFunctionBefore . "();" . $triggerScript;



        //check if write script
        if ($triggerScript != "") {
            $jsUniqueID = "kx_input_select_" . uniqid();
            $this->addFooterJS('<script>
			' . $jsUniqueID . ' = {};
			(function() {
			
				var t = this;
				this.startValueToSet = "' . $valueSelected . '";
				
				this.firstCall = 1;
				this.init = function(){
					$(document).ready(t.ready);
				};
				this.trigger = function(){' . $triggerScript . '}
				'.($reloadOnFocus == "1" ? '
				    this.triggerClick = function(){' . $triggerScriptClick . '}
				' : '') .'
				this.ready = function(){
                    ' . ($typeLoad == "ajax" ? 't.trigger();' : '')
                . $triggerJSFunction
                . $triggerJSFunctionClick
                . '
				}

				this.init();

			}).apply(' . $jsUniqueID . ');

		</script>');
        }

        return array("success" => "true", "html" => $objHTML, "scriptLoader" => $scriptLoader);
    }

    protected function getSelectOptionByArray($arrayOptions, $valueSelected, $autoSelect, &$attributes)
    {

        //set variables
        $objHTML = "";

        //check variables
        if (!is_array($arrayOptions))
            return "";

        //Array Multidimensional
        if (is_array(current($arrayOptions))) {

            //set variables
            $countTot = count($arrayOptions);
            $count = 0;
            $selectFirst = "0";

            //check for auto Select
            if ($autoSelect == "1" && $countTot == 1)
                $selectFirst = "1";

            // scan array and add option

            foreach ($arrayOptions as $arrayOption) {
                $localAttributes = "";

                foreach ($arrayOption as $nameField => $valueField) {
                    $count++;
                    switch ($nameField) {
                        case "value":
                            $value = $valueField;
                            if ($value == $valueSelected) {
                                $selected = " selected='selected'";
                            } else {
                                if ($selectFirst == "1" && $count == 1) {
                                    $selected = " selected='selected'";
                                } else
                                    $selected = "";
                            }
                            break;
                        case "text":
                            $option = $valueField;
                            break;
                        default:
                            $localAttributes .= " " . $nameField . '="' . $valueField . '"';
                            break;
                    }

                }

                $objHTML .= $this->getSelectOption(array("value" => $value, "option" => $option, "attributes" => $attributes . $localAttributes . $selected));
            }
        } else //Simple Array
        {
            // scan array and add option
            foreach ($arrayOptions as $key => $value) {
                if ($key == $valueSelected) {
                    $selected = " selected='selected'";
                } else {
                    $selected = "";
                }
                $objHTML .= $this->getSelectOption(array("value" => $key, "option" => $value, "attributes" => $attributes . $selected));
            }
        }
        return $objHTML;
    }

    protected function getSelectOption($arrayParam = array())
    {
        $value = $this->K_COMMON->getVarArray($arrayParam, "value");
        $option = $this->K_COMMON->getVarArray($arrayParam, "option");
        $attributes = $this->K_COMMON->getVarArray($arrayParam, "attributes");

        $objHTML = '<option value="' . $value . '" ' . $attributes . '>' . htmlentities($option) . '</option>';
        return $objHTML;
    }


    /**
     * append a general input (text, email, password etc)
     *
     * @param array $arrayParam array of input parameters.<br>
     *
     *  Parameters: <br>
     *
     * *    type    : type of html input (text, email etc)
     * *    name    : name of input (it's used for create input hidden)
     * *    id    : optional id of input
     * *    value    : optional value of the input
     * *    class    : optional class of the input
     * *    attributes    : optional attributes of the input
     * *    labelPlaceholder    : optional label of the input placeholder
     * *    labelInput    : label of input
     * *    textHelp    : the Text Help is positioned under the input
     * *    noLabel    : if 1 not create label tag
     * *    containerClass    : the class of container
     * *    required    : set input as required
     * *    addIcon    : icon class added as input group addon to the main input
     * *    size    : "sm" for small
     * *    multiple    : set the name of field as array.
     * *    disabled: optional 1 or 0
     *
     *
     */
    protected function appendGeneralInput($idToAppend, $arrayParam = array())
    {

        // set variables
        $type = $this->K_COMMON->getVarArray($arrayParam, "type");
        $name = $this->K_COMMON->getVarArray($arrayParam, "name");
        $ID = $this->K_COMMON->getVarArray($arrayParam, "id");
        $value = $this->K_COMMON->getVarArray($arrayParam, "value");
        $class = $this->K_COMMON->getVarArray($arrayParam, "class");
        $attributes = $this->K_COMMON->getVarArray($arrayParam, "attributes");
        $labelPlaceholder = $this->K_COMMON->getVarArray($arrayParam, "labelPlaceholder");
        $multiple = $this->K_COMMON->getVarArray($arrayParam, "multiple");
        $required = $this->K_COMMON->getVarArray($arrayParam, "required");
        $addIcon = $this->K_COMMON->getVarArray($arrayParam, "addIcon");
        $addGroup = $this->K_COMMON->getVarArray($arrayParam, "addGroup");
        $size = $this->K_COMMON->getVarArray($arrayParam, "size");
        $disabled = $this->K_COMMON->getVarArray($arrayParam, "disabled");

        // check size
        if ($size == "sm")
            $class .= " form-control-sm";

        // verify if required is == 1
        if ($required == "1")
            $attributes .= ' required="" ';

        // if is set "multiple" add array at input name
        if ($multiple == "1") {
            $ID .= "[]";
            $arrayParam["id"] = $ID;
            if ($name != "") {
                $name .= "[]";
                $arrayParam["name"] = $name;
            }
        }

        //check if disabled
        if ($disabled == "1")
            $attributes .= ' disabled="disabled" ';

        // set attribute "placeholder"
        if ($labelPlaceholder != "")
            $attributes .= ' placeholder="' . $labelPlaceholder . '"';

        //create final HTML of the input requested
        $value = htmlspecialchars($value, ENT_COMPAT);
        $objHTML =
            '<input type="' . $type . '" id="' . $ID . '" name="' . $name . '" class="form-control ' . $class . '"'
            . ' value="' . $value . '"' . $attributes . ' />';

        if ($addIcon != "")
            $objHTML =
                '<div id="addonGroup_' . $ID . '" class="input-group' . ($size == "sm" ? " input-group-sm" : "") . '">'
                . $objHTML
                . ' <span class="input-group-addon btn"><i id="' . $ID . 'Icon" class="fa ' . $addIcon . '"></i></span></div>';
        elseif ($addGroup != "")
            $objHTML =
                '<div id="addonGroup_' . $ID . '" class="input-group' . ($size == "sm" ? " input-group-sm" : "") . '">'
                . $objHTML
                . ' <span class="input-group-addon btn">' . $addGroup . '</span></div>';

        // append object of input
        $this->appendHTML($idToAppend, $objHTML);

    }


    /**
     * create Field container with all elements requested
     *
     * @since 1.00
     * @access protected
     *
     * @param array $arrayParam array for input paraeters.<br>
     * Please refer to each input for correct parameters.<br>
     * If you use on template, you can pass the parameters on "lns_form_variable" variable.
     *
     *  Mandatory Parameters: <br>
     *
     * *    id    : id of input
     * *    name    : name of input (it's used for create input hidden)
     * *    labelInput    : label of input
     * *    textHelp    : the Thext Help is positioned under the input
     * *    noLabel    : if 1 not create label tag
     * *    containerClass    : the class of container
     * *    inputHTML    : HTML of input to place on container
     * *    noFieldSet    : not show fieldset tag
     * *    noContainer    : not show div container class
     *
     * @return html generated
     */
    protected function getFieldContainer($arrayParam = [])
    {


        //get variables of input
        $ID = $this->K_COMMON->getVarArray($arrayParam, "id");
        $labelInput = $this->K_COMMON->getVarArray($arrayParam, "labelInput");
        $noLabel = $this->K_COMMON->getVarArray($arrayParam, "noLabel");
        $containerClass = $this->K_COMMON->getVarArray($arrayParam, "containerClass");
        $containerStyle = $this->K_COMMON->getVarArray($arrayParam, "containerStyle");
        $noFieldSet = $this->K_COMMON->getVarArray($arrayParam, "noFieldSet");
        $noContainer = $this->K_COMMON->getVarArray($arrayParam, "noContainer");
        $inputHTML = $this->K_COMMON->getVarArray($arrayParam, "inputHTML");
        $textHelp = $this->K_COMMON->getVarArray($arrayParam, "textHelp");
        $objHTMLTextHelp = ""; // little description under field
        $objHTMLHiddenField = ""; // an input hidden linked to the main input
        $objHTMLFieldInputContainerBefore = "";
        $objHTMLFieldInputContainerAfter = "";

        // check ID
        if ($ID == "")
            $ID = uniqid();


        //check variable
        if ((!isset($arrayParam["containerClass"]) && $containerClass == ""))
            $containerClass = "form-group";

        // set field sub description (if requested)
        if ($textHelp != "")
            $objHTMLTextHelp = '<label class="small text-muted">' . $textHelp . '</label>';


        if ($noFieldSet == "1") {
            $objHTMLFieldsetBefore = "";
            $objHTMLFieldsetAfter = "";

        } else {
            $IDContainer = "fieldset_" . $ID;
            $objHTMLFieldsetBefore = "<fieldset id=\"" . $IDContainer . "\">";
            $objHTMLFieldsetAfter = "</fieldset>";
            $this->setIDObject($IDContainer);
            $this->setIDContent($IDContainer);
        }
        if ($noContainer == "1") {
            $objHTMLFieldContainerBefore = "";
            $objHTMLFieldContainerAfter = "";
        } else {
            $IDContainer = "container_" . $ID;
            $IDContainerInput = "container_in_" . $ID;
            $objHTMLFieldContainerBefore = '<div id="' . $IDContainer . '" class="' . $containerClass . '" ' . ($containerStyle != "" ? 'style="' . $containerStyle . '"' : '') . '>';
            $objHTMLFieldInputContainerBefore = '<div id="' . $IDContainerInput . '">';
            $objHTMLFieldInputContainerAfter = '</div>';
            $objHTMLFieldContainerAfter = "</div>";
            $this->setIDObject($IDContainerInput); //overwrite the fieldset ID only if show container
            $this->setIDContent($IDContainerInput);//overwrite the fieldset ID only if show container
        }


        // set label (set noLabel = 1 if don't want label)
        if ($noLabel != "1")
            $objHTMLLabel = $this->getLabel(array("for" => $ID, "value" => $labelInput)) . chr(13);
        else
            $objHTMLLabel = "";

        // create final html for the input
        $objHTML = '
				' . $objHTMLFieldsetBefore
            . $objHTMLFieldContainerBefore
            . $objHTMLFieldInputContainerBefore
            . $objHTMLLabel
            . $inputHTML
            . $objHTMLFieldInputContainerAfter
            . $objHTMLTextHelp
            . $objHTMLHiddenField
            . $objHTMLFieldContainerAfter
            . $objHTMLFieldsetAfter;


        // return
        return $objHTML;
    }

    /**
     * append Field container
     *
     * @since 1.00
     * @access protected
     *
     * @param array $arrayParam array for input paraeters.<br>
     *
     */
        protected function appendFieldContainer($idToAppend, $arrayParam = [])
    {

        //Append container
        $this->appendHTML($idToAppend, $this->getFieldContainer($arrayParam));
    }
}

?>


