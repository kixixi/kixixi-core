<?
/**
 * KIXIXI HTML Components (photo)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage objects\photo
 * @since 1.00
 */
namespace k_htmlcomponents\objects\general\photo;

class obj extends \k_webmanager\objects\general\general_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";


    /**
     * get an HR
     *
     * @since 1.00
     * @access public
     *
     * @param array $arrayParam array with parameters for create Form
     *
     *  The array of form support this values<br>
     *
     * *    id    : optional id of the form
     * *    iFileName    : name of input file
     * *    hiddenName    : optional name for a variable to send with file (ex: "keyID")
     * *    hiddenValue    : optional value of variable to send with file
     * *    onFileChange    : optional javascript function to call when change file name
     * *    onRotate    : optional javascript function to call when Rotate Img
     * *    onRemove    : optional javascript function to call when Remove Img
     * *    label    : optional label for photo
     * *    src    : optional url of actual photo (if exist)
     * *    kcp    : plugin name for ajax call after on change image
     * *    kco    : plugin object name for ajax call after on change image
     * *    kco    : plugin action for ajax call after on change image
     * *    showPlus    : optional show PLUS button
     * *    showEdit    : optional show EDIT button
     * *    showRotate    : optional show ROTATE buttons
     * *    showRemove    : optional show REMOVE button
     *
     *
     * @return webobject
     */
    public function get($arrayParam = [])
    {


        //set variables
        $id = $this->K_COMMON->getVarArray($arrayParam, "id");
        $iFileName = $this->K_COMMON->getVarArray($arrayParam, "iFileName");
        $hiddenName = $this->K_COMMON->getVarArray($arrayParam, "hiddenName");
        $hiddenValue = $this->K_COMMON->getVarArray($arrayParam, "hiddenValue");
        $onFileChange = $this->K_COMMON->getVarArray($arrayParam, "onFileChange");
        $onRotate = $this->K_COMMON->getVarArray($arrayParam, "onRotate");
        $onRemove = $this->K_COMMON->getVarArray($arrayParam, "onRemove");
        $label = $this->K_COMMON->getVarArray($arrayParam, "label");
        $src = $this->K_COMMON->getVarArray($arrayParam, "src");
        $kcp = $this->K_COMMON->getVarArray($arrayParam, "kcp");
        $kco = $this->K_COMMON->getVarArray($arrayParam, "kco");
        $kca = $this->K_COMMON->getVarArray($arrayParam, "kca");
        $showPlus = $this->K_COMMON->getVarArray($arrayParam, "showPlus");
        $showEdit = $this->K_COMMON->getVarArray($arrayParam, "showEdit");
        $showRotate = $this->K_COMMON->getVarArray($arrayParam, "showRotate");
        $showRemove = $this->K_COMMON->getVarArray($arrayParam, "showRemove");
        $optionsImage = "";
        $fileButton = "";

        $labelHTML = "";

        //replace "\" on kco with double "\" for javascript
        $kco = str_replace("\\", "\\\\", $kco);

        //check variables
        if ($id == "")
            $id = uniqid();
        if ($label != "")
            $labelHTML = '<div><label for="commercialName">' . $label . '</label></div>';

        // create html string
        $containerID = 'container_photo_' . $id ;
        $this->appendHTML("", $labelHTML.'<div id="' . $containerID . '" class="imgContainer card">'
            . '<img src="" class="imgToShow img-responsive" id="photo_' . $id . '" style="width:100%;"/>
            </div>');

        //Check what to show
        if ($showPlus)
            $optionsImage .= ',"plus":1';
        if ($showEdit)
            $optionsImage .= ',"edit":1';
        if ($showRotate)
        {
            $optionsImage .= ',"rotate":1';
            $optionsImage .= ',"onRotate":' . ($onRotate != "" ? $onRotate : 't.rotateImg');
        }
        if ($showRemove)
        {
            $optionsImage .= ',"remove":1';
            $optionsImage .= ',"onRemove":' . ($onRemove != "" ? $onRemove : 't.removePhoto');
        }
        $optionsImage .= ',"hiddenName":"' . $hiddenName . '","hiddenValue":"' . $hiddenValue . '"';
        $fileButton = '{"iFileName":"' . $iFileName . '","onFileChange":' . ($onFileChange != "" ? $onFileChange : 't.photoChange') . '}';

    
        //Add CSS
        $this->addHeadFile([$this->getPathFileCSS("get")]);

        //Add Javascript
        $jsUniqueID = "kx_photo_" . uniqid();
        $this->addFooterJS('<script>
			' . $jsUniqueID . ' = {};
			(function() {
			
				var t = this;
				
				this.init = function(){
					$(document).ready(t.ready);
				};
				
				t.removePhoto = function(){
                    var manageImg = $(this).closest(".manageImg");
                    var manageImgHidden = manageImg.find(".manageImgHidden");
                    var keyRef  = $("input[name=\'keyRef\']").val();
                    var keyRefValue  = $("input[name=\'keyRefValue\']").val();
                    
                    var dataToSend = {};
                    dataToSend[keyRef] = keyRefValue;
                    
                    
                    waitingDialog.show(\'Removing...\', {dialogSize: \'sm\', progressType: \'warning\'});
                    $.ajax({
                        url: KXJS.buildAPICallUrl("KXCJS","' . $kcp . '","' . $kco . '","' . $kca . '","callType=remove"),
                        data: dataToSend
                    }).done(function (data) {
                        waitingDialog.hide();
                        r = $.parseJSON(data);
                        if (r.success == "true") {
                            KXJS.getDivManageImg({"appendTo":"#'.$containerID.'"'.$optionsImage.', "fileButton" : '.$fileButton.'});
                        }
                    });
                    
                };
				
				t.rotateImg = function(rotation,obj){

                    var manageImg = obj.closest(".manageImg");
                    var manageImgHidden = manageImg.find(".manageImgHidden");
                    var keyRef  = $("input[name=\'keyRef\']").val();
                    var keyRefValue  = $("input[name=\'keyRefValue\']").val();
                    
                    var dataToSend = {};
                    dataToSend.rotation = rotation;
                    dataToSend[keyRef] = keyRefValue;
                    
                    waitingDialog.show(\'Rotating...\', {dialogSize: \'sm\', progressType: \'warning\'});
                    
                    $.ajax({
                            url: KXJS.buildAPICallUrl("KXCJS","' . $kcp . '","' . $kco . '","' . $kca . '","callType=rotate"),
                            data: dataToSend			
                            }).done(function(data) { 
                                r = $.parseJSON(data);
                                waitingDialog.hide();
                                imgSrc = $("#'.$containerID.' img").attr("src");
                                $("#'.$containerID.' img").attr("src","");
                                $("#'.$containerID.' img").attr("src",imgSrc+"?"+ new Date().getTime());
                            });
                };
								
				t.photoChange = function(){

                    var idForm = $(this).parents("form").attr("id");
                    var form = new FormData($(\'#\'+idForm)[0]);
                    
                
                    waitingDialog.show(\'Uploading...\', {dialogSize: \'sm\', progressType: \'warning\'});
                
                    // Make the ajax call
                    $.ajax({	
                        url: KXJS.buildAPICallUrl("KXCJS","' . $kcp . '","' . $kco . '","' . $kca . '","callType=upload"),
                        type: \'POST\',
                        data: form,
                        cache: false,
                        contentType: false,
                        processData: false
                    }).done(function(data) {
                            waitingDialog.hide();
                            r = $.parseJSON(data);
                            if(r.success == "true"){
                                KXJS.getDivManageImg({"src":r.image,"appendTo":"#'.$containerID.'"'.$optionsImage.', "fileButton" : '.$fileButton.'});
                            }
                            else{
                                alert("Upload Error");
                            }
                        });
                } 
				
				this.ready = function(){
                                KXJS.getDivManageImg({"src":"'.$src.'","appendTo":"#'.$containerID.'"'.$optionsImage.', "fileButton" : '.$fileButton.'});
   	                   
                    
				}

				this.init();

			}).apply(' . $jsUniqueID . ');

		</script>');


        // return
        return $this;
    }

}

?>

