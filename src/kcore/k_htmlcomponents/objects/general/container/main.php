<?
/**
 * KIXIXI HTML Components (container)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage objects\container
 * @since 1.00
 */
namespace k_htmlcomponents\objects\general\container;

class obj extends \k_webmanager\objects\general\general_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";


    /**
     * get an HR
     *
     * @since 1.00
     * @access public
     *
     * @return webobject
     */
    public function get($arrayParam = [])
    {


        //set variables
        $id = $this->K_COMMON->getVarArray($arrayParam, "id");
        $class = $this->K_COMMON->getVarArray($arrayParam,"class");
        $style = $this->K_COMMON->getVarArray($arrayParam, "style");
        $attributes = $this->K_COMMON->getVarArray($arrayParam,"attributes");

        //check variables
        if ($id == "")
            $id = "container_" . uniqid();

        //style
        if ($style != "")
            $attributes .= 'style = "'.$style.'"';

        // create html string
        $this->appendHTML("", '<div id="' . $id . '" class="container '.$class.'"'.$attributes.' ></div>');


        // return
        return $this;
    }

}

?>

