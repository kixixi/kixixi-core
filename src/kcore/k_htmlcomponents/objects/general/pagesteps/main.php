<?
/**
 * KIXIXI HTML Components (pagesteps)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage objects\hr
 * @since 1.00
 */
namespace k_htmlcomponents\objects\general\pagesteps;

class obj extends \k_webmanager\objects\general\general_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * Number of total steps
     *
     * @since 1.00
     * @access public
     * @var int
     */
    public $nSteps = 0;

    /**
     * Number of step to start
     *
     * @since 1.00
     * @access public
     * @var int
     */
    public $startStep = 0;

    /**
     * Array of ID Content
     *
     * @since 1.00
     * @access public
     * @var int
     */
    public $idContentByStep = [];

    /**
     * Array of ID Content
     *
     * @since 1.00
     * @access public
     * @var int
     */
    public $idButtonsByStep = [];


    /**
     * Constructor
     *
     * @since 1.00
     *
     */
    function __construct($AV = [])
    {
        parent::__construct();
        $this->create($AV);

        return $this;
    }

    /**
     * get obect
     *
     * @since 1.00
     * @access public
     *
     * @return webobject
     */
    public function get($arrayParam = [])
    {
    }

    /**
     * add Step
     *
     * @since 1.00
     * @access public
     *
     * @return int id of content
     */
    public function addStep($arrayParam = [])
    {

        //set variables
        $step = $this->K_COMMON->getVarArray($arrayParam, "step");
        $form = $this->K_COMMON->getVarArray($arrayParam, "form");
        $btnNext = $this->K_COMMON->getVarArray($arrayParam, "btnNext");
        $btnBack = $this->K_COMMON->getVarArray($arrayParam, "btnBack");
        $title = $this->K_COMMON->getVarArray($arrayParam, "title");
        $style = "";

        //set variables
        $idStep = "pageStep_" . $step; //not change, is used also in form object
        $idStepDiv = $idStep . "_step";
        $idContent = $idStep . "_content";
        $idButtons = $idStep . "_buttons";


        if ($step != $this->startStep)
            $style .= "display:none;";
        if ($title == "")
            $title = "Step";


        // create html string
        $idPageStep = $this->getIDContent();
        $this->appendHTML($idPageStep, '<div id="' . $idStep . '" style="' . $style . '" ></div></div>');

        //Add Step
        $perc = intval(($step / $this->nSteps) * 100);
        $this->appendHTML($idStep, '<div id="' . $idStepDiv . '">'

                .'<div class="alert alert-info" role="alert">
  <strong>'.$title.'</strong> '.$step.'/'.$this->nSteps.'
</div>');

        //create form Step
        if (is_array($form)) {

            $stepObj = $this->appendForm($idStep, $form);
            $this->appendInput($stepObj, ["type" => "hidden", "name" => "pageStep", "value" => $step]);
            $this->appendHTML($stepObj, '<div id="' . $idContent . '"></div>');

        } else {
            $this->appendHTML($idStep, '<div id="' . $idContent . '"></div>');
            $stepObj = $idStep;
        }



        //Add buttons
        if (is_array($btnBack) || is_array($btnNext)) {

            //append container buttons
            $this->appendHTML($stepObj, '<div id="' . $idButtons . '"></div>');

            //NEW ROW
            $this->appendRow($idButtons, ["class" => "m-b-1"]);
            $this->appendColumn($this->lastRow, ["nCol" => 12]);

            //append buttons
            if (is_array($btnBack))
            {
                $btnBack["class"] = "btnBack";
                $this->K_COMMON->concatVarArray($btnBack, "attributes", 'data-nstep="'.$step.'"');
                $this->appendInput($this->lastCol, $btnBack);
            }
            if (is_array($btnNext))
            {
                if (!is_array($form))
                {
                    $btnNext["class"] = "btnNext";
                    $this->K_COMMON->concatVarArray($btnNext, "attributes", 'data-nstep="'.$step.'"');
                }
                $this->appendInput($this->lastCol, $btnNext);
            }

        }

        return $idContent;


    }

    /**
     * create Steps
     *
     * @since 1.00
     * @access public
     *
     */
    function create($arrayParam)
    {
        //set variables
        $nSteps = $this->K_COMMON->getVarArray($arrayParam, "nSteps");
        $startStep = $this->K_COMMON->getVarArray($arrayParam, "startStep");
        $this->nSteps = $nSteps;
        $this->startStep = $startStep;

        //reset variables
        $this->idContentByStep = [];
        $this->idButtonsByStep = [];

        //check variables
        $id = "pageSteps_" . uniqid();

        //set default HTML ID
        $this->setIDObject($id);
        $this->setIDContent($id);


        // create html string
        $this->appendHTML("", '<div id="' . $id . '" ></div></div>');


        $this->addFooterJS('<script>'
            . 'var ' . $id . '_manage = function(){
            
            $("#'.$id.' .btnBack").click(
            function()
            {
            var step = $(this).attr("data-nstep");
            step = parseInt(step);
            $("#'.$id.' #pageStep_"+(step)).hide();
            $("#'.$id.' #pageStep_"+(step-1)).show();
            });
            
            
            $("#'.$id.' .btnNext").click(
            function()
            {
            var step = $(this).attr("data-nstep");
            step = parseInt(step);
            $("#'.$id.' #pageStep_"+(step)).hide();
            $("#'.$id.' #pageStep_"+(step + 1)).show();
            });
            
            };
							$(document).ready(function(){
								' . $id . '_manage();
							});
							
							
							
						</script>'
        );


    }

    /**
     * get id Content of a Step
     *
     * @since 1.00
     * @access public
     *
     * @return webobject
     */
    function getStepIDContent($step)
    {

        return $this->idContentByStep = [$step];


    }

    /**
     * get id Buttons of a Step
     *
     * @since 1.00
     * @access public
     *
     * @return webobject
     */
    function getStepIDButtons($step)
    {

        return $this->idButtonsByStep = [$step];


    }

}

?>

