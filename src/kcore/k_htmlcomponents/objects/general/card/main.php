<?
/**
 * KIXIXI HTML Components (card)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage objects\form
 * @since 1.00
 */
namespace k_htmlcomponents\objects\general\card;

class obj extends \k_webmanager\objects\general\general_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";


    /**
     * get a CARD
     *
     * @since 1.00
     * @access public
     *
     * * @param array $arrayParam array for input paraeters.<br>
     *
     *  Parameters: <br>
     *
     * *    id    : id of card
     * *    class    : class of card
     * *    items    : array of ordered items of card (example: ["block" => ["title" => "Best Articles"],"image-bottom"])
     *   *    block
     *   *    image-top
     *   *    image-bottom

     *
     * @return webobject
     */
    public function get($arrayParam = [])
    {


        //set variables
        $id = $this->K_COMMON->getVarArray($arrayParam, "id");
        $class = $this->K_COMMON->getVarArray($arrayParam, "class");
        $items = $this->K_COMMON->getVarArray($arrayParam, "items");


        //check variables
        if ($id == "")
            $id = "card_" . uniqid();

        //check items
        if (empty($items))
            $items = [];

        $cardItems = "";
        $itemN = 0;

        foreach ($items as $itemCode => $itemVal)
        {
            $itemN ++;
            if (!is_array($itemVal))
            {
                $itemCode = $itemVal;
            }
            switch ($itemCode)
            {
                case "block":
                    $title = $this->K_COMMON->getVarArray($itemVal, "title");
                    $text = $this->K_COMMON->getVarArray($itemVal, "text");
                    $link = $this->K_COMMON->getVarArray($itemVal, "link");

                    $cardItems .= '<div id = "' . $id . '_block_' . $itemN . '" class="card-block">';
                        if ($title != "")
                            $cardItems .= '<h4 class="card-title"><a href="'.($link != "" ? $link : "#") . '">' . $title . '</a></h4>';
                        if ($text != "")
                            $cardItems .= '<p class="card-text">' . $text . '</p>';

                    $cardItems .= '</div>';





                    break;
                case "image-top":
                    $cardItems .= '<img class="card-img-top" data-src="">';
                    break;
                case "image-bottom":
                    $cardItems .= '<img class="card-img-bottom" data-src="">';
                    break;
            }
        }

        $card = '
            <div class="card ' . $class . '" id = "' . $id . '">
            '.$cardItems.'
            </div>
        ';
        // create html string
        $this->appendHTML("", $card);


        // return
        return $this;
    }


}

?>

