<?
/**
 * KIXIXI HTML Components (form)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage objects\form
 * @since 1.00
 */
namespace k_htmlcomponents\objects\general\form;

class obj extends \k_webmanager\objects\general\general_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * Type of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $type = "form";


    /**
     * get a Form
     *
     * @since 1.00
     * @access public
     *
     * @param array $arrayParam array with parameters for create Form
     *
     *  The array of form support this values<br>
     *
     * *    id    : optional id of the form
     * *    class    : optional class of the form
     * *    action : action Form
     * *    attributes    : optional extra form attributes
     * *    html    : optional html to insert in form tag
     * *    inLine: 1 for form In Line
     * *    functionSubmit    : optional javascript function name to call on submit form
     * *    functionPreSubmit    : optional javascript function name to call before submit
     * *    functionPostSubmit    : optional javascript function name to call after submit
     * *    kcall    : optional kcall Ex: "KXCJS" (for submit by ajax call) - if == "KXCDWF" execute download file procedure
     * *    kcp    : optional plugin name (for submit by ajax call)
     * *    kco    : optional plugin objecet name (for submit by ajax call)
     * *    kca    : optional plugin action code (for submit by ajax call)
     * *    dialogMessage    : optional Message for dialog on submit Form (default "Saving...")
     * *    closeModal    : optional the ID of modal to close after submit form (on success)
     * *    openModal: array of parameters for open model (on success = true), all results will be sent to the modal
     * *        idModal: id of modal
     * *        kcp: kcp of ajax
     * *        kco: kco of ajax
     * *        kca: kca of ajax
     * *        parameters: parameters for ajax
     * *        parametersRef: parameters for ajax to get from page by ID
     * *        onClose: function to call on close modal
     * *    reloadTableList    : optional the ID of table to reload (on success)
     * *    reloadTableListParam    : array optional param for "loadTableList" of table lsi
     * *    reloadTableListWithHead    : optional the ID of table to reload (on success)
     * *    reloadPageActual: optional if 1 reload actual page with same URL
     * *    reloadPage: optional if 1 reload page with new ID or new Code EX of URL: get/17
     * *    reloadPageURL: url for reload page (if different from actual
     * *    reloadMethod: REST or GET
     * *    reloadPluginMethod: method for plugin, "get", "put" or your custom method (default "get") - replace "put" method on url
     * *    reloadPageCheckCode: optional Code to check if we are in modify or not
     * *    reloadPageGetCode: optional Code to get from answer for URL GET
     * *    locationURL: optional URL if success after submit
     * *    formMethod: method of form (get, post). Default get
     * *    showFeedback: if 1 or empty show feedback
     * *    showSuccess: if 1 or empty show Success on fields*
     * *    showErrors: if 1 or empty show errors on fields
     * *    resetOnSuccess: reset form on success ajax
     * *    nextStep: if used "pagesteps" object you can specify the next step on success
     * *    setSavedMessage: 1 for set saved message div
     * @return webobject
     */
    public function get($arrayParam = [])
    {


        //set variables
        $id = $this->K_COMMON->getVarArray($arrayParam, "id");
        $action = $this->K_COMMON->getVarArray($arrayParam, "action");
        $class = $this->K_COMMON->getVarArray($arrayParam, "class");
        $inLine = $this->K_COMMON->getVarArray($arrayParam, "inLine");
        $attributes = $this->K_COMMON->getVarArray($arrayParam, "attributes");
        $html = $this->K_COMMON->getVarArray($arrayParam, "html");
        $functionPostSubmit = $this->K_COMMON->getVarArray($arrayParam, "functionPostSubmit");
        $functionPreSubmit = $this->K_COMMON->getVarArray($arrayParam, "functionPreSubmit");
        $functionSubmit = $this->K_COMMON->getVarArray($arrayParam, "functionSubmit");
        $setSavedMessage = $this->K_COMMON->getVarArray($arrayParam, "setSavedMessage");
        $kcall = $this->K_COMMON->getVarArray($arrayParam, "kcall");
        $kcp = $this->K_COMMON->getVarArray($arrayParam, "kcp");
        $kco = $this->K_COMMON->getVarArray($arrayParam, "kco");
        $kca = $this->K_COMMON->getVarArray($arrayParam, "kca");
        $dialogMessage = $this->K_COMMON->getVarArray($arrayParam, "dialogMessage");
        $closeModal = $this->K_COMMON->getVarArray($arrayParam, "closeModal");
        $openModal = $this->K_COMMON->getVarArray($arrayParam, "openModal");
        $reloadTableList = $this->K_COMMON->getVarArray($arrayParam, "reloadTableList");
        $reloadTableListParam = $this->K_COMMON->getVarArray($arrayParam, "reloadTableListParam");
        $reloadTableListWithHead = $this->K_COMMON->getVarArray($arrayParam, "reloadTableListWithHead");
        $resetOnSuccess = $this->K_COMMON->getVarArray($arrayParam, "resetOnSuccess");
        $nextStep = $this->K_COMMON->getVarArray($arrayParam, "nextStep");
        $reloadPageActual = $this->K_COMMON->getVarArray($arrayParam, "reloadPageActual");
        $reloadPage = $this->K_COMMON->getVarArray($arrayParam, "reloadPage");
        $reloadMethod = $this->K_COMMON->getVarArray($arrayParam, "reloadMethod");
        $reloadPluginMethod = $this->K_COMMON->getVarArray($arrayParam, "reloadPluginMethod");
        $reloadPageURL = $this->K_COMMON->getVarArray($arrayParam, "reloadPageURL");
        $locationURL = $this->K_COMMON->getVarArray($arrayParam, "locationURL");
        $reloadPageCheckCode = $this->K_COMMON->getVarArray($arrayParam, "reloadPageCheckCode");
        $reloadPageGetCode = $this->K_COMMON->getVarArray($arrayParam, "reloadPageGetCode");
        $formMethod = $this->K_COMMON->getVarArray($arrayParam, "formMethod");
        $showFeedback = $this->K_COMMON->getVarArray($arrayParam, "showFeedback");
        $showSuccess = $this->K_COMMON->getVarArray($arrayParam, "showSuccess");
        $showErrors = $this->K_COMMON->getVarArray($arrayParam, "showErrors");
        $optionsValidator = "delay:500";
        if ($action == "" && $kcp == "")
            $action = $this->K_COMMON->getCurrentURL();
        if ($dialogMessage == "")
            $dialogMessage = "Saving...";


        //replace "\" on kco with double "\" for javascript
        $kco = str_replace("\\", "\\\\", $kco);

        //check variables
        if ($id == "")
            $id = "form_" . uniqid();
        if ($formMethod == "")
            $formMethod = "get";
        if ($reloadMethod == "")
            $reloadMethod = "REST";
        if ($reloadPage == "1") {
            if ($reloadPluginMethod == "")
                $reloadPluginMethod = "get";
            if ($reloadPageCheckCode == "")
                $reloadPageCheckCode = "keyID";
            if ($reloadPageGetCode == "")
                $reloadPageGetCode = "keyID";
            if ($reloadPageURL == "")
                $reloadPageURL = $this->K_COMMON->getCurrentURI();

            if ($reloadMethod == "REST") {

                $posPut = strpos($reloadPageURL, "/put"); //used for panel
                if ($posPut > 0)
                {
                    $reloadPageURL = substr($reloadPageURL, 0, $posPut);
                }
                else
                {
                    $posPut = strpos($reloadPageURL, "/get"); //used for panel
                    if ($posPut > 0)
                    {
                        $reloadPageURL = substr($reloadPageURL, 0, $posPut);
                    }
                }
                if (substr($reloadPageURL, -1, 1) != "/")
                    $reloadPageURL .= "/";


            } else {
                if (strpos($reloadPageURL, "?") === false)
                    $reloadPageURL .= "?";
            }
        }
        if ($inLine == "1")
            $class .= " form-inline";

        if (is_array($reloadTableListParam))
            $reloadTableListParamJSON = json_encode($reloadTableListParam);
        else
            $reloadTableListParamJSON = "";



        if ($showFeedback == "")
            $showFeedback = "1";
        if ($showErrors == "")
            $showErrors = "1";
        if ($showSuccess == "")
            $showSuccess = "1";


        //Check for validator
        if ($showFeedback == "1")
            $optionsValidator .= ",showFeedback:true";
        else
            $optionsValidator .= ",showFeedback:false";
        if ($showErrors == "1")
            $optionsValidator .= ",showErrors:true";
        else
            $optionsValidator .= ",showErrors:false";
        if ($showSuccess == "1")
            $optionsValidator .= ",showSuccess:true";
        else
            $optionsValidator .= ",showSuccess:false";
        $idSavedMessage = "savedMessage_" . $id;


        // create html string
        $this->appendHTML("",
            '<form action="' . $action . '" method="' . $formMethod . '" id="' . $id . '" '. ($showFeedback == "1" ? 'data-toggle="validator" ' : '') . ' role="form"  class="' . $class . '"' . $attributes . ' >'
            . $html
            . '</form>'
            . ($setSavedMessage == "1" ? $this->getSubmitMessageDiv($idSavedMessage) : ""));

        //set default HTML ID
        $this->lastForm = $this;
        $this->lastObject = $this;
        $this->setIDObject($id);
        $this->setIDContent($id);

        //Check for Open Modal
        if (is_array($openModal)) {
            $idModal = $this->K_COMMON->getVarArray($openModal, "idModal");
            $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");
            $webModal->newModalJS($openModal);
            $this->mergeObjectVariables($webModal);
        }

        // set javascript and css to be included on page
        $this->addExternalPlugin(["validator", "waitingfor"]);
        if ($functionSubmit != "")
            $this->addFooterJS('<script>'
                . '$(document).ready(function(){
								$("#' . $id . '").validator({' . $optionsValidator . '}).on("submit", function (e) {
									if (e.isDefaultPrevented()) {
										} else {
											e.preventDefault();
											' . $functionSubmit . '();
										}
									})
							});
						</script>');
        elseif ($kcp != "") {

            if ($kcall == "")
                $kcall = "KXCJS";

            $reloadTableListScript = "";
            if (is_array($reloadTableList))
            {
                foreach ($reloadTableList as $rt)
                {
                    $reloadTableListScript .= "kx_tablelist_script_" . $rt . ".loadTableList(".$reloadTableListParamJSON.");";
                }
            }
            elseif ($reloadTableList != "")
                $reloadTableListScript .= "kx_tablelist_script_" . $reloadTableList . ".loadTableList(".$reloadTableListParamJSON.");";

            if (is_array($reloadTableListWithHead))
            {
                foreach ($reloadTableListWithHead as $rt)
                {
                    $reloadTableListScript .= "kx_tablelist_script_" . $rt . ".loadTableList({onlyRow:0});";
                }
            }
            elseif ($reloadTableListWithHead != "")
                $reloadTableListScript .=  "kx_tablelist_script_" . $reloadTableListWithHead . ".loadTableList({onlyRow:0});";





            $this->addFooterJS('<script>'
                . 'var ' . $id . '_functionSubmit = function(){
                       ' . ($functionPreSubmit != "" ? $functionPreSubmit . "();" : "") . '
                                dialogModal = waitingDialog.show("' . $dialogMessage . '", {dialogSize: "sm", progressType: "warning"});
                                
                                $.post(KXJS.buildAPICallUrl("' . $kcall . '","' . $kcp . '","' . $kco . '","' . $kca . '"), $("#' . $id . '").serialize(),
                                    function(data) {
                                    
                                        waitingDialog.hide();
                                        var r = $.parseJSON(data);
                                        
                                        KXJS.showSavedMessageJSON("#'.$idSavedMessage.'",r,1);
                                        
                                        '
                . ($functionPostSubmit != "" ? $functionPostSubmit . "(r,'" . $id . "');" : "")
                . ($resetOnSuccess == "1" ?
                    'if(r.success == "true"){
                        $("#' . $id . '")[0].reset();
                    }'
                    : "")
                . ($nextStep != "" ?
                    'if(r.success == "true"){
                        $("#pageStep_'. ($nextStep - 1). '").hide();
                        $("#pageStep_'. $nextStep . '").show();
                    }'
                    : "")
                . ($reloadPageActual == "1" ?
                    'if(r.success == "true"){
                    location.reload();
                                             }'
                    : "")
                . ($reloadPage == "1" ?
                    'if(r.success == "true"){
                                                if($("#' . $reloadPageCheckCode . '").val() == "")
                                                    {'
                    . (
                    $reloadMethod != "REST" ?
                        'location.href = "' . $reloadPageURL . '" + "&' . $reloadPageGetCode . '="+r.' . $reloadPageGetCode . ';'
                        : 'location.href = "' . $reloadPageURL . '" + "' . $reloadPluginMethod . '/"+r.' . $reloadPageGetCode . ';'
                    )
                    . '}
                                             }'
                    : "")
                . ($locationURL != "" ?
                    'if(r.success == "true"){'
                    . (
                        'location.href = "' . $locationURL . '";'
                    )
                    . '}'
                    : "")

                . ($closeModal != "" ?
                    '
                                            if(r.success == "true"){
                                                dialogModal.on("hidden.bs.modal"
                                                        , function () {'
                                                    .$reloadTableListScript
                                                    . ($openModal != "" ?
                                                        '
                                                                            if(r.success == "true"){
                                                                                var extraParamToSend = r;
                                                                                
                                                                               '.$idModal.'OpenModal({extraParam: extraParamToSend});'.'
                                                                            }
                                                                           
                                                                            '
                                                        : ''
                                                    )
                                                    . '}
                                                );
                                                
                                                $("#' . $closeModal . '").modal("hide");
                                                waitingDialog.hide();
                                            }
                                            else{
                                                waitingDialog.hide();
                                            }
                                            '
                    : (
                    'if(r.success == "true") { '
                    .$reloadTableListScript
                    . '}'

                    )
                )
                . ($openModal != "" && $closeModal == "" ?
                    '
                                            if(r.success == "true"){
                                                var extraParamToSend = r;
                                                
                                               '.$idModal.'OpenModal({extraParam: extraParamToSend});'.'
                                            }
                                           
                                            '
                    : ''
                )
                . '
                                        

                                    });
                                };
							$(document).ready(function(){
								$("#' . $id . '").validator({' . $optionsValidator . '}).on("submit", function (e) {
									if (e.isDefaultPrevented()) {
										} else {
											e.preventDefault();
											' . $id . '_functionSubmit();
										}
									})
							});
						</script>');
        }

        // return
        return $this;
    }

    /**
     * get a submit message DIV.
     * This is used for display message after submit form
     *
     * @since 1.00
     * @access public
     *
     *
     * @return string with html of div
     */

    public function getSubmitMessageDiv($id = "savedMessage")
    {
        return '<div class="savedMessage" id="'.$id.'"></div>';
    }
}

?>

