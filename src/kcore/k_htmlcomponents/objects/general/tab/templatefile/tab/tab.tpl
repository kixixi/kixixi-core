<!-- BEGIN TAB -->
<ul id="{TAB.tabID}" class="nav nav-tabs {TAB.class}">
	<!-- BEGIN NAVTAB -->
    	<li class="nav-item {TAB.NAVTAB.class}">
        	<a href="#{TAB.NAVTAB.refID}" class="nav-link {TAB.NAVTAB.active}" data-toggle="tab">{TAB.NAVTAB.title}</a>
        </li>
	<!-- END NAVTAB -->
</ul>
<!-- BLOCKSTART CONTENT -->
<div id="{TAB.tabID}Content" class="tab-content m-b p-t" {TAB.contentAttributes}>
  <!-- BEGIN TABCONTENT -->
  	<div class="tab-pane {TAB.TABCONTENT.class} {TAB.TABCONTENT.active}" id="{TAB.TABCONTENT.id}">
    	{TAB.TABCONTENT.html}
  	</div>
  <!-- END TABCONTENT -->
</div>
<!-- BLOCKEND CONTENT -->
<!-- END TAB -->