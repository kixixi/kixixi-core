<?
/**
 * KIXIXI HTML Components (tab)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage objects\row
 * @since 1.00
 */
namespace k_htmlcomponents\objects\general\tab;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * Base Template of Tabs
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $template;

    /**
     * True if tab parsed
     *
     * @since 1.00
     * @access protected
     * @var boolean
     */
    var $parsed = false;

    /**
     * HTML of tabs parsed
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    var $tabParsedHTML = "";

    /**
     * ID of tabs
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    var $tabID = "";

    /**
     * Constructor
     *
     * @since 1.00
     *
     */
    function __construct($AV = [])
    {
        parent::__construct();
        $this->template = $this->getTemplateLayout("tab");
        $this->create($AV);
    }

    /**
     * get a tab object (web object)
     * This function is used from template manager,
     * otherwise you can use single function for create table, and after use "get" to get table object.
     *
     * @param array $AV array of table parameters.<br>
     *  Parameters: <br>
     *
     * *    id    : id of tab
     * *    contentAttributes    : attributes for content
     * *    tabs    : array of fields (columns)
     *   *    classNav : class of tab
     *   *    classContent : class of content
     *   *    id : id content
     *   *    label : label
     *   *    active : 1 for active
     *   *    html : html of content
     *
     * @return web_object
     */
    public function get($AV = [])
    {

        //if $AV not empty create newTable with $AV Parameters
        if (!empty($AV))
            $this->create($AV);

        //Append HTML of table builded
        $this->appendHTML("",$this->parse());

        //Return Object
        return $this;
    }

    /**
     * create new TAB
     *
     * @param array $arrayVariable array of table parameters.<br>
     * Please refer to "get" function for correct parameters
     *
     */
    public function create($arrayVariable = [])
    {

        //reset template
        $this->template->destroy();
        $this->parsed = false;

        //set variables
        $tabID = $this->K_COMMON->getVarArray($arrayVariable, "id");
        $contentAttributes = $this->K_COMMON->getVarArray($arrayVariable, "contentAttributes");
        $tabs = $this->K_COMMON->getVarArray($arrayVariable, "tabs");
        $dictionary = $this->K_COMMON->getVarArray($arrayVariable,"dictionary");

        //create TAB ID
        if ($tabID == "") {
            $tabID = "navs_" . uniqid();
        }
        $this->tabID = $tabID;

        //set default HTML ID
        $this->setIDObject($this->tabID);
        $this->setIDContent($this->tabID);

        //create BLOCK TAB
        $arraySet = array();
        $arraySet["contentAttributes"] = $contentAttributes;
        $arraySet["tabID"] = $tabID;
        $this->template->assign_block_vars("TAB", $arraySet);

        //create tabs
        if (is_array($tabs))
        {
            if (isset($tabs["tab"]))
                $tabs = $tabs["tab"];

            foreach ($tabs as $tab)
            {
                //Translate dictionary
                $dictionaryToTranslate = $this->K_COMMON->getVarArray($tab,"dictionary");
                if (is_array($dictionaryToTranslate))
                    foreach ($dictionaryToTranslate as $key => $value)
                    {
                        $tab[$key] = $this->convertLabelDictionary(["dictionary" => $value], $dictionary) ;
                    }
                $this->addTab($tab);
            }

        }
    }

    /**
     * Add TAB
     *
     * @param array $arrayParam array of table parameters.<br>
     * Please refer to "get" function for correct parameters
     *
     */
    protected function addTab($arrayParam = array())
    {
        $classNav = $this->K_COMMON->getVarArray($arrayParam, "classNav");
        $classContent = $this->K_COMMON->getVarArray($arrayParam, "classContent");
        $id = $this->K_COMMON->getVarArray($arrayParam, "id");
        $label = $this->K_COMMON->getVarArray($arrayParam, "label");
        $active = $this->K_COMMON->getVarArray($arrayParam, "active");
        $html = $this->K_COMMON->getVarArray($arrayParam, "html");



        if ($id == "") {
            $id = "navs_content_" . uniqid();
        }

        $this->addTabNav(["class" => $classNav, "refID" => $id, "label" => $label, "active" => $active]);
        $this->addTabContent(["class" => "m-l-2 " . $classContent, "id" => $id, "active" => $active, "html" => $html]);
    }

    /**
     * Add TAB NAV
     *
     * @param array $arrayParam array of table parameters.<br>
     * Please refer to "get" function for correct parameters
     *
     */
    protected function addTabNav($arrayParam = array())
    {
        $class = $this->K_COMMON->getVarArray($arrayParam, "class");
        $refID = $this->K_COMMON->getVarArray($arrayParam, "refID");
        $label = $this->K_COMMON->getVarArray($arrayParam, "label");
        $active = $this->K_COMMON->getVarArray($arrayParam, "active");
        $arraySet = array();
        $arraySet["class"] = $class;
        $arraySet["refID"] = $refID;
        $arraySet["title"] = $label;
        if ($active == "1")
            $arraySet["active"] = "active";
        $this->template->assign_block_vars("TAB.NAVTAB", $arraySet);
    }

    /**
     * Add TAB CONTENT
     *
     * @param array $arrayParam array of table parameters.<br>
     * Please refer to "get" function for correct parameters
     *
     */
    protected function addTabContent($arrayParam = array())
    {

        $id = $this->K_COMMON->getVarArray($arrayParam, "id");
        $class = $this->K_COMMON->getVarArray($arrayParam, "class");
        $html = $this->K_COMMON->getVarArray($arrayParam, "html");
        $active = $this->K_COMMON->getVarArray($arrayParam, "active");

        $arraySet = array();
        $arraySet["id"] = $id;
        $arraySet["class"] = $class;
        $arraySet["html"] = $html;
        if ($active == "1")
            $arraySet["active"] = "active";
        $this->template->assign_block_vars("TAB.TABCONTENT", $arraySet);
    }

    /**
     * Get TAB NAV Inner
     *
     *
     * @return web_object
     */
    protected function getTabNavInner()
    {
        if (!$this->parsed)
            $this->parse();

        $html = $this->tabParsedHTML;
        $pattern = "/<ul id=\"".$this->tabID."\"(.*?)>(.*?)<\/ul>/si";
        preg_match($pattern, $html, $matches);
        return $matches[2];

    }

    /**
     * Get TAB CONTENT Inner
     *
     *
     * @return web_object
     */
    protected function getTabContentInner()
    {
        if (!$this->parsed)
            $this->parse();

        $html = $this->tabParsedHTML;
        $pattern = "/<!-- BLOCKSTART CONTENT -->(.*?)>(.*?)<!-- BLOCKEND CONTENT -->/si";
        preg_match($pattern, $html, $matches);
        return $matches[2];

    }

    /**
     * ADD TAB To Actual Tab
     *
     *
     * @return web_object
     */
    public function addTabToExistingTabs($objToCustomize, $tabID, $idItemTab, $label,$objToAppend, $active = "0")
    {

        //reset
        $this->create([]);

        //Add Tabs
        $sidebarArray = [];
        $sidebarArray["label"] = $label;
        $sidebarArray["refID"] = $idItemTab;
        $sidebarArray["active"] = $active;
        $this->addTabNav($sidebarArray);
        
        $sidebarArray = [];
        $sidebarArray["id"] = $idItemTab;
        $sidebarArray["active"] = $active;
        $sidebarArray["html"] = "";
        $this->addTabContent($sidebarArray);

        //get Tabs Created
        $objHtmlNav = $this->getTabNavInner();
        $objHtmlContent = $this->getTabContentInner();

        $objToCustomize -> mergeObjectVariables($this);

        //Append Tab
        $objToCustomize->appendHTML($tabID, $objHtmlNav);
        $objToCustomize->appendHTML($tabID . "Content", $objHtmlContent);

        //Append object
        if (!empty($objToAppend))
            $objToCustomize->appendObject($idItemTab,$objToAppend);

        return $objToCustomize;

    }

    /**
     * Parse the TABS and return HTML
     *
     * @return string
     */
    protected function parse()
    {

        $this->tabParsedHTML = $this->template->pparse("body");
        $this->parsed = true;
        return $this->tabParsedHTML;
    }


}


?>