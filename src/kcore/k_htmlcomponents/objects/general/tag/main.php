<?
/**
 * KIXIXI HTML Components (tag)
 * generic html tag
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage objects\tag
 * @since 1.00
 */
namespace k_htmlcomponents\objects\general\tag;

class obj extends \k_webmanager\objects\general\general_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";



    /**
     * get this object
     *
     * @since 1.00
     * @access public
     *
     * @param array $arrayParam array with parameters for create Form
     *
     *  The array of form support this values<br>
     *
     * *    id    : optional id of the form
     * *    tag    : tag html (ex: "p", "div" etc)
     * *    class    : optional class of the form
     * *    attributes    : optional extra form attributes
     * *    content    : optional content to insert in tag
     *
     * @return webobject
     */
    public function get($arrayParam = [])
    {


        //set variables
        $id = $this->K_COMMON->getVarArray($arrayParam, "id");
        $class = $this->K_COMMON->getVarArray($arrayParam, "class");
        $style = $this->K_COMMON->getVarArray($arrayParam, "style");
        $attributes = $this->K_COMMON->getVarArray($arrayParam, "attributes");
        $content = $this->K_COMMON->getVarArray($arrayParam, "content");
        $tag = $this->K_COMMON->getVarArray($arrayParam, "tag");

        //check variables
        if ($id == "")
            $id = $tag . "_" . uniqid();

        //style
		if ($style != "")
            $attributes .= 'style = "'.$style.'"';

        // create html string
        $this->appendHTML("",'<'.$tag.' id="' . $id . '" class="' . $class . '"' . $attributes . '>' . $content . '</'.$tag.'>');

        // return
        return $this;
    }

}

?>

