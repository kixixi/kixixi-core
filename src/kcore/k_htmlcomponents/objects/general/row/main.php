<?
/**
 * KIXIXI HTML Components (row)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage objects\row
 * @since 1.00
 */

namespace k_htmlcomponents\objects\general\row;
class obj extends \k_webmanager\objects\general\general_object\obj
{
	/**
	 * Version of object
	 *
	 * @since 1.00
	 * @access public
	 * @var string
	 */
	public $ver = "1.0";

	/**
	 * Get HTML of a row well formatted, using bootstrap .
	 *
	 * @param array $arrayVariable array of parameters
	 * The following parameters are accepted:<br>
	 *	*	id (optional): the id of object
	 *	*	class (optional): class of the object
	 *	*	attributes (optional): additional attributes
     *	*	margin (optional): margin
	 *	*	wo (optional): web object to insert in column
	 *	*	html (optional): content of the column


	 *
	 * @return array with html.
	 */
	public function get($arrayVariable = array()){



		//set variables
		$id = $this->K_COMMON->getVarArray($arrayVariable,"id");
		$class = $this->K_COMMON->getVarArray($arrayVariable,"class");
		$style = $this->K_COMMON->getVarArray($arrayVariable, "style");
		$attributes = $this->K_COMMON->getVarArray($arrayVariable,"attributes");
        $margin = $this->K_COMMON->getVarArray($arrayVariable,"margin");
		$html = $this->K_COMMON->getVarArray($arrayVariable,"html");
		$wo = $this->K_COMMON->getVarArray($arrayVariable, "wo");

		//check variables
		if($id == ""){
			$id = "row_".uniqid();
		}
		if ($margin != "")
			$style .= "margin:" . $margin . ";";


		//set default HTML ID
		$this->setIDObject($id);
		$this->setIDContent($id);

		//style
		if ($style != "")
			$attributes .= 'style = "'.$style.'"';

		// create html string
		$this->appendHTML("", '
		<div id="'.$id.'" class="row '.$class.'"'.$attributes.'>'
					.$html
					.'
					</div>
					');



		//Append web object
		$this->appendObject($id,$wo);


		//return
		return $this;
	}
}
?>

