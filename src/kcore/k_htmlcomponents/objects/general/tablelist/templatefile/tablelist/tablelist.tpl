<!-- BEGIN HEAD -->
<div class="table-responsive no-more-tables tablelist-responsive" id="{tableID}">
    <table class="table table-striped table-bordered {tableClass}">

            <thead class="thead-default" id="{tableID}Head">
                <tr>
                    <!-- BEGIN COLUMN -->
                    <th class="{HEAD.COLUMN.columnClass}" {HEAD.COLUMN.columnAttributes}>
                    	{HEAD.COLUMN.value}
                        <!-- BEGIN VALUE -->
                           <{HEAD.COLUMN.VALUE.tag} class="{HEAD.COLUMN.VALUE.valueClass}" {HEAD.COLUMN.VALUE.valueAttributes} >{HEAD.COLUMN.VALUE.value}</{HEAD.COLUMN.VALUE.tag}>
                    	<!-- END VALUE -->
                    </th>
                    <!-- END COLUMN -->
                </tr>
            </thead>

        <tbody id="{tableID}Body">
<!-- END HEAD -->
            <!-- BEGIN HIDDENFIELDS -->
            <input type="hidden" id="{HIDDENFIELDS.id}" value="{HIDDENFIELDS.value}">
            <!-- END HIDDENFIELDS -->
            <!-- BEGIN ROW -->
            <tr class="{ROW.rowClass}"  {ROW.rowAttributes}>
                <!-- BEGIN COLUMN -->
                <td data-title="{ROW.COLUMN.title}" class="{ROW.COLUMN.columnClass}" {ROW.COLUMN.columnAttributes}>
                        <!-- BEGIN VALUE -->
                        <{ROW.COLUMN.VALUE.tag} class="{ROW.COLUMN.VALUE.valueClass}" {ROW.COLUMN.VALUE.valueAttributes} >{ROW.COLUMN.VALUE.value}</{ROW.COLUMN.VALUE.tag}>
                    	<!-- END VALUE -->
                </td>
                <!-- END COLUMN -->
            </tr>
            <!-- END ROW -->

            <!-- BEGIN FOOTER -->
            <tr class="{FOOTER.rowClass}" {FOOTER.rowAttributes}>
                <!-- BEGIN COLUMN -->
                <td class="tableListColumn {FOOTER.COLUMN.columnClass}" {FOOTER.COLUMN.columnAttributes}>
                    <!-- BEGIN VALUE -->
                    <span class="{FOOTER.COLUMN.VALUE.valueClass}" {FOOTER.COLUMN.VALUE.valueAttributes}>{FOOTER.COLUMN.VALUE.value}</span>
                    <!-- END VALUE -->
                </td>
                <!-- END COLUMN -->
            </tr>
            <!-- END FOOTER -->
<!-- BEGIN HEAD -->
        </tbody>
    </table>
</div>
<!-- END HEAD -->
