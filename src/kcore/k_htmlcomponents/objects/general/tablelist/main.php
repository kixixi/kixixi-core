<?
/**
 * KIXIXI HTML Components (tablelist)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage objects\tablelist
 * @since 1.00
 */
namespace k_htmlcomponents\objects\general\tablelist;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * template object (tpl file)
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $template;

    /**
     * head (true of false if exist or not head of table)
     *
     * @since 1.00
     * @access protected
     * @var boolean
     */
    protected $head = false;

    /**
     * rowType
     * the type of last row insert on table
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $rowType;

    /**
     * Array of Head Titles
     *
     * @since 1.00
     * @access protected
     * @var array
     */
    protected $colHeadTitle = array();

    /**
     * Number of last column appended
     *
     * @since 1.00
     * @access protected
     * @var integer
     */
    protected $actColNum;

    /**
     * __construct
     *
     */
    function __construct()
    {
        //call parent
        parent::__construct();

        //open tpl of table list
        $this->template = $this->getTemplateLayout("tablelist");

    }

    /**
     * Manage Ajax Request
     *
     * @since 1.00
     * @access public
     */
    public function objectRequest($ArrayVariable)
    {

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //manage request
        switch ($KCA) {
            case "loadDataTable" :
                $obj = $this->loadDataTable($ArrayVariable);
                echo json_encode(["success" => "true", "html" => $obj->getHTMLWithCSSJS()]);
                break;
            case "askConfirm" :
                echo json_encode($this->getHtmlAskConfirm($ArrayVariable));
                break;

        }
    }

    /**
     * get a table object (web object)
     * This function is used from template manager,
     * otherwise you can use single function for create table, and after use "get" to get table object.
     *
     * @param array $AV array of table parameters.<br>
     * Please refer to "newTable" function for correct parameters
     *
     * @return web_object
     */
    public function get($AV = [])
    {

        //if $AV not empty create newTable with $AV Parameters
        if (!empty($AV))
            $this->newTable($AV);

        //Append HTML of table builded
        $this->appendHTML("", $this->parse());

        //Return Object
        return $this;
    }


    /**
     * Create a New Table object (web object)
     *
     * @param array $AV array of table parameters.<br>
     *
     *  Parameters: <br>
     *
     * *    id    : id of table
     * *    tableClass    : class of table
     * *    triggerFunction    : called after loaded table
     * *    hiddenFields    : array of fields hidden on tablelist (used on reload data, example on use of resfresh table)
     * *    groupby    : array of fields to group record
     * *    fields    : array of fields (columns)
     *   *    label : label of column
     *   *    beforeFieldValue : string before Field
     *   *    afterFieldValue : string after Field
     *   *    type: type of column : blank or "value" for qryField value, "dictionary" get value from dictionary, "input" for input, "state" for state
     *   *    qryField : field to get from query
     *   *    qryFieldDictioanry : field to get from query (translate from Dictionary
     *   *    fieldFunction: function to manipulate qryField. Ex: fieldFunction => ["number_format",["[VALUE]",4,",","."]]
     *   *    columnClass : class of colun
     *   *    width : width of column
     *   *    input : get an html input
     *   *    buttons : array of buttons <br>
     *        May be possible add all parameters of "button" object (from "htmlcomponents") <br>
     *      *    action : empty | "edit" | "copy" | "editModal" | "remove" | "download" |"modalConfirm" | "refreshTableList" | "lock" | "unlock" | "checkbox" | "modal" | "link" (if not empty create automatically a button. Only "modal" and "link" can be used more than once)
     *      *    qryAttributes : array of attributes from data table to add to the button (Ex: ["keyID" => "keyID"])
     *      *    staticAttributes : array of attributes static created when build table head (Ex: ["customCode" => "1"])
     *      *    dynamicAttributes : array of attributes static created when build rows (Ex: ["webCode" => "1"])
     *      *    ajax : array of connection parameters: kcp, kco, kca (used only for action "remove", "lock", "unlock")
     *      *    idModal : id of modal if we open a modal (default "modalID")
     *      *    reloadOnSuccess : 1 if you want reload table list on ajax answer success = "true"
     *      *    triggerFunction    : called on success ajax (where expected)
     * *    data    : data array for table (results of get db data as array)
     * *    hideHead    : 1 for not show head
     * *    rowProgression: if valued is used for replace only one row
     * *    dictionary    : dictionary for label transaction
     * *    connection    : array for data connection (get table content)
     *   *    pluginName : name of plugin
     *   *    pluginObject : name of object
     *   *    functionName : function name (return an array of data db results)
     *   *    functionVariables : array of variables for function
     * *    dataLoad    : array for data connection (get table content)
     *   *    pluginName : name of plugin
     *   *    pluginData : name of data object
     * *    ajax    : array for data connection by ajax (call direct to object)
     *   *    kcp : name of plugin
     *   *    kco : name of object
     *   *    kca : function name (return an array of data db results)
     *   *    showLoading : if 1 show Loading PopUp
     *   *    notLoad : if 1 not load data immediatly, only manually
     *   *    functionStartReload: Function to call before Reload (only for reload = 1)
     *   *    functionEachReload: Function to call each Reload (only for reload = 1)
     *   *    functionEndReload: Function to call End Reload (only for reload = 1)
     *   *  fieldsPageToGet: array of fields to get from page when refresh table. Ex: ["webCode" => "webCode"]
     * *    fVar    : array of variables to be sent to the function that get data table (merged with functionVariables).
     *                  may be used for example from "appendWebTemplate" to pass parameters or by ajax call
     * *    tableListRefID    : id of object on web_templates_content table
     *
     *
     *
     * Example: $OBJTableList->newTable(
     *  [
     *   "id"=>"tableList"
     *   ,"hideHead" => "0"
     *   ,"data" => $dataTable
     *   ,"fields" =>
     *       [
     *           [
     *               "label" => $this->dictionary["labelName"]
     *               ,"qryField" => "name"
     *           ]
     *           ,
     *           [
     *               "label" => $this->dictionary["labeRole"]
     *                  ,"type" => "dictionary"
     *                  ,"qryField" => "role"
     *              ]
     *              ,
     *              [
     *               "buttons" =>
     *                   [
     *                       [
     *                           "title" => $this->dictionary["labelDelete"]
     *                           ,"class" => "btnDelete btn-secondary btn-sm"
     *                           ,"qryAttributes" => ["keyID" => "keyID"]
     *                           ,"staticAttributes" => "a" => "1"
     *                           ,"dynamicAttributes" => "webCode" => "1"
     *                           ,"attributes" => ""
     *                           ,"symbolClass" => "fa fa-remove"
     *                       ]
     *                   ]
     *           ]
     *       ]
     *  ]
     * );
     *
     *
     * @return html generated
     */
    public function newTable($AV)
    {


        //$this->K_COMMON->printArray($AV);
        $this->template->destroy();

        $this->addHeadFile([$this->getPathFileCSS("tablelist")]);

        //get variables
        $tableID = $this->K_COMMON->getVarArray($AV, "id");
        $tableClass = $this->K_COMMON->getVarArray($AV, "tableClass");
        $fields = $this->K_COMMON->getVarArray($AV, "fields");
        $groupBy = $this->K_COMMON->getVarArray($AV, "groupBy");
        $hiddenFields = $this->K_COMMON->getVarArray($AV, "hiddenFields");
        $triggerFunctionTable = $this->K_COMMON->getVarArray($AV, "triggerFunction");
        $data = $this->K_COMMON->getVarArray($AV, "data");
        $hideHead = $this->K_COMMON->getVarArray($AV, "hideHead");
        $rowProgression = $this->K_COMMON->getVarArray($AV, "rowProgression");
        $dictionary = $this->K_COMMON->getVarArray($AV, "dictionary");
        $connection = $this->K_COMMON->getVarArray($AV, "connection");
        $dataLoad = $this->K_COMMON->getVarArray($AV, "dataLoad");
        $ajax = $this->K_COMMON->getVarArray($AV, "ajax");
        $fieldsPageToGet = $this->K_COMMON->getVarArray($AV, "fieldsPageToGet");
        $fVar = $this->K_COMMON->getVarArray($AV, "fVar");
        $addScript = false;
        $loadDataByAjax = false;
        $triggerString = "";
        $scriptString = "";
        $groupByFieldValue = "";
        $groupByFieldToGroup = "";
        $groupByRowNumber = 0;

        //get variables from objData
        $objData = $this->K_COMMON->getVarArray($AV, "objData");
        $tableListRefID = $this->K_COMMON->getVarArray($objData, "tplCNTID");



        //check variables
        if ($hideHead == "")
            $hideHead = "0";

        if ($tableListRefID == "")
            $tableListRefID = "0";
        if (empty($triggerFunctionTable))
            $triggerFunctionTable = "";

        //check if load data by ajax or not
        if (empty($data) && (!(empty($dataLoad)) || !(empty($connection)) || !(empty($ajax))))
            $loadDataByAjax = true;

        //check data
        if (is_string($data))
            if (!empty($data))
                $data = json_decode($data,true);


        //set table class on template
        $this->template->assign_vars(array("tableClass" => $tableClass, "tableID" => $tableID));

        //search if load data
        if (is_array($dataLoad))
        {
            if (!empty($dataLoad)) {
                $pluginName = $this->K_COMMON->getVarArray($dataLoad, "pluginName");
                $pluginData = $this->K_COMMON->getVarArray($dataLoad, "pluginData");

                if ($pluginName != "" && $pluginData != "") {
                    $tableData = $this->K_COMMON->getPluginDataObject($pluginName, $pluginData);
                    $tableData->get("*");

                    $data = $tableData->getResults("array");

                    $ajax = [];
                    $ajax["kcp"] = "k_htmlcomponents";
                    $ajax["kco"] = "tablelist";
                    $ajax["kca"] = "loadDataTable";


                    //Create Field Array
                    if (!is_array($fields))
                    {
                        $fieldsTemp = explode(",", $tableData->getListOfFields("", false, true));
                        foreach ($fieldsTemp as $f)
                        {
                            $f = trim($f);
                            $fields[] = ["label" => $f, "qryField" => $f];
                        }

                    }




                }

            }
        }


        //create Head
        if (is_array($fields)) {

            if (isset($fields["field"])) //from xml
                $fields = $fields["field"];

            if (is_array($groupBy))
            {
                if (!empty($groupBy))
                {
                    $groupByFieldToGroup = $groupBy[0]["groupField"];
                    $groupByFieldToShow = $groupBy[0]["qryField"];
                }
            }
            else
                $groupBy = [];




            //for each field
            $buttons = [];
            foreach ($fields as $field) {
                $label = $this->convertLabelDictionary($this->K_COMMON->getVarArray($field, "label"), $dictionary);
                $columnClass = $this->K_COMMON->getVarArray($field, "columnClass");
                $width = $this->K_COMMON->getVarArray($field, "width");
                $buttonsTmp = $this->K_COMMON->getVarArray($field, "buttons");
                if (is_array($buttonsTmp))
                    $buttons = array_merge($buttons, $buttonsTmp);

                $this->addColumnHead($label, $columnClass, $width, $hideHead);
            }

            $this->createButtonsScript($tableID, $buttons, $scriptString, $triggerString, 1);

            //for each field of Group
            $buttons = [];
            foreach ($groupBy as $field) {
                $buttonsTmp = $this->K_COMMON->getVarArray($field, "buttons");
                if (is_array($buttonsTmp))
                    $buttons = array_merge($buttons, $buttonsTmp);

            }

            $this->createButtonsScript($tableID, $buttons, $scriptString, $triggerString, 2);


        }

        //Add script only first time (when show head)
        if ($hideHead == "0") {
            $addScript = true;
        }
        else {
            //connection by Ajax
            if (is_array($connection)) {

                //get connection variables
                $pluginName = $this->K_COMMON->getVarArray($connection, "pluginName");
                $pluginObject = $this->K_COMMON->getVarArray($connection, "pluginObject");
                $functionName = $this->K_COMMON->getVarArray($connection, "functionName");
                $functionVariables = $this->K_COMMON->getVarArray($connection, "functionVariables");


                //Load Data
                if (!is_array($functionVariables))
                    $functionVariables = [];
                if (is_array($fVar))
                    $functionVariables = array_merge($functionVariables, $fVar);
                $functionVariables["OM"] = "array";

                // get plugin from to load data
                $objPlugin = $this->K_COMMON->getPluginObject($pluginName, $pluginObject);
         
                $data = $objPlugin->$functionName(array_merge($_REQUEST,$functionVariables));


            }
        }



        //create data table
        if (is_array($hiddenFields))
            foreach ($hiddenFields as $hFieldsID => $hFieldsValue)
            {
                $this->template->assign_block_vars("HIDDENFIELDS", array("id" => $tableID . "_" . $hFieldsID, "value" => $hFieldsValue));
            }




        if (is_array($data)) {

            if (!is_array($fields))
                $fields = [];


            $recordNumber = 0;


            foreach ($data as $record) {

                if ($rowProgression == "")
                    $recordNumber++;
                else
                    $recordNumber = $rowProgression;

                if ($groupByFieldToGroup != "")
                {

                    $gbv = $this->K_COMMON->getVarArray($record, $groupByFieldToGroup);

                    if ($groupByFieldValue != $gbv)
                    {

                        $groupByRowNumber ++;


                        //add row
                        $rowGroupClass = " rowGroupProgression rowGroupProgression".$groupByRowNumber;
                        $this->addRow("R", $rowGroupClass, 'data-rowgroupprogression="'.$groupByRowNumber.'"');

                        $this->addFieldsOnTable($record, $groupBy, $dictionary, 2);

                    }


                    $groupByFieldValue = $gbv;




                }

                //add row
                $rowClass = $this->K_COMMON->getVarArray($record, "rowClass");
                $rowClass .= " rowProgression rowProgression" . $recordNumber;
                $this->addRow("R", $rowClass, 'data-rowprogression="' . $recordNumber.'"');

                $this->addFieldsOnTable($record, $fields, $dictionary, 1);



            }

        }

        //Add script
        $scriptLoadTableList = "";
        if ($addScript) {
            if ($loadDataByAjax) {

                //set variables for ajax
                $dataCall = ["onlyRow" => 1, "tableListRefID" => $tableListRefID];
                $kcp = "";
                $kco = "";
                $kca = "";
                $showLoading = 0;
                $notLoadNowFromAjax = "";
                $functionStartReload = "";
                $functionEachReload = "";
                $functionEndReload = "";

                if (is_array($connection)) {
                    $kcp = "k_htmlcomponents";
                    $kco = "tablelist";
                    $kca = "loadDataTable";
                    $showLoading = 1;

                    if (is_array($fVar))
                        $dataCall["fVar"] = $fVar;





                } elseif (is_array($ajax)) {
                    $kcp = $this->K_COMMON->getVarArray($ajax, "kcp");
                    $kco = $this->K_COMMON->getVarArray($ajax, "kco");
                    $kca = $this->K_COMMON->getVarArray($ajax, "kca");
                    $showLoading = $this->K_COMMON->getVarArray($ajax, "showLoading");
                    $notLoadNowFromAjax = $this->K_COMMON->getVarArray($ajax, "notLoad");
                    $functionStartReload = $this->K_COMMON->getVarArray($ajax, "functionStartReload");
                    $functionEachReload = $this->K_COMMON->getVarArray($ajax, "functionEachReload");
                    $functionEndReload = $this->K_COMMON->getVarArray($ajax, "functionEndReload");

                    //replace "\" on kco with double "\" for javascript
                    $kco = str_replace("\\", "\\\\", $kco);



                    if (is_array($fVar))
                        $dataCall = array_merge($dataCall, $fVar);


                }



                //script load table list
                if ($kcp != "")
                {

                    $dataJSONAdd = "";
                    if (is_string($fieldsPageToGet))
                        $fieldsPageToGet = json_decode($fieldsPageToGet,true);

                    if (is_array($fieldsPageToGet))
                    {

                        foreach ($fieldsPageToGet as $field => $val)
                        {
                            if (is_array($val))
                            {
                                $dataJSONAdd .= '"' . $val["parameter"] . '": ($("#'.$val["htmlid"].'").is(":checkbox") ? ($("#'.$val["htmlid"].'").is(":checked") ? $("#'.$val["htmlid"].'").val() : "") : $("#'.$val["htmlid"].'").val()),';
                            }
                            else
                                $dataJSONAdd .= '"' . $field . '": ($("#'.$val.'").is(":checkbox") ? ($("#'.$val.'").is(":checked") ? $("#'.$val.'").val() : "") : $("#'.$val.'").val()),';
                        }
                    }

                    $dataJSON = json_encode($dataCall);
                    $dataJSON = substr_replace($dataJSON,"{" . $dataJSONAdd,0,1);

                    $scriptLoadTableList = '
                    this.loadTableListSingleRow = function(options)
                    {
                        var options = options || {};
                            var onlyRow = 1;
                            var reload = 0;
                            var extraParam = "";
                            var dataAjax = ' . $dataJSON . ';
                            var rowProgression = options.rowProgression;
                            if (typeof (options.extraParam) == "object")
                                {
                               
                                Object.assign(dataAjax, options.extraParam);
                                }
                            
                            
                            dataAjax.onlyRow = onlyRow;
                            dataAjax.rowProgression = rowProgression;
                            
                            $.ajax({
                                url: KXJS.buildAPICallUrl("KXCJS","' . $kcp . '","' . $kco . '","' . $kca . '", extraParam),
                                data: dataAjax
                        
                            }).done(function(data) {
                                r = $.parseJSON(data);
                                if(r.success == "true"){}
                                    $("#' . $tableID . ' .rowProgression" + rowProgression).replaceWith(r.html);
                                    t.assignTrigger({rowProgression: rowProgression});
                                }
                            );
                    }
                    this.loadTableListWithReload = function(options)
                    {
                        var options = options || {};
                        options.startReload = 1;
                        options.extraParam = {};
                        options.extraParam.startReload = 1;
                        '.($functionStartReload != "" ? $functionStartReload . '()' : "").'
                        t.loadTableList(options);
                    }
                    this.loadTableList = function(options)
                        {
                        
                            var options = options || {};
                            var onlyRow = 1;
                            var reload = 0;
                            var startReload = 0;
                            var extraParam = "";
                            var dataAjax = ' . $dataJSON . ';
                            if (typeof (options.onlyRow) != "undefined")
                                onlyRow = options.onlyRow;
                            if (typeof (options.startReload) != "undefined")
                                startReload = options.startReload;
                            if (typeof (options.extraParam) == "object")
                                {
                                
                                Object.assign(dataAjax, options.extraParam);
                                }
                            
                            
                            dataAjax.onlyRow = onlyRow;
                            
                            
                            ' . ($showLoading == "1" ? 'waitingDialog.show("Loading...", {dialogSize: "sm", progressType: "warning"});' : "") . '
                            $.ajax({
                                url: KXJS.buildAPICallUrl("KXCJS","' . $kcp . '","' . $kco . '","' . $kca . '", extraParam),
                                data: dataAjax
                        
                            }).done(function(data) {
                                r = $.parseJSON(data);
                                ' . ($showLoading == "1" ? 'waitingDialog.hide();' : "") . '
                                
                                
                                if(r.success == "true"){
                                
                                    if (typeof (r.reload) != "undefined")
                                        {
                                            reload = r.reload; //1: for reloadm, 2: for reload ended
                                            reloadRefCode = r.reloadRefCode;
                                        }
                                
                                    if (onlyRow == 1) 
                                        {
                                            if (reload == 1 || reload == 2)
                                                {
                                                    if (startReload == 1)
                                                        $("#' . $tableID . ' tbody").html(r.html);
                                                    else
                                                        $("#' . $tableID . ' tbody").append(r.html);
                                                        
                                                    if (reload == 1)
                                                    {
                                                        options.startReload = 0;
                                                    
                                                        if (typeof (options.extraParam) == "undefined")
                                                        options.extraParam = {};
                                                        
                                                        options.extraParam.reloadRefCode = reloadRefCode;
                                                        options.extraParam.startReload = 0;
                                                        
                                                        '.($functionEachReload != "" ? $functionEachReload . '(r)' : "").'
                                                        
                                                        t.loadTableList(options);
                                                    }    
                                                    
                                                    if (reload == 2)
                                                    {
                                                    
                                                        '.($functionEndReload != "" ? $functionEndReload . '(r)' : "").'
                                                        
                                                    }
                                                    
                                                    
                                                }    
                                            else
                                                $("#' . $tableID . ' tbody").html(r.html);
                                        }
                                    else
                                        {
                                            $("#' . $tableID . '").html(r.html);
                                            $("#' . $tableID . ' .table").unwrap();
                                        }
                                    t.assignTrigger();
                                }
                                else
                                    alert("Error Security");
                            });
                            
                        }
                ';
                }
            }


            $jsUniqueID = "kx_tablelist_script_" . $tableID;


            $this->addFooterJS('
                <script>
                ' . $jsUniqueID . ' = {};
                (function() {
                    var t = this;
                    
                    this.init = function(){
                        $(document).ready(t.ready);
                    };
                    this.ready = function(){
                        t.assignTrigger();
                        ' . ($loadDataByAjax && !$notLoadNowFromAjax ? "t.loadTableList();" : "") . '
                    }
                                        
                    ' . $scriptString . '
                    
                    ' . ($loadDataByAjax ? $scriptLoadTableList : "") . '
                    
                    
                    
                    t.assignTrigger = function(options){
                        options = options || {};
                        extraClass = "";
                        rowProgression = 0;
                        
                        if (typeof (options.rowProgression) != "undefined")
                                {
                                    rowProgression = options.rowProgression;
                                    extraClass += " .rowProgression" + rowProgression;
                                 }   
                        
                        
                        ' . ($triggerFunctionTable != "" ? $triggerFunctionTable . '({rowProgression: rowProgression});' : '')
                        

                        

                        .$triggerString .'
                    }
    
                    this.init();
    
                }).apply(' . $jsUniqueID . ');
                </script>
			');



        }


    }

    /**
     * get HTML Modal Ask Confirm
     *
     * @return array with html
     */
    protected function getHtmlAskConfirm($ArrayVariable)
    {

        return $this->K_COMMON->getModalAskConfirm($ArrayVariable);

    }


    /**
     * get object table from a Object Used Record
     *
     * @param array $AV array of table parameters.<br>
     *
     *  Parameters: <br>
     *
     * *    fVar    : array of variables to be sent to the function that get data table (merged with functionVariables).
     *                  may be used for example from "appendWebTemplate" to pass parameters
     * *    tableListRefID    : id of object on web_templates_content table
     *
     * @return web_object
     */
    public function loadDataTable($AV)
    {
        //get variables
        $tableListRefID = $this->K_COMMON->getVarArray($AV, "tableListRefID");
        $fVar = $this->K_COMMON->getVarArray($AV, "fVar");
        $onlyRow = $this->K_COMMON->getVarArray($AV, "onlyRow");

        if ($onlyRow == "")
            $onlyRow = 1;

        //include table
        $webObjectsUsed = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_templates_content");

        //Load record from object used
        $webObjectsUsed->get(
            [
                "plugin" => "tpo_wo_plugin_name"
                , "object" => "tpo_wo_plugin_object"
                , "parameters" => "tpo_wo_parameters"
            ]
            ,
            [
                "id_web_template_content" => $tableListRefID
            ]
        );

        //get parameters for table
        $rowObjUsed = $webObjectsUsed->fetch();
        $objParameters = $rowObjUsed->parameters;

        //convert parameters to array
        $objParametersArray = json_decode($objParameters, true);
        $objParametersArray["hideHead"] = $onlyRow;
        $objParametersArray["fVar"] = $fVar;

        //return object table

        return $this->get($objParametersArray);
    }

    /**
     * Add Column to Head and create Head if not exist
     *
     * @param string $label label of column head
     * @param string $columnClass class of column
     * @param string $width width of column
     * @param integer $hideHead if 1 not show head
     *
     * @return web_object
     */
    public function addColumnHead($label = "", $columnClass = "", $width = "", $hideHead = 0)
    {
        if ($hideHead == 0) {
            if (!$this->head) {
                $this->template->assign_block_vars("HEAD", array());
                $this->head = true;
            }
            $arraySet = array();
            $arraySet["value"] = $label;
            $arraySet["columnClass"] = $columnClass;
            if ($width != "") $arraySet["columnAttributes"] = 'style="width:' . $width . ';max-width:' . $width . ';"';
            $this->template->assign_block_vars("HEAD.COLUMN", $arraySet);
        }
        //Put on Array Head
        $this->colHeadTitle[] = $label;
        $this->rowType = "HEAD";
    }

    /**
     * Add Row to table
     *
     * @param string $type R for Row and F for Footer
     * @param string $rowClass class of row
     * @param string $rowAttributes attributes for row
     *
     * @return web_object
     */
    public function addRow($type = "R", $rowClass = "", $rowAttributes = "")
    {
        switch ($type) {
            case "R":
                $this->rowType = "ROW";
                break;
            case "F":
                $this->rowType = "FOOTER";
                break;
            default :
                $this->rowType = "ROW";
                break;
        }

        $this->template->assign_block_vars($this->rowType, array("rowClass" => $rowClass, "rowAttributes" => $rowAttributes));
        $this->actColNum = 0;
    }

    /**
     * Add Column at Row
     *
     * @param string $value optional value to insert in column (if not empty add the value to column)
     * @param string $valueTitle Title for value
     * @param string $valueLink link for value
     * @param string $valueClass class for value
     * @param string $valueAttributes attributes for value
     * @param string $width width
     * @param string $columnClass class for column
     * @param string $valueTag tag for value (default span)
     *
     */
    public function addColumn($value = "", $valueTitle = "", $valueLink = "", $valueClass = ""
        , $valueAttributes = "", $width = "", $columnClass = "", $valueTag = "span", $colSpan = "1")
    {
        $arraySet = [];
        $arraySet["columnAttributes"] = "";
        $arraySet["title"] = $this->colHeadTitle[$this->actColNum];
        $arraySet["columnClass"] = $columnClass;
        if ($width != "")
            $arraySet["columnAttributes"] .= 'style="width:' . $width . ';max-width:' . $width . ';"';
        if ($colSpan != "1")
            $arraySet["columnAttributes"] .= 'colspan="' . $colSpan . '"';

        $this->template->assign_block_vars($this->rowType . ".COLUMN", $arraySet);
        if ($value != "") $this->addValue($value, $valueTitle, $valueLink, $valueClass, $valueAttributes, $valueTag);
        $this->actColNum += 1;
    }

    /**
     * Add Column with State
     *
     * @param boolean $value true | false
     * @param string $valueTitle Title for value
     * @param string $valueLink link for value
     * @param string $valueClass class for value
     * @param string $valueAttributes attributes for value
     * @param string $width width
     * @param string $columnClass class for column
     *
     */
    public function addColumnState($value = true, $valueTitle = "", $valueLink = "", $valueClass = "", $valueAttributes = "", $width = "", $columnClass = "")
    {
        $arraySet = array();
        $arraySet["title"] = $this->colHeadTitle[$this->actColNum];
        $arraySet["columnClass"] = $columnClass;
        if ($width != "") $arraySet["columnAttributes"] = 'style="width:' . $width . ';max-width:' . $width . ';"';
        $this->template->assign_block_vars($this->rowType . ".COLUMN", $arraySet);

        $this->addValueState($value, $valueTitle, $valueLink, $valueClass, $valueAttributes);
        $this->actColNum += 1;
    }


    /**
     * Add Column with Progress Bar
     *
     * @param string $value progress value (From 1 to 100)
     * @param string $valueTitle Title for value
     * @param string $valueClass class for value
     *
     */
    public function addColumnProgBar($value = "", $valueTitle = "", $valueClass = "")
    {
        $value = '<progress class="progress" value="' . $value . '" max="100">' . $value . '%</progress>';
        $this->addColumn($value, $valueTitle, "", $valueClass);
    }

    /**
     * Add Value at last Column inserted
     *
     * @param string $value progress value (From 1 to 100)
     * @param string $valueTitle Title for value
     * @param string $valueLink link for value
     * @param string $valueClass class for value
     * @param string $valueAttributes attributes for value
     * @param string $valueTag tag for value (default span)
     *
     */
    public function addValue($value = "", $valueTitle = "", $valueLink = "", $valueClass = "", $valueAttributes = "", $valueTag = "span")
    {
        $arraySet = array();
        if (($valueLink != "") || (is_array($valueLink))) {

            if (is_array($valueLink)) {
                $valueLinkA = $this->K_COMMON->getVarArray($valueLink, "link");
                $valueLinkT = $this->K_COMMON->getVarArray($valueLink, "target");
            } else {
                $valueLinkA = $valueLink;
                $valueLinkT = "_self";
            }

            $arraySet["value"] = '<a href="' . $valueLinkA . '" target="' . $valueLinkT . '">' . $value . '</a>';
        } else $arraySet["value"] = $value;
        $arraySet["valueClass"] = $valueClass;
        if ($valueTitle != "") $valueAttributes .= ' title="' . $valueTitle . '"';
        $arraySet["valueAttributes"] = $valueAttributes;
        $arraySet["tag"] = $valueTag;
        $this->template->assign_block_vars($this->rowType . ".COLUMN.VALUE", $arraySet);
    }

    /**
     * Add Column with State
     *
     * @param boolean $value true | false
     * @param string $valueTitle Title for value
     * @param string $valueLink link for value
     * @param string $valueClass class for value
     * @param string $valueAttributes attributes for value
     * @param string $width width
     * @param string $columnClass class for column
     *
     */
    public function addValueState($value = true, $valueTitle = "", $valueLink = "", $valueClass = "", $valueAttributes = "")
    {

        if ($value) {
            $valueSymbol = "";
            $symbolClass = "fa fa-check text-success";
        } else {
            $valueSymbol = "";
            $symbolClass = "fa fa-minus-circle text-danger";
        }
        $this->addValueWithSymbolClass($valueSymbol, $valueTitle, $valueLink, $valueClass . " fontSize24", $valueAttributes, $symbolClass);

    }

    /**
     * Add Value at last Column inserted
     *
     * @param string $value progress value (From 1 to 100)
     * @param string $valueTitle Title for value
     * @param string $valueLink link for value
     * @param string $valueClass class for value
     * @param string $valueAttributes attributes for value
     * @param string $valueTag tag for value (default span)
     *
     */
    public function addObject($objToAppend, $valueClass = "", $valueAttributes = "", $valueTag = "span")
    {
        $this->mergeObjectVariables($objToAppend);
        $this->addValue($objToAppend->getHTML(), "", "", $valueClass, $valueAttributes, $valueTag);
    }


    /**
     * Add Value With Symbol at last Column inserted
     *
     * @param string $value value
     * @param string $valueTitle Title for value
     * @param string $valueLink link for value
     * @param string $valueClass class for value
     * @param string $valueAttributes attributes for value
     * @param string $symbolClass class of symbol (Ex: "fa fa-check")
     *
     */
    public function addValueWithSymbolClass($value = "", $valueTitle = "", $valueLink = "", $valueClass = "", $valueAttributes = "", $symbolClass = "")
    {
        $this->addValue('<i class="' . $symbolClass . '"></i> ' . $value, $valueTitle, $valueLink, $valueClass, $valueAttributes);
    }

    /**
     * Add Button at last Column inserted
     *
     * @param string $value value
     * @param string $valueTitle Title for value
     * @param string $valueLink link for value
     * @param string $valueClass class for value
     * @param string $valueAttributes attributes for value
     * @param string $symbolClass class of symbol (Ex: "fa fa-check")
     * @param string $containerClass class of container
     *
     */
    public function addButton($value = "", $valueTitle = "", $valueLink = "", $valueClass = "", $valueAttributes = "", $symbolClass = "", $containerClass = "")
    {
        if ($valueLink == "")
            $valueLink = "#";
        $this->addValue('<a href="' . $valueLink . '" class="btn ' . $valueClass . ' ' . $symbolClass . '" title="' . $valueTitle . '"' . $valueAttributes . '>' . $value . '</a> ', "", "", $containerClass, "");
    }

    /**
     * Check Show IF for fields and buttons
     *
     * @param array $record record to check
     * @param array | string $showIF
     *
     * @return boolean
     *
     */
    protected function checkShowIF($record, $showIF)
    {



        //set param
        $res = true;

        //check showIF
        if (is_string($showIF))
            $showIF = json_decode($showIF,true);

        if (is_array($showIF)) {
            foreach ($showIF as $key => $val) {
                if (is_array($val))
                {
                    $key = key($val);
                    $val = current($val);
                }
                $recVal = $this->K_COMMON->getVarArray($record, $key);
                if ((string)$recVal !== (string)$val)
                    $res = false; //it's ok for multiple condition

            }
        }

        return $res;
    }

    /**
     * Check Class IF for fields and buttons
     *
     * @param array $record record to check
     * @param array $classIF
     *
     * @return boolean
     *
     */
    protected function checkClassIF($record, $classIF)
    {



        //set param
        $res = "";

        if (is_array($classIF)) {
            foreach ($classIF as $class => $val) {
                $key = key($val);
                $val = current($val);

                $recVal = $this->K_COMMON->getVarArray($record, $key);
                if ((string)$recVal == (string)$val)
                    $res .= $class; //it's ok for multiple condition

            }
        }


        return $res;
    }
    /**
     * Check Hide IF for fields and buttons
     *
     * @param array $record record to check
     * @param array | string $showIF
     *
     * @return boolean
     *
     */
    protected function checkHideIF($record, $showIF)
    {



        //set param
        $res = true;

        //check showIF
        if (is_string($showIF))
            $showIF = json_decode($showIF,true);

        if (is_array($showIF)) {
            foreach ($showIF as $key => $val) {
                if (is_array($val))
                {
                    $key = key($val);
                    $val = current($val);
                }
                $recVal = $this->K_COMMON->getVarArray($record, $key);
                if ((string)$recVal == (string)$val)
                    $res = false; //it's ok for multiple condition

            }
        }

        return $res;
    }


    /**
     * Get parsed Table
     *
     */
    protected function parse()
    {
        return $this->template->pparse("body");
    }


    /**
     * Build Trigger Function Param
     *
     */
    protected function buildTriggerFunctionParam($ar)
    {

        $ret = '';

        foreach ($ar as $k => $v) {

            if ($ret != "")
                $ret .= ',';

            if (is_array($v))
            {

                $v = $this->buildTriggerFunctionParam($v);
                $ret .= '"'.$k.'" : '.$v;
            }
            else
            {
                if ($k == "qryAttribute")
                {
                    $v = "btnClicked.attr('" . $v . "')";
                    return $v;
                }
                else
                {
                    $ret .= '"'.$k.'" : '.$v;
                }
            }
        }

        return '{' . $ret . '}';

    }


    /**
     * Add Fields on Table
     *
     * @param array $record array of record
     * @param array $fields array of fields
     * @param array $dictionary array of dictionary
     * @param integer $typeOfField 1: noraml table field, 2: field for Group
     *
     */
    protected function addFieldsOnTable($record, $fields, $dictionary, $typeOfField)
    {

        //Include modules
        $objInput = $this->K_COMMON->getPluginObject("k_htmlcomponents", "input");



        //reset Button progression (is global of all buttons on list)
        if ($typeOfField == 1)
            $btnNum = 0;
        else
            $btnNum = 100;

        //for each field
        $fieldNum = 0;
        foreach ($fields as $field) {

            $fieldNum ++;

            //get field variables
            $type = $this->K_COMMON->getVarArray($field, "type");
            $value= $this->K_COMMON->getVarArray($field, "value");
            $beforeFieldValue = $this->K_COMMON->getVarArray($field, "beforeFieldValue");
            $afterFieldValue = $this->K_COMMON->getVarArray($field, "afterFieldValue");
            $qryField = $this->K_COMMON->getVarArray($field, "qryField");
            $formula = $this->K_COMMON->getVarArray($field, "formula");
            $fieldFunction = $this->K_COMMON->getVarArray($field, "fieldFunction");
            $buttons = $this->K_COMMON->getVarArray($field, "buttons");
            $input = $this->K_COMMON->getVarArray($field, "input");
            $classIF = $this->K_COMMON->getVarArray($field, "classIF");
            $showIF = $this->K_COMMON->getVarArray($field, "showIF");
            $hideIF = $this->K_COMMON->getVarArray($field, "hideIF");
            $columnClass = $this->K_COMMON->getVarArray($field, "columnClass");

            $classIFToAppend = $this->checkClassIF($record, $classIF);

            //add column
            if ($typeOfField == 1)
                $this->addColumn("", "", "", "", "", "", $columnClass . " " . $classIFToAppend);
            else
            {
                if ($fieldNum == 1)
                    $this->addColumn("", "", "", "", '', "", $columnClass . " " . $classIFToAppend, "span", "99");
            }

            //check if to get data
            if ($qryField != "")
                $value = $this->K_COMMON->getVarArray($record, $qryField);

            //check if function manipulate value
            if (is_array($fieldFunction)) {

                $fieldFunctionName = $fieldFunction[0];
                $fieldFunctionParam = [];
                foreach ($fieldFunction[1] as $el) {
                    $el = str_replace("[VALUE]", $value, $el);
                    $fieldFunctionParam[] = $el;
                }
                $value = call_user_func_array($fieldFunctionName, $fieldFunctionParam);

            }


            $showField1 = $this->checkShowIF($record, $showIF);
            $showField2 = $this->checkHideIF($record, $hideIF);

            //check if must add input
            if ($showField1 && $showField2)
                switch ($type) {
                    case "":
                    case "value":
                        if ($qryField != "")
                            $this->addValue($beforeFieldValue . $value . $afterFieldValue, "", "", "", "");
                        break;
                    case "static":
                        $this->addValue($beforeFieldValue . $value . $afterFieldValue, "", "", "", "");
                        break;
                    case "formula":
                        preg_match_all("/\[([^\]]*)\]/", $formula, $matches);
                        foreach ($matches[1] as $key => $val) {
                            $tmpVal = $this->K_COMMON->getVarArray($record, $val);
                            $formula = str_replace("[$val]", $tmpVal, $formula);

                        }

                        eval('$value = ' . $formula . ";");
                        $this->addValue($beforeFieldValue . $value . $afterFieldValue, "", "", "", "");

                        break;
                    case "dictionary":
                        if ($qryField != "") {
                            $value = $this->convertLabelDictionary(["dictionary" => $value], $dictionary);
                            $this->addValue($beforeFieldValue . $value . $afterFieldValue, "", "", "", "");
                        }
                        break;
                    case "state":

                        if ($qryField != "")
                            $this->addValueState($value);
                        break;
                    case "input":
                        if (is_array($input)) {

                            //set variables
                            $qryAttributes = $this->K_COMMON->getVarArray($input, "qryAttributes");
                            $dynamicAttributes = $this->K_COMMON->getVarArray($input, "dynamicAttributes");
                            $attributesString = "";

                            //check query attributes
                            if (is_array($qryAttributes)) {
                                foreach ($qryAttributes as $key => $val) {
                                    $attVal = $this->K_COMMON->getVarArray($record, $val);
                                    $attributesString .= " " . $key . "='" . $attVal . "'";
                                }
                            }

                            //check dynamic attributes
                            if (is_array($dynamicAttributes)) {
                                foreach ($dynamicAttributes as $key => $val) {
                                    $attributesString .= " " . $key . "='" . $val . "'";
                                }
                            }

                            //if qryFiled != "" set value for input
                            if (isset($input["qryField"]))
                                $input[$input["qryField"]] = $value;

                            //get input
                            $this->K_COMMON->concatVarArray($input, "attributes", $attributesString);
                            $objInput->getWO($input);

                            //Append Input
                            $this->addValue($objInput->getHTML());
                            $this->mergeObjectVariables($objInput);
                        }
                        break;
                }


            //check if must add buttons
            if (is_array($buttons)) {
                if (isset($buttons["button"])) //from xml
                {
                    if (key($buttons["button"]) == "0")
                        $buttons = $buttons["button"];
                    else
                        $buttons = [$buttons["button"]];
                }


                foreach ($buttons as $button) {
                    $btnNum++;

                    //get button data
                    $action = $this->K_COMMON->getVarArray($button, "action");
                    $icon = $this->K_COMMON->getVarArray($button, "icon");
                    $qryAttributes = $this->K_COMMON->getVarArray($button, "qryAttributes");
                    $dynamicAttributes = $this->K_COMMON->getVarArray($button, "dynamicAttributes");
                    $showIF = $this->K_COMMON->getVarArray($button, "showIF");
                    $hideIF = $this->K_COMMON->getVarArray($button, "hideIF");
                    $buttonParameters = $this->K_COMMON->getVarArray($button, "buttonParameters");
                    $attributesString = "";
                    $btnPar = $button;
                    $btnParDefault = [];
                    $btnPar["type"] = "btn";



                    //check query attributes
                    if (is_string($qryAttributes))
                        $qryAttributes = json_decode($qryAttributes,true);

                    //check dynamic attributes
                    if (is_string($dynamicAttributes))
                        $dynamicAttributes = json_decode($dynamicAttributes,true);

                    $showButton1 = $this->checkShowIF($record, $showIF);
                    $showButton2 = $this->checkHideIF($record, $hideIF);

                    //check button parameters
                    if (is_array($buttonParameters)) {
                        foreach ($buttonParameters as $key => $val) {
                            $recVal = $this->K_COMMON->getVarArray($record, $val);
                            $btnPar[$key] = $recVal;

                        }
                    }


                    if ($showButton1 && $showButton2) {
                        //check query attributes
                        if (is_array($qryAttributes)) {
                            foreach ($qryAttributes as $key => $val) {
                                $attVal = $this->K_COMMON->getVarArray($record, $val);
                                $attributesString .= " " . $key . "='" . $attVal . "'";
                            }
                        }

                        //check dynamic attributes
                        if (is_array($dynamicAttributes)) {
                            foreach ($dynamicAttributes as $key => $val) {
                                $attributesString .= " " . $key . "='" . $val . "'";
                            }
                        }
                        //Action
                        switch ($action) {
                            case "link":
                                $this->K_COMMON->concatVarArray($btnPar, "class", " btnLink");
                                $btnParDefault["size"] = "sm";
                                $btnParDefault["semanticColor"] = "secondary";
                                $btnPar["class"] .= " btnLink" . $btnNum;
                                break;
                            case "modal":
                                if ($icon != "")
                                    $btnParDefault["class"] = "btnModal";
                                else
                                    $btnParDefault["class"] = "btnModal fa fa-edit";
                                $btnParDefault["class"] .= " btnModal" . $btnNum;
                                $btnParDefault["size"] = "sm";
                                $btnParDefault["semanticColor"] = "secondary";

                                break;
                            case "modalConfirm":
                                if ($icon != "")
                                    $btnParDefault["class"] = "btnModalConfirm";
                                else
                                    $btnParDefault["class"] = "btnModalConfirm fa fa-check";
                                $btnParDefault["class"] .= " btnModalConfirm" . $btnNum;
                                $btnParDefault["size"] = "sm";
                                $btnParDefault["semanticColor"] = "secondary";

                                break;
                            case "download":
                                if ($icon != "")
                                    $btnParDefault["class"] = "btnDownload";
                                else
                                    $btnParDefault["class"] = "btnDownload fa fa-download";
                                $btnParDefault["class"] .= " btnDownload" . $btnNum;
                                $btnParDefault["size"] = "sm";
                                $btnParDefault["semanticColor"] = "secondary";

                                break;
                            case "edit":
                                if ($icon != "")
                                    $btnParDefault["class"] = "btnEdit";
                                else
                                    $btnParDefault["class"] = "btnEdit fa fa-edit";
                                $btnParDefault["class"] .= " btnEdit" . $btnNum;
                                $btnParDefault["size"] = "sm";
                                $btnParDefault["semanticColor"] = "secondary";

                                break;
                            case "copy":
                                if ($icon != "")
                                    $btnParDefault["class"] = "btnCopy";
                                else
                                    $btnParDefault["class"] = "btnCopy fa fa-copy";
                                $btnParDefault["class"] .= " btnCopy" . $btnNum;
                                $btnParDefault["size"] = "sm";
                                $btnParDefault["semanticColor"] = "secondary";

                                break;
                            case "editModal":
                                if ($icon != "")
                                    $btnParDefault["class"] = "btnEditModal";
                                else
                                    $btnParDefault["class"] = "btnEditModal fa fa-edit";
                                $btnParDefault["class"] .= " btnEditModal" . $btnNum;
                                $btnParDefault["size"] = "sm";
                                $btnParDefault["semanticColor"] = "secondary";

                                break;
                            case "remove":
                                if ($icon != "")
                                    $btnParDefault["class"] = "btnDelete";
                                else
                                {
                                    $btnParDefault["class"] = "btnDelete fa fa-remove";

                                }
                                $btnParDefault["class"] .= " btnDelete" . $btnNum;
                                $btnParDefault["size"] = "sm";
                                $btnParDefault["semanticColor"] = "secondary";

                                break;
                            case "refreshTableList":
                                if ($icon != "")
                                    $btnParDefault["class"] = "btnRefreshTableList";
                                else
                                {
                                    $btnParDefault["class"] = "btnRefreshTableList fa fa-refresh";

                                }
                                $btnParDefault["class"] .= " btnRefreshTableList" . $btnNum;
                                $btnParDefault["size"] = "sm";
                                $btnParDefault["semanticColor"] = "secondary";

                                break;
                            case "lock":
                                if ($icon != "")
                                    $btnParDefault["class"] = "btnLock";
                                else
                                {
                                    $btnParDefault["class"] = "btnLock fa fa-lock";

                                }
                                $btnParDefault["class"] .= " btnLock" . $btnNum;
                                $btnParDefault["size"] = "sm";
                                $btnParDefault["semanticColor"] = "secondary";

                                break;
                            case "unlock":
                                if ($icon != "")
                                    $btnParDefault["class"] = "btnUnlock";
                                else
                                {
                                    $btnParDefault["class"] = "btnUnlock fa fa-unlock";

                                }
                                $btnParDefault["class"] .= " btnUnlock" . $btnNum;
                                $btnParDefault["size"] = "sm";
                                $btnParDefault["semanticColor"] = "secondary";

                                break;
                            case "checkbox":
                                $btnClass = "";
                                if ($icon != "")
                                    $btnClass = "btnCheckbox";
                                else
                                    $btnClass = "btnCheckbox";
                                $btnClass .= " btnCheckbox" . $btnNum;

                                $this->K_COMMON->concatVarArray($btnPar, "class", " " . $btnClass );

                                $btnPar["type"] = "checkbox";
                                if (isset($btnPar["ajax"])) {
                                    unset($btnPar["ajax"]); //remove because some html components as "checkbox" use it
                                }

                                break;

                        }

                        //merge parameters
                        $btnPar = array_merge($btnParDefault, $btnPar);

                        //add button
                        $this->K_COMMON->concatVarArray($btnPar, "attributes", $attributesString);
                        $objInput->getWO($btnPar);

                        //Append Button
                        $this->addValue($objInput->getHTML());
                        $this->mergeObjectVariables($objInput);
                    }


                }
            }

        }
    }


    /**
     * Create Buttons Script
     *
     * @param strin $tableID table ID
     * @param array $buttons array of buttons
     * @param string $scriptString string of Script
     * @param string $triggerString string of Trigger Script
     * @param integer $typeOfField 1: noraml table field, 2: field for Group
     *
     */
    protected function createButtonsScript($tableID, $buttons, &$scriptString, &$triggerString, $typeOfField) {

        //reset Button progression (is global of all buttons on list)
        if ($typeOfField == 1)
            $btnNum = 0;
        else
            $btnNum = 100;

        if (!empty($buttons)) {

            if (isset($buttons["button"])) //from xml
            {
                if (key($buttons["button"]) == "0")
                    $buttons = $buttons["button"];
                else
                    $buttons = [$buttons["button"]];
            }


            foreach ($buttons as $button) {
                $btnNum++;

                //get button data
                $action = $this->K_COMMON->getVarArray($button, "action");
                $btnAjax = $this->K_COMMON->getVarArray($button, "ajax");
                $reloadOnSuccess = $this->K_COMMON->getVarArray($button, "reloadOnSuccess");

                if (is_string($btnAjax))
                    $btnAjax = json_decode($btnAjax, true);

                //Connection
                if (is_array($btnAjax)) {
                    $btnkcp = $this->K_COMMON->getVarArray($btnAjax, "kcp");
                    $btnkco = $this->K_COMMON->getVarArray($btnAjax, "kco");
                    $btnkca = $this->K_COMMON->getVarArray($btnAjax, "kca");


                } else {
                    $btnkcp = "";
                    $btnkco = "";
                    $btnkca = "";
                }

                //replace "\" on kco with double "\" for javascript
                $btnkco = str_replace("\\", "\\\\", $btnkco);


                //check query attributes for ajax call
                $qryAttributes = $this->K_COMMON->getVarArray($button, "qryAttributes");
                $staticAttributes = $this->K_COMMON->getVarArray($button, "staticAttributes");
                $dynamicAttributes = $this->K_COMMON->getVarArray($button, "dynamicAttributes");
                $idModal = $this->K_COMMON->getVarArray($button, "idModal");
                if (is_string($qryAttributes))
                    $qryAttributes = json_decode($qryAttributes, true);
                if (is_string($staticAttributes))
                    $staticAttributes = json_decode($staticAttributes, true);
                if (is_string($dynamicAttributes))
                    $dynamicAttributes = json_decode($dynamicAttributes, true);
                if ($idModal == "")
                    $idModal = "modalID";
                if (!is_array($qryAttributes))
                    $qryAttributes = [];
                if (!is_array($dynamicAttributes))
                    $dynamicAttributes = [];

                //merge qryAttibutes e dynamicAttributes because use the same method to get value
                $qryAttributes = array_merge($qryAttributes, $dynamicAttributes);

                $triggerFunction = $this->K_COMMON->getVarArray($btnAjax, "triggerFunction");
                $triggerFunctionParam = $this->K_COMMON->getVarArray($btnAjax, "triggerFunctionParam");
                if (is_array($triggerFunctionParam))
                {
                    $triggerFunctionParamJSON = $this->buildTriggerFunctionParam($triggerFunctionParam);

                }
                else
                    $triggerFunctionParamJSON = "{}";

                //Action
                switch ($action) {
                    case "modal":
                        //crate parameter for ajax call
                        $JSButtonAttributesString = 'idModal:"'.$idModal.'"';

                        if (is_array($qryAttributes)) {
                            foreach ($qryAttributes as $key => $val) {
                                $JSButtonAttributesString .= ', ' . $key . ': btnClicked.attr("' . $key . '")';
                            }
                        }

                        if (is_array($staticAttributes)) {
                            foreach ($staticAttributes as $key => $val) {
                                $JSButtonAttributesString .= ', ' . $key . ':"' . $val.'"';

                            }
                        }

                        $scriptString .= '
                                        t.modal'.$btnNum.' = function(){
                                            var btnClicked = $(this);
                                            var keyID = $(this).attr("keyID");
                                            waitingDialog.show("Loading...", {dialogSize: "sm", progressType: "warning"});
                                            $.ajax({
                                                  url: KXJS.buildAPICallUrl("KXCJS","' . $btnkcp . '","' . $btnkco . '","' . $btnkca . '"),
                                                data: {' . $JSButtonAttributesString . '}
                                            }).done(function(data) {
                                                r = $.parseJSON(data);
                                                waitingDialog.hide();
                                                if(r.success == "true"){
                                                    $(r.html).appendTo("body");
                                                    $("#'.$idModal.'").modal("show");
                                                    $("#'.$idModal.'").on("hidden.bs.modal", function (e) {
                                                        $("#'.$idModal.'").remove();
                                                    });
                                                    ' . ($triggerFunction != "" ? $triggerFunction . '('.$triggerFunctionParamJSON.');' : '') . '
                                                    }
                                                else
                                                    alert("Error");
                                            });
                                        }
                                    ';

                        $triggerString .= '$("#' . $tableID . '" + extraClass + " .btnModal'.$btnNum.'").click(t.modal'.$btnNum.');';
                        break;
                    case "modalConfirm":
                        //crate parameter for ajax call
                        $JSButtonAttributesString = 'idModal:"'.$idModal.'"';

                        if (is_array($qryAttributes)) {
                            foreach ($qryAttributes as $key => $val) {
                                $JSButtonAttributesString .= ', ' . $key . ': btnClicked.attr("' . $key . '")';
                            }
                        }
                        if (is_array($staticAttributes)) {
                            foreach ($staticAttributes as $key => $val) {
                                $JSButtonAttributesString .= ', ' . $key . ':"' . $val.'"';

                            }
                        }

                        $scriptString .= '
                                        t.modal'.$btnNum.' = function(){
                                            var btnClicked = $(this);
                                            var keyID = $(this).attr("keyID");
                                            waitingDialog.show("Loading ...", {dialogSize: "sm", progressType: "warning"});
                                            $.ajax({
                                                url: KXJS.buildAPICallUrl("KXCJS","k_htmlcomponents","tablelist","askConfirm"),
                                                data: {
                                                    type: "confirm",
                                                    idModal:"confirmModal"}
                                            }).done(function(data) {
                                                r = $.parseJSON(data);
                                                waitingDialog.hide();
                                                if(r.success == "true"){
                                                    $(r.html).appendTo("body");
                                                    $("#confirmModal").modal("show");
                                                    $("#confirmModal").on("hidden.bs.modal", function (e) {
                                                        $("#confirmModal").remove();
                                                    });
                                    
                                                    $("#confirmModal input[name=\"yes\"]").click
                                                    (
                                                        function()
                                                            {
                                                                $("#confirmModal").modal("hide");
                                                                waitingDialog.show("Please Wait ...", {dialogSize: "sm", progressType: "warning"});
                                                                $.ajax({
                                                                          url: KXJS.buildAPICallUrl("KXCJS","' . $btnkcp . '","' . $btnkco . '","' . $btnkca . '"),
                                                                        data: {' . $JSButtonAttributesString . '}
                                                                    }).done(function(data) {
                                                                        r = $.parseJSON(data);
                                                                        waitingDialog.hide();
                                                                        if(r.success == "true"){
                                                                        ' . ($triggerFunction != "" ? $triggerFunction . '('.$triggerFunctionParamJSON.');' : '
                                                                        ' . ($reloadOnSuccess == "1" ? 't.loadTableList();' : '').'
                                                                        if (typeof (r.message) != "undefined")
                                                                            alert(r.message)

                                                                        ') . '
                                                                            
                                                                       }
                                                                        else
                                                                            {
                                                                                if (typeof (r.message) != "undefined")
                                                                                    alert(r.message)
                                                                                else
                                                                                    alert("Error");
                                                                            }
                                                                    });
                                                            }
                                                    );
                                                }
                                                else
                                                    alert("Error Security");
                                            });
                                            
                                            
                                            
                                            
                                            
                                            
                                        }
                                    ';

                        $triggerString .= '$("#' . $tableID . '" + extraClass + " .btnModalConfirm'.$btnNum.'").click(t.modal'.$btnNum.');';
                        break;
                    case "download":

                        //crate parameter for ajax call
                        $JSButtonAttributesString = '"idModal='.$idModal . '"';

                        if (is_array($qryAttributes)) {
                            foreach ($qryAttributes as $key => $val) {

                                $JSButtonAttributesString .= ' + "&' . $key . '="+btnClicked.attr("' . $key . '")';
                            }
                        }
                        if (is_array($staticAttributes)) {
                            foreach ($staticAttributes as $key => $val) {
                                $JSButtonAttributesString .= ' + "&' . $key . '='.$val.'"';
                            }
                        }

                        $scriptString .= '
                                        t.modal'.$btnNum.' = function(){
                                            var btnClicked = $(this);
                                            var keyID = $(this).attr("keyID");
                                            waitingDialog.show("Loading...", {dialogSize: "sm", progressType: "warning"});
                                            $.ajax({
                                                url: KXJS.buildAPICallUrl("KXCJS","k_htmlcomponents","tablelist","askConfirm"),
                                                data: {
                                                    type: "confirm",
                                                    idModal:"confirmModal"}
                                            }).done(function(data) {
                                                r = $.parseJSON(data);
                                                waitingDialog.hide();
                                                if(r.success == "true"){
                                                    $(r.html).appendTo("body");
                                                    $("#confirmModal").modal("show");
                                                    $("#confirmModal").on("hidden.bs.modal", function (e) {
                                                        $("#confirmModal").remove();
                                                    });
                                    
                                                    $("#confirmModal input[name=\"yes\"]").click
                                                    (
                                                        function()
                                                            {
                                                                $("#confirmModal").modal("hide");
                                                                var varRequest = ' . $JSButtonAttributesString . ';
                                                                var url = KXJS.buildAPICallUrl("KXCDWF", "' . $btnkcp . '", "' . $btnkco . '", "' . $btnkca . '",varRequest);

                                                                KXJS.downloadFile(url);
                                                                
                                                                
                                                            }
                                                    );
                                                }
                                                else
                                                    alert("Error Security");
                                            });
                                            
                                            
                                            
                                            
                                            
                                            
                                        }
                                    ';

                        $triggerString .= '$("#' . $tableID . '" + extraClass + " .btnDownload'.$btnNum.'").click(t.modal'.$btnNum.');';
                        break;
                    case "link":
                        $urlButton = $this->K_COMMON->getVarArray($button, "url");
                        $urlBaseType = $this->K_COMMON->getVarArray($button, "urlBaseType");
                        $urlseparator = $this->K_COMMON->getVarArray($button, "urlSeparator");
                        $urlTarget = $this->K_COMMON->getVarArray($button, "urlTarget");
                        if ($urlTarget == "")
                            $urlTarget = "_blank";

                        //crate parameter for link
                        $JSButtonAttributesString = '';
                        if (is_array($qryAttributes)) {
                            //$this->K_COMMON->printArray($qryAttributes);
                            foreach ($qryAttributes as $key => $val) {
                                if (empty($key))
                                    $key = "";


                                if ($urlseparator == "&")
                                    $JSButtonAttributesString .= ' + "&' . $key . '="+btnClicked.attr("' . $key . '")';
                                else
                                {
                                    if ($key != "")
                                    {
                                        if ($urlButton == "" && $JSButtonAttributesString == "")
                                            $JSButtonAttributesString .= ' + "' . $key . '/"+btnClicked.attr("' . $key . '")';
                                        else
                                            $JSButtonAttributesString .= ' + "/' . $key . '/"+btnClicked.attr("' . $key . '")';
                                    }
                                    else
                                    {
                                        if ($urlButton == "" && $JSButtonAttributesString == "")
                                            $JSButtonAttributesString .= ' + btnClicked.attr("' . $key . '")';
                                        else
                                            $JSButtonAttributesString .= ' + "/"+btnClicked.attr("' . $key . '")';
                                    }
                                }

                            }
                        }
                        if (is_array($staticAttributes)) {
                            foreach ($staticAttributes as $key => $val) {
                                if ($urlseparator == "&")
                                    $JSButtonAttributesString .= ' + "&' . $key . '='.$val.'"';
                                else
                                {
                                    if ($urlButton == "" && $JSButtonAttributesString == "")
                                        $JSButtonAttributesString .= ' + "' . $key . '/'.$val.'"';
                                    else
                                        $JSButtonAttributesString .= ' + "/' . $key . '/'.$val.'"';
                                }

                            }
                        }
                        //echo "BASE TYPE = " . $urlBaseType .  "<br>";
                        switch ($urlBaseType)
                        {
                            case "panel":
                                $scriptString .= '
                                        t.clickBtnLink'.$btnNum.' = function() {
                                            var btnClicked = $(this);
                                            var keyID = $(this).attr("keyID");
                                            location.href = KXJS.panelUrlBase + "' . $urlButton . '"' . $JSButtonAttributesString . ';
                                        }
                                    ';
                                break;
                            case "panelPlugin":

                                $scriptString .= '
                                        t.clickBtnLink'.$btnNum.' = function() {
                                            var btnClicked = $(this);
                                            var keyID = $(this).attr("keyID");
                                            location.href = KXJS.panelUrlPluginBase + "' . $urlButton . '"' . $JSButtonAttributesString . ';
                                        }
                                    ';
                                break;
                            case "panelActual":

                                $scriptString .= '
                                        t.clickBtnLink'.$btnNum.' = function() {
                                            var btnClicked = $(this);
                                            var keyID = $(this).attr("keyID");
                                            location.href = KXJS.panelUrlActual + "' . $urlButton . '"' . $JSButtonAttributesString . ';
                                        }
                                    ';
                                break;
                            case "":
                                $scriptString .= '
                                        t.clickBtnLink'.$btnNum.' = function() {
                                            var btnClicked = $(this);
                                            var keyID = $(this).attr("keyID");
                                            var urlToOpen = "'.$urlButton.'"' . $JSButtonAttributesString . ';
                                            window.open(urlToOpen,"'.$urlTarget.'");                     
                                        }
                                    ';
                                break;

                        }

                        $triggerString .= '$("#' . $tableID . '" + extraClass + " .btnLink'.$btnNum.'").click(t.clickBtnLink'.$btnNum.');';


                        break;
                    case "edit":
                        //crate parameter for link
                        $JSButtonAttributesString = '';
                        if (is_array($qryAttributes)) {

                            foreach ($qryAttributes as $key => $val) {
                                $JSButtonAttributesString .= ' + "/' . $key . '/"+btnClicked.attr("' . $key . '")';
                            }
                        }
                        if (is_array($staticAttributes)) {
                            foreach ($staticAttributes as $key => $val) {
                                $JSButtonAttributesString .= ' + "/' . $key . '/'.$val.'"';

                            }
                        }
                        $scriptString .= '
                                        t.editData'.$btnNum.' = function() {
                                            var btnClicked = $(this);
                                            var keyID = $(this).attr("keyID");
                                            location.href = KXJS.panelUrlActual + "get/" + keyID ' . $JSButtonAttributesString . ';
                                            }
                                    ';


                        $triggerString .= '$("#' . $tableID . '" + extraClass + " .btnEdit'.$btnNum.'").click(t.editData'.$btnNum.');';

                        break;
                    case "copy":
                        //crate parameter for link
                        $JSButtonAttributesString = '';
                        if (is_array($qryAttributes)) {
                            foreach ($qryAttributes as $key => $val) {
                                $JSButtonAttributesString .= ' + "/' . $key . '/"+btnClicked.attr("' . $key . '")';
                            }
                        }

                        if (is_array($staticAttributes)) {
                            foreach ($staticAttributes as $key => $val) {
                                $JSButtonAttributesString .= ' + "/' . $key . '/'.$val.'"';

                            }
                        }
                        $scriptString .= '
                                        t.copyData'.$btnNum.' = function() {
                                            var btnClicked = $(this);
                                            var keyID = $(this).attr("keyID");
                                            location.href = KXJS.panelUrlActual + "copy/" + keyID ' . $JSButtonAttributesString . ';
                                            }
                                    ';

                        $triggerString .= '$("#' . $tableID . '" + extraClass + " .btnCopy'.$btnNum.'").click(t.copyData'.$btnNum.');';


                        break;
                    case "editModal":

                        //crate parameter for ajax call
                        $JSButtonAttributesString = 'idModal:"'.$idModal.'"';

                        if (is_array($qryAttributes)) {
                            foreach ($qryAttributes as $key => $val) {
                                $JSButtonAttributesString .= ', ' . $key . ': btnClicked.attr("' . $key . '")';
                            }
                        }
                        if (is_array($staticAttributes)) {
                            foreach ($staticAttributes as $key => $val) {
                                $JSButtonAttributesString .= ', ' . $key . ':"' . $val.'"';

                            }
                        }

                        $scriptString .= '
                                        t.editModal'.$btnNum.' = function(){
                                            var btnClicked = $(this);
                                            var keyID = $(this).attr("keyID");
                                            waitingDialog.show("Loading...", {dialogSize: "sm", progressType: "warning"});
                                            $.ajax({
                                                  url: KXJS.buildAPICallUrl("KXCJS","' . $btnkcp . '","' . $btnkco . '","' . $btnkca . '"),
                                                data: {' . $JSButtonAttributesString . '}
                                            }).done(function(data) {
                                                r = $.parseJSON(data);
                                                waitingDialog.hide();
                                                if(r.success == "true"){
                                                    $(r.html).appendTo("body");
                                                    $("#'.$idModal.'").modal("show");
                                                    $("#'.$idModal.'").on("hidden.bs.modal", function (e) {
                                                        $("#'.$idModal.'").remove();
                                                    });
                                                    ' . ($triggerFunction != "" ? $triggerFunction . '('.$triggerFunctionParamJSON.');' : '') . '
                                                    }
                                                else
                                                    alert("Error");
                                            });
                                        }
                                    ';

                        $triggerString .= '$("#' . $tableID . '" + extraClass + " .btnEditModal'.$btnNum.'").click(t.editModal'.$btnNum.');';


                        break;
                    case "remove":

                        $confirmType = $this->K_COMMON->getVarArray($button, "confirmType");

                        if ($confirmType == "")
                            $confirmType = "delete";

                        //crate parameter for ajax call
                        $JSButtonAttributesString = "";
                        if (is_array($qryAttributes)) {
                            foreach ($qryAttributes as $key => $val) {
                                if ($JSButtonAttributesString != "")
                                    $JSButtonAttributesString .= ', ' . $key . ': btnDelete.attr("' . $key . '")';
                                else
                                    $JSButtonAttributesString .= ' ' . $key . ': btnDelete.attr("' . $key . '")';
                            }
                        }
                        if (is_array($staticAttributes)) {
                            foreach ($staticAttributes as $key => $val) {
                                if ($JSButtonAttributesString != "")
                                    $JSButtonAttributesString .= ', ' . $key . ':"' . $val.'"';
                                else
                                    $JSButtonAttributesString .= ' ' . $key . ':"' . $val.'"';

                            }
                        }

                        $scriptString .= '
                                        t.askDelete'.$btnNum.' = function(){
                                            var btnDelete = $(this);
                                            var keyID = $(this).attr("keyID");
                                            var rowToDelete = $(this).closest("tr");
                                            if(keyID == ""){
                                                alert("error");
                                                return;
                                            }
                                            waitingDialog.show("Loading...", {dialogSize: "sm", progressType: "warning"});
                                            $.ajax({
                                                url: KXJS.buildAPICallUrl("KXCJS","k_htmlcomponents","tablelist","askConfirm"),
                                                data: {
                                                    type: "'.$confirmType.'",
                                                    idModal:"deleteModal"}
                                            }).done(function(data) {
                                                r = $.parseJSON(data);
                                                waitingDialog.hide();
                                                if(r.success == "true"){
                                                    $(r.html).appendTo("body");
                                                    $("#deleteModal").modal("show");
                                                    $("#deleteModal").on("hidden.bs.modal", function (e) {
                                                        $("#deleteModal").remove();
                                                    });
                                    
                                                    $("#deleteModal input[name=\"yes\"]").click
                                                    (
                                                        function()
                                                            {
                                                                $("#deleteModal").modal("hide");
                                                                t.deleteRecord(keyID,rowToDelete,btnDelete);
                                                            }
                                                    );
                                                }
                                                else
                                                    alert("Error Security");
                                            });
                                        }
                                        
                                        t.deleteRecord = function(keyID,rowToDelete,btnDelete){
                                            $.ajax({
                                                url: KXJS.buildAPICallUrl("KXCJS","' . $btnkcp . '","' . $btnkco . '","' . $btnkca . '"),
                                                data: {' . $JSButtonAttributesString . '}
                                        
                                            }).done(function(data) {
                                                var r = $.parseJSON(data);
                                                if(r.success == "true")	
                                                    rowToDelete.remove();
                                                    '.($reloadOnSuccess == "1" ? 't.loadTableList();' : '').'
                                                    
                                            });
                                        }
                                    ';


                        $triggerString .= '$("#' . $tableID . '" + extraClass + " .btnDelete'.$btnNum.'").click(t.askDelete'.$btnNum.');';


                        break;
                    case "refreshTableList":

                        $tableIDToReload = $this->K_COMMON->getVarArray($button, "tableID");

                        //crate parameter for ajax call
                        $JSButtonAttributesString = "";
                        if (is_array($qryAttributes)) {
                            foreach ($qryAttributes as $key => $val) {
                                if ($JSButtonAttributesString != "")
                                    $JSButtonAttributesString .= ', ' . $key . ': btnRefreshTableList.attr("' . $key . '")';
                                else
                                    $JSButtonAttributesString .= ' ' . $key . ': btnRefreshTableList.attr("' . $key . '")';
                            }
                        }
                        if (is_array($staticAttributes)) {
                            foreach ($staticAttributes as $key => $val) {
                                if ($JSButtonAttributesString != "")
                                    $JSButtonAttributesString .= ', ' . $key . ':"' . $val.'"';
                                else
                                    $JSButtonAttributesString .= ' ' . $key . ':"' . $val.'"';

                            }
                        }

                        $functionTempReload = '';
                        if (is_array($tableIDToReload))
                        {
                            foreach ($tableIDToReload as $idR)
                            {
                                $functionTempReload .= 'kx_tablelist_script_' . $idR . '.loadTableList(options);';
                            }
                        }
                        else
                            $functionTempReload = 'kx_tablelist_script_' . $tableIDToReload . '.loadTableList(options);';


                        $scriptString .='
                                        
                                    t.refreshTableList'.$btnNum.' = function(){
                                            var btnRefreshTableList = $(this);
                                            var options = {};
                                            options.extraParam = {' . $JSButtonAttributesString . '};
                                    '.$functionTempReload.'
                                    return;

}
                                    ';

                        $triggerString .= '$("#' . $tableID . '" + extraClass + " .btnRefreshTableList'.$btnNum.'").click(t.refreshTableList'.$btnNum.');';


                        break;
                    case "lock":
                        //crate parameter for ajax call
                        $JSButtonAttributesString = "";
                        if (is_array($qryAttributes)) {
                            foreach ($qryAttributes as $key => $val) {
                                if ($JSButtonAttributesString != "")
                                    $JSButtonAttributesString .= ', ' . $key . ': btnLock.attr("' . $key . '")';
                                else
                                    $JSButtonAttributesString .= ' ' . $key . ': btnLock.attr("' . $key . '")';
                            }
                        }
                        if (is_array($staticAttributes)) {
                            foreach ($staticAttributes as $key => $val) {
                                if ($JSButtonAttributesString != "")
                                    $JSButtonAttributesString .= ', ' . $key . ':"' . $val.'"';
                                else
                                    $JSButtonAttributesString .= ' ' . $key . ':"' . $val.'"';

                            }
                        }
                        $scriptString .= '
                                        t.askLock'.$btnNum.' = function(){
                                            var btnLock = $(this);
                                            var keyID = $(this).attr("keyID");
                                            if(keyID == ""){
                                                alert("error");
                                                return;
                                            }
                                            waitingDialog.show("Processing...", {dialogSize: "sm", progressType: "warning"});
                                            $.ajax({
                                                url: KXJS.buildAPICallUrl("KXCJS","k_htmlcomponents","tablelist","askConfirm"),
                                                data: {
                                                    type: "confirm",
                                                    idModal:"IDModal"}
                                            }).done(function(data) {
                                                r = $.parseJSON(data);
                                                waitingDialog.hide();
                                                if(r.success == "true"){
                                                    $(r.html).appendTo("body");
                                                    $("#IDModal").modal("show");
                                                    $("#IDModal").on("hidden.bs.modal", function (e) {
                                                        $("#IDModal").remove();
                                                    });
                                    
                                                    $("#IDModal input[name=\"yes\"]").click
                                                    (
                                                        function()
                                                            {
                                                                $("#IDModal").modal("hide");
                                                                t.lockRecord(btnLock);
                                                            }
                                                    );
                                                }
                                                else
                                                    alert("Error Security");
                                            });
                                        }
                                        
                                        t.lockRecord = function(btnLock){
                                            $.ajax({
                                                url: KXJS.buildAPICallUrl("KXCJS","' . $btnkcp . '","' . $btnkco . '","' . $btnkca . '"),
                                                data: {' . $JSButtonAttributesString . '}
                                        
                                            }).done(function(data) {
                                                var r = $.parseJSON(data);
                                                if(r.success == "true")	
                                                    t.ready();
                                            });
                                        }
                                    ';

                        $triggerString .= '$("#' . $tableID . '" + extraClass + " .btnLock'.$btnNum.'").click(t.askLock'.$btnNum.');';


                        break;
                    case "unlock":
                        //crate parameter for ajax call
                        $JSButtonAttributesString = "";
                        if (is_array($qryAttributes)) {
                            foreach ($qryAttributes as $key => $val) {
                                if ($JSButtonAttributesString != "")
                                    $JSButtonAttributesString .= ', ' . $key . ': btnUnlock.attr("' . $key . '")';
                                else
                                    $JSButtonAttributesString .= ' ' . $key . ': btnUnlock.attr("' . $key . '")';
                            }
                        }
                        if (is_array($staticAttributes)) {
                            foreach ($staticAttributes as $key => $val) {
                                if ($JSButtonAttributesString != "")
                                    $JSButtonAttributesString .= ', ' . $key . ':"' . $val.'"';
                                else
                                    $JSButtonAttributesString .= ' ' . $key . ':"' . $val.'"';

                            }
                        }
                        $scriptString .= '
                                        t.askUnlock'.$btnNum.' = function(){
                                            var btnUnlock = $(this);
                                            var keyID = $(this).attr("keyID");
                                            if(keyID == ""){
                                                alert("error");
                                                return;
                                            }
                                            waitingDialog.show("Processing...", {dialogSize: "sm", progressType: "warning"});
                                            $.ajax({
                                                url: KXJS.buildAPICallUrl("KXCJS","k_htmlcomponents","tablelist","askConfirm"),
                                                data: {
                                                    type: "confirm",
                                                    idModal:"IDModal"}
                                            }).done(function(data) {
                                                r = $.parseJSON(data);
                                                waitingDialog.hide();
                                                if(r.success == "true"){
                                                    $(r.html).appendTo("body");
                                                    $("#IDModal").modal("show");
                                                    $("#IDModal").on("hidden.bs.modal", function (e) {
                                                        $("#IDModal").remove();
                                                    });
                                    
                                                    $("#IDModal input[name=\"yes\"]").click
                                                    (
                                                        function()
                                                            {
                                                                $("#IDModal").modal("hide");
                                                                t.unlockRecord(btnUnlock);
                                                            }
                                                    );
                                                }
                                                else
                                                    alert("Error Security");
                                            });
                                        }
                                        
                                        t.unlockRecord = function(btnUnlock){
                                            $.ajax({
                                                url: KXJS.buildAPICallUrl("KXCJS","' . $btnkcp . '","' . $btnkco . '","' . $btnkca . '"),
                                                data: {' . $JSButtonAttributesString . '}
                                        
                                            }).done(function(data) {
                                                var r = $.parseJSON(data);
                                                if(r.success == "true")	
                                                    t.ready();
                                            });
                                        }
                                    ';

                        $triggerString .= '$("#' . $tableID . '" + extraClass + " .btnUnlock'.$btnNum.'").click(t.askUnlock'.$btnNum.');';


                        break;
                    case "checkbox":
                        $refreshRow = $this->K_COMMON->getVarArray($button, "refreshRow");

                        //crate parameter for ajax call
                        $JSButtonAttributesString = "";
                        if (is_array($qryAttributes)) {
                            foreach ($qryAttributes as $key => $val) {
                                if ($JSButtonAttributesString != "")
                                    $JSButtonAttributesString .= ', ' . $key . ': btnClicked.attr("' . $key . '")';
                                else
                                    $JSButtonAttributesString .= ' ' . $key . ': btnClicked.attr("' . $key . '")';
                            }
                        }
                        if (is_array($staticAttributes)) {
                            foreach ($staticAttributes as $key => $val) {
                                if ($JSButtonAttributesString != "")
                                    $JSButtonAttributesString .= ', ' . $key . ':"' . $val.'"';
                                else
                                    $JSButtonAttributesString .= ' ' . $key . ':"' . $val.'"';

                            }
                        }

                        $scriptString .= '
                                        t.checkboxClick'.$btnNum.' = function(){
                                        
                                            var btnClicked = $(this);
                                            var keyID = $(this).attr("keyID");
                                            var val = 0;
                                            if ($(this).is(":checked")) val = 1;
                                            
                                            waitingDialog.show("Saving...", {dialogSize: "sm", progressType: "warning"});
                                            
                                            
                                            $.ajax({
                                                url: KXJS.buildAPICallUrl("KXCJS","' . $btnkcp . '","' . $btnkco . '","' . $btnkca . '","checked="+val),
                                                data: {' . $JSButtonAttributesString . '}
                                        
                                            }).done(function(data) {
                                                var r = $.parseJSON(data);
                                                waitingDialog.hide();
                                                funcParam = '.$triggerFunctionParamJSON.';
                                                funcParam.rowProgression = btnClicked.closest("tr").attr("data-rowprogression");
                                                
                                                
                                                ' . ($refreshRow != "" ? '
                                                        kx_tablelist_script_tableList.loadTableListSingleRow({rowProgression: funcParam.rowProgression, extraParam : funcParam});
                                                
                                                ' : '') . '
                                                
                                                
                                                
                                                ' . ($triggerFunction != "" ? $triggerFunction . '(funcParam);' : '') . '
                                                
                                                
                                            });
                                            
                                            
                                            
                                            
                                        }
                                        
                                       
                                    ';
                        $triggerString .= '$("#' . $tableID . '" + extraClass + " .btnCheckbox'.$btnNum.'").click(t.checkboxClick'.$btnNum.');';
                        break;

                }

            }
        }
    }

}


?>