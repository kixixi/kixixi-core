<?
/**
 * KIXIXI HTML Components (heading)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage objects\alert
 * @since 1.00
 */
namespace k_htmlcomponents\objects\general\heading;

class obj extends \k_webmanager\objects\general\web_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";


    /**
     * get an HTML heading
     *
     * @since 1.00
     * @access public
     *
     * @param array $arrayVariable
     * The following directives can be used in the array variables:<br>
     *    *    id: id of html object
     *    *    title: title of heading
     *    *    importance: 1 to 6
     *    *    semanticColor: optional set color of button by bootstrap semantic purpose (primary, secondary, success, info, warning, danger, link)
     *    *    align: align
     *    *    style: style
     *    *    class: class
     *
     * @return object
     */
    public function get($arrayVariable = [])
    {


        //set variables
        $id = $this->K_COMMON->getVarArray($arrayVariable, "id");
        $title = $this->K_COMMON->getVarArray($arrayVariable, "title");
        $importance = $this->K_COMMON->getVarArray($arrayVariable, "importance");
        $semanticColor = $this->K_COMMON->getVarArray($arrayVariable, "semanticColor");
        $align = $this->K_COMMON->getVarArray($arrayVariable, "align");
        $style = $this->K_COMMON->getVarArray($arrayVariable, "style");
        $class = $this->K_COMMON->getVarArray($arrayVariable, "class");
        $attributes = "";

        //check variables
        if ($id == "")
            $id = "heading_" . uniqid();
        if ($importance == "")
            $importance = "1";
        if ($align != "")
            $style = "text-align:" . $align . ";" . $style;
        else
            $style = "text-align:" . "center" . ";" . $style;

        //style
        if ($style != "")
            $attributes .= 'style = "'.$style.'"';

        if ($semanticColor != "")
            $class .= " text-" . $semanticColor;


        // create html string
        $this->appendHTML("", '<h'.$importance.' id="'.$id.'" ' . $attributes . ' class="' . $class .'" >'.$title.'</h'.$importance.'>');


        // return
        return $this;
    }

}

?>

