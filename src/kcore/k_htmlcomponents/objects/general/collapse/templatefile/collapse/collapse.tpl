<!-- BEGIN COLLAPSE -->
<div id="{COLLAPSE.collapseID}" class="panel-group {COLLAPSE.collapseClass}">
    <!-- BEGIN ITEM -->
    <div class="panel panel-default  {COLLAPSE.ITEM.classPanel}">
        <div class="panel-heading  {COLLAPSE.ITEM.classHeading}" id="{COLLAPSE.ITEM.titleID}">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#{COLLAPSE.collapseID}" href="#{COLLAPSE.ITEM.contentID}">
                    {COLLAPSE.ITEM.title}
                </a>
            </h4>
        </div>
        <div id="{COLLAPSE.ITEM.contentID}" class="panel-collapse collapse" aria-labelledby="{COLLAPSE.ITEM.titleID}">
            <div id="{COLLAPSE.ITEM.bodyID}" class="panel-body  {COLLAPSE.ITEM.classBody}">
                {COLLAPSE.ITEM.content}
            </div>
        </div>
    </div>
    <!-- END ITEM -->
</div>
<!-- END COLLAPSE -->
