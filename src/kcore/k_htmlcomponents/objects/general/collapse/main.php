<?
namespace k_htmlcomponents\objects\general\collapse;
class obj extends \k_webmanager\objects\general\general_object\obj
{
	/**
	 * Version of object
	 *
	 * @since 1.00
	 * @access public
	 * @var string
	 */
	public $ver = "1.0";


	/**
	 * Template
	 *
	 * @since 1.00
	 * @access public
	 * @var template
	 */
	protected $template;

	/**
	 * If is true the collapse is created
	 *
	 * @since 1.00
	 * @access public
	 * @var template
	 */
	var $collapse = false;

	/**
	 * if true have the card classes
	 *
	 * @since 1.00
	 * @access public
	 * @var template
	 */
	var $asCard = false;

	/**
	 * ID of collapse
	 *
	 * @since 1.00
	 * @access public
	 * @var template
	 */
	var $collapseID;

	/**
	 * Construct
	 *
	 */
	public function __construct($AV = []) {

		parent::__construct();

		//get template
		$this->template = $this->getTemplateLayout("collapse");

		$this->create($AV);
	}

	/**
	 * get a collapse object (web object)
	 * This function is used from template manager,
	 * otherwise you can use single function for create it, and after use "get" to get table object.
	 *
	 * @param array $AV array of table parameters.<br>
	 *  Parameters: <br>
	 *
	 * *    id    : id of collapse
	 * *    contentAttributes    : attributes for content
	 * *    items    : array of fields (columns)
	 *   *    classNav : class of tab
	 *   *    classContent : class of content
	 *   *    id : id content
	 *   *    label : label
	 *   *    html : html of content
	 *
	 * @return web_object
	 */
	public function get($AV = [])
	{

		//if $AV not empty create with $AV Parameters
		if (!empty($AV))
			$this->create($AV);

		//Append HTML of builded
		$this->appendHTML("",$this->parse());

		//Return Object
		return $this;
	}

	/**
	 * create new COLLAPSE
	 *
	 * @param array $arrayVariable array of table parameters.<br>
	 * Please refer to "get" function for correct parameters
	 *
	 */
	public function create($arrayVariable = [])
	{

		//reset template
		$this->template->destroy();
		$this->parsed = false;

		//set variables
		$collapseID = $this->K_COMMON->getVarArray($arrayVariable, "id");
		$collapseClass = $this->K_COMMON->getVarArray($arrayVariable,"class");
		$items = $this->K_COMMON->getVarArray($arrayVariable, "items");
		$dictionary = $this->K_COMMON->getVarArray($arrayVariable,"dictionary");
		$asCard = $this->K_COMMON->getVarArray($arrayVariable,"asCard");

		//create TAB ID
		if ($collapseID == "") {
			$collapseID = "collapse_" . uniqid();
		}
		$this->collapseID = $collapseID;
		$this->collapse = true;

		//set default HTML ID
		$this->setIDObject($this->collapseID);
		$this->setIDContent($this->collapseID);

		//check variables
		if ($asCard === true || $asCard === "1")
			$this->asCard = true;


		//create BLOCK
		$arraySet = array();
		$arraySet["collapseID"] = $collapseID;
		$arraySet["collapseClass"] = $collapseClass;
		$this->template->assign_block_vars("COLLAPSE", $arraySet);

		//create tabs
		if (is_array($items))
		{

			if (isset($items["item"]))
				$items = $items["item"];

			foreach ($items as $item)
			{
				//Translate dictionary
				$dictionaryToTranslate = $this->K_COMMON->getVarArray($item,"dictionary");
				if (is_array($dictionaryToTranslate))
					foreach ($dictionaryToTranslate as $key => $value)
					{
						$item[$key] = $this->convertLabelDictionary(["dictionary" => $value], $dictionary) ;
					}
				$this->addItem($item);
			}

		}
	}

	/**
	 * Add ITEM
	 *
	 * @param array $arrayParam array of table parameters.<br>
	 * Please refer to "get" function for correct parameters
	 *
	 * @return array
	 */
	protected function addItem($arrayParam = array())
	{
		//set variables
		$class = $this->K_COMMON->getVarArray($arrayParam, "class");
		$id = $this->K_COMMON->getVarArray($arrayParam, "id");
		$label = $this->K_COMMON->getVarArray($arrayParam, "label");
		$html = $this->K_COMMON->getVarArray($arrayParam, "html");


		//check id content
		if ($id == "") {
			$id = "collapse_content_" . uniqid();
		}

		//setVariable
		$arraySet = array();
		$arraySet["titleID"] = $id . "_T";
		$arraySet["bodyID"] = $id . "_B";
		$arraySet["contentID"] = $id;
		$arraySet["title"] = $label;
		$arraySet["content"] = $html;

		if ($this->asCard)
		{
			$arraySet["classPanel"] = "card";
			$arraySet["classHeading"] = "card-header";
			$arraySet["classBody"] = "card-body";
		}


		$this->template->assign_block_vars("COLLAPSE.ITEM",$arraySet);

		return ["contentID" => $id];


	}




	/**
	 * Get HTML of a list group well formatted, using bootstrap .
	 *
	 * @return array with html.
	 */
	public function parse() {
		return $this->template->pparse("body");
	}
}
?>

