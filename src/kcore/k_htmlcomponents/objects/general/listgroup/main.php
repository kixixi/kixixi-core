<?
/**
 * KIXIXI HTML Components (listgroup)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage objects\listgroup
 * @since 1.00
 */
namespace k_htmlcomponents\objects\general\listgroup;

class obj extends \k_webmanager\objects\general\general_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";


    /**
     * Template
     *
     * @since 1.00
     * @access public
     * @var template
     */
    protected $template;


    function __construct($AV = [])
    {
        parent::__construct();
        $this->template = $this->getTemplateLayout("template");
        if (!empty($AV))
            $this->create($AV);
    }

    /**
     * Get HTML of a list group well formatted, using bootstrap .
     *
     * @param array $arrayVariable array of parameters
     * The following parameters are accepted:<br>
     *    *    id (optional): the id of object
     *    *    class (optional): class of the object
     *    *    attributes (optional): additional attributes
     *    *    html (optional): content of the column
     *    *    items (optional): array of items (may be button type or link type)
     *    *    values (optional): array of values (will be converted in items for list group)
     *    *    addingValues: array with fields for adding
     * The following directives can be used in the array variables:<br>
     *    *    fields (optional): array with input fields
     *    *    valuesHName (optional): name of valuesH (the hidden name of form)
     *    *    associative (optional): 1 if you want an associative array (default 0)
     *
     * * Example: $OBJTableList->appendListGroup(
     *  [
     *   "id"=>"lgroup"
     *  , "values" => ["a":"1","b":2"]
     * , "addingValues" =>
     *  [
     *   "valuesHName" => "parameter"
     *  ]
     * );
     *
     * * Example: $OBJTableList->appendListGroup(
     *  [
     *   "id"=>"lgroup"
     *  , "values" =>
     *  [
     *      [0] => ["name" => "john", "email":"john@john.com"]
     *      [1] => ["name" => "adam", "email":"adam@adam.com"]
     *  ]
     * , "addingValues" =>
     *  [
     *   "valuesHName" => "parameter"
     *  ]
     * );
     *
     *
     * @return array with html.
     */
    public function create($arrayVariable = [])
    {

        //reset template
        $this->template->destroy();

        //include css
        $this->addHeadFile([$this->getPathFileCSS("template")]);

        //get variables
        $id = $this->K_COMMON->getVarArray($arrayVariable, "id");
        $class = $this->K_COMMON->getVarArray($arrayVariable, "class");
        $attributes = $this->K_COMMON->getVarArray($arrayVariable, "attributes");
        $html = $this->K_COMMON->getVarArray($arrayVariable, "html");
        $items = $this->K_COMMON->getVarArray($arrayVariable, "items");
        $values = $this->K_COMMON->getVarArray($arrayVariable, "values");
        $addingValues = $this->K_COMMON->getVarArray($arrayVariable, "addingValues");
        $valuesJSON = "";
        $nFields = 1;
        //check variables
        if ($id == "") {
            $id = "listgroup_" . uniqid();
        }
        if ($attributes != "")
            $attributes = " " . $attributes;

        $class .= " kxlistgroup";

        //set variables for adding
        if (is_array($addingValues)) {

            $name = $id;
            $fields = $this->K_COMMON->getVarArray($addingValues, "fields");

            if (!is_array($fields)) {
                $fields = [
                    ["type" => "text", "labelInput" => "Code", "id" => $name . "Code", "name" => "value", "value" => ""]
                    , ["type" => "text", "labelInput" => "Text", "id" => $name . "Text", "name" => "text", "value" => ""]
                ];
            }

            //set n° fields
            $nFields = count($fields);
        }

        //check values
        if (is_string($values) && !empty($values)) {
            $valuesJSON = $values;
            $values = json_decode($values, true);

        } else
            if (is_array($values))
                $valuesJSON = json_encode($values);

        //convert values to array with correct fields name
        if (!empty($values)) {

            $values_tmp = $values;
            if (!is_array(current($values_tmp))) {
                $values = [];
                foreach ($values_tmp as $itemKey => $itemValue) {
                    $values[] = [$fields[0]["name"] => $itemKey, $fields[1]["name"] => $itemValue];

                }
            }

            $valuesJSON = json_encode($values);

        }

        $this->appendRow("",["id"=>$id."_rowContainerField","style"=>"border:1px solid #e1e1e1; margin:0px; margin-bottom:5px;"]);
        $this->appendColumn($this->lastRow,["nCol" => 12,"id"=>$id."_colContainerField"]);
        //check if adding Values Row
        if (is_array($addingValues)) {
            //New ROW
            $this->appendRow($id."_colContainerField");

            $name = $id;
            $valuesH = $valuesJSON;
            $valuesHName = $this->K_COMMON->getVarArray($addingValues, "valuesHName");
            $associative = $this->K_COMMON->getVarArray($addingValues, "associative");

            $nColumnLastRow = 0;
            $JSButtonAttributes = "";
            $JSButtonValue = "";
            $JSRecalculate = "";
            if ($valuesHName == "")
                $valuesHName = $name . "OptionsValuesH";


            if (is_array($fields)) {


                $JSRecalculateAssociative = false;
                if ($associative === "1" && count($fields) == 2) {
                    $JSRecalculate = 'var attName = $(this).attr("data-' . strtolower($fields[0]["name"]) . '");';
                    $JSRecalculate .= 'var attValue = $(this).attr("data-' . strtolower($fields[1]["name"]) . '");';
                    $JSRecalculateAssociative = true;

                }

                $i = 1;
                $JSButtonAttributes = "";
                $nColumnLastRow = 0;
                foreach ($fields as $key => $field) {
                    if (isset($field["param"])) {
                        $param = $field["param"];
                        if ($this->K_COMMON->getVarArray($param, "newRow") == "1") {
                            $this->appendRow($id."_colContainerField");
                            $nColumnLastRow = 0;
                        }
                        $nCol = $this->K_COMMON->getVarArray($param, "nCol");
                        if ($nCol == "")
                            $nCol = 1;
                    } else {
                        $nCol = intval(10 / $nFields);
                    }

                    if (isset($field["input"])) {
                        $field = $field["input"];
                    }
                    $nColumnLastRow += $nCol;

                    //Column Code
                    $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
                    $field["id"] = $name . $field["name"];
                    $this->appendInput($this->lastCol, $field);

                    $JSButtonValue .= 'inputRef = $("#' . $name . $field["name"] . '");';
                    $JSButtonValue .= 'inputType = inputRef.attr("type");';

                    $JSButtonValue .= 'if (inputType == "checkbox")';
                    $JSButtonValue .= '    inputValue' . strtolower($field["name"]) . ' = (inputRef.is(":checked") ? inputRef.attr("value") : "");';
                    $JSButtonValue .= 'else';
                    $JSButtonValue .= '    inputValue' . strtolower($field["name"]) . ' = inputRef.val();';
                    $JSButtonValue .= 'if (inputValue' . strtolower($field["name"]) . ' != "") if (BtnValue != "") BtnValue += " - " + inputValue' . strtolower($field["name"]) . '; else BtnValue = inputValue' . strtolower($field["name"]) . ';';

                    $JSButtonAttributes .= ' data-' . strtolower($field["name"]) . '="\' +  inputValue' . strtolower($field["name"]) . ' + \'"';


                    if (!$JSRecalculateAssociative)
                        $JSRecalculate .= "o." . $field["name"] . '=$(this).attr("data-' . strtolower($field["name"]) . '");';

                    $i++;
                }


                $JSButtonValue .= 'textToAppend = \'<button type="button" class="list-group-item list-group-item-action small" ' . $JSButtonAttributes . '>\' + BtnValue + \'</button>\';';
                $JSButtonValue .= '$("#' . $id . '").append(textToAppend);';


            }


            //Column Button
            if ($nColumnLastRow > 10)
                $this->appendRow($id."_colContainerField");
            $nameF = $name . "BtnAdd";
            $this->appendColumn($this->lastRow, ["nCol" => 1]);
            $this->appendInput($this->lastCol, ["type" => "btn", "value" => '<i class="fa fa-plus"></i>', "label" => "", "id" => $nameF, "name" => $nameF, "semanticColor" => "primary", "size" => "", "emptyLabel" => "1"]);

            //Hidden field
            $nameF = $valuesHName;
            $idF = $name . "OptionsValuesH";
            $this->appendInput($this->lastCol, ["type" => "hidden", "id" => $idF, "name" => $nameF, "value" => $valuesH]);

            // set javascript
            $jsUniqueID = "kx_listgroup_" . uniqid();
            $this->addFooterJS('<script>
			' . $jsUniqueID . ' = {};
			(function() {
				var t = this;
				    t.recalculateOptionValuesH = function () {

                    var arrayOptions = $("#' . $id . ' button").map(function () {
                        
                        '
                . ($associative === "1" ?
                    $JSRecalculate . '
                            return \'"\'+attName+\'":"\'+attValue+\'"\';
                            '
                    :
                    '
                            var o = new Object();
                            ' . $JSRecalculate . '
                            return JSON.stringify(o);
                            '
                ) . '
            
                    }).get();

                    ' . ($associative === "1" ?
                    '$("#' . $name . "OptionsValuesH" . '").val("{" + arrayOptions + "}");'
                    : '$("#' . $name . "OptionsValuesH" . '").val("[" + arrayOptions + "]");') . '
                }
                t.optionMoveUp = function () {
                    $(this).insertBefore($(this).prev());
                    t.recalculateOptionValuesH();
                }
                t.optionRemove = function (e) {
                    e.preventDefault();
                    $(this).remove();
                    t.recalculateOptionValuesH();
            
                }
                t.optionAdd = function () {
            
                    var BtnValue = "";
                    var inputRef = "";
                    var inputType = "";
                    var inputValue = "";
                    var textToAppend = "";
                    ' . $JSButtonValue . ';
                    currentOption = $("#' . $id . ' button").last();
                    currentOption.click(t.optionMoveUp);
                    currentOption.on("contextmenu", t.optionRemove);
            
                    t.recalculateOptionValuesH();
            
                }
				
				this.init = function(){
					$(document).ready(t.ready);
				};
				this.trigger = function(){
				    $("#' . $name . "BtnAdd" . '").click(t.optionAdd);
                    $("#' . $id . ' button").click(t.optionMoveUp);
                    $("#' . $id . ' button").on("contextmenu", t.optionRemove);
				}
				this.ready = function(){
				
                    t.trigger();
				}

				this.init();

			}).apply(' . $jsUniqueID . ');

		</script>');
        }

        $this->template->assign_block_vars("LISTGROUP", array("id" => $id, "class" => $class, "attributes" => $attributes, "html" => $html));

        //generate list
        if (is_array($values) && (!empty($values))) {


            $items = [];
            foreach ($values as $itemKey => $itemValue) {
                $attributesItem = "";
                $valueItem = "";


                if (is_array($itemValue)) {
                    foreach ($itemValue as $key => $value) {

                        IF (empty($value))
                            $value = "";

                        if ($value != "")
                            if ($valueItem != "")
                                $valueItem .= " - " . $value;
                            else
                                $valueItem = $value;

                        $attributesItem .= 'data-' . strtolower($key) . '="' . $value . '"';
                    }
                } else {
                    $valueItem = $itemKey . " - " . $itemValue;
                    if ($nFields == 2) {
                        $attributesItem .= 'data-' . strtolower($fields[0]["name"]) . '="' . $itemKey . '"';
                        $attributesItem .= 'data-' . strtolower($fields[1]["name"]) . '="' . $itemValue . '"';
                    } else
                        $attributesItem .= 'data-' . strtolower($itemKey) . '="' . $itemValue . '"';
                }

                $items[] = ["type" => "button", "class" => "small", "value" => $valueItem, "attributes" => $attributesItem];
            }
        }

        if (is_array($items)) {
            foreach ($items as $item) {
                switch ($item["type"]) {
                    case "button":
                        $this->addButton($item);
                        break;
                    case "link":
                        $this->addLink($item);
                        break;
                }
            }
        }

    }


    /**
     * Add Row with a Button to listgroup
     *
     * @param array $arrayVariable array of parameters
     * The following parameters are accepted:<br>
     *    *    value : value of button
     *    *    class (optional): class
     *    *    attributes (optional): additional attributes
     */
    public function addButton($arrayVariable)
    {

        //get variables
        $value = $this->K_COMMON->getVarArray($arrayVariable, "value");
        $class = $this->K_COMMON->getVarArray($arrayVariable, "class");
        $attributes = $this->K_COMMON->getVarArray($arrayVariable, "attributes");

        $html = '<button type="button" class="list-group-item list-group-item-action ' . $class . '"' . $attributes . '>' . $value . '</button>';
        $this->template->assign_block_vars("LISTGROUP.HTML", ["value" => $html]);

    }

    /**
     * Add Row with a Link to listgroup
     *
     * @param array $arrayVariable array of parameters
     * The following parameters are accepted:<br>
     *    *    value : value of button
     *    *    class (optional): class
     *    *    attributes (optional): additional attributes
     */
    public function addLink($arrayVariable)
    {

        //get variables
        $value = $this->K_COMMON->getVarArray($arrayVariable, "value");
        $class = $this->K_COMMON->getVarArray($arrayVariable, "class");
        $attributes = $this->K_COMMON->getVarArray($arrayVariable, "attributes");

        $html = '<a href="#" class="list-group-item ' . $class . '"' . $attributes . '>' . $value . '</a>';
        $this->template->assign_block_vars("LISTGROUP.HTML", ["value" => $html]);


    }

    /**
     * Get HTML of a list group well formatted, using bootstrap .
     *
     * @return array with html.
     */
    public function get($AV = [])
    {
        if (!empty($AV))
            $this->create($AV);
        $this->appendHTML("", $this->parse());
        return $this;


    }

    /**
     * Get HTML of a list group well formatted, using bootstrap .
     *
     * @return array with html.
     */
    public function parse()
    {
        return $this->template->pparse("body");
    }

}

?>

