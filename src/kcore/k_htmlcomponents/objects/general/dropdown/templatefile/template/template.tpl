<!-- BEGIN DROPDOWN -->
<div id="{DROPDOWN.id}" style="{DROPDOWN.style}" class="btn-group">
    <button type="button" class="btn {DROPDOWN.class} dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{DROPDOWN.label}</button>
    <div class="dropdown-menu">
        <!-- BEGIN ITEM -->
        <a class="dropdown-item {DROPDOWN.ITEM.class}" href="{DROPDOWN.ITEM.link}" {DROPDOWN.ITEM.attributes}>{DROPDOWN.ITEM.label}</a>
        <!-- END ITEM -->
    </div>
</div>

<!-- END DROPDOWN -->
