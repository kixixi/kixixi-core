<?
namespace k_htmlcomponents\objects\general\dropdown;
class obj extends \k_webmanager\objects\general\general_object\obj
{
	/**
	 * Version of object
	 *
	 * @since 1.00
	 * @access public
	 * @var string
	 */
	public $ver = "1.0";


	/**
	 * Template
	 *
	 * @since 1.00
	 * @access public
	 * @var template
	 */
	protected $template;


	/**
	 * ID of collapse
	 *
	 * @since 1.00
	 * @access public
	 * @var template
	 */
	var $dropdownID;

	/**
	 * Construct
	 *
	 */
	public function __construct($AV = []) {

		parent::__construct();

		//get template
		$this->template = $this->getTemplateLayout("template");

		$this->create($AV);
	}

	/**
	 * get a dropdown object (web object)
	 * This function is used from template manager,
	 * otherwise you can use single function for create it, and after use "get" to get table object.
	 *
	 * @param array $AV array of table parameters.<br>
	 *  Parameters: <br>
	 *
	 * *    id    : id of dropdown
	 *   *    label : label of dropdown
	 *   *    items    : array of fields (columns)
	 * 			*	*	class    : class of item
     * 			*	*	link    : default #
     * 			*	*	label    : label of item
	 * 			*	*	attributes    : optional attributes of the input
	 *   *    class: class of dropdown
	 *   *    size: "": normal,  "sm": small, "lg":large
	 *   *	  semanticColor: optional set color of button by bootstrap semantic purpose (primary, secondary, success, info, warning, danger, link)
	 *
	 * @return web_object
	 */
	public function get($AV = [])
	{

		//if $AV not empty create with $AV Parameters
		if (!empty($AV))
			$this->create($AV);

		//Append HTML of builded
		$this->appendHTML("",$this->parse());

		//Return Object
		return $this;
	}

	/**
	 * create new COLLAPSE
	 *
	 * @param array $arrayVariable array of table parameters.<br>
	 * Please refer to "get" function for correct parameters
	 *
	 */
	public function create($arrayVariable = [])
	{

		//reset template
		$this->template->destroy();
		$this->parsed = false;

		//set variables
		$dropdownID = $this->K_COMMON->getVarArray($arrayVariable, "id");
        $style = $this->K_COMMON->getVarArray($arrayVariable, "style");
		$dropdownClass = $this->K_COMMON->getVarArray($arrayVariable,"class");
		$items = $this->K_COMMON->getVarArray($arrayVariable, "items");
		$label = $this->K_COMMON->getVarArray($arrayVariable,"label");
        $dictionary = $this->K_COMMON->getVarArray($arrayVariable,"dictionary");
        $size = $this->K_COMMON->getVarArray($arrayVariable,"size");
        $semanticColor = $this->K_COMMON->getVarArray($arrayVariable,"semanticColor");

		//create ID
		if ($dropdownID == "") {
			$dropdownID = "dropdown_" . uniqid();
		}
		$this->dropdownID = $dropdownID;

		//set default HTML ID
		$this->setIDObject($this->dropdownID);
		$this->setIDContent($this->dropdownID);

		//check variables
        switch ($size) {
            case "sm":
                $dropdownClass .= " btn-sm";
                break;
            case "lg":
                $dropdownClass .= " btn-lg";
                break;
        }

        // set semantic button color
        if ($semanticColor != "") {
            $dropdownClass .= " btn-" . $semanticColor;
        }

		//create BLOCK
		$arraySet = array();
		$arraySet["id"] = $dropdownID;
        $arraySet["style"] = $style;
		$arraySet["class"] = $dropdownClass;
        $arraySet["label"] = $label;
		$this->template->assign_block_vars("DROPDOWN", $arraySet);

		//create tabs
		if (is_array($items))
		{

			if (isset($items["item"]))
				$items = $items["item"];

			foreach ($items as $item)
			{
				//Translate dictionary
				$dictionaryToTranslate = $this->K_COMMON->getVarArray($item,"dictionary");
				if (is_array($dictionaryToTranslate))
					foreach ($dictionaryToTranslate as $key => $value)
					{
						$item[$key] = $this->convertLabelDictionary(["dictionary" => $value], $dictionary) ;
					}
				$this->addItem($item);
			}

		}
	}

	/**
	 * Add ITEM
	 *
	 * @param array $arrayParam array of table parameters.<br>
	 * Please refer to "get" function for correct parameters
	 *
	 * @return array
	 */
	protected function addItem($arrayParam = array())
	{
		//set variables
		$class = $this->K_COMMON->getVarArray($arrayParam, "class");
		$link = $this->K_COMMON->getVarArray($arrayParam, "link");
        $label = $this->K_COMMON->getVarArray($arrayParam, "label");
        $attributes = $this->K_COMMON->getVarArray($arrayParam, "attributes");
        $attributesString = "";

        //Check Variables
        if ($link == "")
            $link = "#";

        //check attributes
        if (is_array($attributes)) {
            foreach ($attributes as $key => $val) {
                $attributesString .= " data-" . $key . "='" . $val . "''";
            }
        } else
            $attributesString = $attributes;

		//setVariable
		$arraySet = array();
		$arraySet["class"] = $class;
        $arraySet["link"] = $link;
        $arraySet["label"] = $label;
        $arraySet["attributes"] = $attributesString;


		$this->template->assign_block_vars("DROPDOWN.ITEM",$arraySet);


	}




	/**
	 * Get HTML of a list group well formatted, using bootstrap .
	 *
	 * @return array with html.
	 */
	public function parse() {
		return $this->template->pparse("body");
	}
}
?>

