<?
/**
 * KIXIXI HTML Components (chart)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage objects\form
 * @since 1.00
 */
namespace k_htmlcomponents\objects\general\chart;

class obj extends \k_webmanager\objects\general\general_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";


    /**
     * get a CHART
     *
     * @since 1.00
     * @access public
     *
     * * @param array $arrayParam array for input parameters.<br>
     *
     *  Parameters: <br>
     *
     * *    id      : id of chart
     * *    class   : class of chart
     * *    labelY  : label of Y
     * *    labelX  : label of X
     * *    arrayLabelX  : labelx X
     * *    arrayLabelY  : labelx Y (empty for auto)
     * *    arrayData  : value of Y (only for simple chart)
     * *    datasets : datasets of chart for complexed chart
     * *    formatY: array with format (Ex: ["symbol" => "$", "replace" => "/(\d)(?=(\d{3})+(?!\d))/g, "$1,""])
     * *    showGridLineX: show grid line X, 1 or 0 (referred to X axis)
     * *    showGridLineY: show grid line Y,  1 or 0 (referred to Y axis)
     * *    afterTickToLabelConversion: relative JS function
     * *    afterBuildChart: relative JS function
     *
     * @return webobject
     */
    public function get($arrayParam = [])
    {

        //js for create chart
        $this->addExternalPlugin("chartjs");

        //set variables
        $id = $this->K_COMMON->getVarArray($arrayParam, "id");
        $class = $this->K_COMMON->getVarArray($arrayParam, "class");
        $labelY = $this->K_COMMON->getVarArray($arrayParam, "labelY");
        $labelX = $this->K_COMMON->getVarArray($arrayParam, "labelX");
        $arrayData = $this->K_COMMON->getVarArray($arrayParam, "arrayData");
        $datasets = $this->K_COMMON->getVarArray($arrayParam, "datasets");
        $arrayLabelX = $this->K_COMMON->getVarArray($arrayParam, "arrayLabelX");
        $arrayLabelY = $this->K_COMMON->getVarArray($arrayParam, "arrayLabelY");
        $formatY = $this->K_COMMON->getVarArray($arrayParam, "formatY");
        $type = $this->K_COMMON->getVarArray($arrayParam, "type");
        $showGridLineX = $this->K_COMMON->getVarArray($arrayParam, "showGridLineX");
        $showGridLineY = $this->K_COMMON->getVarArray($arrayParam, "showGridLineY");
        $afterTickToLabelConversion = $this->K_COMMON->getVarArray($arrayParam, "afterTickToLabelConversion");
        $afterBuildChart = $this->K_COMMON->getVarArray($arrayParam, "afterBuildChart");
        $displayLabel = "false";
        $functionFormatY = "";

        //check variables
        if ($id == "")
            $id = "chart_" . uniqid();

        if ($type == "")
            $type = "line";

        if (empty($arrayData))
            $arrayData = [];
        if (empty($datasets))
            $datasets = [];
        if (empty($arrayLabelY))
            $arrayLabelY = [];
        if (!empty($formatY)) {
            $symbol = $this->K_COMMON->getVarArray($formatY, "symbol");
            $replace = $this->K_COMMON->getVarArray($formatY, "replace");
            $functionFormatY = "
                function(value) {
         
                 return '" . $symbol . "' + value.toString().replace(" . $replace . ") ;
              }
              ";
        }

        //create html
        $chart = '
            <canvas class="' . $class . '" id = "' . $id . '"></canvas>
        ';

        //convert array X and Y for js
        $arrayLabelX = "['" . implode("','", $arrayLabelX) . "']";
        $arrayLabelY = "['" . implode("','", $arrayLabelY) . "']";

        if (empty($datasets))
            $datasets["data"] = $arrayData;

        $datasetsJSON = json_encode($datasets);

        $script = "    <script>

                        KXJS.namespace(\"k_htmlcomponents.objects.chart_" . $id . "\");

                        // create method of plugin
                        (function () {
                            var t = this;
                            this.init = function () {
                                $(document).ready(t.ready);
                            };
                            t.createChart = function () {
                                var ctx = document.getElementById('" . $id . "').getContext('2d');
                                t.chart = new Chart(ctx, {
                                    type: '" . $type . "',
                                    data: {
                                        xLabels: " . $arrayLabelX . ",
                                        yLabels: " . $arrayLabelY . ",
                                        datasets: [" . $datasetsJSON . "]
                                    },
                                    options: {
                                    responsive: true,
                                    tooltips: {
                                        mode: 'index',
                                        intersect: false,
                                    },
                                    hover: {
                                        mode: 'nearest',
                                        intersect: true
                                    },
                                       legend: {
                                            display: " . $displayLabel . "
                                            

                                        },
                                        scales: {
                                            xAxes: [{
                                            
            " . ($afterTickToLabelConversion != "" ? "afterTickToLabelConversion: " . $afterTickToLabelConversion . " ," : "") . "
                                                gridLines: {
                                                    display: " . ($showGridLineY == "1" ? "true" : "false") . "
                                                },
                                                scaleLabel: {
                                                    display: true,
                                                    labelString: '" . $labelX . "'
                                                }
                                                
                                            }],
                                            yAxes: [{
                                                gridLines: {
                                                    display: " . ($showGridLineX == "1" ? "true" : "false") . "
                                                },
                                                scaleLabel: {
                                                    display: true,
                                                    labelString: '" . $labelY . "'
                                                },
                                                ticks: {
                                                callback: " . $functionFormatY . "
                                                }
                                            }]
                                        }
                                    }
                                });
                            }
                            t.ready = function () {
                                t.createChart();
                                " . ($afterBuildChart != "" ? $afterBuildChart . "(t.chart);" : "") . "
                            };

                            this.init();

                        }).apply(KXNS.k_htmlcomponents.objects.chart_" . $id . ");
                       </script>";

    // create html string
$this->appendHTML("", $chart);
$this->addFooterJS($script);

    // return
return $this;
}


}

?>

