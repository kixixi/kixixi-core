<?
/**
 * KIXIXI k_registry (contact)
 * Create a web object, with list of contacts of a user
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_registry
 * @subpackage objects\contact
 * @since 1.00
 */
namespace k_registry\objects\general\contact;
class obj extends \k_webmanager\objects\general\general_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * anaCode of user to get contacts
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $anaCode;

    /**
     * Constructor
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    anaCode: user code to get addresses
     */
    public function __construct($AV)
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
        $this->anaCode = $this->K_COMMON->getVarArray($AV, "anaCode");

    }

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //switch request
        switch ($KCA) {
            case "saveData" :
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "delete" :
                return json_encode($this->delete($ArrayVariable));
                break;
            case "addContact":
                return json_encode($this->getModalEdit($ArrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableList($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
        }
    }

    /**
     * get this object
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "")
     *    *    anaCode: user code to get addresses
     *
     * @return object this object
     */
    public function get($arrayVariable = [])
    {

        //set variables
        $pluginMethod = $this->K_COMMON->getVarArray($arrayVariable, "pluginMethod");
        $this->anaCode = $this->K_COMMON->getVarArray($arrayVariable, "anaCode");

        //check security
        if ($this->anaCode == "")
            return $this;

        switch ($pluginMethod) {
            case "list":
            case "":
                $this->getPageList($arrayVariable);
                break;

        }

        //Return
        return $this;
    }

    protected function getPageList($arrayVariable = array())
    {

        //add files to include
        $this->addHeadFile([$this->getPathFileJS("list")]);


        //NEW ROW
        $this->appendRow("");

        //get Table List
        $objTableList = $this->getTableList($arrayVariable);
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendObject("", $objTableList);

        //NEW ROW
        $this->appendRow("");

        //append hidden variables
        $this->appendInput($this->lastRow, ["type" => "hidden", "id" => "anaCode", "name" => "anaCode", "value" => $this->anaCode]);

        //button
        $label = $this->dictionary["LabelAddContact"];
        $name = "btnAddContact";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btn", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);


    }

    protected function delete($ArrayVariable)
    {
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $table = $this->getTable("contacts");
        $table->delete(array("id_contacts" => $keyID));
        return array("success" => "true");
    }

    protected function saveData($ArrayVariable)
    {
        $contactType = $this->K_COMMON->getVarArray($ArrayVariable, "contactType");
        $anaCode = $this->K_COMMON->getVarArray($ArrayVariable, "anaCode");
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $value = "";
        $prefix = "";

        if ($contactType == "EM") {
            $prefix = "";
            $value = $this->K_COMMON->getVarArray($ArrayVariable, "valueEmail");
        } elseif ($contactType == "TEL") {
            $prefix = $this->K_COMMON->getVarArray($ArrayVariable, "prefix_valueTel");
            $value = $this->K_COMMON->getVarArray($ArrayVariable, "valueTel");
        } elseif ($contactType == "CEL") {
            $prefix = $this->K_COMMON->getVarArray($ArrayVariable, "prefix_valueCell");
            $value = $this->K_COMMON->getVarArray($ArrayVariable, "valueCell");
        }
        if ($contactType == "" || $value == "")
            return array("success" => "false", "message" => "Error Security");

        if ($keyID == "")
            $arrayWhere = array();
        else
            $arrayWhere = array("id_contacts" => $keyID);
        $table = $this->getTable("contacts");
        $table->put(array("company_code" => $anaCode, "contacts_type" => $contactType, "contacts_prefix" => $prefix, "contacts_value" => $value), $arrayWhere);
        if ($keyID == "")
            $keyID = $table->getNewID();
        return array("success" => "true", "message" => $this->dictionary["msgSuccessSave"], "newID" => $keyID);
    }

    protected function getTableList($ArrayVariable = array())
    {
        $anaCode = $this->anaCode;
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");
        if ($anaCode == "")
            $anaCode = $this->K_COMMON->getVarArray($ArrayVariable, "anaCode");


        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        if ($anaCode == "")
            return $OBJTableList->get();

        //load value
        $table = $this->getTable("contacts");
        $table->get(array("id_contacts", "contacts_prefix", "contacts_type", "contacts_value"), ["company_code" => $anaCode]);
        $dataTable = $table->getResults("array");

        //create table
        $OBJTableList->newTable(
            [
                "id" => "contactsTableList"
                , "hideHead" => $onlyRow
                , "data" => $dataTable
                , "fields" =>
                [
                    [
                        "label" => $this->dictionary["LabelContact"]
                        , "qryField" => "contacts_type"
                    ]
                    ,
                    [
                        "label" => $this->dictionary["LabelContactType"]
                        , "qryField" => "contacts_value"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->dictionary["labelEditContact"]
                                    , "class" => "fa fa-edit btnEdit"
                                    , "size" => "sm"
                                    , "semanticColor" => "secondary"
                                    , "qryAttributes" => ["keyID" => "id_contacts"]
                                ]
                                ,
                                [
                                    "title" => $this->dictionary["labelDeleteContact"]
                                    , "action" => "remove"
                                    , "qryAttributes" => ["keyID" => "id_contacts"]
                                    , "ajax" => ["kcp" => $this->getPluginName(),"kco" => $this->getPluginObjectName(),"kca" => "delete"]
                                ]
                            ]
                    ]
                ]
            ]
        );



        return $OBJTableList->get();




    }

    protected function getModalEdit($ArrayVariable)
    {

        //Set Variables
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $anaCode = $this->K_COMMON->getVarArray($ArrayVariable, "anaCode");
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");
        $value_type = "";
        $value_phone = "";
        $value_prefix_phone = "";
        $value_mobilePhone = "";
        $value_prefix_mobilePhone = "";
        $value_email = "";

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");
        $webModal->newModal(["id" => $idModal, "title" => $this->dictionary["TitleContact"], "type" => "general"]);

        //add files to include
        $webModal->addHeadFile([$this->getPathFileJS("put")]);

        //check if modify
        if ($keyID != "") {
            $table = $this->getTable("contacts");
            $table->get(array("contacts_type", "contacts_prefix", "contacts_value"), array("id_contacts" => $keyID));

            if ($row = $table->fetch()) {

                $value_type = $row->contacts_type;
                if ($row->contacts_type == "TEL") {
                    $value_phone = $row->contacts_value;
                    $value_prefix_phone = $row->contacts_prefix;
                }
                if ($row->contacts_type == "CEL") {
                    $value_mobilePhone = $row->contacts_value;
                    $value_prefix_mobilePhone = $row->contacts_prefix;
                }
                if ($row->contacts_type == "EM")
                    $value_email = $row->contacts_value;

            }
        }


        //create form
        $webModal->appendForm($webModal->modalBody,
            [
                "id" => "addContactForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "closeModal" => $idModal
            ]
        );

        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //contact type
        $label = $this->dictionary["LabelContactType"];
        $name = "contactType";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
        $webModal->appendInput($webModal->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "id" => $name
                , "name" => $name
                , "value" => $value_type
                , "required" => "0"
                , "typeLoad" => "array"
                , "options" => ["" => "", "EM" => "EM", "CEL" => "CEL", "TEL" => "TEL"]
                , "options" => ["EM" => $this->dictionary["LabelEmail"], "CEL" => $this->dictionary["LabelCel"], "TEL" => $this->dictionary["LabelTel"]]
            ]
        );

        //keyID hidden
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "id" => "keyIDModal", "name" => "keyID", "value" => $keyID]);
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "id" => "anaCode", "name" => "anaCode", "value" => $anaCode]);


        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //phone
        $label = $this->dictionary["LabelTel"];
        $name = "valueTel";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
        $webModal->appendInput($webModal->lastCol, ["type" => "phone", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_phone, "prefixValue" => $value_prefix_phone,"rowContainerClass"=>"fieldContactTel fieldContact hidden"]);

        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //mobile phone
        $label = $this->dictionary["LabelCel"];
        $name = "valueCell";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
        $webModal->appendInput($webModal->lastCol, ["type" => "phone", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_mobilePhone, "prefixValue" => $value_prefix_mobilePhone,"rowContainerClass"=>"fieldContactCel fieldContact hidden"]);

        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //email
        $label = $this->dictionary["LabelEmail"];
        $name = "valueEmail";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
        $webModal->appendInput($webModal->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_email,"containerClass"=>"fieldContactEmail fieldContact hidden"]);


        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //button
        $label = $this->dictionary["LabelSave"];
        $name = "btnSave";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 3]);
        $webModal->appendInput($webModal->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];

    }


}

?>