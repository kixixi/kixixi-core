// create namespace
KXJS.namespace("k_registry.objects.contact");
// create method of plugin
(function() {
	var t = this;

	t.changeType = function(){
		var contactType = $("#contactType").val();
		$("#addContactForm .fieldContact").addClass("hidden");
		if(contactType == "EM")
			$("#addContactForm .fieldContactEmail").removeClass("hidden");
		else if(contactType == "CEL")
			$("#addContactForm .fieldContactCel").removeClass("hidden");
		else if(contactType == "TEL")
			$("#addContactForm .fieldContactTel").removeClass("hidden");
		
	}
	t.ready = function() {
		if($("#contactType").val()!= "")	
			t.changeType();

		$("#contactType").change(t.changeType);

		$("#btnNew").click(function(){
			$("#addContactForm input").val("");
			$("#addContactForm select option").first().prop("selected",true);
			$("#addContactForm .fieldContactCel select option").first().prop("selected",true);
			$("#addContactForm .fieldContactTel select option").first().prop("selected",true);
		});
	}

}).apply(KXNS.k_registry.objects.contact);

// document ready
$(document).ready(KXNS.k_registry.objects.contact.ready);