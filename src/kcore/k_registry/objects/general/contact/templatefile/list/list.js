// create namespace
KXJS.namespace("k_registry.objects.contact");
// create method of plugin
(function() {
	var t = this;
	t.askDeleteContact = function(){
		var keyID = $(this).attr("keyID");
		if(keyID == ""){
			alert("error");
			return;
		}
		waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'warning'});
		$.ajax({
			url: KXJS.buildAPICallUrl("KXCJS","k_registry","contact","askConfirm"),
			data: { 
				type: "deleteContact",
				idModal:"deleteContactModal"}
	
		}).done(function(data) {
			r = $.parseJSON(data);
			waitingDialog.hide();
			if(r.success == "true"){
				$(r.html).appendTo('body');
				$('#deleteContactModal').modal("show");
				$('#deleteContactModal').on('hidden.bs.modal', function (e) {
					$('#deleteContactModal').remove();
				});
	
				$('#deleteContactModal input[name="no"]').click(function(){$('#deleteContactModal').modal("hide");});
				$('#deleteContactModal input[name="yes"]').click(function(){$('#deleteContactModal').modal("hide");t.deleteContact(keyID);});
			}
			else
				alert("Error Security");
		});
	}
	t.deleteContact = function(keyID){
		$.ajax({
			url: KXJS.buildAPICallUrl("KXCJS","k_registry","contact","deleteContact"),
			data: { 
				keyID: keyID
				}
	
		}).done(function(data) {
			var r = $.parseJSON(data);
			if(r.success == "true")	
				t.loadTableList();
		});
	}
	t.addContact  = function(){
		var keyID = $(this).attr("keyID");
		var anaCode = $("#anaCode").val();
		waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'warning'});
		$.ajax({
			url: KXJS.buildAPICallUrl("KXCJS","k_registry","contact","addContact"),
			data: { 
				idModal:"modalAddContact",
				keyID: keyID,
				anaCode:anaCode
				
				}
	
		}).done(function(data) {
			r = $.parseJSON(data);
			waitingDialog.hide();
			if(r.success == "true"){
				$(r.html).appendTo('body');
				$('#modalAddContact').modal("show");
				$('#modalAddContact').on('hidden.bs.modal', function (e) {
					$('#modalAddContact').remove();
					t.loadTableList();
					
				});
		
				$('#modalAddContact input[name="back"]').click(function(){$('#modalAddContact').modal("hide");});
			}
			else
				alert("Error Security");
		});
	};
	t.loadTableList = function(){
		waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'warning'});
		var anaCode = $("#anaCode").val();
		$.ajax({
			url: KXJS.buildAPICallUrl("KXCJS","k_registry","contact","getTableList"),
			data: { 
				onlyRow: "1",
				anaCode: anaCode
				}
	
		}).done(function(data) {
			r = $.parseJSON(data);
			waitingDialog.hide();
			if(r.success == "true"){
				$('#contactsTableList tbody').html(r.html);
				t.assignTrigger();
				kx_tablelist_script_contactsTableList.assignTrigger();
			}
			else
				alert("Error Security");
		});
		
	}
	t.assignTrigger = function() {
		$("#contactsTableList .btnEdit").click(t.addContact);
		//$(".btnDeleteContact").click(t.askDeleteContact);
	}
	t.ready = function() {
		t.assignTrigger();
		$("#btnAddContact").click(t.addContact);
	}

}).apply(KXNS.k_registry.objects.contact);

// document ready
$(document).ready(KXNS.k_registry.objects.contact.ready);
