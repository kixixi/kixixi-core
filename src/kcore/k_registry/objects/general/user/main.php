<?
/**
 * KIXIXI k_registry (user)
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\user
 * @subpackage objects\contact
 * @since 1.00
 */
namespace k_registry\objects\general\user;
class obj extends \k_webmanager\objects\general\general_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";


    /**
     * Constructor
     *
     * @param array $AV array of variables
     * The following directives can be used in the array variables:<br>
     *    *    anaCode: user code to get addresses
     */
    public function __construct($AV)
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();

    }

    /**
     * Save data user (of current user)
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    name: name of user
     *    *    lastName: last name of user
     *
     * @return array
     */
    public function saveData($ArrayVariable)
    {

        //set variable
        $name = $this->K_COMMON->getVarArray($ArrayVariable, "name");
        $lastName = $this->K_COMMON->getVarArray($ArrayVariable, "lastName");
        $language = $this->K_COMMON->getVarArray($ArrayVariable, "language");
        $anaCode = $this->K_COMMON->getCurrentUser();

        //include objects
        $objSessionApi = $this->K_COMMON->getPluginObject("k_sessions", "api");

        //include table
        $table = $this->getTable("registry");

        if (!$this->K_SECAUTH->verifyUserLogged()) {
            return ["success" => "false", "reason" => "securityError", "message" => "Security Error"];
        }

        //create query registry
        $valueAV = array();
        $valueAV["ana_full_name"] = $lastName . " " . $name;
        $valueAV["ana_first_name"] = $name;
        $valueAV["ana_last_name"] = $lastName;

        $whereAV = array();
        if ($anaCode != "")
            $whereAV["ana_code"] = $anaCode;

        $table->put($valueAV, $whereAV);
        $newID = $table->getNewID();

        if ($language != "")
            $this->K_COMMON->setUserLanguage($anaCode,$language);

        //reset session variables
        $objSessionApi->resetSessionVariables();

        return array("success" => "true", "newID" => $newID, "message" => $this->dictionary["msgSuccessSave"]);
    }

}

?>