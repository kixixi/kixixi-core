<?
/**
 * KIXIXI k_secauth (registration)
 * functions for manage user registration
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_registry
 * @subpackage objects\registration
 * @since 1.00
 */
namespace k_registry\objects\general\registration;
class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();

    }

    /**
     * Manage Actions from ajax
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($arrayVariable)
    {

    }

    /**
     * Manage registration with FB
     * Use "appID" and "secret" parameters
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     * The following directives can be used in the array variables:<br>
     *    *    fbAccessToken: Access Token of FB
     *    *    fbid: Facebook ID of user
     *    *    anaRefCode: Ana Code if Friend
     *    *     autoLogin: if 1 or empty do auto login
     *
     * @return array ["success" => "true"] | ["success" => "false"]
     *
     */
    public function registrationFB($ArrayVariable)
    {


        //Set Variable
        $FB_appId = $this->getParamValue("FB_appId");
        $FB_secret = $this->getParamValue("FB_secret");
        $anaRefCode = $this->K_COMMON->getVarArray($ArrayVariable, "anaRefCode");
        $fbid = $this->K_COMMON->getVarArray($ArrayVariable, "fbid");
        $fbAccessToken = $this->K_COMMON->getVarArray($ArrayVariable, "fbAccessToken");
        $sendEmailToUser = $this->K_COMMON->getVarArray($ArrayVariable, "sendEmailToUser");
        $userLevel = $this->K_COMMON->getVarArray($ArrayVariable, "userLevel");
        $autoLogin = $this->K_COMMON->getVarArray($ArrayVariable, "autoLogin");
        $signupCheckbox = $this->K_COMMON->getVarArray($ArrayVariable, "signupCheckbox");
        if ($autoLogin == "")
            $autoLogin = "1";
        if ($fbAccessToken == "")
            $fbAccessToken = $this->K_COMMON->getVarArray($ArrayVariable, "fbAccessToken");
        $birthDate = "";
        $firstName = "";
        $lastName = "";
        $email = "";
        $profileImage = "";


        //include FB 
        $pathSDKFB = $this->K_COMMON->getPathSDK(["name" => "facebook"]);
        include_once($pathSDKFB . '/fbbase/facebook.php');


        if ($fbAccessToken != "") {

            //facebook object
            $facebook = new \Facebook(array(
                'appId' => $FB_appId,
                'secret' => $FB_secret
            ));

            //set facebook token
            $facebook->setAccessToken($fbAccessToken);

            //get user connected to facebook
            $fbuser = $facebook->getUser();
            if ($fbuser) {
                try {
                    //get facebook profile
                    $fb_profile = $facebook->api('/me?fields=id,first_name,last_name,birthday,picture,email');
                    
                    $fbid = $fb_profile["id"];
                    if ($fb_profile['email'] != "") {
                        //get FB Data
                        $birthDate = $this->K_COMMON->getVarArray($fb_profile,"birthday");
                        $firstName = $fb_profile['first_name'];
                        $lastName = $fb_profile['last_name'];
                        $email = $fb_profile['email'];
                        $profileImage = $fb_profile["picture"]["data"]["url"];

                        //Verify if user exist with FB
                        $table = $this->getTable("registry");
                        $table->get(["ana_last_name", "ana_first_name"], ["ana_facebook_id" => $fbid]);
                        if ($row = $table->fetch()) {

                            if ($autoLogin == "1")
                            {
                                $objDoLogin = $this->K_COMMON->getPluginObject("k_secauth", "dologin");
                                $resLoginFB = $objDoLogin->loginFB(["fbAccessToken" => $fbAccessToken]);
                                if ($resLoginFB["success"] == "true") {
                                    $ana_code_current = $this->K_COMMON->getCurrentUser();
                                    return array("success" => "true", "token" => session_id(), "anaCode" => $ana_code_current);
                                } else
                                    return array("success" => "false", "reason" => "noLogin", "message" => "User Not Logged");
                            }
                            else
                            {
                                $errmsg = $this->dictionary["msgEmailDup"];
                                return array("success" => "false", "reason" => "emaildup", "message" => $errmsg);
                            }



                        }
                        else
                            {

                        }

                    }
                } catch (FacebookApiException $e) {
                    return ["success" => "false", "reason" => "fberror", "messagge" => "Error connecting to Facebook"];
                }

            }
            else
                return ["success" => "false", "reason" => "fberror", "messagge" => "Error connecting to Facebook"];

        } else
            return ["success" => "false", "reason" => "fberror", "messagge" => "No Facebook Data"];

        //Register User on DB
        $res = $this->registration(
            [
                "firstName" => $firstName
                ,"lastName" => $lastName
                ,"email" => $email
                ,"passw" => ""
                ,"birthDate" => $birthDate
                ,"fbid" => $fbid
                ,"anaRefCode" => $anaRefCode
                ,"sendEmailToUser" => $sendEmailToUser
                ,"userLevel"=> $userLevel
                ,"autoLogin" => $autoLogin
                ,"signupCheckbox" => $signupCheckbox

            ]
        );

        //check if created user
        if ($res["success"] == "false")
            return $res;

        //get ana code of user
        $user_ana_code = $this->K_COMMON->getCurrentUser();

        //Check if upload image
        if ($profileImage != "" && $fbid != "" && $res["success"] == "true")
        {
            $photoRes = $this->uploadUserImage(["urlImg" => "http://graph.facebook.com/$fbid/picture?type=large"]);

            if ($this->K_COMMON->getVarArray($photoRes,"success") == "true")
                $photoName = $photoRes["filename"];
            else
                $photoName = "";

            //Update photo on registry
            $tableREG = $this->getTable("registry");
            $tableREG->put(["ana_photo" => $photoName], ["ana_code" => $user_ana_code]);

        }



        return $res;


    }

    /**
     * Manage registration
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *     firstName: First Name
     *    *     lastName: Last Name
     *    *     email: Email
     *    *     passw: Password
     *    *     birthDate: Birth Date
     *    *     profileImage: Profile Image (string with path and image name)
     *    *     fbid: Facebook ID of user
     *    *     anaRefCode: Ana Code if Friend
     *    *     sendEmailToUser: 1 for send email to user after registration
     *    *     autoLogin: if 1 or empty do auto login
     *
     * @return array ["success" => "true"] | ["success" => "false"]
     *
     */
    public function registration($ArrayVariable)
    {

        //$this->K_COMMON->printArray($ArrayVariable);

        //Include Module
        $tableUSR = $this->K_COMMON->getPluginDataObject("k_secauth", "login");
        $tableREG = $this->getTable("registry");
        $tableCNT = $this->getTable("contacts");
        $tableLGN = $this->K_COMMON->getPluginDataObject("k_secauth", "login");
        $objMailing = $this->K_COMMON->getPluginObject("k_mailing", "api");
        $objRegistryApi = $this->K_COMMON->getPluginObject("k_registry", "api");

        //set Variables
        $anaRefCode = $this->K_COMMON->getVarArray($ArrayVariable, "anaRefCode");
        $firstName = $this->K_COMMON->getVarArray($ArrayVariable, "firstName");
        $lastName = $this->K_COMMON->getVarArray($ArrayVariable, "lastName");
        $email = $this->K_COMMON->getVarArray($ArrayVariable, "email");
        $passwOri = $this->K_COMMON->getVarArray($ArrayVariable, "passw");
        $signupCheckbox = $this->K_COMMON->getVarArray($ArrayVariable, "signupCheckbox");
        $loginWebCodeParam = $this->getParamValue("loginWebCode", "", "k_secauth");

        if ($loginWebCodeParam != "")
            $webToLogin = $loginWebCodeParam;
        else
            $webToLogin = $this->K_COMMON->getCurrentWeb();


        if ($passwOri == "")
            $passwOri = $this->K_COMMON->generateRandomString(8);
        $password = sha1($passwOri);
        $birthDate = $this->K_COMMON->getVarArray($ArrayVariable, "birthDate");
        $profileImage = $this->K_COMMON->getVarArray($ArrayVariable, "profileImage");
        $fbid = $this->K_COMMON->getVarArray($ArrayVariable, "fbid");
        $sendEmailToUser = $this->K_COMMON->getVarArray($ArrayVariable, "sendEmailToUser");
        $autoLogin = $this->K_COMMON->getVarArray($ArrayVariable, "autoLogin");
        $userLevel = $this->K_COMMON->getVarArray($ArrayVariable, "userLevel");
        if ($userLevel == "")
            $userLevel = "2";
        if ($autoLogin == "")
            $autoLogin = "1";
        
        //check for refCode
        if ($anaRefCode == "")
        {
            $sessionFriendCode = $this->K_COMMON->getSessionFriendCode();
            if ($sessionFriendCode != "")
                $anaRefCode = $sessionFriendCode;
        }

        //Check Fields Empty
        if ($firstName == "" || $lastName == "" || $email == "")
            return array("success" => "false", "reason" => "emptyFields", "message" => $this->dictionary["emptyFields"]);


        //Verify if user exist by Email
        $tableCNT->get(["company_code"], ["contacts_type" => "EM", "contacts_value" => $email]);
        if ($row = $tableCNT->fetch()) {


            //Check if user with this email is registered in this web site
            $CondAdd = "lgn_web_code='" . $webToLogin . "'";
            $CondAdd .= " AND REG.ana_code = '" . $row->company_code . "'";
            $CondAdd .= " AND contacts_type='EM' AND contacts_value='" . $email ."'";

            
            $tableUSR->getByQuery("login_check", "", $CondAdd);
            if ($tableUSR->fetch()) {
                $errmsg = $this->dictionary["msgEmailDup"];
                return array("success" => "false", "reason" => "emaildup", "message" => $errmsg);
            }


        }

        //Check if user facebook not exist
        if ($fbid != "")
        {
            $tableREG->get(["ana_code"], ["ana_facebook_id" => $fbid]);
            if ($row = $tableREG->fetch()) {
                $errmsg = $this->dictionary["msgEmailDup"];
                return array("success" => "false", "reason" => "emaildup", "message" => $errmsg);
            }
        }


        //Save Data in database

        //Registry
        $putArray = [
            "ana_first_name" => $firstName
            , "ana_last_name" => $lastName
            , "ana_full_name" => $lastName . " " . $firstName
            , "ana_facebook_id" => $fbid
            , "ana_photo" => $profileImage
        ];

        if ($birthDate != "")
            $putArray['ana_birth_date'] = $birthDate;

        if($anaRefCode != "") 
            $putArray["ana_ref_code"] = $anaRefCode;

        $tableREG->put($putArray);

        $newID = $tableREG->getNewID();
        if ($newID == 0)
            return array("success" => "false", "reason" => "errorSaving", "message" => $this->dictionary["errorSaving"]);

        //get ana Code
        $tableREG->get(["ana_code"], ["id_registry" => $newID]);
        if ($row = $tableREG->fetch()) {
            $ana_code = $row->ana_code;
        }


        //contacts
        $tableCNT->put([
            "company_code" => $ana_code
            , "contacts_type" => "EM"
            , "contacts_value" => $email
        ]);


        //create default password
        $tableLGN->put(
            [
                "lgn_web_code" => $webToLogin
                , "lgn_pwd" => $password
                , "lgn_user_code" => $ana_code
                , "lgn_level_code" => $userLevel
            ]
        );

        //save check box
        $objRegistryApi->saveRegistryData(["anaCode" => $ana_code, "values" => $signupCheckbox]);
        


        //Do Auto login
        if ($autoLogin == "1")
        {
            $objDoLogin = $this->K_COMMON->getPluginObject("k_secauth", "dologin");
            $objDoLogin->login(array("user" => $email, "pwd" => $passwOri, "sessionNotExpire" => "1"));
        }

        

        //Send Email To User
        if ($sendEmailToUser == "1")
            $objMailing->doAction($this->namePlugin, $this->namePluginObject
                , $webToLogin
                , "USERREG", ["useremail" => $email], ["firstName" => $firstName, "lastName" => $lastName, "email" => $email, "pwd" => $passwOri]);



        return array("success" => "true", "token" => session_id(), "anaCode" => $ana_code);


    }

    /**
     * Upload User Image
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    urlImg: url of image
     *
     * @return array ["success" => "true"] | ["success" => "false"]
     *
     */
    function uploadUserImage($ArrayVariable = array())
    {


        //Set Variable
        $user_ana_code = $this->K_COMMON->getCurrentUser();
        $urlImg = $this->K_COMMON->getVarArray($ArrayVariable, "urlImg");
        $time = time();
        $ext = "";

        //Set Dir File
        $DirFile = $this->K_COMMON->pathTmp;
        if (!is_dir($DirFile)) mkdir($DirFile);
        $NewFileName = "photo-profile-" . $user_ana_code;

        //Check if Image From Url
        if ($urlImg != "") {

            $ext = "jpg";
            $NewFileName = $NewFileName . "." . $ext;

            file_put_contents($DirFile . $NewFileName, $this->K_COMMON->curlGetUrlContent($urlImg));
        } else {
            return array("success" => "false");
         }

        //Resize Image
        if (strtolower($ext) == "bmp")
            $ext = "png";
        $NewFileName_UsrImg = "profileImage." . $ext;
        $APIImages = new \k_imagemanager\objects\general\api\obj;
        $APIImages->imageResize(
            [
                "filename" => $DirFile . $NewFileName,
                "newfilename" => $DirFile . $NewFileName_UsrImg,
                "width" => "100%"
            ]
        );

        //Upload File
        $apiFM = $this->K_COMMON->getPluginObject("k_filemanager", "api");
        $filename = $apiFM->uploadStaticContent(
          [
              "sourceFile" => $DirFile . $NewFileName_UsrImg
              ,"targetDir" => "users/".$user_ana_code
              ,"targetFile" => $NewFileName_UsrImg
          ]
        );

        return array("success" => "true", "filename" => $NewFileName_UsrImg, "urlPhoto" => $filename);



    }
}

?>