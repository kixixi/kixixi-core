<?
/**
 * KIXIXI k_registry (webservice)
 * Create a web object, with list of address of a user
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_registry
 * @subpackage objects\webservice
 * @since 1.00
 */
namespace k_registry\objects\general\webservice;
class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authWebServiceModReg";

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable) {
        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //switch request
        switch ($KCA) {
            case "checkExistRegistry":
                return json_encode($this->checkExistRegistry($ArrayVariable));
                break;
            case "saveRegistry": 
                return json_encode($this->saveRegistry());
                break;
            case "saveBaseDataUser":
                $objUser = $this->K_COMMON->getPluginObject("k_registry", "user");
                return json_encode($objUser->saveData($ArrayVariable));
                break;
            case "signup":
                $objSignup = $this->K_COMMON->getPluginObject("k_registry", "registration");
                $ArrayVariable["sendEmailToUser"] = $this->K_COMMON->getVarArray($ArrayVariable,"smu");
                return json_encode($objSignup->registration($ArrayVariable));
                break;
            case "signupFB":
                $ArrayVariable["sendEmailToUser"] = $this->K_COMMON->getVarArray($ArrayVariable,"smu");
                $objSignup = $this->K_COMMON->getPluginObject("k_registry", "registration");
                return json_encode($objSignup->registrationFB($ArrayVariable));
                break;
            case "getBaseDataUser":
                $objRegApi = $this->K_COMMON->getPluginObject("k_registry", "api");

                $resData["success"]="true";

                //get actual data
                $anaCode = $this->K_COMMON->getCurrentUser();
                $anaRefCode = $objRegApi->getRefCode();
                if ($anaRefCode == "")
                    $anaRefCode = 0;

                $resData["anaCode"] = $anaCode;
                $resData["anaRefCode"] = $anaRefCode;
                $resData["firstName"] = $this->K_COMMON->getCurrentUserFirstName();
                $resData["lastName"] = $this->K_COMMON->getCurrentUserLastName();
                $resData["language"] = $this->K_COMMON->getUserLanguage();


                return json_encode($resData);

                break;
            case "getProfileDataUser":
                $resData = $this->getProfileDataUser();
                return json_encode($resData);
                break;


        }

        return "";
    }

    public function getProfileDataUser(){
        $objRegApi = $this->K_COMMON->getPluginObject("k_registry", "api");


        $resData["success"]="true";

        //get actual data
        $anaCode = $this->K_COMMON->getCurrentUser();
        $anaRefCode = $objRegApi->getRefCode();
        if ($anaRefCode == "")
            $anaRefCode = 0;

        $resData["anaCode"] = $anaCode;
        $resData["anaRefCode"] = $anaRefCode;
        $resData["firstName"] = $this->K_COMMON->getCurrentUserFirstName();
        $resData["lastName"] = $this->K_COMMON->getCurrentUserLastName();
        $resData["language"] = $this->K_COMMON->getUserLanguage();



        return $resData;
    }

    protected function checkExistRegistry($ArrayVariable) {

        //set variables
        $code = $this->K_COMMON->getVarArray($ArrayVariable,"code");

        //load data
        $table = $this->getTable("registry");
        $table->get("*",["ana_code" => $code]);
        $data = $table->fetch();
        if ($data)
            return ["success"=>"true","keyID"=>$data->id_registry];
        else
            return ["success"=>"false","reason"=>"notExist"];


    }

    protected function saveRegistry() {

        //Include Module
        $table = $this->getTable("registry");

        //set variables
        $code = $this->K_COMMON->getVarRequest("code");
        $fstname = $this->K_COMMON->getVarRequest("fstname");
        $lstname = $this->K_COMMON->getVarRequest("lstname");
        $fllname = $this->K_COMMON->getVarRequest("fllname");
        $regname = $this->K_COMMON->getVarRequest("regname");
        $anaVAT = $this->K_COMMON->getVarRequest("anaVAT");
        $anaCF = $this->K_COMMON->getVarRequest("anaCF");
        $keyID = "";

        //Check if exist
        if ($code != "") {
            $resAnag = $this->checkExistRegistry(["code"=>$code]);
            if ($resAnag["success"] == "true")
                $keyID = $resAnag["keyID"];
        }

        //save Registry
        $AVFields = [];
        $AVFields["ana_code"] = $code;
        $AVFields["ana_first_name"] = $fstname;
        $AVFields["ana_last_name"] = $lstname;
        $AVFields["ana_full_name"] = $fllname;
        $AVFields["ana_registered_name"] = $regname;
        $AVFields["ana_vat"] = $anaVAT;
        $AVFields["ana_fiscal_code"] = $anaCF;

        $AVCond = [];
        if ($keyID != "")
            $AVCond["id_registry"] = $keyID;


        $table->put($AVFields,$AVCond);

        //return
        return ["success" => "true"];

    }
    
    

   

}

?>