<?
namespace k_registry\objects\general\api;
class obj extends \k_common\objects\general\base\obj
{
    var $ver = "1.0";
    var $dictionary;

    public function __construct()
    {
        parent::__construct();
        
    }


    /**
     * PLUGIN MATCH for WEB DATA
     *
     * @param array $AV $wo
     *
     * @return web_object|string
     */
    public function pluginMatch($AV)
    {

        //set variables
        $data_array = $AV["data_array"];
        $parameters = explode(".", $data_array[1]);
        $ret = "";

        switch ($parameters[0])
        {
            case "affiliate":
                if ($parameters[1] == "link")
                {
                    $objWebManager = $this->K_COMMON->getPluginObject("k_webmanager", "api");

                    $url = $objWebManager->getWebURL();

                    if (isset($parameters[2]))
                        $url = $url . $parameters[2];

                    $ret = $url . "?friendCode=" . $this->K_COMMON->getCurrentUser();
                }
                break;
        }

        return $ret;

    }

    /**
     * Get Language of the user specified
     *
     * @param string $anaCode ana code of user to get language
     *
     * @return string with language code.
     */
    public function getRegistryName($anaCode = "")
    {
        if ($anaCode == "")
            $anaCode = $this->K_COMMON->getCurrentUser();

        $table = $this->K_COMMON->getPluginDataObject("k_registry", "registry");
        $table->get("ana_first_name, ana_last_name", ["ana_code" => $anaCode]);
        $data = $table->fetch();

        if ($data)
            return ["firstname" => $data->ana_first_name,"lastname" => $data->ana_last_name];
        else
            return "";
    }

    /**
     * Get Language of the user specified
     *
     * @param string $anaCode ana code of user to get language
     *
     * @return string with language code.
     */
    public function getUserLanguage($anaCode = "")
    {
        if ($anaCode == "")
            $anaCode = $this->K_COMMON->getCurrentUser();

        $table = $this->K_COMMON->getPluginDataObject("k_registry", "registry");
        $table->get("ana_language", ["ana_code" => $anaCode]);
        $data = $table->fetch();

        if ($data)
            return $data->ana_language;
        else
            return "";
    }

    /**
     * Get Ref Code of the user specified
     *
     * @param string $anaCode ana code of user to get ref code
     *
     * @return string with ana ref code.
     */
    public function getRefCode($anaCode = "")
    {
        if ($anaCode == "")
            $anaCode = $this->K_COMMON->getCurrentUser();

        $table = $this->getTable("registry");
        $table->call("registry_get_ref_code","'" . $anaCode."'");
        $data = $table->fetch();

        if ($data)
            return $data->returnvalue;
        else
            return "";
    }

    /**
     * Set Ref Code of the user specified
     *
     * @param string $refCode ana code of user ref
     * @param string $anaCode ana code of user to get ref code
     *
     * @return string with ana ref code.
     */
    public function setRefCode($refCode, $anaCode = "")
    {
        if ($anaCode == "")
            $anaCode = $this->K_COMMON->getCurrentUser();

        $table = $this->getTable("registry");
        
        $table->put(["ana_ref_code" => $refCode],["ana_code" => $anaCode]);

        return true;
    }

    /**
     * Set Gender
     *
     * @param string $gender gender ("M" or "F")
     * @param string $anaCode ana code of user to get ref code
     *
     * @return string with ana ref code.
     */
    public function setGender($gender, $anaCode = "")
    {
        if ($anaCode == "")
            $anaCode = $this->K_COMMON->getCurrentUser();

        $table = $this->getTable("registry");

        $table->put(["ana_sex" => $gender],["ana_code" => $anaCode]);

        return true;
    }

    /**
     * Set Birth Date
     *
     * @param string $birthdate birthdate yyyy-mm-dd
     * @param string $anaCode ana code of user to get ref code
     *
     * @return string with ana ref code.
     */
    public function setBirthDate($birthdate, $anaCode = "")
    {
        if ($anaCode == "")
            $anaCode = $this->K_COMMON->getCurrentUser();

        $table = $this->getTable("registry");

        $table->put(["ana_birth_date" => $birthdate],["ana_code" => $anaCode]);

        return true;
    }

    /**
     * Set User Name
     *
     * @param string $firstName first name
     * @param string $lastName last name
     * @param string $anaCode ana code of user
     *
     * @return boolean
     */
    public function setUserName($firstName, $lastName, $anaCode = "")
    {
        if ($anaCode == "")
            $anaCode = $this->K_COMMON->getCurrentUser();

        $table = $this->getTable("registry");

        $table->put(["ana_first_name" => $firstName,"ana_last_name" => $lastName],["ana_code" => $anaCode]);

        return true;
    }



    /**
     * Set User Email
     *
     * @param string $email email
     * @param string $anaCode ana code of user
     *
     * @return boolean
     */
    public function setUserEmail($email, $anaCode = "")
    {

        if ($anaCode == "")
            $anaCode = $this->K_COMMON->getCurrentUser();

        //include object
        $objRegApi = $this->K_COMMON->getPluginObject("k_registry", "api");


        //save
        $AVcontact = array();
        $AVcontact["contactType"] = "EM";
        $AVcontact["anaCode"] = $anaCode;
        $AVcontact["contactValue"] = $email;
        $AVcontact["uniqueRecord"] = "1";
        $objRegApi->saveContact($AVcontact);

        return true;
    }

    /**
     * Get Contact of the user specified
     *
     * @param string $anaCode ana_code of user to get language
     * @param string $contactType ana_code of user to get language
     * @param array $arrayVariable extra param (OM, qty)
     *
     * @return string with language code.
     */
    public function getContact($anaCode, $contactType, $arrayVariable = [])
    {

        //set variables
        $OM = $this->K_COMMON->getVarArray($arrayVariable, "OM");
        $qty = $this->K_COMMON->getVarArray($arrayVariable, "qty");
        $Return_IDS = "";
        $Return_Value = "";
        $Return_Prefix = "";
        $i = 0;

        //include table
        $table = $this->getTable("contacts");

        //check variables
        if ($qty == "")
            $qty = 9999;
        if ($anaCode == "")
            $anaCode = $this->K_COMMON->getCurrentUser();

        //get data
        $table->get("*", ["company_code" => $anaCode, "contacts_type" => strtoupper($contactType)]);
        while (($data = $table->fetch()) && ($i < $qty)) {

            $Return_Value .= "," . $data->contacts_value;
            $Return_Prefix .= "," . $data->contacts_prefix;
            $Return_IDS .= "," . $data->id_contacts;
            $i++;
        }

        //return
        if ($Return_Value != "")
            $Return_Value = substr($Return_Value, 1);
        if ($Return_Prefix != "")
            $Return_Prefix = substr($Return_Prefix, 1);
        if ($OM == "ARRAY")
            return ["ids" => $Return_IDS, "value" => $Return_Value, "prefix" => $Return_Prefix];

        return $Return_Value;
    }

    /**
     * Get Ana Code By Email
     *
     * @param array $arrayVariable extra param (email)
     *
     * @return string with language code.
     */
    public function getUserByEmail($arrayVariable = [])
    {

        //set variables
        $email = $this->K_COMMON->getVarArray($arrayVariable, "email");
        $anaCode = "";

        //include table
        $table = $this->getTable("contacts");

        //check variables

        //get data
        $table->get("*", ["contacts_value" => $email, "contacts_type" => "EM"]);
        if($data = $table->fetch()) {

            $anaCode = $data->company_code;

        }


        return $anaCode;
    }

    /**
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    keyID: keyID of contacts (only for modify)
     *    *    anaCode: (ana Code of contact to change). if empty get actual user anaCode
     *    *    contactType: (EM, TEL, CEL)
     *    *    contactValue: value of contact (EX: Email)
     *    *    contactPrefix: prefix uf contacts is a phone
     *    *    uniqueRecord: 1 if overwrite record (in this case not set keyID)
     *
     *@return array
     *
     */
    public function saveContact($ArrayVariable)
    {
        //set variables
        $anaCode = $this->K_COMMON->getVarArray($ArrayVariable, "anaCode");
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $contactType = $this->K_COMMON->getVarArray($ArrayVariable, "contactType");
        $contactValue = $this->K_COMMON->getVarArray($ArrayVariable, "contactValue");
        $contactPrefix = $this->K_COMMON->getVarArray($ArrayVariable, "contactPrefix");
        $uniqueRecord = $this->K_COMMON->getVarArray($ArrayVariable, "uniqueRecord");
        if ($anaCode == "")
            $anaCode = $this->K_COMMON->getCurrentUser();

        //include table
        $table = $this->getTable("contacts");


        //get if unique records
        if ($uniqueRecord == "1") {
            $table->get("id_contacts", array("company_code" => $anaCode, "contacts_type" => $contactType));
            if ($row = $table->fetch())
                $keyID = $row->id_contacts;
        }

        //Check if record if of correct anaCode
        if ($keyID != ""){
            $table->get("id_contacts", array("company_code" => $anaCode, "contacts_type" => $contactType, "id_contacts" => $keyID));
            $row = $table->fetch();
            if (!$row)
                return array("success" => "false", "reason" => "securityError", "message" => "Security Error");

        }

        //create query contact
        $valueAV = array();
        $valueAV["contacts_value"] = $contactValue;
        $valueAV["company_code"] = $anaCode;
        $valueAV["contacts_type"] = $contactType;
        $valueAV["contacts_prefix"] = $contactPrefix;
        $valueAV["company_code"] = $anaCode;

        $whereAV = array();
        if ($keyID != "") {
            $whereAV["company_code"] = $anaCode;
            $whereAV["id_contacts"] = $keyID;
        }
        
        $table->put($valueAV, $whereAV);
        return array("success" => "true");
    }



    /**
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    keyID: keyID of contacts (only for modify)
     *    *    anaCode: (ana Code of contact to change). if empty get actual user anaCode
     *    *    address: address
     *    *    zipCode: zip code
     *    *    city: city
     *    *    province: province
     *    *    type: "legal", "invoice", "shipping"
     *    *    getGeolocation: get geo location
     *    *    uniqueRecord: get id of record automatically
     *    *    description: description of address
     *    *    addressNote: note for this address
     *    *    attention: "attention" for shipping
     *    *    legalAddress: 1 if legal address
     *    *    invoiceAddress: 1 if invoice address
     *    *    shippingAddress: 1 if shipping  address
     *
     *@return array
     *
     */
    public function saveAddress($ArrayVariable)
    {

        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $anaCode = $this->K_COMMON->getVarArray($ArrayVariable, "anaCode");
        $adr_address = $this->K_COMMON->getVarArray($ArrayVariable, "address");
        $adr_zipCode = $this->K_COMMON->getVarArray($ArrayVariable, "zipCode");
        $adr_city = $this->K_COMMON->getVarArray($ArrayVariable, "city");
        $adr_country = $this->K_COMMON->getVarArray($ArrayVariable, "country");
        $adr_province = $this->K_COMMON->getVarArray($ArrayVariable, "province");
        $getGeolocation = $this->K_COMMON->getVarArray($ArrayVariable, "getGeolocation");
        $uniqueRecord = $this->K_COMMON->getVarArray($ArrayVariable, "uniqueRecord");
        $description = $this->K_COMMON->getVarArray($ArrayVariable, "description");
        $addressNote = $this->K_COMMON->getVarArray($ArrayVariable, "addressNote");
        $attention = $this->K_COMMON->getVarArray($ArrayVariable, "attention");
        $legalAddress = $this->K_COMMON->getVarArray($ArrayVariable, "legalAddress");
        $invoiceAddress = $this->K_COMMON->getVarArray($ArrayVariable, "invoiceAddress");
        $shippingAddress = $this->K_COMMON->getVarArray($ArrayVariable, "shippingAddress");
        $lat = "";
        $lng = "";

        //include table
        $table = $this->getTable("address");

        //get if unique records
        if ($uniqueRecord == "1") {

            $arrayLoadWhere = array();
            $arrayLoadWhere["company_code"] = $anaCode;
            if ($legalAddress == "1") $arrayLoadWhere["legal_address"] = 1;
            if ($invoiceAddress == "1") $arrayLoadWhere["invoice_address"] = 1;
            if ($shippingAddress == "1") $arrayLoadWhere["shipping_address"] = 1;

            $table->get("id_address", $arrayLoadWhere);


            if ($row = $table->fetch())
                $keyID = $row->id_address;
        }
        if ($getGeolocation == "1") {
            $fullAddress = $adr_address . "," . $adr_city . "," . $adr_province . "," . $adr_city . "," . $adr_country;
            $resultGeogode = $this->K_COMMON->getGeocodeAddress(array("address" => $fullAddress));
            if ($resultGeogode["success"] == "false")
                return array("success" => "false", "reason" => "geolocationError", "message" => "geolocation error");
            $lat = $resultGeogode["lat"];
            $lng = $resultGeogode["lng"];
            $geo_country = $resultGeogode["country"];
            if (($adr_country == "") || (strlen($adr_country) > 2)) $adr_country = $geo_country;
        }


        //create array for save address
        $valueAV = array();
        $valueAV["address_name"] = $adr_address;
        $valueAV["zip_code"] = $adr_zipCode;
        $valueAV["city"] = $adr_city;
        $valueAV["country"] = $adr_country;
        $valueAV["province"] = $adr_province;
        $valueAV["adr_latitude"] = $lat;
        $valueAV["adr_longitude"] = $lng;
        $valueAV["site_type_description"] = $description;
        $valueAV["address_name_add"] = $addressNote;
        $valueAV["address_attention"] = $attention;
        if ($legalAddress !== "")
            $valueAV["legal_address"] = $legalAddress;
        if ($invoiceAddress !== "")
            $valueAV["invoice_address"] = $invoiceAddress;
        if ($shippingAddress !== "")
            $valueAV["shipping_address"] = $shippingAddress;


        $whereAV = array();
        if ($keyID != "") {
            $whereAV["company_code"] = $anaCode;
            $whereAV["id_address"] = $keyID;
        } else
            $valueAV["company_code"] = $anaCode;

        $table->put($valueAV, $whereAV);
        if ($keyID == "")
            $keyID = $table->getNewID();

        return array("success" => "true", "keyID" => $keyID);
    }

    /**
     * Save Registry Data
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    anaCode: ana code of user to save data
     *    *    values: pairs of values to save on DB
     *
     *@return array
     *
     */
    public function saveRegistryData($ArrayVariable)
    {


        $anaCode = $this->K_COMMON->getVarArray($ArrayVariable, "anaCode");
        $values = $this->K_COMMON->getVarArray($ArrayVariable, "values");

        $tableREGDATA = $this->getTable("registry_data");


        //save check box
        if (!empty($values))
            foreach ($values as $k => $v)
            {
                if ($v == "")
                    $v = "0";
                $tableREGDATA->put(
                    [
                        "and_code" => $anaCode
                        ,"and_field_code" => $k
                        ,"and_field_value" => $v
                    ]
                );
            }

        return array("success" => "true");
    }




}

?>