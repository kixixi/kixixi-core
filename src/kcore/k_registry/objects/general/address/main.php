<?
/**
 * KIXIXI k_registry (address)
 * Create a web object, with list of address of a user
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_registry
 * @subpackage objects\address
 * @since 1.00
 */
namespace k_registry\objects\general\address;
class obj extends \k_webmanager\objects\general\web_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * anaCode of user to get addresses
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $anaCode;

    /**
     * Constructor
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    anaCode: user code to get addresses
     */
    public function __construct($arrayVariable)
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
        $this->anaCode = $this->K_COMMON->getVarArray($arrayVariable, "anaCode");
    }

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //switch request
        switch ($KCA) {
            case "saveData" :
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "delete" :
                return json_encode($this->delete($ArrayVariable));
                break;
            case "addAddress":
                return json_encode($this->getModalEdit($ArrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableList($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
        }

    }

    /**
     * get this object
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "")
     *    *    anaCode: user code to get addresses
     *
     * @return object this object
     */
    public function get($arrayVariable = array())
    {

        //set variables
        $pluginMethod = $this->K_COMMON->getVarArray($arrayVariable, "pluginMethod");
        $this->anaCode = $this->K_COMMON->getVarArray($arrayVariable, "anaCode");

        //check security
        if ($this->anaCode == "")
            return $this;

        switch ($pluginMethod) {
            case "list":
            case "":
                $this->getPageList($arrayVariable);
                break;

        }

        //Return
        return $this;
    }

    public function getPageList($arrayVariable = array())
    {
        //add files to include
        $this->addHeadFile([$this->getPathFileJS("list")]);


        //NEW ROW
        $this->appendRow("");

        //get Table List
        $objTableList = $this->getTableList($arrayVariable);
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendObject("", $objTableList);

        //NEW ROW
        $this->appendRow("");

        //append hidden variables
        $this->appendInput($this->lastRow, ["type" => "hidden", "id" => "anaCode", "name" => "anaCode", "value" => $this->anaCode]);

        //button
        $label = $this->dictionary["LabelNewAddr"];
        $name = "btnAddAddress";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btn", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

    }

    public function saveData($ArrayVariable)
    {
        //set variable
        $anaCode = $this->K_COMMON->getVarArray($ArrayVariable, "anaCode");
        if ($anaCode == "")
            return ["success" => "false","reason"=>"errorSecurity","message"=>"Error Security"];

        //include object
        $objRegApi = $this->K_COMMON->getPluginObject("k_registry", "api");

        //address variable
        $description = $this->K_COMMON->getVarArray($ArrayVariable, "description");
        $addressNote = $this->K_COMMON->getVarArray($ArrayVariable, "addressNote");
        $attention = $this->K_COMMON->getVarArray($ArrayVariable, "attention");
        $legalAddress = $this->K_COMMON->getVarArray($ArrayVariable, "legalAddress");
        $invoiceAddress = $this->K_COMMON->getVarArray($ArrayVariable, "invoiceAddress");
        $shippingAddress = $this->K_COMMON->getVarArray($ArrayVariable, "shippingAddress");
        $address = $this->K_COMMON->getVarArray($ArrayVariable, "address");
        $city = $this->K_COMMON->getVarArray($ArrayVariable, "city");
        $zipCode = $this->K_COMMON->getVarArray($ArrayVariable, "zip_code");
        $province = $this->K_COMMON->getVarArray($ArrayVariable, "province");
        $country = $this->K_COMMON->getVarArray($ArrayVariable, "state");
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");

        //save address
        $addressArray = array();
        $addressArray["keyID"] = $keyID;
        $addressArray["anaCode"] = $anaCode;
        $addressArray["description"] = $description;
        $addressArray["addressNote"] = $addressNote;
        $addressArray["attention"] = $attention;
        $addressArray["legalAddress"] = $legalAddress;
        $addressArray["invoiceAddress"] = $invoiceAddress;
        $addressArray["shippingAddress"] = $shippingAddress;
        $addressArray["address"] = $address;
        $addressArray["zipCode"] = $zipCode;
        $addressArray["city"] = $city;
        $addressArray["country"] = $country;
        $addressArray["province"] = $province;
        //$addressArray["getGeolocation"] = "1";
        //$addressArray["uniqueRecord"] = "1";
        $saveAddressArray = $objRegApi->saveAddress($addressArray);
        if ($saveAddressArray["success"] == "false")
            return $saveAddressArray;
        $keyID = $saveAddressArray["keyID"];
        return array("success" => "true", "message" => $this->dictionary["msgSuccessSave"], "newID" => $keyID);
    }



    protected function getTableList($ArrayVariable = array())
    {

        $anaCode = $this->K_COMMON->getVarArray($ArrayVariable, "anaCode");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");


        //load value
        $table = $this->getTable("address");
        $table->getByQuery("address_with_data", "", ["company_code" => $anaCode]);

        $dataTable = $table->getResults("array");

        //create table
        $OBJTableList->newTable(
            [
                "id" => "addressTableList"
                , "hideHead" => $onlyRow
                , "data" => $dataTable
                , "fields" =>
                [
                    [
                        "label" => $this->dictionary["labelTableAddress"]
                        , "qryField" => "address"
                    ]
                    ,
                    [
                        "label" => $this->dictionary["labelAddressType"]
                        , "qryField" => "type"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->dictionary["LabelEditAddr"]
                                    , "class" => "fa fa-edit btnEdit"
                                    , "size" => "sm"
                                    , "semanticColor" => "secondary"
                                    , "qryAttributes" => ["keyID" => "keyID"]
                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelDeleteAddr"]
                                    , "action" => "remove"
                                    , "qryAttributes" => ["keyID" => "keyID"]
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                                ]
                            ]
                    ]
                ]
            ]
        );


        return $OBJTableList->get();


    }

    public function delete($ArrayVariable)
    {
        $id = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $table = $this->getTable("address");
        $table->delete(array("id_address" => $id));
        return array("success" => "true");
    }

    protected function getModalEdit($ArrayVariable)
    {


        //Set Variables
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $anaCode = $this->K_COMMON->getVarArray($ArrayVariable, "anaCode");
        $idModal = "modalAddAddr";
        $value_keyID = "";
        $value_anaCode = "";
        $value_address = "";
        $value_city = "";
        $value_zip_code = "";
        $value_province = "";
        $value_country = "";
        $value_description = "";
        $value_addressNote = "";
        $value_attention = "";
        $value_legalAddress = "";
        $value_invoiceAddress = "";
        $value_shippingAddress = "";

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");
        $webModal->newModal(["id" => $idModal, "title" => $this->dictionary["TitleAddress"], "type" => "general"]);

        $table = $this->getTable("address");
        $table->get(array("id_address", "company_code", "address_name", "zip_code", "city", "province", "country", "site_type_description", "address_name_add", "address_attention", "legal_address", "invoice_address", "shipping_address"), array("id_address" => $keyID));
        if ($row = $table->fetch()) {
            $value_keyID = $row->id_address;
            $value_anaCode = $row->company_code;
            $value_address = $row->address_name;
            $value_city = $row->city;
            $value_zip_code = $row->zip_code;
            $value_province = $row->province;
            $value_country = $row->country;
            $value_description = $row->site_type_description;
            $value_addressNote = $row->address_name_add;
            $value_attention = $row->address_attention;
            $value_legalAddress = $row->legal_address;
            $value_invoiceAddress = $row->invoice_address;
            $value_shippingAddress = $row->shipping_address;
        }

        //create form
        $webModal->appendForm($webModal->modalBody,
            [
                "id" => "addressSettingForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "closeModal" => $idModal
            ]
        );

        //keyID hidden
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "id" => "keyIDModal", "name" => "keyID", "value" => $keyID]);
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "id" => "anaCode", "name" => "anaCode", "value" => $anaCode]);


        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //description
        $label = $this->dictionary["LabelAddressDescr"];
        $name = "description";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
        $webModal->appendInput($webModal->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_description]);

        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //note
        $label = $this->dictionary["LabelAddressNote"];
        $name = "addressNote";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
        $webModal->appendInput($webModal->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_addressNote]);

        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //attention
        $label = $this->dictionary["LabelAttention"];
        $name = "attention";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
        $webModal->appendInput($webModal->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_attention]);

        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //Country
        $label = $this->dictionary["LabelCountry"];
        $name = "country";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
        $webModal->appendInput($webModal->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "id" => $name
                , "name" => $name
                , "value" => $value_country
                , "typeLoad" => "function"
                , "pluginName" => "k_usefuldata"
                , "pluginObject" => "api"
                , "functionName" => "getCountry"
                , "pluginAction" => ""
                , "id" => "country"
                , "name" => "country"
            ]
        );

        //Province
        $label = $this->dictionary["LabelProvince"];
        $name = "province";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
        $webModal->appendInput($webModal->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "typeLoad" => "ajax"
                , "loadTrigger" => "change"
                , "loadTriggerRef" => "#country"
                , "parameterRef" => "country"
                , "pluginName" => "k_usefuldata"
                , "pluginObject" => "api"
                , "pluginAction" => "getProvince"
                , "id" => $name
                , "name" => $name
                , "value" => $value_province
                , "firstValueCall" => $value_country
            ]
        );

        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //City
        $label = $this->dictionary["LabelCity"];
        $name = "city";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 9]);
        $webModal->appendInput($webModal->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "typeLoad" => "ajax"
                , "loadTrigger" => "change"
                , "loadTriggerRef" => "#province"
                , "parameterRef" => "province"
                , "pluginName" => "k_usefuldata"
                , "pluginObject" => "api"
                , "pluginAction" => "getCity"
                , "id" => $name
                , "name" => $name
                , "value" => $value_city
                , "firstValueCall" => $value_province
            ]
        );

        //ZIP CODE
        $label = $this->dictionary["LabelZipCode"];
        $name = "zip_code";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 3]);
        $webModal->appendInput($webModal->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_zip_code]);

        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //ADDRESS
        $label = $this->dictionary["LabelAddress"];
        $name = "address";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
        $webModal->appendInput($webModal->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_address]);

        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //Legal Address
        $label = $this->dictionary["LabelLegalAddress"];
        $name = "legalAddress";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 4]);
        $webModal->appendInput($webModal->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value_legalAddress]);

        //Invoice Address
        $label = $this->dictionary["LabelInvoiceAddress"];
        $name = "invoiceAddress";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 4]);
        $webModal->appendInput($webModal->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value_invoiceAddress]);

        //Shipping Address
        $label = $this->dictionary["LabelShippingAddress"];
        $name = "shippingAddress";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 4]);
        $webModal->appendInput($webModal->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value_shippingAddress]);


        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //button
        $label = $this->dictionary["LabelSave"];
        $name = "btnSave";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 4]);
        $webModal->appendInput($webModal->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);


        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];

    }

}

?>