// create namespace
KXJS.namespace("k_registry.objects.address");
// create method of plugin
(function() {
	var t = this;
	t.addAddress = function(){
		var keyID = $(this).attr("keyID");
		var anaCode = $("#anaCode").val();
		waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'warning'});
		$.ajax({
			url: KXJS.buildAPICallUrl("KXCJS","k_registry","address","addAddress"),
			data: { 
				idModal:"modalAddAddr",
				keyID: keyID,
				anaCode:anaCode
				}
	
		}).done(function(data) {
			r = $.parseJSON(data);
			waitingDialog.hide();
			if(r.success == "true"){
				$(r.html).appendTo('body');
				$('#modalAddAddr').modal("show");
				$('#modalAddAddr').on('hidden.bs.modal', function (e) {
					$('#modalAddAddr').remove();
					t.loadTableList();
					
				});
		
				$('#modalAddAddr input[name="back"]').click(function(){$('#modalAddAddr').modal("hide");});
			}
			else
				alert("Error Security");
		});
	};
	t.loadTableList = function(){
		waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'warning'});
		var anaCode = $("#anaCode").val();
		$.ajax({
			url: KXJS.buildAPICallUrl("KXCJS","k_registry","address","getTableList"),
			data: { 
				onlyRow: "1",
				anaCode: anaCode
				}
	
		}).done(function(data) {
			r = $.parseJSON(data);
			waitingDialog.hide();
			if(r.success == "true"){
				$('#tabAddr tbody').html(r.html);
				kx_tablelist_script_addressTableList.assignTrigger();
				t.assignTrigger();
			}
			else
				alert("Error Security");
		});
		
	}
	t.assignTrigger = function(){
		$("#addressTableList .btnEdit").click(t.addAddress);
		
	}
	t.ready = function() {
		t.assignTrigger();
		$("#btnAddAddress").click(t.addAddress);
	}
	$(document).ready(t.ready);

}).apply(KXNS.k_registry.objects.address);
