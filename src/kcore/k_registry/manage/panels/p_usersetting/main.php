<?
/**
 * KIXIXI k_registry (p_usersetting)
 * Create a panel web object for change first name and last name of current user
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_registry
 * @subpackage objects\p_usersetting
 * @since 1.00
 */
namespace k_registry\manage\panels\p_usersetting;
class obj extends \k_webmanager\objects\general\base_panel\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();


    }

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //switch request
        switch ($KCA) {
            case "saveData" :
                return json_encode($this->saveData($ArrayVariable));
                
        }
    }

    /**
     * get this object
     *
     * @return object this object
     */
    public function get()
    {

        //set variables
        $value_name = "";
        $value_lastName = "";
        $anaCode = $this->K_COMMON->getCurrentUser();

        //get user data

        $table = $this->getTable("registry");
        $table->get(array("ana_last_name", "ana_first_name"), array("ana_code" => $anaCode));
        if ($row = $table->fetch()) {
            $value_name = $row->ana_first_name;
            $value_lastName = $row->ana_last_name;
        }

        //create form
        $this->appendForm("",
            [
                "id" => "userSettingForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
            ]
        );

        //NEW ROW
        $this->appendRow($this->lastForm);

        //Name
        $label = $this->dictionary["LabelName"];
        $name = "name";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_name, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //Last Name
        $label = $this->dictionary["LabelLastName"];
        $name = "lastName";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_lastName, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button Save
        $label = $this->dictionary["LabelSave"];
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);



        return $this;
    }

    protected function saveData($ArrayVariable)
    {

        $objUser = $this->K_COMMON->getPluginObject("k_registry", "user");

        return $objUser->saveData($ArrayVariable);
    }


}

?>