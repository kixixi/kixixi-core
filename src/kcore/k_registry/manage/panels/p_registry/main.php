<?
/**
 * KIXIXI k_registry (p_registry)
 * Create a panel web object for manage registry
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_registry
 * @subpackage objects\p_registry
 * @since 1.00
 */
namespace k_registry\manage\panels\p_registry;
class obj extends \k_webmanager\objects\general\base_panel\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authManageRegistry";

    /**
     * actual url of panel
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $urlActual = "";

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
    }

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

          //switch request
        switch ($KCA) {
            case "saveData" :
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "delete" :
                echo json_encode($this->delete($ArrayVariable));
                break;
        }
    }

    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "put", "get")
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {

        //set variables
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");
        $this->urlActual = $this->K_COMMON->getVarArray($ArrayVariable, "panelUrlActual");

        switch ($pluginMethod) {
            case "list":
            case "":
                $this->getPageList($ArrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }

        //Return
        return $this;
    }

    protected function getPageList($ArrayVariable)
    {

        //set variables
        $pluginParam = $this->K_COMMON->getVarArray($ArrayVariable, "pluginParam");
        $searchText = $pluginParam;

        //add files to include
        $this->addHeadFile([$this->getPathFileJS("list")]);

        //NEW ROW
        $this->appendRow("");

        //Input Search
        $label = $this->dictionary["LabelName"];
        $name = "searchField";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "", "required" => "1"]);

        //button Search
        $label = "&nbsp;";
        $name = "btnSearch";
        $this->appendColumn($this->lastRow, ["nCol" => 1]);
        $this->appendInput($this->lastCol, ["type" => "btn", "value" => '<i class="fa fa-search"></i>', "label" => $label, "id" => $name, "name" => $name, "semanticColor" => "primary"]);


        //NEW ROW
        $this->appendRow("");

        //get Table List
        $objTableList = $this->getTableList(["searchText" => $searchText]);
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendObject("", $objTableList);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button ADD Person
        $label = $this->dictionary["LabelAddPerson"];
        $name = "btnAddReg";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success", "link" => $this->urlActual . "put/person"]);

        //button ADD Company
        $label = $this->dictionary["LabelAddSociety"];
        $name = "btnAddReg";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success", "link" => $this->urlActual . "put/company"]);

    }

    protected function getPagePut($ArrayVariable = array())
    {
        
        //set variables
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");
        $pluginParam = $this->K_COMMON->getVarArray($ArrayVariable, "pluginParam");
        $society = false;
        $value_keyID = "";
        $value_name = "";
        $value_lastName = "";
        $value_fullName = "";
        $value_taxCode = "";
        $value_salutation = "";
        $value_profession = "";
        $value_sex = "";
        $value_birthDate = "";
        $value_placeOfBirth = "";
        $value_nationality = "";
        $value_documentType = "";
        $value_documentNumber = "";
        $value_docAuthority = "";
        $value_docPlace = "";
        $value_docCreateDate = "";
        $value_docExpiryDate = "";
        $value_language = "";
        $value_iSupplier = "";
        $value_note = "";
        $value_foreign = "";
        $value_regName = "";
        $value_legalShape = "";
        $value_VATnumber = "";
        $editingWeb = $this->K_COMMON->getEditingWeb();

        $anaCode = "";
        if ($pluginMethod == "get") {
            $keyID = $pluginParam;
            if ($keyID != "") {
                $table = $this->getTable("registry");
                $arrayValues = [
                    "id_registry"
                    , "ana_code"
                    , "ana_first_name"
                    , "ana_last_name"
                    , "ana_full_name"
                    , "ana_fiscal_code"
                    , "ana_salutation"
                    , "ana_profession"
                    , "ana_sex"
                    , "ana_birth_date"
                    , "ana_birth_place"
                    , "ana_nationality"
                    , "ana_document_type"
                    , "ana_document_number"
                    , "ana_document_issue_authority"
                    , "ana_document_issue_place"
                    , "ana_document_issue_date"
                    , "ana_document_expire_date"
                    , "ana_language"
                    , "ana_note"
                    , "ana_internal_supplier"
                    , "ana_foreign"
                    , "ana_registered_name"
                    , "ana_legal_shape"
                    , "ana_vat"

                ];

                $table->get($arrayValues, array("id_registry" => $pluginParam));
                if ($row = $table->fetch()) {
                    $anaCode = $row->ana_code;
                    $value_keyID = $row->id_registry;
                    $value_name = $row->ana_first_name;
                    $value_lastName = $row->ana_last_name;
                    $value_fullName = $row->ana_full_name;
                    $value_taxCode = $row->ana_fiscal_code;
                    $value_salutation = $row->ana_salutation;
                    $value_profession = $row->ana_profession;
                    $value_sex = $row->ana_sex;
                    $value_birthDate = $row->ana_birth_date;
                    $value_placeOfBirth = $row->ana_birth_place;
                    $value_nationality = $row->ana_nationality;
                    $value_documentType = $row->ana_document_type;
                    $value_documentNumber = $row->ana_document_number;
                    $value_docAuthority = $row->ana_document_issue_authority;
                    $value_docPlace = $row->ana_document_issue_place;
                    $value_docCreateDate = $row->ana_document_issue_date;
                    $value_docExpiryDate = $row->ana_document_expire_date;
                    $value_language = $row->ana_language;
                    $value_iSupplier = $row->ana_internal_supplier;
                    $value_note = $row->ana_note;
                    $value_foreign = $row->ana_foreign;
                    $value_regName = $row->ana_registered_name;
                    if ($row->ana_registered_name != "")
                        $society = true;
                    $value_legalShape = $row->ana_legal_shape;
                    $value_VATnumber = $row->ana_vat;
                }
            }
        } else {
            if ($pluginParam == "company")
                $society = true;
        }

        //Add name on top
        $this->appendHTML("", "<H2>" . $value_fullName . "</H2>");

        //create form
        $this->appendForm("",
            [
                "id" => "registryForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "reloadPage" => "1"
                , "reloadPageCheckCode" => "keyID"
                , "reloadPageGetCode" => "anaCode"
            ]
        );

        //keyID hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "keyID", "value" => $value_keyID]);
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "anaCode", "name" => "anaCode", "value" => $anaCode]);

        //Create TABS Array
        $arrayTabs = [];
        if ($society)
            $arrayTabs[] = [
                "label" => $this->dictionary["LabelTitleSociety"]
                , "active" => "1"
                , "id" => "tabCompany"
            ];
        else
            $arrayTabs[] = [
                "label" => $this->dictionary["LabelTitlePerson"]
                , "active" => "1"
                , "id" => "tabReg"
            ];
        $arrayTabs[] = ["id" => "tabGeneral", "label" => $this->dictionary["LabelTitleGeneral"]];
        if ($value_keyID != "") {
            $arrayTabs[] = ["id" => "tabContacts", "label" => $this->dictionary["LabelTitleContacts"]];
            $arrayTabs[] = ["id" => "tabAddr", "label" => $this->dictionary["LabelTitleAddress"]];
            $arrayTabs[] = ["id" => "tabLogin", "label" => $this->dictionary["LabelTitleLogin"]];
            $arrayTabs[] = ["id" => "tabAuth", "label" => $this->dictionary["LabelTitleAuth"]];
        }

        //create tabs
        $this->appendTab($this->lastForm,
            [
                "id" => "tabsRegistry"
                , "tabs" => $arrayTabs
            ]
        );

        if ($society) {
            /******* TAB COMPANY *******/

            //NEW ROW
            $this->appendRow("tabCompany");

            //company name
            $label = $this->dictionary["LabelRegName"];
            $name = "regName";
            $this->appendColumn($this->lastRow, ["nCol" => 4]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_regName]);

            //legal shape
            $label = $this->dictionary["LabelLegalShape"];
            $name = "legalShape";
            $this->appendColumn($this->lastRow, ["nCol" => 2]);
            $this->appendInput($this->lastCol,
                [
                    "type" => "select"
                    , "labelInput" => $label
                    , "id" => $name
                    , "name" => $name
                    , "value" => $value_legalShape
                    , "required" => "0"
                    , "typeLoad" => "function"
                    , "pluginName" => "k_usefuldata"
                    , "pluginObject" => "api"
                    , "functionName" => "getCustomFieldsValues"
                    , "functionVariables" => ["cfCode" => "LS"]
                ]
            );

            $this->appendRow("tabCompany");

            //Vat Number
            $label = $this->dictionary["LabelTaxCode"];
            $name = "taxCode";
            $this->appendColumn($this->lastRow, ["nCol" => 4]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_taxCode]);

            //Tax Code
            $label = $this->dictionary["LabelVATNumber"];
            $name = "VATnumber";
            $this->appendColumn($this->lastRow, ["nCol" => 4]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_VATnumber]);


        }
        else {
            /******* TAB PERSON *******/

            //NEW ROW
            $this->appendRow("tabReg");

            //name
            $label = $this->dictionary["LabelName"];
            $name = "name";
            $this->appendColumn($this->lastRow, ["nCol" => 3]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_name]);

            //last name
            $label = $this->dictionary["LabelLastName"];
            $name = "lastName";
            $this->appendColumn($this->lastRow, ["nCol" => 3]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_lastName]);

            //NEW ROW
            $this->appendRow("tabReg");

            //CF
            $label = $this->dictionary["LabelTaxCode"];
            $name = "taxCode";
            $this->appendColumn($this->lastRow, ["nCol" => 3]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_taxCode]);

            //greeting
            $label = $this->dictionary["LabelSalutation"];
            $name = "salutation";
            $this->appendColumn($this->lastRow, ["nCol" => 3]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_salutation]);

            //NEW ROW
            $this->appendRow("tabReg");

            //sex
            $label = $this->dictionary["LabelSex"];
            $name = "sex";
            $this->appendColumn($this->lastRow, ["nCol" => 3]);
            $this->appendInput($this->lastCol,
                [
                    "type" => "select"
                    , "labelInput" => $label
                    , "id" => $name
                    , "name" => $name
                    , "value" => $value_sex
                    , "required" => "0"
                    , "typeLoad" => "array"
                    , "options" => ["M" => $this->dictionary["Male"], "F" => $this->dictionary["Female"]]
                ]
            );

            //profession
            $label = $this->dictionary["LabelProfession"];
            $name = "profession";
            $this->appendColumn($this->lastRow, ["nCol" => 3]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_profession]);

            //NEW ROW
            $this->appendRow("tabReg");

            //Birth Date
            $label = $this->dictionary["LabelBirthDate"];
            $name = "birthDate";
            $this->appendColumn($this->lastRow, ["nCol" => 2]);
            $this->appendInput($this->lastCol, ["type" => "datePicker", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_birthDate, "minView" => "month", "startView" => "month"]);

            //Birth Place
            $label = $this->dictionary["LabelPlaceOfBirth"];
            $name = "placeOfBirth";
            $this->appendColumn($this->lastRow, ["nCol" => 4]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_placeOfBirth]);

            //NEW ROW
            $this->appendRow("tabReg");

            //Nationality
            $label = $this->dictionary["LabelNationality"];
            $name = "nationality";
            $this->appendColumn($this->lastRow, ["nCol" => 6]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_nationality]);

            //NEW ROW
            $this->appendRow("tabReg");

            //document type
            $label = $this->dictionary["LabelDocumentType"];
            $name = "documentType";
            $this->appendColumn($this->lastRow, ["nCol" => 3]);
            $this->appendInput($this->lastCol,
                [
                    "type" => "select"
                    , "labelInput" => $label
                    , "id" => $name
                    , "name" => $name
                    , "value" => $value_documentType
                    , "required" => "0"
                    , "typeLoad" => "array"
                    , "options" => ["DL" => $this->dictionary["LabelDocTypeDriveLicense"], "ID" => $this->dictionary["labelDocTypeID"], "PP" => $this->dictionary["labelDocTypePassport"], "OT" => $this->dictionary["labelDocTypeOther"]]
                ]
            );

            //doc number
            $label = $this->dictionary["LabelDocumentNumber"];
            $name = "documentNumber";
            $this->appendColumn($this->lastRow, ["nCol" => 3]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_documentNumber]);

            //NEW ROW
            $this->appendRow("tabReg");

            //authority
            $label = $this->dictionary["LabelAuthority"];
            $name = "docAuthority";
            $this->appendColumn($this->lastRow, ["nCol" => 3]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_docAuthority]);

            //authority place
            $label = $this->dictionary["LabelDocPlace"];
            $name = "docPlace";
            $this->appendColumn($this->lastRow, ["nCol" => 3]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_docPlace]);

            //NEW ROW
            $this->appendRow("tabReg");

            //document date
            $label = $this->dictionary["LabelDocCreateData"];
            $name = "docCreateDate";
            $this->appendColumn($this->lastRow, ["nCol" => 3]);
            $this->appendInput($this->lastCol, ["type" => "datePicker", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_docCreateDate, "minView" => "month", "startView" => "month"]);

            //document expiry
            $label = $this->dictionary["LabelDocExpiry"];
            $name = "docExpiryDate";
            $this->appendColumn($this->lastRow, ["nCol" => 3]);
            $this->appendInput($this->lastCol, ["type" => "datePicker", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_docExpiryDate, "minView" => "month", "startView" => "month"]);



        }

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->dictionary["LabelSave"];
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success", "class"=>"m-t-2"]);


        /******* TAB GENERAL *******/

        //NEW ROW
        $this->appendRow("tabGeneral");

        //Language
        $tableLN = $this->K_COMMON->getPluginDataObject("k_usefuldata", "languages");
        $tableLN->get(["value" => "iso", "text" => "name"]);
        $comboLanguages = $tableLN->getResults("array");
        $label = $this->dictionary["LabelLang"];
        $name = "language";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "id" => $name
                , "name" => $name
                , "value" => $value_language
                , "required" => "0"
                , "typeLoad" => "array"
                , "options" => $comboLanguages
            ]
        );

        //NEW ROW
        $this->appendRow("tabGeneral");

        //Internal Supplier
        $label = $this->dictionary["LabelISupplier"];
        $name = "iSupplier";
        $this->appendColumn($this->lastRow, ["nCol" => 2]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value_iSupplier]);

        //NEW ROW
        $this->appendRow("tabGeneral");

        //foreign
        $label = $this->dictionary["LabelForeign"];
        $name = "foreign";
        $this->appendColumn($this->lastRow, ["nCol" => 2]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value_foreign]);

        //NEW ROW
        $this->appendRow("tabGeneral");

        //preview
        $label = $this->dictionary["LabelNote"];
        $name = "note";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "textarea", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_note, "required" => "0"]);

        /******* TAB CONTACTS *******/

        //get contacts object
        if ($value_keyID != "")
        {
            $objContacts = $this->K_COMMON->getPluginObject("k_registry", "contact");
            $objContacts->get(["anaCode" => $anaCode]);
            $this->appendObject("tabContacts", $objContacts);
        }

        /******* TAB ADDRESS *******/

        //get address object
        if ($value_keyID != "")
        {
            $objAddress = $this->K_COMMON->getPluginObject("k_registry", "address");
            $objAddress->get(["anaCode" => $anaCode]);
            $this->appendObject("tabAddr", $objAddress);
        }

        /******* TAB LOGIN *******/

        //get login object
        if ($value_keyID != "")
        {
            $objLogin = $this->K_COMMON->getPluginObject("k_secauth", "managelogin");
            $objLogin->get(["anaCode" => $anaCode, "webCode" => $editingWeb]);
            $this->appendObject("tabLogin", $objLogin);
        }

        /******* TAB AUTHORIZATION *******/

        //get login authorization
        if ($value_keyID != "") {
            $objAuth = $this->K_COMMON->getPluginObject("k_secauth", "manageauthorization");
            $objAuth->get(["anaCode" => $anaCode, "webCode" => $editingWeb]);
            $this->appendObject("tabAuth", $objAuth);
        }


    }

    protected function delete($ArrayVariable)
    {
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $table = $this->getTable("registry");
        $table->delete(array("id_registry" => $keyID));
        return array("success" => "true");
    }

    protected function saveData($ArrayVariable)
    {
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $anaCode = $this->K_COMMON->getVarArray($ArrayVariable, "anaCode");
        $name = $this->K_COMMON->getVarArray($ArrayVariable, "name");
        $lastName = $this->K_COMMON->getVarArray($ArrayVariable, "lastName");
        $taxCode = $this->K_COMMON->getVarArray($ArrayVariable, "taxCode");
        $salutation = $this->K_COMMON->getVarArray($ArrayVariable, "salutation");
        $profession = $this->K_COMMON->getVarArray($ArrayVariable, "profession");
        $sex = $this->K_COMMON->getVarArray($ArrayVariable, "sex");
        $birthDate = $this->K_COMMON->getVarArray($ArrayVariable, "birthDate");
        $placeOfBirth = $this->K_COMMON->getVarArray($ArrayVariable, "placeOfBirth");
        $nationality = $this->K_COMMON->getVarArray($ArrayVariable, "nationality");
        $documentType = $this->K_COMMON->getVarArray($ArrayVariable, "documentType");
        $documentNumber = $this->K_COMMON->getVarArray($ArrayVariable, "documentNumber");
        $docAuthority = $this->K_COMMON->getVarArray($ArrayVariable, "docAuthority");
        $docPlace = $this->K_COMMON->getVarArray($ArrayVariable, "docPlace");
        $docCreateDate = $this->K_COMMON->getVarArray($ArrayVariable, "docCreateDate");
        $docExpiryDate = $this->K_COMMON->getVarArray($ArrayVariable, "docExpiryDate");
        $language = $this->K_COMMON->getVarArray($ArrayVariable, "language");
        $iSupplier = $this->K_COMMON->getVarArray($ArrayVariable, "iSupplier");
        $foreign = $this->K_COMMON->getVarArray($ArrayVariable, "foreign");
        $regName = $this->K_COMMON->getVarArray($ArrayVariable, "regName");
        $legalShape = $this->K_COMMON->getVarArray($ArrayVariable, "legalShape");
        $VATnumber = $this->K_COMMON->getVarArray($ArrayVariable, "VATnumber");
        $note = $this->K_COMMON->getVarArray($ArrayVariable, "note");


        $arrayValues = [];
        $arrayValues["ana_code"] = $anaCode;
        $arrayValues["ana_first_name"] = $name;
        $arrayValues["ana_last_name"] = $lastName;
        if ($regName != "")
            $arrayValues["ana_full_name"] = $regName;
        else
            $arrayValues["ana_full_name"] = $lastName . " " . $name;
        $arrayValues["ana_fiscal_code"] = $taxCode;
        $arrayValues["ana_salutation"] = $salutation;
        $arrayValues["ana_profession"] = $profession;
        $arrayValues["ana_sex"] = $sex;
        if ($birthDate != "")
            $arrayValues["ana_birth_date"] = $birthDate;
        $arrayValues["ana_birth_place"] = $placeOfBirth;
        $arrayValues["ana_nationality"] = $nationality;
        $arrayValues["ana_document_type"] = $documentType;
        $arrayValues["ana_document_number"] = $documentNumber;
        $arrayValues["ana_document_issue_authority"] = $docAuthority;
        $arrayValues["ana_document_issue_place"] = $docPlace;
        if ($docCreateDate != "")
            $arrayValues["ana_document_issue_date"] = $docCreateDate;
        if ($docExpiryDate != "")
            $arrayValues["ana_document_expire_date"] = $docExpiryDate;
        $arrayValues["ana_language"] = $language;
        $arrayValues["ana_note"] = $note;
        if ($iSupplier != "")
            $arrayValues["ana_internal_supplier"] = $iSupplier;
        else
            $arrayValues["ana_internal_supplier"] = 0;
        if ($foreign != "")
            $arrayValues["ana_foreign"] = $foreign;
        else
            $arrayValues["ana_foreign"] = 0;
        if ($regName != "")
            $arrayValues["ana_registered_name"] = $regName;
        else
            $arrayValues["ana_registered_name"] = "";
        $arrayValues["ana_legal_shape"] = $legalShape;
        $arrayValues["ana_vat"] = $VATnumber;


        $table = $this->getTable("registry");

        $table->showSQL();
        $table->putByKey($arrayValues);
        if ($keyID == "")
            $keyID = $table->getNewID();

        $anaCode = $this->getCompanyByID(["keyID" => $keyID]);
        return array("success" => "true", "message" => $this->dictionary["msgSuccessSave"], "anaCode" => $anaCode);
    }

    protected function getTableList($ArrayVariable = array())
    {


        $searchText = $this->K_COMMON->getVarArray($ArrayVariable, "searchText");
        $onlyRow = "";
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        //load value
        $table = $this->getTable("registry");
        if ($searchText != "")
            $table->get(["keyID"=>"id_registry","ana_code", "ana_full_name"], [" ana_full_name LIKE %searchText%"], ["searchText" => "%" . $searchText . "%"]);
        else
            $table->get(array("keyID"=>"id_registry","ana_code", "ana_full_name"));
        $dataTable = $table->getResults("array");

        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableList"
                , "tableID" => "tableList"
                , "hideHead" => $onlyRow
                , "data" => $dataTable
                , "fields" =>
                [
                    [
                        "label" => $this->dictionary["LabelCode"]
                        , "qryField" => "ana_code"
                    ]
                    ,
                    [
                        "label" => $this->dictionary["LabelName"]
                        , "qryField" => "ana_full_name"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->dictionary["labelEditReg"]
                                    , "action" => "edit"
                                    , "qryAttributes" => ["keyID" => "keyID","anaCode" => "ana_code"]
                                ]
                                ,
                                [
                                    "title" => $this->dictionary["labelDeleteReg"]
                                    , "action" => "remove"
                                    , "qryAttributes" => ["keyID" => "keyID"]
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                                ]
                            ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();




    }

    public function getCompanyByID($ArrayVariable = array())
    {
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        if ($keyID != "")
            $arrayWhere = ["id_registry" => $keyID];
        else
            $arrayWhere = [];
        $table = $this->getTable("registry");
        $table->get(["ana_code"], $arrayWhere);
        $array = array();
        $array[""] = "";
        if ($row = $table->fetch()) {
            return $row->ana_code;
        }
    }


}

?>