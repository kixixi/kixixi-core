// create namespace
KXJS.namespace("k_registry.objects.p_registry");
// create method of plugin
(function() {
	var t = this;

	t.searchReg = function(){
		var searchText = $("#searchField").val();
		location.href = KXJS.panelUrlActual + "list/"+searchText;
	}
	t.ready = function() {
		$("#btnSearch").click(t.searchReg)
	}

}).apply(KXNS.k_registry.objects.p_registry);

// document ready
$(document).ready(KXNS.k_registry.objects.p_registry.ready);