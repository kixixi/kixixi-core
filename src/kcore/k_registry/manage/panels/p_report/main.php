<?
/**
 * KIXIXI k_registry (p_report)
 * Create a panel web object for create registry report (use k_report)
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_registry
 * @subpackage objects\p_report
 * @since 1.00
 */
namespace k_registry\manage\panels\p_report;
class obj extends \k_report\objects\general\report_excel\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";



    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
    }

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");
        //get template
        switch ($KCA) {
            default :return parent::objectRequest($ArrayVariable);
            break;
        }
    }

    protected function getReportArrayFilterFields(){
        $arrayFilterSetting = [];
        $arrayFilterSetting["CLICODE"] = [
            "filterName"=>$this->dictionary["labelFiltersRegCode"]
            ,"filterSetting"=>[
                "tab"=>"registry"
                ,"type"=>"text"
                ,"parameters"=>[
                    "id" => "tabRegReCode"
                    , "name" => "tabRegRegCode"
                    , "size" => "sm"
                    , "labelInput" =>$this->dictionary["labelFiltersRegCode"]
                    , "attributes" => "filterName=\"CLICODE\""
                ]
            ]
        ];
        $arrayFilterSetting["ANAGFN"]= [
            "filterName"=>$this->dictionary["labelFiltersName"]
            ,"filterSetting"=>[
                "tab"=>"registry"
                ,"type"=>"text"
                ,"parameters"=>[
                    "id" => "tabRegTwsName"
                    ,"name" => "tabRegTwsName"
                    ,"labelInput" => $this->dictionary["labelFiltersName"]
                    ,"size" => "sm"
                    ,"attributes" => "filterName=\"ANAGFN\""
                ]
            ]
        ];

        $arrayFilterSetting["PROV"]= [
            "filterName"=>$this->dictionary["labelFiltersProvince"]
            ,"filterSetting"=>[
                "tab"=>"registry"
                ,"type"=>"text"
                ,"parameters"=> ["id" => "tabRegProvince"
                    , "name" => "tabRegProvince"
                    , "labelInput" => $this->dictionary["labelFiltersProvince"]
                    , "size" => "sm"
                    , "attributes" => "filterName=\"PROV\""
                ]
            ]
        ];
        $arrayFilterSetting["CITY"]= [
            "filterName"=>$this->dictionary["labelFiltersCity"]
            ,"filterSetting"=>[
                "tab"=>"registry"
                ,"type"=>"text"
                ,"parameters"=>[
                    "id" => "tabRegCity"
                    , "name" => "tabRegCity"
                    , "labelInput" => $this->dictionary["labelFiltersCity"]
                    , "size" => "sm"
                    , "attributes" => "filterName=\"CITY\""

                ]
            ]
        ];
        $arrayFilterSetting["COUNTRY"] = [
            "filterName"=>$this->dictionary["labelFiltersCountry"]
            ,"filterSetting"=>[
                "tab"=>"registry"
                ,"type"=>"text"
                ,"parameters"=>[
                    "id" => "tabRegCountry"
                    , "name" => "tabRegCountry"
                    , "labelInput" => $this->dictionary["labelFiltersCountry"]
                    , "size" => "sm"
                    , "attributes" => "filterName=\"COUNTRY\""

                ]
            ]
        ];
        $arrayFilterSetting["CLIACTDESC"] = [
            "filterName"=>$this->dictionary["labelFiltersActDefault"]
            ,"filterSetting"=>[
                "tab"=>"registry"
                ,"type"=>"select"
                ,"parameters"=>[
                    "id" => "tabRegActDefault"
                    , "name" => "tabRegActDefault"
                    , "labelInput" => $this->dictionary["labelFiltersActDefault"]
                    , "size" => "sm"
                    , "attributes" => "filterName=\"CLIACTDESC\""
                    , "typeLoad" => "function"
                    , "pluginName" => "k_usefuldata"
                    , "pluginObject" => "api"
                    , "functionName" => "getCustomFieldsValues"
                    , "functionVariables" => ["cfCode" => "ACTDEF"]

                ]
            ]
        ];
        //create array tabs
        $arrayTabsSetting = [];
        $arrayTabsSetting["registry"] = $this->dictionary["labelTabsInRegistry"];

        return ["filters"=>$arrayFilterSetting,"tabs"=>$arrayTabsSetting];

    }
    protected function getReportArrayFilterQuery(){
        $arrayFilterSetting = [];
        $arrayFilterSetting["CLICODE"] = [
            "condition"=>[
                "baseCondition"=>"AND cmp.ana_code in ('%value%')"
                ,"registry" => "AND reg.ana_code in ('%value%')"
            ]

        ];

        $arrayFilterSetting["ANAGFN"]= [
            "condition"=>[
                "baseCondition"=>" AND cmp.ana_full_name like '%%value%%'"
                ,"registry" => "AND reg.ana_full_name like '%%value%%'"
            ]

        ];


        $arrayFilterSetting["PROV"]= [
            "condition"=>[
                "baseCondition"=>" AND adr.province in ('%value%')"
                ,"address" => " AND adr.province in ('%value%')"
            ]

        ];
        $arrayFilterSetting["CITY"]= [
            "condition"=>[
                "baseCondition"=>"AND adr.city='%value%'"
                ,"address" =>" AND adr.city='%value%'"
            ]

        ];
        $arrayFilterSetting["COUNTRY"] = [
            "condition"=>[
                "baseCondition"=>"AND adr.country ='%value%'"
            ]

        ];
        $arrayFilterSetting["CLIACTDESC"] = [
            "condition"=>[
                "baseCondition"=>"AND cmp.ana_activity_description  ='%value%'"
                ,"registry"=>"AND reg.ana_activity_description  ='%value%'"
            ]

        ];


        return ["filters"=>$arrayFilterSetting];

    }
    protected function getReportArrayField($ArrayVariable)
    {
        $groupSelected = $this->K_COMMON->getVarArray($ArrayVariable, "groupSelected");
        $checkPermiss = $this->K_COMMON->getVarArray($ArrayVariable, "checkPermiss");
        
        $arrayField = [];
        if (!$checkPermiss || $this->isAuthorizedPlugin('authManageReport')) {
            $arrayField = array_merge($arrayField,
                [
                  'ANACODE' => [
                      "labelField"=>"C. Code"
                      ,"labelFieldExcel"=>$this->dictionary["labelFieldClientCode"]
                      ,"qryField"=>"cmp.ana_code"
                      ,"tableCode"=>"registry"

                  ]
                , 'CLIENT' => [
                    "labelField"=>"C. Name"
                    ,"labelFieldExcel"=>$this->dictionary["labelFieldFullName"]
                    ,"qryField"=>"cmp.ana_full_name"
                    ,"tableCode"=>"registry"
                ]
                , 'ANA_CF' => [
                    "labelField"=>"CF"
                    ,"labelFieldExcel"=>"CF"
                    ,"qryField"=>"cmp.ana_fiscal_code"
                    ,"tableCode"=>"registry"
                    ,"type"=>"string"
                ]
                , 'ANA_PI' => [
                    "labelField"=>"PI"
                    ,"labelFieldExcel"=>"PI"
                    ,"qryField"=>"cmp.ana_vat"
                    ,"tableCode"=>"registry"
                    ,"type"=>"string"
                ]
                , 'ADDRESS' => [
                    "labelField"=>"Address"
                    ,"labelFieldExcel"=>$this->dictionary["labelFieldAddress"]
                    ,"qryField"=>"concat(adr.address_name,' ',if(adr.address_number is null,'',adr.address_number))"
                    ,"tableCode"=>"address"
                ]
                , 'CITY' => [
                    "labelField"=>"City"
                    ,"labelFieldExcel"=>$this->dictionary["labelFieldCity"]
                    ,"qryField"=>"adr.city"
                    ,"tableCode"=>"registry"
                ]
                , 'PROV' => [
                    "labelField"=>"Province"
                    ,"labelFieldExcel"=>$this->dictionary["labelFieldProvince"]
                    ,"qryField"=>"adr.province"
                    ,"tableCode"=>"address"
                ]
                , 'COUNTRY' => [
                    "labelField"=>"Country"
                    ,"labelFieldExcel"=>"country"
                    ,"qryField"=>"country"
                    ,"tableCode"=>"registry"
                ]
                , 'STATE' => [
                    "labelField"=>"State"
                    ,"labelFieldExcel"=>"state"
                    ,"qryField"=>"state"
                    ,"tableCode"=>"address"
                ]
                , 'ZIPCODE' => [
                    "labelField"=>"CAP"
                    ,"labelFieldExcel"=>$this->dictionary["labelFieldZIP"]
                    ,"qryField"=>"adr.zip_code"
                    ,"tableCode"=>"address"
                    ,"type"=>"string"
                ]
                , 'TEL' => [
                    "labelField"=>"Telefono"
                    ,"labelFieldExcel"=>"Tel"
                    ,"qryField"=>"phone.contacts_value"
                    ,"tableCode"=>"registry"
                    ,"tableCode"=>"phone"
                ]
                , 'EM' => [
                    "labelField"=>"EMail"
                    ,"labelFieldExcel"=>"EMail"
                    ,"qryField"=>"email.contacts_value"
                    ,"tableCode"=>"email"
                ]
                , 'WWW' => [
                    "labelField"=>"Web Site"
                    ,"labelFieldExcel"=>"WWW"
                    ,"qryField"=>"webSite.contacts_value"
                    ,"tableCode"=>"webSite"
                ]
                , 'CLIACTDES' => [
                    "labelField"=>"Activity D."
                    ,"labelFieldExcel"=>"Activity D."
                    ,"qryField"=>"ana_activity_description"
                    ,"tableCode"=>"registry"
                ]
                ]);
        }

        $arrayReturn = [
            "fields" => $arrayField
            ,"tables" => [
                "address" => "left join k_registry_address adr ON adr.company_code=cmp.ana_code"
                ,"phone" => "left join (select company_code,min(contacts_value) contacts_value from k_registry_contacts WHERE contacts_type = 'TEL'  GROUP BY company_code ) phone ON phone.company_code=cmp.ana_code"
                ,"email" => "left join (select company_code,min(contacts_value) contacts_value from k_registry_contacts WHERE contacts_type = 'EM'  GROUP BY company_code ) email ON email.company_code=cmp.ana_code"
                ,"webSite" => "left join (select company_code,min(contacts_value) contacts_value from k_registry_contacts WHERE contacts_type = 'WWW'  GROUP BY company_code) webSite ON webSite.company_code=cmp.ana_code"
            ]
            ,"tablesDefault"=>
                [
                    "registry"=>"(select * from k_registry_registry reg where 1 = 1 %condition%) cmp"
                ]
        ];
        return $arrayReturn;
    }


}

?>