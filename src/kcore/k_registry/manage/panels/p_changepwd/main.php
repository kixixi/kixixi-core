<?
/**
 * KIXIXI k_registry (p_changepwd)
 * Create a panel web object for change password of current user
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_registry
 * @subpackage objects\p_changepwd
 * @since 1.00
 */
namespace k_registry\manage\panels\p_changepwd;
class obj extends \k_webmanager\objects\general\base_panel\obj
{
	/**
	 * Version of object
	 *
	 * @since 1.00
	 * @access public
	 * @var string
	 */
	var $ver = "1.0";

	/**
	 * Constructor
	 *
	 * @param array $arrayVariable array of variables
	 * The following directives can be used in the array variables:<br>
	 *    *    anaCode: user code to get addresses
	 */
	public function __construct($AV) {
		parent::__construct();
		$this->dictionary = $this->getDictionary();
		

	}

	/**
	 * Manage Actions from ajax
	 *
	 * @param array $ArrayVariable array of variables
	 * The following directives can be used in the array variables:<br>
	 *    *    KCA: Action to execute
	 *
	 * @return string|json|array depend on ajax type call
	 *
	 */
	public function objectRequest($ArrayVariable) {
		//set variable
		$KCA = $this->K_COMMON->getVarRequest("KCA");
		
		//switch request
		switch ($KCA) {
			case "saveData" :
				if (!$this->K_SECAUTH->verifyUserLogged())
					return array("success" => "false", "reason" => "nologin");
				else
					return json_encode($this->saveData($ArrayVariable));
			break;
		}
	}

	/**
	 * get this object
	 *
	 * @param array $ArrayVariable array of variables
	 * The following directives can be used in the array variables:<br>
	 *    *    pluginMethod: method of plugin ("", "get")
	 *
	 * @return object this object
	 */
	public function get($ArrayVariable = array())
	{


		$pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");
		

		switch ($pluginMethod) {
			case "get":
			case "":
				$this->getPagePut($ArrayVariable);

				break;

		}


		return $this;
	}

	protected function getPagePut($ArrayVariable)
	{


		//create form
		$this->appendForm("",
			[
				"id" => "form"
				, "setSavedMessage" => "1"
				, "kcp" => $this->namePlugin
				, "kco" => $this->namePluginObject
				, "kca" => "saveData"
			]
		);

		//NEW ROW
		$this->appendRow($this->lastForm);

		//old pwd
		$label = $this->dictionary["labelFormActualPwd"];
		$name = "oldPwd";
		$this->appendColumn($this->lastRow, ["nCol" => 3]);
		$this->appendInput($this->lastCol, ["type" => "password", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "", "required" => "1"]);

		//NEW ROW
		$this->appendRow($this->lastForm);

		//new pwd
		$label = $this->dictionary["labelFormNewPwd"];
		$name = "newPwd";
		$this->appendColumn($this->lastRow, ["nCol" => 3]);
		$this->appendInput($this->lastCol, ["type" => "password", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "", "required" => "1"]);

		//NEW ROW
		$this->appendRow($this->lastForm);

		//confirm pwd
		$label = $this->dictionary["labelFormConfNewPwd"];
		$name = "confPwd";
		$this->appendColumn($this->lastRow, ["nCol" => 3]);
		$this->appendInput($this->lastCol, ["type" => "password", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "", "required" => "1"]);

		//NEW ROW
		$this->appendRow($this->lastForm);

		//button
		$label = $this->dictionary["labelFormSave"];
		$name = "btnSave";
		$this->appendColumn($this->lastRow, ["nCol" => 3]);
		$this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

	}

	protected function saveData($arrayVariable){


        $objPassword = $this->K_COMMON->getPluginObject("k_secauth", "password");
        return $objPassword->change($arrayVariable);


	}
	
}
?>