<?
/**
 * KIXIXI k_registry (p_addresssetting)
 * Create a panel web object for change address of current user
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_registry
 * @subpackage objects\p_addresssetting
 * @since 1.00
 */
namespace k_registry\manage\panels\p_addresssetting;
class obj extends \k_webmanager\objects\general\base_panel\obj
{
	/**
	 * Version of object
	 *
	 * @since 1.00
	 * @access public
	 * @var string
	 */
	var $ver = "1.0";

	/**
	 * Constructor
	 *
	 * @param array $arrayVariable array of variables
	 * The following directives can be used in the array variables:<br>
	 *    *    anaCode: user code to get addresses
	 */
	public function __construct($AV) {
		parent::__construct();
		$this->dictionary = $this->getDictionary();
		
	}

	/**
	 * Manage Actions from ajax
	 *
	 * @param array $ArrayVariable array of variables
	 * The following directives can be used in the array variables:<br>
	 *    *    KCA: Action to execute
	 *
	 * @return string|json|array depend on ajax type call
	 *
	 */
	public function objectRequest($ArrayVariable) {

		//set variable
		$KCA = $this->K_COMMON->getVarRequest("KCA");

		//switch request
		switch ($KCA) {
			case "saveData" :
				return json_encode($this->saveData($ArrayVariable));
			break;
		}
	}

	/**
	 * get this object
	 *
	 * @return object this object
	 */
	public function get($ArrayVariable = []){

		//set variables
		$anaCode = $this->K_COMMON->getCurrentUser();
		$value_address = "";
		$value_city = "";
		$value_zip_code = "";
		$value_province = "";
		$value_country = "";

		//loadAddress
		$table = $this->getTable("address");
		$table->get(array("address_name", "zip_code", "city", "province", "country"), array("company_code" => $anaCode, "legal_address" => 1));
		if ($row = $table->fetch()) {
			$value_address = $row->address_name;
			$value_city = $row->city;
			$value_zip_code = $row->zip_code;
			$value_province = $row->province;
			$value_country = $row->country;
		}

		//create form
		$this->appendForm("",
			[
				"id" => "userForm"
				, "setSavedMessage" => "1"
				, "kcp" => $this->namePlugin
				, "kco" => $this->namePluginObject
				, "kca" => "saveData"
			]
		);

		//NEW ROW
		$this->appendRow($this->lastForm);

		//Country
		$label = $this->dictionary["LabelCountry"];
		$name = "country";
		$this->appendColumn($this->lastRow, ["nCol" => 3]);
		$this->appendInput($this->lastCol,
			[
				"type"=>"select"
				,"labelInput" => $label
				,"typeLoad"=>"function"
				,"pluginName"=>"k_usefuldata"
				, "pluginObject" => "api"
				,"functionName"=>"getCountry"
				,"pluginAction"=>""
				,"id"=>$name
				,"name"=>$name
				,"value" => $value_country
			]
		);

		//Province
		$label = $this->dictionary["LabelProvince"];
		$name = "province";
		$this->appendColumn($this->lastRow, ["nCol" => 3]);
		$this->appendInput($this->lastCol,
			[
				"type"=>"select"
				,"labelInput" => $label
				,"typeLoad"=>"ajax"
				,"loadTrigger"=>"change"
				,"loadTriggerRef"=>"#country"
				,"parameterRef"=>"country"
				,"pluginName"=>"k_usefuldata"
				, "pluginObject" => "api"
				,"pluginAction"=>"getProvince"
				,"id"=>$name
				,"name"=>$name
				,"value" => $value_province
				,"firstValueCall" => $value_country
			]
		);

		//NEW ROW
		$this->appendRow($this->lastForm);

		//City
		$label = $this->dictionary["LabelCity"];
		$name = "city";
		$this->appendColumn($this->lastRow, ["nCol" => 4]);
		$this->appendInput($this->lastCol,
			[
				"type"=>"select"
				,"labelInput" => $label
				,"typeLoad"=>"ajax"
				,"loadTrigger"=>"change"
				,"loadTriggerRef"=>"#province"
				,"parameterRef"=>"province"
				,"pluginName"=>"k_usefuldata"
				, "pluginObject" => "api"
				,"pluginAction"=>"getCity"
				,"id"=>$name
				,"name"=>$name
				,"value" => $value_city
				,"firstValueCall" => $value_province
			]
		);

		//ZIP CODE
		$label = $this->dictionary["LabelZipCode"];
		$name = "zip_code";
		$this->appendColumn($this->lastRow, ["nCol" => 2]);
		$this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_zip_code]);

		//NEW ROW
		$this->appendRow($this->lastForm);

		//ADDRESS
		$label = $this->dictionary["LabelAddress"];
		$name = "address";
		$this->appendColumn($this->lastRow, ["nCol" => 6]);
		$this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_address]);

		//NEW ROW
		$this->appendRow($this->lastForm);

		//button
		$label = $this->dictionary["LabelSave"];
		$name = "btnSave";
		$this->appendColumn($this->lastRow, ["nCol" => 3]);
		$this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

		return $this;


	}

	protected function saveData($ArrayVariable){

	    //set variable
		$anaCode = $this->K_COMMON->getCurrentUser();

        //include object
        $objRegApi = $this->K_COMMON->getPluginObject("k_registry", "api");

		//address variable
		$address = $this->K_COMMON->getVarArray($ArrayVariable,"address");
		$city = $this->K_COMMON->getVarArray($ArrayVariable,"city");
		$zipCode = $this->K_COMMON->getVarArray($ArrayVariable,"zip_code");
		$province = $this->K_COMMON->getVarArray($ArrayVariable,"province");
		$country = $this->K_COMMON->getVarArray($ArrayVariable,"country");
		
		//save address
		$addressArray = [];
		$addressArray["anaCode"] = $anaCode;
		$addressArray["address"] = $address;
		$addressArray["zipCode"] = $zipCode;
		$addressArray["city"] = $city;
		$addressArray["country"] = $country;
		$addressArray["province"] = $province;
		$addressArray["legalAddress"] = "1";
		$addressArray["uniqueRecord"] = "1";
		$saveAddressArray = $objRegApi->saveAddress($addressArray);

		if($saveAddressArray["success"] == "false")
			return $saveAddressArray;
			
		return array("success"=>"true","message"=>$this->dictionary["msgSuccessSave"]);
	}


	
}
?>