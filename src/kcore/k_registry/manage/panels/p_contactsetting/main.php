<?
/**
 * KIXIXI k_registry (p_contactsetting)
 * Create a panel web object for change contacts of current user
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_registry
 * @subpackage objects\p_contactsetting
 * @since 1.00
 */
namespace k_registry\manage\panels\p_contactsetting;
class obj extends \k_webmanager\objects\general\base_panel\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
    }

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //switch request
        switch ($KCA) {
            case "saveData" :
                return json_encode($this->saveData($ArrayVariable));
                break;
        }
    }

    /**
     * get this object
     *
     * @return object this object
     */
    public function get($ArrayVariable = [])
    {


        //set variables
        $anaCode = $this->K_COMMON->getCurrentUser();
        $value_phone = "";
        $value_prefix_phone = "";
        $value_mobilePhone = "";
        $value_prefix_mobilePhone = "";
        $value_email = "";

        //load Contacts
        $table = $this->getTable("contacts");
        $table->get(array("id_contacts", "contacts_type", "contacts_prefix", "contacts_value"), array("company_code" => $anaCode));
        while ($row = $table->fetch()) {
            if ($row->contacts_type == "TEL") {
                $value_phone = $row->contacts_value;
                $value_prefix_phone = $row->contacts_prefix;
            }
            if ($row->contacts_type == "CEL") {
                $value_mobilePhone = $row->contacts_value;
                $value_prefix_mobilePhone = $row->contacts_prefix;
            }
            if ($row->contacts_type == "EM")
                $value_email = $row->contacts_value;

        }

        //create form
        $this->appendForm("",
            [
                "id" => "userForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
            ]
        );

        //NEW ROW
        $this->appendRow($this->lastForm);

        //phone
        $label = $this->dictionary["LabelPhoneNumber"];
        $name = "phone";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "phone", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_phone, "prefixValue" => $value_prefix_phone]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //mobile phone
        $label = $this->dictionary["LabelMobilePhone"];
        $name = "mobilePhone";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "phone", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_mobilePhone, "prefixValue" => $value_prefix_mobilePhone]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //email
        $label = $this->dictionary["LabelEmail"];
        $name = "email";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_email]);


        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->dictionary["LabelSave"];
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);


        return $this;

    }

    

    protected function saveData($ArrayVariable)
    {

        //set variable
        $email = $this->K_COMMON->getVarArray($ArrayVariable, "email");
        $mobilePhone = $this->K_COMMON->getVarArray($ArrayVariable, "mobilePhone");
        $prefix_mobilePhone = $this->K_COMMON->getVarArray($ArrayVariable, "prefix_mobilePhone");
        $phone = $this->K_COMMON->getVarArray($ArrayVariable, "phone");
        $prefix_phone = $this->K_COMMON->getVarArray($ArrayVariable, "prefix_phone");
        $anaCode = $this->K_COMMON->getCurrentUser();

        //include object
        $objRegApi = $this->K_COMMON->getPluginObject("k_registry", "api");



        //saveEmail
        $AVcontact = [];
        $AVcontact["anaCode"] = $anaCode;
        $AVcontact["contactType"] = "EM";
        $AVcontact["contactValue"] = $email;
        $AVcontact["uniqueRecord"] = "1";
        $objRegApi->saveContact($AVcontact);

        //save cell
        $AVcontact = [];
        $AVcontact["anaCode"] = $anaCode;
        $AVcontact["contactType"] = "CEL";
        $AVcontact["contactValue"] = $mobilePhone;
        $AVcontact["contactPrefix"] = $prefix_mobilePhone;
        $AVcontact["uniqueRecord"] = "1";
        $objRegApi->saveContact($AVcontact);

        //save Phone
        $AVcontact = [];
        $AVcontact["anaCode"] = $anaCode;
        $AVcontact["contactType"] = "TEL";
        $AVcontact["contactValue"] = $phone;
        $AVcontact["contactPrefix"] = $prefix_phone;
        $AVcontact["uniqueRecord"] = "1";
        $objRegApi->saveContact($AVcontact);

        return array("success" => "true", "message" => $this->dictionary["msgSuccessSave"]);

    }

}

?>