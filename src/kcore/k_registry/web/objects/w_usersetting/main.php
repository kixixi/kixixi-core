<?
/**
 * KIXIXI k_registry (w_usersetting)
 * Create web object for signup
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\$k_registry
 * @subpackage web\objects\w_usersetting
 * @since 1.00
 */
namespace k_registry\web\objects\w_usersetting;
class obj extends \k_webmanager\objects\general\web_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();

    }

    /**
     * Manage Actions from ajax
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($arrayVariable)
    {
        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //switch request
        switch ($KCA) {
            case "saveData":
                return json_encode($this->saveData($arrayVariable));
                break;

            case "enableLoginFB":
                return json_encode($this->enableLoginFB($arrayVariable));
                break;
        }



    }

    /**
     * get this object
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *     showName: 1 for show name field
     *    *     showLastName: 1 for show last name field
     *
     * @return object this object
     */
    public function get($arrayVariable = [])
    {
        //set variables
        $showName = $this->K_COMMON->getVarArray($arrayVariable, "showName");
        $showTaxCode = $this->K_COMMON->getVarArray($arrayVariable, "showTaxCode");
        $showVATNumber = $this->K_COMMON->getVarArray($arrayVariable, "showVATNumber");
        $showBirthDateAndPlace = $this->K_COMMON->getVarArray($arrayVariable, "showBirthDateAndPlace");
        $showPhone = $this->K_COMMON->getVarArray($arrayVariable, "showPhone");
        $showMobilePhone = $this->K_COMMON->getVarArray($arrayVariable, "showMobilePhone");
        $showEmail = $this->K_COMMON->getVarArray($arrayVariable, "showEmail");
        $showAddress = $this->K_COMMON->getVarArray($arrayVariable, "showAddress");
        $typeAddress = $this->K_COMMON->getVarArray($arrayVariable, "typeAddress");
        $showLoginFB = $this->K_COMMON->getVarArray($arrayVariable, "showLoginFB");
        $anaCode = $this->K_COMMON->getCurrentUser();
        $value_name = "";
        $value_lastName = "";
        $value_taxCode = "";
        $value_VATNumber = "";
        $value_birthDate = "";
        $value_birthPlace = "";
        $value_phone = "";
        $value_prefix_phone = "";
        $value_mobilePhone = "";
        $value_prefix_mobilePhone = "";
        $value_email = "";
        $value_address = "";
        $value_city = "";
        $value_zip_code = "";
        $value_province = "";
        $value_country = "";
        $value_login_fb = 0;
        $typeAddressQryField = "legal_address";

        //get variables from objData
        $objData = $this->K_COMMON->getVarArray($arrayVariable, "objData");
        $tplCNTID = $this->K_COMMON->getVarArray($objData, "tplCNTID");


        //include JS
        $this->addHeadFile([$this->getPathFileJS("usersetting")]);
        $this->addExternalPlugin("facebook");


        //check variable
        switch ($typeAddress) {
            case "legal":
                $typeAddressQryField = "legal_address";
                break;
            case "invoice":
                $typeAddressQryField = "invoice_address";
                break;
            case "shipping":
                $typeAddressQryField = "shipping_address";
                break;
        }

        //include table
        $tableAddress = $this->K_COMMON->getPluginDataObject("k_registry", "address");
        $tableRegistry = $this->K_COMMON->getPluginDataObject("k_registry", "registry");

        //load registry
        $tableRegistry->get(["ana_last_name", "ana_first_name", "ana_fiscal_code","ana_vat", "ana_birth_date", "ana_birth_place", "ana_facebook_id"], ["ana_code" => $anaCode]);
        if ($row = $tableRegistry->fetch()) {
            $value_name = $row->ana_first_name;
            $value_lastName = $row->ana_last_name;;
            $value_taxCode = $row->ana_fiscal_code;
            $value_VATNumber = $row->ana_vat;
            if ($row->ana_facebook_id != "")
                $value_login_fb = 1;
            if ($row->ana_birth_date != "0000-00-00")
                $value_birthDate = $row->ana_birth_date;
            $value_birthPlace = $row->ana_birth_place;
        }
        //loadContacts
        $table = $this->K_COMMON->getPluginDataObject("k_registry", "contacts");
        $table->get(["id_contacts", "contacts_type", "contacts_prefix", "contacts_value"], ["company_code" => $anaCode]);
        while ($row = $table->fetch()) {
            if ($row->contacts_type == "TEL") {
                $value_phone = $row->contacts_value;
                $value_prefix_phone = $row->contacts_prefix;
            }
            if ($row->contacts_type == "CEL") {
                $value_mobilePhone = $row->contacts_value;
                $value_prefix_mobilePhone = $row->contacts_prefix;
            }
            if ($row->contacts_type == "EM") {
                $value_email = $row->contacts_value;
            }
        }


        //loadAddress
        $tableAddress->get(["address_name", "zip_code", "city", "province", "country"], [$typeAddressQryField => "1", "company_code" => $anaCode]);
        if ($row = $tableAddress->fetch()) {
            $value_address = $row->address_name;
            $value_city = $row->city;
            $value_zip_code = $row->zip_code;
            $value_province = $row->province;
            $value_country = $row->country;
        }

        //create form
        $this->appendForm("",
            [
                "id" => "userForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
            ]
        );

        //NEW ROW
        $this->appendRow($this->lastForm);

        if ($showName == "1") {
            //name
            $label = $this->dictionary["LabelName"];
            $name = "name";
            $this->appendColumn($this->lastRow, ["nCol" => 6]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_name]);

            //last name
            $label = $this->dictionary["LabelLastName"];
            $name = "lastName";
            $this->appendColumn($this->lastRow, ["nCol" => 6]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_lastName]);
        }

        if ($showPhone == "1") {

            //NEW ROW
            $this->appendRow($this->lastForm);

            //phone
            $label = $this->dictionary["LabelPhoneNumber"];
            $name = "phone";
            $this->appendColumn($this->lastRow, ["nCol" => 6]);
            $this->appendInput($this->lastCol, ["type" => "phone", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_phone, "prefixValue" => $value_prefix_phone]);
        }
        if ($showMobilePhone == "1") {
            //mobile phone
            $label = $this->dictionary["LabelMobilePhone"];
            $name = "mobilePhone";
            $this->appendColumn($this->lastRow, ["nCol" => 6]);
            $this->appendInput($this->lastCol, ["type" => "phone", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_mobilePhone, "prefixValue" => $value_prefix_mobilePhone]);
        }
        if ($showEmail == "1") {

            //NEW ROW
            $this->appendRow($this->lastForm);

            //email
            $label = $this->dictionary["LabelEmail"];
            $name = "email";
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_email, "required" => "1"]);
        }
        if ($showTaxCode == "1") {

            //NEW ROW
            $this->appendRow($this->lastForm);

            //CF
            $label = $this->dictionary["LabelTaxCode"];
            $name = "taxCode";
            $this->appendColumn($this->lastRow, ["nCol" => 6]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_taxCode]);
        }
        if ($showVATNumber == "1") {

            //VAT NUMBER
            $label = $this->dictionary["LabelVATNumber"];
            $name = "VATNumber";
            $this->appendColumn($this->lastRow, ["nCol" => 6]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_VATNumber]);
        }
        if ($showBirthDateAndPlace == "1") {

            //NEW ROW
            $this->appendRow($this->lastForm);

            //Birth Date
            $label = $this->dictionary["LabelBirthDate"];
            $name = "birthDate";
            $this->appendColumn($this->lastRow, ["nCol" => 4]);
            $this->appendInput($this->lastCol, ["type" => "datePicker", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_birthDate, "required" => "1", "minView" => "month", "startView" => "month"]);

            //Birth Place
            $label = $this->dictionary["LabelBirthPlace"];
            $name = "birthPlace";
            $this->appendColumn($this->lastRow, ["nCol" => 8]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_birthPlace]);
        }
        if ($showAddress == "1") {

            //NEW ROW
            $this->appendRow($this->lastForm);

            //Country
            $label = $this->dictionary["LabelCountry"];
            $name = "country";
            $this->appendColumn($this->lastRow, ["nCol" => 6]);
            $this->appendInput($this->lastCol,
                [
                    "type" => "select"
                    , "labelInput" => $label
                    , "typeLoad" => "function"
                    , "pluginName" => "k_usefuldata"
                    , "pluginObject" => "api"
                    , "functionName" => "getCountry"
                    , "pluginAction" => ""
                    , "id" => $name
                    , "name" => $name
                    , "value" => $value_country
                ]
            );

            //Province
            $label = $this->dictionary["LabelProvince"];
            $name = "province";
            $this->appendColumn($this->lastRow, ["nCol" => 6]);
            $this->appendInput($this->lastCol,
                [
                    "type" => "select"
                    , "labelInput" => $label
                    , "typeLoad" => "ajax"
                    , "loadTrigger" => "change"
                    , "loadTriggerRef" => "#country"
                    , "parameterRef" => "country"
                    , "pluginName" => "k_usefuldata"
                    , "pluginObject" => "api"
                    , "pluginAction" => "getProvince"
                    , "id" => $name
                    , "name" => $name
                    , "value" => $value_province
                    , "firstValueCall" => $value_country
                ]
            );

            //NEW ROW
            $this->appendRow($this->lastForm);

            //City
            $label = $this->dictionary["LabelCity"];
            $name = "city";
            $this->appendColumn($this->lastRow, ["nCol" => 9]);
            $this->appendInput($this->lastCol,
                [
                    "type" => "select"
                    , "labelInput" => $label
                    , "typeLoad" => "ajax"
                    , "loadTrigger" => "change"
                    , "loadTriggerRef" => "#province"
                    , "parameterRef" => "province"
                    , "pluginName" => "k_usefuldata"
                    , "pluginObject" => "api"
                    , "pluginAction" => "getCity"
                    , "id" => $name
                    , "name" => $name
                    , "value" => $value_city
                    , "firstValueCall" => $value_province
                ]
            );

            //ZIP CODE
            $label = $this->dictionary["LabelZipCode"];
            $name = "zip_code";
            $this->appendColumn($this->lastRow, ["nCol" => 3]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_zip_code]);

            //NEW ROW
            $this->appendRow($this->lastForm);

            //ADDRESS
            $label = $this->dictionary["LabelAddress"];
            $name = "address";
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_address]);
        }

        if ($showLoginFB == "1")
        {
            //NEW ROW
            $this->appendRow($this->lastForm);

            //LOGIN WITH FB
            $label = $this->dictionary["LabelEnableLoginFB"];
            $this->appendColumn($this->lastRow, ["nCol" => 3]);
            $this->appendHTML($this->lastCol, $label);

            $name = "loginWithFB";
            $this->appendColumn($this->lastRow, ["nCol" => 3]);
            /*
            $this->appendInput($this->lastCol,
                [
                    "type" => "checkbox",
                    //"labelInput" => $label,
                    "id" => $name,
                    "name" => $name,
                    "value" => "1",
                    "checked" => $value_login_fb,
                    "toggle" => "1",
                    "labelPos" => "SX",
                    "triggerFunction" => "KXNS.k_registry.web.objects.usersetting.enableLoginFB"
                ]
            );
*/
            $this->appendInput($this->lastCol,
                [
                    "type" => "btn",
                    //"labelInput" => $label,
                    "id" => $name,
                    "name" => $name,
                    "onoff" => $value_login_fb,
                    "toggle" => "1",
                    "triggerFunction" => "KXNS.k_registry.web.objects.usersetting.enableLoginFB"
                ]
            );

        }




        //add input value
        $name = "addressType";
        $this->appendInput($this->lastForm, ["type" => "hidden", "value" => $typeAddress, "id" => $name, "name" => $name]);
        $name = "refid";
        $this->appendInput($this->lastForm, ["type" => "hidden", "value" => $tplCNTID, "id" => $name, "name" => $name]);


        //NEW ROW
        $this->appendRow($this->lastForm);

        //LOGIN WITH FB
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendHTML($this->lastCol, "<br>");


        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->dictionary["LabelSave"];
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;
    }

    /**
     * get Parameterization field name last name
     *
     * @param array $param array with variables
     *        boolean $newRow: true/false for create new row
     *        int $nCol : number of column
     *
     * @return null
     */
    protected function appendEditShowName($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }
        //labelInput
        $label = "Show Name";
        $name = "showName";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    /**
     * get Parameterization field Tax Code
     *
     * @param array $param array with variables
     *        boolean $newRow: true/false for create new row
     *        int $nCol : number of column
     *
     * @return null
     */
    protected function appendEditShowTaxCode($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }
        //labelInput
        $label = "Show Tax Code";
        $name = "showTaxCode";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    /**
     * get Parameterization field VAT Number
     *
     * @param array $param array with variables
     *        boolean $newRow: true/false for create new row
     *        int $nCol : number of column
     *
     * @return null
     */
    protected function appendEditShowVATNumber($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }
        //labelInput
        $label = "Show VAT Number";
        $name = "showVATNumber";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    /**
     * get Parameterization field Birth Date And Place
     *
     * @param array $param array with variables
     *        boolean $newRow: true/false for create new row
     *        int $nCol : number of column
     *
     * @return null
     */
    protected function appendEditShowBirthDateAndPlace($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }
        //labelInput
        $label = "Show Birth Date And Place";
        $name = "showBirthDateAndPlace";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    /**
     * get Parameterization field phone
     *
     * @param array $param array with variables
     *        boolean $newRow: true/false for create new row
     *        int $nCol : number of column
     *
     * @return null
     */
    protected function appendEditShowPhone($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }
        //labelInput
        $label = "Show Phone";
        $name = "showPhone";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    /**
     * get Parameterization field mobile phone
     *
     * @param array $param array with variables
     *        boolean $newRow: true/false for create new row
     *        int $nCol : number of column
     *
     * @return null
     */
    protected function appendEditShowMobilePhone($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }
        //labelInput
        $label = "Show Mobile Phone";
        $name = "showMobilePhone";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    /**
     * get Parameterization field email
     *
     * @param array $param array with variables
     *        boolean $newRow: true/false for create new row
     *        int $nCol : number of column
     *
     * @return null
     */
    protected function appendEditShowEmail($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }
        //labelInput
        $label = "Show Email";
        $name = "showEmail";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    /**
     * get Parameterization field address
     *
     * @param array $param array with variables
     *        boolean $newRow: true/false for create new row
     *        int $nCol : number of column
     *
     * @return null
     */
    protected function appendEditShowAddress($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }
        //labelInput
        $label = "Show Address";
        $name = "showAddress";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    /**
     * get Parameterization field Login FB
     *
     * @param array $param array with variables
     *        boolean $newRow: true/false for create new row
     *        int $nCol : number of column
     *
     * @return null
     */
    protected function appendEditShowLoginFB($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }
        //labelInput
        $label = "Show Login FB Switch";
        $name = "showLoginFB";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }
    /**
     * get Parameterization type address
     *
     * @param array $param array with variables
     *        boolean $newRow: true/false for create new row
     *        int $nCol : number of column
     *
     * @return null
     */
    protected function appendEditAddressType($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }
        //labelInput
        $label = "Address Type";
        $name = "typeAddress";
        $addressTypeOption = ["legal" => "Legal", "invoice" => "Invoice", "shipping" => "Shipping"];
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $addressTypeOption, "noEmptyOption" => "1", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);
    }


    /**
     * get Parameterization Data
     *
     * @param array $objParameters array with variables
     * @return this object
     */
    public function parameterizationGetForm($objParameters)
    {
        $arrayFormField = $objParameters;
        $this->appendEditShowName($arrayFormField, true, 12);
        $this->appendEditShowTaxCode($arrayFormField, true, 12);
        $this->appendEditShowVATNumber($arrayFormField, true, 12);
        $this->appendEditShowBirthDateAndPlace($arrayFormField, true, 12);
        $this->appendEditShowPhone($arrayFormField, true, 12);
        $this->appendEditShowMobilePhone($arrayFormField, true, 12);
        $this->appendEditShowEmail($arrayFormField, true, 12);
        $this->appendEditShowAddress($arrayFormField, true, 12);
        $this->appendEditAddressType($arrayFormField, true, 12);
        $this->appendEditShowLoginFB($arrayFormField, true, 12);

        return $this;

    }

    /**
     * save Parameterization Data
     *
     * @param array $arrayVariable array with variables
     * Parameters: <br>
     *  *   showName:   1 for show name field
     *  *   showLastName:   1 for show last name field
     *
     * @return array
     */
    public function parameterizationSaveData($arrayVariable)
    {
        //set variables
        $showName = $this->K_COMMON->getVarArray($arrayVariable, "showName");
        $showTaxCode = $this->K_COMMON->getVarArray($arrayVariable, "showTaxCode");
        $showVATNumber = $this->K_COMMON->getVarArray($arrayVariable, "showVATNumber");
        $showBirthDateAndPlace = $this->K_COMMON->getVarArray($arrayVariable, "showBirthDateAndPlace");
        $showPhone = $this->K_COMMON->getVarArray($arrayVariable, "showPhone");
        $showMobilePhone = $this->K_COMMON->getVarArray($arrayVariable, "showMobilePhone");
        $showEmail = $this->K_COMMON->getVarArray($arrayVariable, "showEmail");
        $showAddress = $this->K_COMMON->getVarArray($arrayVariable, "showAddress");
        $typeAddress = $this->K_COMMON->getVarArray($arrayVariable, "typeAddress");
        $showLoginFB = $this->K_COMMON->getVarArray($arrayVariable, "showLoginFB");

        return [
            "showName" => $showName,
            "showTaxCode" => $showTaxCode,
            "showVATNumber" => $showVATNumber,
            "showBirthDateAndPlace" => $showBirthDateAndPlace,
            "showPhone" => $showPhone,
            "showMobilePhone" => $showMobilePhone,
            "showEmail" => $showEmail,
            "showLoginFB" => $showLoginFB,
            "showAddress" => $showAddress,
            "typeAddress" => $typeAddress
        ];
    }

    /**
     * save user setting
     *
     * @param array $arrayVariable array with variables
     * Parameters: <br>
     *  *   refid:          Web Object Template Content ID
     *  *   name:           Name of customer
     *  *   lastName:       Last Name of customer
     *  *   taxCode:        Tax Code
     *  *   VATNumber:      VAT Number
     *  *   birthDate:      Birth Date
     *  *   birthPlace:     Birth Place
     *  *   phone:          Phone Number
     *  *   prefix_phone:   Prefix Phone
     *  *   mobilePhone:    Mobile Phone Number
     *  *   email:          Email Address
     *  *   address:        Address
     *  *   city:           City
     *  *   zip_code:       Zip Code
     *  *   province:       Province
     *  *   country:        Country
     *  *   addressType:    Address Type legal/invoice/shipping
     *
     * * @see saveAddress
     * * @see saveContact
     * @return array
     */
    protected function saveData($arrayVariable)
    {

        if (!$this->K_SECAUTH->verifyUserLogged())
            return array("success" => "false", "reason" => "nologin", "message" => "Please Do Login");

        //set variable
        $refid = $this->K_COMMON->getVarArray($arrayVariable, "refid");
        $name = $this->K_COMMON->getVarArray($arrayVariable, "name");
        $lastName = $this->K_COMMON->getVarArray($arrayVariable, "lastName");
        $taxCode = $this->K_COMMON->getVarArray($arrayVariable, "taxCode");
        $VATNumber = $this->K_COMMON->getVarArray($arrayVariable, "VATNumber");
        $birthDate = $this->K_COMMON->getVarArray($arrayVariable, "birthDate");
        $birthPlace = $this->K_COMMON->getVarArray($arrayVariable, "birthPlace");
        $phone = $this->K_COMMON->getVarArray($arrayVariable, "phone");
        $prefix_phone = $this->K_COMMON->getVarArray($arrayVariable, "prefix_phone");
        $mobilePhone = $this->K_COMMON->getVarArray($arrayVariable, "mobilePhone");
        $prefix_mobilePhone = $this->K_COMMON->getVarArray($arrayVariable, "prefix_mobilePhone");
        $email = $this->K_COMMON->getVarArray($arrayVariable, "email");
        $address = $this->K_COMMON->getVarArray($arrayVariable, "address");
        $city = $this->K_COMMON->getVarArray($arrayVariable, "city");
        $zip_code = $this->K_COMMON->getVarArray($arrayVariable, "zip_code");
        $province = $this->K_COMMON->getVarArray($arrayVariable, "province");
        $country = $this->K_COMMON->getVarArray($arrayVariable, "country");
        $addressType = $this->K_COMMON->getVarArray($arrayVariable, "addressType");
        $anaCode = $this->K_COMMON->getCurrentUser();

        //include table
        $table = $this->K_COMMON->getPluginDataObject("k_registry", "registry");

        //include obj
        $kwebmanagerApi = $this->K_COMMON->getPluginObject("k_webmanager", "api");
        $objRegApi = $this->K_COMMON->getPluginObject("k_registry", "api");

        //load parameter of obj Used and get ID
        $objUsedData = $kwebmanagerApi->getObjUsed(["keyID" => $refid, "OM" => "table"]);
        if ($data = $objUsedData->fetch()) {
            $saveParameter = json_decode($data->tpo_wo_parameters, true);
            $showName = $this->K_COMMON->getVarArray($saveParameter, "showName");
            $showTaxCode = $this->K_COMMON->getVarArray($saveParameter, "showTaxCode");
            $showVATNumber = $this->K_COMMON->getVarArray($saveParameter, "showVATNumber");
            $showBirthDateAndPlace = $this->K_COMMON->getVarArray($saveParameter, "showBirthDateAndPlace");
            $showAddress = $this->K_COMMON->getVarArray($saveParameter, "showAddress");
            $showLoginFB = $this->K_COMMON->getVarArray($saveParameter, "showLoginFB");
            $showPhone = $this->K_COMMON->getVarArray($saveParameter, "showPhone");
            $showMobilePhone = $this->K_COMMON->getVarArray($saveParameter, "showMobilePhone");
            $showEmail = $this->K_COMMON->getVarArray($saveParameter, "showEmail");

        }
        else
            return $this->K_COMMON->resultReturn("JSON", false, ["reason" => "errorSecurity", "message" => "Error Security"]);

        //create query registry
        $valueAV = [];
        if ($showName == "1") {
            $valueAV["ana_first_name"] = $name;
            $valueAV["ana_last_name"] = $lastName;
        }
        if ($showTaxCode == "1")
            $valueAV["ana_fiscal_code"] = $taxCode;
        if ($showVATNumber == "1")
            $valueAV["ana_vat"] = $VATNumber;

        if ($showBirthDateAndPlace == "1") {
            $valueAV["ana_birth_date"] = $birthDate;
            $valueAV["ana_birth_place"] = $birthPlace;
        }
        $whereAV = [];
        $whereAV["ana_code"] = $anaCode;
        $table->put($valueAV, $whereAV);


        //check variable and save
        if ($showEmail == "1") {

            $AVcontact = [];
            $AVcontact["anaCode"] = $anaCode;
            $AVcontact["contactType"] = "EM";
            $AVcontact["contactValue"] = $email;
            $AVcontact["uniqueRecord"] = "1";
            $objRegApi->saveContact($AVcontact);

        }
        if ($showMobilePhone == "1") {

            //save cell
            $AVcontact = [];
            $AVcontact["anaCode"] = $anaCode;
            $AVcontact["contactType"] = "CEL";
            $AVcontact["contactValue"] = $mobilePhone;
            $AVcontact["contactPrefix"] = $prefix_mobilePhone;
            $AVcontact["uniqueRecord"] = "1";
            $objRegApi->saveContact($AVcontact);

        }
        if ($showPhone == "1") {

            //save Phone
            $AVcontact = [];
            $AVcontact["anaCode"] = $anaCode;
            $AVcontact["contactType"] = "TEL";
            $AVcontact["contactValue"] = $phone;
            $AVcontact["contactPrefix"] = $prefix_phone;
            $AVcontact["uniqueRecord"] = "1";
            $objRegApi->saveContact($AVcontact);



        }



        //check variable
        if ($showAddress == "1") {

            //save address
            $addressArray = [];
            $arraySaveAddress["anaCode"] = $anaCode;
            $arraySaveAddress["address"] = $address;
            $arraySaveAddress["city"] = $city;
            $arraySaveAddress["zipCode"] = $zip_code;
            $arraySaveAddress["province"] = $province;
            $arraySaveAddress["country"] = $country;
            $arraySaveAddress["uniqueRecord"] = "1";
            switch ($addressType) {
                case "legal":
                    $arraySaveAddress["legalAddress"] = 1;
                    break;
                case "invoice":
                    $arraySaveAddress["invoiceAddress"] = 1;
                    break;
                case "shipping":
                    $arraySaveAddress["shippingAddress"] = 1;
                    break;
            }
            $saveAddressArray = $objRegApi->saveAddress($arraySaveAddress);
;

            if ($saveAddressArray["success"] != "true")
                return $saveAddressArray;
        }
        return array("success" => "true", "message" => $this->dictionary["msgSuccessSave"]);

    }
    

    function enableLoginFB($ArrayVariable)
    {

        //set Variables
        $anaCode = $this->K_COMMON->getCurrentUser();
        $FB_appId = $this->getParamValue("FB_appId");
        $FB_secret = $this->getParamValue("FB_secret");
        $fbAccessToken = $this->K_COMMON->getVarArray($ArrayVariable, "fbAccessToken");
        $checked = $this->K_COMMON->getVarArray($ArrayVariable, "checked");


        //load table
        $tableREG = $this->K_COMMON->getPluginDataObject("k_registry", "registry");
        $tableCT = $this->K_COMMON->getPluginDataObject("k_registry", "contacts");



        //include FB
        $pathSDKFB = $this->K_COMMON->getPathSDK(["name" => "facebook"]);
        include_once($pathSDKFB . '/fbbase/facebook.php');


        if ($checked == "0")
        {
            //save data on registry
            $valueAV = [];
            $whereAV = [];
            $valueAV["ana_facebook_id"] = "";
            $whereAV["ana_code"] = $anaCode;
            $tableREG->put($valueAV, $whereAV);



            return ["success" => "true"];
        }


        if ($fbAccessToken != "") {

            //facebook object
            $facebook = new \Facebook(array(
                'appId' => $FB_appId,
                'secret' => $FB_secret
            ));

            //set facebook token
            $facebook->setAccessToken($fbAccessToken);

            //get user connected to facebook
            $fbuser = $facebook->getUser();
            if ($fbuser) {
                try {
                    //get facebook profile
                    $fb_profile = $facebook->api('/me?fields=id,first_name,last_name,email');

                    $fbid = $fb_profile["id"];
                    $email = $fb_profile['email'];


                    //Check if exist user with this fbid
                    $whereAV = [];
                    $whereAV["ana_facebook_id"] = $fbid;
                    $whereAV[] = "ana_code <> '$anaCode'";
                    $tableREG->get("*",$whereAV);
                    $data = $tableREG->fetch();

                    if ($data)
                        return ["success" => "false", "reason" => "fberror", "message" => $this->getDictionaryWord("msgUserExist")];

                    //Check if exist user with this  email
                    $whereAV = [];
                    $whereAV["contacts_value"] = $email;
                    $whereAV[] = "company_code <> '$anaCode'";
                    $tableCT->get("*",$whereAV);
                    $data = $tableCT->fetch();

                    if ($data)
                        return ["success" => "false", "reason" => "fberror", "message" => $this->getDictionaryWord("msgUserExist")];

                    //save data on registry
                    $valueAV = [];
                    $whereAV = [];
                    $valueAV["ana_facebook_id"] = $fbid;
                    $whereAV["ana_code"] = $anaCode;
                    $tableREG->put($valueAV, $whereAV);



                    return ["success" => "true"];
                }catch (FacebookApiException $e) {
                    return ["success" => "false", "reason" => "fberror", "message" => "Error connecting to Facebook"];
                }
            }
        }



    }
}

?>