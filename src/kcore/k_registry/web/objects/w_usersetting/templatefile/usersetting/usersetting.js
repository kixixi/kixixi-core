// create namespace
KXJS.namespace("k_registry.web.objects.usersetting");
// create method of plugin
(function () {
    var t = this;
    t.pluginName = "k_registry";
    t.pluginObject = "web\\objects\\w_usersetting";
    t.ready = function () {
        $("#loginWithFB").click(t.enableLoginFB);
    }

    this.enableLoginFB = function() {
        KXJS.loginFB({"scope":"email", "fnSuccess":t.enableLoginFBCall});
    }

    this.enableLoginFBCall = function(options) {
        var addVar = "";

        var checked = 0;
        if ($("#loginWithFB").attr("aria-pressed") == "true") checked = 1;

        fbme = options.me;
        fbatk = options.fbatk;

        addVar += "&fbAccessToken="+fbatk;
        addVar += "&checked="+checked;

        waitingDialog.show('Saving...', {dialogSize: 'sm', progressType: 'warning'});

        $.post(
            KXJS.buildAPICallUrl("KXCJS",t.pluginName,t.pluginObject,"enableLoginFB"),
            addVar,
            function(data) {
                r = $.parseJSON(data);
                waitingDialog.hide();

                if (r.success == "true"){
                }
                else
                {
                    $("#loginWithFB").prop('checked', false);
                    alert(r.message);
                }

            }
        );
    }

    this.init = function () {
        $(document).ready(t.ready);
    };

    t.init();

}).apply(KXNS.k_registry.web.objects.usersetting);



