<?
/**
 * KIXIXI $k_registry (w_signup)
 * Create web object for signup
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\$k_registry
 * @subpackage web\objects\w_signup
 * @since 1.00
 */
namespace k_registry\web\objects\w_signup;
class obj extends \k_webmanager\objects\general\web_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //switch request
        switch ($KCA) {
            case "signup":
            case "signupFB":
                $ArrayVariable["signupType"] = $KCA;
                return json_encode($this->checkAndSignup($ArrayVariable));
        }

    }

    /**
     * get this object
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    nexturl: url after signup
     *    *    showFBSignup: set 1 for show FBSignup btn
     *    *    showSignup: set 1 for show classic signup form
     *    *    labelSignupOldWay: text for label if showFBSignup and showSignup are set 1
     *    *    signupCheckbox: json with all checkbox to accept on classic signup
     *    *    dictionary: dictionary for custom label
     *    *    objData: data of web object
     *
     * @return object this object
     */
    public function get($arrayVariable = [])
    {


        //set variables
        $nextURL = $this->K_COMMON->getVarRequest("nexturl");
        if ($nextURL == "")
            $nextURL = $this->K_COMMON->getVarArray($arrayVariable, "nexturl");
        $showFBSignup = $this->K_COMMON->getVarArray($arrayVariable, "showFBSignup");
        $showSignup = $this->K_COMMON->getVarArray($arrayVariable, "showSignup");
        $labelSignupOldWay = $this->K_COMMON->getVarArray($arrayVariable, "labelSignupOldWay");
        $signupCheckbox = json_decode($this->K_COMMON->getVarArray($arrayVariable, "signupCheckbox"), true);
        $tmpDictionary = $this->K_COMMON->getVarArray($arrayVariable, "dictionary");

        //get variables from objData
        $objData = $this->K_COMMON->getVarArray($arrayVariable, "objData");
        $tplCNTID = $this->K_COMMON->getVarArray($objData, "tplCNTID");

        //add files to include
        $this->addHeadFile([$this->getPathFileJS("signup")]);
        $this->addHeadFile([$this->getPathFileCSS("signup")]);
        $this->addExternalPlugin("facebook");

        //NEW ROW
        $this->appendRow("");

        //Append CARD
        $this->appendColumn($this->lastRow, ["nCol" => 12, "offset" => ""]);


        //create form
        $this->appendForm($this->lastCol,
            [
                "id" => "signupform"
                , "setSavedMessage" => "1"
                , "functionSubmit" => "KXNS.k_registry.objects.w_signup.signup"
            ]
        );

        //hidden variables
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "nextURL", "name" => "nextURL", "value" => $nextURL]);
        $this->appendInput($this->lastForm, ["type" => "hidden", "name" => "refid", "value" => $tplCNTID]);

        if ($labelSignupOldWay != "") {
            //NEW ROW
            $this->appendRow($this->lastForm, ["id" => "rowOldWayLabel"]);
            $label = $labelSignupOldWay;

            //Signup with email label
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendHTML($this->lastCol, $label);
        }

        if ($showSignup == "1") {

            //NEW ROW
            $this->appendRow($this->lastForm);
            //firstname
            $label = $this->getDictionaryWord("labelSignupName");
            $name = "firstName";
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelPlaceholder" => $label, "id" => $name, "name" => $name, "noLabel" => "1", "required" => "1"]);

            //NEW ROW
            $this->appendRow($this->lastForm);
            //lastname
            $label = $this->getDictionaryWord("labelSignupLastName");
            $name = "lastName";
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelPlaceholder" => $label, "id" => $name, "name" => $name, "noLabel" => "1", "required" => "1"]);


            //NEW ROW
            $this->appendRow($this->lastForm);

            //email
            $label = $this->getDictionaryWord("labelSignupEmail");
            $name = "email";
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelPlaceholder" => $label, "id" => $name, "name" => $name, "noLabel" => "1", "required" => "1"]);

            //NEW ROW
            $this->appendRow($this->lastForm);

            //password
            $label = $this->getDictionaryWord("labelSignupPwd");
            $name = "passw";
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendInput($this->lastCol, ["type" => "password", "labelPlaceholder" => $label, "id" => $name, "name" => $name, "noLabel" => "1", "required" => "1"]);

        }


        //add all checkbox
        $i = 0;
        if (!empty($signupCheckbox))
            foreach ($signupCheckbox as $checkbox)
            {

                $checkRequired = $checkbox["checkRequired"];
                $checkLabel = $this->K_COMMON->getVarArray($tmpDictionary, $checkbox["checkLabel"]);
                if ($checkLabel == "")
                    $checkLabel = $checkbox["checkLabel"];
                $checkName = "signupCheckBox_" . $i;
                $i++;
                //NEW ROW
                $this->appendRow($this->lastForm);

                $label = $checkLabel;
                $name = $checkName;
                $this->appendColumn($this->lastRow, ["nCol" => 12]);
                $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "required" => $checkRequired]);
            }

        if($showSignup == "1"){
            //NEW ROW
            $this->appendRow($this->lastForm, ["class" => "m-b-1"]);

            //button Signup
            $label = $this->getDictionaryWord("signup");
            $name = "signupBtn";
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "blocked" => "1", "semanticColor" => "success"]);

        }

        if ($showFBSignup == "1") {
            //NEW ROW
            $this->appendRow($this->lastForm);

            //button Signup FB
            $label = $this->getDictionaryWord("labelSignupWithFB");
            $name = "signupFB";
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendInput($this->lastCol, ["type" => "btn", "value" => $label, "id" => $name, "name" => $name, "class" => "btnWithFB", "blocked" => "1"]);
        }

        return $this;
    }

    /**
     * check variable and call signup function
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    refid:        object used id
     *    *    smu:             1 if must send email to user
     *    *    signupType:      type of signup (normal or fb)
     *
     * @return array
     */
    public function checkAndSignup($arrayVariable = [])
    {


        //include obj
        $kwebmanagerApi = $this->K_COMMON->getPluginObject("k_webmanager", "api");
        $objSignup = $this->K_COMMON->getPluginObject("k_registry", "registration");

        //set variable
        $refid = $this->K_COMMON->getVarArray($arrayVariable, "refid");
        $signupType = $this->K_COMMON->getVarArray($arrayVariable, "signupType");

        //load parameter of obj Used
        $objUsedData = $kwebmanagerApi->getObjUsed(["keyID" => $refid, "OM" => "table"]);
        if ($data = $objUsedData->fetch()) {
            $parameter = json_decode($data->tpo_wo_parameters, true);
            $sendEmailToUser = $this->K_COMMON->getVarArray($parameter, "sendEmailToUser");
            $signupCheckbox = json_decode($this->K_COMMON->getVarArray($parameter, "signupCheckbox"), true);
            $arrayVariable["sendEmailToUser"] = $sendEmailToUser;

            //check variable and set field for registration
            $i = 0;
            if (!empty($signupCheckbox))
                foreach ($signupCheckbox as $checkbox) {
                    $checkRequired = $checkbox["checkRequired"];
                    $checkVal = $this->K_COMMON->getVarArray($arrayVariable, "signupCheckBox_" . $i);
                    $checkFieldCode = $checkbox["checkFieldCode"];
                    $arrayVariable["signupCheckbox"][$checkFieldCode] = $checkVal;
                    unset($arrayVariable["signupCheckBox_" . $i]);
                    $i++;
                    if ($checkRequired == "1" && $checkVal == "")
                        return ["success" => "false", "reason" => "errorCheckbox", "message" =>$this->getDictionaryWord("emsgCheckbox")];
                }
        }
        
        if ($signupType == "signup")
            return $objSignup->registration($arrayVariable);
        elseif ($signupType == "signupFB")
            return $objSignup->registrationFB($arrayVariable);
    }


    /**
     * get Parameterization Form
     *
     * @param array $objParameters array with variables
     *
     * @return webobject
     */
    protected function appendEditSignupFB($param = [], $newRow = true, $nCol = 6, $idRowUP = "")
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow($idRowUP);
        }
        //labelInput
        $label = "Show Signup FB";
        $name = "showFBSignup";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditSignup($param = [], $newRow = true, $nCol = 6, $idRowUP = "")
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow($idRowUP);
        }
        //labelInput
        $label = "Show Signup";
        $name = "showSignup";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);

    }

    protected function appendEditNextUrl($param = [], $newRow = true, $nCol = 6, $idRowUP = "")
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow($idRowUP);
        }
        //labelInput
        $label = "Url After Signup";
        $name = "nexturl";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditLabelOldWay($param = [], $newRow = true, $nCol = 6, $idRowUP = "")
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow($idRowUP);
        }
        //labelInput
        $label = "Label Signup with email";
        $name = "labelSignupOldWay";
        $placeHolder = $this->getDictionaryWord("labelSignupOldWay");
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelPlaceholder" => $placeHolder, "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendSendEmailToUser($param = [], $newRow = true, $nCol = 6, $idRowUP = "")
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow($idRowUP);
        }

        //labelInput
        $label = "Send Email To User";
        $name = "sendEmailToUser";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value]);


    }

    protected function appendSetSignupCheckbox($param = [], $newRow = true, $nCol = 6, $idRowUP = "",$webCode)
    {
        if ($newRow) {
            //NEW ROW
            $this->appendRow($idRowUP);
        }
        //labelInput
        $label = "Signup Checkbox";
        $name = "signupCheckbox";
        $placeHolder = "";
        $textHelp = "";
        $value = $this->K_COMMON->getVarArray($param, "signupCheckbox");
        $value_signupCheckbox = json_decode($value, true);

        $tmplPluginName = $this->K_COMMON->getVarArray($param, "tmplPluginName");
        $tmplPluginObject = $this->K_COMMON->getVarArray($param, "tmplPluginObject");

        $listGrupItems = [];
        $listGrupItems[] = ["type" => "text", "class" => "", "value" => "", "name" => "checkFieldCode", "labelPlaceholder" => "Field Code", "labelInput" => "Field Code"];
        $listGrupItems[] =
            [
                "type" => "combodictionary",
                "reloadOnFocus" => "1",
                "class" => "",
                "value" => "",
                "name" => "checkLabel",
                "pluginName" => $tmplPluginName,
                "pluginObject" => $tmplPluginObject,
                "webCode" => $webCode
            ];
        $listGrupItems[] = ["type" => "checkbox", "class" => "", "value" => "1", "name" => "checkRequired", "labelInput" => "Required"];
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendListGroup($this->lastCol,
            [
                "id" => $name
                , "values" => $value_signupCheckbox
                , "attributes" => 'style="height:200px; overflow: scroll; overflow-x: hidden;"'
                , "addingValues" =>
                [
                    "valuesHName" => $name
                    , "fields" => $listGrupItems
                ]
            ]
        );
    }

    public function parameterizationGetForm($objParameters, $objExtraParameters)
    {
        $arrayFormField = $objParameters;

        $tmplPluginName = $this->K_COMMON->getVarArray($objExtraParameters, "tmplPluginName");
        $tmplPluginObject = $this->K_COMMON->getVarArray($objExtraParameters, "tmplPluginObject");
        $arrayFormField["tmplPluginName"] = $tmplPluginName;
        $arrayFormField["tmplPluginObject"] = $tmplPluginObject;
        $webCode = $this->K_COMMON->getVarArray($objExtraParameters, "webCode");


        //create tab
        $tabsArray[] = ["id" => "tabObjGeneral", "label" => "General", "active" => "1"];
        $tabsArray[] = ["id" => "tabObjCheckBox", "label" => "CheckBox"];

        $this->appendTab("",
            [
                "id" => "tabObjSignup"
                , "tabs" => $tabsArray
            ]
        );

        $this->appendEditNextUrl($arrayFormField, true, 12, "tabObjGeneral");
        $this->appendEditLabelOldWay($arrayFormField, true, 12, "tabObjGeneral");

        $this->appendEditSignupFB($arrayFormField, true, 6, "tabObjGeneral");
        $this->appendEditSignup($arrayFormField, false, 6, "tabObjGeneral");

        $this->appendSendEmailToUser($arrayFormField, true, 6, "tabObjGeneral");

        $this->appendSetSignupCheckbox($arrayFormField, true, 12, "tabObjCheckBox", $webCode);


        return $this;

    }

    /**
     * save Parameterization Data
     *
     * @param array $AV array with variables
     *
     * @return webobject
     */
    public function parameterizationSaveData($AV)
    {
        $showFBSignup = $this->K_COMMON->getVarArray($AV, "showFBSignup");
        $showSignup = $this->K_COMMON->getVarArray($AV, "showSignup");
        $nexturl = $this->K_COMMON->getVarArray($AV, "nexturl");
        $labelSignupOldWay = $this->K_COMMON->getVarArray($AV, "labelSignupOldWay");
        $sendEmailToUser = $this->K_COMMON->getVarArray($AV, "sendEmailToUser");
        $signupCheckbox = $this->K_COMMON->getVarArray($AV, "signupCheckbox");
        if ($signupCheckbox == "") $signupCheckbox = "{}";


        return [
            "showFBSignup" => $showFBSignup
            , "showSignup" => $showSignup
            , "nexturl" => $nexturl
            , "labelSignupOldWay" => $labelSignupOldWay
            , "sendEmailToUser" => $sendEmailToUser
            , "signupCheckbox" => $signupCheckbox
        ];
    }

}

?>