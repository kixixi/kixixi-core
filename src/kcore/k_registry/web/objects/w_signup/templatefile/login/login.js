// create namespace
KXJS.namespace("k_secauth.objects.w_login");
// create method of plugin
(function() {
    var t = this;
    t.pluginName = "k_secauth";
    t.pluginObject= "web\\objects\\w_login";
   t.ready = function() {
       $("#pwdLost").click(t.pwdLost);
       $("#loginFB").click(t.loginFacebook);
       $("#signupBtn").click(t.signup);

    }

    t.loginFacebook = function(){
        KXJS.loginFB({"scope":"email", "fnSuccess":t.loginFb});
    }

    t.loginFb = function (options) {

        var sessionNotExpire = $("#sessionNotExpire").is(':checked');
        var addVar = "";

        fbme = options.me;
        fbatk = options.fbatk;

        if (sessionNotExpire) addVar = "sessionNotExpire=1";
        addVar += "&fbAccessToken="+fbatk;

        waitingDialog.show('Login...', {dialogSize: 'sm', progressType: 'warning'});

        $.post(
            KXJS.buildAPICallUrl("KXCJS","k_secauth","webservice","loginFB"),
            addVar,
            function(data) {

                var urlToLoad = $("#loginform #nextURL").val();
                
                r = $.parseJSON(data);
                waitingDialog.hide();

                if (r.success == "true"){
                    if ((urlToLoad != "") && (urlToLoad != location.href)) location.href = urlToLoad;
                    else location.reload();
                }
                else
                    KXJS.showSavedMessage($(".savedMessage"),r.message,false);
            }
        );

    }

    t.doLogin = function() {


        waitingDialog.show('Login...', {dialogSize: 'sm', progressType: 'warning'});

        $.post(
            KXJS.buildAPICallUrl("KXCJS","k_secauth","webservice","login")
            , $("#loginform").serialize()
            ,function(data) {
                var urlToLoad = $("#loginform #nextURL").val();

                r = $.parseJSON(data);

                waitingDialog.hide();

                if (r.success == "true"){
                    if ((urlToLoad != "") && (urlToLoad != location.href))
                        location.href = urlToLoad;
                    else
                        location.reload();
                }
                else
                if(r.success == "false")
                    KXJS.showSavedMessage($(".savedMessage"),r.message,false);
            }
        );
    }

    t.pwdLost = function() {

        var email = $('#loginform input[name="user"]').val();

        addVar = "&email=" + email;
        $.post(
            KXJS.buildAPICallUrl("KXCJS","k_secauth","webservice","pwdLost"),
            addVar,
            function(data) {
                r = $.parseJSON(data);
                KXJS.showSavedMessageJSON (".savedMessage",r);

            }
        );

    }

    t.signup = function() {
        location.href = "/signup?nexturl="+encodeURIComponent(head_nexturl); ;
    }

    t.pwdKeyUp = function(e) {
        if (e.which == 13)
            $("#loginform").submit();
    }


    this.init = function(){
        $(document).ready(t.ready);
    };

    t.init();

}).apply(KXNS.k_secauth.objects.w_login);



