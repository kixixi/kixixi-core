// create namespace
KXJS.namespace("k_registry.objects.w_signup");
// create method of plugin
(function () {
    var t = this;
    t.pluginName = "k_registry";
    t.pluginObject = "web\\objects\\w_signup";
    t.ready = function () {
        $("#signupFB").click(t.signupFacebook);
    }

    t.signupFacebook = function () {
        KXJS.loginFB({"scope": "email", "fnSuccess": t.signupFb});
    }

    t.signupFb = function (options) {
        var addVar = "";
        fbme = options.me;
        fbatk = options.fbatk;

        addVar += "&fbAccessToken=" + fbatk;

        waitingDialog.show('Signup...', {dialogSize: 'sm', progressType: 'warning'});

        $.post(
            KXJS.buildAPICallUrl("KXCJS", "k_registry", "web\\objects\\w_signup", "signupFB"),
            $("#signupform").serialize()+addVar,
            function (data) {
                var urlToLoad = $("#signupform #nextURL").val();

                r = $.parseJSON(data);
                waitingDialog.hide();
                if (r.success == "true") {
                    if ((urlToLoad != "") && (urlToLoad != location.href)) location.href = urlToLoad;
                    else location.reload();
                }
                else
                    KXJS.showSavedMessage($(".savedMessage"), r.message, false);
            }
        );

    }

    t.signup = function () {
        waitingDialog.show('signup...', {dialogSize: 'sm', progressType: 'warning'});
        $.post(
            KXJS.buildAPICallUrl("KXCJS", "k_registry", "web\\objects\\w_signup", "signup")
            , $("#signupform").serialize()
            , function (data) {
                var urlToLoad = $("#signupform #nextURL").val();
                r = $.parseJSON(data);
                waitingDialog.hide();
                if (r.success == "true") {
                    if ((urlToLoad != "") && (urlToLoad != location.href))
                        location.href = urlToLoad;
                    else
                        location.reload();
                }
                else if (r.success == "false")
                    KXJS.showSavedMessage($(".savedMessage"), r.message, false);
            }
        );
    }


    this.init = function () {
        $(document).ready(t.ready);
    };

    t.init();

}).apply(KXNS.k_registry.objects.w_signup);



