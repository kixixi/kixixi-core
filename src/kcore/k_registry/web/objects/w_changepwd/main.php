<?
/**
 * KIXIXI k_registry (w_changepwd)
 * Create web object for signup
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\$k_registry
 * @subpackage web\objects\w_changepwd
 * @since 1.00
 */
namespace k_registry\web\objects\w_changepwd;
class obj extends \k_webmanager\objects\general\web_object\obj
{	/**
	 * Version of object
	 *
	 * @since 1.00
	 * @access public
	 * @var string
	 */
	var $ver = "1.0";

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		$this->dictionary = $this->getDictionary();
	}

	/**
	 * Manage Actions from ajax
	 *
	 * @param array $arrayVariable array of variables
	 * The following directives can be used in the array variables:<br>
	 *    *    KCA: Action to execute
	 *
	 * @return string|json|array depend on ajax type call
	 *
	 */
	public function objectRequest($arrayVariable) {
		//set variable
		$KCA = $this->K_COMMON->getVarRequest("KCA");
		
		//switch request
		switch ($KCA) {
			case "saveData" :
					return json_encode($this->saveData($arrayVariable));
			break;
		}
	}

	/**
	 * get this object
	 *
	 * @param array $arrayVariable array of variables
	 * The following directives can be used in the array variables:<br>
	 *    *    pluginMethod: method of plugin ("", "get")
	 *
	 * @return object this object
	 */
	public function get($arrayVariable = [])
	{


		//create form
		$this->appendForm("",
			[
				"id" => "form"
				, "setSavedMessage" => "1"
				, "kcp" => $this->namePlugin
				, "kco" => $this->namePluginObject
				, "kca" => "saveData"
			]
		);

		//NEW ROW
		$this->appendRow($this->lastForm);

		//old pwd
		$label = $this->dictionary["labelFormActualPwd"];
		$name = "oldPwd";
		$this->appendColumn($this->lastRow, ["nCol" => 3]);
		$this->appendInput($this->lastCol, ["type" => "password", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "", "required" => "1"]);

		//NEW ROW
		$this->appendRow($this->lastForm);

		//new pwd
		$label = $this->dictionary["labelFormNewPwd"];
		$name = "newPwd";
		$this->appendColumn($this->lastRow, ["nCol" => 3]);
		$this->appendInput($this->lastCol, ["type" => "password", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "", "required" => "1"]);

		//NEW ROW
		$this->appendRow($this->lastForm);

		//confirm pwd
		$label = $this->dictionary["labelFormConfNewPwd"];
		$name = "confPwd";
		$this->appendColumn($this->lastRow, ["nCol" => 3]);
		$this->appendInput($this->lastCol, ["type" => "password", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "", "required" => "1"]);

		//NEW ROW
		$this->appendRow($this->lastForm);

		//button
		$label = $this->dictionary["labelFormSave"];
		$name = "btnSave";
		$this->appendColumn($this->lastRow, ["nCol" => 3]);
		$this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

		return $this;
	}

	/**
	 * get Parameterization Data
	 *
	 * @param array $objParameters array with variables
	 * @return this object
	 */
	public function parameterizationGetForm($objParameters)
	{
		return $this;

	}

	/**
	 * save Parameterization Data
	 *
	 * @param array $arrayVariable array with variables
	 * Parameters: <br>
	 *
	 * @return array
	 */
	public function parameterizationSaveData($arrayVariable)
	{
		//set variables


		return [];
	}

	/**
	 * Save Password
	 *
	 * @param array $arrayVariable array of variables
	 * The following directives can be used in the array variables:<br>
	 *      *   oldPwd:     old password
	 *      *   newPwd:   	new password
	 *      *   confPwd:    confirm new password
	 *
	 * @return array
	 */
	protected function saveData($arrayVariable){

		// Check User Logged
		if (!$this->K_SECAUTH->verifyUserLogged())
			return array("success" => "false", "reason" => "nologin","message"=>"Please Do Login");


		// get variables
		$oldPwd = $this->K_COMMON->getVarArray($arrayVariable, "oldPwd");
		$newPwd = $this->K_COMMON->getVarArray($arrayVariable, "newPwd");
		$confPwd = $this->K_COMMON->getVarArray($arrayVariable, "confPwd");


		// set variable
		$loginWebCodeParam = $this->getParamValue("loginWebCode", "", "k_secauth");
		if ($loginWebCodeParam != "")
			$webMngUser = $loginWebCodeParam;
		else
			$webMngUser = $this->K_COMMON->getCurrentWeb();
		$anaCode = $this->K_COMMON->getCurrentUser();
		$table = $this->K_COMMON->getPluginDataObject("k_secauth", "login");

		// Check if match new Pwd
		if ($newPwd != $confPwd) {

			return array("success"=>"false","reason" => "pwdnotmatch", "message"=>$this->dictionary["msgErrorPwdNotMatch"]);

		}

		//check if user logged
		if ($this->K_SECAUTH->verifyUserLogged()) {

			//load user data
			$table->get(["lgn_pwd","id_login"],["lgn_user_code" => $anaCode, "lgn_web_code" => $webMngUser]);
			$row = $table->fetch();

			//Check if user exist
			if ($row) //Ha trovato l'utente
			{

				//Check if old pwd match
				if (sha1($oldPwd) == $row->lgn_pwd) {

					//Update Pwd

					$table->put(["lgn_pwd" => sha1($newPwd)],["id_login" => $row->id_login]);
					return array("success"=>"true","message"=>$this->dictionary["msgSuccessSave"]);

				} else {

					//Old Pwd not match

					return array("success"=>"false","reason"=>"oldpwd","message"=>$this->dictionary["msgErrorOldPwd"]);
				}


			} else {
				return array("success"=>"false","reason"=>"nologin","message"=>"Data Error!");
			}


		}
		else
			return array("success"=>"false","reason"=>"nologin");

	}
	
}
?>

