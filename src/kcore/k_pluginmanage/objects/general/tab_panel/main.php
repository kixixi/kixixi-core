<?
namespace k_pluginmanage\objects\general\tab_panel;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    var $ver = "1.0";

    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";
    
    var $dictionary;

    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();

    }

    //manage action from ajax
    public function objectRequest($arrayVariable)
    {
        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "saveData":
                return json_encode($this->saveData($arrayVariable));
                break;
            case "getModalAdd":
                return json_encode($this->getModalAdd($arrayVariable));
                break;
            case "delete":
                return json_encode($this->delete($arrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableList($arrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
            case "getMenuUp":
                return $this->getMenuUp($arrayVariable);
                break;
            case "getMenuRef":
                return $this->getMenuRef($arrayVariable);
                break;
        }
    }

    public function get($arrayVariable = array())
    {

        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($arrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
                $this->getPageList($arrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($arrayVariable);
                break;
        }
        return $this;
    }

    protected function getPageList($arrayVariable)
    {


        //set variable
        $plgID = $this->K_COMMON->getVarArray($arrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($arrayVariable, "pluginObject");
        $pluginObject = str_replace("\\", "\\\\", $pluginObject);
        $listMyMList = $this->getTableList($arrayVariable);
        $this->appendHTML("", $listMyMList);
        $this->appendInput("", ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "btnAddPanel", "value" => $this->dictionary["LabelAdd"]
            , "openModal" => [
                "idModal" => "modalAddPanel"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAdd"
                , "parameters" => ["plgID" => $plgID, "pluginObject" => $pluginObject]
            ]
        ]);

    }

    protected function getTableList($arrayVariable)
    {

        // set variable
        $plgID = $this->K_COMMON->getVarArray($arrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($arrayVariable, "pluginObject");
        $onlyRow = $this->K_COMMON->getVarArray($arrayVariable, "onlyRow");

        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        if ($onlyRow == "1") {
            //Load Table
            $table = $this->K_COMMON->getPluginDataObject("k_panelmanage", "menu");
            $table->get(["id_menu", "pmn_menu_code", "pmn_label", "pmn_plugin_name", "pmn_plugin_object", "pmn_owner_plugin_name", "pmn_owner_plugin_object"],
                ["pmn_owner_plugin_name" => $plgID, "pmn_owner_plugin_object" => $pluginObject]);
            $dataTable = $table->getResults("array");
        } else
            $dataTable = "";
        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableListPanels"
                , "hideHead" => $onlyRow
                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getTableList"]
                , "data" => $dataTable
                , "fVar" => ["plgID" => $plgID, "pluginObject" => $pluginObject]
                , "dictionary" => $this->getDictionary($plgID, $pluginObject)
                , "fields" =>
                [
                    [
                        "label" => $this->dictionary["LabelListCode"]
                        , "qryField" => "pmn_menu_code"
                    ]
                    ,
                    [
                        "label" => $this->dictionary["LabelListLabel"]
                        , "type" => "dictionary"
                        , "qryField" => "pmn_label"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->dictionary["LabelListEdit"]
                                    , "action" => "editModal"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                    , "qryAttributes" => [
                                    "keyID" => "id_menu"
                                    , "plgID" => "pmn_owner_plugin_name"
                                    , "pluginObject" => "pmn_owner_plugin_object"
                                ]

                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelListDelete"]
                                    , "qryAttributes" => [
                                    "keyID" => "id_menu"
                                    , "plgID" => "pmn_owner_plugin_name"
                                    , "pluginObject" => "pmn_owner_plugin_object"
                                ]
                                    , "action" => "remove"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                                ]
                            ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();

    }

    public function getModalAdd($arrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($arrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(
            [
                "id" => $idModal
                , "title" => $this->dictionary["LabelModalTitleAddPanel"]
                , "type" => "general"
                , "width" => "900px"
            ]
        );
        $webModal->appendHtml($webModal->modalBody, $this->getPagePut($arrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getMenuUp($arrayVariable)
    {
        //set variable
        $keyIDToLeave = $this->K_COMMON->getVarArray($arrayVariable, "keyIDToLeave");
        if ($keyIDToLeave == "")
            $keyIDToLeave = "''";
        $plgID = $this->K_COMMON->getVarArray($arrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($arrayVariable, "pluginObject");
        $OM = $this->K_COMMON->getVarArray($arrayVariable, "OM");

        //get Table
        $table = $this->K_COMMON->getPluginDataObject("k_panelmanage", "menu");
        //get all websites for combo
        $table->get(["value" => "pmn_menu_code", "text" => "CONCAT(pmn_plugin_name,' => ',pmn_plugin_object,IF (pmn_plugin_object <> '' , ' => ' , ''),pmn_menu_code)"], ["pmn_plugin_name" => $plgID, "pmn_plugin_object" => "", "id_menu" => [" <> " . $keyIDToLeave]]);
        return $table->getResults($OM);
    }

    protected function getMenuRef($arrayVariable)
    {
        //set variable
        $keyIDToLeave = $this->K_COMMON->getVarArray($arrayVariable, "keyIDToLeave");
        if ($keyIDToLeave == "")
            $keyIDToLeave = "''";
        $plgID = $this->K_COMMON->getVarArray($arrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($arrayVariable, "pluginObject");
        $OM = $this->K_COMMON->getVarArray($arrayVariable, "OM");

        //get Table
        $table = $this->K_COMMON->getPluginDataObject("k_panelmanage", "menu");
        //get all websites for combo
        $table->get(["value" => "pmn_menu_code", "text" => "CONCAT(pmn_plugin_name,' => ',pmn_plugin_object,IF (pmn_plugin_object <> '' , ' => ' , ''),pmn_menu_code)"],
            ["pmn_plugin_name" => $plgID, "pmn_plugin_object" => [" <> ''"], "id_menu" => [" <> " . $keyIDToLeave]]);
        return $table->getResults($OM);
    }

    protected function getPagePut($arrayVariable)
    {

        //set variable
        $keyID = $this->K_COMMON->getVarArray($arrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($arrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($arrayVariable, "pluginObject");

        $value_menuCode = "";
        $value_menuUpCode = "";
        $value_menuRefCode = "";
        $value_order = "";
        $value_label = "";
        $value_icon = "";
        $value_auth_pName = "";
        $value_auth = "";
        $value_panelUrl = $pluginObject;
        $value_panelPluginName = $plgID;
        $value_panelPluginObject = $pluginObject;
        $value_panelPluginNameDisplay = $plgID;
        $value_panelPluginObjectDisplay = $pluginObject;
        $value_panelPluginMethod = "get";
        $value_panelTplCode = "";
        $value_authCode = "";


        //Load Table
        $table = $this->K_COMMON->getPluginDataObject("k_panelmanage", "menu");
        $table->get(["*"],
            ["pmn_owner_plugin_name" => $plgID, "pmn_owner_plugin_object" => $pluginObject, "id_menu" => $keyID]);
        $data = $table->fetch();
        if ($data) {
            $value_menuCode = $data->pmn_menu_code;
            $value_menuUpCode = $data->pmn_menu_up_code;
            $value_menuRefCode = $data->pmn_menu_ref_code;
            $value_order = $data->pmn_order;
            $value_label = $data->pmn_label;
            $value_icon = $data->pmn_icon;
            $value_auth_pName = $data->pmn_authorization_plugin_name;
            $value_authCode = $value_auth_pName . "\\" . $data->pmn_authorization;
            $value_authCode = str_replace("\\", "\\\\\\\\", $value_authCode);

            $value_panelUrl = $data->pmn_url;
            $value_panelPluginNameDisplay = $data->pmn_plugin_name;
            $value_panelPluginObjectDisplay = $data->pmn_plugin_object;
            $value_panelPluginName = $data->pmn_get_plugin_name;
            $value_panelPluginObject = $data->pmn_get_plugin_object;
            $value_panelPluginMethod = $data->pmn_get_plugin_method;
            $value_panelTplCode = $data->pmn_tpl_code;
        }
        //create form
        $this->appendForm("",
            [
                "id" => "pmnForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "reloadTableList" => "tableListPanels"
                , "closeModal" => "modalAddPanel"

            ]
        );

        //keyID hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "keyID", "value" => $keyID]);

        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plgID", "name" => "plgID", "value" => $plgID]);

        //object
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginObject", "name" => "pluginObject",
            "value" => $pluginObject]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        // menu code
        $label = $this->dictionary["LabelFormMenuCode"];
        $name = "menuCode";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_menuCode, "required" => "1"]);

        // label code
        $label = $this->dictionary["LabelFormLabelCode"];
        $name = "labelCode";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_combodictionary"
            , [
                "labelInput" => $label
                , "name" => $name
                , "value" => $value_label
                , "pluginName" => $plgID
                , "pluginObject" => $pluginObject
            ]
        );


        // order
        $label = $this->dictionary["LabelFormOrder"];
        $name = "order";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_order]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        // icon
        $label = $this->dictionary["LabelFormIcon"];
        $name = "icon";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_icon]);


        //NEW ROW
        $this->appendRow($this->lastForm);

        //plugin
        $label = $this->dictionary["LabelFormAuthPlugin"];
        $name = "selectPlugin";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                ,"labelInput" => $label
                ,"id" => $name
                ,"name" => $name
                ,"value" => $value_auth_pName
                ,"required" => "1"
                ,"typeLoad"=>"function"
                ,"pluginName"=>"k_secauth"
                ,"pluginObject"=>"api"
                ,"functionName"=>"getPluginsWithAuth"
            ]
        );


        //authorization
        $label = $this->dictionary["LabelFormAuth"];
        $name = "auth";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, [
            "type" => "select",
            "labelInput" => $label,
            "id" => $name,
            "name" => $name,
            "value" => $value_authCode,
            "required" => "0",
            "loadTriggerRef" => "#selectPlugin",
            "parameterRef" => "plugin",
            "typeLoad" => "ajax",
            "pluginName" => "k_usefuldata",
            "pluginObject" => "api",
            "pluginAction" => "getListAuthType",

        ]);







        // panel url
        $label = $this->dictionary["LabelPanelUrl"];
        $name = "panelUrl";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_panelUrl, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //*************** DISPLAY PLUGIN *********/
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "PLUGIN WHERE DISPLAY", "importance" => "6", "class" => "gameDate"]);


        // plugin name Display
        $label = $this->dictionary["LabelPluginNameDisplay"];
        $name = "panelPluginNameDisplay";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_comboplugin",
            ["labelInput" => $label, "withKCore" => "1", "id" => $name, "name" => $name, "value" => $value_panelPluginNameDisplay]);

        // plugin obj Display
        $label = $this->dictionary["LabelPluginObjDisplay"];
        $name = "panelPluginObjectDisplay";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_combopluginobjects",
            ["labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_panelPluginObjectDisplay
                , "loadTriggerRef" => "#panelPluginNameDisplay"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        // menu up code
        $label = $this->dictionary["LabelFormMenuUpCode"];
        $name = "menuUpCode";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "typeLoad" => "ajax"
                , "loadTrigger" => "change"
                , "loadTriggerRef" => ["#panelPluginNameDisplay", "#panelPluginObjectDisplay", "#keyID"]
                , "parameterRef" => ["plgID", "pluginObject", "keyIDToLeave"]
                , "pluginName" => "k_pluginmanage"
                , "pluginObject" => "tab_panel"
                , "pluginAction" => "getMenuUp"
                , "id" => $name
                , "name" => $name
                , "value" => $value_menuUpCode
            ]
        );


        // menu REF code
        $label = $this->dictionary["LabelFormMenuRefCode"];
        $textHelp = $this->dictionary["LabelFormMenuRefCodeHelp"];
        $name = "menuRefCode";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "textHelp" => $textHelp
                , "typeLoad" => "ajax"
                , "loadTrigger" => "change"
                , "loadTriggerRef" => ["#panelPluginNameDisplay", "#panelPluginObjectDisplay", "#keyID"]
                , "parameterRef" => ["plgID", "pluginObject", "keyIDToLeave"]
                , "pluginName" => "k_pluginmanage"
                , "pluginObject" => "tab_panel"
                , "pluginAction" => "getMenuRef"
                , "id" => $name
                , "name" => $name
                , "value" => $value_menuRefCode
            ]
        );


        //*************** PLUGIN TO GET *********/
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "PLUGIN TO GET", "importance" => "6", "class" => "gameDate"]);


        //NEW ROW
        $this->appendRow($this->lastForm);

        // plugin name
        $label = $this->dictionary["LabelPluginName"];
        $name = "panelPluginName";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_comboplugin",
            ["labelInput" => $label, "withKCore" => "1", "id" => $name, "name" => $name, "value" => $value_panelPluginName]);


        // plugin obj
        $label = $this->dictionary["LabelPluginObj"];
        $name = "panelPluginObject";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_combopluginobjects",
            ["labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_panelPluginObject
                , "loadTriggerRef" => "#panelPluginName"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        // plugin method
        $label = $this->dictionary["LabelPluginMethod"];
        $name = "panelPluginMethod";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_panelPluginMethod]);

        // template code
        $label = $this->dictionary["LabelTplCode"];
        $name = "panelTplCode";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_combopaneltemplates",
            ["labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_panelTplCode
                , "loadTriggerRef" => ["#panelPluginName", "#panelPluginObject"]]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->dictionary["LabelFormSave"];
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;
    }

    public function delete($arrayVariable)
    {
        $keyID = $this->K_COMMON->getVarArray($arrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($arrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($arrayVariable, "pluginObject");

        $table = $this->K_COMMON->getPluginDataObject("k_panelmanage", "menu");

        $table->delete(["id_menu" => $keyID, "pmn_owner_plugin_name" => $plgID, "pmn_owner_plugin_object" => $pluginObject]);

        return array("success" => "true");
    }

    public function saveData($arrayVariable)
    {

        //set variable
        $keyID = $this->K_COMMON->getVarArray($arrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($arrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($arrayVariable, "pluginObject");
        $menuCode = $this->K_COMMON->getVarArray($arrayVariable, "menuCode");
        $menuUpCode = $this->K_COMMON->getVarArray($arrayVariable, "menuUpCode");
        $menuRefCode = $this->K_COMMON->getVarArray($arrayVariable, "menuRefCode");
        $order = $this->K_COMMON->getVarArray($arrayVariable, "order");
        $labelCode = $this->K_COMMON->getVarArray($arrayVariable, "labelCode");
        $icon = $this->K_COMMON->getVarArray($arrayVariable, "icon");
        $auth = $this->K_COMMON->getVarArray($arrayVariable, "auth");
        $panelPluginNameDisplay = $this->K_COMMON->getVarArray($arrayVariable, "panelPluginNameDisplay");
        $panelPluginObjectDisplay = $this->K_COMMON->getVarArray($arrayVariable, "panelPluginObjectDisplay");
        $panelPluginName = $this->K_COMMON->getVarArray($arrayVariable, "panelPluginName");
        $panelPluginObject = $this->K_COMMON->getVarArray($arrayVariable, "panelPluginObject");
        $panelPluginMethod = $this->K_COMMON->getVarArray($arrayVariable, "panelPluginMethod");
        $panelTplCode = $this->K_COMMON->getVarArray($arrayVariable, "panelTplCode");
        $panelUrl = $this->K_COMMON->getVarArray($arrayVariable, "panelUrl");
        $arrayWhere = "";
        $plgNameAuth = "";


        //check variable
        $authCode = "";
        if ($auth != "")
        {
            $r = $this->K_COMMON->getAuthCodeData($auth);
            $plgNameAuth = $r["pluginName"];
            $authCode = $r["code"];

        }



        if ($panelUrl == "")
            $panelUrl = $pluginObject;


        //include tables
        $table = $this->K_COMMON->getPluginDataObject("k_panelmanage", "menu");

        if ($plgID == "" || $menuCode == "")
            return ["success" => "false", "reason" => "inputVoid", "message" => "Please fill in the fields!"];

        if ($keyID != "")
            $arrayWhere = ["id_menu" => $keyID];
        else {
            //check if cod dup
            $table->get(["id_menu"], ["pmn_owner_plugin_name" => $plgID, "pmn_owner_plugin_object" => $pluginObject, "pmn_menu_code" => $menuCode]);
            $data = $table->fetch();
            if ($data)
                return ["success" => "false", "reason" => "actionCodeDup", "message" => $this->dictionary["emsgPanelCodeDup"]];
        }


        $table->put(
            [
                "pmn_owner_plugin_name" => $plgID
                , "pmn_owner_plugin_object" => $pluginObject
                , "pmn_plugin_name" => $panelPluginNameDisplay
                , "pmn_plugin_object" => $panelPluginObjectDisplay
                , "pmn_menu_code" => $menuCode
                , "pmn_menu_up_code" => $menuUpCode
                , "pmn_menu_ref_code" => $menuRefCode
                , "pmn_order" => $order
                , "pmn_label" => $labelCode
                , "pmn_icon" => $icon
                , "pmn_authorization_plugin_name" => $plgNameAuth
                , "pmn_authorization" => $authCode
                , "pmn_get_plugin_name" => $panelPluginName
                , "pmn_get_plugin_object" => $panelPluginObject
                , "pmn_get_plugin_method" => $panelPluginMethod
                , "pmn_tpl_code" => $panelTplCode
                , "pmn_url" => $panelUrl
            ], $arrayWhere
        );

        return ["success" => "true"];
    }

}

?>