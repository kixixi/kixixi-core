<?
/**
 * PLUGIN MANAGE (parameters)
 *
 *
 * @paramor Marco Del Principe <m.delprincipe73@gmail.com>
 * @paramor Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_pluginmanage
 * @subpackage objects\parameters
 * @since 1.00
 */
namespace k_pluginmanage\objects\general\tab_parameters;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";
    


    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "saveData":
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "getModalAdd":
                return json_encode($this->getModalAdd($ArrayVariable));
                break;
            case "delete":
                return json_encode($this->delete($ArrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableList($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
        }
    }

    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "put", "get")
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {

        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
                $this->getPageList($ArrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    protected function getPageList($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");

        $listMyMList = $this->getTableList($ArrayVariable);
        $this->appendHTML("", $listMyMList);

        $this->appendInput("", ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "btnAddParam", "value" => $this->getDictionaryWord("LabelAdd")
            , "openModal" => [
                "idModal" => "modalAddParam"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAdd"
                , "parameters" => ["plgID" => $plgID]
            ]
        ]);

    }

    protected function getTableList($ArrayVariable)
    {

        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");

        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        if ($onlyRow == "1") {
            //Load Table
            $table = $this->K_COMMON->getPluginDataObject("k_common", "parameters_definition");
            $table->getByQuery("with_value", ["id_parameters_definition", "PRD.par_plugin_name", "PRD.par_code", "PRD.par_description", "par_value"],
                ["PR.par_web_code" => "", "PRD.par_plugin_name" => $plgID]);

            $dataTable = $table->getResults("array");
        } else
            $dataTable = "";
        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableListParams"
                , "hideHead" => $onlyRow
                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getTableList"]
                , "data" => $dataTable
                , "fVar" => ["plgID" => $plgID]

                , "fields" =>
                [
                    [
                        "label" => $this->getDictionaryWord("LabelListParamCode")
                        , "qryField" => "par_code"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListParamDescr")
                        , "qryField" => "par_description"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListParamValue")
                        , "qryField" => "par_value"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->getDictionaryWord("LabelListEdit")
                                    , "action" => "editModal"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                    , "qryAttributes" => [
                                    "keyID" => "id_parameters_definition"
                                    , "plgID" => "par_plugin_name"
                                    , "parCode" => "par_code"
                                ]

                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelListDelete")
                                    , "qryAttributes" => [
                                    "keyID" => "id_parameters_definition"
                                    , "plgID" => "par_plugin_name"
                                    , "parCode" => "par_code"
                                ]
                                    , "action" => "remove"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                                ]
                            ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();

    }

    protected function getModalAdd($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->getDictionaryWord("LabelModalTitleAddParam"), "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePut($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getPagePut($ArrayVariable)
    {

        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $parCode = $this->K_COMMON->getVarArray($ArrayVariable, "parCode");
        $value_code = "";
        $value_label = "";
        $value_parValue = "";
        $value_desc = "";
        $fieldDisable = "";


        //Load Table

        $table = $this->K_COMMON->getPluginDataObject("k_common", "parameters_definition");
        $table->getByQuery("with_value", [],
            ["PR.par_web_code" => "", "PRD.par_plugin_name" => $plgID, "PRD.id_parameters_definition" => $keyID, "PRD.par_code" => $parCode]);
        $data = $table->fetch();
        if ($data) {
            $value_code = $data->code;
            $value_label = $data->codeLabel;
            $value_parValue = $data->value;
            $value_desc = $data->description;
        }
        //create form
        $this->appendForm("",
            [
                "id" => "paramForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "reloadTableList" => "tableListParams"
                , "closeModal" => "modalAddParam"


            ]
        );

        if ($value_code != "")
            $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "paramCode", "name" => "paramCode", "value" => $value_code]);

        //keyID hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "keyID", "value" => $keyID]);

        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //param obj code
        if ($value_code != "")
            $fieldDisable = "1";
        $label = $this->getDictionaryWord("LabelFormCode");
        $name = "paramCode";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_code, "disabled" => $fieldDisable, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //param name
        $label = $this->getDictionaryWord("LabelFormLabelCode");
        $name = "paramLabelCode";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_combodictionary"
            , [
                "labelInput" => $label
                , "name" => $name
                , "value" => $value_label
                , "pluginName" => $plgID
            ]
        );

        //NEW ROW
        $this->appendRow($this->lastForm);

        //param is container
        $label = $this->getDictionaryWord("LabelFormValue");
        $name = "paramValue";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_parValue]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //param is container
        $label = $this->getDictionaryWord("LabelFormDescr");
        $name = "paramDescr";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_desc]);


        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->getDictionaryWord("LabelFormSave");
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;

    }

    protected function delete($ArrayVariable)
    {
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $parCode = $this->K_COMMON->getVarArray($ArrayVariable, "parCode");

        $tableDefinition = $this->K_COMMON->getPluginDataObject("k_common", "parameters_definition");
        $tableDefinition->delete(["par_plugin_name" => $plgID, "par_code" => $parCode]);


        $table = $this->K_COMMON->getPluginDataObject("k_common", "parameters");
        $table->delete(["par_web_code" => "", "par_plugin_name" => $plgID, "par_code" => $parCode]);

        return array("success" => "true");
    }

    protected function saveData($ArrayVariable)
    {

        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $paramCode = $this->K_COMMON->getVarArray($ArrayVariable, "paramCode");
        $paramLabelCode = $this->K_COMMON->getVarArray($ArrayVariable, "paramLabelCode");
        $paramValue = $this->K_COMMON->getVarArray($ArrayVariable, "paramValue");
        $paramDescr = $this->K_COMMON->getVarArray($ArrayVariable, "paramDescr");
        //include tables
        $tableParamDefinition = $this->K_COMMON->getPluginDataObject("k_common", "parameters_definition");
        $tableParam = $this->K_COMMON->getPluginDataObject("k_common", "parameters");


        if ($plgID == "" || $paramCode == "")
            return ["success" => "false", "reason" => "inputVoid", "message" => "Please fill in the fields!"];

        if ($keyID = "") {
            //check if cod dup
            $tableParamDefinition->get(["id_parameters"], ["plugin_name" => $plgID, "plugin_object" => $pluginObject, "par_code" => $paramCode]);
            $data = $tableParamDefinition->fetch();
            if ($data)
                return ["success" => "false", "reason" => "actionCodeDup", "message" => $this->getDictionaryWord("emsgParamCodeDup")];

        }
        $tableParamDefinition->putByKey(
            [
                "par_plugin_name" => $plgID
                , "par_code" => $paramCode
                , "par_code_label" => $paramLabelCode
                , "par_description" => $paramDescr

            ]
        );

        $tableParam->putByKey(
            [
                "par_plugin_name" => $plgID
                , "par_code" => $paramCode
                , "par_value" => $paramValue
                , "par_web_code" => ""
                

            ]
        );

        return ["success" => "true"];
    }


}

?>