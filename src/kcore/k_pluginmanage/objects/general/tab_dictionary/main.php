<?
/**
 * PLUGIN MANAGE (Actions)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_pluginmanage
 * @subpackage objects\actions
 * @since 1.00
 */
namespace k_pluginmanage\objects\general\tab_dictionary;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";

    /**
     * languages managef by default
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $dicLanguages;

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->dicLanguages = ["EN" => "EN", "IT" => "IT", "FR" => "FR", "ES" => "ES", "DE" => "DE"];

    }

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //set variables
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable,"webCode");

        //check authorization
        if (!$this->isAuthorizedWebSiteLayout($webCode) || !$this->isDeveloper())
            return json_encode(["success" => "false", "reason" => "securityError"]);

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "saveData":
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "getModalAdd":
                return json_encode($this->getModalAdd($ArrayVariable));
                break;
            case "delete":
                return json_encode($this->delete($ArrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableList($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
        }
    }

    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "put", "get")
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {


        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");
        switch ($pluginMethod) {
            case "":
            case "list":
                $this->getPageList($ArrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    protected function getPageList($ArrayVariable)
    {

         //set variable
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable,"webCode");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");

        //add files to include
        $this->appendRow("");
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                , "typeLoad" => "array"
                , "options" => $this->dicLanguages
                , "id" => "lngDictionary"
                , "labelInput" => $this->getDictionaryWord("LabelSelectLanguage")
                , "noEmptyOption" => "1"
                , "triggerEvent" => "change"
                , "triggerFunction" => "kx_tablelist_script_tableListDictionary.loadTableList"
            ]
        );

        //replace "\" on kco with double "\" for javascript
        $pluginObject = str_replace("\\", "\\\\", $pluginObject);
        $listMyMList = $this->getTableList($ArrayVariable);
        $this->appendHTML("", $listMyMList);
        $this->appendInput("", ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "btnAddDictionary", "value" => $this->getDictionaryWord("LabelAdd")
            , "openModal" => [
                "idModal" => "modalAddDictionary"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAdd"
                , "parameters" => ["plgID" => $plgID, "pluginObject" => $pluginObject, "webCode" => $webCode]
            ]
        ]);

    }

    protected function getTableList($ArrayVariable)
    {

        // set variable
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable,"webCode");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");
        $lng = $this->K_COMMON->getVarArray($ArrayVariable, "lngDictionary");
        if ($lng == "")
            $lng = $this->language;
        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        if ($onlyRow == "1") {
            //Load Table

            $table = $this->K_COMMON->getPluginDataObject("k_dictionary", "dictionary");
            $table->get(
                [
                    "id_dictionary",
                    "dic_web_code",
                    "dic_code",
                    "dic_word",
                    "dic_module",
                    "dic_module_object",
                    "dic_language_code"
                ],
                [
                    "dic_web_code" => $webCode,
                    "dic_module" => $plgID,
                    "dic_module_object" => $pluginObject,
                    "dic_language_code" => $lng
                ]
            );
            $dataTable = $table->getResults("array");
        } else
            $dataTable = "";
        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableListDictionary"
                , "hideHead" => $onlyRow
                , "data" => $dataTable
                , "fieldsPageToGet" => ["lngDictionary" => "lngDictionary"]
                , "ajax" =>
                [
                    "kcp" => $this->getPluginName(),
                    "kco" => $this->getPluginObjectName(),
                    "kca" => "getTableList"

                ]
                , "fVar" => ["plgID" => $plgID, "pluginObject" => $pluginObject, "webCode" => $webCode]
                , "fields" =>
                [
                    [
                        "label" => $this->getDictionaryWord("LabelListCode")
                        , "qryField" => "dic_code"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListWord")
                        , "qryField" => "dic_word"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->getDictionaryWord("LabelListEdit")
                                    , "action" => "editModal"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                    , "qryAttributes" => [
                                    "keyID" => "id_dictionary"
                                    , "langCode" => "dic_language_code"
                                    , "dicCode" => "dic_code"
                                    , "plgID" => "dic_module"
                                    , "pluginObject" => "dic_module_object"
                                    , "webCode" => "dic_web_code"
                                ]

                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelListDelete")
                                    , "qryAttributes" => [
                                        "webCode" => "dic_web_code"
                                    , "dicCode" => "dic_code"
                                    , "plgID" => "dic_module"
                                    , "pluginObject" => "dic_module_object"
                                ]
                                    , "action" => "remove"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                                ]
                            ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();

    }

    protected function getModalAdd($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->getDictionaryWord("LabelModalTitleAddDic"), "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePut($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getPagePut($ArrayVariable)
    {

        //set variable
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable,"webCode");
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $langCode = $this->K_COMMON->getVarArray($ArrayVariable, "langCode");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $valueDicCode = "";
        $valueDicWord = "";
        $value_link = "{}";

        //Load Table
        $tableDic = $this->K_COMMON->getPluginDataObject("k_dictionary", "dictionary");
        $tableLink = $this->K_COMMON->getPluginDataObject("k_dictionary", "dictionary_definition");


        if ($keyID != "") {
            //get dictionary data
            $tableDic->get(["dic_code", "dic_word"], ["dic_module" => $plgID, "dic_module_object" => $pluginObject, "id_dictionary" => $keyID]);
            $data = $tableDic->fetch();
            if ($data) {
                $valueDicCode = $data->dic_code;
                $valueDicWord = $data->dic_word;

                //get link data
                $tableLink->get(["dcd_link"], ["dcd_module" => $plgID, "dcd_module_object" => $pluginObject, "dcd_code" => $valueDicCode]);
                $data = $tableLink->fetch();
                if ($data) {
                    $value_link = $data->dcd_link;
                }
            }
        }
        //create form
        $this->appendForm("",
            [
                "id" => "dicForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "reloadTableList" => "tableListDictionary"
                , "closeModal" => "modalAddDictionary"

            ]
        );

        //keyID hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "keyID", "value" => $keyID]);

        //webCode hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "webCode", "name" => "webCode", "value" => $webCode]);

        //dic lang hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "langCode", "name" => "langCode", "value" => $langCode]);

        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);

        //object
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "obj", "name" => "pluginObject", "value" => $pluginObject]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //code
        $label = $this->getDictionaryWord("LabelFormCode");
        $name = "dicCode";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $valueDicCode, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //word
        $label = $this->getDictionaryWord("LabelFormWord");
        $name = "dicWord";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $valueDicWord, "textHelp" => $this->getDictionaryWord("LabelFormWordTextHelp"), "required" => "1"]);


        if ($keyID != "") {
            //NEW ROW
            $this->appendRow($this->lastForm);

            //Heading
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
            $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "LINK", "importance" => "5"]);
            $this->appendHTML($this->lastCol, "<small><center>" . $this->getDictionaryWord("LabelFormLinkHelp") . "</center></small>");

            //NEW ROW
            $this->appendRow($this->lastForm);

            $name = "linkData";
            $listGrupItems = [];
            $listGrupItems[] = ["param" => ["newRow" => "", "nCol" => "3"], "input" => ["type" => "text", "class" => "", "value" => "", "name" => "linkCode", "labelInput" => $this->getDictionaryWord("LabelFormLinkCode"), "labelPlaceholder" => "link1"]];
            $listGrupItems[] = ["param" => ["newRow" => "", "nCol" => "9"], "input" => ["type" => "text", "class" => "", "value" => "", "name" => "link", "labelInput" => $this->getDictionaryWord("LabelFormLinkVal"), "textHelp" => "Ex: /[language]/privacy"]];
            $listGrupItems[] = ["param" => ["newRow" => "1", "nCol" => "3"], "input" => ["type" => "select", "typeLoad" => "array", "options" => ["SELF" => "_self", "BLANK" => "_blank"], "class" => "", "value" => "", "name" => "target", "labelInput" => $this->getDictionaryWord("LabelFormLinkTarget")]];
            $listGrupItems[] = ["param" => ["newRow" => "", "nCol" => "3"], "input" => ["type" => "text", "class" => "", "value" => "", "name" => "class", "labelInput" => $this->getDictionaryWord("LabelFormLinkClass")]];
            $listGrupItems[] = ["param" => ["newRow" => "", "nCol" => "4"], "input" => ["type" => "text", "class" => "", "value" => "", "name" => "title", "labelInput" => $this->getDictionaryWord("LabelFormLinkTitle")]];
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendListGroup($this->lastCol,
                [
                    "id" => $name
                    , "values" => $value_link
                    , "attributes" => 'style="max-height:200px; overflow: scroll; overflow-x: hidden;"'
                    , "addingValues" =>
                    [
                        "valuesHName" => $name
                        , "fields" => $listGrupItems
                    ]
                ]
            );
        }



        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->getDictionaryWord("LabelFormSave");
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;

    }

    protected function delete($ArrayVariable)
    {
        //set variables
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable,"webCode");
        $dicCode = $this->K_COMMON->getVarArray($ArrayVariable, "dicCode");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");

        //include object
        $objDictionary = $this->K_COMMON->getPluginObject("k_dictionary", "objects\\general\\dictionary");

        // delete
        $objDictionary->delete(
            [
                "webCode" => $webCode,
                "word" => $dicCode,
                "pluginName" => $plgID,
                "pluginObject" => $pluginObject
            ]
        );

        return array("success" => "true");
    }

    protected function saveData($ArrayVariable)
    {

        //set variable
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable,"webCode");
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $langCode = $this->K_COMMON->getVarArray($ArrayVariable, "langCode");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $dicCode = $this->K_COMMON->getVarArray($ArrayVariable, "dicCode");
        $dicWord = $this->K_COMMON->getVarArray($ArrayVariable, "dicWord");
        $linkData = $this->K_COMMON->getVarArray($ArrayVariable, "linkData");
        if ($linkData == "")
            $linkData = [];
        else
            $linkData = json_decode($linkData, true);

        $dicLinkData = [];
        foreach ($linkData as $link) {
            $linkCode = $link["linkCode"];
            $linkVal = $link["link"];
            $linkTarget = $link["target"];
            $linkClass = $link["class"];
            $linkTitle = $link["title"];

            $dicLinkData[$linkCode] = [
                "code" => $linkCode
                , "target" => $linkTarget
                , "link" => $linkVal
                , "class" => $linkClass
                , "title" => $linkTitle
            ];
        }

        //include object
        $objDictionary = $this->K_COMMON->getPluginObject("k_dictionary", "objects\\general\\dictionary");

        if ($plgID == "" || $dicCode == "" || $dicWord == "")
            return ["success" => "false", "reason" => "inputVoid", "message" => "Please fill in the fields!"];

        if ($keyID == "") {


            //check if word exist
            $wordExist = $objDictionary->checkWordExist(
                [
                    "webCode" => $webCode,
                    "pluginName" => $plgID,
                    "pluginObject" => $pluginObject,
                    "word" => $dicCode,
                ]
            );

            if ($wordExist)
                return ["success" => "false", "reason" => "exist", "message" => "This Word Code Exist!"];

            foreach ($this->dicLanguages as $langCode) {
                $objDictionary->put(
                    [
                        "webCode" => $webCode,
                        "pluginName" => $plgID,
                        "pluginObject" => $pluginObject,
                        "language" => $langCode,
                        "word" => $dicCode,
                        "translation" => $dicWord,
                        "link" => $dicLinkData
                    ]
                );
            }
        } else {
            $objDictionary->put(
                [
                    "webCode" => $webCode,
                    "pluginName" => $plgID,
                    "pluginObject" => $pluginObject,
                    "language" => $langCode,
                    "word" => $dicCode,
                    "translation" => $dicWord,
                    "link" => $dicLinkData
                ]
            );
        }

        return ["success" => "true"];
    }


}

?>