<?
/**
 * PLUGIN MANAGE (tables)
 *
 *
 * @paramor Marco Del Principe <m.delprincipe73@gmail.com>
 * @paramor Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_pluginmanage
 * @subpackage objects\tables
 * @since 1.00
 */
namespace k_pluginmanage\objects\general\tab_tables;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";


    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "moveFieldUP":
                return json_encode($this->moveFieldUP($ArrayVariable));
                break;
            case "saveDataTable":
                return json_encode($this->saveDataTable($ArrayVariable));
                break;

            case "openModalCustomFields":
                return json_encode($this->openModalCustomFields($ArrayVariable));
                break;
            case "saveDataTableField":
                return json_encode($this->saveDataTableField($ArrayVariable));
                break;
            case "saveDataTableForeignKeys":
                return json_encode($this->saveDataTableForeignKeys($ArrayVariable));
                break;
            case "saveDataTableIndexesKey":
                return json_encode($this->saveDataTableIndexesKey($ArrayVariable));
                break;
            case "getModalAddTable":
                return json_encode($this->getModalAddTable($ArrayVariable));
                break;
            case "getModalAddTableWithCustomFields":
                return json_encode($this->getModalAddTableWithCustomFields($ArrayVariable));
                break;
            case "getModalAddTableField":
            case "getModalAddCustomField":
                return json_encode($this->getModalAddTableField($ArrayVariable));
                break;
            case "getModalAddTableForeignKeys":
                return json_encode($this->getModalAddTableForeignKeys($ArrayVariable));
                break;
            case "getModalAddTableIndexesKey":
                return json_encode($this->getModalAddTableIndexesKey($ArrayVariable));
                break;
            case "deleteTable":
                return json_encode($this->deleteTable($ArrayVariable));
                break;
            case "deleteTableWithCustomFields":
                return json_encode($this->deleteTableWithCustomFields($ArrayVariable));
                break;
            case "deleteTableField":
                return json_encode($this->deleteTableField($ArrayVariable));
                break;
            case "deleteTableForeignKeys":
                return json_encode($this->deleteTableForeignKeys($ArrayVariable));
                break;
            case "deleteTableIndexesKey":
                return json_encode($this->deleteTableIndexesKey($ArrayVariable));
                break;
            case "getTableListTables":
                $tableList = $this->getTableListTables($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
            case "getTableListTablesWithCustomField":
                $tableList = $this->getTableListTablesWithCustomField($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
            case "getTableListTableField":
                $tableList = $this->getTableListTableField($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
            case "getTableListTableForeignKeys":
                $tableList = $this->getTableListTableForeignKeys($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
            case "getTableListTableIndexesKey":
                $tableList = $this->getTableListTableIndexesKey($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
            case "getTableOptions":
                return $this->getTableOptions($ArrayVariable);
                break;
            case "getFieldOptions":
                return $this->getFieldOptions($ArrayVariable);
                break;
        }
    }

    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "put", "get")
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {

        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
                $this->getPageList($ArrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    protected function getPageList($ArrayVariable)
    {


        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");

        $this->appendRow("");
        $this->appendColumn($this->lastRow, ["id" => "colTableList", "nCol" => 3]);
        $this->appendColumn($this->lastRow, ["id" => "colFieldList", "nCol" => 9]);

        $this->appendRow("colTableList");
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "Plugin Tables", "importance" => "5"]);


        $listTables = $this->getTableListTables($ArrayVariable);
        $this->appendRow("colTableList");
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendHTML($this->lastCol, $listTables);
        $this->appendInput($this->lastCol, ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "btnAddTable", "value" => $this->getDictionaryWord("LabelAdd")
            , "openModal" => [
                "idModal" => "modalAddTable"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAddTable"
                , "parameters" => ["plgID" => $plgID, "pluginObject" => $pluginObject]
            ]
        ]);

        $this->appendRow("colTableList");
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "Tables Customized", "importance" => "5"]);
        $this->appendHTML($this->lastCol, "<small style='text-align=center;'>" . "List table with a custom field" . "</small>");


        $listTables = $this->getTableListTablesWithCustomField($ArrayVariable);
        $this->appendRow("colTableList");
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendHTML($this->lastCol, $listTables);
        $this->appendInput($this->lastCol, ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "btnAddTableWithCustomFields", "value" => $this->getDictionaryWord("LabelAdd")
            , "openModal" => [
                "idModal" => "modalAddTableWithCustomFields"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAddTableWithCustomFields"
                , "parameters" => ["plgID" => $plgID, "pluginObject" => $pluginObject]
            ]
        ]);


        //Load field


        $listTableFields = $this->getTableListTableField($ArrayVariable);

        //Load table
        $this->appendRow("colFieldList");
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $labelTitle = $this->getDictionaryWord("LabelFields");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $labelTitle, "importance" => "3"]);

        $this->appendRow("colFieldList");
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendHTML($this->lastCol, $listTableFields);
        $this->appendInput($this->lastCol, ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "btnAddTableField", "value" => $this->getDictionaryWord("LabelAdd")
            , "openModal" => [
                "idModal" => "modalAddTableField"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAddTableField"
                , "parametersRef" => ["tableName" => "tableListTableFields_tableName", "plgID" => "tableListTableFields_plgID", "pluginObject" => "tableListTableFields_pluginObject", "customBuilderPlugin" => "tableListTableFields_customBuilderPlugin"]
            ]
        ]);

        //Load Foreign keys

        $listTableForeignKeys = $this->getTableListTableForeignKeys($ArrayVariable);
        $this->appendRow("colFieldList");
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $labelTitle = $this->getDictionaryWord("LabelForeignKeys");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $labelTitle, "importance" => "3"]);

        $this->appendRow("colFieldList");
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendHTML($this->lastCol, $listTableForeignKeys);
        $this->appendInput($this->lastCol, ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "btnAddTableForeignKeys", "value" => $this->getDictionaryWord("LabelAdd")
            , "openModal" => [
                "idModal" => "modalAddTableForeignKeys"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAddTableForeignKeys"
                , "parametersRef" => ["tableName" => "tableListTableForeignKeys_tableName", "plgID" => "tableListTableForeignKeys_plgID", "pluginObject" => "tableListTableForeignKeys_pluginObject", "customBuilderPlugin" => "tableListTableForeignKeys_customBuilderPlugin"]
            ]
        ]);

        //Load Indexes Key

        $listTableIndexesKey = $this->getTableListTableIndexesKey($ArrayVariable);
        $this->appendRow("colFieldList");
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $labelTitle = $this->getDictionaryWord("LabelIndexesKey");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $labelTitle, "importance" => "3"]);

        $this->appendRow("colFieldList");
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendHTML($this->lastCol, $listTableIndexesKey);
        $this->appendInput($this->lastCol, ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "btnAddTableIndexesKey", "value" => $this->getDictionaryWord("LabelAdd")
            , "openModal" => [
                "idModal" => "modalAddTableIndexesKey"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAddTableIndexesKey"
                , "parametersRef" => ["tableName" => "tableListTableIndexesKey_tableName", "plgID" => "tableListTableIndexesKey_plgID", "pluginObject" => "tableListTableIndexesKey_pluginObject", "customBuilderPlugin" => "tableListTableIndexesKey_customBuilderPlugin"]
            ]
        ]);
    }

    protected function getTableListTables($ArrayVariable)
    {

        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");

        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        //load tables
        if ($onlyRow == "1") {
            $count = 0;
            $dataTable = [];

            //Get Plugin table
            $tablePluginData = $this->K_COMMON->getListPluginDBTables($plgID);

            while ($dataList = $tablePluginData->fetch()) {
                $name = $dataList->TABLE_NAME;
                $dataTable[$count]["name"] = $name;
                $dataTable[$count]["nameShort"] = str_replace($plgID . "_","",$name);
                $dataTable[$count]["plgID"] = $plgID;
                $dataTable[$count]["pluginObject"] = $pluginObject;
                $count++;
            }

        } else
            $dataTable = "";


        $OBJTableList->newTable(
            [
                "id" => "tableListTables"
                , "hideHead" => $onlyRow
                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getTableListTables"]
                , "data" => $dataTable
                , "fVar" => ["plgID" => $plgID, "pluginObject" => $pluginObject]
                , "fields" =>
                [
                    [
                        "label" => $this->getDictionaryWord("LabelListName")
                        , "qryField" => "nameShort"
                    ]

                    ,
                    [
                        "buttons" =>
                            [
                                /*
                                 [
                                     "title" => $this->getDictionaryWord("LabelListEdit")
                                     , "action" => "editModal"
                                     , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAddTable"]
                                     , "qryAttributes" => [
                                     "tableName" => "name"
                                     , "plgID" => "plgID"
                                     , "pluginObject" => "pluginObject"

                                 ]
                                     , "staticAttributes" => ["edit" => "1"]
                                 ]
                                 ,
                                */
                                [
                                    "title" => $this->getDictionaryWord("LabelListEditField")
                                    , "action" => "refreshTableList"
                                    , "tableID" => ["tableListTableFields", "tableListTableForeignKeys", "tableListTableIndexesKey"]
                                    , "icon" => "fa-table"
                                    , "qryAttributes" => [
                                    "tableName" => "name"
                                    , "plgID" => "plgID"
                                    , "pluginObject" => "pluginObject"
                                ]
                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelListDelete")
                                    , "qryAttributes" => [
                                    "tableName" => "name"
                                    , "plgID" => "plgID"
                                    , "pluginObject" => "pluginObject"
                                ]
                                    , "action" => "remove"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "deleteTable"]
                                ]
                            ]
                    ]
                ]
            ]
        );
        return $OBJTableList->get();

    }

    protected function getTableListTablesWithCustomField($ArrayVariable)
    {

        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");

        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        //load tables
        if ($onlyRow == "1") {
            $count = 0;
            $dataTable = "";
            //Get Plugin table
            $this->K_COMMON->getListPluginDBTablesWithCustomField($plgID);

            while ($dataList = $this->kxdb->fetch()) {
                $name = $dataList->TABLE_NAME;
                $customPlgID = $this->K_COMMON->getPluginNameByTable($name);
                $customBuilderPlugin = "";
                if ($customPlgID != $plgID)
                    $customBuilderPlugin = $plgID;
                $dataTable[$count]["name"] = $name;
                $dataTable[$count]["plgID"] = $customPlgID;
                $dataTable[$count]["customBuilderPlugin"] = $customBuilderPlugin;
                $dataTable[$count]["pluginObject"] = $pluginObject;
                $count++;
            }


        } else
            $dataTable = "";

        $OBJTableList->newTable(
            [
                "id" => "tableListTablesWithCustomFields"
                , "hideHead" => $onlyRow
                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getTableListTablesWithCustomField"]
                , "data" => $dataTable
                , "fVar" => ["plgID" => $plgID, "pluginObject" => $pluginObject]
                , "fields" =>
                [
                    [
                        "label" => $this->getDictionaryWord("LabelListName")
                        , "qryField" => "name"
                    ]

                    ,
                    [
                        "buttons" =>
                            [
                                /*
                                 [
                                     "title" => $this->getDictionaryWord("LabelListEdit")
                                     , "action" => "editModal"
                                     , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAddTable"]
                                     , "qryAttributes" => [
                                     "tableName" => "name"
                                     , "plgID" => "plgID"
                                     , "pluginObject" => "pluginObject"

                                 ]
                                     , "staticAttributes" => ["edit" => "1"]
                                 ]
                                 ,
                                */
                                [
                                    "title" => $this->getDictionaryWord("LabelListEditField")
                                    , "action" => "refreshTableList"
                                    , "tableID" => ["tableListTableFields", "tableListTableForeignKeys", "tableListTableIndexesKey"]
                                    , "icon" => "fa-table"
                                    , "qryAttributes" => [
                                    "tableName" => "name"
                                    , "plgID" => "plgID"
                                    , "pluginObject" => "pluginObject"
                                    , "customBuilderPlugin" => "customBuilderPlugin"
                                ]
                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelListDelete")
                                    , "qryAttributes" => [
                                    "tableName" => "name"
                                    , "plgID" => "plgID"
                                    , "pluginObject" => "pluginObject"
                                ]
                                    , "action" => "remove"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "deleteTableWithCustomFields"]
                                ]
                            ]
                    ]
                ]
            ]
        );
        return $OBJTableList->get();

    }

    protected function getTableListTableField($ArrayVariable)
    {

        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $customBuilderPlugin = $this->K_COMMON->getVarArray($ArrayVariable, "customBuilderPlugin");
        $tableName = $this->K_COMMON->getVarArray($ArrayVariable, "tableName");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");
        //$tableName = $plgID . "_" . $tableName;

        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        //load tables
        if ($onlyRow == "1") {
            $count = 0;
            $dataTable = [];
            $fieldName = "";
            //Get All Plugin Event
            $this->kxdb->loadTableDefinition($tableName);
            while ($data = $this->kxdb->fetch()) {
                $fieldNamePre = $fieldName;
                $fieldName = $data->Field;
                $fieldType = $data->Type;
                $fieldNull = $data->Null;
                $fieldKey = $data->Key;
                $fieldDefault = $data->Default;
                $fieldComment = $data->Comment;
                $fieldExtra = $data->Extra;


                if ($fieldNull == "YES")
                    $fieldNull = 1;
                else
                    $fieldNull = 0;

                $dataTable[$count]["fieldName"] = $fieldName;
                $dataTable[$count]["fieldNamePre"] = $fieldNamePre;
                $dataTable[$count]["fieldType"] = $fieldType;
                $dataTable[$count]["fieldNull"] = $fieldNull;
                $dataTable[$count]["fieldKey"] = $fieldKey;
                $dataTable[$count]["fieldDefault"] = $fieldDefault;
                $dataTable[$count]["fieldComment"] = $fieldComment;
                $dataTable[$count]["fieldExtra"] = $fieldExtra;
                $dataTable[$count]["tableName"] = $tableName;
                $dataTable[$count]["plgID"] = $plgID;
                $dataTable[$count]["pluginObject"] = $pluginObject;
                $dataTable[$count]["customBuilderPlugin"] = $customBuilderPlugin;

                if ($customBuilderPlugin == "") {
                    if (strpos($fieldName, "zkx_") === false)
                        $dataTable[$count]["editableField"] = 1;
                    else
                        $dataTable[$count]["editableField"] = 0;
                } else
                    if (strpos($fieldName, $customBuilderPlugin) !== false)
                        $dataTable[$count]["editableField"] = "1";
                    else
                        $dataTable[$count]["editableField"] = 0;
                $count++;
            }

        } else
            $dataTable = "";

        $OBJTableList->newTable(
            [
                "id" => "tableListTableFields"
                , "hideHead" => $onlyRow
                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getTableListTableField"]
                , "data" => $dataTable
                , "fVar" => ["plgID" => $plgID, "pluginObject" => $pluginObject, "customBuilderPlugin" => $customBuilderPlugin]
                , "hiddenFields" => ["tableName" => $tableName, "plgID" => $plgID, "pluginObject" => $pluginObject, "customBuilderPlugin" => $customBuilderPlugin]
                , "fields" =>
                [
                    [
                        "label" => $this->getDictionaryWord("LabelListName")
                        , "qryField" => "fieldName"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListType")
                        , "qryField" => "fieldType"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListKey")
                        , "qryField" => "fieldKey"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListDefault")
                        , "qryField" => "fieldDefault"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListNull")
                        , "type" => "state"
                        , "showIF" => ["fieldNull" => "1"]
                        , "qryField" => "fieldNull"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListComment")
                        , "qryField" => "fieldComment"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->getDictionaryWord("LabelListEdit")
                                    , "action" => "editModal"
                                    , "showIF" => ["editableField" => "1"]
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAddTableField"]
                                    , "qryAttributes" => [
                                    "tableName" => "tableName"
                                    , "fieldName" => "fieldName"
                                    , "plgID" => "plgID"
                                    , "pluginObject" => "pluginObject"
                                    , "customBuilderPlugin" => "customBuilderPlugin"

                                ]
                                    , "staticAttributes" => ["edit" => "1"]
                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelListMoveFieldUP")
                                    , "action" => "modalConfirm"
                                    , "showIF" => ["editableField" => "1"]
                                    , "icon" => "fa-arrow-up"
                                    , "qryAttributes" => [
                                    "tableName" => "tableName"
                                    , "fieldName" => "fieldName"
                                    , "fieldNamePre" => "fieldNamePre"
                                    , "plgID" => "plgID"
                                    , "pluginObject" => "pluginObject"
                                    , "customBuilderPlugin" => "customBuilderPlugin"
                                ]
                                    , "ajax" => ["kcp" => $this->getPluginName()
                                    , "kco" => $this->getPluginObjectName()
                                    , "kca" => "moveFieldUP"
                                    , "triggerFunction" => "kx_tablelist_script_tableListTableFields.loadTableList"
                                    , "triggerFunctionParam" => ["extraParam" => [
                                        "tableName" => ["qryAttribute" => "tableName"]
                                        , "plgID" => ["qryAttribute" => "plgID"]
                                        , "pluginObject" => ["qryAttribute" => "pluginObject"]
                                        , "customBuilderPlugin" => ["qryAttribute" => "customBuilderPlugin"]
                                    ]
                                    ]
                                ]
                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelListDelete")
                                    , "qryAttributes" => [
                                    "tableName" => "tableName"
                                    , "fieldName" => "fieldName"
                                    , "plgID" => "plgID"
                                    , "pluginObject" => "pluginObject"
                                ]
                                    , "action" => "remove"
                                    , "showIF" => ["editableField" => "1"]
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "deleteTableField"]
                                ]

                            ]
                    ]
                ]
            ]
        );


        return $OBJTableList->get();

    }

    protected function getTableListTableForeignKeys($ArrayVariable)
    {

        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $customBuilderPlugin = $this->K_COMMON->getVarArray($ArrayVariable, "customBuilderPlugin");
        $tableName = $this->K_COMMON->getVarArray($ArrayVariable, "tableName");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");
        //$tableName = $plgID . "_" . $tableName;

        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        //load tables
        if ($onlyRow == "1") {
            $count = 0;
            $dataTable = "";
            //Get All Plugin Event
            $this->kxdb->loadForeignKeyDefinition($tableName);
            $keyNameOld = "";
            while ($data = $this->kxdb->fetch()) {

                $keyName = $data->CONSTRAINT_NAME;
                //$keyName = str_replace($tableName . "_", "", $keyName);
                $fieldName = $data->COLUMN_NAME;
                $tableNameRef = $data->REFERENCED_TABLE_NAME;
                $fieldNameRef = $data->REFERENCED_COLUMN_NAME;

                $dataTable[$count]["keyName"] = $keyName;
                $dataTable[$count]["fieldName"] = $fieldName;
                $dataTable[$count]["fieldNameRef"] = $fieldNameRef;
                $dataTable[$count]["tableNameRef"] = $tableNameRef;
                $dataTable[$count]["tableName"] = $tableName;
                $dataTable[$count]["customBuilderPlugin"] = $customBuilderPlugin;
                if ($customBuilderPlugin == "") {
                    if (strpos($keyName, "zkx_") === false)
                        $dataTable[$count]["editableField"] = 1;
                    else
                        $dataTable[$count]["editableField"] = 0;
                } else
                    if (strpos($keyName, $customBuilderPlugin) !== false)
                        $dataTable[$count]["editableField"] = "1";
                    else
                        $dataTable[$count]["editableField"] = 0;
                if ($keyName == $keyNameOld) {
                    unset($dataTable[$count]);
                    $count--;
                    $dataTable[$count]["fieldName"] .= ", " . $fieldName;
                    $dataTable[$count]["fieldNameRef"] .= ", " . $fieldNameRef;

                } else {
                    $dataTable[$count]["fieldName"] = $fieldName;
                    $dataTable[$count]["fieldNameRef"] = $fieldNameRef;
                }
                $count++;
                $keyNameOld = $keyName;

            }

        } else
            $dataTable = "";
        $OBJTableList->newTable(
            [
                "id" => "tableListTableForeignKeys"
                , "hideHead" => $onlyRow
                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getTableListTableForeignKeys"]
                , "data" => $dataTable
                , "fVar" => ["plgID" => $plgID, "pluginObject" => $pluginObject, "customBuilderPlugin" => $customBuilderPlugin]
                , "hiddenFields" => ["tableName" => $tableName, "plgID" => $plgID, "pluginObject" => $pluginObject, "customBuilderPlugin" => $customBuilderPlugin]
                , "fields" =>
                [
                    [
                        "label" => $this->getDictionaryWord("LabelListFKeyName")
                        , "qryField" => "keyName"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListFKeyFieldName")
                        , "qryField" => "fieldName"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListFKeyTableNameRef")
                        , "qryField" => "tableNameRef"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListFKeyFieldNameRef")
                        , "qryField" => "fieldNameRef"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->getDictionaryWord("LabelListEdit")
                                    , "action" => "editModal"
                                    , "showIF" => ["editableField" => "1"]
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAddTableForeignKeys"]
                                    , "qryAttributes" => [
                                    "tableName" => "tableName"
                                    , "foreignKeyName" => "keyName"
                                    , "plgID" => "plgID"
                                    , "pluginObject" => "pluginObject"
                                    , "customBuilderPlugin" => "customBuilderPlugin"

                                ]
                                    , "staticAttributes" => ["edit" => "1"]
                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelListDelete")
                                    , "qryAttributes" => [
                                    "tableName" => "tableName"
                                    , "foreignKeyName" => "keyName"
                                    , "plgID" => "plgID"
                                    , "pluginObject" => "pluginObject"
                                ]
                                    , "action" => "remove"
                                    , "showIF" => ["editableField" => "1"]
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "deleteTableForeignKeys"]
                                ]
                            ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();

    }

    protected function getTableListTableIndexesKey($ArrayVariable)
    {

        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $customBuilderPlugin = $this->K_COMMON->getVarArray($ArrayVariable, "customBuilderPlugin");
        $tableName = $this->K_COMMON->getVarArray($ArrayVariable, "tableName");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");
        //$tableName = $plgID . "_" . $tableName;

        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        //load tables
        if ($onlyRow == "1") {
            $count = 0;
            $dataTable = [];
            $fieldName = "";
            //Get All Plugin Event
            $this->kxdb->loadIndexDefinition($tableName);
            $keyNameOld = "";
            while ($data = $this->kxdb->fetch()) {

                $keyName = $data->Key_name;
                //$keyName = str_replace($tableName . "_", "", $keyName);
                $unique = ($data->Non_unique == "1" ? 0 : 1);
                $fieldName = $data->Column_name;
                $dataTable[$count]["tableName"] = $tableName;
                $dataTable[$count]["keyName"] = $keyName;
                $dataTable[$count]["unique"] = $unique;
                $dataTable[$count]["customBuilderPlugin"] = $customBuilderPlugin;
                if ($customBuilderPlugin == "") {
                    if (strpos($keyName, "zkx_") === false)
                        $dataTable[$count]["editableField"] = 1;
                    else
                        $dataTable[$count]["editableField"] = 0;
                } else
                    if (strpos($keyName, $customBuilderPlugin) !== false)
                        $dataTable[$count]["editableField"] = "1";
                    else
                        $dataTable[$count]["editableField"] = 0;
                if ($keyName == $keyNameOld) {
                    unset($dataTable[$count]);
                    $count--;
                    $dataTable[$count]["fieldName"] .= ", " . $fieldName;
                } else {
                    $dataTable[$count]["fieldName"] = $fieldName;
                }
                $count++;
                $keyNameOld = $keyName;
            }
        } else
            $dataTable = "";
        $OBJTableList->newTable(
            [
                "id" => "tableListTableIndexesKey"
                , "hideHead" => $onlyRow
                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getTableListTableIndexesKey"]
                , "data" => $dataTable
                , "fVar" => ["plgID" => $plgID, "pluginObject" => $pluginObject, "customBuilderPlugin" => $customBuilderPlugin]
                , "hiddenFields" => ["tableName" => $tableName, "plgID" => $plgID, "pluginObject" => $pluginObject, "customBuilderPlugin" => $customBuilderPlugin]
                , "fields" =>
                [
                    [
                        "label" => "keyName"//$this->getDictionaryWord("LabelListName")
                        , "qryField" => "keyName"
                    ]
                    ,
                    [
                        "label" => "fieldName"//$this->getDictionaryWord("LabelListKey")
                        , "qryField" => "fieldName"
                    ]
                    ,
                    [
                        "label" => "unique"//$this->getDictionaryWord("LabelListType")
                        , "type" => "state"
                        , "showIF" => ["unique" => "1"]
                        , "qryField" => "unique"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->getDictionaryWord("LabelListEdit")
                                    , "showIF" => ["editableField" => "1"]
                                    , "hideIF" => ["keyName" => "PRIMARY"]
                                    , "action" => "editModal"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAddTableIndexesKey"]
                                    , "qryAttributes" => [
                                    "tableName" => "tableName"
                                    , "indexKeyName" => "keyName"
                                    , "plgID" => "plgID"
                                    , "pluginObject" => "pluginObject"
                                    , "customBuilderPlugin" => "customBuilderPlugin"
                                ]
                                    , "staticAttributes" => ["edit" => "1"]
                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelListDelete")
                                    , "showIF" => ["editableField" => "1"]
                                    , "hideIF" => ["keyName" => "PRIMARY"]
                                    , "qryAttributes" => [
                                    "tableName" => "tableName"
                                    , "indexKeyName" => "keyName"
                                    , "plgID" => "plgID"
                                    , "pluginObject" => "pluginObject"
                                ]
                                    , "action" => "remove"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "deleteTableIndexesKey"]
                                ]

                            ]
                    ]
                ]
            ]
        );


        return $OBJTableList->get();

    }

    protected function getModalAddTable($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->getDictionaryWord("LabelModalTitleAddTable"), "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePutTable($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];

    }

    protected function getModalAddTableWithCustomFields($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->getDictionaryWord("LabelModalTitleAddTable"), "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePutTableWithCustomFields($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];

    }

    protected function getModalAddTableField($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->getDictionaryWord("LabelModalTitleAddField"), "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePutTableField($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getModalAddTableForeignKeys($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->getDictionaryWord("LabelModalTitleAddForeignKeys"), "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePutTableForeignKeys($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getModalAddTableIndexesKey($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->getDictionaryWord("LabelModalTitleAddIndexesKey"), "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePutTableIndexesKey($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getPagePutTable($ArrayVariable)
    {

        //set variable
        $name = $this->K_COMMON->getVarArray($ArrayVariable, "tableName");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $edit = $this->K_COMMON->getVarArray($ArrayVariable, "edit");
        $value_name = "";
        $fieldDisable = "";

        //create form
        $this->appendForm("",
            [
                "id" => "tableForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveDataTable"
                , "reloadTableList" => "tableListTables"
                , "closeModal" => "modalAddTable"
            ]
        );

        if ($edit != "") {
            $fieldDisable = "1";

            $tablePluginData = $this->K_COMMON->getListPluginDBTables($name);
            $data = $tablePluginData->fetch();

            if ($data) {
                $value_name = $name;
                $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "tableName", "value" => $value_name]);
                $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "edit", "name" => "edit", "value" => $edit]);

            }
        }
        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);
        //object
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginObject", "name" => "pluginObject", "value" => $pluginObject]);

        //NEW ROW
        $this->appendRow($this->lastForm);
        $label = $this->getDictionaryWord("LabelFormName");
        $name = "name";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_name, "disabled" => $fieldDisable]);


        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->getDictionaryWord("LabelFormSave");
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;

    }

    protected function getPagePutTableWithCustomFields($ArrayVariable)
    {

        //set variable
        $name = $this->K_COMMON->getVarArray($ArrayVariable, "tableName");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $edit = $this->K_COMMON->getVarArray($ArrayVariable, "edit");
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");
        $value_name = "";
        $fieldDisable = "";

        //create form
        $this->appendForm("",
            [
                "id" => "tableWithCustomFieldsForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "openModalCustomFields"
                , "reloadTableList" => "tableListTablesWithCustomFields"
                , "closeModal" => "modalAddTableWithCustomFields"
                , "openModal" => [
                "idModal" => "modalAddTableField"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "getModalAddCustomField"
            ]
            ]
        );

        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);
        //object
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginObject", "name" => "pluginObject", "value" => $pluginObject]);

        //NEW ROW
        $this->appendRow($this->lastForm);
        $label = "Plugin";
        $name = "pluginName";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_comboplugin", ["labelInput" => $label, "withKCore" => "1", "id" => $name, "name" => $name]);

        $label = $this->getDictionaryWord("LabelFormTblWithCstmField");
        $name = "tableName";
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_comboplugindata",
            [
                "labelInput" => $label,
                "id" => $name
                , "name" => $name
                , "value" => ""
                , "loadTriggerRef" => "#" . $idModal . " #pluginName"
            ]
        );

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->getDictionaryWord("LabelFormNext");
        $name = "btnNext";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;

    }

    protected function getPagePutTableField($ArrayVariable)
    {

        //set variable
        $fieldName = $this->K_COMMON->getVarArray($ArrayVariable, "fieldName");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $tableName = $this->K_COMMON->getVarArray($ArrayVariable, "tableName");
        $customBuilderPlugin = $this->K_COMMON->getVarArray($ArrayVariable, "customBuilderPlugin");
        $edit = $this->K_COMMON->getVarArray($ArrayVariable, "edit");
        $value_fieldName = "";
        $value_fieldKey = "";
        $value_fieldNull = "1";
        $value_fieldType = "";
        $value_fieldLen = "";
        $value_fieldDefault = "";
        $value_fieldComment = "";
        $value_fieldAutoIncrement = "";
        //create form
        $this->appendForm("",
            [
                "id" => "tableFieldsForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveDataTableField"
                , "reloadTableList" => ["tableListTableFields", "tableListTablesWithCustomFields"]
                , "reloadTableListParam" => ["extraParam" => ["tableName" => $tableName, "plgID" => $plgID, "customBuilderPlugin" => $pluginObject, "customBuilderPlugin" => $customBuilderPlugin]]
                , "closeModal" => "modalAddTableField"
            ]
        );

        if ($customBuilderPlugin != "")
            $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "customBuilderPlugin", "name" => "customBuilderPlugin", "value" => $customBuilderPlugin]);


        if ($edit != "") {
            //load tables field data
            $this->kxdb->loadTableDefinition($tableName, $fieldName);
            $data = $this->kxdb->fetch();
            if ($data) {

                $typeOri = $data->Type;
                $type = $typeOri;
                $len = "";
                $null = $data->Null;
                //check variable
                if (strpos($typeOri, "(") > 0) {
                    $type = substr($typeOri, 0, strpos($typeOri, "("));
                    $len = substr($typeOri, strpos($typeOri, "("));
                    $len = str_replace(")", "", $len);
                    $len = str_replace("(", "", $len);
                }
                if ($null == "YES")
                    $value_fieldNull = "1";
                else
                    $value_fieldNull = "";


                $value_fieldName = $data->Field;
                $value_fieldNameOld = $value_fieldName;
                if ($customBuilderPlugin != "")//replace zkx_customerBuilderPlugin for edit name of customized table
                    $value_fieldName = str_replace("zkx_" . $customBuilderPlugin . "_", "", $value_fieldName);
                $value_fieldKey = ($data->Key == "PRI" ? "1" : "");
                $value_fieldType = $type;
                $value_fieldLen = $len;
                $value_fieldDefault = $data->Default;
                $value_fieldComment = $data->Comment;
                $fieldExtra = $data->Extra;
                if ($fieldExtra != "")
                    $value_fieldAutoIncrement = (strpos("auto_increment", $data->Extra) === false ? "" : "1");
                $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "fieldNameOld", "value" => $value_fieldNameOld]);
                $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "edit", "name" => "edit", "value" => $edit]);
            }
        }
        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);
        //object
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginObject", "name" => "pluginObject", "value" => $pluginObject]);

        //table
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "tableName", "name" => "tableName", "value" => $tableName]);

        //NEW ROW
        $this->appendRow($this->lastForm);
        $label = $this->getDictionaryWord("LabelFormFieldName");
        $name = "fieldName";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_fieldName]);

        //NEW ROW
        $this->appendRow($this->lastForm);
        $label = $this->getDictionaryWord("LabelFormFieldType");
        $name = "fieldType";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_fieldType]);

        //NEW ROW
        $this->appendRow($this->lastForm);
        $label = $this->getDictionaryWord("LabelFormFieldLen");
        $name = "fieldLen";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_fieldLen]);

        //NEW ROW
        $this->appendRow($this->lastForm);
        $label = $this->getDictionaryWord("LabelFormFieldDefault");
        $name = "fieldDefault";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_fieldDefault]);

        //NEW ROW
        $this->appendRow($this->lastForm);
        $label = $this->getDictionaryWord("LabelFormFieldComment");
        $name = "fieldComment";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_fieldComment]);

        //NEW ROW
        $this->appendRow($this->lastForm);
        $label = $this->getDictionaryWord("LabelFormFieldKey");
        $name = "fieldKey";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value_fieldKey]);

        //NEW ROW
        $this->appendRow($this->lastForm);
        $label = $this->getDictionaryWord("LabelFormFieldAIncr");
        $name = "fieldAutoIncrement";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value_fieldAutoIncrement]);

        //NEW ROW
        $this->appendRow($this->lastForm);
        $label = $this->getDictionaryWord("LabelFormFieldNull");
        $name = "fieldNull";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value_fieldNull]);


        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->getDictionaryWord("LabelFormSave");
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;


    }

    protected function getPagePutTableForeignKeys($ArrayVariable)
    {
        //include object
        $objApiUsefulData = $this->K_COMMON->getPluginObject("k_usefuldata", "api");

        //set variable
        $foreignKeyName = $this->K_COMMON->getVarArray($ArrayVariable, "foreignKeyName");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $tableName = $this->K_COMMON->getVarArray($ArrayVariable, "tableName");
        $customBuilderPlugin = $this->K_COMMON->getVarArray($ArrayVariable, "customBuilderPlugin");
        $edit = $this->K_COMMON->getVarArray($ArrayVariable, "edit");
        $value_foreignKeyName = "";
        $value_foreignKeyNameOld = "";
        $value_foreignKeyTable = $tableName;
        $value_foreignKeyFields = "";
        $value_foreignKeyTableRef = "";
        $value_foreignKeyField = "";
        $value_foreignKeyPlgID = "";
        $value_onUpdate = "";
        $value_onDelete = "";

        //create form
        $this->appendForm("",
            [
                "id" => "tableForeignKeyForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveDataTableForeignKeys"
                , "reloadTableList" => "tableListTableForeignKeys"
                , "reloadTableListParam" => ["extraParam" => ["tableName" => $tableName, "plgID" => $plgID, "customBuilderPlugin" => $pluginObject, "customBuilderPlugin" => $customBuilderPlugin]]
                , "closeModal" => "modalAddTableForeignKeys"
            ]
        );

        if ($customBuilderPlugin != "")
            $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "customBuilderPlugin", "name" => "customBuilderPlugin", "value" => $customBuilderPlugin]);

        if ($edit != "") {
            //load tables field data
            $this->kxdb->loadForeignKeyDefinition($tableName, $foreignKeyName);
            while ($data = $this->kxdb->fetch()) {

                $foreignKeyName = $data->CONSTRAINT_NAME;
                $value_foreignKeyNameOld = $foreignKeyName;
                $fieldName = $data->COLUMN_NAME;
                $tableNameRef = $data->REFERENCED_TABLE_NAME;
                $fieldNameRef = $data->REFERENCED_COLUMN_NAME;
                $onUpdate = $data->UPDATE_RULE;
                $onDelete = $data->DELETE_RULE;
                //get plgID ref by table ref name
                $plgIDRefArray = explode("_", $tableNameRef);
                $plgIDRef = $this->K_COMMON->getVarArray($plgIDRefArray, 0) . "_" . $this->K_COMMON->getVarArray($plgIDRefArray, 1);

                $value_foreignKeyName = str_replace($tableName . "_", "", $foreignKeyName);
                if ($customBuilderPlugin != "")//replace zkx_customerBuilderPlugin for edit name of customized table
                    $value_foreignKeyName = str_replace("zkx_" . $customBuilderPlugin . "_", "", $value_foreignKeyName);

                $value_foreignKeyTable = $tableName;
                $value_foreignKeyFields [] = ["fieldName" => $fieldName, "fieldRef" => $fieldNameRef];

                $value_foreignKeyTableRef = $tableNameRef;
                $value_foreignKeyField = $fieldNameRef;
                $value_foreignKeyPlgID = $plgIDRef;
                $value_onUpdate = $onUpdate;
                $value_onDelete = $onDelete;
            }
            $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "foreignKeyNameOld", "name" => "foreignKeyNameOld", "value" => $value_foreignKeyNameOld]);
            $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "edit", "name" => "edit", "value" => $edit]);

        }
        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);
        //object
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginObject", "name" => "pluginObject", "value" => $pluginObject]);

        //table
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "tableName", "name" => "tableName", "value" => $tableName]);

        //NEW ROW
        $this->appendRow($this->lastForm);
        $label = $this->getDictionaryWord("LabelFormForeignKeyName");
        $name = "foreignKeyName";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_foreignKeyName, "required" => "1"]);

        //load field
        $this->kxdb->loadTableDefinition($tableName);
        $optionsField = [];
        while ($data = $this->kxdb->fetch()) {
            $fieldName = $data->Field;
            $optionsField[] = ["text" => $fieldName, "value" => $fieldName];
        }

        //load plugin ref
        $optionsPluginRef = [];
        $listPlugin = $objApiUsefulData->listPluginsAndObjects(["withKCore" => "1"]);
        foreach ($listPlugin as $plugin) {
            $pluginRefName = $plugin["value"];
            $optionsPluginRef[] = ["text" => $pluginRefName, "value" => $pluginRefName];
        }

        //NEW ROW
        $this->appendRow($this->lastForm);
        $label = $this->getDictionaryWord("LabelFormFKeyPluginRef");
        $name = "pluginRef";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $optionsPluginRef, "labelInput" => $label, "value" => $value_foreignKeyPlgID, "name" => $name, "id" => $name]);

        //NEW ROW
        $this->appendRow($this->lastForm);
        $label = $this->getDictionaryWord("LabelFormFKeyTableRef");
        $name = "tableRef";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "select"
            , "typeLoad" => "ajax"
            , "pluginName" => $this->namePlugin
            , "pluginObject" => $this->namePluginObject
            , "pluginAction" => "getTableOptions"
            , "parameterRef" => "plgID"
            , "loadTrigger" => "change"
            , "loadTriggerRef" => "#pluginRef"
            , "value" => $value_foreignKeyTableRef
            , "firstValueCall" => $value_foreignKeyPlgID
            , "labelInput" => $label, "name" => $name, "id" => $name]);

        //NEW ROW
        $this->appendRow($this->lastForm);
        $label = $this->getDictionaryWord("LabelFormForeignKeyOnDel");
        $name = "onDelete";
        $optionsOnDelete = ["CASCADE" => "CASCADE", "NO ACTION" => "NO ACTION", "RESTRICT" => "RESTRICT", "SET NULL" => "SET NULL"];
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $optionsOnDelete, "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_onDelete]);

        //NEW ROW
        $this->appendRow($this->lastForm);
        $label = $this->getDictionaryWord("LabelFormForeignKeyOnUp");
        $name = "onUpdate";
        $optionsOnUpdate = ["CASCADE" => "CASCADE", "NO ACTION" => "NO ACTION", "RESTRICT" => "RESTRICT", "SET NULL" => "SET NULL"];
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $optionsOnUpdate, "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_onUpdate]);

        //Heading
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->getDictionaryWord("LabelFormForeignKeyHelp"), "importance" => "5"]);


        //NEW ROW
        $this->appendRow($this->lastForm);
        $name = "foreignKeyFields";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendListGroup($this->lastCol,
            [
                "id" => $name
                , "attributes" => 'style="height:176; overflow: scroll; overflow-x: hidden;"'
                , "values" => $value_foreignKeyFields
                , "addingValues" =>
                [
                    "valuesHName" => "foreignKeyField"
                    , "fields" => [
                    ["type" => "select", "typeLoad" => "array", "options" => $optionsField, "labelInput" => $this->getDictionaryWord("LabelFormForeignKeyField"), "name" => "fieldName"]
                    , ["type" => "select"
                        , "typeLoad" => "ajax"
                        , "pluginName" => $this->namePlugin
                        , "pluginObject" => $this->namePluginObject
                        , "pluginAction" => "getFieldOptions"
                        , "parameterRef" => "tableName"
                        , "loadTrigger" => "change"
                        , "loadTriggerRef" => "#tableRef"
                        , "firstValueCall" => $value_foreignKeyTableRef
                        , "labelInput" => $this->getDictionaryWord("LabelFormFKeyFieldRef")
                        , "name" => "fieldRef"
                    ]
                ]
                ]
            ]
        );

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->getDictionaryWord("LabelFormSave");
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);
        return $this;
    }

    protected function getPagePutTableIndexesKey($ArrayVariable)
    {
        //set variable
        $indexKeyName = $this->K_COMMON->getVarArray($ArrayVariable, "indexKeyName");
        $indexKeyNameOld = $this->K_COMMON->getVarArray($ArrayVariable, "indexKeyNameOld");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $tableName = $this->K_COMMON->getVarArray($ArrayVariable, "tableName");
        $customBuilderPlugin = $this->K_COMMON->getVarArray($ArrayVariable, "customBuilderPlugin");
        $edit = $this->K_COMMON->getVarArray($ArrayVariable, "edit");
        $value_indexKeyName = "";
        $value_indexKeyType = "NORMAL";
        $value_indexKeyFields = "";
        $value_indexKeyMethod = "BTREE";
        $value_nonUnique = "";
        $disabled = "";
        //create form
        $this->appendForm("",
            [
                "id" => "tableIndexesKeyForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveDataTableIndexesKey"
                , "reloadTableList" => "tableListTableIndexesKey"
                , "reloadTableListParam" => ["extraParam" => ["tableName" => $tableName, "plgID" => $plgID, "customBuilderPlugin" => $pluginObject, "customBuilderPlugin" => $customBuilderPlugin]]
                , "closeModal" => "modalAddTableIndexesKey"
            ]
        );

        if ($customBuilderPlugin != "")
            $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "customBuilderPlugin", "name" => "customBuilderPlugin", "value" => $customBuilderPlugin]);

        if ($edit != "") {
            //load tables field data
            $this->kxdb->loadIndexDefinition($tableName, $indexKeyName);
            while ($data = $this->kxdb->fetch()) {

                $keyName = $data->Key_name;
                $nonUnique = $data->Non_unique;
                $fieldName = $data->Column_name;
                $keyType = $data->Index_type;
                $value_indexKeyName = $keyName;
                $indexKeyNameOld = $keyName;
                $value_indexKeyName = str_replace($tableName . "_", "", $value_indexKeyName);
                if ($customBuilderPlugin != "")//replace zkx_customerBuilderPlugin for edit name of customized table
                    $value_indexKeyName = str_replace("zkx_" . $customBuilderPlugin . "_", "", $value_indexKeyName);
                $value_indexKeyType = $keyType;
                $value_indexKeyFields [] = ["fieldName" => $fieldName];
                $value_nonUnique = $nonUnique;
            }
            $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "indexKeyNameOld", "name" => "indexKeyNameOld", "value" => $indexKeyNameOld]);
            $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "edit", "name" => "edit", "value" => $edit]);
        }
        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);
        //object
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginObject", "name" => "pluginObject", "value" => $pluginObject]);
        //table
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "tableName", "name" => "tableName", "value" => $tableName]);

        //NEW ROW
        $this->appendRow($this->lastForm);
        $label = $this->getDictionaryWord("LabelFormIndexKeyName");
        $name = "indexKeyName";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_indexKeyName, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);
        $label = $this->getDictionaryWord("LabelFormIndexKeyType");
        $name = "indexKeyType";
        $optionsType = ["BTREE" => "BTREE", "FULLTEXT" => "FULLTEXT"];
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $optionsType, "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_indexKeyType]);

        //NEW ROW
        $this->appendRow($this->lastForm);
        $label = $this->getDictionaryWord("LabelFormIndexNonUnique");
        $name = "nonUnique";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value_nonUnique]);


        //Heading
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->getDictionaryWord("LabelFormIndexHelp"), "importance" => "5"]);


        //NEW ROW
        $this->appendRow($this->lastForm);
        $name = "indexKeyFields";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);

        //load field
        $this->kxdb->loadTableDefinition($tableName);
        $optionsField = [];
        while ($data = $this->kxdb->fetch()) {
            $optionsField[] = ["text" => $data->Field, "value" => $data->Field];
        }
        $this->appendListGroup($this->lastCol,
            [
                "id" => $name
                , "attributes" => 'style="height:176; overflow: scroll; overflow-x: hidden;"'
                , "values" => $value_indexKeyFields
                , "addingValues" =>
                [
                    "valuesHName" => $name
                    , "fields" => [
                    ["type" => "select", "typeLoad" => "array", "options" => $optionsField, "labelInput" => $this->getDictionaryWord("LabelFormIndexKeyFields"), "name" => "fieldName"]
                ]
                ]
            ]
        );


        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->getDictionaryWord("LabelFormSave");
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;


    }

    protected function deleteTable($ArrayVariable)
    {
        $name = $this->K_COMMON->getVarArray($ArrayVariable, "tableName");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");

        //drop actual Event
        $query = " DROP TABLE IF EXISTS  " . $name . ";";
        $this->kxdb->prepare($query);
        $this->kxdb->execute();

        return array("success" => "true");
    }

    protected function deleteTableWithCustomFields($ArrayVariable)
    {
        $name = $this->K_COMMON->getVarArray($ArrayVariable, "tableName");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");

        //drop actual Event
        $query = " DROP TABLE IF EXISTS  " . $name . ";";
        $this->kxdb->prepare($query);
        $this->kxdb->execute();

        return array("success" => "true");
    }

    protected function deleteTableField($ArrayVariable)
    {
        $tableName = $this->K_COMMON->getVarArray($ArrayVariable, "tableName");
        $fieldName = $this->K_COMMON->getVarArray($ArrayVariable, "fieldName");

        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");

        //drop actual Event
        $query = " ALTER TABLE " . $tableName . " DROP COLUMN " . $fieldName . ";";
        $this->kxdb->prepare($query);
        $this->kxdb->execute();

        return array("success" => "true");
    }

    protected function deleteTableForeignKeys($ArrayVariable)
    {
        $tableName = $this->K_COMMON->getVarArray($ArrayVariable, "tableName");
        $foreignKeyName = $this->K_COMMON->getVarArray($ArrayVariable, "foreignKeyName");

        $this->kxdb->removeTableFKey($tableName, $foreignKeyName);

        return array("success" => "true");
    }

    protected function deleteTableIndexesKey($ArrayVariable)
    {
        $tableName = $this->K_COMMON->getVarArray($ArrayVariable, "tableName");
        $indexKeyName = $this->K_COMMON->getVarArray($ArrayVariable, "indexKeyName");

        $this->kxdb->removeTableIndex($tableName, $indexKeyName);

        return array("success" => "true");
    }

    protected function saveDataTable($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $name = $this->K_COMMON->getVarArray($ArrayVariable, "name");
        $edit = $this->K_COMMON->getVarArray($ArrayVariable, "edit");
        $errorMsg = "";
        $tableName = $name;
        //check variable
        if ($edit == "") {
            if ($pluginObject != "")
                $tableName = $pluginObject . "_" . $name;
            $tableName = $plgID . "_" . $name;
        }


        //$query = " DROP EVENT IF EXISTS  " . $name . ";";
        //$this->kxdb->prepare($query);
        //$this->kxdb->execute();

        $query = "CREATE TABLE " . $tableName . " (
                    id_" . $name . " INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
                    creationdate datetime,
                    createdby VarChar(10),
                    changedate datetime,
                    changedby VarChar(10)
                    )";

        //$this->kxdb->showSQL();
        $this->kxdb->setTraceError();
        $this->kxdb->prepare($query);
        $this->kxdb->execute();

        $traceLog = $this->kxdb->getTraceLog();
        if (count($traceLog) > 0) {
            foreach ($traceLog as $err) {
                $errorMsg .= $err["errorStr"] . chr(10);
            }
            return ["success" => "false", "reason" => "error", "message" => $errorMsg];
        }
        return ["success" => "true"];
    }

    protected function openModalCustomFields($ArrayVariable)
    {

        $customBuilderPlugin = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "pluginName");
        $tableName = $this->K_COMMON->getVarArray($ArrayVariable, "tableName");
        $tableName = $this->K_COMMON->getTableNameByObject($plgID, $tableName);

        return ["success" => "true", "plgID" => $plgID, "tableName" => $tableName, "customBuilderPlugin" => $customBuilderPlugin];
    }

    protected function saveDataTableField($ArrayVariable)
    {

        //set variable
        $customBuilderPlugin = $this->K_COMMON->getVarArray($ArrayVariable, "customBuilderPlugin");
        $tableName = $this->K_COMMON->getVarArray($ArrayVariable, "tableName");
        $fieldNameOld = $this->K_COMMON->getVarArray($ArrayVariable, "fieldNameOld");
        $fieldNamePre = $this->K_COMMON->getVarArray($ArrayVariable, "fieldNamePre");
        $fieldName = $this->K_COMMON->getVarArray($ArrayVariable, "fieldName");
        $fieldType = $this->K_COMMON->getVarArray($ArrayVariable, "fieldType");
        $fieldLen = $this->K_COMMON->getVarArray($ArrayVariable, "fieldLen");
        $fieldKey = $this->K_COMMON->getVarArray($ArrayVariable, "fieldKey");
        $fieldNull = $this->K_COMMON->getVarArray($ArrayVariable, "fieldNull");
        $fieldDefault = $this->K_COMMON->getVarArray($ArrayVariable, "fieldDefault");
        $fieldComment = $this->K_COMMON->getVarArray($ArrayVariable, "fieldComment");
        $fieldAutoIncrement = $this->K_COMMON->getVarArray($ArrayVariable, "fieldAutoIncrement");
        $errorMsg = "";

        //check variable
        if ($fieldName == "")
            return ["success" => "false", "reason" => "fieldNameNull", "message" => "Field Name Null"];
        if ($customBuilderPlugin != "")
            $fieldName = "zkx_" . $customBuilderPlugin . "_" . $fieldName;

        $fieldParam = [
            "Type" => $fieldType
            , "Len" => $fieldLen
            , "Default" => $fieldDefault
            , "Comment" => $fieldComment
            , "NULL" => $fieldNull
            , "AUTOINCREMENT" => $fieldAutoIncrement
            , "PK" => $fieldKey
        ];
        $this->kxdb->setTraceError();
        $this->kxdb->tableAddField($tableName, $fieldNameOld, $fieldName, $fieldParam, $fieldNamePre);

        $traceLog = $this->kxdb->getTraceLog();
        if (count($traceLog) > 0) {
            foreach ($traceLog as $err) {
                $errorMsg .= $err["errorStr"] . chr(10);
            }
            return ["success" => "false", "reason" => "error", "message" => $errorMsg];
        }


        return ["success" => "true"];
    }

    protected function saveDataTableForeignKeys($ArrayVariable)
    {

        //set variable
        $customBuilderPlugin = $this->K_COMMON->getVarArray($ArrayVariable, "customBuilderPlugin");
        $foreignKeyName = $this->K_COMMON->getVarArray($ArrayVariable, "foreignKeyName");
        $foreignKeyNameOld = $this->K_COMMON->getVarArray($ArrayVariable, "foreignKeyNameOld");
        $tableName = $this->K_COMMON->getVarArray($ArrayVariable, "tableName");
        $tableRef = $this->K_COMMON->getVarArray($ArrayVariable, "tableRef");
        $foreignKeyField = $this->K_COMMON->getVarArray($ArrayVariable, "foreignKeyField");
        $foreignKeyField = json_decode($foreignKeyField, true);
        $onDelete = $this->K_COMMON->getVarArray($ArrayVariable, "onDelete");
        $onUpdate = $this->K_COMMON->getVarArray($ArrayVariable, "onUpdate");
        $errorMsg = "";
        $fieldName = "";
        $fieldRef = "";

        //check variable
        if ($foreignKeyName == "")
            return ["success" => "false", "reason" => "foreignKeyNameNull", "message" => "Foreign Key Name Null"];
        if (!is_array($foreignKeyField))
            return ["success" => "false", "reason" => "foreignKeyFieldNull", "message" => "No Field On Key"];
        if ($customBuilderPlugin != "")
        {
            $prefix = "zkx_" . $customBuilderPlugin . "_";
            if (strpos($foreignKeyName, $prefix) !== 0)
                $foreignKeyName = $prefix . $foreignKeyName;
        }
        else{
            $prefix = $tableName . "_";
            if (strpos($foreignKeyName, $prefix) !== 0)
                $foreignKeyName = $prefix . $foreignKeyName;
        }

        //read field with field ref
        foreach (($foreignKeyField) as $dataForeign) {
            $fieldName .= "," . $this->K_COMMON->getVarArray($dataForeign, "fieldName");
            $fieldRef .= "," . $this->K_COMMON->getVarArray($dataForeign, "fieldRef");
        }
        if ($fieldName == "" || $fieldRef == "")
            return ["success" => "false", "reason" => "noField", "message" => "No Field On Key"];
        $fieldName = substr($fieldName, 1);
        $fieldRef = substr($fieldRef, 1);
        $keys[] = [
            'name' => $foreignKeyName,
            'table' => $tableName,
            'fields' => $fieldName,
            'refTable' => $tableRef,
            'refFields' => $fieldRef,
            'onDelete' => $onDelete,
            'onUpdate' => $onUpdate
        ];

        $this->kxdb->setTraceError();

        $this->kxdb->removeTableFKey($tableName, $foreignKeyNameOld);
        $this->kxdb->createTableForeignKey($keys, "");

        $traceLog = $this->kxdb->getTraceLog();
        if (count($traceLog) > 0) {
            foreach ($traceLog as $err) {
                $errorMsg .= $err["errorStr"] . chr(10);
            }
            return ["success" => "false", "reason" => "error", "message" => $errorMsg];
        }


        return ["success" => "true"];
    }

    protected function saveDataTableIndexesKey($ArrayVariable)
    {
        //set variables
        $tableName = $this->K_COMMON->getVarArray($ArrayVariable, "tableName");
        $customBuilderPlugin = $this->K_COMMON->getVarArray($ArrayVariable, "customBuilderPlugin");
        $indexKeyName = $this->K_COMMON->getVarArray($ArrayVariable, "indexKeyName");
        $indexKeyNameOld = $this->K_COMMON->getVarArray($ArrayVariable, "indexKeyNameOld");
        $indexKeyType = $this->K_COMMON->getVarArray($ArrayVariable, "indexKeyType");
        $indexKeyFields = $this->K_COMMON->getVarArray($ArrayVariable, "indexKeyFields");
        $indexKeyFields = json_decode($indexKeyFields, true);
        $indexKeyNonUnique = $this->K_COMMON->getVarArray($ArrayVariable, "nonUnique");
        $errorMsg = "";
        $fieldName = "";

        //check variable
        if ($indexKeyName == "")
            return ["success" => "false", "reason" => "indexNameNull", "message" => "Index Name Null"];

        if (!is_array($indexKeyFields))
            return ["success" => "false", "reason" => "indexFieldNull", "message" => "No Field On Index"];

        if ($customBuilderPlugin != "")
            $indexKeyName = "zkx_" . $customBuilderPlugin . "_" . $indexKeyName;

        //read field
        foreach ($indexKeyFields as $dataIndex) {
            $fieldName .= "," . $this->K_COMMON->getVarArray($dataIndex, "fieldName");
        }
        if ($fieldName == "")
            return ["success" => "false", "reason" => "noField", "message" => "No Field On Index"];
        $fieldName = substr($fieldName, 1);
        $keys[] = [
            'table' => $tableName,
            'name' => $indexKeyName,
            'type' => $indexKeyType,
            'columns' => $fieldName,
            'unique' => ($indexKeyNonUnique != "1" ? "1" : "0")
        ];

        $this->kxdb->setTraceError();

        $this->kxdb->removeTableIndex($tableName, $indexKeyNameOld);
        $this->kxdb->createTableIndexKey($keys, "");

        $traceLog = $this->kxdb->getTraceLog();
        if (count($traceLog) > 0) {
            foreach ($traceLog as $err) {
                $errorMsg .= $err["errorStr"] . chr(10);
            }
            return ["success" => "false", "reason" => "error", "message" => $errorMsg];
        }


        return ["success" => "true"];
    }

    protected function moveFieldUP($ArrayVariable)
    {
        //set variable
        $tableName = $this->K_COMMON->getVarArray($ArrayVariable, "tableName");
        $fieldName = $this->K_COMMON->getVarArray($ArrayVariable, "fieldName");
        $fieldNamePre = $this->K_COMMON->getVarArray($ArrayVariable, "fieldNamePre");

        $this->kxdb->loadTableDefinition($tableName, $fieldNamePre);
        $data = $this->kxdb->fetch();
        if ($data) {
            $typeOri = $data->Type;
            $type = $typeOri;
            $len = "";
            $null = $data->Null;
            //check variable
            if (strpos($typeOri, "(") > 0) {
                $type = substr($typeOri, 0, strpos($typeOri, "("));
                $len = substr($typeOri, strpos($typeOri, "("));
                $len = str_replace(")", "", $len);
                $len = str_replace("(", "", $len);
            }
            if ($null == "YES")
                $value_fieldNull = "1";
            else
                $value_fieldNull = "";


            $value_fieldName = $fieldName;
            $value_fieldKey = ($data->Key == "PRI" ? $data->Key : "");
            $value_fieldType = $type;
            $value_fieldLen = $len;
            $value_fieldDefault = $data->Default;
            $value_fieldComment = $data->Comment;
            $fieldExtra = $data->Extra;
            $value_fieldAutoIncrement = "";


            $arrayField = [
                "tableName" => $tableName
                , "fieldNameOld" => $fieldNamePre
                , "fieldNamePre" => $value_fieldName
                , "fieldName" => $fieldNamePre
                , "fieldType" => $value_fieldType
                , "fieldLen" => $value_fieldLen
                , "fieldKey" => $value_fieldKey
                , "fieldNull" => $value_fieldNull
                , "fieldDefault" => $value_fieldDefault
                , "fieldComment" => $value_fieldComment
                , "fieldAutoIncrement" => $value_fieldAutoIncrement
            ];
            $result = $this->saveDataTableField($arrayField);
            return ($result);
        }
        return ["success" => "false"];
    }

    protected function getTableOptions($arrayVariable)
    {
        $plgID = $this->K_COMMON->getVarArray($arrayVariable, "plgID");
        $OM = $this->K_COMMON->getVarArray($arrayVariable, "OM");

        //load table ref
        $tablePluginData = $this->K_COMMON->getListPluginDBTables($plgID);
        $optionsTableRef = [];
        while ($dataList = $tablePluginData->fetch()) {
            $tableRefName = $dataList->TABLE_NAME;
            $optionsTableRef[] = ["text" => $tableRefName, "value" => $tableRefName];
        }
        $optionsXML = $this->K_COMMON->dbDataToXML(["array" => $optionsTableRef]);
        return $optionsXML;
    }

    protected function getFieldOptions($arrayVariable)
    {
        $tableName = $this->K_COMMON->getVarArray($arrayVariable, "tableName");
        $OM = $this->K_COMMON->getVarArray($arrayVariable, "OM");

        //load field ref
        $this->kxdb->loadTableDefinition($tableName);
        $optionsFieldRef = [];
        while ($data = $this->kxdb->fetch()) {
            $fieldRefName = $data->Field;
            $optionsFieldRef[] = ["text" => $fieldRefName, "value" => $fieldRefName];

        }
        $optionsXML = $this->K_COMMON->dbDataToXML(["array" => $optionsFieldRef]);
        return $optionsXML;
    }
}

?>