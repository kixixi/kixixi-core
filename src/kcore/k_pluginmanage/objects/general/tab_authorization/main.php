<?
/**
 * PLUGIN MANAGE (authorization)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_pluginmanage
 * @subpackage objects\authorization
 * @since 1.00
 */
namespace k_pluginmanage\objects\general\tab_authorization;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";


    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {

        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "saveData":
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "getModalAdd":
                return json_encode($this->getModalAdd($ArrayVariable));
                break;
            case "delete":
                return json_encode($this->delete($ArrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableList($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
        }
    }

    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "put", "get")
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {


        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
                $this->getPageList($ArrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    protected function getPageList($ArrayVariable)
    {


        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");

        $listMyMList = $this->getTableList($ArrayVariable);
        $this->appendHTML("", $listMyMList);

        $this->appendInput("", ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "btnAddAuth", "value" => $this->getDictionaryWord("LabelAdd")
            , "openModal" => [
                "idModal" => "modalAddAuth"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAdd"
                , "parameters" => ["plgID" => $plgID]
            ]
        ]);

    }

    protected function getTableList($ArrayVariable)
    {


        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");

        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        if ($onlyRow == "1") {
            //Load Table
            $table = $this->K_COMMON->getPluginDataObject("k_secauth", "authtype");
            $table->get(["id_form", "code_form", "plugin_name"],
                ["plugin_name" => $plgID]);
            $dataTable = $table->getResults("array");
        } else
            $dataTable = "";
        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableListAuths"
                , "hideHead" => $onlyRow
                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getTableList"]
                , "data" => $dataTable
                , "fVar" => ["plgID" => $plgID]

                , "fields" =>
                [
                    [
                        "label" => $this->getDictionaryWord("LabelListAuthCode")
                        , "qryField" => "code_form"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->getDictionaryWord("LabelListEdit")
                                    , "action" => "editModal"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                    , "qryAttributes" => [
                                    "keyID" => "id_form"
                                    , "plgID" => "plugin_name"

                                ]

                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelListDelete")
                                    , "qryAttributes" => [
                                    "keyID" => "id_form"
                                    , "plgID" => "plugin_name"

                                ]
                                    , "action" => "remove"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                                ]
                            ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();

    }

    protected function getModalAdd($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->getDictionaryWord("LabelModalTitleAddAuth"), "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePut($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getPagePut($ArrayVariable)
    {

        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $value_code = "";
        $value_label = "";
        $value_descr = "";
        $value_data_ref = "";
        $value_web_target = "";


        //Load Table
        $table = $this->K_COMMON->getPluginDataObject("k_secauth", "authtype");
        $table->get("*", ["plugin_name" => $plgID, "id_form" => $keyID]);
        $data = $table->fetch();
        if ($data) {
            $value_code = $data->code_form;
            $value_label = $data->code_label;
            $value_descr = $data->description;
            $value_data_ref = $data->code_table_ref;
            $value_web_target = $data->auth_by_web_target;
        }
        //create form
        $this->appendForm("",
            [
                "id" => "authForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "reloadTableList" => "tableListAuths"
                , "closeModal" => "modalAddAuth"


            ]
        );

        //keyID hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "keyID", "value" => $keyID]);

        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //auth obj code
        $label = $this->getDictionaryWord("LabelFormCode");
        $name = "authCode";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_code, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //auth name
        $label = $this->getDictionaryWord("LabelFormLabelCode");
        $name = "authLabelCode";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_combodictionary"
            , [
                "labelInput" => $label
                , "name" => $name
                , "value" => $value_label
                , "pluginName" => $plgID
            ]
        );

        //NEW ROW
        $this->appendRow($this->lastForm);

        //auth is container
        $label = $this->getDictionaryWord("LabelFormDescr");
        $name = "authDescr";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_descr]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //auth is container
        $label = $this->getDictionaryWord("LabelFormDataRef");
        $name = "authDataRef";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_data_ref]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //auth is container
        $label = $this->getDictionaryWord("LabelFormWebTarget");
        $name = "webTarget";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value_web_target]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->getDictionaryWord("LabelFormSave");
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;

    }

    protected function delete($ArrayVariable)
    {
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");

        $table = $this->K_COMMON->getPluginDataObject("k_secauth", "authtype");

        $table->delete(["id_form" => $keyID, "plugin_name" => $plgID]);

        return array("success" => "true");
    }

    protected function saveData($ArrayVariable)
    {

        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $authCode = $this->K_COMMON->getVarArray($ArrayVariable, "authCode");
        $authLabelCode = $this->K_COMMON->getVarArray($ArrayVariable, "authLabelCode");
        $authDescr = $this->K_COMMON->getVarArray($ArrayVariable, "authDescr");
        $authDataRef = $this->K_COMMON->getVarArray($ArrayVariable, "authDataRef");
        $webTarget = $this->K_COMMON->getVarArray($ArrayVariable, "webTarget");
        $arrayWhere = "";
        //include tables
        $table = $this->K_COMMON->getPluginDataObject("k_secauth", "authtype");


        if ($plgID == "" || $authCode == "")
            return ["success" => "false", "reason" => "inputVoid", "message" => "Please fill in the fields!"];

        if ($keyID != "")
            $arrayWhere = ["id_form" => $keyID];
        else {
            //check if cod dup
            $table->get(["id_form"], ["plugin_name" => $plgID,"code_form" => $authCode]);
            $data = $table->fetch();
            if ($data)
                return ["success" => "false", "reason" => "actionCodeDup", "message" => $this->getDictionaryWord("emsgAuthCodeDup")];
        }

        if ($webTarget == '')
            $webTarget = 0;

        $table->put(
            [
                "plugin_name" => $plgID
                , "code_form" => $authCode
                , "code_label" => $authLabelCode
                , "description" => $authDescr
                , "code_table_ref" => $authDataRef
                , "auth_by_web_target"=>$webTarget
            ], $arrayWhere
        );

        return ["success" => "true"];
    }


}

?>