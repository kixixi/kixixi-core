<?
/**
 * PLUGIN MANAGE (Dashboard)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_pluginmanage
 * @subpackage objects\customization
 * @since 1.00
 */
namespace k_pluginmanage\objects\general\tab_customization;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "saveData":
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "getModalAdd":
                return json_encode($this->getModalAdd($ArrayVariable));
                break;
            case "delete":
                return json_encode($this->delete($ArrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableList($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
        }
    }

    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "put", "get")
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {


        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
                $this->getPageList($ArrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    protected function getPageList($ArrayVariable)
    {


        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        //replace "\" on kco with double "\" for javascript
        $pluginObject = str_replace("\\", "\\\\", $pluginObject);
        $listMyMList = $this->getTableList($ArrayVariable);
        $this->appendHTML("", $listMyMList);
        $this->appendInput("", ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "btnAddCustomization", "value" => $this->getDictionaryWord("LabelAdd")
            , "openModal" => [
                "idModal" => "modalAddCustomization"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAdd"
                , "parameters" => ["plgID" => $plgID, "pluginObject" => $pluginObject]
            ]
        ]);

    }

    protected function getTableList($ArrayVariable)
    {

        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");

        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        if ($onlyRow == "1") {
            //Load Table
            $table = $this->K_COMMON->getPluginDataObject("k_custommanager", "customization");
            $table->get([
                    "id_custom"
                    , "cst_target_plugin_name"
                    , "cst_target_plugin_object"
                    , "cst_description"
                    , "cst_active"
                ]
                , [
                    "cst_owner_plugin_name" => $plgID
                    , "cst_owner_plugin_object" => $pluginObject
                ]);
            $dataTable = $table->getResults("array");
        } else
            $dataTable = "";
        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableListCustomization"
                , "hideHead" => $onlyRow
                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getTableList"]
                , "data" => $dataTable
                , "fVar" => ["plgID" => $plgID, "pluginObject" => $pluginObject]
                , "dictionary" => $this->getDictionary($plgID, $pluginObject)
                , "fields" =>
                [
                    [
                        "label" => $this->getDictionaryWord("LabelListTargetPlg")
                        , "qryField" => "cst_target_plugin_name"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListTargetPlgObject")
                        , "qryField" => "cst_target_plugin_object"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListDescr")
                        , "qryField" => "cst_description"
                    ]

                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListCustomActive")
                        , "qryField" => "cst_active"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->getDictionaryWord("LabelListEdit")
                                    , "action" => "editModal"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                    , "qryAttributes" => [
                                    "keyID" => "id_custom"
                                ]
                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelListDelete")
                                    , "qryAttributes" => [
                                    "keyID" => "id_custom"
                                ]
                                    , "action" => "remove"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                                ]
                            ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();

    }

    protected function getModalAdd($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->getDictionaryWord("LabelModalTitleAddCustom"), "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePut($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getPagePut($ArrayVariable)
    {

        //add files to include
        $this->addHeadFile([$this->getPathFileJS("put")]);

        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $value_cstCode = "";
        $value_sourcePluginName = "";
        $value_sourcePluginObject = "";
        $value_targetWhen = "";
        $value_targetPluginName = "";
        $value_targetPluginObject = "";
        $value_description = "";
        $value_parameters = "";
        $value_active = "";

        //Load Table
        $table = $this->K_COMMON->getPluginDataObject("k_custommanager", "customization");

        $table->get([
            'id_custom'
            , 'cst_code'
            , 'cst_owner_plugin_name'
            , 'cst_owner_plugin_object'
            , 'cst_source_plugin_name'
            , 'cst_source_plugin_object'
            , 'cst_target_when'
            , 'cst_target_plugin_name'
            , 'cst_target_plugin_object'
            , 'cst_description'
            , 'cst_parameters'
            , 'cst_active'
        ],
            ["id_custom" => $keyID]);
        $data = $table->fetch();
        if ($data) {
            $plgID = $data->cst_owner_plugin_name;
            $value_cstCode = $data->cst_code;
            $pluginObject = $data->cst_owner_plugin_object;
            $value_sourcePluginName = $data->cst_source_plugin_name;
            $value_sourcePluginObject = $data->cst_source_plugin_object;
            $value_targetWhen = $data->cst_target_when;
            $value_targetPluginName = $data->cst_target_plugin_name;
            $value_targetPluginObject = $data->cst_target_plugin_object;
            $value_parameters = $data->cst_parameters;
            $value_description = $data->cst_description;
            $value_active = $data->cst_active;
        }
        //create form
        $this->appendForm("",
            [
                "id" => "customizationForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "reloadTableList" => "tableListCustomization"
                , "closeModal" => "modalAddCustomization"

            ]
        );

        //keyID hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "keyID", "value" => $keyID]);

        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plgID", "name" => "plgID", "value" => $plgID]);

        //object
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginObject", "name" => "pluginObject", "value" => $pluginObject]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //Custom Code
        $label = $this->getDictionaryWord("LabelFormCode");
        $name = "cstCode";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_cstCode, "required" => 1]);

        //Custom Description
        $label = $this->getDictionaryWord("LabelFormDescription");
        $name = "description";
        $this->appendColumn($this->lastRow, ["nCol" => 9]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_description, "required" => 1]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //when method is called
        $label = $this->getDictionaryWord("LabelFormTargetWhen");
        $name = "targetWhen";
        $textHelp = $this->getDictionaryWord("LabelFormTargetWhenTextHelp");
        $optionsTargetWhen = ["before" => $this->getDictionaryWord("before"), "after" => $this->getDictionaryWord("after"), "replace" => $this->getDictionaryWord("replace")];
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $optionsTargetWhen, "textHelp" => $textHelp, "labelInput" => $label, "textHelp" => $textHelp, "id" => $name, "name" => $name, "value" => $value_targetWhen, "required" => 1]);


        //Custom Active
        $label = $this->getDictionaryWord("LabelFormActive");
        $name = "active";
        $this->appendColumn($this->lastRow, ["nCol" => 2]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value_active, "labelPos" => "UP"]);


        //*************** SOURCE PLUGIN *********/
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "PLUGIN THAT CUSTOMIZE", "importance" => "6", "class" => "gameDate"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        // plugin
        $label = $this->getDictionaryWord("LabelFormSourcePluginName");
        $name = "sourcePluginName";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_comboplugin", ["labelInput" => $label, "withKCore" => "1", "value" => $value_sourcePluginName, "id" => $name, "name" => $name, "required" => 1]);

        // object
        $label = $this->getDictionaryWord("LabelFormSourcePluginObject");
        $nameTriggerRef = $name;
        $name = "sourcePluginObject";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_combopluginobjects", ["labelInput" => $label, "value" => $value_sourcePluginObject, "id" => $name, "name" => $name, "loadTriggerRef" => "#" . $nameTriggerRef, "required" => 1]);

        //NEW ROW
        $this->appendRow($this->lastForm);


        //method parameters
        $label = $this->getDictionaryWord("LabelFormParameters");
        $name = "parameters";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "textarea", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_parameters]);



        //*************** TARGET PLUGIN *********/

        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "PLUGIN TO CUSTOMIZE", "importance" => "6", "class" => "gameDate"]);



        //NEW ROW
        $this->appendRow($this->lastForm);

        // plugin to customize
        $label = $this->getDictionaryWord("LabelFormTargetPluginName");
        $name = "targetPluginName";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_comboplugin", ["labelInput" => $label, "withKCore" => "1", "value" => $value_targetPluginName, "id" => $name, "name" => $name, "required" => 1]);

        // object to customize
        $label = $this->getDictionaryWord("LabelFormTargetPluginObject");
        $nameTriggerRef = $name;
        $name = "targetPluginObject";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_combopluginobjects", ["labelInput" => $label, "value" => $value_targetPluginObject, "id" => $name, "name" => $name, "loadTriggerRef" => "#" . $nameTriggerRef, "required" => 1]);




        //********** SAVE **************/

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->getDictionaryWord("LabelFormSave");
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;

    }

    protected function delete($ArrayVariable)
    {
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");

        $table = $this->K_COMMON->getPluginDataObject("k_custommanager", "customization");

        $table->delete(["id_custom" => $keyID]);

        return array("success" => "true");
    }

    protected function saveData($ArrayVariable)
    {

        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $cstCode = $this->K_COMMON->getVarArray($ArrayVariable, "cstCode");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $sourcePluginName = $this->K_COMMON->getVarArray($ArrayVariable, "sourcePluginName");
        $sourcePluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "sourcePluginObject");
        $targetWhen = $this->K_COMMON->getVarArray($ArrayVariable, "targetWhen");
        $targetPluginName = $this->K_COMMON->getVarArray($ArrayVariable, "targetPluginName");
        $targetPluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "targetPluginObject");
        $description = $this->K_COMMON->getVarArray($ArrayVariable, "description");
        $parameters = $this->K_COMMON->getVarArray($ArrayVariable, "parameters");
        $active = $this->K_COMMON->getVarArray($ArrayVariable, "active");
        $arrayWhere = "";


        //include tables
        $table = $this->K_COMMON->getPluginDataObject("k_custommanager", "customization");


        if ($plgID == "" ||
            $pluginObject == "" ||
            $sourcePluginName == "" ||
            $sourcePluginObject == "" ||
            $targetWhen == "" ||
            $targetPluginName == "" ||
            $targetPluginObject == "" ||
            $description == ""
        )
            return ["success" => "false", "reason" => "inputVoid", "message" => "Please fill in the fields!"];

        if ($keyID != "")
            $arrayWhere = ["id_custom" => $keyID];
        
        $table->put(
            [
                'cst_code' => $cstCode
                ,'cst_owner_plugin_name' => $plgID
                , 'cst_owner_plugin_object' => $pluginObject
                , 'cst_source_plugin_name' => $sourcePluginName
                , 'cst_source_plugin_object' => $sourcePluginObject
                , 'cst_target_when' => $targetWhen
                , 'cst_target_plugin_name' => $targetPluginName
                , 'cst_target_plugin_object' => $targetPluginObject
                , 'cst_description' => $description
                , 'cst_parameters' => $parameters
                , 'cst_active' => $active
            ], $arrayWhere
        );

        return ["success" => "true"];
    }

}

?>