// create namespace
KXJS.namespace("k_pluginmanage.objects.tab_customization");

// create method of plugin
(function () {
    var t = this;
    t.urlBase = "";
    t.actualDir = "";

    t.init = function () {
        $(document).ready(t.ready);
    };


    t.typeChange = function () {
        if ($("#type").val() == "menu"){
            $("#auth option").first().prop("selected", true);
            $("#auth").prop("disabled", true);
        }
        else
            $("#auth").prop("disabled", false);
    }


    t.ready = function () {
        t.typeChange();
        $("#type").change(t.typeChange);
    }

    t.init();

}).apply(KXNS.k_pluginmanage.objects.tab_customization);

