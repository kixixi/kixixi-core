<?
/**
 * PLUGIN MANAGE (Download)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_pluginmanage
 * @subpackage objects\download
 * @since 1.00
 */
namespace k_pluginmanage\objects\general\builder;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * download base dir
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $dirDownloadBase;

    /**
     * download dir
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $dirDownload;

    /**
     * plugin Name is the plugin ID
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $pluginName;

    /**
     * plugin Ref is the plugin ID that we use for download data or settings
     * For example, for a kdata, plugin Name, is the name that user give to his plugin, while "pluginRef" is the plugin from which download data
     *
     * @since 1.00
     * @access public
     * @var string
     */
    protected $pluginRef;

    /**
     * package (Ex: kplugin, kcore, ktemplate, kdata)
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $package;

    /**
     * content of package (Ex: country, language, plugindata, plugin)
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $packageContent;

    /**
     * temp dir where for build package
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $tempPackageDir = "packagebuild";

    /**
     * data plugin settings
     *
     * @since 1.00
     * @access public
     * @var array
     */
    var $pluginSettings;

    /**
     * plugin related
     *
     * @since 1.00
     * @access public
     * @var array
     */
    var $pluginRelated;

    /**
     * plugin Authors
     *
     * @since 1.00
     * @access public
     * @var array
     */
    var $pluginAuthors;

    /**
     * plugin parameters
     *
     * @since 1.00
     * @access public
     * @var array
     */
    var $pluginParameters;

    /**
     * plugin webCode from which to take data
     *
     * @since 1.00
     * @access public
     * @var array
     */
    var $pluginWebCodeTakeData;


    public function initialize($arrayVariable)
    {

        //include table
        $tablePlugins = $this->K_COMMON->getPluginDataObject("k_common", "plugins");

        //set variables
        $this->pluginName = $this->K_COMMON->getVarArray($arrayVariable, "pluginName");
        $this->pluginWebCodeTakeData = $this->K_COMMON->getVarArray($arrayVariable, "webCodeTakeData");
        $this->dirDownloadBase = $this->K_COMMON->getPathTmp() . $this->tempPackageDir;
        $this->dirDownload = $this->dirDownloadBase . "/package";


        //delete folder if exist old version
        $this->K_COMMON->emptyDir(["dir" => $this->dirDownloadBase]);
        $this->K_COMMON->rmDir(["dir" => $this->dirDownloadBase]);

        //set plugin DIR
        $dirTempPlugin = $this->dirDownload;

        //create plugin dir
        $this->K_COMMON->mkDir(["dir" => $dirTempPlugin, "mask" => 0777]);

        //Load data Plugin
        $tablePlugins->get("*", ["plg_id" => $this->pluginName]);
        $data = $tablePlugins->fetch();
        if ($data) {

            $this->package = $data->plg_type;
            $this->packageContent = $data->plg_content;

            $this->pluginSettings = $data;
            $this->pluginAuthors = json_decode($data->plg_authors, true);
            $this->pluginRelated = json_decode($data->plg_related, true);
            $this->pluginParameters = json_decode($data->plg_parameters, true);
            $pluginRef = $this->K_COMMON->getVarArray($this -> pluginParameters, "pluginref");

            if ($pluginRef != "")
                $this->pluginRef = $pluginRef;
        }
        else
            return ["success" => "false"];


        //copy ori dir
        $pluginDir = $this->K_COMMON->getPathPluginObject(true, $this->pluginName, "");

        if ($this->package == "kplugin" || $this->package == "kcore" || $this->package == "ktemplate")
            $this->K_COMMON->copyDir($pluginDir, $dirTempPlugin);

        //create Install XML
        $this->createInstallXML();

    }

    public function createZipFile($ArrayVariable)
    {

        //setVariable
        $fileName = $this->K_COMMON->getVarArray($ArrayVariable, "fileName");
        $dirToLoad = $this->K_COMMON->getPathTmp() . $this->tempPackageDir;
        $filePathZIP = $this->K_COMMON->getPathTmp();

        // Get real path for our folder
        $rootPath = realpath($dirToLoad);

        // Initialize archive object
        $zip = new \ZipArchive();
        $zip->open($filePathZIP . $fileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        // Create recursive directory iterator
        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($rootPath),
            \RecursiveIteratorIterator::LEAVES_ONLY
        );



        foreach ($files as $name => $file) {

            // Skip directories (they would be added automatically)
            if (!$file->isDir()) {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }

        // Zip archive will be created only after closing object
        $zip->close();

        return ["success" => "true", "filePath" => $filePathZIP, "fileName" => $fileName];
    }

    public function createInstallXML()
    {

        //Add Config
        $xml = new \k_common\objects\general\xml\obj('<install/>');
        $xml->addChild("package", $this->package);
        $xml->addChild("content", $this->packageContent);
        $xml->addChild("pluginName", $this->pluginName);
        $this->K_COMMON->saveXMLToFile($xml, $this->dirDownloadBase , "package.xml");

    }

    function emptyDefinitionDataDir()
    {

        $dir = $this->dirDownload;
        $dir .= "/data/tables";

        $this->K_COMMON->emptyDir(["dir" => $dir]);

    }

    function emptyDefinitionDir($definitionDir)
    {

        //include
        $objApiUsefulData = $this->K_COMMON->getPluginObject("k_usefuldata", "api");

        //get list of plugin objects
        $listObject = $objApiUsefulData->listPluginsAndObjects(["pluginName" => $this->pluginName]);

        //set variables
        $dirBase = $this->dirDownload;

        //empty definition dir on base plugin dir
        $dir = $dirBase . "/definition/" . $definitionDir;
        $this->K_COMMON->emptyDir(["dir" => $dir, "rmDir" => "1"]);

        //empty objects definition dir
        foreach ($listObject as $obj) {

            $objType = $obj["objType"];
            $objName = $obj["objName"];
            $dir = $dirBase;

            $objectDir = $this->K_COMMON->getDirByObjectType($objType);
            $dir .= "/" . $objectDir . "/" . $objName;

            $dir .= "/definition/" . $definitionDir;


            //delete all file from dir if exist in old version (copied from plugin dir)
            $this->K_COMMON->emptyDir(["dir" => $dir, "rmDir" => "1"]);
        }


    }

    function exportObjectXML($pluginObj, $xml, $definitionDir, $emptyDir, $filename)
    {


        if ($xml->count() == 0)
            return;

        //set variables
        $dirTempPlugin = $this->dirDownload;

        //set dir
        $dir = $dirTempPlugin;
        if ($pluginObj != "")
            $dir .= "/" . str_replace("\\","/",$pluginObj);

        //dir depend on template type
        if ($definitionDir != "")
            $dir .= "/definition/" . $definitionDir;

        //create dir
        $this->K_COMMON->mkDir(["dir" => $dir, "mask" => 0777]);

        //delete all file from dir if exist in old version (copied from plugin dir)
        if ($emptyDir)
            $this->K_COMMON->emptyDir(["dir" => $dir]);

        //create file name of xml file
        $filename .= ".xml";

        $this->K_COMMON->saveXMLToFile($xml, $dir, $filename);



    }

    //Default for export table definition
    function exportDataObjectXML($dataName, $xml, $definitionDir, $emptyDir, $filename)
    {

        if ($xml->count() == 0)
            return;

        //set variables
        $dirTempPlugin = $this->dirDownload;

        //set dir
        $dir = $dirTempPlugin;
        $dir .= "/data/tables/" . str_replace("\\","/",$dataName);

        //dir definition
        $dir .= "/definition/" . $definitionDir;

        //create dir
        $this->K_COMMON->mkDir(["dir" => $dir, "mask" => 0777]);

        //delete all file from dir if exist in old version (coped from plugin dir)
        if ($emptyDir)
            $this->K_COMMON->emptyDir(["dir" => $dir]);


        //create file name of xml file
        $filename .= ".xml";

        $this->K_COMMON->saveXMLToFile($xml, $dir, $filename);



    }

    /*********************************************************/
    /************** PROCEDURES DOWNLOAD DATA TABLES **********/
    /*********************************************************/


    public function downloadData($ArrayVariable)
    {



        //set variable
        $pluginName = $this->K_COMMON->getVarArray($ArrayVariable, "pluginName");
        if ($pluginName == "")
            $pluginName = $this->pluginName;
        $dirTempPlugin = $this->K_COMMON->getVarArray($ArrayVariable, "dir");


        $tablesList = $this->K_COMMON->getListPluginDBTables($pluginName, "ARRAY");

        foreach ($tablesList as $tablePlugin) {

            $tablePlugin = str_replace($pluginName . "_","", $tablePlugin);

            $this->downloadDataTable(
                [
                    "pluginName" => $pluginName
                    , "tablePlugin" => $tablePlugin
                    , "dir" => $dirTempPlugin
                ]
            );

        }


    }

    public function downloadDataTable($ArrayVariable)
    {

        //set variable
        $pluginName = $this->K_COMMON->getVarArray($ArrayVariable, "pluginName");
        $tablePlugin = $this->K_COMMON->getVarArray($ArrayVariable, "tablePlugin");
        $tableFilter = $this->K_COMMON->getVarArray($ArrayVariable, "tableFilter");
        $dirTempPlugin = $this->dirDownload . $this->K_COMMON->getVarArray($ArrayVariable, "dir") . "/" . $pluginName;

        // create xml
        $xml = new \SimpleXMLElement('<data/>');

        $xml->addChild("pluginName", $pluginName);
        $xml->addChild("tableName", $tablePlugin);

        //include table
        $table = $this->K_COMMON->getPluginDataObject($pluginName, $tablePlugin);

        $fieldsList = $table->getListOfFields($pluginName . "_" . $tablePlugin, true, true);

        //get data of table
        $table->get($fieldsList, $tableFilter);
        $tableResults = $table->getResults("xml", true, true);

        $xmlTable = \simplexml_load_string($tableResults, 'SimpleXMLElement', LIBXML_NOBLANKS);
        $xmlDataTag = $xml->addChild("dataTable");
        $this->K_COMMON->appendToXml($xmlDataTag, $xmlTable);

        //open dir and save xml
        $dir = $dirTempPlugin . "/" . $tablePlugin;

        //create new dir
        $this->K_COMMON->mkDir(["dir" => $dir, "mask" => 0777]);

        $xmlFile = $xml->asXML();
        $filename = "data.xml";
        $content = $xmlFile;
        $arraySaveContent = [];
        $arraySaveContent["dir"] = $dir;
        $arraySaveContent["filename"] = $filename;
        $arraySaveContent["content"] = $content;
        $arraySaveContent["append"] = 0;
        $this->K_COMMON->saveFileContent($arraySaveContent);


    }

    public function downloadRelatedData($ArrayVariable)
    {

        //set variable
        $pluginName = $this->K_COMMON->getVarArray($ArrayVariable, "pluginName");
        if ($pluginName == "")
            $pluginName = $this->pluginName;
        $dirTempPlugin = $this->K_COMMON->getVarArray($ArrayVariable, "dir");

        //include table
        $table = $this->K_COMMON->getPluginDataObject("k_common", "plugins");

        //get data
        $table->get(["plg_related"], ["plg_id" => $pluginName]);
        $data = $table->fetch();
        if ($data)
            $pluginsRelated = (array)json_decode($data->plg_related, true);
        else
            return ["success" => "false"];

        foreach ($pluginsRelated as $keyPluginType => $pluginsType) {
            if (is_array($pluginsType))
                if (count($pluginsType) > 0)
                    foreach ($pluginsType as $plugins) {
                        //create dir for save into dir of current download pluglin
                        $this->downloadData(["dir" => $dirTempPlugin, "pluginName" => $plugins["id"]]);
                    }
        }
    }


    /*********************************************************/
    /************** PROCEDURES DOWNLOAD DEFINITION************/
    /*********************************************************/


    public function downloadSP($ArrayVariable)
    {

        //set variable
        $arrayOfFunction = [];
        $arrayOfProcedure = [];

        //Get All Plugin Procedures Definition
        $query = "SHOW PROCEDURE STATUS WHERE DB = DATABASE() AND Name Like '" . $this->pluginName . "%';";
        $this->kxdb->prepare($query);
        $this->kxdb->execute();


        while ($dataList = $this->kxdb->fetch()) {
            $name = $dataList->Name;
            $arrayOfProcedure[] = $name;
        }

        //Get All Plugin Function Definition
        $query = "SHOW FUNCTION STATUS WHERE DB = DATABASE() AND Name Like '" . $this->pluginName . "%';";
        $this->kxdb->prepare($query);
        $this->kxdb->execute();


        while ($dataList = $this->kxdb->fetch()) {
            $name = $dataList->Name;
            $arrayOfFunction[] = $name;
        }


        //get Definition of procedure and create XML
        $firstTime = true;
        foreach ($arrayOfProcedure as $name) {

            $procName = str_replace($this->pluginName . "_", "", $name);


            $dataSP = $this->kxdb->getSPDefinition($name);


            if ($dataSP["type"] != "") {


                //create tag
                $xml = new \k_common\objects\general\xml\obj('<functions/>');
                $xmlFunction = $xml->addChild("function");

                //create other tag
                $xmlFunction->addChild("name", $procName);
                $xmlFunction->addChild("type", "PROCEDURE");
                $xmlFunction->addChild("parameters", $dataSP["parameters"]);
                $xmlFunction->addChild("returntype", $dataSP["return"]);
                $s = $xmlFunction->addChild("statement");
                $s->addCData($dataSP["statement"]);

                if ($firstTime)
                    $emptyDir = true;
                else
                    $emptyDir = false;

                //Export
                $dirXML = "sp";
                $this->exportObjectXML( "", $xml, $dirXML, $emptyDir, $procName);


                //set previous variables
                $firstTime = false;


            }
        }

        //get Definition of function and create XML
        //first time not initialize because directory may be reset from procedure creation
        foreach ($arrayOfFunction as $name) {

            $procName = str_replace($this->pluginName . "_", "", $name);


            $dataSP = $this->kxdb->getSPDefinition($name);

            if ($dataSP["type"] != "") {


                //create tag
                $xml = new \k_common\objects\general\xml\obj('<functions/>');
                $xmlFunction = $xml->addChild("function");

                //create other tag
                $xmlFunction->addChild("name", $procName);
                $xmlFunction->addChild("type", "FUNCTION");
                $xmlFunction->addChild("parameters", $dataSP["parameters"]);
                $xmlFunction->addChild("returntype", $dataSP["return"]);
                $s = $xmlFunction->addChild("statement");
                $s->addCData($dataSP["statement"]);

                if ($firstTime)
                    $emptyDir = true;
                else
                    $emptyDir = false;

                //Export
                $dirXML = "sp";
                $this->exportObjectXML( "", $xml, $dirXML, $emptyDir, $procName);


                //set previous variables
                $firstTime = false;


            }
        }


    }

    public function downloadTrigger($ArrayVariable)
    {


        //Load Triggers
        $this->kxdb->loadTriggerList($this->pluginName);
        $this->kxdb->execute();

        //Export data
        $firstTime = true;
        while ($dataList = $this->kxdb->fetch()) {
            $name = $dataList->TRIGGER_NAME;
            $table = $dataList->EVENT_OBJECT_TABLE;
            $timing = $dataList->ACTION_TIMING;
            $manipulation = $dataList->EVENT_MANIPULATION;
            $statement = $dataList->ACTION_STATEMENT;


            $trigName = str_replace($table . "_", "", $name);
            $dataName = str_replace($this->pluginName . "_", "", $table);

            //create tag
            $xml = new \k_common\objects\general\xml\obj('<triggers/>');
            $xmlFunction = $xml->addChild("trigger");

            //create other tag
            $xmlFunction->addChild("fires", $timing);
            $xmlFunction->addChild("event", $manipulation);
            $s = $xmlFunction->addChild("statement");
            $s->addCData($statement);

            if ($firstTime)
                $emptyDir = true;
            else
                $emptyDir = false;

            //Export
            $dirXML = "trigger";
            $this->exportDataObjectXML($dataName, $xml, $dirXML, $emptyDir, $trigName);


            //set previous variables
            $firstTime = false;
        }


    }

    public function downloadEvents($ArrayVariable)
    {


        //Load Events
        $this->kxdb->loadEventsList($this->pluginName);

        //Export data
        $firstTime = true;
        while ($dataList = $this->kxdb->fetch()) {
            $name = $dataList->EVENT_NAME;
            $startTime = $dataList->STARTS;
            $interval = $dataList->INTERVAL_VALUE . " " . $dataList->INTERVAL_FIELD;
            $evName = str_replace($this->pluginName . "_", "", $name);
            $statement = $dataList->EVENT_DEFINITION;

            //create tag
            $xml = new \k_common\objects\general\xml\obj('<events/>');
            $xmlFunction = $xml->addChild("event");

            //create other tag
            $xmlFunction->addChild("name", $evName);
            $xmlFunction->addChild("interval", $interval);
            $xmlFunction->addChild("starttime", $startTime);
            $s = $xmlFunction->addChild("statement");
            $s->addCData($statement);

            if ($firstTime)
                $emptyDir = true;
            else
                $emptyDir = false;

            //Export
            $dirXML = "event";
            $this->exportObjectXML( "", $xml, $dirXML, $emptyDir, $evName);


            //set previous variables
            $firstTime = false;
        }


    }

    public function downloadWebTemplate($ArrayVariable)
    {

        //set variables
        $templateType = $this->K_COMMON->getVarArray($ArrayVariable, "templateType");
        $webCode = $this->pluginWebCodeTakeData;
        $pluginObjPrev = "";
        $firstTime = true;

        //load tables
        $tableTPL = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_templates");
        $tableTPLCNT = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_templates_content");

        //load data template of this plugin
        $arayTPLWhere = [];
        $arayTPLWhere["tpl_plugin_name"] = $this->pluginName;
        if ($templateType == "panel")
            $arayTPLWhere[] = '(tpl_type="panel")';
        else
            $arayTPLWhere[] = '(tpl_web_code = "'.$webCode.'" AND tpl_type = "web")';

        $tableTPL->get(
            [
                "tpl_code"
                , "tpl_web_code"
                , "tpl_name"
                , "tpl_file"
                , "tpl_type"
                , "tpl_include"
                , "tpl_plugin_object"
            ]
            ,
            $arayTPLWhere
        );
        while ($dataTPL = $tableTPL->fetch()) {

            //set template variables
            $pluginObj = $dataTPL->tpl_plugin_object;
            $tplType = $dataTPL->tpl_type;
            $code = $dataTPL->tpl_code;
            $name = $dataTPL->tpl_name;
            $fileTPL = $dataTPL->tpl_file;
            $webCodeTPL = $dataTPL->tpl_web_code;

            // create xml general tag
            if ($tplType == "web")
                $xml = new \SimpleXMLElement('<webtemplates/>');
            elseif ($tplType == "panel")
                $xml = new \SimpleXMLElement('<paneltemplates/>');

            //create tag webtemplate / paneltemplate
            if ($tplType == "web")
                $xmlTagWebTemplate = $xml->addChild("webtemplate");
            elseif ($tplType == "panel")
                $xmlTagWebTemplate = $xml->addChild("paneltemplate");

            // create xml of include
            $xmlTempInclude = new \SimpleXMLElement('<include/>');
            if ($dataTPL->tpl_include != "") {
                $include = json_decode($dataTPL->tpl_include, true);
                $this->K_COMMON->arrayToXml($include, $xmlTempInclude);
            }

            //create tag into tag webtemplate
            $xmlTagWebTemplate->addChild("code", $code);
            $xmlTagWebTemplate->addChild("name", $name);
            $xmlTagWebTemplate->addChild("file", $fileTPL);
            $this->K_COMMON->appendToXml($xmlTagWebTemplate, $xmlTempInclude);

            //create tag objects into tag webtemplate
            $xmlTagObjects = $xmlTagWebTemplate->addChild("objects");

            //load Template Content
            $tableTPLCNT->get(
                "*"
                ,
                [
                    "tpo_plugin_name" => $this->pluginName
                    , "tpo_plugin_object" => $pluginObj
                    , "tpo_tpl_code" => $code
                    , "tpo_web_code" => $webCodeTPL
                ]
            );
            while ($dataTPLCNT = $tableTPLCNT->fetch()) {

                //set template content variables
                $id = $dataTPLCNT->tpo_tpl_id_obj;
                $idup = $dataTPLCNT->tpo_tpl_id_obj_up;
                $order = $dataTPLCNT->tpo_order;


                //create tag object into tag objects
                $xmlTagObject = $xmlTagObjects->addChild("object");

                //create tag into tag object
                $xmlTagObject->addChild("id", $id);
                $xmlTagObject->addChild("idup", $idup);
                $xmlTagObject->addChild("order", $order);

                //create tag kwebobjectpar
                $xmlTempKWebObjectPar = new \SimpleXMLElement('<kwebobjectpar/>');
                if ($dataTPLCNT->tpo_parameters != "") {
                    $parametersWOU = json_decode($dataTPLCNT->tpo_parameters, true);// create xml temp
                    $this->K_COMMON->arrayToXml($parametersWOU, $xmlTempKWebObjectPar);
                }
                $this->K_COMMON->appendToXml($xmlTagObject, $xmlTempKWebObjectPar);

                //create tag kwebobject data
                $xmlTempKWebObjectData = new \SimpleXMLElement('<kwebobjectdata/>');
                if ($dataTPLCNT->tpo_data != "") {
                    $arrDataPar = json_decode($dataTPLCNT->tpo_data, true);// create xml temp
                    $this->K_COMMON->arrayToXml($arrDataPar, $xmlTempKWebObjectData);
                }
                $this->K_COMMON->appendToXml($xmlTagObject, $xmlTempKWebObjectData);

                //get object used of template
                $pluginNameWOU = $dataTPLCNT->tpo_wo_plugin_name;
                $pluginobjectWOU = $dataTPLCNT->tpo_wo_plugin_object;
                $pCodeWOU = $dataTPLCNT->tpo_wo_plugin_object_pcode;


                $xmlTempContentPar = new \k_common\objects\general\xml\obj('<parameters/>');
                if ($dataTPLCNT->tpo_wo_parameters != "") {
                    $parametersWOU = json_decode($dataTPLCNT->tpo_wo_parameters, true);// create xml temp
                    $this->K_COMMON->arrayToXml($parametersWOU, $xmlTempContentPar, "", true);
                }

                //create tag into tag object
                $xmlTagObject->addChild("pluginname", $pluginNameWOU);
                $xmlTagObject->addChild("pluginobject", $pluginobjectWOU);
                $xmlTagObject->addChild("pcode", $pCodeWOU);
                $this->K_COMMON->appendToXml($xmlTagObject, $xmlTempContentPar);


            }


            //dir depend on template type
            if ($tplType == "web")
                $dirXML = "webtemplate";
            elseif ($tplType == "panel")
                $dirXML = "paneltemplate";

            //check if delete all file from dir if exist in old version (copied from plugin dir)
            if ($pluginObj != $pluginObjPrev || $firstTime)
                $emptyDir = true;
            else
                $emptyDir = false;

            //Export
            $this->exportObjectXML( $pluginObj, $xml, $dirXML, $emptyDir, $code);


            //set previous variables
            $pluginObjPrev = $pluginObj;
            $firstTime = false;
        }


        return ["success" => "true"];


    }

    public function downloadWebPages($ArrayVariable)
    {

        //set variables
        $webCode = $this->pluginWebCodeTakeData;
        $pluginObjPrev = "";
        $firstTime = true;

        //include modules
        $objURLRewrite = $this->K_COMMON->getPluginObject("k_webmanager", "urlrewrite");

        //load tables
        $tableTPL = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_templates");
        $tableWPG = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_pages");

        //load data
        $arrayWPGWhere = [];
        $arrayWPGWhere["wpg_plugin_name"] = $this->pluginName;
        $arrayWPGWhere[] = 'wpg_web_code = "'.$webCode.'"';
        $tableWPG->get(["*"],$arrayWPGWhere);
        while ($dataWPG = $tableWPG->fetch()) {

            //set template variables
            $keyID = $dataWPG->id_web_page;
            $pluginObj = $dataWPG->wpg_plugin_object;
            $title = $dataWPG->wpg_title;
            $code = $dataWPG->wpg_code;
            $name = $dataWPG->wpg_name;
            $tmplID = $dataWPG->wpg_tmpl_id;
            $checkLogged = $dataWPG->wpg_check_logged;

            //Load Template Data
            $arrayTPLWhere = [];
            $arrayTPLWhere["id_web_template"] = $tmplID;
            $arrayTPLWhere["tpl_plugin_name"] = $this->pluginName; //We can get template only if we are in the same "plugin"
            $tableTPL->get(["*"],$arrayTPLWhere);
            $dataTPL = $tableTPL->fetch();
            if ($dataTPL)
            {
                $tmplCode = $dataTPL->tpl_code;
            }
            else
            {
                $tmplCode = "";
            }

            //get url
            $url = $objURLRewrite->getURL(
                [
                    "pluginName" => $this->pluginName
                    , "pluginObject" => $pluginObj
                    , "refCode" => $keyID
                    , "webCode" => $webCode
                ]
            );


            // create xml general tag
            $xml = new \SimpleXMLElement('<webpages/>');
            $xmlWebPage = $xml->addChild("webpage");

            //create tag into tag webtemplate
            $xmlWebPage->addChild("title", $title);
            $xmlWebPage->addChild("code", $code);
            $xmlWebPage->addChild("name", $name);
            $xmlWebPage->addChild("logged", $checkLogged);
            $xmlWebPage->addChild("tmplCode", $tmplCode);
            $xmlWebPage->addChild("url", $url);


            //dir depend on template type
            $dirXML = "webpage";

            //check if delete all file from dir if exist in old version (copied from plugin dir)
            if ($pluginObj != $pluginObjPrev || $firstTime)
                $emptyDir = true;
            else
                $emptyDir = false;

            //Export
            $this->exportObjectXML( $pluginObj, $xml, $dirXML, $emptyDir, $code);


            //set previous variables
            $pluginObjPrev = $pluginObj;
            $firstTime = false;
        }


        return ["success" => "true"];


    }

    public function downloadDictionary($ArrayVariable)
    {

        //set variables
        $webCode = $this->pluginWebCodeTakeData;
        $pluginName = $this->K_COMMON->getVarArray($ArrayVariable, "pluginName");
        if ($pluginName == "")
            $pluginName = $this->pluginName;
        $languageCode = $this->K_COMMON->getVarArray($ArrayVariable, "languageCode");

        //get dictionary table
        $tableDIC = $this->K_COMMON->getPluginDataObject("k_dictionary", "dictionary");
        $cond = ["dic_module" => $pluginName];

        if ($webCode != "")
            $cond[] = "(dic_web_code = '' OR dic_web_code = '".$webCode."')";

        if ($languageCode != "")
            $cond["dic_language_code"] = $languageCode;
        $tableDIC->getByQuery("dictionary_complete","*", $cond, "", "", ["dic_module_object", "dic_code", "dic_language_code"]);

        $xml = new \k_common\objects\general\xml\obj('<dictionary/>');
        $lastWordCode = "";
        $lastObjectCode = "";
        $firstTime = true;
        while ($dataDIC = $tableDIC->fetch()) {

            $wordWebCode = $dataDIC->dic_web_code;
            $pluginObject = $dataDIC->dic_module_object;
            $wordCode = $dataDIC->dic_code;
            $wordLink = $dataDIC->dcd_link;
            if ($lastObjectCode <> $dataDIC->dic_module_object) {
                //check we must save data
                if (!$firstTime) {
                    $this->exportObjectXML($lastObjectCode, $xml, "dictionary", true, "dictionary");
                    $xml = new \k_common\objects\general\xml\obj('<dictionary/>');
                }

            }
            if ($lastWordCode <> $wordCode || $lastObjectCode <> $pluginObject) {

                $xmlTagWord = $xml->addChild("word");
                $xmlTagWord->addChild("code", $wordCode);
                if ($wordWebCode != "")
                    $xmlTagWord->addChild("isWeb", "1"); //is a word for web
                $xmlTagLanguages = $xmlTagWord->addChild("languages");

                //link
                if ($wordLink != "")
                {

                    $xmlTemp = new \SimpleXMLElement('<link/>');
                    $tempArray = json_decode($wordLink, true);
                    $this->K_COMMON->arrayToXml($tempArray, $xmlTemp);
                    $this->K_COMMON->appendToXml($xmlTagWord, $xmlTemp);

                }

            }

            //Add Word to XML
            $xmlTagTranslation = $xmlTagLanguages->addChild($dataDIC->dic_language_code);
            $xmlTagTranslation->addCData($dataDIC->dic_word);

            //set previous variables
            $lastWordCode = $dataDIC->dic_code;
            $lastObjectCode = $pluginObject;
            $firstTime = false;

        }

        $this->exportObjectXML($lastObjectCode, $xml, "dictionary", true, "dictionary");

        return ["success" => "true"];


    }

    public function downloadMailingActions($ArrayVariable)
    {


        //get table
        $table = $this->K_COMMON->getPluginDataObject("k_mailing", "actions");
        $cond = ["act_plugin_name" => $this->pluginName, "act_web_code" => ""];
        $table->get("*", $cond, "", "", ["act_plugin_object", "act_code"]);


        $xml = new \k_common\objects\general\xml\obj('<actions/>');
        $lastCode = "";
        $lastObjectCode = "";
        $firstTime = true;
        while ($data = $table->fetch()) {

            $pluginObject = $data->act_plugin_object;
            $code = $data->act_code;
            $actFrom = $data->act_email_from;
            $actTo = $data->act_email_to;
            $actMsgCode = $data->act_message_code;

            if ($lastObjectCode <> $pluginObject) {
                //check we must save data
                if (!$firstTime) {
                    $this->exportObjectXML($lastObjectCode, $xml, "actions", true, "act");
                    $xml = new \k_common\objects\general\xml\obj('<actions/>');
                }

            }
            if ($lastCode <> $code || $lastObjectCode <> $pluginObject) {

                $xmlTagAction = $xml->addChild("action");
                $xmlTagAction->addChild("code", $code);
                $xmlTagAction->addChild("from", $actFrom);
                $xmlTagAction->addChild("to", $actTo);
                $xmlTagAction->addChild("messagecode", $actMsgCode);


            }


            //set previous variables
            $lastCode = $code;
            $lastObjectCode = $pluginObject;
            $firstTime = false;

        }

        $this->exportObjectXML($lastObjectCode, $xml, "actions", true, "act");

        return ["success" => "true"];


    }

    public function downloadMailingMessages($ArrayVariable)
    {


        //get table
        $table = $this->K_COMMON->getPluginDataObject("k_mailing", "messages");
        $cond = ["msg_plugin_name" => $this->pluginName, "msg_web_code" => ""];
        $table->get("*", $cond, "", "", ["msg_plugin_object", "msg_code", "msg_language_code"]);


        $xml = new \k_common\objects\general\xml\obj('<messages/>');
        $lastCode = "";
        $lastObjectCode = "";
        $firstTime = true;
        while ($data = $table->fetch()) {

            $pluginObject = $data->msg_plugin_object;
            $code = $data->msg_code;
            $languageCode = $data->msg_language_code;
            $msgSubject = $data->msg_subject;
            $msgMessage = $data->msg_message;

            if ($lastObjectCode <> $pluginObject) {
                //check we must save data
                if (!$firstTime) {
                    $this->exportObjectXML($lastObjectCode, $xml, "messages", true, "messages");
                    $xml = new \k_common\objects\general\xml\obj('<messages/>');
                }

            }
            if ($lastCode <> $code || $lastObjectCode <> $pluginObject) {

                $xmlTagWord = $xml->addChild("message");
                $xmlTagWord->addChild("code", $code);
                $xmlTagLanguages = $xmlTagWord->addChild("languages");

            }

            //Add Word to XML
            $xmlTagTranslation = $xmlTagLanguages->addChild($languageCode);
            $xmlTagSubject = $xmlTagTranslation->addChild("subject");
            $xmlTagSubject->addCData($msgSubject);
            $xmlTagBody = $xmlTagTranslation->addChild("body");
            $xmlTagBody->addCData($msgMessage);

            //set previous variables
            $lastCode = $code;
            $lastObjectCode = $pluginObject;
            $firstTime = false;

        }

        $this->exportObjectXML($lastObjectCode, $xml, "messages", true, "messages");

        return ["success" => "true"];


    }

    public function downloadWebObjects($ArrayVariable)
    {

        //get dictionary table
        $table = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_objects");
        $cond = ["wob_plugin_name" => $this->pluginName];
        $table->get("*", $cond, "", "", ["wob_plugin_object"]);


        $xml = new \k_common\objects\general\xml\obj('<webobjects/>');
        $lastCode = "";
        $lastObjectCode = "";
        $firstTime = true;
        while ($data = $table->fetch()) {

            $pluginObject = $data->wob_plugin_object;
            $code = $data->id_web_objects;
            $container = $data->wob_container;
            $name = $data->wob_name;
            $type = $data->wob_type;
            $prototypes = $data->wob_prototypes;

            if ($lastObjectCode <> $pluginObject) {
                //check we must save data
                if (!$firstTime) {
                    $this->exportObjectXML($lastObjectCode, $xml, "webobject", true, "webobject");
                    $xml = new \k_common\objects\general\xml\obj('<webobjects/>');
                }

            }
            if ($lastCode <> $code || $lastObjectCode <> $pluginObject) {

                $xmlTagBase = $xml->addChild("webobject");
                $xmlTagBase->addChild("name", $name);
                $xmlTagBase->addChild("container", $container);
                $xmlTagBase->addChild("type", $type);

                // create xml of prototypes
                $xmlTemp = new \SimpleXMLElement('<prototypes/>');
                if ($prototypes != "") {
                    $tempArray = json_decode($prototypes, true);
                    $this->K_COMMON->arrayToXml($tempArray, $xmlTemp);
                    $this->K_COMMON->appendToXml($xmlTagBase, $xmlTemp);

                }


            }

            //set previous variables
            $lastCode = $code;
            $lastObjectCode = $pluginObject;
            $firstTime = false;

        }

        $this->exportObjectXML($lastObjectCode, $xml, "webobject", true, "webobject");

        return ["success" => "true"];


    }

    public function downloadAuthorization($ArrayVariable)
    {


        //get dictionary table
        $table = $this->K_COMMON->getPluginDataObject("k_secauth", "authtype");
        $cond = ["plugin_name" => $this->pluginName];
        $table->get("*", $cond, "", "", ["code_form"]);


        $xml = new \k_common\objects\general\xml\obj('<authorizations/>');
        $lastCode = "";
        while ($data = $table->fetch()) {

            $code = $data->code_form;
            $label = $data->code_label;
            $dataref = $data->code_table_ref;
            $byWebTarget = $data->auth_by_web_target;
            $description = $data->description;

            if ($lastCode <> $code) {


                $xmlTagBase = $xml->addChild("authorization");
                $xmlTagBase->addChild("code", $code);
                $xmlTagBase->addChild("label", $label);
                $xmlTagBase->addChild("dataref", $dataref);
                $xmlTagBase->addChild("byWebTarget", $byWebTarget);
                $xmlTagBase->addChild("description", $description);

            }


            //set previous variables
            $lastCode = $code;

        }

        $this->exportObjectXML("", $xml, "authorization", true, "auth");

        return ["success" => "true"];


    }

    public function downloadPanelMenu($ArrayVariable)
    {

        //get dictionary table
        $table = $this->K_COMMON->getPluginDataObject("k_panelmanage", "menu");
        $cond = ["pmn_owner_plugin_name" => $this->pluginName];
        $table->get("*", $cond, "", "", ["pmn_owner_plugin_object", "pmn_menu_code"]);


        $xml = new \k_common\objects\general\xml\obj('<panel/>');
        $lastCode = "";
        $lastObjectCode = "";
        $firstTime = true;
        while ($data = $table->fetch()) {

            $fld_owner_pluginObject = $data->pmn_owner_plugin_object;
            $fld_pluginName = $data->pmn_plugin_name;
            $fld_pluginObject = $data->pmn_plugin_object;
            $fld_code = $data->pmn_menu_code;
            $fld_menuRefCode = $data->pmn_menu_ref_code;
            $fld_upCode = $data->pmn_menu_up_code;
            $fld_order = $data->pmn_order;
            $fld_label = $data->pmn_label;
            $fld_icon = $data->pmn_icon;
            $fld_authPName = $data->pmn_authorization_plugin_name;
            $fld_authCode = $data->pmn_authorization;
            $fld_url = $data->pmn_url;
            $fld_GPName = $data->pmn_get_plugin_name;
            $fld_GPObj = $data->pmn_get_plugin_object;
            $fld_GPMethod = $data->pmn_get_plugin_method;
            $fld_GPTplCode = $data->pmn_tpl_code;


            if ($lastObjectCode <> $fld_owner_pluginObject) {
                //check we must save data
                if (!$firstTime) {
                    $this->exportObjectXML($lastObjectCode, $xml, "panel", false, "menu");
                    $xml = new \k_common\objects\general\xml\obj('<panel/>');
                }

            }

            if ($lastCode <> $fld_code || $lastObjectCode <> $fld_owner_pluginObject) {


                $xmlTagBase = $xml->addChild("menu");

                $xmlTagBase->addChild("displayPluginName", $fld_pluginName);
                $xmlTagBase->addChild("displayPluginObject", $fld_pluginObject);

                $xmlTagBase->addChild("code", $fld_code);
                $xmlTagBase->addChild("refCode", $fld_menuRefCode);
                $xmlTagBase->addChild("menuup", $fld_upCode);
                $xmlTagBase->addChild("order", $fld_order);

                $xmlTagBase->addChild("label", $fld_label);
                $xmlTagBase->addChild("icon", $fld_icon);

                $xmlTagBase->addChild("authPName", $fld_authPName);
                $xmlTagBase->addChild("auth", $fld_authCode);



                $xmlTagBase->addChild("url", $fld_url);


                $xmlTagBase->addChild("getPluginName", $fld_GPName);
                $xmlTagBase->addChild("getPluginObject", $fld_GPObj);
                $xmlTagBase->addChild("getPluginMethod", $fld_GPMethod);
                $xmlTagBase->addChild("getPluginTpl", $fld_GPTplCode);


            }


            //set previous variables
            $lastCode = $fld_code;
            $lastObjectCode = $fld_owner_pluginObject;
            $firstTime = false;

        }

        $this->exportObjectXML($lastObjectCode, $xml, "panel", false, "menu");

        return ["success" => "true"];


    }

    public function downloadPanelDashboard($ArrayVariable)
    {

        //get dictionary table
        $table = $this->K_COMMON->getPluginDataObject("k_panelmanage", "dashboard");
        $cond = ["dmn_plugin_name" => $this->pluginName];
        $table->get("*", $cond, "", "", ["dmn_plugin_object", "dmn_menu_code"]);


        $xml = new \k_common\objects\general\xml\obj('<panel/>');
        $lastCode = "";
        $lastObjectCode = "";
        $firstTime = true;
        while ($data = $table->fetch()) {

            $fld_pluginObject = $data->dmn_plugin_object;
            $fld_code = $data->dmn_menu_code;
            $fld_type = $data->dmn_type;
            $fld_label = $data->dmn_label;
            $fld_icon = $data->dmn_icon;
            $fld_authPName = $data->dmn_authorization_plugin_name;
            $fld_authCode = $data->dmn_authorization;


            if ($lastObjectCode <> $fld_pluginObject) {
                //check we must save data
                if (!$firstTime) {
                    //in this case we export without empty dir, because is the same of panel menu
                    $this->exportObjectXML($lastObjectCode, $xml, "panel", false, "dashboard");
                    $xml = new \k_common\objects\general\xml\obj('<panel/>');
                }

            }

            if ($lastCode <> $fld_code || $lastObjectCode <> $fld_pluginObject) {


                $xmlTagBase = $xml->addChild("dashboard");
                $xmlTagBase->addChild("code", $fld_code);
                $xmlTagBase->addChild("type", $fld_type);
                $xmlTagBase->addChild("label", $fld_label);
                $xmlTagBase->addChild("icon", $fld_icon);

                $xmlTagBase->addChild("authPName", $fld_authPName);
                $xmlTagBase->addChild("auth", $fld_authCode);


            }


            //set previous variables
            $lastCode = $fld_code;
            $lastObjectCode = $fld_pluginObject;
            $firstTime = false;

        }

        $this->exportObjectXML($lastObjectCode, $xml, "panel", false, "dashboard");

        return ["success" => "true"];


    }

    public function downloadParameters()
    {


        //get tables
        $tablePar = $this->K_COMMON->getPluginDataObject("k_common", "parameters");
        $tableParDef = $this->K_COMMON->getPluginDataObject("k_common", "parameters_definition");
        $cond = ["par_plugin_name" => $this->pluginName];
        $tableParDef->get("*", $cond, "", "", ["par_code"]);


        $xml = new \k_common\objects\general\xml\obj('<parameters/>');
        $lastCode = "";
        $lastObjectCode = "";
        while ($data = $tableParDef->fetch()) {

            $fld_code = $data->par_code;
            $fld_description = $data->par_description;
            $fld_label = $data->par_code_label;

            if ($lastCode <> $fld_code) {

                //get default value
                $tablePar->get("*", ["par_plugin_name = '" . $this->pluginName . "' AND par_code='" . $fld_code . "' AND (par_web_code is null OR par_web_code ='')   "]);
                $dataPar = $tablePar->fetch();
                if ($dataPar)
                    $fld_defValue = $dataPar->par_value;
                else
                    $fld_defValue = "";

                $xmlTagBase = $xml->addChild("parameter");
                $xmlTagBase->addChild("code", $fld_code);
                $xmlTagBase->addChild("description", $fld_description);
                $xmlTagBase->addChild("label", $fld_label);
                $xmlTagBase->addChild("value", $fld_defValue);


            }


            //set previous variables
            $lastCode = $fld_code;

        }

        $this->exportObjectXML($lastObjectCode, $xml, "parameters", true, "param");

        return ["success" => "true"];


    }

    public function downloadPluginConfig($ArrayVariable)
    {


        $xml = new \k_common\objects\general\xml\obj('<config/>');

        $fld_type = $this->pluginSettings->plg_type;
        $fld_content = $this->pluginSettings->plg_content;
        $fld_name = $this->pluginSettings->plg_name;
        $fld_ver = $this->pluginSettings->plg_ver;
        $fld_language = $this->pluginSettings->plg_language;
        $fld_description = $this->pluginSettings->plg_description;
        $fld_authors = $this->pluginSettings->plg_authors;
        $fld_related = $this->pluginSettings->plg_related;
        $fld_parameters = $this->pluginSettings->plg_parameters;


        $xmlTagBase = $xml;
        $xmlTagBase->addChild("id", $this->pluginName);
        $xmlTagBase->addChild("type", $fld_type);
        $xmlTagBase->addChild("content", $fld_content);
        $xmlTagBase->addChild("name", $fld_name);
        $xmlTagBase->addChild("ver", $fld_ver);
        $xmlTagBase->addChild("language", $fld_language);
        $xmlTagBase->addChild("description", $fld_description);


        //create tag authors
        $xmlTempAuthors = new \SimpleXMLElement('<authors/>');
        if ($fld_authors) {
            $arrTmp = json_decode($fld_authors, true);// create xml temp
            $this->K_COMMON->arrayToXml($arrTmp, $xmlTempAuthors);
        }
        $this->K_COMMON->appendToXml($xmlTagBase, $xmlTempAuthors);

        //create tag related
        $xmlTempRelated = new \SimpleXMLElement('<related/>');
        if ($fld_related) {
            $arrTmp = json_decode($fld_related, true);// create xml temp
            $this->K_COMMON->arrayToXml($arrTmp, $xmlTempRelated);
        }
        $this->K_COMMON->appendToXml($xmlTagBase, $xmlTempRelated);

        //create tag parameters
        $xmlTempParameters = new \SimpleXMLElement('<parameters/>');
        if ($fld_parameters) {
            $arrTmp = json_decode($fld_parameters, true);// create xml temp
            $this->K_COMMON->arrayToXml($arrTmp, $xmlTempParameters);
        }
        $this->K_COMMON->appendToXml($xmlTagBase, $xmlTempParameters);


        $this->exportObjectXML("", $xml, "", false, "config");

        return ["success" => "true"];


    }

    public function downloadDataDefinition($ArrayVariable)
    {


        //set variables
        $tablesList = [];
        $plgID = $this->pluginName;

        //Load Tables of Plugin
        $tablesList = $this->K_COMMON->getListPluginDBTables($plgID, "ARRAY");


        foreach ($tablesList as $t) {
            $this->downloadDataDefinitionTable(
                [
                    "pluginName" => $plgID,
                    "tableName" => $t
                ]
            );
        }

        //Load Tables of others plugin customized from this plugin
        $this->K_COMMON->getListPluginDBTablesWithCustomField($plgID);

        $count = 0;
        $tablesList = [];
        while ($dataList = $this->kxdb->fetch()) {

            $name = $dataList->TABLE_NAME;
            $customPlgID = $this->K_COMMON->getPluginNameByTable($name);
            $tablesList[$count]["name"] = $name;
            $tablesList[$count]["plgID"] = $customPlgID;
            $count++;

        }

        foreach ($tablesList as $t) {
            $plgIDCustomized = $t["plgID"];
            $tableCustomized = $t["name"];
            $this->downloadDataDefinitionTable(
                [
                    "pluginName" => $plgIDCustomized,
                    "tableName" => $tableCustomized,
                    "isCustom" => "1"
                ]
            );
        }


        return ["success" => "true"];


    }

    public function downloadDataDefinitionTable($ArrayVariable)
    {


        //set variables
        $plgID = $this->pluginName; //Actual Plugin ID
        $pluginName = $this->K_COMMON->getVarArray($ArrayVariable, "pluginName"); //plugin ID of table to be downloaded
        $tableName = $this->K_COMMON->getVarArray($ArrayVariable, "tableName");
        $isCustom = $this->K_COMMON->getVarArray($ArrayVariable, "isCustom");
        $tableNameShort = str_replace($pluginName . "_", "", $tableName);


        //set Data Name
        $dataName = $tableNameShort;
        if ($isCustom == "1")
            $dataName = "zkx_" . $pluginName . "_" . $dataName;
        //$dataName = "zkx_custom_" . substr($pluginName, strpos($pluginName,"_") + 1) . "_" . $dataName;

        $dataName = strtolower($dataName);

        //Load Table Definition
        $this->K_COMMON->kxdb->loadTableDefinition($tableName);
        $xml = new \k_common\objects\general\xml\obj('<tables/>');
        $xmlTagTable = $xml->addChild("table");
        $xmlTagTable->addChild("name", $tableNameShort);
        if ($isCustom == "1")
        {
            $xmlTagCustom = $xmlTagTable->addChild("custom");
            $xmlTagCustom->addChild("plugin", $pluginName);
        }


        $xmlTagColumns = $xmlTagTable->addChild("columns");
        while ($data = $this->K_COMMON->kxdb->fetch()) {

            $fieldName = $data->Field;
            $addField = false;

            if ($isCustom != "1")
            {

                if (substr($fieldName, 0, 4) != "zkx_")
                    $addField = true;
            }
            else
            {
                if (substr($fieldName, 0, 5 + strlen($plgID)) == "zkx_" . $plgID . "_"){
                    $fieldName = str_replace("zkx_" . $plgID . "_","",$fieldName);
                    $addField = true;
                }
            }

            if ($addField)
            {
                $xmlTagColumn = $xmlTagColumns->addChild("column");
                $xmlTagColumn->addChild("field", $fieldName);
                $xmlTagColumn->addChild("type", $data->Type);
                $xmlTagColumn->addChild("null", $data->Null);
                $xmlTagColumn->addChild("key", ($data->Key == "PRI" ? $data->Key : ""));
                $xmlTagColumn->addChild("default", $data->Default);
                $xmlTagColumn->addChild("extra", $data->Extra);
                $xmlTagColumn->addChild("comment", $data->Comment);
            }



        }

        //Load Index Definition
        $this->K_COMMON->kxdb->loadIndexDefinition($tableName);
        $xmlTagIndexes = $xmlTagTable->addChild("indexes");
        $lastKeyName = "";
        $lastKeyNameToSave = "";
        $lastNonUnique = "";
        $lastIndexType = "";
        $indexFields = "";
        $addIndex = false;
        while ($data = $this->K_COMMON->kxdb->fetch()) {

            $keyName = $data->Key_name;
            $keyNameToSave = str_replace("zkx_" . $plgID . "_", "", $keyName);
            $keyNameToSave = str_replace($tableName . "_", "", $keyNameToSave);

            //Add prev index
            if ($keyName != $lastKeyName) {

                if ($indexFields != "") {
                    $indexFields = substr($indexFields, 1);
                    $xmlTagIndex = $xmlTagIndexes->addChild("index");
                    $xmlTagIndex->addChild("name", $lastKeyNameToSave);
                    $xmlTagIndex->addChild("unique", ($lastNonUnique == "1" ? 0 : 1));
                    $xmlTagIndex->addChild("columns", $indexFields);
                    $xmlTagIndex->addChild("type", $lastIndexType);
                    $indexFields = "";

                }
            }

            $addIndex = false;

            if ($keyName != "PRIMARY") {
                if ($isCustom != "1") {
                    if (strpos($keyName, "zkx_") === false)
                        $addIndex = true;

                } else {
                    if (strpos($keyName, "zkx_" . $plgID . "_") !== false)
                        $addIndex = true;
                }

            }


            if ($addIndex)
                $indexFields .= "," . $data->Column_name;


            $lastKeyName = $keyName;
            $lastKeyNameToSave = $keyNameToSave;
            $lastNonUnique = $data->Non_unique;
            $lastIndexType = $data->Index_type;


        }

        //Add prev index
        if ($addIndex && $indexFields != "")
        {
            $indexFields = substr($indexFields, 1);
            $xmlTagIndex = $xmlTagIndexes->addChild("index");
            $xmlTagIndex->addChild("name", $lastKeyNameToSave);
            $xmlTagIndex->addChild("unique", ($lastNonUnique == "1" ? 0 : 1));
            $xmlTagIndex->addChild("columns", $indexFields);
            $xmlTagIndex->addChild("type", $lastIndexType);
        }






        //Load Foreign Key Definition
        $this->K_COMMON->kxdb->loadForeignKeyDefinition($tableName);
        $xmlTagFKeys = $xmlTagTable->addChild("fkeys");
        $lastKeyName = "";
        $lastKeyNameToSave = "";
        $lastRefTable = "";
        $lastOnDelete = "";
        $lastOnUpdate = "";
        $fkFields = "";
        $fkFieldsRef = "";
        $addFK = false;
        while ($data = $this->K_COMMON->kxdb->fetch()) {


            $keyName = $data->CONSTRAINT_NAME;
            $keyNameToSave = str_replace("zkx_" . $plgID . "_","",$keyName);
            $keyNameToSave = str_replace($tableName . "_", "", $keyNameToSave);



            //Add prev index
            if ($keyName != $lastKeyName) {

                if ($fkFields != "") {
                    $fkFields = substr($fkFields, 1);
                    $fkFieldsRef = substr($fkFieldsRef, 1);

                    $xmlTagFK = $xmlTagFKeys->addChild("fk");
                    $xmlTagFK->addChild("name", $lastKeyNameToSave);
                    $xmlTagFK->addChild("fields", $fkFields);
                    $xmlTagFK->addChild("ref_table_name", $lastRefTable);
                    $xmlTagFK->addChild("ref_fields", $fkFieldsRef);
                    $xmlTagFK->addChild("on_delete", $lastOnDelete);
                    $xmlTagFK->addChild("on_update", $lastOnUpdate);

                    $fkFields = "";
                    $fkFieldsRef = "";

                }
            }

            $addFK = false;

            if ($isCustom != "1")
            {
                if (strpos($keyName, "zkx_") === false)
                    $addFK = true;

            }
            else
            {
                if (strpos($keyName, "zkx_" . $plgID . "_") !== false)
                    $addFK = true;
            }

            if ($addFK)
            {
                $fkFields .= "," . $data->COLUMN_NAME;
                $fkFieldsRef .= "," . $data->REFERENCED_COLUMN_NAME;

            }




            $lastKeyName = $keyName;
            $lastKeyNameToSave = $keyNameToSave;
            $lastRefTable = $data->REFERENCED_TABLE_NAME;
            $lastOnDelete = $data->DELETE_RULE;
            $lastOnUpdate = $data->UPDATE_RULE;


        }



        //Add prev FK
        if ($addFK && $fkFields != "")
        {
            $fkFields = substr($fkFields, 1);
            $fkFieldsRef = substr($fkFieldsRef, 1);

            $xmlTagFK = $xmlTagFKeys->addChild("fk");
            $xmlTagFK->addChild("name", $lastKeyNameToSave);
            $xmlTagFK->addChild("fields", $fkFields);
            $xmlTagFK->addChild("ref_table_name", $lastRefTable);
            $xmlTagFK->addChild("ref_fields", $fkFieldsRef);
            $xmlTagFK->addChild("on_delete", $lastOnDelete);
            $xmlTagFK->addChild("on_update", $lastOnUpdate);


        }


        //export file xml
        $this->exportDataObjectXML($dataName, $xml, "table", false, "table");


        return ["success" => "true"];


    }


    /*********************************************************/
    /************** PROCEDURES DOWNLOAD DATA************/
    /*********************************************************/

    protected function downloadCustom($ArrayVariable)
    {


    //get dictionary table
    $table = $this->K_COMMON->getPluginDataObject("k_custommanager", "customization");
    $cond = ["cst_owner_plugin_name" => $this->pluginName];
    $table->get("", $cond);

    while ($data = $table->fetch()) {

        $cst_code = $data->cst_code;
        $cst_owner_plugin_object = $data->cst_owner_plugin_object;
        $cst_source_plugin_name = $data->cst_source_plugin_name;
        $cst_source_plugin_object = $data->cst_source_plugin_object;
        $cst_target_when = $data->cst_target_when;
        $cst_target_plugin_name = $data->cst_target_plugin_name;
        $cst_target_plugin_object = $data->cst_target_plugin_object;
        $cst_description = $data->cst_description;
        $cst_parameters = $data->cst_parameters;


        $xml = new \k_common\objects\general\xml\obj('<customs/>');

        //Add Word to XML
        $xmlTagField = $xml->addChild("custom");

        $xmlTagField->addChild("code", $cst_code);
        $xmlTagField->addChild("description", $cst_description);
        $xmlTagField->addChild("source_plugin", $cst_source_plugin_name);
        $xmlTagField->addChild("source_object", $cst_source_plugin_object);
        $xmlTagField->addChild("target_when", $cst_target_when);
        $xmlTagField->addChild("target_plugin", $cst_target_plugin_name);
        $xmlTagField->addChild("target_object", $cst_target_plugin_object);

        // create xml of parameters
        $xmlParameters = new \SimpleXMLElement('<parameters/>');
        if ($cst_parameters != "") {
            $cst_parameters_array = json_decode($cst_parameters, true);
            $this->K_COMMON->arrayToXml($cst_parameters_array, $xmlParameters);
        }

        //Add Parameters
        $this->K_COMMON->appendToXml($xmlTagField, $xmlParameters);

        $this->exportObjectXML( $cst_owner_plugin_object, $xml, "custom", false, $cst_code);


    }

    return ["success" => "true"];




    }

    public function downloadCustomFields($ArrayVariable)
    {

        //get dictionary table


        $table = $this->K_COMMON->getPluginDataObject("k_usefuldata", "customfields");
        $cond = ["k_usefuldata_customfield.plugin_name" => $this->pluginName];
        $table->getByQuery("customfields","", $cond, "", "", ["k_usefuldata_customfield.plugin_name", "k_usefuldata_customfield.custom_field_code"]);

        $xml = new \k_common\objects\general\xml\obj('<customfields/>');
        $lastCode = "";
        while ($data = $table->fetch()) {

            $custom_field_code = $data->custom_field_code;
            $field_name = $data->field_name;
            $custom_field_order = $data->custom_field_order;
            $field_code = $data->field_code;
            $field_text = $data->field_text;

            if ($lastCode <> $custom_field_code) {

                $xmlTagCF = $xml->addChild("customfield");
                $xmlTagCF->addChild("code", $custom_field_code);
                $xmlTagCF->addChild("name", $field_name);
                $xmlTagFields = $xmlTagCF->addChild("fields");

            }

            //Add Word to XML
            $xmlTagField = $xmlTagFields->addChild("field");
            $xmlTagField->addChild("code", $field_code);
            $xmlTagText = $xmlTagField->addChild("text");
            $xmlTagText->addCData($field_text);
            $xmlTagField->addChild("order", $custom_field_order);

            //set previous variables
            $lastCode = $custom_field_code;

        }

        $this->exportObjectXML( "", $xml, "customfields", true, "customfields");

        return ["success" => "true"];


    }

    public function downloadUsefulDataCountry($ArrayVariable)
    {


        //set variables
        $languageCode = $this->K_COMMON->getVarArray($ArrayVariable, "languageCode");
        $iso = $this->K_COMMON->getVarArray($ArrayVariable, "iso");
        $cond = [];

        //get dictionary table
        $table = $this->K_COMMON->getPluginDataObject("k_usefuldata", "country");
        if ($iso != "")
            $cond["iso"] = $iso;
        if ($languageCode != "")
            $cond["language"] = $languageCode;
        $table->get("", $cond, "", "", ["iso", "language"]);

        $xml = new \k_common\objects\general\xml\obj('<countries/>');
        $lastCode = "";
        while ($data = $table->fetch()) {

            $iso = $data->iso;
            $name = $data->name;
            $printable_name = $data->printable_name;
            $iso3 = $data->iso3;
            $numcode = $data->numcode;
            $language = $data->language;

            if ($lastCode <> $iso) {

                $xmlTagCF = $xml->addChild("country");
                $xmlTagCF->addChild("iso", $iso);
                $xmlTagCF->addChild("iso3", $iso3);
                $xmlTagCF->addChild("numcode", $numcode);
                $xmlTagLanguages = $xmlTagCF->addChild("languages");

            }

            //Add to XML
            $xmlTagLanguage = $xmlTagLanguages->addChild($language);
            $xmlTagName = $xmlTagLanguage->addChild("name");
            $xmlTagName->addCData($name);
            $xmlTagPName = $xmlTagLanguage->addChild("printable_name");
            $xmlTagPName->addCData($printable_name);


            //set previous variables
            $lastCode = $iso;

        }

        $this->exportObjectXML( "", $xml, "countries", false, "country");

        return ["success" => "true"];


    }

    public function downloadUsefulDataProvince($ArrayVariable)
    {


        //set variables
        $languageCode = $this->K_COMMON->getVarArray($ArrayVariable, "languageCode");
        $iso = $this->K_COMMON->getVarArray($ArrayVariable, "iso");
        $cond = [];

        //get dictionary table
        $table = $this->K_COMMON->getPluginDataObject("k_usefuldata", "province");
        if ($iso != "")
            $cond["iso"] = $iso;
        if ($languageCode != "")
            $cond["language"] = $languageCode;
        $table->get("", $cond, "", "", ["iso", "code", "language"]);

        $xml = new \k_common\objects\general\xml\obj('<provinces/>');
        $lastCode = "";
        $lastCodeIso = "";
        while ($data = $table->fetch()) {

            $iso = $data->iso;
            $code = $data->code;
            $province = $data->province;
            $region = $data->region;
            $language = $data->language;

            if ($lastCodeIso <> $iso || $lastCode <> $code) {

                $xmlTagCF = $xml->addChild("province");
                $xmlTagCF->addChild("iso", $iso);
                $xmlTagCF->addChild("code", $code);
                $xmlTagLanguages = $xmlTagCF->addChild("languages");

            }

            //Add to XML
            $xmlTagLanguage = $xmlTagLanguages->addChild($language);
            $xmlTagName = $xmlTagLanguage->addChild("province");
            $xmlTagName->addCData($province);
            $xmlTagPName = $xmlTagLanguage->addChild("region");
            $xmlTagPName->addCData($region);


            //set previous variables
            $lastCodeIso = $iso;
            $lastCode = $code;

        }

        $this->exportObjectXML( "", $xml, "countries", false, "province");

        return ["success" => "true"];


    }

    public function downloadUsefulDataCity($ArrayVariable)
    {


        //set variables
        $languageCode = $this->K_COMMON->getVarArray($ArrayVariable, "languageCode");
        $iso = $this->K_COMMON->getVarArray($ArrayVariable, "iso");
        $cond = [];

        //get dictionary table
        $table = $this->K_COMMON->getPluginDataObject("k_usefuldata", "city");
        if ($iso != "")
            $cond["iso"] = $iso;
        if ($languageCode != "")
            $cond["language"] = $languageCode;
        $table->get("", $cond, "", "", ["iso", "zip_code"]);

        $xml = new \k_common\objects\general\xml\obj('<cities/>');

        while ($data = $table->fetch()) {

            $iso = $data->iso;
            $city = $data->city;
            $zip_code = $data->zip_code;
            $province = $data->province;
            $region = $data->region;
            $language = $data->language;

            $xmlTagCF = $xml->addChild("city");
            $xmlTagCF->addChild("iso", $iso);
            $xmlTagCF->addChild("name", $city);
            $xmlTagCF->addChild("zip_code", $zip_code);
            $xmlTagCF->addChild("province", $province);
            $xmlTagCF->addChild("region", $region);
            $xmlTagCF->addChild("language", $language);


        }

        $this->exportObjectXML( "", $xml, "countries", false, "city");

        return ["success" => "true"];


    }

    public function downloadUsefulDataLanguages($ArrayVariable)
    {


        //set variables
        $languageCode = $this->K_COMMON->getVarArray($ArrayVariable, "languageCode");
        $iso = $this->K_COMMON->getVarArray($ArrayVariable, "iso");
        $cond = [];

        //get dictionary table
        $table = $this->K_COMMON->getPluginDataObject("k_usefuldata", "languages");
        if ($iso != "")
            $cond["iso"] = $iso;
        if ($languageCode != "")
            $cond["language"] = $languageCode;
        $table->get("", $cond, "", "", ["iso", "language"]);

        $xml = new \k_common\objects\general\xml\obj('<languages/>');

        while ($data = $table->fetch()) {

            $iso = $data->iso;
            $name = $data->name;
            $language = $data->language;

            $xmlTagCF = $xml->addChild("language");
            $xmlTagCF->addChild("iso", $iso);
            $xmlTagCF->addChild("name", $name);
            $xmlTagCF->addChild("language", $language);


        }

        $this->exportObjectXML("", $xml, "languages", false, "language");

        return ["success" => "true"];


    }

    /*********************************************************/
    /************** PROCEDURES BUILD PACKAGE      ************/
    /*********************************************************/

    public function buildPackage($ArrayVariable = [])
    {

        //set variable
        $createZIP = $this->K_COMMON->getVarArray($ArrayVariable, "createZIP");
        $rebuild = $this->K_COMMON->getVarArray($ArrayVariable, "rebuild");


        //Empty dirs definition
        $this->emptyDefinitionDir("authorization");
        $this->emptyDefinitionDir("parameters");
        $this->emptyDefinitionDir("messages");
        $this->emptyDefinitionDir("actions");
        $this->emptyDefinitionDir("sp");
        $this->emptyDefinitionDir("template");
        $this->emptyDefinitionDir("event");
        $this->emptyDefinitionDir("dictionary");
        $this->emptyDefinitionDir("panel");
        $this->emptyDefinitionDir("custom");
        $this->emptyDefinitionDir("webobject");
        $this->emptyDefinitionDir("webtemplate");
        $this->emptyDefinitionDir("paneltemplate");
        $this->emptyDefinitionDir("webpage");
        $this->emptyDefinitionDir("customfields");


        $this->emptyDefinitionDataDir();


        switch ($this -> package) {
            case "kplugin":
            case "kcore":


                //download Data Definition Table
                $res = $this->downloadDataDefinition($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;


                //download config.xml
                $res = $this->downloadPluginConfig($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;


                //download Dictionary
                $res = $this->downloadDictionary($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download Mailing Messages
                $res = $this->downloadMailingMessages($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download Mailing Actions
                $res = $this->downloadMailingActions($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download web template
                $res = $this->downloadWebTemplate(["templateType" => "panel"]);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download web objects
                $res = $this->downloadWebObjects($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download auth
                $res = $this->downloadAuthorization($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download panel menu
                $res = $this->downloadPanelMenu($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download panel dashboard
                $res = $this->downloadPanelDashboard($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download parameters
                $res = $this->downloadParameters($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download Stored Procedures
                $res = $this->downloadSP($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download triggers
                $res = $this->downloadTrigger($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download Events
                $res = $this->downloadEvents($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download custom fields
                $res = $this->downloadCustomFields($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download custom
                $res = $this->downloadCustom($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                break;
            case "ktemplate":

                //download Data Definition Table
                $res = $this->downloadDataDefinition($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;


                //download config.xml
                $res = $this->downloadPluginConfig($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;


                //download Dictionary
                $res = $this->downloadDictionary($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download Mailing Messages
                $res = $this->downloadMailingMessages($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download Mailing Actions
                $res = $this->downloadMailingActions($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download web template
                $res = $this->downloadWebTemplate(["templateType" => "web"]);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download web pages
                $res = $this->downloadWebPages($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download web objects
                $res = $this->downloadWebObjects($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download auth
                $res = $this->downloadAuthorization($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download panel menu
                $res = $this->downloadPanelMenu($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download panel dashboard
                $res = $this->downloadPanelDashboard($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download parameters
                $res = $this->downloadParameters($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download Stored Procedures
                $res = $this->downloadSP($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download triggers
                $res = $this->downloadTrigger($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download Events
                $res = $this->downloadEvents($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;


                //download custom fields
                $res = $this->downloadCustomFields($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;

                //download custom
                $res = $this->downloadCustom($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;


                //Insert in local directory files of template
                $webTemplate = $this->K_COMMON->getPluginObject("k_webmanager", "web_templates");
                $ArrayVariableFiles["pluginName"] = $this->pluginName;
                $ArrayVariableFiles["webCodeSource"] = $this->pluginWebCodeTakeData;
                $ArrayVariableFiles["webCodeTarget"] = "";

                $webTemplate->copyKTemplateMediaFiles($ArrayVariableFiles);


                break;




            case "kdata":


                //download config.xml
                $res = $this->downloadPluginConfig($ArrayVariable);
                if ($this->K_COMMON->getVarArray($res, "success") == "false")
                    return $res;


                switch ($this -> packageContent)
                {
                    case "country":

                        $language = $this->K_COMMON->getVarArray($this -> pluginParameters, "language");

                        //Country
                        $res = $this->downloadUsefulDataCountry(
                            [
                                "languageCode" => $language
                            ]
                        );
                        if ($this->K_COMMON->getVarArray($res, "success") == "false")
                            return $res;

                        //Province
                        $res = $this->downloadUsefulDataProvince(
                            [
                                "languageCode" => $language
                            ]
                        );
                        if ($this->K_COMMON->getVarArray($res, "success") == "false")
                            return $res;

                        //City
                        $res = $this->downloadUsefulDataCity(
                            [
                                "languageCode" => $language
                            ]
                        );
                        if ($this->K_COMMON->getVarArray($res, "success") == "false")
                            return $res;

                        break;

                    case "language":
                        $language = $this->K_COMMON->getVarArray($this -> pluginParameters, "language");

                        $res = $this->downloadUsefulDataLanguages(
                            [
                                "languageCode" => $language
                            ]
                        );

                        if ($this->K_COMMON->getVarArray($res, "success") == "false")
                            return $res;

                        break;
                    case "dictionary":

                        $language = $this->K_COMMON->getVarArray($this -> pluginParameters, "language");

                        $res = $this->downloadDictionary(
                            [
                                "pluginName" => $this->pluginRef
                                ,"languageCode" => $language
                            ]
                        );
                        if ($this->K_COMMON->getVarArray($res, "success") == "false")
                            return $res;

                        break;
                    case "plugindata":


                        $relateddata = $this->K_COMMON->getVarArray($this -> pluginParameters, "relateddata");

                        $dirDataDefault = "/definition/datadefault";

                        $this->downloadData(
                            [
                                "dir" => $dirDataDefault
                                ,"pluginName" => $this->pluginRef
                            ]
                        );

                        //check if need download related data and download
                        if ($relateddata == 1) {
                            $this->downloadRelatedData(
                                [
                                    "dir" => $dirDataDefault
                                    ,"pluginName" => $this->pluginRef
                                ]
                            );
                        }

                        break;
                }
                break;

        }





        $res = ["success" => "true"];
        if ($createZIP == "1")
        {
            $resZIP = $this->createZipFile(["fileName" => $this->pluginName . '.zip']);
            $res["filePath"] = $resZIP["filePath"];
            $res["fileName"] = $resZIP["fileName"];
        }


        if ($rebuild == "1")
        {

            $pluginDir = $this->K_COMMON->getPathPluginObject(true, $this->pluginName, "");
            $this->K_COMMON->emptyDir(["dir" => $pluginDir]);
            $this->K_COMMON->mkDir(["dir" => $pluginDir, "mask" => 0777]);
            $this->K_COMMON->copyDir($this->dirDownload, $pluginDir);

        }


        return $res;




    }

    public function downloadPackage($ArrayVariable)
    {


        //set variable
        $pluginName = $this->K_COMMON->getVarArray($ArrayVariable, "pluginName");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");

        //set object
        $objDownload = $this->getObject("builder");


        //initialize
        $objDownload->initialize(
            [
                "pluginName" => $pluginName,
                "webCodeTakeData" => $webCode

            ]
        );


        $res = $objDownload -> buildPackage(["createZIP" => "1"]);

        return ["success" => "true", "content" => "", "type" => "zip", "filePath" => $res["filePath"], "fileName" => $res["fileName"]];


    }

    public function rebuildPackage($ArrayVariable)
    {

        //set variable
        $pluginName = $this->K_COMMON->getVarArray($ArrayVariable, "pluginName");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");


        //initialize
        $this->initialize(
            [
                "pluginName" => $pluginName,
                "webCodeTakeData" => $webCode

            ]
        );

        $this -> buildPackage(["rebuild" => "1"]);

        return ["success" => "true", "message" => "Package Rebuilded!"];
    }


}

?>