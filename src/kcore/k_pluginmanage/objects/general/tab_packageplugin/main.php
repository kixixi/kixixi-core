<?
namespace k_pluginmanage\objects\general\tab_packageplugin;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    var $ver = "1.0";

    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";
    
    var $dictionary;
    var $dicLanguages;
    var $packageContentType = "";
    var $packageKeyIDPlg = "";
    var $pluginInfo = [];

    /**
     * actual url of panel
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $urlActual = "";

    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
    }

    public function get($ArrayVariable = array())
    {

        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");
        $this->urlActual = $this->K_COMMON->getVarArray($ArrayVariable, "panelUrlActual");

        switch ($pluginMethod) {
            case "":
            case "list":
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //switch request
        switch ($KCA) {
            case "saveData" :
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "downloadPackage" :
                return $this->downloadPackage($ArrayVariable);
                break;
            case "rebuildPackage" :
                return json_encode($this->rebuildPackage($ArrayVariable));
                break;
            case "delete" :
                return json_encode($this->delete($ArrayVariable));
                break;


        }
    }

    protected function getPagePut($ArrayVariable = array())
    {
        //include obj
        $objPlugins = $this->K_COMMON->getPluginObject("k_common", "plugins");

        //set variables
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");
        $pluginParam = $this->K_COMMON->getVarArray($ArrayVariable, "pluginParam");
        $extraParam = $this->K_COMMON->getVarArray($ArrayVariable, "urlExtraParam");
        $objName = $this->K_COMMON->getVarArray($extraParam, "objName");
        $objType = $this->K_COMMON->getVarArray($extraParam, "objType");

        if ($objName != "") {
            $objName = substr($objType, 0, strpos($objType, "_")) . "\\" . substr($objType, strpos($objType, "_") + 1) . "\\" . $objName;
        }
        $objViewer = $this->K_COMMON->getObjectDescByName($objName);
        $keyIDPlg = "";
        $plgID = $pluginParam;
        $plgType = $this->K_COMMON->getPluginType($plgID);

        //Add name on top
        $this->appendRow("");
        $colTitle = $this->appendColumn($this->lastRow, ["nCol" => 12, "style" => "text-align: center;"]);
        $this->appendRow("");
        $this->appendColumn($this->lastRow, ["nCol" => 12]);

        //get info plugin
        if ($pluginMethod == "get") {
            if ($plgID != "") {
                $this->pluginInfo = $objPlugins->getPluginInfo(["plgID" => $plgID]);

                $pluginName = $this->pluginInfo["plg_name"];
                $keyIDPlg = $this->pluginInfo["id_plugin"];
                $contentType = $this->pluginInfo["plg_content"];
                $title = $pluginName;
                $this->appendHTML($colTitle, "<H2>" . $title . "</H2>");
                if ($objName != "")
                    $this->appendHTML($colTitle, "<br><H4>" . $objViewer . "</H4>");

                if ($objName != "") {

                    $this->appendWO("", "k_htmlcomponents", "web\objects\w_breadcrumbs",
                        [
                            "crumbs" => [$pluginName => $this->urlActual . "get/" . $plgID, $objViewer => ""]
                        ]
                    );
                }

                //plugin
                $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);

            }

            $this->packageContentType = $contentType;
            $this->packageKeyIDPlg = $keyIDPlg;

        }


        //Create TABS Array
        $arrayTabs = [];
        if ($objName == "")
            $arrayTabs[] = ["id" => "tabPlugin", "label" => $this->dictionary["LabelTabPlugin"], "active" => "1"];

        if ($keyIDPlg != "") {
            if ($objName == "")
            {
                $arrayTabs[] = ["id" => "tabPackage", "label" => $this->dictionary["LabelTabPackage"]];
                $arrayTabs[] = ["id" => "tabObjects", "label" => $this->dictionary["LabelTabObjects"]];
                $arrayTabs[] = ["id" => "tabParameters", "label" => $this->dictionary["LabelTabParameters"]];


            }
            $arrayTabs[] = ["id" => "tabDictionary", "label" => $this->dictionary["LabelTabDictionary"]];
            $arrayTabs[] = ["id" => "tabMessage", "label" => $this->dictionary["LabelTabMessage"]];
            $arrayTabs[] = ["id" => "tabAction", "label" => $this->dictionary["LabelTabAction"]];
            if ($objName != "")
                $arrayTabs[] = ["id" => "tabWebObject", "label" => $this->dictionary["LabelTabWebObject"], "active" => "1"];
            if ($objName == "")
                $arrayTabs[] = ["id" => "tabAuth", "label" => $this->dictionary["LabelTabAuth"]];
            if ($objName == "")
                $arrayTabs[] = ["id" => "tabPanel", "label" => $this->dictionary["LabelTabPanel"]];
            $arrayTabs[] = ["id" => "tabDashboard", "label" => $this->dictionary["LabelTabDashboard"]];
            if ($objName == "" && $plgType == "ktemplate")
                $arrayTabs[] = ["id" => "tabWebTemplates", "label" => $this->dictionary["LabelTabWebTemplates"]];
            if ($objName != "")
                $arrayTabs[] = ["id" => "tabPanelTemplates", "label" => $this->dictionary["LabelTabPanelTemplates"]];
            if ($objName != "")
                $arrayTabs[] = ["id" => "tabCustomization", "label" => $this->dictionary["LabelTabCustomization"]];
            if ($objName == "") {
                $arrayTabs[] = ["id" => "tabProceduresAndFunctions", "label" => $this->dictionary["LabelTabProcAndFunc"]];
                $arrayTabs[] = ["id" => "tabTriggers", "label" => $this->dictionary["LabelTabTriggers"]];
                $arrayTabs[] = ["id" => "tabEvents", "label" => $this->dictionary["LabelTabEvents"]];
                $arrayTabs[] = ["id" => "tabTables", "label" => $this->dictionary["LabelTabTables"]];
                $arrayTabs[] = ["id" => "tabQueries", "label" => $this->dictionary["LabelTabQueries"]];
            }
        } else {
        }

        //create tabs
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendTab($this->lastCol,
            [
                "id" => "tabPluginManage"
                , "tabs" => $arrayTabs
            ]
        );


        //TODO: MDP - Ho rimosso il webCode perchè in questo caso stiamo lavorando su dictionary generici dei panel. Verificare che sia tutto ok

        $arrayParam =
            [
                "keyIDPlg" => $keyIDPlg,
                "plgID" => $plgID,
                "pluginObject" => $objName,
                "webCode" => '', //$this->K_COMMON->getEditingWeb(),
                "extraParam" => $extraParam
            ];
        if ($objName == "") {
            //plugin
            $this->getPluginForm($arrayParam);
        }
        if ($keyIDPlg != "") {
            // dictionary
            $objDictionary = $this->getObject("tab_dictionary")->get($arrayParam);
            $this->appendObject("tabDictionary", $objDictionary);

            //action
            $objActions = $this->getObject("tab_actions")->get($arrayParam);
            $this->appendObject("tabAction", $objActions);

            //messages
            $objMessage = $this->getObject("tab_messages")->get($arrayParam);
            $this->appendObject("tabMessage", $objMessage);

            if ($objName != "") {
                //web object
                $objWebObjects = $this->getObject("tab_webobjects")->get($arrayParam);
                $this->appendObject("tabWebObject", $objWebObjects);
            } else {
                $objObjects = $this->getObject("tab_objects")->get($arrayParam);
                $this->appendObject("tabObjects", $objObjects);

                //Package
                $arrayParamContent = $arrayParam;
                $arrayParamContent["keyIDPlg"] = $this->packageKeyIDPlg;
                $arrayParamContent["contentType"] = $this->packageContentType;
                $objPackage = $this->getObject("tab_packagebuild")->get($arrayParamContent);
                $this->appendObject("tabPackage", $objPackage);

                //Auth
                $objAuth = $this->getObject("tab_authorization")->get($arrayParam);
                $this->appendObject("tabAuth", $objAuth);

                //Panel
                $objPanel = $this->getObject("tab_panel")->get($arrayParam);
                $this->appendObject("tabPanel", $objPanel);
            }





            //Dashboard
            $objDashboard = $this->getObject("tab_dashboard")->get($arrayParam);
            $this->appendObject("tabDashboard", $objDashboard);

            //Tab Web Templates
            if ($objName == "" && $plgType == "ktemplate")
            {
                $objTemplate = $this->getObject("tab_templates")->get(array_merge($arrayParam,["tplType" => "web"]));
                $this->appendObject("tabWebTemplates", $objTemplate);
            }


            //Tab Panel Templates
            if ($objName != "")
            {
                $objTemplate = $this->getObject("tab_templates")->get(array_merge($arrayParam,["tplType" => "panel"]));
                $this->appendObject("tabPanelTemplates", $objTemplate);
            }



            if ($objName == "") {
                //tabParameters
                $objTemplate = $this->getObject("tab_parameters")->get($arrayParam);
                $this->appendObject("tabParameters", $objTemplate);
                //tabProceduresAndFunctions
                $objTemplate = $this->getObject("tab_proceduresandfunctions")->get($arrayParam);
                $this->appendObject("tabProceduresAndFunctions", $objTemplate);//tabProceduresAndFunctions
                //tabTriggers
                $objTemplate = $this->getObject("tab_triggers")->get($arrayParam);
                $this->appendObject("tabTriggers", $objTemplate);
                //tabEvents
                $objTemplate = $this->getObject("tab_events")->get($arrayParam);
                $this->appendObject("tabEvents", $objTemplate);
                //tabEvents
                $objTemplate = $this->getObject("tab_tables")->get($arrayParam);
                $this->appendObject("tabTables", $objTemplate);
                //tabQueries
                $objTemplate = $this->getObject("tab_queries")->get($arrayParam);
                $this->appendObject("tabQueries", $objTemplate);
            }

            //customization
            if ($objName != "") {
                $objWebObjects = $this->getObject("tab_customization")->get($arrayParam);
                $this->appendObject("tabCustomization", $objWebObjects);
            }


        } else {
        }
    }

    protected function getPluginForm($ArrayVariable)
    {


        //include object
        $objPlugins = $this->K_COMMON->getPluginObject("k_common", "plugins");

        //set variable
        $extraParam = $this->K_COMMON->getVarArray($ArrayVariable, "extraParam");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $keyIDPlg = $this->K_COMMON->getVarArray($ArrayVariable, "keyIDPlg");
        $plgType = strtolower($this->K_COMMON->getVarArray($extraParam, "type"));
        $optionPlgType = ["kcore" => "Core", "kplugin" => "Plugin", "ktemplate" => "Template"];
        $plgName = "";
        $plgVer = "1.0.0";
        $plgDescr = "";
        $plgAuthors = "";
        $plgRelated = "";
        $plgLang = "";
        $disabled = "0";
        $plgIDName = "";


        if ($plgID != "" && $plgID != "0") {
            //get info plugin
            $plgName = $this->pluginInfo["plg_name"];
            $plgIDName = $this->pluginInfo["plg_id_name"];
            $plgType = $this->pluginInfo["plg_type"];
            $plgVer = $this->pluginInfo["plg_ver"];
            $plgDescr = $this->pluginInfo["plg_description"];
            $plgAuthors = $this->pluginInfo["plg_authors"];
            $plgRelated = $this->pluginInfo["plg_related"]; //plugin and template related
            $plgLang = $this->pluginInfo["plg_language"];
            $disabled = "1";
        }



        //create form
        $this->appendForm("tabPlugin",
            [
                "id" => "pluginForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "reloadPage" => "1"
                , "reloadPageCheckCode" => "keyIDPlg"
                , "reloadPageGetCode" => "plgID"
            ]
        );


        //NEW ROW
        $this->appendRow($this->lastForm);

        $name = "keyIDPlg";
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $keyIDPlg]);

        $name = "plgType";
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $plgType]);

        //plugin id
        if ($keyIDPlg != "") { //if edit create input hidden
            //keyID hidden
            $name = "plgID";
            $this->appendInput($this->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $plgIDName]);

/*
            //plugin type
            $label = $this->dictionary["LabelPluginType"];
            $name = "";
            $this->appendColumn($this->lastRow, ["nCol" => 3]);
            $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $optionPlgType, "labelInput" => $label, "id" => $name, "name" => $name, "value" => $plgType, "required" => "1", "disabled" => $disabled]);
*/

            //plugin id
            $label = $this->dictionary["LabelPluginID"];
            $name = "";
            $this->appendColumn($this->lastRow, ["nCol" => 4]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $plgIDName, "required" => "1", "disabled" => $disabled]);
        } else {


            /*
            //plugin type
            $label = $this->dictionary["LabelPluginType"];
            $name = "plgType";
            $this->appendColumn($this->lastRow, ["nCol" => 3]);
            $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $optionPlgType, "labelInput" => $label, "id" => $name, "name" => $name, "value" => $plgType, "required" => "1", "disabled" => $disabled]);
            */

            //plugin id
            $label = $this->dictionary["LabelPluginID"];
            $name = "plgID";
            $textHelp = $this->dictionary["plgIDTextHelp"];
            $labelPlaceHolder = "Ex: articles";
            $this->appendColumn($this->lastRow, ["nCol" => 4]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $plgIDName, "required" => "1", "disabled" => $disabled, "textHelp" => $textHelp, "labelPlaceholder" => $labelPlaceHolder, "attributes" => ' pattern="[a-z0-9]{1,}"']);
        }
        //NEW ROW
        $this->appendRow($this->lastForm);

        //plugin name
        $label = $this->dictionary["LabelPluginVer"];
        $name = "plgVer";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $plgVer]);

        //plugin language
        $label = $this->dictionary["LabelPluginLng"];
        $name = "plgLang";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_combolanguages", ["labelInput" => $label, "name" => $name, "value" => $plgLang]);


        //NEW ROW
        $this->appendRow($this->lastForm);

        //plugin name
        $label = $this->dictionary["LabelPluginName"];
        $name = "plgName";
        $this->appendColumn($this->lastRow, ["nCol" => 7]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $plgName, "attributes" => ' pattern="[a-Z0-9]{1,}"']);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //plugin name
        $label = $this->dictionary["LabelPluginDescr"];
        $name = "plgDescr";
        $this->appendColumn($this->lastRow, ["nCol" => 7]);
        $this->appendInput($this->lastCol, ["type" => "textarea", "labelInput" => $label, "id" =>
            $name, "name" => $name, "value" => $plgDescr]);

        //Heading Authors
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->dictionary["LabelAuthorsTitle"], "importance" => "5"]);
        $this->appendHTML($this->lastCol, "<small><center>" . $this->dictionary["LabelAuthorsSubTitle"] . "</center></small><br>");


        //New ROW AUTHORS

        $plgAuthorsVal = $this->K_COMMON->getVarArray(json_decode($plgAuthors, true), "author");
        $name = "plgAuthors";
        $this->appendListGroup($this->lastForm,
            [
                "id" => $name
                , "attributes" => 'style="height:176; overflow: scroll; overflow-x: hidden;"'
                , "values" => $plgAuthorsVal
                , "addingValues" =>
                [
                    "valuesHName" => "authors"
                    , "fields" => [
                    ["type" => "text", "labelInput" => $this->dictionary["LabelAuthorName"], "name" => "name"]
                    , ["type" => "text", "labelInput" => $this->dictionary["LabelAuthorEmail"], "name" => "email"]
                    , ["type" => "text", "labelInput" => $this->dictionary["LabelAuthorWebSite"], "name" => "www"]
                ]
                ]
            ]
        );

        //Heading Plugin related
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->dictionary["LabelPlgRelatedTitle"], "importance" => "5"]);
        $this->appendHTML($this->lastCol, "<small><center>" . $this->dictionary["LabelPlgRelatedSubTitle"] . "</center></small><br>");

        //load kplugin
        $pluginOptions = $objPlugins->listPlugins(["KPlugin" => "1"]);
        $plgAuthorsKplugin = $this->K_COMMON->getVarArray(json_decode($plgRelated, true), "kplugin");
        $name = "plgRelatedKplugin";
        $this->appendListGroup($this->lastForm,
            [
                "id" => $name
                , "attributes" => 'style="height:176; overflow: scroll; overflow-x: hidden;"'
                , "values" => $plgAuthorsKplugin
                , "addingValues" =>
                [
                    "valuesHName" => "relatedKplugin"
                    , "fields" => [
                    ["type" => "select", "typeLoad" => "array", "options" => $pluginOptions, "labelInput" => $this->dictionary["LabelKpluginID"], "name" => "id"]
                    , ["type" => "text", "labelInput" => $this->dictionary["LabelPluginVer"], "name" => "minver"]
                ]
                ]
            ]
        );

        //Heading Template related
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->dictionary["LabelTplRelatedTitle"], "importance" => "5"]);
        $this->appendHTML($this->lastCol, "<small><center>" . $this->dictionary["LabelTplRelatedSubTitle"] . "</center></small><br>");

        //load ktemplate
        $templateOptions = $objPlugins->listPlugins(["KTemplate" => "1"]);
        $plgRelatedKtemplate = $this->K_COMMON->getVarArray(json_decode($plgRelated, true), "ktemplate");
        $name = "plgRelatedKtemplate";
        $this->appendListGroup($this->lastForm,
            [
                "id" => $name
                , "attributes" => 'style="height:176; overflow: scroll; overflow-x: hidden;"'
                , "values" => $plgRelatedKtemplate
                , "addingValues" =>
                [
                    "valuesHName" => "relatedktemplate"
                    , "fields" => [
                    ["type" => "select", "typeLoad" => "array", "options" => $templateOptions, "labelInput" => $this->dictionary["LabelKtemplateID"], "name" => "id"]
                    , ["type" => "text", "labelInput" => $this->dictionary["LabelPluginVer"], "name" => "minver"]
                ]
                ]
            ]
        );

        $this->appendRow($this->lastForm);
        //button
        $label = $this->dictionary["LabelSave"];
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);


    }

    protected function saveData($ArrayVariable)
    {

        return $this->buildPlugin($ArrayVariable);
    }

    public function buildPlugin($ArrayVariable)
    {


        //include table
        $tablePlugins = $this->K_COMMON->getPluginDataObject("k_common", "plugins");

        //set variable for plugin tab
        $keyIDPlg = $this->K_COMMON->getVarArray($ArrayVariable, "keyIDPlg");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $plgType = $this->K_COMMON->getVarArray($ArrayVariable, "plgType");
        $plgName = $this->K_COMMON->getVarArray($ArrayVariable, "plgName");
        $plgVer = $this->K_COMMON->getVarArray($ArrayVariable, "plgVer");
        $plgLang = $this->K_COMMON->getVarArray($ArrayVariable, "plgLang");
        $plgDescr = $this->K_COMMON->getVarArray($ArrayVariable, "plgDescr");
        $authors = $this->K_COMMON->getVarArray($ArrayVariable, "authors");
        $relatedKplugin = $this->K_COMMON->getVarArray($ArrayVariable, "relatedKplugin");
        $relatedktemplate = $this->K_COMMON->getVarArray($ArrayVariable, "relatedktemplate");
        if ($authors == "") $authors = "{}";
        if ($relatedKplugin == "") $relatedKplugin = "{}";
        if ($relatedktemplate == "") $relatedktemplate = "{}";
        $plgRelated = '{"kplugin":' . $relatedKplugin . ',"ktemplate":' . $relatedktemplate . '}';
        $authors = '{"author":' . $authors . '}';
        $plgRoot = $this->K_COMMON->getPluginRootByType($plgType);
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode"); //used only for ktemplate

        //check variable
        if (strpos($plgName, "_") != "") {
            return ["success" => "false", "reason" => "pluginNameError"];
        }

        if ($plgID == "")
            return ["success" => "false", "reason" => "plgIDNull"];

        if (strpos($plgID, "_") !== false)
            return ["success" => "false", "message" => $this->getDictionaryWord("msgErrorSaveCharNotAllowed"), "reason" => "charNotAllowed"];

        $plgID = $plgRoot . $plgID;
        if ($plgType == "kcore") $dir = $this->K_COMMON->getPathKCore();
        elseif ($plgType == "kplugin") $dir = $this->K_COMMON->getPathKPlugin();
        elseif ($plgType == "ktemplate") $dir = $this->K_COMMON->getPathKTemplate();
        else
            return ["success" => "false", "reason" => "plgTypeError"];

        if ($keyIDPlg != "") {
            $tablePlugins->get(["id_plugin"], ["plg_id" => $plgID]);
            $data = $tablePlugins->fetch();
            if ($data) {
                if ($data->id_plugin != $keyIDPlg)
                    return ["success" => "false", "reason" => "errorSecurity"];
            }
        } else {
            //verify if plugin exist on db
            $tablePlugins->get(["plg_id"], ["plg_type" => $plgType, "plg_id" => $plgID]);
            $data = $tablePlugins->fetch();
            if ($data)
                return ["success" => "false", "reason" => "plginAlreadyExist", "message" => $this->dictionary["emsgPluginExist"]];

            // verify if plugin exist on dir
            if ($dir != "") {
                $dir .= "/" . $plgID;
                if (is_dir($dir) === true)
                    return ["success" => "false", "reason" => "dirAlreadyExist", "message" => $this->dictionary["emgDirPluginExist"]];
            }
        }

        //save data plugins
        $arrayPutPlugins = [
            "plg_id" => $plgID
            , "plg_type" => $plgType
            , "plg_name" => $plgName
            , "plg_ver" => $plgVer
            , "plg_language" => $plgLang
            , "plg_description" => $plgDescr
            , "plg_authors" => $authors
            , "plg_related" => $plgRelated
        ];
        $tablePlugins->putByKey($arrayPutPlugins);
        $keyID = $tablePlugins->getNewID();

        //Create Plugin Dir
        $this->K_COMMON->mkDir(["dir" => $dir, "mask" => 0777]);

        //If new ktemplate, create first template
        if ($plgType == "ktemplate" && $keyIDPlg == ""){
            $objWebTemplate = $this->K_COMMON->getPluginObject("k_webmanager","web_templates");

            $objWebTemplate->saveTplWeb([
                "pluginName" => $plgID
                , "pluginObject" => "web\\templates\\default"
                , "name" => "Home"
                , "code" => "HOME"
                , "webCode" => $webCode
                , "tplType" => "web"
            ]);

        }

        return array("success" => "true", "message" => $this->dictionary["msgSuccessSave"], "plgID" => $plgID, "keyID" => $keyID);
    }

    function getPageList($ArrayVariable)
    {

        //set variables
        $pluginParam = $this->K_COMMON->getVarArray($ArrayVariable, "pluginParam");
        $urlActual = $this->K_COMMON->getVarArray($ArrayVariable, "panelUrlActual");
        $plgType = $this->K_COMMON->getVarArray($ArrayVariable, "plgType");

        $searchText = $pluginParam;

        //add files to include
        $this->addHeadFile([$this->getPathFileJS("list")]);

        //NEW ROW
        $this->appendRow("");

        //get Table List
        $objTableList = $this->getTableList($ArrayVariable);
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendObject("", $objTableList);

        //NEW ROW
        $this->appendRow("");

        //button ADD New Plugin
        $label = $this->dictionary["LabelAddPlugin"];
        $name = "btnAdd";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btn", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success", "link" => $urlActual . "put/0/type/" . strtolower($plgType)]);

        return $this;

    }

    protected function getTableList($ArrayVariable = array())
    {

          //set variables
        $plgType = $this->K_COMMON->getVarArray($ArrayVariable, "plgType");

        //set object
        $onlyRow = "";
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        //include obj
        $objApiUsefulData = $this->K_COMMON->getPluginObject("k_usefuldata", "api");

        $listPlugin = $objApiUsefulData->listPluginsAndObjects(["pluginType" => [$plgType => "1"]]);

        $dataTable = $listPlugin;
        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableList"
                , "tableID" => "tableList"
                , "hideHead" => $onlyRow
                , "data" => $dataTable
                , "fields" =>
                [
                    [
                        "label" => $this->dictionary["LabelPluginName"]
                        , "qryField" => "text"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->dictionary["LabelEdit"]
                                    , "action" => "link"
                                    , "class" => "fa fa-edit"
                                    , "qryAttributes" => ["get" => "value"]
                                    , "urlBaseType" => "panelActual"
                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelRebuild")
                                    , "action" => "modalConfirm"
                                    , "icon" => "fa-save"
                                    , "qryAttributes" => [
                                    "pluginName" => "value"
                                ]
                                    , "ajax" => [
                                    "kcp" => $this->getPluginName()
                                    , "kco" => $this->getPluginObjectName()
                                    , "kca" => "rebuildPackage"
                                ]
                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelDownload")
                                    , "action" => "download"
                                    , "icon" => "fa-download"
                                    , "qryAttributes" => [
                                    "pluginName" => "value"
                                ]
                                    , "ajax" => [
                                    "kcp" => $this->getPluginName()
                                    , "kco" => $this->getPluginObjectName()
                                    , "kca" => "downloadPackage"
                                ]
                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelDelete"]
                                    , "action" => "remove"
                                    , "qryAttributes" => ["keyID" => "value"]
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                                ]
                            ]
                    ]
                ]
            ]
        );
        return $OBJTableList->get();

    }

    protected function delete($ArrayVariable)
    {

        //set variables
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");


        if ($plgID == "")
            return array("success" => "false");

        $objPlugins = $this->K_COMMON->getPluginObject("k_common", "plugins");
        $objPlugins->deletePlugin(["plgID" => $plgID]);

        return array("success" => "true");
    }

    public function downloadPackage($ArrayVariable)
    {

        //set object
        $objDownload = $this->getObject("builder");
        $ArrayVariable["webCode"] = $this->K_COMMON->getEditingWeb();

        return $objDownload -> downloadPackage($ArrayVariable);



    }

    public function rebuildPackage($ArrayVariable)
    {

        //set object
        $objDownload = $this->getObject("builder");
        $ArrayVariable["webCode"] = $this->K_COMMON->getEditingWeb();

        return $objDownload -> rebuildPackage($ArrayVariable);


    }

}

?>