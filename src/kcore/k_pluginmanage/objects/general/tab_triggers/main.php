<?
/**
 * PLUGIN MANAGE (triggers)
 *
 *
 * @paramor Marco Del Principe <m.delprincipe73@gmail.com>
 * @paramor Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_pluginmanage
 * @subpackage objects\triggers
 * @since 1.00
 */
namespace k_pluginmanage\objects\general\tab_triggers;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";


    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "saveData":
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "getModalAdd":
                return json_encode($this->getModalAdd($ArrayVariable));
                break;
            case "delete":
                return json_encode($this->delete($ArrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableList($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
        }
    }

    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "put", "get")
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {

        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
                $this->getPageList($ArrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    protected function getPageList($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");

        $listMyMList = $this->getTableList($ArrayVariable);
        $this->appendHTML("", $listMyMList);

        $this->appendInput("", ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "btnAddTrigger", "value" => $this->getDictionaryWord("LabelAdd")
            , "openModal" => [
                "idModal" => "modalAddTrigger"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAdd"
                , "parameters" => ["plgID" => $plgID, "pluginObject" => $pluginObject]
            ]
        ]);

    }

    protected function getTableList($ArrayVariable)
    {

        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");
        $searchSPName = $plgID;
        if ($pluginObject != "")
            $searchSPName .= "_" . $pluginObject;

        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        if ($onlyRow == "1") {
            $count = 0;
            $dataTable = "";
            //Get All Plugin Trigger
            $this->kxdb->loadTriggerList($plgID);

            while ($dataList = $this->kxdb->fetch()) {
                $name = $dataList->TRIGGER_NAME;
                $table = $dataList->EVENT_OBJECT_TABLE;
                $timing = $dataList->ACTION_TIMING;
                $manipulation = $dataList->EVENT_MANIPULATION;
                $dataTable[$count]["name"] = $name;
                $dataTable[$count]["timing"] = $timing;
                $dataTable[$count]["manipulation"] = $manipulation;
                $dataTable[$count]["tableName"] = $table;
                $dataTable[$count]["plgID"] = $plgID;
                $dataTable[$count]["pluginObject"] = $pluginObject;
                $count++;
            }

        } else
            $dataTable = "";

        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableListTriggers"
                , "hideHead" => $onlyRow
                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getTableList"]
                , "data" => $dataTable
                , "fVar" => ["plgID" => $plgID, "pluginObject" => $pluginObject]

                , "fields" =>
                [
                    [
                        "label" => $this->getDictionaryWord("LabelListName")
                        , "qryField" => "name"
                    ]
                    , [
                    "label" => $this->getDictionaryWord("LabelListTableName")
                    , "qryField" => "tableName"
                ]
                    , [
                    "label" => $this->getDictionaryWord("LabelListTableTiming")
                    , "qryField" => "timing"
                ]
                    , [
                    "label" => $this->getDictionaryWord("LabelListTableManip")
                    , "qryField" => "manipulation"
                ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->getDictionaryWord("LabelListEdit")
                                    , "action" => "editModal"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                    , "qryAttributes" => [
                                    "triggerName" => "name"
                                    , "tableName" => "tableName"
                                    , "plgID" => "plgID"
                                    , "pluginObject" => "pluginObject"

                                ]
                                    , "staticAttributes" => ["edit" => "1"]
                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelListDelete")
                                    , "qryAttributes" => [
                                    "triggerName" => "name"
                                    , "tableName" => "tableName"
                                    , "plgID" => "plgID"
                                    , "pluginObject" => "pluginObject"
                                ]
                                    , "action" => "remove"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                                ]
                            ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();

    }

    protected function getModalAdd($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->getDictionaryWord("LabelModalTitleAdd"), "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePut($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getPagePut($ArrayVariable)
    {

        //set variable
        $name = $this->K_COMMON->getVarArray($ArrayVariable, "triggerName");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $edit = $this->K_COMMON->getVarArray($ArrayVariable, "edit");
        $fieldDisable = "";
        $value_name = "";
        $value_objectTable = "";
        $value_actionTiming = "";
        $value_eventManipulation = "";
        $value_actionStatement = "";

        //create form
        $this->appendForm("",
            [
                "id" => "paramForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "reloadTableList" => "tableListTriggers"
                , "closeModal" => "modalAddTrigger"


            ]
        );

        if ($edit != "") {
            //load sp data
            $triggerData = $this->kxdb->loadTriggerDefinition($name);
            $data = $triggerData->fetch();
            if ($data) {
                $value_name = $name;
                $value_objectTable = $data->EVENT_OBJECT_TABLE;
                $value_actionTiming = $data->ACTION_TIMING;
                $value_eventManipulation = $data->EVENT_MANIPULATION;
                $value_actionStatement = $data->ACTION_STATEMENT;
                $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "objectTable", "value" => $value_objectTable]);
                $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "actionTiming", "value" => $value_actionTiming]);
                $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "eventManipulation", "value" => $value_eventManipulation]);
            }
        } else {
            $value_actionStatement = "BEGIN" . chr(13) . "END";
        }

        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);

        //object
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginObject", "name" => "pluginObject", "value" => $pluginObject]);


        //trigger name
        if ($edit != "") {
            $fieldDisable = "1";

            //NEW ROW
            $this->appendRow($this->lastForm);
            $label = $this->getDictionaryWord("LabelFormLabelName");

            $name = "name";
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_name, "disabled" => $fieldDisable]);
        }

        //NEW ROW
        $this->appendRow($this->lastForm);

        //trigger object table
        $label = $this->getDictionaryWord("LabelFormLabelObjectTable");
        $name = "objectTable";
        $tableListData = $this->K_COMMON->getListPluginDBTables($plgID);

        $optionsTable = [];
        $count = 0;
        while ($row = $tableListData->fetch()) {

            $optionsTable[$count]["value"] = $row->TABLE_NAME;
            $optionsTable[$count]["text"] = $row->TABLE_NAME;
            $count++;
        }
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $optionsTable, "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_objectTable, "disabled" => $fieldDisable]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //trigger action timing
        $label = $this->getDictionaryWord("LabelFormLabelActionTiming");
        $name = "actionTiming";
        $optionsActionTiming = ["BEFORE" => "BEFORE", "AFTER" => "AFTER"];
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $optionsActionTiming, "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_actionTiming, "disabled" => $fieldDisable]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //trigger event manipulation
        $label = $this->getDictionaryWord("LabelFormLabelEventManip");
        $name = "eventManipulation";
        $optionsEventManipulation = ["INSERT" => "INSERT", "UPDATE" => "UPDATE", "DELETE" => "DELETE"];
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $optionsEventManipulation, "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_eventManipulation, "disabled" => $fieldDisable]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //trigger action statement
        $label = $this->getDictionaryWord("LabelFormLabelActionStatement");
        $name = "actionStatement";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "textarea", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_actionStatement]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->getDictionaryWord("LabelFormSave");
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;

    }

    protected function delete($ArrayVariable)
    {
        $name = $this->K_COMMON->getVarArray($ArrayVariable, "triggerName");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");

        //drop actual procedure
        $query = " DROP TRIGGER IF EXISTS  " . $name . ";";
        $this->kxdb->prepare($query);
        $this->kxdb->execute();

        return array("success" => "true");
    }

    protected function saveData($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $objectTable = $this->K_COMMON->getVarArray($ArrayVariable, "objectTable");
        $actionTiming = $this->K_COMMON->getVarArray($ArrayVariable, "actionTiming");
        $eventManipulation = $this->K_COMMON->getVarArray($ArrayVariable, "eventManipulation");
        $actionStatement = $this->K_COMMON->getVarArray($ArrayVariable, "actionStatement");
        $errorMsg = "";
        $name = strtolower($objectTable . "_" . $actionTiming . "_" . $eventManipulation);

        $query = " DROP TRIGGER IF EXISTS  " . $name . ";";
        $this->kxdb->prepare($query);
        $this->kxdb->execute();

        $query = " CREATE TRIGGER " . $name . "  " . $actionTiming . " " . $eventManipulation . " ON " . $objectTable . " FOR EACH ROW " . $actionStatement . ";";
        $this->kxdb->setTraceError();
        $this->kxdb->prepare($query);
        $this->kxdb->execute();

        $traceLog = $this->kxdb->getTraceLog();
        if (count($traceLog) > 0) {
            foreach ($traceLog as $err) {
                $errorMsg .= $err["errorStr"] . chr(10);
            }
            return ["success" => "false", "reason" => "error", "message" => $errorMsg];
        }


        return ["success" => "true"];
    }


}

?>