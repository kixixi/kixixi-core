<?
/**
 * PLUGIN MANAGE (proceduresandfunctions)
 *
 *
 * @paramor Marco Del Principe <m.delprincipe73@gmail.com>
 * @paramor Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_pluginmanage
 * @subpackage objects\proceduresandfunctions
 * @since 1.00
 */
namespace k_pluginmanage\objects\general\tab_proceduresandfunctions;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";


    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "saveData":
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "getModalAdd":
                return json_encode($this->getModalAdd($ArrayVariable));
                break;
            case "delete":
                return json_encode($this->delete($ArrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableList($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
        }
    }

    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "put", "get")
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {

        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
                $this->getPageList($ArrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    protected function getPageList($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");

        $listMyMList = $this->getTableList($ArrayVariable);
        $this->appendHTML("", $listMyMList);

        $this->appendInput("", ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "btnAddFuncAndPar", "value" => $this->getDictionaryWord("LabelAdd")
            , "openModal" => [
                "idModal" => "modalAddProceduresAndFunctions"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAdd"
                , "parameters" => ["plgID" => $plgID, "pluginObject" => $pluginObject]
            ]
        ]);

    }

    protected function getTableList($ArrayVariable)
    {

        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");
        $searchSPName = $plgID;
        if ($pluginObject != "")
            $searchSPName .= "_" . $pluginObject;

        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        if ($onlyRow == "1") {
            $count = 0;
            $dataTable = "";
            //Get All Plugin Function Definition
            $query = "SHOW FUNCTION STATUS WHERE DB = DATABASE() AND Name Like '" . $searchSPName . "%';";
            $this->kxdb->prepare($query);
            $this->kxdb->execute();

            while ($dataList = $this->kxdb->fetch()) {
                $name = $dataList->Name;
                $type = "function";
                $dataTable[$count]["name"] = $name;
                $dataTable[$count]["type"] = $type;
                $dataTable[$count]["plgID"] = $plgID;
                $dataTable[$count]["pluginObject"] = $pluginObject;
                $count++;
            }

            //Get All Plugin Procedures Definition
            $query = "SHOW PROCEDURE STATUS WHERE DB = DATABASE() AND Name Like '" . $searchSPName . "%';";
            $this->kxdb->prepare($query);
            $this->kxdb->execute();


            while ($dataList = $this->kxdb->fetch()) {
                $name = $dataList->Name;
                $type = "procedure";
                $dataTable[$count]["name"] = $name;
                $dataTable[$count]["type"] = $type;
                $dataTable[$count]["plgID"] = $plgID;
                $dataTable[$count]["pluginObject"] = $pluginObject;
                $count++;
            }

        } else
            $dataTable = "";

        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableListProceduresAndFunctions"
                , "hideHead" => $onlyRow
                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getTableList"]
                , "data" => $dataTable
                , "fVar" => ["plgID" => $plgID, "pluginObject" => $pluginObject]

                , "fields" =>
                [
                    [
                        "label" => $this->getDictionaryWord("LabelListName")
                        , "qryField" => "name"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListType")
                        , "qryField" => "type"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->getDictionaryWord("LabelListEdit")
                                    , "action" => "editModal"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                    , "qryAttributes" => [
                                    "pfname" => "name"
                                    , "plgID" => "plgID"
                                    , "pluginObject" => "pluginObject"
                                    , "pftype" => "type"

                                ]
                                    , "staticAttributes" => ["edit" => "1"]

                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelListDelete")
                                    , "qryAttributes" => [
                                    "pfname" => "name"
                                    , "plgID" => "plgID"
                                    , "pluginObject" => "pluginObject"
                                    , "pftype" => "type"
                                ]
                                    , "action" => "remove"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                                ]
                            ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();

    }

    protected function getModalAdd($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->getDictionaryWord("LabelModalTitleAdd"), "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePut($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getPagePut($ArrayVariable)
    {

        //set variable
        $name = $this->K_COMMON->getVarArray($ArrayVariable, "pfname");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $type = strtoupper($this->K_COMMON->getVarArray($ArrayVariable, "pftype"));
        $edit = $this->K_COMMON->getVarArray($ArrayVariable, "edit");
        $fieldDisable = "";
        $value_name = "";
        $value_type = "";
        $value_parameters = "";
        $value_returnType = "";
        $value_statement = "";

        //create form
        $this->appendForm("",
            [
                "id" => "paramForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "reloadTableList" => "tableListProceduresAndFunctions"
                , "closeModal" => "modalAddProceduresAndFunctions"


            ]
        );

        if ($edit != "") {
            //load sp data
            $procData = $this->kxdb->getSPDefinition($name);
            $value_name = $name;
            $value_type = $type;
            $value_parameters = $procData["parameters"];
            $value_returnType = $procData["return"];
            $value_statement = $procData["statement"];

            $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "name", "value" => $value_name]);
            $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "type", "value" => $value_type]);
        } else {
            $value_statement = "BEGIN" . chr(13) . "END";
        }

        //edit flag
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "edit", "value" => $edit]);

        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);

        //object
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginObject", "name" => "pluginObject", "value" => $pluginObject]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //param obj code
        if ($edit != "")
            $fieldDisable = "1";
        $label = $this->getDictionaryWord("LabelFormLabelName");
        $name = "name";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_name, "disabled" => $fieldDisable, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //param name
        $label = $this->getDictionaryWord("LabelFormLabelType");
        $name = "type";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => ["PROCEDURE" => "Procedure", "FUNCTION" => "Function"], "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_type, "disabled" => $fieldDisable, "required" => "1"]);


        //NEW ROW
        $this->appendRow($this->lastForm);

        //param is container
        $label = $this->getDictionaryWord("LabelFormLabelParameters");
        $name = "parameters";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_parameters]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //param is container
        $label = $this->getDictionaryWord("LabelFormLabelReturnType");
        $name = "returnType";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_returnType]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //param is container
        $label = $this->getDictionaryWord("LabelFormLabelStatement");
        $name = "statement";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "textarea", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_statement]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->getDictionaryWord("LabelFormSave");
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;

    }

    protected function delete($ArrayVariable)
    {
        $name = $this->K_COMMON->getVarArray($ArrayVariable, "pfname");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $spName = $name;

        //drop actual procedure
        $query = " DROP PROCEDURE IF EXISTS  " . $spName . ";";
        $this->kxdb->prepare($query);
        $this->kxdb->execute();


        //drop actual procedure
        $query = " DROP FUNCTION IF EXISTS  " . $spName . ";";
        $this->kxdb->prepare($query);
        $this->kxdb->execute();

        return array("success" => "true");
    }

    protected function saveData($ArrayVariable)
    {
        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $name = $this->K_COMMON->getVarArray($ArrayVariable, "name");
        $type = $this->K_COMMON->getVarArray($ArrayVariable, "type");
        $parameters = $this->K_COMMON->getVarArray($ArrayVariable, "parameters");
        $returnType = $this->K_COMMON->getVarArray($ArrayVariable, "returnType");
        $statement = $this->K_COMMON->getVarArray($ArrayVariable, "statement");
        $edit = $this->K_COMMON->getVarArray($ArrayVariable, "edit");
        $errorMsg = "";

        //check variable
        if ($edit == "") {
            $spName = $plgID;
            if ($pluginObject != "")
                $spName = strtolower($plgID . "_" . $pluginObject);
            if ($spName != "")
                $spName = strtolower($plgID . "_" . $name);
        } else {
            $spName = $name;
        }

        if ($type == "FUNCTION")
            $returnType = "RETURNS " . $returnType;

        //drop actual procedure
        $query = " DROP PROCEDURE IF EXISTS  " . $spName . ";";
        $this->kxdb->prepare($query);
        $this->kxdb->execute();

        //drop actual procedure
        $query = " DROP FUNCTION IF EXISTS  " . $spName . ";";
        $this->kxdb->prepare($query);
        $this->kxdb->execute();


        //create new procedure / function
        $query = " CREATE " . $type . " " . $spName . " (" . $parameters . ") " . $returnType . " " . $statement . ";";
        $this->kxdb->setTraceError();
        $this->kxdb->prepare($query);
        $this->kxdb->execute();

        $traceLog = $this->kxdb->getTraceLog();
        if (count($traceLog) > 0) {
            foreach ($traceLog as $err) {
                $errorMsg .= $err["errorStr"] . chr(10);
            }
            return ["success" => "false", "reason" => "error", "message" => $errorMsg];
        }


        return ["success" => "true"];
    }


}

?>