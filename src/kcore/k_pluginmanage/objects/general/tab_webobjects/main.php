<?
namespace k_pluginmanage\objects\general\tab_webobjects;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    var $ver = "1.0";

    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";

    var $dictionary;

    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
    }

    //manage action from ajax
    public function objectRequest($ArrayVariable)
    {
        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "saveData":
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "saveDataPType":
                return json_encode($this->saveDataPType($ArrayVariable));
                break;
            case "getModalAdd":
                return json_encode($this->getModalAdd($ArrayVariable));
                break;
            case "delete":
                return json_encode($this->delete($ArrayVariable));
                break;
            case "deletePType":
                return json_encode($this->deletePType($ArrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableList($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
        }
    }

    public function get($ArrayVariable = array())
    {

        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    protected function getPagePut($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $this->getFormWOB($ArrayVariable);

        $this->appendRow("");
        $this->appendColumn($this->lastRow, ["nCol" => 12]);

        $listMyMList = $this->getTableList($ArrayVariable);
        $this->appendHTML($this->lastCol, $listMyMList);

        $this->appendRow("");
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "btnAddPType", "value" => $this->dictionary["LabelAdd"]
            , "openModal" => [
                "idModal" => "modalAddPType"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAdd"
                , "parameters" => ["plgID" => $plgID, "pluginObject" => str_replace('\\','\\\\',$pluginObject)]
            ]
        ]);

    }

    protected function getTableList($ArrayVariable)
    {

        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");
        $id_web_objects = "";

        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        //Load Table
        $table = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_objects");

        $table->get(["id_web_objects" , "wob_prototypes"],
            ["wob_plugin_name" => $plgID, "wob_plugin_object" => $pluginObject]);

        $data = $table->fetch();
        if ($data) {
            $id_web_objects = $data->id_web_objects;

        }

        if ($onlyRow == "1") {


            if ($data) {
                $prototypes = $data->wob_prototypes;
                $dataTable = json_decode($prototypes, true);

            }

        } else
            $dataTable = "";
        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableListPType"
                , "hideHead" => $onlyRow
                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getTableList"]
                , "data" => $dataTable
                , "fVar" => ["plgID" => $plgID, "pluginObject" => $pluginObject]

                , "fields" =>
                [
                    [
                        "label" => $this->dictionary["LabelListWOBPTypeName"]
                        , "qryField" => "name"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->dictionary["LabelListEdit"]
                                    , "action" => "editModal"
                                    , "idModal" => "modalAddPType"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                    , "qryAttributes" => [
                                    "pTypeCode" => "code"
                                ]
                                    , "staticAttributes" => [
                                    "plgID" => $plgID
                                    , "pluginObject" => str_replace('\\','\\\\',$pluginObject)
                                ]

                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelListDelete"]
                                    , "action" => "remove"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "deletePType"]
                                    , "qryAttributes" => [
                                    "pTypeCode" => "code"

                                ]
                                    , "staticAttributes" => [
                                    "plgID" => $plgID
                                    , "pluginObject" => str_replace('\\','\\\\',$pluginObject)
                                    ,"keyID" => $id_web_objects

                                ]
                                ]
                            ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();

    }

    public function getModalAdd($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->dictionary["LabelModalTitleAddPType"], "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePutPrototype($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getFormWOB($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $valye_wob_name = "";
        $value_wob_container = "";
        $value_wob_type = "";
        $typeArray = ["1" => "Web Object", "2" => "Web Object Advanced"];

        //Load Table
        $table = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_objects");
        $table->get(["wob_container", "wob_type", "wob_name"],
            ["wob_plugin_name" => $plgID, "wob_plugin_object" => $pluginObject]);
        $data = $table->fetch();
        if ($data) {
            $valye_wob_name = $data->wob_name;
            $value_wob_container = $data->wob_container;
            $value_wob_type = $data->wob_type;
        }

        $this->appendRow("");
        $this->appendColumn($this->lastRow, ["nCol" => 12]);

        //create form
        $this->appendForm($this->lastCol,
            [
                "id" => "wobForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
            ]
        );

        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plgID", "name" => "plgID", "value" => $plgID]);
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginObject", "name" => "pluginObject", "value" => $pluginObject]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //wob name
        $label = $this->dictionary["LabelFormWOBName"];
        $name = "wobName";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $valye_wob_name]);

        //wob name
        $label = $this->dictionary["LabelFormWOBType"];
        $name = "wobType";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $typeArray, "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_wob_type]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //wob is container
        $label = $this->dictionary["LabelFormWOBIsContainer"];
        $name = "wobContainer";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $value_wob_container]);


        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->dictionary["LabelFormSave"];
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;

    }

    protected function getPagePutPrototype($ArrayVariable)
    {

        //set variable
        $pTypeCode = $this->K_COMMON->getVarArray($ArrayVariable, "pTypeCode");
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $value_ptypes = "";
        
        //Load Table
        $table = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_objects");
        $table->get(["wob_prototypes"], ["wob_plugin_name" => $plgID, "wob_plugin_object" => $pluginObject]);
        $data = $table->fetch();
        if ($data) {
            $value_ptypes = $data->wob_prototypes;
        }

        //load Data pType
        $arrayPTypes = json_decode($value_ptypes, true);
        $arrayPType = $this->K_COMMON->getVarArray($arrayPTypes, $pTypeCode);

        $value_pTypeCode = $this->K_COMMON->getVarArray($arrayPType, "code");
        $value_pTypeName = $this->K_COMMON->getVarArray($arrayPType, "name");
        $value_pTypeParameters = $this->K_COMMON->getVarArray($arrayPType, "parameters");

        //create form
        $this->appendForm("",
            [
                "id" => "wobPTypeForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveDataPType"
                , "reloadTableList" => "tableListPType"
                , "closeModal" => $idModal
            ]
        );

        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);

        //object
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginObject", "name" => "pluginObject", "value" => $pluginObject]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //wob obj code
        $label = $this->dictionary["LabelFormPTypeCode"];
        $name = "pTypeCode";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_pTypeCode, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //wob name
        $label = $this->dictionary["LabelFormPTypeName"];
        $name = "pTypeName";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_pTypeName, "required" => "1"]);

        //list parameters
        $name = "parameterGroup";
        $this->appendListGroup($this->lastCol,
            [
                "id" => $name
                , "attributes" => 'style="height:176; overflow: scroll; overflow-x: hidden;"'
                , "values" => $value_pTypeParameters
                , "addingValues" =>
                [
                    "valuesHName" => "parameter"
                    , "fields" => [
                    ["type" => "text", "labelInput" => $this->dictionary["LabelPTypeParam"], "name" => "pTypeParam"]
                    , ["type" => "text", "labelInput" => $this->dictionary["LabelPTypeParamVal"], "name" => "pTypeParamValue"]
                ]
                ]
            ]
        );

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->dictionary["LabelFormSave"];
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;

    }

    public function delete($ArrayVariable)
    {
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");

        $table = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_objects");

        $table->delete(["id_web_objects" => $keyID, "wob_plugin_name" => $plgID, "wob_plugin_object" => $pluginObject]);

        return array("success" => "true");
    }

    public function deletePType($ArrayVariable)
    {
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $pTypeCode = $this->K_COMMON->getVarArray($ArrayVariable, "pTypeCode");


        $table = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_objects");

        $table->get(["*"],["id_web_objects" => $keyID, "wob_plugin_name" => $plgID, "wob_plugin_object" => $pluginObject] );
        if ($data = $table->fetch())
        {
            $prototypes = $data->wob_prototypes;
            $arrayPTypes = json_decode($prototypes, true);

            unset($arrayPTypes[$pTypeCode]);

            $prototypes = json_encode($arrayPTypes, true);

            $table->put(
                [
                    "wob_prototypes" => $prototypes
                ], ["id_web_objects" => $keyID]
            );
        }
        else
            return ["success" =>"false"];


        return ["success" => "true"];
    }

    public function saveDataPType($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $pTypeName = $this->K_COMMON->getVarArray($ArrayVariable, "pTypeName");
        $pTypeCode = $this->K_COMMON->getVarArray($ArrayVariable, "pTypeCode");
        $pTypeParam = $this->K_COMMON->getVarArray($ArrayVariable, "parameter");
        $prototypes = [];
        //include tables
        $table = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_objects");

        if ($plgID == "" || $pluginObject == "f" || $pTypeCode == "")
            return ["success" => "false", "reason" => "inputVoid", "message" => "Please fill in the fields!"];

        $table->get(["wob_prototypes"], ["wob_plugin_name" => $plgID, "wob_plugin_object" => $pluginObject]);
        $data = $table->fetch();
        if ($data) {
            $prototypes = $data->wob_prototypes;
            $arrayPTypes = json_decode($prototypes, true);
            $arrayPTypes[$pTypeCode]["code"] = $pTypeCode;
            $arrayPTypes[$pTypeCode]["name"] = $pTypeName;

            //clear array parameters
            $arrayPTypes[$pTypeCode]["parameters"] = [];

            $pTypeParamsArray = json_decode($pTypeParam, true);
            if (!empty($pTypeParamsArray))
                foreach ($pTypeParamsArray as $pTypeParamArray)
                    $arrayPTypes[$pTypeCode]["parameters"][$pTypeParamArray["pTypeParam"]] = $pTypeParamArray["pTypeParamValue"];

            $prototypes = json_encode($arrayPTypes, true);
        }

        $table->put(
            [
                "wob_prototypes" => $prototypes
            ], ["wob_plugin_name" => $plgID, "wob_plugin_object" => $pluginObject]
        );

        return ["success" => "true"];
    }

    public function saveData($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $wobContainer = $this->K_COMMON->getVarArray($ArrayVariable, "wobContainer");
        $wobType = $this->K_COMMON->getVarArray($ArrayVariable, "wobType");
        $wobName = $this->K_COMMON->getVarArray($ArrayVariable, "wobName");
        $arrayWhere = "";

        // Check Variables
        if ($wobContainer == "")
            $wobContainer = 0;

        //include tables
        $table = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_objects");

        if ($plgID == "" || $pluginObject == "" || $wobType == "" || $wobName == "")
            return ["success" => "false", "reason" => "inputVoid", "message" => "Please fill in the fields!"];

        $table->get(["id_web_objects"], ["wob_plugin_name" => $plgID, "wob_plugin_object" => $pluginObject]);
        $data = $table->fetch();
        if ($data) {
            $arrayWhere = ["wob_plugin_name" => $plgID, "wob_plugin_object" => $pluginObject];
        }
        //$table->showSQL();
        $table->put(
            [
                "wob_plugin_name" => $plgID
                , "wob_plugin_object" => $pluginObject
                , "wob_container" => $wobContainer
                , "wob_type" => $wobType
                , "wob_name" => $wobName
            ], $arrayWhere
        );

        return ["success" => "true", "message" => $this->dictionary["msgSuccessSave"]];
    }


}

?>