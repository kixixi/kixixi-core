<?
namespace k_pluginmanage\objects\general\tab_messages;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    var $ver = "1.0";

    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";

    var $dictionary;
    var $msgLanguages;

    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
        $this->msgLanguages = ["EN" => "EN", "IT" => "IT", "FR" => "FR", "ES" => "ES", "DE" => "DE"];

    }

    //manage action from ajax
    public function objectRequest($ArrayVariable)
    {
        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "saveData":
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "getModalAdd":
                return json_encode($this->getModalAdd($ArrayVariable));
                break;
            case "delete":
                return json_encode($this->delete($ArrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableList($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
        }
    }

    public function get($ArrayVariable = array())
    {

        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
                $this->getPageList($ArrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    protected function getPageList($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");

        //create modal for language
        $this->appendRow("");
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                , "typeLoad" => "array"
                , "options" => $this->msgLanguages
                , "id" => "lngMessages"
                , "labelInput" => $this->dictionary["LabelSelectLanguage"]
                , "noEmptyOption" => "1"
                , "triggerEvent" => "change"
                , "triggerFunction" => "kx_tablelist_script_tableListMessages.loadTableList"
            ]
        );

        $listMyMList = $this->getTableList($ArrayVariable);
        $this->appendHTML("", $listMyMList);
        $this->appendInput("", ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "btnAddMessage", "value" => $this->dictionary["LabelAdd"]
            , "openModal" => [
                "idModal" => "modalAddMessage"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAdd"
                , "parameters" => ["plgID" => $plgID, "pluginObject" => $pluginObject]
            ]
        ]);

    }

    protected function getTableList($ArrayVariable)
    {

        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");
        $lng = $this->K_COMMON->getVarArray($ArrayVariable, "lngMessages");
        if ($lng == "")
            $lng = $this->language;
        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        if ($onlyRow == "1") {
            //Load Table
            $table = $this->K_COMMON->getPluginDataObject("k_mailing", "messages");
            $table->get(["id_message", "msg_plugin_name", "msg_plugin_object", "msg_code", "msg_subject"],
                ["msg_web_code" => "", "msg_plugin_name" => $plgID, "msg_plugin_object" => $pluginObject, "msg_language_code" => $lng]);
            $dataTable = $table->getResults("array");
        } else
            $dataTable = "";
        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableListMessages"
                , "hideHead" => $onlyRow
                , "fieldsPageToGet" => ["lngMessages" => "lngMessages"]
                , "ajax" =>
                [
                    "kcp" => $this->getPluginName()
                    , "kco" => $this->getPluginObjectName()
                    , "kca" => "getTableList"

                ]
                , "data" => $dataTable
                , "fVar" => ["plgID" => $plgID, "pluginObject" => $pluginObject]

                , "fields" =>
                [
                    [
                        "label" => $this->dictionary["LabelListCode"]
                        , "qryField" => "msg_code"
                    ]
                    ,
                    [
                        "label" => $this->dictionary["LabelListSubject"]
                        , "qryField" => "msg_subject"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->dictionary["LabelListEdit"]
                                    , "action" => "editModal"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                    , "qryAttributes" => [
                                    "keyID" => "id_message"
                                    , "msgCode" => "msg_code"
                                    , "plgID" => "msg_plugin_name"
                                    , "pluginObject" => "msg_plugin_object"
                                ]

                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelListDelete"]
                                    , "qryAttributes" => [
                                    "msgCode" => "msg_code"
                                    , "plgID" => "msg_plugin_name"
                                    , "pluginObject" => "msg_plugin_object"
                                ]
                                    , "action" => "remove"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                                ]
                            ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();

    }

    public function getModalAdd($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->dictionary["LabelModalTitleAddMsg"], "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePut($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getPagePut($ArrayVariable)
    {

        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $webCode = "";  //"" because where are creating general messages
        $value_msgCode = "";
        $value_msgSubject = "";
        $value_msgMessage = "";

        //Load Table
        $table = $this->K_COMMON->getPluginDataObject("k_mailing", "messages");
        $table->get(["msg_code", "msg_subject", "msg_message"],
            ["msg_web_code" => "", "msg_plugin_name" => $plgID, "msg_plugin_object" => $pluginObject, "id_message" => $keyID]);
        $data = $table->fetch();
        if ($data) {
            $value_msgCode = $data->msg_code;
            $value_msgSubject = $data->msg_subject;
            $value_msgMessage = $data->msg_message;
        }
        //create form
        $this->appendForm("",
            [
                "id" => "messageForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "reloadTableList" => "tableListMessages"
                , "closeModal" => "modalAddMessage"

            ]
        );

        //keyID hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "keyID", "value" => $keyID]);

        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);

        //object
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginObject", "name" => "pluginObject", "value" => $pluginObject]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //code
        $label = $this->dictionary["LabelFormCode"];
        $name = "msgCode";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_msgCode, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //message
        $label = $this->dictionary["LabelFormSubject"];
        $name = "msgSubject";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_msgSubject, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //message
        $label = $this->dictionary["LabelFormMessage"];
        $name = "msgMessage";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "textEditor", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_msgMessage, "webCode" => $webCode]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->dictionary["LabelFormSave"];
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;

    }

    public function delete($ArrayVariable)
    {
        $msgCode = $this->K_COMMON->getVarArray($ArrayVariable, "msgCode");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");

        $table = $this->K_COMMON->getPluginDataObject("k_mailing", "messages");
        $table->delete(["msg_web_code" => "", "msg_code" => $msgCode, "msg_plugin_name" => $plgID, "msg_plugin_object" => $pluginObject]);

        return array("success" => "true");
    }


    public function saveData($ArrayVariable)
    {

        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $msgCode = $this->K_COMMON->getVarArray($ArrayVariable, "msgCode");
        $msgSubject = $this->K_COMMON->getVarArray($ArrayVariable, "msgSubject");
        $msgMessage = $this->K_COMMON->getVarArray($ArrayVariable, "msgMessage");

        //include tables
        $table = $this->K_COMMON->getPluginDataObject("k_mailing", "messages");

        if ($plgID == "" || $msgCode == "" || $msgSubject == "" || $msgMessage == "")
            return ["success" => "false", "reason" => "inputVoid", "message" => "Please fill in the fields!"];


        if ($keyID == "") {
            //check if cod dup
            $table->get(["id_message"], ["msg_plugin_name" => $plgID, "msg_plugin_object" => $pluginObject, "msg_code" => $msgCode]);
            $data = $table->fetch();
            if ($data)
                return ["success" => "false", "reason" => "actionCodeDup", "message" => $this->dictionary["emsgMsgCodeDup"]];
            foreach ($this->msgLanguages as $langCode) {
                $table->put(
                    [
                        "msg_web_code" => ""
                        ,"msg_plugin_name" => $plgID
                        , "msg_plugin_object" => $pluginObject
                        , "msg_code" => $msgCode
                        , "msg_language_code" => $langCode
                        , "msg_subject" => $msgSubject
                        , "msg_message" => $msgMessage
                    ]
                );
            }
        } else {
            $table->put(
                [
                    "msg_subject" => $msgSubject
                    , "msg_message" => $msgMessage
                ]
                , ["id_message" => $keyID]
            );


        }

        return ["success" => "true"];
    }


}

?>