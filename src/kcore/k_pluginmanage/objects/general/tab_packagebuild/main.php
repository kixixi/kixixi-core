<?

namespace k_pluginmanage\objects\general\tab_packagebuild;
class obj extends \k_webmanager\objects\general\base_panel\obj
{
    var $ver = "1.0";

    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";

    var $dictionary;


    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();

    }

    //manage action from ajax
    public function objectRequest($ArrayVariable)
    {

        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "downloadPackage" :
                return $this->downloadPackage($ArrayVariable);
                break;
            case "rebuildPackage" :
                return json_encode($this->rebuildPackage($ArrayVariable));
                break;

        }
    }

    public function get($ArrayVariable = array())
    {

        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");


        switch ($pluginMethod) {
            case "list":
            case "":
                $this->getPagePut($ArrayVariable);

                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);

                break;

        }


        return $this;
    }

    protected function getPagePut($ArrayVariable)
    {


        //set variables
        $pluginName = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");

        //add files to include
        $this->addHeadFile([$this->getPathFileJS("put")]);


        $this->appendHtml($this->lastCol, "<br><br>");


        /*  REBUILD PACKAGE PLUGIN  */

        //create form
        $this->appendForm("",
            [
                "id" => "rebuildPackageForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "rebuildPackage"
                , "dialogMessage" => "Rebuilding ..."

            ]
        );

        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginName", "name" => "pluginName", "value" => $pluginName]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $this->appendColumn($this->lastRow, ["nCol" => 5]);
        $this->appendHtml($this->lastCol, $this->getDictionaryWord("TextRebuildPackage"));
        $this->appendHtml($this->lastCol, "<br><br>");



        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->dictionary["labelFormRebuildPack"];
        $name = "btnRebuildPackage";
        $this->appendColumn($this->lastRow, ["nCol" => 5]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        $this->appendHtml($this->lastCol, "<br><br>");

        /*  PACKAGE PLUGIN  */

        //create form
        $this->appendForm("",
            [
                "id" => "packageForm"

            ]
        );

        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginName", "name" => "pluginName", "value" => $pluginName]);

        //NEW ROW
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendHtml($this->lastCol, $this->getDictionaryWord("TextDownloadPackage"));
        $this->appendHtml($this->lastCol, "<br><br>");


        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->dictionary["labelFormDownload"];
        $name = "btnDownloadPackage";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "btn", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);




    }

    public function downloadPackage($ArrayVariable)
    {

        //set object
        $objDownload = $this->getObject("builder");
        $ArrayVariable["webCode"] = $this->K_COMMON->getEditingWeb();

        return $objDownload -> downloadPackage($ArrayVariable);



    }

    public function rebuildPackage($ArrayVariable)
    {

        //set object
        $objDownload = $this->getObject("builder");
        $ArrayVariable["webCode"] = $this->K_COMMON->getEditingWeb();

        return $objDownload -> rebuildPackage($ArrayVariable);


    }



}

?>