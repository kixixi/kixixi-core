// create namespace
KXJS.namespace("k_pluginmanage.objects.general.packagebuild");

// create method of plugin
(function(NS) {
    var t = this;
    t.pluginName = "k_pluginmanage";
    t.pluginObject= "objects\\general\\tab_packagebuild";

    t.ready = function() {

        $("#btnDownloadPackage").click(function(){
            var varRequest = "pluginName="+$("#packageForm #pluginName").val();
            

            var url = KXJS.buildAPICallUrl("KXCDWF", t.pluginName, t.pluginObject, "downloadPackage",varRequest);

            KXJS.downloadFile(url);
        });


    }



    this.init = function(){
        $(document).ready(t.ready);

    };

    this.init();

}).apply(KXNS.k_pluginmanage.objects.general.packagebuild);

