<?
/**
 * PLUGIN MANAGE (Dashboard)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_pluginmanage
 * @subpackage objects\dashboard
 * @since 1.00
 */
namespace k_pluginmanage\objects\general\tab_dashboard;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";


    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "saveData":
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "getModalAdd":
                return json_encode($this->getModalAdd($ArrayVariable));
                break;
            case "delete":
                return json_encode($this->delete($ArrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableList($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
        }
    }

    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "put", "get")
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {


        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
                $this->getPageList($ArrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    protected function getPageList($ArrayVariable)
    {


        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");

        $listMyMList = $this->getTableList($ArrayVariable);
        $this->appendHTML("", $listMyMList);
        $this->appendInput("", ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "btnAddDashboard", "value" => $this->getDictionaryWord("LabelAdd")
            , "openModal" => [
                "idModal" => "modalAddDashboard"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAdd"
                , "parameters" => ["plgID" => $plgID, "pluginObject" => $pluginObject]
            ]
        ]);

    }

    protected function getTableList($ArrayVariable)
    {

        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");

        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        if ($onlyRow == "1") {
            //Load Table
            $table = $this->K_COMMON->getPluginDataObject("k_panelmanage", "dashboard");
            $table->get(["id_menu_dashboard", "dmn_type", "dmn_menu_code", "dmn_label", "dmn_plugin_name", "dmn_plugin_object"],
                ["dmn_plugin_name" => $plgID, "dmn_plugin_object" => $pluginObject]);
            $dataTable = $table->getResults("array");
        } else
            $dataTable = "";
        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableListDashboards"
                , "hideHead" => $onlyRow
                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getTableList"]
                , "data" => $dataTable
                , "fVar" => ["plgID" => $plgID, "pluginObject" => $pluginObject]
                , "dictionary" => $this->getDictionary($plgID,$pluginObject)
                , "fields" =>
                [
                    [
                        "label" => $this->getDictionaryWord("LabelListCode")
                        , "qryField" => "dmn_menu_code"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListType")
                        , "qryField" => "dmn_type"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListLabel")
                        , "type" => "dictionary"
                        , "qryField" => "dmn_label"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->getDictionaryWord("LabelListEdit")
                                    , "action" => "editModal"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                    , "qryAttributes" => [
                                    "keyID" => "id_menu_dashboard"
                                    , "plgID" => "dmn_plugin_name"
                                    , "pluginObject" => "dmn_plugin_object"
                                ]

                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelListDelete")
                                    , "qryAttributes" => [
                                    "keyID" => "id_menu_dashboard"
                                    , "plgID" => "dmn_plugin_name"
                                    , "pluginObject" => "dmn_plugin_object"
                                ]
                                    , "action" => "remove"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                                ]
                            ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();

    }

    protected function getModalAdd($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->getDictionaryWord("LabelModalTitleAddDash"), "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePut($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getPagePut($ArrayVariable)
    {

        //add files to include
        $this->addHeadFile([$this->getPathFileJS("put")]);


        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");

        $value_menuCode = "DSB";
        $value_type = "";
        $value_label = "";
        $value_icon = "";
        $value_auth = "";

        //load authorization
        $authArray = $this->K_COMMON->getPluginObject("k_secauth", "api")->getListAuth(["OM" => "array", "plgID" => $plgID]);

        //Load Table
        $table = $this->K_COMMON->getPluginDataObject("k_panelmanage", "dashboard");
        $table->get(["dmn_menu_code", "dmn_type", "dmn_label", "dmn_icon", "dmn_authorization"],
            ["dmn_plugin_name" => $plgID, "dmn_plugin_object" => $pluginObject, "id_menu_dashboard" => $keyID]);
        $data = $table->fetch();
        if ($data) {
            $value_menuCode = $data->dmn_menu_code;
            $value_type = $data->dmn_type;
            $value_label = $data->dmn_label;
            $value_icon = $data->dmn_icon;
            $value_auth = $data->dmn_authorization;
        }
        //create form
        $this->appendForm("",
            [
                "id" => "dmnForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "reloadTableList" => "tableListDashboards"
                , "closeModal" => "modalAddDashboard"

            ]
        );

        //keyID hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "keyID", "value" => $keyID]);

        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plgID", "name" => "plgID", "value" => $plgID]);

        //object
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginObject", "name" => "pluginObject", "value" => $pluginObject]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        // menu code
        $label = $this->getDictionaryWord("LabelFormMenuCode");
        $name = "menuCode";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_menuCode, "required" => "1"]);

        // label code
        $label = $this->getDictionaryWord("LabelFormType");
        $name = "type";
        $optionType = ["menu"=>"Menu","card"=>"Card"];
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $optionType, "noEmptyOption" => "1" ,"labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_type]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        // label code
        $label = $this->getDictionaryWord("LabelFormLabelCode");
        $name = "labelCode";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_combodictionary"
            , [
                "labelInput" => $label
                , "name" => $name
                , "value" => $value_label
                , "pluginName" => $plgID
                , "pluginObject" => $pluginObject
                , "required" => "1"
            ]
        );



        // label code
        $label = $this->getDictionaryWord("LabelFormIcon");
        $name = "icon";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_icon]);

        //NEW ROW
        $this->appendRow($this->lastForm);


        // label code
        $label = $this->getDictionaryWord("LabelFormAuth");
        $name = "auth";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $authArray, "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_auth]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->getDictionaryWord("LabelFormSave");
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;

    }

    protected function delete($ArrayVariable)
    {
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");

        $table = $this->K_COMMON->getPluginDataObject("k_panelmanage", "dashboard");

        $table->delete(["id_menu_dashboard" => $keyID, "dmn_plugin_name" => $plgID, "dmn_plugin_object" => $pluginObject]);

        return array("success" => "true");
    }

    protected function saveData($ArrayVariable)
    {

        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $menuCode = $this->K_COMMON->getVarArray($ArrayVariable, "menuCode");
        $type = $this->K_COMMON->getVarArray($ArrayVariable, "type");
        $labelCode = $this->K_COMMON->getVarArray($ArrayVariable, "labelCode");
        $icon = $this->K_COMMON->getVarArray($ArrayVariable, "icon");
        $auth = $this->K_COMMON->getVarArray($ArrayVariable, "auth");
        $arrayWhere = "";
        $plgNameAuth = "";

        //check variable
        if ($auth != "") {
            $plgNameAuth = $plgID;
        }
        //include tables
        $table = $this->K_COMMON->getPluginDataObject("k_panelmanage", "dashboard");

        if ($plgID == "" || $menuCode == "")
            return ["success" => "false", "reason" => "inputVoid", "message" => "Please fill in the fields!"];

        if ($keyID != "")
            $arrayWhere = ["id_menu_dashboard" => $keyID];
        else {
            //check if cod dup
            $table->get(["id_menu_dashboard"], ["dmn_plugin_name" => $plgID, "dmn_plugin_object" => $pluginObject, "dmn_menu_code" => $menuCode]);
            $data = $table->fetch();
            if ($data)
                return ["success" => "false", "reason" => "actionCodeDup", "message" => $this->getDictionaryWord("emsgDashboardCodeDup")];
        }


        $table->put(
            [
                "dmn_plugin_name" => $plgID
                , "dmn_plugin_object" => $pluginObject
                , "dmn_menu_code" => $menuCode
                , "dmn_type" => $type
                , "dmn_label" => $labelCode
                , "dmn_icon" => $icon
                , "dmn_authorization_plugin_name" => $plgNameAuth
                , "dmn_authorization" => $auth
            ], $arrayWhere
        );

        return ["success" => "true"];
    }

}

?>