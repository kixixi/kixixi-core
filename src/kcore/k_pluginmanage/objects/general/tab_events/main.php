<?
/**
 * PLUGIN MANAGE (events)
 *
 *
 * @paramor Marco Del Principe <m.delprincipe73@gmail.com>
 * @paramor Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_pluginmanage
 * @subpackage objects\events
 * @since 1.00
 */
namespace k_pluginmanage\objects\general\tab_events;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "saveData":
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "getModalAdd":
                return json_encode($this->getModalAdd($ArrayVariable));
                break;
            case "delete":
                return json_encode($this->delete($ArrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableList($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
        }
    }

    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "put", "get")
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {

        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
                $this->getPageList($ArrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    protected function getPageList($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");

        $listMyMList = $this->getTableList($ArrayVariable);
        $this->appendHTML("", $listMyMList);

        $this->appendInput("", ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "btnAddEvent", "value" => $this->getDictionaryWord("LabelAdd")
            , "openModal" => [
                "idModal" => "modalAddEvent"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAdd"
                , "parameters" => ["plgID" => $plgID, "pluginObject" => $pluginObject]
            ]
        ]);

    }

    protected function getTableList($ArrayVariable)
    {

        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");
        $searchSPName = $plgID;
        if ($pluginObject != "")
            $searchSPName .= "_" . $pluginObject;

        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        if ($onlyRow == "1") {
            $count = 0;
            $dataTable = "";
            //Get All Plugin Event
            $this->kxdb->loadEventsList($plgID);

            while ($dataList = $this->kxdb->fetch()) {
                $name = $dataList->EVENT_NAME;
                $startTime = $dataList->STARTS;
                $interval = $dataList->INTERVAL_VALUE." ".$dataList->INTERVAL_FIELD;
                $dataTable[$count]["name"] = $name;
                $dataTable[$count]["interval"] = $interval;
                $dataTable[$count]["startTime"] = $startTime;
                $dataTable[$count]["plgID"] = $plgID;
                $dataTable[$count]["pluginObject"] = $pluginObject;
                $count++;
            }

        } else
            $dataTable = "";

        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableListEvents"
                , "hideHead" => $onlyRow
                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getTableList"]
                , "data" => $dataTable
                , "fVar" => ["plgID" => $plgID, "pluginObject" => $pluginObject]

                , "fields" =>
                [
                    [
                        "label" => $this->getDictionaryWord("LabelListName")
                        , "qryField" => "name"
                    ]
                    , [
                    "label" => $this->getDictionaryWord("LabelListInterval")
                    , "qryField" => "interval"
                ]
                    , [
                    "label" => $this->getDictionaryWord("LabelListStartTime")
                    , "qryField" => "startTime"
                ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->getDictionaryWord("LabelListEdit")
                                    , "action" => "editModal"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                    , "qryAttributes" => [
                                    "eventName" => "name"
                                    , "plgID" => "plgID"
                                    , "pluginObject" => "pluginObject"

                                ]
                                    , "staticAttributes" => ["edit" => "1"]
                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelListDelete")
                                    , "qryAttributes" => [
                                    "eventName" => "name"
                                    , "plgID" => "plgID"
                                    , "pluginObject" => "pluginObject"
                                ]
                                    , "action" => "remove"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                                ]
                            ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();

    }

    protected function getModalAdd($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->getDictionaryWord("LabelModalTitleAdd"), "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePut($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getPagePut($ArrayVariable)
    {

        //set variable
        $name = $this->K_COMMON->getVarArray($ArrayVariable, "eventName");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $edit = $this->K_COMMON->getVarArray($ArrayVariable, "edit");
        $fieldDisable = "";
        $value_name = "";
        $value_interval = "";
        $value_intervalNumb = "";
        $value_intervalPeriod = "";
        $value_startTime = "";
        $value_startTimeDate = "";
        $value_startTimeHour = "";
        $value_statement = "";

        //create form
        $this->appendForm("",
            [
                "id" => "paramForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "reloadTableList" => "tableListEvents"
                , "closeModal" => "modalAddEvent"


            ]
        );

        if ($edit != "") {
            //load sp data
            $eventData = $this->kxdb->loadEventsList($name);
            $data = $eventData->fetch();
            if ($data) {
                $value_name = $name;
                $value_intervalNumb = $data->INTERVAL_VALUE;
                $value_intervalPeriod = $data->INTERVAL_FIELD;
                $startTime = $data->STARTS;
                $value_startTimeDate =substr($startTime,0,strpos($startTime," "));
                $value_startTimeHour = substr($startTime,strpos($startTime," ")+1);
                $value_statement = $data->EVENT_DEFINITION;
                $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "name", "value" => $value_name]);
                $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "edit", "value" => $edit]);
            }
        } else {
            $value_statement = "BEGIN" . chr(13) . "END";
        }

        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);

        //object
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginObject", "name" => "pluginObject", "value" => $pluginObject]);


        //event name
        if ($edit != "") {
            $fieldDisable = "1";

        }
        //NEW ROW
        $this->appendRow($this->lastForm);
        $label = $this->getDictionaryWord("LabelFormLabelName");

        $name = "name";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_name, "disabled" => $fieldDisable]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //interval
        $label = $this->getDictionaryWord("LabelFormLabelInterval");
        $name = "intervalNumb";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "numb","labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_intervalNumb]);

        $label = "";
        $name = "intervalPeriod";
        $optionInterval = ["YEAR"=>"YEAR","QUARTER"=>"QUARTER","MONTH"=>"MONTH","DAY"=>"DAY","HOUR"=>"HOUR","MINUTE"=>"MINUTE","WEEK"=>"WEEK","SECOND"=>"SECOND"];
        $this->appendColumn($this->lastRow, ["nCol" => 9]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" =>$optionInterval, "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_intervalPeriod]);


        //NEW ROW
        $this->appendRow($this->lastForm);

        //startTime
        $label = $this->getDictionaryWord("LabelFormLabelStartTime");
        $name = "startTimeDate";

        //date
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "datePicker",  "labelInput" => $label, "id" => $name, "name" => $name, "minView" => "month", "starView" => "month","value"=>$value_startTimeDate]);

        // Hour
        $label = "";
        $name = "startTimeHour";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "datePicker",  "labelInput" => $label, "id" => $name, "name" => $name, "minView" => "hour", "maxView" => "day", "startView" => "day", "formatDate" => "hh:ii","value"=>$value_startTimeHour]);


        //NEW ROW
        $this->appendRow($this->lastForm);

        //event action statement
        $label = $this->getDictionaryWord("LabelFormLabelStatement");
        $name = "statement";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "textarea", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_statement]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->getDictionaryWord("LabelFormSave");
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;

    }

    protected function delete($ArrayVariable)
    {
        $name = $this->K_COMMON->getVarArray($ArrayVariable, "eventName");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");

        //drop actual Event
        $query = " DROP EVENT IF EXISTS  " . $name . ";";
        $this->kxdb->prepare($query);
        $this->kxdb->execute();

        return array("success" => "true");
    }

    protected function saveData($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $name = $this->K_COMMON->getVarArray($ArrayVariable, "name");
        $edit = $this->K_COMMON->getVarArray($ArrayVariable, "edit");

        $intervalNumb = $this->K_COMMON->getVarArray($ArrayVariable, "intervalNumb");
        $intervalPeriod = $this->K_COMMON->getVarArray($ArrayVariable, "intervalPeriod");
        $startTimeDate = $this->K_COMMON->getVarArray($ArrayVariable, "startTimeDate");
        $startTimeHour = $this->K_COMMON->getVarArray($ArrayVariable, "startTimeHour");
        $statement = $this->K_COMMON->getVarArray($ArrayVariable, "statement");
        $errorMsg = "";

        //check variable
        if($edit == ""){
            if($pluginObject!= "")
                $name = $pluginObject."_".$name;
            $name = $plgID."_".$name;
        }
        $query = " DROP EVENT IF EXISTS  " . $name . ";";
        $this->kxdb->prepare($query);
        $this->kxdb->execute();

        $query = " CREATE EVENT " . $name . " ON SCHEDULE EVERY " . $intervalNumb." ".$intervalPeriod . ($startTimeDate." ".$startTimeHour != "" ? " STARTS '" . $startTimeDate." ".$startTimeHour . "'" : "") . " DO " . $statement;
        $this->kxdb->setTraceError();
        $this->kxdb->prepare($query);
        $this->kxdb->execute();

        $traceLog = $this->kxdb->getTraceLog();
        if (count($traceLog) > 0) {
            foreach ($traceLog as $err) {
                $errorMsg .= $err["errorStr"] . chr(10);
            }
            return ["success" => "false", "reason" => "error", "message" => $errorMsg];
        }


        return ["success" => "true"];
    }


}

?>