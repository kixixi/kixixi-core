<?
namespace k_pluginmanage\objects\general\tab_templates;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    var $ver = "1.0";

    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";

    var $dictionary;

    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
    }

    //manage action from ajax
    public function objectRequest($ArrayVariable)
    {
        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "saveData":
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "getModalAdd":
                return json_encode($this->getModalAdd($ArrayVariable));
                break;
            case "deleteTpl":
                return json_encode($this->delete($ArrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableListPanelTemplates($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
        }
    }

    public function get($ArrayVariable = array())
    {
        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
                $this->getPageList($ArrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    protected function getPageList($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $tplType = $this->K_COMMON->getVarArray($ArrayVariable, "tplType");




        switch ($tplType)
        {
            case "web":

                $this->appendRow("", ["class" => "m-t-3"]);
                $this->appendColumn($this->lastRow);
                $this->appendHTML($this->lastCol, $this->dictionary["msgAddWebTemplate"]);

                $this->appendRow("", ["class" => "m-t-3"]);
                $this->appendColumn($this->lastRow);
                $this->appendInput($this->lastCol, ["type" => "btn", "semanticColor" => "success",
                    "id" => "btnAddTemplateWeb"
                    , "value" => $this->dictionary["LabelAddWebTemplate"]
                    , "link" => $this->getPanelManageURL() . "/k_webmanager/p_pages/list/developer"
                    , "linkTarget" => "_blank"
                ]);
                break;
            case "panel":
                $listMyMList = $this->getTableListPanelTemplates($ArrayVariable);
                $this->appendHTML("", $listMyMList);

                $this->appendInput("", ["type" => "btn", "class" => "m-t-3 m-l-1", "semanticColor" => "success",
                    "id" => "btnAddTemplatePanel"
                    , "value" => $this->dictionary["LabelAddPanelTemplate"]
                    , "openModal" => [
                        "idModal" => "modalAddTemplatesPanel"
                        , "kcp" => $this->getPluginName()
                        , "kco" => $this->getPluginObjectName()
                        , "kca" => "getModalAdd"
                        , "parameters" =>
                            [
                                "plgID" => $plgID,
                                "pluginObject" => str_replace("\\", "\\\\",$pluginObject),
                                "tplType" => "panel",
                                "webCode" => ""
                            ]
                    ]
                ]);
                break;
        }




    }

    protected function getTableListPanelTemplates($ArrayVariable)
    {

        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $tplType = $this->K_COMMON->getVarArray($ArrayVariable, "tplType");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");
        $arrayWhere =
            [
                "tpl_plugin_name" => $plgID,
                "tpl_type" => $tplType,
                "tpl_web_code" => ($tplType == "web" ? $webCode : "")
            ];

        //get pluginObject html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        //check variable
        if ($pluginObject != "")
            $arrayWhere["tpl_plugin_object"] = $pluginObject;

        if ($onlyRow == "1") {
            $table = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_templates");
            $table->get(["id_web_template,tpl_code,tpl_name"], $arrayWhere);
            $dataTable = $table->getResults("array");
        } else
            $dataTable = "";

        //create tablelist
        $OBJTableList->newTable(
            [
                "id" => "tableListTemplates_" . $tplType
                , "tableID" => "tableListTemplates_" . $tplType
                , "ajax" =>
                [
                    "kcp" => $this->getPluginName()
                    , "kco" => $this->getPluginObjectName()
                    , "kca" => "getTableList"

                ]
                , "hideHead" => $onlyRow
                , "data" => $dataTable
                , "fVar" =>
                [
                    "plgID" => $plgID,
                    "pluginObject" => $pluginObject,
                    "webCode" => $webCode,
                    "tplType" => $tplType
                ]
                , "fields" =>
                [
                    [
                        "label" => $this->dictionary["LabelCode"]
                        , "qryField" => "tpl_code"
                    ]
                    ,
                    [
                        "label" => $this->dictionary["LabelName"]
                        , "qryField" => "tpl_name"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->dictionary["LabelEdit"]
                                    , "action" => "editModal"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                    , "qryAttributes" => [
                                        "keyID" => "id_web_template"
                                    ]

                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelEdit"]
                                    , "action" => "link"
                                    , "qryAttributes" => ["keyID" => "id_web_template"]
                                    , "class" => "fa fa-arrows-alt"
                                    , "urlSeparator" => "&"
                                    , "url" => $this->K_COMMON->buildAPICallUrl("KXCH", "k_webmanager", "template_editor", "templateEditor")
                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelDuplicate"]
                                    , "action" => "modal"
                                    , "icon" => "fa-copy"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                    , "staticAttributes" => [
                                            "copy" => "1"

                                        ]
                                    , "qryAttributes" => [
                                        "keyID" => "id_web_template"
                                    ]
                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelDelete"]
                                    , "action" => "remove"
                                    , "qryAttributes" => ["keyID" => "id_web_template"]
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "deleteTpl"]
                                ]
                            ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();

    }


    public function getModalAdd($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->dictionary["LabelModalTitleAddTemplate"], "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePut($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getPagePut($ArrayVariable)
    {

        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $copy = $this->K_COMMON->getVarArray($ArrayVariable, "copy");
        $tplType = $this->K_COMMON->getVarArray($ArrayVariable, "tplType");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        $disabledFields = "";
        $value_name = "";
        $value_code = "";
        $value_file = "";
        $value_include = "";


        //Load Table
        if ($keyID != "") {
            //load data template
            $table = $this->K_COMMON->getPluginDataObject("k_webmanager","web_templates");
            $table->get("*", ["id_web_template" => $keyID]);
            $rowObj = $table->fetch();
            if ($rowObj) {
                $value_name = $rowObj->tpl_name;
                $value_code = $rowObj->tpl_code;
                $value_file = $rowObj->tpl_file;
                $value_include_tmp = $rowObj->tpl_include;
                $tplType = $rowObj->tpl_type;
                $webCode = $rowObj->tpl_web_code;
                $plgID = $rowObj->tpl_plugin_name;
                $pluginObject = $rowObj->tpl_plugin_object;


                //convert include
                if ($value_include_tmp != "") {

                    $value_include_tmp = (array)json_decode($value_include_tmp, true);
                    foreach ($value_include_tmp as $k => $v) {
                        foreach ($v as $file) {
                            $value_include[] = ["value" => $k, "text" => $file];
                        }
                    }
                    $value_include = json_encode($value_include);

                }
            }


        }

        //create form
        $this->appendForm("",
            [
                "id" => "templateForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "reloadTableList" => "tableListTemplates_" . $tplType
                , "closeModal" => $idModal

            ]
        );

        //check if copy
        if ($copy == 1) {
            $name = "copyID";
            $this->appendInput($this->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $keyID]);
            $keyID = ""; //reset keyID
            $value_name = $value_name . " copy";
        }

        //hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "keyID", "value" => $keyID]);
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "tplType", "name" => "tplType", "value" => $tplType]);
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "webCode", "name" => "webCode", "value" => $webCode]);

        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "pluginName", "value" => $plgID]);

        //pluginObject
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginObject", "name" => "pluginObject", "value" => $pluginObject]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //NEW COLUMN
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $label = $this->dictionary["LabelTplCode"];
        $name = "code";
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_code, "disabled" => $disabledFields, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //NEW COLUMN
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $label = $this->dictionary["LabelTplName"];
        $name = "name";
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_name, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //NEW COLUMN
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $label = $this->dictionary["LabelTplFile"];
        $textHelp = $this->dictionary["LabelTplFileHelp"];
        $name = "file";
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_file, "textHelp" => $textHelp]);

        //New ROW
        $this->appendRow($this->lastForm);

        //Heading
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->dictionary["LabelTplHeadAddFile"], "importance" => "5"]);

        //Fields for insert new value
        $this->appendRow($this->lastForm);

        //List Group of options
        $name = "includeGroup";

        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendListGroup($this->lastCol,
            [
                "id" => $name
                , "attributes" => 'style="height:135px; overflow: scroll; overflow-x: hidden;"'
                , "values" => $value_include
                , "addingValues" =>
                [
                    "valuesHName" => "include"
                    , "fields" => [
                    [
                        "type" => "select"
                        , "labelInput" => "Type"
                        , "name" => "value"
                        , "required" => "0"
                        , "typeLoad" => "array"
                        , "noEmptyOption" => "1"
                        , "options" => ["css" => "css", "js" => "js", "plugin" => "plugin"]
                    ]
                    , ["type" => "text", "labelInput" => "File", "id" => "fileName", "name" => "text"]

                ]
                ]
            ]
        );


        //NEW ROW
        $this->appendRow($this->lastForm);

        //button Save
        $label = $this->dictionary["LabelSave"];
        $name = "btnSaveMonth";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "emptyLabel" => "1", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        //button Modify Template
        if ($keyID != "") {

            $label = $this->dictionary["LabelBtnModifyTemplate"];
            $name = "btnModifyTemplate";
            $this->appendColumn($this->lastRow, ["nCol" => 6]);
            $this->appendInput($this->lastCol,
                [
                    "type" => "btn"
                    , "emptyLabel" => "1"
                    , "value" => $label
                    , "id" => $name
                    , "name" => $name
                    , "semanticColor" => "primary"
                    , "link" => $this->K_COMMON->buildAPICallUrl("KXCH", "k_webmanager", "template_editor", "templateEditor", "keyID=" . $keyID)
                    , "linkTarget" => "_blank"
                ]
            );
        }
        return $this;

    }

    public function delete($ArrayVariable)
    {
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");

        $webTemplate = $this->K_COMMON->getPluginObject("k_webmanager", "web_templates");
        return $webTemplate->deleteTpl(["keyID" => $keyID]);
    }

    public function saveData($ArrayVariable)
    {
        $tplType = $this->K_COMMON->getVarArray($ArrayVariable, "tplType");
        $webTemplate = $this->K_COMMON->getPluginObject("k_webmanager", "web_templates");

        if ($tplType == "web")
        {
            return $webTemplate->saveTplWeb($ArrayVariable);
        }
        else
            return $webTemplate->saveTplPanel($ArrayVariable);



    }




}

?>