<?
namespace k_pluginmanage\objects\general\tab_objects;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    var $ver = "1.0";
    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";

    var $dictionary;

    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();

    }

    //manage action from ajax
    public function objectRequest($ArrayVariable)
    {
        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "saveData":
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "getModalAdd":
                return json_encode($this->getModalAdd($ArrayVariable));
                break;
            case "delete":
                return json_encode($this->delete($ArrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableList($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
        }
    }

    public function get($ArrayVariable = array())
    {


        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
                $this->getPageList($ArrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    protected function getPageList($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");


        $listMyMList = $this->getTableList($ArrayVariable);
        $this->appendHTML("", $listMyMList);
        $this->appendInput("", ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "btnAddObject", "value" => $this->dictionary["LabelAdd"]
            , "openModal" => [
                "idModal" => "modalAddObject"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAdd"
                , "parameters" => ["plgID" => $plgID, "pluginObject" => $pluginObject]
            ]
        ]);

    }

    protected function getTableList($ArrayVariable)
    {

        //include obj
        $objApiUsefulData = $this->K_COMMON->getPluginObject("k_usefuldata", "api");

        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");

        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        if ($onlyRow == "1") {
            $dataTableGeneral = $objApiUsefulData->listPluginsAndObjects(["pluginName" => $plgID, "withKCore" => "1", "objectType" => "objects\\general"]);
            $dataTableWeb = $objApiUsefulData->listPluginsAndObjects(["pluginName" => $plgID, "withKCore" => "1", "objectType" => "web\\objects"]);
            $dataTablePanel = $objApiUsefulData->listPluginsAndObjects(["pluginName" => $plgID, "withKCore" => "1", "objectType" =>  "manage\\panels"]);
            $dataTable = array_merge($dataTableGeneral,$dataTableWeb,$dataTablePanel);


        } else
            $dataTable = "";

        
        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableListObjects"
                , "hideHead" => $onlyRow
                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getTableList"
            ]
                , "data" => $dataTable
                , "fVar" => ["plgID" => $plgID, "pluginObject" => $pluginObject]

                , "fields" =>
                [
                    [
                        "label" => $this->dictionary["LabelListObjName"]
                        , "qryField" => "text"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->dictionary["LabelEdit"]
                                    , "action" => "link"
                                    , "class" => "fa fa-edit"
                                    , "url" => "get/" . $plgID
                                    , "qryAttributes" => ["objType" => "objTypeFlat","objName"=>"objName"]
                                    , "urlBaseType" => "panelActual"
                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelListDelete"]
                                    , "qryAttributes" => ["pluginObject" => "value"]
                                    , "staticAttributes" => [
                                    "plgID" => $plgID
                                ]
                                    , "action" => "remove"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                                ]
                            ]
                    ]
                ]
            ]
        );
        return $OBJTableList->get();
    }

    public function getModalAdd($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->dictionary["LabelModalTitleAddObj"], "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePut($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getPagePut($ArrayVariable)
    {

        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $typeObject = "";

        //create form
        $this->appendForm("",
            [
                "id" => "objForm"
                , "setSavedObject" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "reloadTableList" => "tableListObjects"
                , "closeModal" => "modalAddObject"

            ]
        );


        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //type
        $label = $this->dictionary["LabelFormObj"];
        $name = "pluginType";
        $arrayType = ["g" => "General", "p" => "Panel", "w" => "Web"];
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $arrayType, "noEmptyOption" => "1", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $typeObject, "required" => "1"]);

        //code
        $label = $this->dictionary["LabelFormObj"];
        $name = "pluginObject";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $pluginObject, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->dictionary["LabelFormSave"];
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;

    }

    public function delete($ArrayVariable)
    {
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        if ($plgID == "" || $pluginObject == "")
            return ["success" => "false", "reason" => "inputVoid", "message" => "Please fill in the fields!"];

        $dir = $this->K_COMMON->getPathPluginObject(true, $plgID, $pluginObject);

        //remove dir
        $this->K_COMMON->emptyDir(["dir" => $dir, "rmDir" => "1"]);

        //$this->K_COMMON->rmDir(["dir" => $dir]);
        return array("success" => "true");
    }


    public function saveData($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $pluginType = $this->K_COMMON->getVarArray($ArrayVariable, "pluginType");
        $dir = $this->K_COMMON->getPathPluginObject(true, $plgID);
        $extend = "";
        $namespace = "";
        if ($plgID == "" || $pluginObject == "")
            return ["success" => "false", "reason" => "inputVoid", "message" => "Please fill in the fields!"];

        if ($pluginType == "g") {

            $dir .= "/objects/general/" . $pluginObject;
            $namespace = $plgID . '\objects\general\[toremove]' . $pluginObject;
            $extend = "\k_webmanager\objects\general\general_object\obj";
        } elseif ($pluginType == "p") {
            if (!(substr($pluginObject,0,2) == "p_") && strpos($pluginObject, "_") === FALSE)
                $pluginObject = "p_" . $pluginObject;
            $dir .= "/manage/panels/" . $pluginObject;
            $namespace = $plgID . '\manage\panels\[toremove]' . $pluginObject;
            $extend = "\k_webmanager\objects\general\base_panel\obj";
        } elseif ($pluginType == "w") {
            if (!(substr($pluginObject,0,2) == "w_") && strpos($pluginObject, "_") === FALSE)
                $pluginObject = "w_" . $pluginObject;
            $dir .= "/web/objects/" . $pluginObject;
            $namespace = $plgID . '\web\objects\[toremove]' . $pluginObject;
            $extend = "\k_webmanager\objects\general\web_object\obj";
        } else
            return ["success" => "false", "reason" => "errorSecurity", "message" => "Error Security"];

        $this->K_COMMON->mkDir(["dir" => $dir, "mask" => 0777]);

        $content =
            '<?
            namespace ' . $namespace . ';
            class obj extends ' . $extend . '
            {
                var $dictionary;

                public function __construct($AV = array())
                {
                    parent::__construct();
                    $this->dictionary = $this->getDictionary();
                }

                public function get($ArrayVariable = array())
                {
                    return $this;
                }

            }
        ?>';
        $content = str_replace("[toremove]", "", $content);
        $this->K_COMMON->saveFileContent(["dir" => $dir, "filename" => "main.php", "content" => $content]);
        return ["success" => "true"];
    }

}

?>