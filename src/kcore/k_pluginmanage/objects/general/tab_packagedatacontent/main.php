<?
namespace k_pluginmanage\objects\general\tab_packagedatacontent;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    var $ver = "1.0";

    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";
    
    var $dictionary;
    var $dicLanguages;

    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
    }

    public function get($ArrayVariable = array())
    {

        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;


        }
        return $this;
    }

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //switch request
        switch ($KCA) {
            case "buildPackageData" :
                return json_encode($this->buildPackageData($ArrayVariable));
                break;



        }
    }

    protected function getPagePut($ArrayVariable)
    {

        //set variables
        $keyIDPlg = $this->K_COMMON->getVarArray($ArrayVariable, "keyIDPlg");
        $contentType = $this->K_COMMON->getVarArray($ArrayVariable, "contentType");
        $plgParametersArray = [];

        //include table
        $tablePlugins = $this->K_COMMON->getPluginDataObject("k_common", "plugins");

        if ($keyIDPlg != "") {
            $tablePlugins->get("*", ["id_plugin" => $keyIDPlg]);
            $data = $tablePlugins->fetch();
            if ($data) {
                $contentType = $data->plg_content;
                $plgParameters = $data->plg_parameters;
                $plgParametersArray = json_decode($plgParameters, true);
            }
        }

        switch ($contentType)
        {
            case "country":
            case "language":

                /*  USEFUL DATA */

                $language = $this->K_COMMON->getVarArray($plgParametersArray, "language");

                //NEW ROW
                $this->appendRow();
                $this->appendColumn($this->lastRow, ["nCol" => 12]);
                $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->dictionary["labelFormUsefulData"], "importance" => "4", "align" => "left"]);


                //create form
                $this->appendForm("",
                    [
                        "id" => "usefuldataForm"
                        , "setSavedMessage" => "1"
                        , "kcp" => $this->namePlugin
                        , "kco" => $this->namePluginObject
                        , "kca" => "buildPackageData"

                    ]
                );

                $name = "keyIDPlg";
                $this->appendInput($this->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $keyIDPlg]);
                $name = "downloadUData";
                $this->appendInput($this->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $contentType]);


                //NEW ROW
                $this->appendRow($this->lastForm);

                //Language
                $label = "Language";
                $name = "language";
                $this->appendColumn($this->lastRow, ["nCol" => 3]);
                $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_combolanguages", ["labelInput" => $label, "name" => $name, "value" => $language]);



                //NEW ROW
                $this->appendRow($this->lastForm);

                //button
                $label = $this->dictionary["labelFormDownloadUsefulData"];
                $name = "btnDownloadUsefulData";
                $this->appendColumn($this->lastRow, ["nCol" => 3]);
                $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

                break;
            case "dictionary":

                /*  DICTIONARY */

                $language = $this->K_COMMON->getVarArray($plgParametersArray, "language");
                $pluginref = $this->K_COMMON->getVarArray($plgParametersArray, "pluginref");

                //NEW ROW
                $this->appendRow($this->lastForm);
                $this->appendColumn($this->lastRow, ["nCol" => 12]);
                $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->dictionary["labelFormDataDictionary"], "importance" => "4", "align" => "left"]);

                //create form
                $this->appendForm("",
                    [
                        "id" => "formDataDictionary"
                        , "setSavedMessage" => "1"
                        , "kcp" => $this->namePlugin
                        , "kco" => $this->namePluginObject
                        , "kca" => "buildPackageData"

                    ]
                );

                $name = "keyIDPlg";
                $this->appendInput($this->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $keyIDPlg]);



                //NEW ROW
                $this->appendRow($this->lastForm);

                //Plugin
                $label = "Plugin";
                $name = "pluginName";
                $this->appendColumn($this->lastRow, ["nCol" => 3]);
                $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_comboplugin", ["labelInput" => $label, "id" => $name, "name" => $name, "value" => $pluginref]);

                //Language
                $label = "Language";
                $name = "language";
                $this->appendColumn($this->lastRow, ["nCol" => 3]);
                $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_combolanguages", ["labelInput" => $label, "name" => $name, "value" => $language]);



                //NEW ROW
                $this->appendRow($this->lastForm);

                //button
                $label = $this->dictionary["labelFormDownloadUsefulData"];
                $name = "btnDownloadDataDictionary";
                $this->appendColumn($this->lastRow, ["nCol" => 3]);
                $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

                break;
            case "plugindata":

                /*  DATI PLUGIN */

                $pluginref = $this->K_COMMON->getVarArray($plgParametersArray, "pluginref");
                $relateddata = $this->K_COMMON->getVarArray($plgParametersArray, "relateddata");


                //NEW ROW
                $this->appendRow($this->lastForm);
                $this->appendColumn($this->lastRow, ["nCol" => 12]);
                $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->dictionary["labelFormDataPluginData"], "importance" => "4", "align" => "left"]);

                //create form
                $this->appendForm("",
                    [
                        "id" => "packageFormData"
                        , "setSavedMessage" => "1"
                        , "kcp" => $this->namePlugin
                        , "kco" => $this->namePluginObject
                        , "kca" => "buildPackageData"

                    ]
                );

                $name = "keyIDPlg";
                $this->appendInput($this->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $keyIDPlg]);


                //NEW ROW
                $this->appendRow($this->lastForm);

                //plugin
                $label = $this->dictionary["labelFormPlugin"];
                $this->appendColumn($this->lastRow, ["nCol" => 4]);
                $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_comboplugin", ["labelInput" => $label, "required" => "1", "value" => $pluginref]);


                //NEW ROW
                $this->appendRow($this->lastForm);

                $label = $this->dictionary["labelFormDownloadRelatedData"];
                $name = "relateddata";
                $this->appendColumn($this->lastRow, ["nCol" => 3]);
                $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $relateddata]);


                //NEW ROW
                $this->appendRow($this->lastForm);

                //button
                $label = $this->dictionary["labelFormDownloadUsefulData"];
                $name = "btnDownloadPluginData";
                $this->appendColumn($this->lastRow, ["nCol" => 3]);
                $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);


                break;
        }








    }

    public function buildPackageData($ArrayVariable)
    {

        //set variable
        $keyIDPlg = $this->K_COMMON->getVarArray($ArrayVariable, "keyIDPlg");
        $pluginName = $this->K_COMMON->getVarArray($ArrayVariable, "pluginName");
        $relateddata = $this->K_COMMON->getVarArray($ArrayVariable, "relateddata");
        $language = $this->K_COMMON->getVarArray($ArrayVariable, "language");
        $contentType = "";
        $plgRelated = "{}";
        $plgParameters = "{}";

        //include table
        $tablePlugins = $this->K_COMMON->getPluginDataObject("k_common", "plugins");



        if ($keyIDPlg != "") {
            $tablePlugins->get("*", ["id_plugin" => $keyIDPlg]);
            $data = $tablePlugins->fetch();
            if ($data) {
                $contentType = $data->plg_content;
            }
        }

        switch ($contentType)
        {
            case "country":
            case "language":

                $plgParameters = '{"language":"'.$language.'"}';

            break;
            case "dictionary":
                $plgRelated = '{"kplugin":{"id":"'.$pluginName.'"}}';
                $plgParameters = '{"pluginref" : "'.$pluginName.'", "language":"'.$language.'"}';

                break;
            case "plugindata":
                $plgRelated = '{"kplugin":{"id":"'.$pluginName.'"}}';
                $plgParameters = '{"pluginref" : "'.$pluginName.'", "relateddata":"'.$relateddata.'"}';
                break;
        }



        //save data plugins
        $arrayPutPlugins = [
            "plg_related" => $plgRelated,
            "plg_parameters" => $plgParameters
        ];
        $tablePlugins->put($arrayPutPlugins,["id_plugin" => $keyIDPlg]);

        return ["success" => "true"];


    }




}

?>