<?
namespace k_pluginmanage\objects\general\tab_queries;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    var $ver = "1.0";

    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";
    
    var $dictionary;

    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();

    }

    //manage action from ajax
    public function objectRequest($ArrayVariable)
    {
        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "saveData":
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "getModalAdd":
                return json_encode($this->getModalAdd($ArrayVariable));
                break;
            case "delete":
                return json_encode($this->delete($ArrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableList($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
        }
    }

    public function get($ArrayVariable = array())
    {

        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
                $this->getPageList($ArrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    protected function getPageList($ArrayVariable)
    {
        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");


        $listMyMList = $this->getTableList($ArrayVariable);
        $this->appendHTML("", $listMyMList);
        $this->appendInput("", ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "btnAddQuery", "value" => $this->dictionary["LabelAdd"]
            , "openModal" => [
                "idModal" => "modalAddQuery"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAdd"
                , "parameters" => ["plgID" => $plgID, "pluginObject" => $pluginObject]
            ]
        ]);

    }

    protected function getTableList($ArrayVariable)
    {

        //include obj
        $objApiUsefulData = $this->K_COMMON->getPluginObject("k_usefuldata", "api");

        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $queryName = $this->K_COMMON->getVarArray($ArrayVariable, "queryName");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");
        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        if ($onlyRow == "1") {
            $dataTable = $objApiUsefulData->listPluginsAndObjects(["pluginName" => $plgID, "withKCore" => "1", "objectType" => "data\\queries"]);
        } else
            $dataTable = "";
        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableListQueries"
                , "hideHead" => $onlyRow
                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getTableList"
            ]
                , "data" => $dataTable
                , "fVar" => ["plgID" => $plgID, "queryName" => $queryName]

                , "fields" =>
                [
                    [
                        "label" => $this->dictionary["LabelListQueryName"]
                        , "qryField" => "text"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->dictionary["LabelEdit"]
                                    , "qryAttributes" => ["queryName" => "objName"]
                                    , "staticAttributes" =>
                                    [
                                        "plgID" => $plgID
                                    ]
                                    , "action" => "editModal"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]

                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelListDelete"]
                                    , "qryAttributes" => ["queryName" => "objName"]
                                    , "staticAttributes" =>
                                    [
                                        "plgID" => $plgID
                                    ]
                                    , "action" => "remove"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                                ]
                            ]
                    ]
                ]
            ]
        );
        return $OBJTableList->get();
    }

    public function getModalAdd($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->dictionary["LabelModalTitleAddQuery"], "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePut($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getPagePut($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $queryName = $this->K_COMMON->getVarArray($ArrayVariable, "queryName");
        $queryFields = "";
        $queryFrom = "";
        $queryWhere = "";
        $queryOrder = "";
        $queryGroup = "";
        $queryHaving = "";

        if ($queryName != "") {
            //set variable
            $pathPlugin = $this->K_COMMON->getPathPluginObject(true, $plgID, "");
            $fileQuery = $pathPlugin . "/data/queries/" . $queryName . "/definition/query/query.xml";

            //check if file Exist
            if (file_exists($fileQuery)) {

                //load xml file
                $xml = simplexml_load_file($fileQuery);

                //scan xml file
                $queryFields = (string)$xml->query->fields;
                $queryFrom = (string)$xml->query->from;
                $queryWhere = (string)$xml->query->where;
                $queryOrder = (string)$xml->query->order;
                $queryGroup = (string)$xml->query->group;
                $queryHaving = (string)$xml->query->having;
            } else
                $queryName = "";

        }


        $arrayTabs[] = ["id" => "tabQueryName", "label" => $this->dictionary["LabelFormQueryName"], "active" => "1"];
        $arrayTabs[] = ["id" => "tabQueryFields", "label" => $this->dictionary["LabelFormQueryFields"]];
        $arrayTabs[] = ["id" => "tabQueryFrom", "label" => $this->dictionary["LabelFormQueryFrom"]];
        $arrayTabs[] = ["id" => "tabQueryWhere", "label" => $this->dictionary["LabelFormQueryWhere"]];
        $arrayTabs[] = ["id" => "tabQueryOrder", "label" => $this->dictionary["LabelFormQueryOrder"]];
        $arrayTabs[] = ["id" => "tabQueryGroup", "label" => $this->dictionary["LabelFormQueryGroup"]];
        $arrayTabs[] = ["id" => "tabQueryHaving", "label" => $this->dictionary["LabelFormQueryHaving"]];

        //create form
        $this->appendForm("",
            [
                "id" => "objForm"
                , "setSavedObject" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "reloadTableList" => "tableListQueries"
                , "closeModal" => "modalAddQuery"

            ]
        );

        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);

        //create tabs
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendTab($this->lastCol,
            [
                "id" => "tabPluginManage"
                , "tabs" => $arrayTabs
            ]
        );

        //NEW ROW
        $this->appendRow("tabQueryName");

        //name
        $label = "";
        $name = "queryName";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $queryName, "required" => "1"]);

        //NEW ROW
        $this->appendRow("tabQueryFields");

        //query fields
        $label = "";
        $name = "queryFields";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "textarea", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $queryFields, "required" => ""]);


        //NEW ROW
        $this->appendRow("tabQueryFrom");

        //query from
        $label = "";
        $name = "queryFrom";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "textarea", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $queryFrom, "required" => ""]);

        //NEW ROW
        $this->appendRow("tabQueryWhere");

        //query where
        $label = "";
        $name = "queryWhere";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "textarea", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $queryWhere, "required" => ""]);

        //NEW ROW
        $this->appendRow("tabQueryOrder");

        //query order
        $label = "";
        $name = "queryOrder";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "textarea", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $queryOrder, "required" => ""]);

        //NEW ROW
        $this->appendRow("tabQueryGroup");

        //query group
        $label = "";
        $name = "queryGroup";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "textarea", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $queryGroup, "required" => ""]);

        //NEW ROW
        $this->appendRow("tabQueryHaving");

        //query having
        $label = "";
        $name = "queryHaving";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "textarea", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $queryHaving, "required" => ""]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->dictionary["LabelFormSave"];
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;

    }

    public function delete($ArrayVariable)
    {
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $queryName = $this->K_COMMON->getVarArray($ArrayVariable, "queryName");


        $dir = $this->K_COMMON->getPathPluginObject(true, $plgID);
        if ($plgID == "" || $queryName == "")
            return ["success" => "false", "reason" => "inputVoid", "message" => "Please fill in the fields!"];

        $dir .= "/data/queries/" . $queryName;

        //remove dir
        $this->K_COMMON->emptyDir(["dir" => $dir, "rmDir" => "1"]);

        //$this->K_COMMON->rmDir(["dir" => $dir]);
        return array("success" => "true");
    }


    public function saveData($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $queryName = $this->K_COMMON->getVarArray($ArrayVariable, "queryName");
        $queryFields = $this->K_COMMON->getVarArray($ArrayVariable, "queryFields");
        $queryFrom = $this->K_COMMON->getVarArray($ArrayVariable, "queryFrom");
        $queryWhere = $this->K_COMMON->getVarArray($ArrayVariable, "queryWhere");
        $queryGroup = $this->K_COMMON->getVarArray($ArrayVariable, "queryGroup");
        $queryHaving = $this->K_COMMON->getVarArray($ArrayVariable, "queryHaving");
        $queryOrder = $this->K_COMMON->getVarArray($ArrayVariable, "queryOrder");


        $dir = $this->K_COMMON->getPathPluginObject(true, $plgID);
        if ($plgID == "" || $queryName == "")
            return ["success" => "false", "reason" => "inputVoid", "message" => "Please fill in the fields!"];

        $queryPath = $dir . "/data/queries/" . $queryName . "/definition/query";
        $this->K_COMMON->mkDir(["dir" => $queryPath, "mask" => 0777]);


        //create tag
        $xml = new \k_common\objects\general\xml\obj('<queries/>');
        $xmlFunction = $xml->addChild("query");

        //create other tag
        $s = $xmlFunction->addChild("name");
        $s->addCData($queryName);

        $s = $xmlFunction->addChild("fields");
        $s->addCData($queryFields);

        $s = $xmlFunction->addChild("from");
        $s->addCData($queryFrom);

        $s = $xmlFunction->addChild("where");
        $s->addCData($queryWhere);

        $s = $xmlFunction->addChild("group");
        $s->addCData($queryGroup);

        $s = $xmlFunction->addChild("order");
        $s->addCData($queryOrder);

        $s = $xmlFunction->addChild("having");
        $s->addCData($queryHaving);


        $this->K_COMMON->saveXMLToFile($xml, $queryPath, "query.xml");

        //$this->K_COMMON->saveFileContent(["dir" => $queryPath, "filename" => "query.xml", "content" => $content]);
        return ["success" => "true"];
    }

}

?>