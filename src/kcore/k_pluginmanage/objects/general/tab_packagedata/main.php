<?

namespace k_pluginmanage\objects\general\tab_packagedata;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    var $ver = "1.0";

    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";

    var $dictionary;
    var $dicLanguages;
    var $packageContentType = "";
    var $packageKeyIDPlg = "";

    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
    }

    public function get($ArrayVariable = array())
    {

        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //switch request
        switch ($KCA) {
            case "saveData" :
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "downloadPackage" :
                return $this->downloadPackage($ArrayVariable);
                break;
            case "rebuildPackage" :
                return json_encode($this->rebuildPackage($ArrayVariable));
                break;
            case "delete" :
                $objPlugin = $this->getObject("tab_packageplugin");
                return json_encode($objPlugin->delete($ArrayVariable));
                break;


        }
    }

    protected function getPagePut($ArrayVariable)
    {

        //include object
        $objPlugins = $this->K_COMMON->getPluginObject("k_common", "plugins");

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $keyIDPlg = $this->K_COMMON->getVarArray($ArrayVariable, "keyIDPlg");
        $optionContentType = ["country" => "Country", "language" => "Languages", "dictionary" => "Dictionary", "plugindata" => "Plugin Data"];
        $plgName = "";
        $contentType = "";
        $plgVer = "1.0.0";
        $plgDescr = "";
        $plgAuthors = "";
        $plgRelated = "";
        $disabled = "0";
        $plgIDName = "";


        if ($plgID != "") {
            //get info plugin
            $infoPlugin = $objPlugins->getPluginInfo(["plgID" => $plgID]);
            $plgName = $infoPlugin["plg_name"];
            $plgIDName = $infoPlugin["plg_id_name"];
            $contentType = $infoPlugin["plg_content"];
            $plgVer = $infoPlugin["plg_ver"];
            $plgDescr = $infoPlugin["plg_description"];
            $plgAuthors = $infoPlugin["plg_authors"];

            $disabled = "1";


        }

        $this->packageContentType = $contentType;
        $this->packageKeyIDPlg = $keyIDPlg;


        //create form
        $this->appendForm("",
            [
                "id" => "pluginForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "reloadPage" => "1"
                , "reloadPageCheckCode" => "keyIDPlg"
                , "reloadPageGetCode" => "plgID"
            ]
        );


        //NEW ROW
        $this->appendRow($this->lastForm);

        $name = "keyIDPlg";
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $keyIDPlg]);

        //plugin id
        if ($keyIDPlg != "") { //if edit create input hidden
            //keyID hidden
            $name = "plgID";
            $this->appendInput($this->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $plgIDName]);

            $name = "contentType";
            $this->appendInput($this->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $contentType]);


            //plugin id
            $label = $this->dictionary["LabelPluginID"];
            $name = "";
            $this->appendColumn($this->lastRow, ["nCol" => 4]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $plgIDName, "required" => "1", "disabled" => $disabled]);
        } else {

            //plugin id
            $label = $this->dictionary["LabelPluginID"];
            $name = "plgID";
            $textHelp = $this->dictionary["plgIDTextHelp"];
            $labelPlaceHolder = "Ex: articlesdictionaryitalian";
            $this->appendColumn($this->lastRow, ["nCol" => 4]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $plgIDName, "required" => "1", "disabled" => $disabled, "textHelp" => $textHelp, "labelPlaceholder" => $labelPlaceHolder, "attributes" => ' pattern="[a-z0-9]{1,}"']);
        }

        //plugin ver
        $label = $this->dictionary["LabelPluginVer"];
        $name = "plgVer";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $plgVer]);

        //content type
        $label = $this->dictionary["LabelContentType"];
        $name = "contentType";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $optionContentType, "labelInput" => $label, "id" => $name, "name" => $name, "value" => $contentType, "required" => "1", "disabled" => $disabled]);


        //NEW ROW
        $this->appendRow($this->lastForm);

        //plugin name
        $label = $this->dictionary["LabelPluginName"];
        $name = "plgName";
        $this->appendColumn($this->lastRow, ["nCol" => 7]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $plgName, "attributes" => ' pattern="[a-Z0-9]{1,}"']);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //plugin name
        $label = $this->dictionary["LabelPluginDescr"];
        $name = "plgDescr";
        $this->appendColumn($this->lastRow, ["nCol" => 7]);
        $this->appendInput($this->lastCol, ["type" => "textarea", "labelInput" => $label, "id" =>
            $name, "name" => $name, "value" => $plgDescr]);

        //Heading Authors
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->dictionary["LabelAuthorsTitle"], "importance" => "5"]);
        $this->appendHTML($this->lastCol, "<small><center>" . $this->dictionary["LabelAuthorsSubTitle"] . "</center></small><br>");


        //New ROW AUTHORS

        $plgAuthorsVal = $this->K_COMMON->getVarArray(json_decode($plgAuthors, true), "author");
        $name = "plgAuthors";
        $this->appendListGroup($this->lastForm,
            [
                "id" => $name
                , "attributes" => 'style="height:176; overflow: scroll; overflow-x: hidden;"'
                , "values" => $plgAuthorsVal
                , "addingValues" =>
                [
                    "valuesHName" => "authors"
                    , "fields" => [
                    ["type" => "text", "labelInput" => $this->dictionary["LabelAuthorName"], "name" => "name"]
                    , ["type" => "text", "labelInput" => $this->dictionary["LabelAuthorEmail"], "name" => "email"]
                    , ["type" => "text", "labelInput" => $this->dictionary["LabelAuthorWebSite"], "name" => "www"]
                ]
                ]
            ]
        );


        $this->appendRow($this->lastForm);
        //button
        $label = $this->dictionary["LabelSave"];
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);


    }


    protected function saveData($ArrayVariable)
    {

        //include table
        $tablePlugins = $this->K_COMMON->getPluginDataObject("k_common", "plugins");

        //set variable for plugin tab
        $keyIDPlg = $this->K_COMMON->getVarArray($ArrayVariable, "keyIDPlg");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $plgType = "kdata";
        $contentType = $this->K_COMMON->getVarArray($ArrayVariable, "contentType");
        $plgName = $this->K_COMMON->getVarArray($ArrayVariable, "plgName");
        $plgVer = $this->K_COMMON->getVarArray($ArrayVariable, "plgVer");
        $plgLang = $this->K_COMMON->getVarArray($ArrayVariable, "plgLang");
        $plgDescr = $this->K_COMMON->getVarArray($ArrayVariable, "plgDescr");
        $authors = $this->K_COMMON->getVarArray($ArrayVariable, "authors");
        $relatedKplugin = $this->K_COMMON->getVarArray($ArrayVariable, "relatedKplugin");
        $relatedktemplate = $this->K_COMMON->getVarArray($ArrayVariable, "relatedktemplate");
        if ($authors == "") $authors = "{}";
        if ($relatedKplugin == "") $relatedKplugin = "{}";
        if ($relatedktemplate == "") $relatedktemplate = "{}";
        $plgRelated = '{"kplugin":' . $relatedKplugin . ',"ktemplate":' . $relatedktemplate . '}';
        $authors = '{"author":' . $authors . '}';
        $plgRoot = $this->K_COMMON->getPluginRootByType($plgType);

        //check variable
        if (strpos($plgName, "_") != "") {
            return ["success" => "false", "reason" => "pluginNameError"];
        }

        if ($plgID == "")
            return ["success" => "false", "reason" => "plgIDNull"];

        if (strpos($plgID, "_") !== false)
            return ["success" => "false", "reason" => "charNotAllowed"];

        $plgID = $plgRoot . $plgID;
        $dir = $this->K_COMMON->getPathKData();

        if ($keyIDPlg != "") {
            $tablePlugins->get(["id_plugin"], ["plg_id" => $plgID]);
            $data = $tablePlugins->fetch();
            if ($data) {
                if ($data->id_plugin != $keyIDPlg)
                    return ["success" => "false", "reason" => "errorSecurity"];
            }
        } else {
            //verify if plugin exist on db
            $tablePlugins->get(["plg_id"], ["plg_type" => $plgType, "plg_id" => $plgID]);
            $data = $tablePlugins->fetch();
            if ($data)
                return ["success" => "false", "reason" => "plginAlreadyExist", "message" => $this->dictionary["emsgPluginExist"]];

            // verify if plugin exist on dir
            if ($dir != "") {
                $dir .= "/" . $plgID;
                if (is_dir($dir) === true)
                    return ["success" => "false", "reason" => "dirAlreadyExist", "message" => $this->dictionary["emgDirPluginExist"]];
            }
        }

        //save data plugins
        $arrayPutPlugins = [
            "plg_id" => $plgID
            , "plg_type" => $plgType
            , "plg_content" => $contentType
            , "plg_name" => $plgName
            , "plg_ver" => $plgVer
            , "plg_language" => $plgLang
            , "plg_description" => $plgDescr
            , "plg_authors" => $authors
            , "plg_related" => $plgRelated
        ];
        $tablePlugins->putByKey($arrayPutPlugins);

        $this->K_COMMON->mkDir(["dir" => $dir, "mask" => 0777]);
        $keyID = $tablePlugins->getNewID();
        return array("success" => "true", "message" => $this->dictionary["msgSuccessSave"], "plgID" => $plgID, "keyID" => $keyID);
    }

    function getPageList($ArrayVariable)
    {

        //set variables
        $pluginParam = $this->K_COMMON->getVarArray($ArrayVariable, "pluginParam");
        $urlActual = $this->K_COMMON->getVarArray($ArrayVariable, "panelUrlActual");

        $searchText = $pluginParam;

        //add files to include
        $this->addHeadFile([$this->getPathFileJS("list")]);

        //NEW ROW
        $this->appendRow("");

        //get Table List
        $objTableList = $this->getTableList();
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendObject("", $objTableList);

        //NEW ROW
        $this->appendRow("");

        //button ADD New Plugin
        $label = $this->dictionary["LabelAddPlugin"];
        $name = "btnAdd";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btn", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success", "link" => $urlActual . "put"]);

        return $this;

    }

    protected function getTableList($ArrayVariable = array())
    {

        //set object
        $onlyRow = "";
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        //include obj
        $objApiUsefulData = $this->K_COMMON->getPluginObject("k_usefuldata", "api");

        $listPlugin = $objApiUsefulData->listPluginsAndObjects(["pluginType" => ["KData" => "1"]]);

        $dataTable = $listPlugin;
        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableList"
                , "tableID" => "tableList"
                , "hideHead" => $onlyRow
                , "data" => $dataTable
                , "fields" =>
                [
                    [
                        "label" => $this->dictionary["LabelPluginName"]
                        , "qryField" => "text"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->dictionary["LabelEdit"]
                                    , "action" => "link"
                                    , "class" => "fa fa-edit"
                                    , "qryAttributes" => ["get" => "value"]
                                    , "urlBaseType" => "panelActual"
                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelRebuild")
                                    , "action" => "modalConfirm"
                                    , "icon" => "fa-save"
                                    , "qryAttributes" => [
                                    "pluginName" => "value"
                                ]
                                    , "ajax" => [
                                    "kcp" => $this->getPluginName()
                                    , "kco" => $this->getPluginObjectName()
                                    , "kca" => "rebuildPackage"
                                ]
                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelDownload")
                                    , "action" => "download"
                                    , "icon" => "fa-download"
                                    , "qryAttributes" => [
                                    "pluginName" => "value"
                                ]
                                    , "ajax" => [
                                    "kcp" => $this->getPluginName()
                                    , "kco" => $this->getPluginObjectName()
                                    , "kca" => "downloadPackage"
                                ]
                                ]

                                ,
                                [
                                    "title" => $this->dictionary["LabelDelete"]
                                    , "action" => "remove"
                                    , "qryAttributes" => ["keyID" => "value"]
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                                ]


                            ]
                    ]
                ]
            ]
        );
        return $OBJTableList->get();

    }


    public function downloadPackage($ArrayVariable)
    {

        //set object
        $objDownload = $this->getObject("builder");
        $ArrayVariable["webCode"] = $this->K_COMMON->getEditingWeb();


        return $objDownload -> downloadPackage($ArrayVariable);



    }

    public function rebuildPackage($ArrayVariable)
    {

        //set object
        $objDownload = $this->getObject("builder");
        $ArrayVariable["webCode"] = $this->K_COMMON->getEditingWeb();

        return $objDownload -> rebuildPackage($ArrayVariable);


    }

}

?>