<?
/**
 * PLUGIN MANAGE (Actions)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_pluginmanage
 * @subpackage objects\actions
 * @since 1.00
 */
namespace k_pluginmanage\objects\general\tab_actions;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Authorization Plugin that Authoruze
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authPlugin = "k_common";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authDev";


    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {

        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "saveData":
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "getModalAdd":
                return json_encode($this->getModalAdd($ArrayVariable));
                break;
            case "delete":
                return json_encode($this->delete($ArrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableList($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
        }
    }

    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "put", "get")
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {

        //check authorization
        if (!$this->isDeveloper())
            return array("success" => "false", "reason" => "securityError");

        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
                $this->getPageList($ArrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    protected function getPageList($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");

        $listMyMList = $this->getTableList($ArrayVariable);

        $this->appendHTML("", $listMyMList);

        $this->appendInput("", ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "btnAddAction", "value" => $this->getDictionaryWord("LabelAdd")
            , "openModal" => [
                "idModal" => "modalAddAction"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAdd"
                , "parameters" => ["plgID" => $plgID, "pluginObject" => $pluginObject]
            ]
        ]);

    }

    protected function getTableList($ArrayVariable)
    {

        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");

        //get pluginObject html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        if ($onlyRow == "1") {
            //Load Table
            $table = $this->K_COMMON->getPluginDataObject("k_mailing", "actions");
            $table->get(["id_action", "act_code", "act_name", "act_plugin_name", "act_plugin_object"],
                ["act_web_code" => "", "act_plugin_name" => $plgID, "act_plugin_object" => $pluginObject]);
            $dataTable = $table->getResults("array");
        } else
            $dataTable = "";


        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableListAction"
                , "tableID" => "tableListAction"
                , "hideHead" => $onlyRow
                , "data" => $dataTable
                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getTableList"]
                , "fVar" => ["plgID" => $plgID, "pluginObject" => $pluginObject]

                , "fields" =>
                [
                    [
                        "label" => $this->getDictionaryWord("LabelListCode")
                        , "qryField" => "act_code"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListName")
                        , "qryField" => "act_name"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->getDictionaryWord("LabelListEdit")
                                    , "action" => "editModal"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                    , "qryAttributes" => [
                                    "keyID" => "id_action"
                                    , "actCode" => "act_code"
                                    , "plgID" => "act_plugin_name"
                                    , "pluginObject" => "act_plugin_object"
                                ]

                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelListDelete")
                                    , "qryAttributes" => [
                                    "keyID" => "id_action"
                                    , "actCode" => "act_code"
                                    , "plgID" => "act_plugin_name"
                                    , "pluginObject" => "act_plugin_object"
                                ]
                                    , "action" => "remove"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                                ]
                            ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();

    }

    protected function getModalAdd($ArrayVariable)
    {
        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->getDictionaryWord("LabelModalTitleAddAction"), "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePut($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getPagePut($ArrayVariable)
    {

        //include script
        $this->addHeadFile([$this->getPathFileJS("put")]);


        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $actCode = $this->K_COMMON->getVarArray($ArrayVariable, "actCode");

        $value_actionCode = "";
        $value_actionName = "";
        $value_actionMailFrom = "";
        $value_actionMailTo = "";
        $value_actionMailFromOptions = "";
        $value_actionMailToOptions = "";
        $value_messageCode = "";
        $option_actionMailFrom = "";
        $option_actionMailTo = "";
        //Load Table
        $table = $this->K_COMMON->getPluginDataObject("k_mailing", "actions");
        $table->get(["act_code", "act_name", "act_email_from", "act_email_to", "act_message_code"],
            ["act_web_code" => "", "act_plugin_name" => $plgID, "act_plugin_object" => $pluginObject, "act_code" => $actCode, "id_action" => $keyID]);
        $data = $table->fetch();
        if ($data) {
            $value_actionCode = $data->act_code;
            $value_actionName = $data->act_name;
            $value_messageCode = $data->act_message_code;

            $value_actionMailFrom = $data->act_email_from;
            $value_actionMailTo = $data->act_email_to;

            $value_actionMailFromOptions = $value_actionMailFrom;
            $value_actionMailToOptions = $value_actionMailTo;

        }
        //create form
        $this->appendForm("",
            [
                "id" => "actionForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "reloadTableList" => "tableListAction"
                , "closeModal" => "modalAddAction"

            ]
        );

        //keyID hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "keyID", "value" => $keyID]);

        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);

        //pluginObject
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginObject", "name" => "pluginObject", "value" => $pluginObject]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //code
        $label = $this->getDictionaryWord("LabelFormCode");
        $name = "actionCode";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_actionCode, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //name
        $label = $this->getDictionaryWord("LabelFormName");
        $name = "actionName";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_actionName, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //Email From Option
        $emailFromOption = [
            [
                "value" => ""
                , "text" => $this->getDictionaryWord("LabelFixedEmailFrom")
                , "fixed" => "1"

            ]
            ,
            [
                "value" => "WEBEMAIL"
                , "text" => $this->getDictionaryWord("LabelDefaultWebEmailFrom")
                , "fixed" => "0"
            ]
            ,
            [
                "value" => "USEREMAILFROM"
                , "text" => $this->getDictionaryWord("LabelUserEmailFrom")
                , "fixed" => "0"
            ]
        ];
        $label = $this->getDictionaryWord("LabelFormActionEmailFrom");
        $name = "actionMailFromOption";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $emailFromOption, "noEmptyOption" => "1", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_actionMailFromOptions]);

        //email From Custom
        $label = "";
        $name = "actionMailFrom";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_actionMailFrom, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //email To Option
        $emailToOption = [
            [
                "value" => ""
                , "text" => $this->getDictionaryWord("LabelFixedEmailTo")
                , "fixed" => "1"
            ]
            ,
            [
                "value" => "WEBEMAILTO"
                , "text" => $this->getDictionaryWord("LabelDefaultWebEmailTo")
                , "fixed" => "0"
            ]
            ,
            [
                "value" => "USEREMAIL"
                , "text" => $this->getDictionaryWord("LabelUserEmailTo")
                , "fixed" => "0"
            ]

        ];
        $label = $this->getDictionaryWord("LabelFormActionEmailTo");
        $name = "actionMailToOption";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $emailToOption, "noEmptyOption" => "1", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_actionMailToOptions]);

        //email To Custom
        $label = "";
        $name = "actionMailTo";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_actionMailTo, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //message code
        $label = $this->getDictionaryWord("LabelFormMessage");

        $objMessage = $this->K_COMMON->getPluginObject("k_mailing","messages");
        $listMessage = $objMessage->getArrayListMessage(["webCode" => "", "plgID" => $plgID, "pluginObject" => $pluginObject]);
        $name = "messageCode";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $listMessage, "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_messageCode, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->getDictionaryWord("LabelFormSave");
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;

    }

    protected function delete($ArrayVariable)
    {
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $actCode = $this->K_COMMON->getVarArray($ArrayVariable, "actCode");

        $table = $this->K_COMMON->getPluginDataObject("k_mailing", "actions");
        $table->delete(["act_web_code" => "", "id_action" => $keyID, "act_plugin_name" => $plgID, "act_plugin_object" => $pluginObject, "act_code" => $actCode]);

        return array("success" => "true");
    }

    protected function saveData($ArrayVariable)
    {


        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $actionCode = $this->K_COMMON->getVarArray($ArrayVariable, "actionCode");
        $actionName = $this->K_COMMON->getVarArray($ArrayVariable, "actionName");
        $actionMailFrom = $this->K_COMMON->getVarArray($ArrayVariable, "actionMailFrom");
        $actionMailTo = $this->K_COMMON->getVarArray($ArrayVariable, "actionMailTo");
        $actionMailFromOption = $this->K_COMMON->getVarArray($ArrayVariable, "actionMailFromOption");
        $actionMailToOption = $this->K_COMMON->getVarArray($ArrayVariable, "actionMailToOption");
        $messageCode = $this->K_COMMON->getVarArray($ArrayVariable, "messageCode");
        $arrayWhere = "";
        //include tables
        $table = $this->K_COMMON->getPluginDataObject("k_mailing", "actions");
        if ($actionMailFrom == "")
            $actionMailFrom = $actionMailFromOption;
        if ($actionMailTo == "")
            $actionMailTo = $actionMailToOption;

        if ($plgID == "" || $actionCode == "" || $messageCode == "")
            return ["success" => "false", "reason" => "inputVoid", "message" => "Please fill in the fields!"];


        if ($keyID != "")
            $arrayWhere = ["id_action" => $keyID];
        else {
            //check if cod dup
            $table->get(["id_action"], ["act_plugin_name" => $plgID, "act_plugin_object" => $pluginObject, "act_code" => $actionCode]);
            $data = $table->fetch();
            if ($data)
                return ["success" => "false", "reason" => "actionCodeDup", "message" => $this->getDictionaryWord("emsgActCodeDup")];

        }
        $table->put(
            [
                "act_web_code" => ""
                , "act_plugin_name" => $plgID
                , "act_plugin_object" => $pluginObject
                , "act_code" => $actionCode
                , "act_name" => $actionName
                , "act_email_from" => $actionMailFrom
                , "act_email_to" => $actionMailTo
                , "act_message_code" => $messageCode
            ], $arrayWhere
        );
        return ["success" => "true"];
    }


}

?>