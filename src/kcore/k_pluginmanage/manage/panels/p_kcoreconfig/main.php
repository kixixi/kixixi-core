<?
/**
 * KIXIXI k_pluginmanage (p_kcoreconfig)
 * Manage Plugin
 *
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_pluginmanage
 * @subpackage objects\p_pluginconfig
 * @since 1.00
 */
namespace k_pluginmanage\manage\panels\p_kcoreconfig;
class obj extends \k_webmanager\objects\general\base_panel\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * actual url of panel
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $urlActual = "";

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
    }


    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "put", "get")
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {

        //set variables
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");
        $this->urlActual = $this->K_COMMON->getVarArray($ArrayVariable, "panelUrlActual");

        switch ($pluginMethod) {
            case "list":
            case "":
                $ArrayVariable["panelUrlActual"] = $this->urlActual;
                $ArrayVariable["plgType"] = "KCore";
                $objPlugin = $this->getObject("tab_packageplugin");
                $this->appendObject("", $objPlugin->getPageList($ArrayVariable));
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }

        //Return
        return $this;
    }

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //switch request
        switch ($KCA) {
            case "delete" :
                $objPlugin = $this->getObject("tab_packageplugin");
                return json_encode($objPlugin->delete($ArrayVariable));
                break;

        }
    }
    protected function getPagePut($ArrayVariable = array())
    {
        $objPlugin = $this->getObject("tab_packageplugin")->get($ArrayVariable);
        $this->appendObject("", $objPlugin);
    }



}

?>