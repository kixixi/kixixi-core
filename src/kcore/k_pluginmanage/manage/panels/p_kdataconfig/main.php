<?
/**
 * KIXIXI k_pluginmanage (p_kpluginconfig)
 * Manage KData
 *
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_pluginmanage
 * @subpackage objects\p_dataconfig
 * @since 1.00
 */
namespace k_pluginmanage\manage\panels\p_kdataconfig;
class obj extends \k_webmanager\objects\general\base_panel\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * actual url of panel
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $urlActual = "";

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
    }


    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "put", "get")
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {

        //set variables
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");
        $this->urlActual = $this->K_COMMON->getVarArray($ArrayVariable, "panelUrlActual");

        switch ($pluginMethod) {
            case "list":
            case "":
                $ArrayVariable["panelUrlActual"] = $this->urlActual;
                $objPlugin = $this->getObject("tab_packagedata");
                $this->appendObject("", $objPlugin->getPageList($ArrayVariable));
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }

        //Return
        return $this;
    }


    protected function getPagePut($ArrayVariable = array())
    {


        //include obj
        $objPlugins = $this->K_COMMON->getPluginObject("k_common", "plugins");

        //set variables
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");
        $pluginParam = $this->K_COMMON->getVarArray($ArrayVariable, "pluginParam");
        $extraParam = $this->K_COMMON->getVarArray($ArrayVariable, "urlExtraParam");
        $objName = $this->K_COMMON->getVarArray($extraParam, "objName");
        $objType = $this->K_COMMON->getVarArray($extraParam, "objType");

        if ($objName != "") {
            $objName = substr($objType, 0, strpos($objType, "_")) . "\\" . substr($objType, strpos($objType, "_") + 1) . "\\" . $objName;
        }
        $objViewer = $this->K_COMMON->getObjectDescByName($objName);
        $keyIDPlg = "";
        $plgID = $pluginParam;
        
        //Add name on top
        $this->appendRow("");
        $colTitle = $this->appendColumn($this->lastRow, ["nCol" => 12, "style" => "text-align: center;"]);
        $this->appendRow("");
        $this->appendColumn($this->lastRow, ["nCol" => 12]);

        //get info plugin
        if ($pluginMethod == "get") {
            if ($plgID != "") {
                $infoPlugin = $objPlugins->getPluginInfo(["plgID" => $plgID]);
                $pluginName = $infoPlugin["plg_name"];
                $keyIDPlg = $infoPlugin["id_plugin"];
                $title = $pluginName;
                $this->appendHTML($colTitle, "<H2>" . $title . "</H2>");
                if ($objName != "")
                    $this->appendHTML($colTitle, "<br><H4>" . $objViewer . "</H4>");

                if ($objName != "") {

                    $this->appendWO("", "k_htmlcomponents", "web\objects\w_breadcrumbs",
                        [
                            "crumbs" => [$pluginName => $this->urlActual . "get/" . $plgID, $objViewer => ""]
                        ]
                    );
                }

                //plugin
                $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);

            }
        }


        //Create TABS Array
        $arrayTabs = [];
        $arrayTabs[] = ["id" => "tabPlugin", "label" => $this->dictionary["LabelTabPlugin"], "active" => "1"];

        if ($keyIDPlg != "") {
            $arrayTabs[] = ["id" => "tabPackage", "label" => $this->dictionary["LabelTabPackage"]];
            $arrayTabs[] = ["id" => "tabContent", "label" => $this->dictionary["LabelTabContent"]];

        }

        //create tabs
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendTab($this->lastCol,
            [
                "id" => "tabPluginManage"
                , "tabs" => $arrayTabs
            ]
        );
        $arrayParam = ["keyIDPlg" => $keyIDPlg, "plgID" => $plgID, "pluginObject" => $objName];

        //Package Data
        $objPackageData = $this->getObject("tab_packagedata")->get($arrayParam);
        $this->appendObject("tabPlugin", $objPackageData);

        if ($keyIDPlg != "") {


            //Package
            $arrayParamContent = $arrayParam;
            $arrayParamContent["keyIDPlg"] = $objPackageData->packageKeyIDPlg;
            $arrayParamContent["contentType"] = $objPackageData->packageContentType;
            $objPackage = $this->getObject("tab_packagebuild")->get($arrayParamContent);
            $this->appendObject("tabPackage", $objPackage);

            //Content
            $arrayParamContent = $arrayParam;
            $arrayParamContent["keyIDPlg"] = $objPackageData->packageKeyIDPlg;
            $arrayParamContent["contentType"] = $objPackageData->packageContentType;
            $objContent = $this->getObject("tab_packagedatacontent")->get($arrayParamContent);
            $this->appendObject("tabContent", $objContent);




        }
    }


}

?>