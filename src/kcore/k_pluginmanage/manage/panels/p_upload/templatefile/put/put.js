// create namespace
KXJS.namespace("k_pluginmanage.objects.p_upload");

// create method of plugin
(function () {
    var t = this;
    t.urlBase = "";
    t.actualDir = "";

    t.init = function () {
        $(document).ready(t.ready);
    };



    t.uploadFile = function () {

        // Get data Form
        var idForm = $(this).parents("form").attr("id");
        var form = new FormData($('#' + idForm)[0]);

        $("#nextBtn").hide();
        dialogModal = waitingDialog.show('Uploading...', {dialogSize: 'sm', progressType: 'warning'});

        // Ajax call
        $.ajax({
            url: KXJS.buildAPICallUrl("KXCJS", "k_pluginmanage", "manage\\panels\\p_upload", "uploadFile")
            , type: 'POST'
            , xhr: function () {
                var myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            success: function (data) {
                waitingDialog.hide();
                r = $.parseJSON(data);
                if (r.success == "true") {
                    dialogModal.on('hidden.bs.modal', function () {KXNS.k_filemanager.objects.filebrowser.loadDir(t.actualDir);});
                }
                else {
                    if (r.reason == "errorSecurity")
                        window.location.reload();
                    else
                        alert(r.message);

                }
            },
            data: form,
            cache: false,
            contentType: false,
            processData: false
        });


    }


    t.ready = function () {

    }

    t.init();

}).apply(KXNS.k_pluginmanage.objects.p_upload);

