<?
namespace k_pluginmanage\manage\panels\p_upload;
class obj extends \k_webmanager\objects\general\base_panel\obj
{
    var $ver = "1.0";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authManageInstallPlugin";

    var $dictionary;

    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();

    }

    //manage action from ajax
    public function objectRequest($ArrayVariable)
    {   //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");
        //get template
        switch ($KCA) {
            case "uploadFile" :
                return json_encode($this->upload($ArrayVariable));
                break;
        }
    }

    public function get($ArrayVariable = array())
    {


        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");


        switch ($pluginMethod) {
            case "list":
            case "":
                $this->getPagePut($ArrayVariable);

                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);

                break;

        }


        return $this;
    }

    protected function getPagePut($ArrayVariable)
    {

        //add files to include
        $this->addHeadFile([$this->getPathFileJS("put")]);

        //create form
        $this->appendForm("",
            [
                "id" => "uploadForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "upload"
            ]
        );

        //keyID hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "keyID", "value" => ""]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        $this->appendRow("", ["id" => ""]);
        $this->appendColumn($this->lastRow, ["nCol" => ["xs" => 8, "md" => 8]]);
        $label = $this->dictionary["labelFileToUpload"];
        $name = "fileToUpload";
        $this->appendInput($this->lastCol,
            [
                "type" => "fileButton"
                , "value" => '<i class="fa fa-upload"></i>'
                , "label" => $label
                , "id" => $name
                , "name" => $name
                , "size" => "sm"
                , "link" => "#"
                , "semanticColor" => "secondary"
                , "iFileName" => "fileToUploadName"
                , "functionOnChange" => "KXNS.k_pluginmanage.objects.p_upload.uploadFile"
            ]
        );

    }

    public function upload($ArrayVariable)
    {

        //check security
        if (!$this->K_SECAUTH->verifyUserLogged())
            return ["success" => "false", "reason" => "errorSecurity", "message" => "notLogged"];

        //include obj
        $objUPD = $this->K_COMMON->getPluginObject("k_updmanager","update");

        //get variables
        $dir = $this->K_COMMON->getPathTmp();
        $fileToUpload = $_FILES["fileToUploadName"];
        $newFileName = strtolower($fileToUpload['name']);
        $newFileName = str_replace(" ", "_", $newFileName);

        //Upload
        $this->K_COMMON->uploadFile($_FILES["fileToUploadName"],$dir,$newFileName);

        // Initialize archive object
        $zip = new \ZipArchive;
        $res = $zip->open($dir . $newFileName);
        if ($res === TRUE) {
            $dirExtract = $dir . "unzipfile";
            $this->K_COMMON->emptyDir(["dir" => $dirExtract]);
            $zip->extractTo($dirExtract);
            $zip->close(); 

            //get all dir and file and check if  have plugin and data dir
            $filesUp = [];
            $dirListUp = [];
            $dirToRead = $dirExtract;
            $this->K_COMMON->getFilesAndDir($dirToRead, $dirListUp, $filesUp);
            $dirPackage = $this->K_COMMON->getVarArray($dirListUp,0);
            $xmlPackage = $this->K_COMMON->getVarArray($filesUp,0);

            if ($dirPackage == "package" && $xmlPackage == "package.xml")
            {

                //load xml file
                $xml = simplexml_load_file($dirExtract . "/" . $xmlPackage);
                //$install = $xml->install;
                $iPackage = $xml->package;

                $iContent = $xml->content;
                $iPlgID = $xml->pluginName;

                if ($iPackage != "" && $iPlgID != "")

                {
                    $pathPlugin = $this->K_COMMON->getPathPluginObject(true, $iPlgID, "");

                    //remove old plugin (before empty Dir)
                    $this->K_COMMON->emptyDir(["dir" => $pathPlugin]);
                    $this->K_COMMON->rmDir(["dir" => $pathPlugin]);


                    //create dir
                    $this->K_COMMON->mkDir(["dir" => $pathPlugin, "mask" => 0777]);

                    //copy in new dir
                    $this->K_COMMON->copyDir($dirExtract . "/" . $dirPackage, $pathPlugin);


                }


            }
            else {
                return ["success" => "false", "reason" => "failOpenFile"];
            }

            return ["success" => "true"];
        } else {
            return ["success" => "false", "reason" => "failOpenFile"];
        }
    }




}

?>