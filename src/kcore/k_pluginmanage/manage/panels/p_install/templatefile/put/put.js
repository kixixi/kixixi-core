// create namespace
KXJS.namespace("k_pluginmanage.objects.p_install");

// create method of plugin
(function () {
    var t = this;

    t.init = function () {
        $(document).ready(t.ready);
    };



    t.install = function () {
        var plugin = $(this).attr("plugin");
        waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'warning'});
        $.ajax({
            url: KXJS.buildAPICallUrl("KXCJS","k_pluginmanage","manage\\panels\\p_install","install"),
            data: {
                plugin:plugin
            }
        }).done(function(data) {
            r = $.parseJSON(data);
            waitingDialog.hide();
            if(r.success == "true"){
                location.reload();
            }
            else {
                alert("Error Security");
                location.reload();
            }
        });

    }


    t.ready = function () {
        $(".btnInstall").click(t.install);
    }

    t.init();

}).apply(KXNS.k_pluginmanage.objects.p_install);

