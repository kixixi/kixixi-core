<?
namespace k_pluginmanage\manage\panels\p_install;
class obj extends \k_webmanager\objects\general\base_panel\obj
{
    var $ver = "1.0";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authManageInstallPlugin";

    var $dictionary;

    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
    }

    //manage action from ajax
    public function objectRequest($ArrayVariable)
    {   //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");
        //get template
        switch ($KCA) {
            case "install" :
                return json_encode($this->install($ArrayVariable));
                break;
        }
    }

    public function get($ArrayVariable = array())
    {
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "list":
            case "":
                $this->getPagePut($ArrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;
        }
        return $this;
    }

    protected function getPagePut($ArrayVariable)
    {

        //add files to include
        $this->addHeadFile([$this->getPathFileJS("put")]);

        //include module
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");
        $objUsefulDataApi = $this->K_COMMON->getPluginObject("k_usefuldata", "api");
        $pluginsList = $objUsefulDataApi->listPluginsAndObjects(
            [
                "byFiles" => "1"
                ,"pluginType" => ["KPlugin" => "1", "KData" => "1", "KTemplate" => "1"]
            ]
        );


        //set variable
        $onlyRow = "";

        //include table
        $table = $this->K_COMMON->getPluginDataObject("k_common", "plugins");

        //create tablelist head
        $OBJTableList->addColumnHead($this->dictionary["LabelPlugin"], "", "", $onlyRow);
        $OBJTableList->addColumnHead($this->dictionary["LabeInstalled"], "", "", $onlyRow);
        $OBJTableList->addColumnHead("", "", "", $onlyRow);

        foreach ($pluginsList as $plugin) {

            $type = $this->K_COMMON->getVarArray($plugin, "type");
            $value = $this->K_COMMON->getVarArray($plugin, "value");
            $text = $this->K_COMMON->getVarArray($plugin, "text");
            if ($type != "kcore") {
                //label
                $OBJTableList->addRow();
                $OBJTableList->addColumn("");


                //get data
                $table->get(["plg_name"], ["plg_id" => $value, "plg_type" => $type]);
                $data = $table->fetch();
                if ($data) {
                    $OBJTableList->addValue($data->plg_name, "", "", "col-md-10", "");
                    $OBJTableList->addColumnState(true);
                    $OBJTableList->addColumn("");
                    $OBJTableList->addButton("", $this->dictionary["LabelInstall"], "#", "btnInstall btn-warning btn-sm width40 btn_" . $value, " plugin=" . $value, "fa fa-refresh");
                } else {
                    $OBJTableList->addValue($text, "", "", "col-md-10", "");
                    $OBJTableList->addColumnState(false);
                    $OBJTableList->addColumn("");
                    $OBJTableList->addButton("", $this->dictionary["LabelInstall"], "#", "btnInstall btn-success btn-sm width40 btn_" . $value, " plugin=" . $value, "fa fa-plus");
                }
            }
        }
        $objTable = $OBJTableList->get();
        $this->appendObject("", $objTable);

    }

    public function install($ArrayVariable)
    {


        //check security
        if (!$this->K_SECAUTH->verifyUserLogged())
            return ["success" => "false", "reason" => "errorSecurity", "message" => "notLogged"];

        //set variable
        $plugin = $this->K_COMMON->getVarArray($ArrayVariable,"plugin");

        //check variable
        if($plugin == "")
            return ["success"=>"false","reason"=>"pluginIsNull"];

        //include obj
        $objUPD = $this->K_COMMON->getPluginObject("k_updmanager", "update");

        //lunch update
        return $objUPD->updatePlugin(["pluginName" => $plugin, "webCode" => $this->K_COMMON->getEditingWeb()]);
        
    }



   
}

?>