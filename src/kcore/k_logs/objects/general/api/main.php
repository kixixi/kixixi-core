<?
/**
 * KIXIXI Logs API
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package KIXIXI
 * @subpackage k_logs\api
 * @since 1.00
 */
namespace k_logs\objects\general\api;
class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";



    /**
     * put log
     *
     * @since 1.00
     *
     * @param array $arrayVariables array of variables
     * The following directives can be used in the array variables:<br>
     *  *   pluginName:  plugin ID
     *  *   pluginObject:  plugin object
     *  *   data:  string data to save
     *
     * @return array
     */
    function put($arrayVariables)
    {

        //set variables
        $pluginName = $this->K_COMMON->getVarArray($arrayVariables, "pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($arrayVariables, "pluginObject");
        $data = $this->K_COMMON->getVarArray($arrayVariables, "data");


        //get panel menu data object
        $table = $this->getTable("logs");

        $data = [
            "log_plugin" => $pluginName
            , "log_plugin_object" => $pluginObject
            , "log_data" => $data


        ];
        $table->put($data);

        $newID = $table->getNewID();

        return ["success" => "true", "newID" => $newID];


    }

}

?>