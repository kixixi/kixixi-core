<?
namespace k_mailing\manage\panels\p_messages;
class obj extends \k_webmanager\objects\general\base_panel\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authManageMailing";

    var $msgLanguages = "";

    public function __construct()
    {
        parent::__construct();
        $this->msgLanguages = ["EN" => "EN", "IT" => "IT", "FR" => "FR", "ES" => "ES", "DE" => "DE"];

    }


    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "saveData":
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "getModalAdd":
                return json_encode($this->getModalAdd($ArrayVariable));
                break;
            case "restoreDefault":
                return json_encode($this->restoreDefault($ArrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableList($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
        }
    }

    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "put", "get")
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {

        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
                $this->getPageList($ArrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    protected function getPageList($ArrayVariable)
    {

        //labelInput
        $label = "Plugin";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_comboplugin"
            , [
                "labelInput" => $label
                , "withKCore" => "1"
                , "byFiles" => "1"
                , "triggerEvent" => "change"
                , "triggerFunction" => "kx_tablelist_script_tableListMessages.loadTableList"
            ]);

        //create modal for language
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                , "typeLoad" => "array"
                , "options" => $this->msgLanguages
                , "id" => "lngMessages"
                , "labelInput" => $this->getDictionaryWord("LabelSelectLanguage")
                , "noEmptyOption" => "1"
                , "triggerEvent" => "change"
                , "triggerFunction" => "kx_tablelist_script_tableListMessages.loadTableList"
            ]
        );
        $listMyMList = $this->getTableList($ArrayVariable);
        $this->appendHTML("", $listMyMList);
    }

    protected function getTableList($ArrayVariable)
    {


        //include css
        $this->addHeadFile([$this->getPathFileCSS("list")]);

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");
        $webCode = $this->K_COMMON->getEditingWeb();
        $lngMessages = $this->K_COMMON->getVarArray($ArrayVariable, "lngMessages");

        //get object tablelist
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        //Load Data
        if ($onlyRow == "1") {
            //Load Table
            $table = $this->K_COMMON->getPluginDataObject("k_mailing", "messages");
            $queryWhere = " (MSGCustom . msg_web_code = " . $webCode . " OR MSGCustom . msg_web_code is null) ";
            if ($plgID != "")
                $queryWhere .= " AND MSGDefault.msg_plugin_name = '" . $plgID . "'";
            if ($lngMessages != "")
                $queryWhere .= " AND MSGDefault.msg_language_code = '" . $lngMessages . "'";
            $table->getByQuery("msg_default_and_custom", "", $queryWhere, ["webCode" => $webCode, "lngMessages" => $lngMessages]);
            $dataTable = $table->getResults("array");
        } else
            $dataTable = "";

        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableListMessages"
                , "hideHead" => $onlyRow
                , "fieldsPageToGet" => ["webCode" => "webCode", "plgID" => "pluginName", "lngMessages" => "lngMessages"]
                , "ajax" =>
                [
                    "kcp" => $this->getPluginName()
                    , "kco" => $this->getPluginObjectName()
                    , "kca" => "getTableList"

                ]
                , "data" => $dataTable
                , "fields" =>
                [
                    [
                        "label" => "Plugin"
                        , "qryField" => "pluginName"
                        , "classIF" => ["isCustomized" => ["isCustomized" => "1"]]
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListCode")
                        , "qryField" => "code"

                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListSubject")
                        , "qryField" => "subjectGeneral"
                    ]
                    ,
                    [
                        "width" => "130px"
                        , "buttons" =>
                        [
                            [
                                "title" => $this->getDictionaryWord("LabelListAdd")
                                , "action" => "editModal"
                                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                , "hideIF" => ["isCustomized" => "1"]
                                , "icon" => "fa-cogs"
                                , "idModal" => "modalAddMessage"
                                , "dynamicAttributes" => ["webCode" => $webCode,"plgID"=>$plgID, "langCode" => $lngMessages]
                                , "qryAttributes" => [
                                "plgID" => "pluginName"
                                , "pluginObject" => "pluginObj"
                                , "msgCode" => "code"
                                , "keyIDCustom" => "keyIDCustom"
                                , "keyIDDefault" => "keyIDDefault"
                            ]
                            ]
                            ,
                            [
                                "title" => $this->getDictionaryWord("LabelListEdit")
                                , "action" => "editModal"
                                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                , "showIF" => ["isCustomized" => "1"]
                                , "idModal" => "modalAddMessage"
                                , "dynamicAttributes" => ["webCode" => $webCode,"plgID"=>$plgID, "langCode" => $lngMessages]
                                , "qryAttributes" => [
                                "plgID" => "pluginName"
                                , "pluginObject" => "pluginObj"
                                , "msgCode" => "code"
                                , "keyIDCustom" => "keyIDCustom"
                                , "keyIDDefault" => "keyIDDefault"
                                , "isCustomized" => "isCustomized"
                            ]
                            ]
                            ,
                            [
                                "title" => $this->getDictionaryWord("LabelListDelete")
                                , "showIF" => ["isCustomized" => "1"]
                                , "dynamicAttributes" => ["webCode" => $webCode,"plgID"=>$plgID, "langCode" => $lngMessages]
                                , "reloadOnSuccess" => "1"
                                , "qryAttributes" => [
                                "plgID" => "pluginName"
                                , "pluginObject" => "pluginObj"
                                , "msgCode" => "code"
                                , "keyIDCustom" => "keyIDCustom"
                                , "keyIDDefault" => "keyIDDefault"
                            ]
                                , "action" => "remove"
                                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "restoreDefault"]
                            ]
                        ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();

    }

    protected function getModalAdd($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->getDictionaryWord("LabelModalTitleAddMsg"), "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePut($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getPagePut($ArrayVariable)
    {

        //set variable
        $keyIDDefault = $this->K_COMMON->getVarArray($ArrayVariable, "keyIDDefault");
        $keyIDCustom = $this->K_COMMON->getVarArray($ArrayVariable, "keyIDCustom");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $langCode = $this->K_COMMON->getVarArray($ArrayVariable, "langCode");

        $value_msgCode = "";
        $value_msgSubject = "";
        $value_msgMessage = "";

        if ($keyIDCustom != "")
            $keyID = $keyIDCustom;
        else
            $keyID = $keyIDDefault;

        //Load Table
        $table = $this->K_COMMON->getPluginDataObject("k_mailing", "messages");
        $table->get(["msg_code", "msg_subject", "msg_message"],
            [
                "msg_plugin_name" => $plgID,
                "msg_plugin_object" => $pluginObject,
                "id_message" => $keyID
            ]
        );

        $data = $table->fetch();
        if ($data) {
            $value_msgCode = $data->msg_code;
            $value_msgSubject = $data->msg_subject;
            $value_msgMessage = $data->msg_message;
        }

        //create form
        $this->appendForm("",
            [
                "id" => "messageForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "reloadTableList" => "tableListMessages"
                , "closeModal" => "modalAddMessage"

            ]
        );

        //keyID Default hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyIDDefault", "name" => "keyIDDefault", "value" => $keyIDDefault]);

        //keyID Custom hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyIDCustom", "name" => "keyIDCustom", "value" => $keyIDCustom]);

        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);

        //object
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginObject", "name" => "pluginObject", "value" => $pluginObject]);

        //msgCode
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "msgCode", "name" => "msgCode", "value" => $value_msgCode]);

        //webCode
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "webCode", "name" => "webCode", "value" => $webCode]);

        //langCode
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "langCode", "name" => "langCode", "value" => $langCode]);


        //NEW ROW
        $this->appendRow($this->lastForm);

        //code
        $label = $this->getDictionaryWord("LabelFormCode");
        $name = "msgCode";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_msgCode, "disabled" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //message
        $label = $this->getDictionaryWord("LabelFormSubject");
        $name = "msgSubject";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_msgSubject, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //message
        $label = $this->getDictionaryWord("LabelFormMessage");
        $name = "msgMessage";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "textEditor", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_msgMessage, "webCode" => $webCode]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->getDictionaryWord("LabelFormSave");
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;

    }

    protected function restoreDefault($ArrayVariable)
    {

        //set variable
        $keyIDCustom = $this->K_COMMON->getVarArray($ArrayVariable, "keyIDCustom");

        //include table
        $table = $this->K_COMMON->getPluginDataObject("k_mailing", "messages");

        $table->delete(["id_message" => $keyIDCustom]);

        return array("success" => "true");
    }

    protected function saveData($ArrayVariable)
    {

        //set variable
        $keyIDCustom = $this->K_COMMON->getVarArray($ArrayVariable, "keyIDCustom");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $msgCode = $this->K_COMMON->getVarArray($ArrayVariable, "msgCode");
        $msgSubject = $this->K_COMMON->getVarArray($ArrayVariable, "msgSubject");
        $msgMessage = $this->K_COMMON->getVarArray($ArrayVariable, "msgMessage");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $langCode = $this->K_COMMON->getVarArray($ArrayVariable, "langCode");
        $arrayWhere = "";

        //include tables
        $table = $this->K_COMMON->getPluginDataObject("k_mailing", "messages");

        if ($plgID == "" || $msgCode == "" || $msgSubject == "" || $msgMessage == "")
            return ["success" => "false", "reason" => "inputVoid", "message" => "Please fill in the fields!"];

        if ($keyIDCustom != "") {
            $arrayWhere = ["id_message" => $keyIDCustom];
        }
        $table->put(
            [
                "msg_plugin_name" => $plgID
                , "msg_plugin_object" => $pluginObject
                , "msg_code" => $msgCode
                , "msg_language_code" => $langCode
                , "msg_subject" => $msgSubject
                , "msg_message" => $msgMessage
                , "msg_web_code" => $webCode
            ],$arrayWhere
        );

        $tableRes = $table->getLastError();
        if ($tableRes["code"] == "1062") {
            return ["success" => "false", "error" => "customCodeDup", "message" => $this->getDictionaryWord("emsgCustomCodeDup")];
        }

        return ["success" => "true"];
    }


}

?>