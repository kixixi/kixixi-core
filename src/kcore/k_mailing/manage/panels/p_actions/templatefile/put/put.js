// create namespace
KXJS.namespace("k_mailing.objects.p_actions");

// create method of plugin
(function (NS) {
    var t = this;
    t.pluginName = "k_mailing";
    t.pluginObject = "p_actions";

    t.emailFromChange = function (firstTime) {
        if (firstTime != 1)
            $("#actionMailFrom").val($("#actionMailFromOption").val());
        if ($("#actionMailFromOption").find(":selected").attr("fixed") != "1")
            $("#actionMailFrom").prop("disabled", true);
        else
            $("#actionMailFrom").prop("disabled", false);
    }

    t.emailToChange = function (firstTime) {
        if (firstTime != 1)
            $("#actionMailTo").val($("#actionMailToOption").val());
        if ($("#actionMailToOption").find(":selected").attr("fixed") != "1")
            $("#actionMailTo").prop("disabled", true);
        else
            $("#actionMailTo").prop("disabled", false);
    }


    t.ready = function () {
        t.emailFromChange("1");
        t.emailToChange("1");

        $("#actionForm #actionMailFromOption").change(t.emailFromChange);
        $("#actionForm #actionMailToOption").change(t.emailToChange);

    }

    this.init = function () {
        $(document).ready(t.ready);
    };

    this.init();

}).apply(KXNS.k_mailing.objects.p_actions);

