<?
namespace k_mailing\manage\panels\p_actions;
class obj extends \k_webmanager\objects\general\base_panel\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";
    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authManageMailing";


    var $msgLanguages = "";

    public function __construct()
    {
        parent::__construct();
        $this->msgLanguages = ["EN" => "EN", "IT" => "IT", "FR" => "FR", "ES" => "ES", "DE" => "DE"];

    }


    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {


        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "saveData":
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "getModalAdd":
                return json_encode($this->getModalAdd($ArrayVariable));
                break;
            case "restoreDefault":
                return json_encode($this->restoreDefault($ArrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableList($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
        }
    }

    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "put", "get")
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {
        
        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
                $this->getPageList($ArrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    protected function getPageList($ArrayVariable)
    {


        //labelInput
        $label = "Plugin";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_comboplugin"
            , [
                "labelInput" => $label
                , "withKCore" => "1"
                , "byFiles" => "1"
                , "triggerEvent" => "change"
                , "triggerFunction" => "kx_tablelist_script_tableListActions.loadTableList"
            ]);

        $listMyMList = $this->getTableList($ArrayVariable);
        $this->appendHTML("", $listMyMList);
    }

    protected function getTableList($ArrayVariable)
    {


        $this->addHeadFile([$this->getPathFileCSS("list")]);
        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");
        $webCode = $this->K_COMMON->getEditingWeb();

        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        //Load Data
        if ($onlyRow == "1") {

            //Load Table
            $table = $this->K_COMMON->getPluginDataObject("k_mailing", "actions");
            $queryWhere = " (ACTCustom . act_web_code = " . $webCode . " OR ACTCustom . act_web_code is null) ";
            if ($plgID != "")
                $queryWhere .= " AND ACTDefault.act_plugin_name = '" . $plgID . "'";
            $table->getByQuery("act_default_and_custom", "", $queryWhere, ["webCode" => $webCode]);
            $dataTable = $table->getResults("array");
        } else
            $dataTable = "";

        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableListActions"
                , "hideHead" => $onlyRow
                , "fieldsPageToGet" => ["webCode" => "webCode", "plgID" => "pluginName"]
                , "ajax" =>
                [
                    "kcp" => $this->getPluginName()
                    , "kco" => $this->getPluginObjectName()
                    , "kca" => "getTableList"

                ]
                , "data" => $dataTable
                , "fields" =>
                [
                    [
                        "label" => "Plugin"
                        , "qryField" => "pluginName"
                        , "classIF" => ["isCustomized" => ["isCustomized" => "1"]]
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListCode")
                        , "qryField" => "code"

                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListEmailFromGeneral")
                        , "qryField" => "emailFromGeneral"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListEmailToGeneral")
                        , "qryField" => "emailToGeneral"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListMsgCodeGeneral")
                        , "qryField" => "messageCodeGeneral"
                    ]
                    ,
                    [
                        "width" => "130px"
                        , "buttons" =>
                        [
                            [
                                "title" => $this->getDictionaryWord("LabelListAdd")
                                , "action" => "editModal"
                                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                , "hideIF" => ["isCustomized" => "1"]
                                , "icon" => "fa-cogs"
                                , "idModal" => "modalAddAction"
                                , "dynamicAttributes" => ["webCode" => $webCode]
                                , "qryAttributes" => [
                                "plgID" => "pluginName"
                                , "pluginObject" => "pluginObj"
                                , "msgCode" => "code"
                                , "keyIDCustom" => "keyIDCustom"
                                , "keyIDDefault" => "keyIDDefault"
                            ]
                            ]
                            ,
                            [
                                "title" => $this->getDictionaryWord("LabelListEdit")
                                , "action" => "editModal"
                                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                , "showIF" => ["isCustomized" => "1"]
                                , "idModal" => "modalAddAction"
                                , "dynamicAttributes" => ["webCode" => $webCode]
                                , "qryAttributes" => [
                                "plgID" => "pluginName"
                                , "pluginObject" => "pluginObj"
                                , "msgCode" => "code"
                                , "keyIDCustom" => "keyIDCustom"
                                , "keyIDDefault" => "keyIDDefault"
                                , "isCustomized" => "isCustomized"
                            ]
                            ]
                            ,
                            [
                                "title" => $this->getDictionaryWord("LabelListDelete")
                                , "showIF" => ["isCustomized" => "1"]
                                , "dynamicAttributes" => ["webCode" => $webCode]
                                , "reloadOnSuccess" => "1"
                                , "qryAttributes" => [
                                "plgID" => "pluginName"
                                , "pluginObject" => "pluginObj"
                                , "msgCode" => "code"
                                , "keyIDCustom" => "keyIDCustom"
                                , "keyIDDefault" => "keyIDDefault"
                            ]
                                , "action" => "remove"
                                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "restoreDefault"]
                            ]
                        ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();

    }

    protected function getModalAdd($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->getDictionaryWord("LabelModalTitleAddAct"), "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePut($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getPagePut($ArrayVariable)
    {

        //include script
        $this->addHeadFile([$this->getPathFileJS("put")]);

        //set variable
        $keyIDDefault = $this->K_COMMON->getVarArray($ArrayVariable, "keyIDDefault");
        $keyIDCustom = $this->K_COMMON->getVarArray($ArrayVariable, "keyIDCustom");
        $isCustomized = $this->K_COMMON->getVarArray($ArrayVariable, "isCustomized");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $disabled = "1";

        if ($isCustomized == "1") {
            $keyID = $keyIDCustom;
        } else {
            $keyID = $keyIDDefault;
        }
        $value_actionCode = "";
        $value_actionMailFrom = "";
        $value_actionMailTo = "";
        $value_actionMailFromOptions = "";
        $value_actionMailToOptions = "";
        $value_messageCode = "";
        $option_actionMailFrom = "";
        $option_actionMailTo = "";

        //Load Table
        $table = $this->K_COMMON->getPluginDataObject("k_mailing", "actions");
        $table->get(["act_code", "act_email_from", "act_email_to", "act_message_code"],
            ["act_plugin_name" => $plgID, "act_plugin_object" => $pluginObject, "id_action" => $keyID]);
        $data = $table->fetch();
        if ($data) {
            $value_actionCode = $data->act_code;
            $value_actionMailFrom = $data->act_email_from;
            $value_actionMailTo = $data->act_email_to;
            $value_messageCode = $data->act_message_code;
            $value_actionMailFromOptions = $value_actionMailFrom;
            $value_actionMailToOptions = $value_actionMailTo;

            if (strpos($value_actionMailFrom, "@") !== false)
                $option_actionMailFrom = $value_actionMailFrom;
            if (strpos($value_actionMailTo, "@") !== false)
                $option_actionMailTo = $value_actionMailTo;
        }
        //create form
        $this->appendForm("",
            [
                "id" => "actionForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "saveData"
                , "reloadTableList" => "tableListActions"
                , "closeModal" => "modalAddAction"

            ]
        );

        //keyID Custom hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyIDCustom", "name" => "keyIDCustom", "value" => $keyIDCustom]);

        //keyID Default hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyIDDefault", "name" => "keyIDDefault", "value" => $keyIDDefault]);

        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);

        //pluginObject
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginObject", "name" => "pluginObject", "value" => $pluginObject]);

        //actionCode
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "actionCode", "name" => "actionCode", "value" => $value_actionCode]);

        //webCode
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "webCode", "name" => "webCode", "value" => $webCode]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //code
        $label = $this->getDictionaryWord("LabelFormCode");
        $name = "actionCode";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_actionCode, "required" => "1", "disabled" => $disabled]);


        //NEW ROW
        $this->appendRow($this->lastForm);

        //Email From Option
        $emailFromOption = [
            [
                "value" => ""
                , "text" => $this->getDictionaryWord("LabelFixedEmailFrom")
                , "fixed" => "1"
            ]
            ,
            [
                "value" => "WEBEMAIL"
                , "text" => $this->getDictionaryWord("LabelDefaultWebEmailFrom")
                , "fixed" => "0"
            ]
            ,
            [
                "value" => "USEREMAILFROM"
                , "text" => $this->getDictionaryWord("LabelUserEmailFrom")
                , "fixed" => "0"
            ]
        ];
        $label = $this->getDictionaryWord("LabelFormActionEmailFrom");
        $name = "actionMailFromOption";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $emailFromOption, "noEmptyOption" => "1", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_actionMailFromOptions]);

        //email From Custom
        $label = "";
        $name = "actionMailFrom";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_actionMailFrom, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //email To Option
        $emailToOption = [
            [
                "value" => ""
                , "text" => $this->getDictionaryWord("LabelFixedEmailTo")
                , "fixed" => "1"
            ]
            ,
            [
                "value" => "WEBEMAILTO"
                , "text" => $this->getDictionaryWord("LabelDefaultWebEmailTo")
                , "fixed" => "0"
            ]
            ,
            [
                "value" => "USEREMAIL"
                , "text" => $this->getDictionaryWord("LabelUserEmailTo")
                , "fixed" => "0"
            ]

        ];
        $label = $this->getDictionaryWord("LabelFormActionEmailTo");
        $name = "actionMailToOption";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $emailToOption, "noEmptyOption" => "1", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_actionMailToOptions]);

        //email To Custom
        $label = "";
        $name = "actionMailTo";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_actionMailTo, "required" => "1"]);


        //NEW ROW
        $this->appendRow($this->lastForm);

        //message code
        $label = $this->getDictionaryWord("LabelFormMessage");
        $listMessage = $this->getObject("messages")->getArrayListMessageNoLanguage(["webCode" => $webCode, "plgID" => $plgID, "pluginObject" => $pluginObject]);
        $name = "messageCode";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $listMessage, "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_messageCode, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->getDictionaryWord("LabelFormSave");
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;

    }

    protected function restoreDefault($ArrayVariable)
    {


        //set variable
        $keyIDCustom = $this->K_COMMON->getVarArray($ArrayVariable, "keyIDCustom");

        //include table
        $table = $this->K_COMMON->getPluginDataObject("k_mailing", "actions");

        $table->delete(["id_action" => $keyIDCustom]);

        return array("success" => "true");
    }

    protected function saveData($ArrayVariable)
    {


        //set variable
        $keyIDCustom = $this->K_COMMON->getVarArray($ArrayVariable, "keyIDCustom");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $actCode = $this->K_COMMON->getVarArray($ArrayVariable, "actionCode");
        $actMailFrom = $this->K_COMMON->getVarArray($ArrayVariable, "actionMailFrom");
        $actMailFromOption = $this->K_COMMON->getVarArray($ArrayVariable, "actionMailFromOption");
        $actMailTo = $this->K_COMMON->getVarArray($ArrayVariable, "actionMailTo");
        $actMailToOption = $this->K_COMMON->getVarArray($ArrayVariable, "actionMailToOption");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $messageCode = $this->K_COMMON->getVarArray($ArrayVariable, "messageCode");
        $arrayWhere = "";

        if ($actMailFrom == "")
            $actMailFrom = $actMailFromOption;
        if ($actMailTo == "")
            $actMailTo = $actMailToOption;

        if ($plgID == "" || $actCode == "" || $messageCode == "")
            return ["success" => "false", "reason" => "inputVoid", "message" => "Please fill in the fields!"];

        //include tables
        $table = $this->K_COMMON->getPluginDataObject("k_mailing", "actions");

        if ($keyIDCustom != "") {
            $arrayWhere = ["id_action" => $keyIDCustom];
        }

        $table->put(
            [
                "act_plugin_name" => $plgID
                , "act_plugin_object" => $pluginObject
                , "act_code" => $actCode
                , "act_web_code" => $webCode
                , "act_email_from" => $actMailFrom
                , "act_email_to" => $actMailTo
                , "act_message_code" => $messageCode
            ], $arrayWhere
        );
        $tableRes = $table->getLastError();
        if ($tableRes["code"] == "1062") {
            return ["success" => "false", "error" => "customCodeDup", "message" => $this->getDictionaryWord("emsgCustomCodeDup")];
        }

        return ["success" => "true"];
    }


}

?>