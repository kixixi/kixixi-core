<?
namespace k_mailing\objects\general\messages;
class obj extends \k_common\objects\general\base\obj
{
    var $ver = "1.0";
    var $dictionary;
    var $msgLanguages;

    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
        $this->msgLanguages = ["EN" => "EN", "IT" => "IT", "FR" => "FR", "ES" => "ES", "DE" => "DE"];

    }

    public function getArrayListMessage($ArrayVariable)
    {
        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $lng = $this->language;

        //get Table
        $table = $this->K_COMMON->getPluginDataObject("k_mailing", "messages");

        //get all websites for combo
        $table->get(["text" => "msg_subject", "value" => "msg_code"],
            ["msg_web_code" => $webCode, "msg_plugin_name" => $plgID, "msg_plugin_object" => $pluginObject, "msg_language_code" => $lng]);
        return $table->getResults("array");

    }

    public function getArrayListMessageNoLanguage($ArrayVariable)
    {
        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");

        //get Table
        $table = $this->K_COMMON->getPluginDataObject("k_mailing", "messages");

        //get all websites for combo
        $table->get(["text" => "msg_subject", "value" => "msg_code"],
            ["msg_web_code" => $webCode, "msg_plugin_name" => $plgID, "msg_plugin_object" => $pluginObject],[],["msg_web_code", "msg_plugin_name", "msg_plugin_object"]);
        return $table->getResults("array");

    }

}

?>