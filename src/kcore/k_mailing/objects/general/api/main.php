<?
/**
 * KIXIXI Mailing (api)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_mailing
 * @subpackage objects\api
 * @since 1.00
 */
namespace k_mailing\objects\general\api;

class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";


    /**
     * Send Email
     *
     * @param string $FromName Name of Sender
     * @param string $FromEmail Email of Sender
     * @param string $To email To
     * @param string $Subject email subject
     * @param string $Message email message
     * @param string $webCode optional code of web
     *
     * @return int (1 or 0)
     */
    function sendEmail($FromName, $FromEmail, $To, $Subject, $Message, $webCode = '')
    {

        //set variables
        $SMTPHost = $this->getParamValue("SMTPHost",$webCode,"k_mailing","");
        $SMTPUsername = $this->getParamValue("SMTPUsername",$webCode,"k_mailing","");
        $SMTPPassword = $this->getParamValue("SMTPPassword",$webCode,"k_mailing","");
        $domainFrom = $this->getParamValue("domainFrom",$webCode,"k_mailing","");

        //include module
        $pathSDKMailer = $this->K_COMMON->getPathSDK(["name" => "PHPMailer"]);
        include_once($pathSDKMailer . '/src/PHPMailerCustom.php');
        include_once($pathSDKMailer . '/src/SMTP.php');

        $mail             = new \PHPMailer\PHPMailer\PHPMailer();
        $mail->CharSet = "UTF-8";
        $mail->IsSMTP(true); // telling the class to use SMTP
        $mail->SMTPAuth      = true; // enable SMTP authentication
        $mail->Port          = 587;  // set the SMTP port for the GMAIL server
        $mail->Host          = $SMTPHost; //SMTP Host
        $mail->Username      = $SMTPUsername; // SMTP account username
        $mail->Password      = $SMTPPassword; // SMTP account password
        $mail->SMTPDebug  = 0;
        $mail->SMTPSecure = "tls";

        $mail->AddReplyTo($FromEmail,$FromName);
        if ($domainFrom != "")
            $FromEmail = str_replace("@", ".",$FromEmail) . "@" . $domainFrom;


        $mail->From       = $FromEmail;
        $mail->FromName   = $FromName;

        $mail->AddAddress($To);

        $mail->Subject    = $Subject;
        $mail->Body    = $Message;
        //$mail->AltBody    = $Message;

        $mail->MsgHTML($Message);

        $res = $mail->Send();

        return $res;


    }

    /**
     * Execute an Eamail Action
     *
     * @param string $pluginName plugin name of action
     * @param string $pluginObject plugin object of action
     * @param string $webCode web code
     * @param string $actionCode action code
     * @param array $globalVariables optional (example "language_code" of email)
     * @param array $messageVariables email message (variables to replace on message)
     *
     * @return string
     */
    function doAction($pluginName, $pluginObject, $webCode, $actionCode, $globalVariables = [], $messageVariables = [])
    {


        //set variables
        if ($webCode == "")
            $webCode = $this->K_COMMON->getCurrentWeb();
        $userCode = $this->K_COMMON->getCurrentUser();
        $messageVariablesToSend = "";

        //check variables
        if ($this->K_COMMON->getVarArray($globalVariables,"language_code") == "")
            $globalVariables["language_code"] = $this->language;

        //convert global variables to json
        $jsonGlobalVariables = json_encode($globalVariables);


        //convert message variables
        foreach ($messageVariables as $key => $value)
        {
            $messageVariablesToSend .= "KEY=>".$key."VAL=>".$value;
        }

        //include table
        $table = $this->getTable("actions");

        //replace "\" on pluginobject with double "\" for query
        $pluginObject = str_replace("\\", "\\\\", $pluginObject);

        //execute action
        $table->call("actions_do_action", "'" . $pluginName . "','" . $pluginObject . "','" . $actionCode . "','" . $webCode . "','" . $userCode . "','" . $jsonGlobalVariables . "','" . $messageVariablesToSend . "'");

        //Send Email
        $this->processEmail();


    }

    /**
     * Send emails in queue
     */
    function processEmail()
    {

        //include table
        $table = $this->getTable("send_request");

        //load data
        $table->get("*",["msr_sended" => "0", "msr_senddate <= NOW()"]);

        //process data
        $arrayID = [];
        while ($data = $table->fetch())
        {

            $fromEmail = $data->msr_from_email;

            if ($this->K_COMMON->validateEmail($data->msr_to_email))
                $this->sendEmail($data->msr_from_name,$fromEmail,$data->msr_to_email,$data->msr_subject,$data->msr_text, $data->msr_web_code);
            $arrayID[] =$data->id_sendrequest;

        }


        //Update ID Sended
        $table->put(["msr_sended" => "1"],["id_sendrequest" => ["in (%WHERE%)" => $arrayID]]);

    }



}


?>