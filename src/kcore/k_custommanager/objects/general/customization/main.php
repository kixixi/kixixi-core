<?
/**
 * KIXIXI Custom Manager
 * Manage Customization
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package k_custommanager\customization
 * @subpackage objects\customization
 * @since 1.00
 */
namespace k_custommanager\objects\general\customization;
class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Customization table
     *
     * @since 1.00
     * @access public
     * @var table
     */
    protected $tableCustomization;

    /**
     * Array of Customizations
     *
     * @since 1.00
     * @access public
     * @var table
     */
    protected $arrayOfCustomizations = [];


    /**
     * Construct
     *
     */
    function __construct()
    {

        //call parent
        parent::__construct();

        //get tables
        $this->tableCustomization = $this->getTable("customization");

        $this->loadCustomizations();

    }

    /**
     * load all customizations
     *
     *
     */
    public function loadCustomizations()
    {

            //load data
            $this->tableCustomization->get("*",
                [
                   "cst_active" => "1"
                ]
            );

            //get array of results
            $res = $this->tableCustomization->getResults("array");

            //insert data in local array of customization
            if (!empty($res)) {
                foreach ($res as $key => $record) {
                    $targetPlugin = $record["cst_target_plugin_name"];
                    $targetObject = $record["cst_target_plugin_object"];
                    $this->arrayOfCustomizations[$targetPlugin . "_" . $targetObject][$record["cst_target_when"]][] =
                        [
                            "pluginName" => $record["cst_source_plugin_name"]
                            , "pluginObject" => $record["cst_source_plugin_object"]
                            , "parameters" => $record["cst_parameters"]
                        ];
                }
            }



    }

    /**
     * Customize Object
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    targetPlugin: plugin to customize
     *    *    targetObject: plugin object to customize
     *    *    targetCode: code of customization
     *    *    parameters: parameters for customiztion
     *
     * @param webobject|array|string $objToCustomize object to be customized (may be array, string or object)
     * @param webobject $objToCustomizeResult only for after call customization. It's the result of function that you want customize
     *
     * @return object customized
     */
    public function customizeObject($arrayVariable = array(), $objToCustomize = null, $objToCustomizeResult = null)
    {

        //set variables
        $parameters = $this->K_COMMON->getVarArray($arrayVariable, "parameters");
        $targetMethod = $this->K_COMMON->getVarArray($arrayVariable, "targetCode");
        $targetPlugin = $this->K_COMMON->getVarArray($arrayVariable, "targetPlugin");
        $targetObject = $this->K_COMMON->getVarArray($arrayVariable, "targetObject");
        $targetWhen = $this->K_COMMON->getVarArray($arrayVariable, "targetWhen");
        $objRet = $objToCustomize;

        //check
        if ($targetMethod == "getWOCustomBuilder" || $targetMethod == "customBuilder")
            return $objRet;


        if ($targetMethod == "getWO")
            $targetMethod = "get";


        //get customization
        $dataCst = $this->K_COMMON->getVarArray($this->arrayOfCustomizations, $targetPlugin . "_" . $targetObject);
        if (!empty($dataCst))
            $dataCst = $this->K_COMMON->getVarArray($dataCst, $targetWhen);
        else
            return $objRet;


        if (!empty($dataCst))
        {

            //echo "<br> **** CHECK CUSTOM FOR $targetPlugin / $targetObject --> $targetMethod*** <br>";
            //$this->K_COMMON->printArray($dataCst);
            //$this->K_COMMON->printArray($this->arrayOfCustomizations);

            //echo "<br> **** START FOREACH $targetPlugin / $targetObject --> $targetMethod *** <br>";

            foreach ($dataCst as $data) {

                //$this->K_COMMON->printArray($data);

                $sourcePlugin = $data["pluginName"];
                $sourceObject = $data["pluginObject"];
                $cstParameters = $data["parameters"];
                $cstParametersArr = json_decode($cstParameters, true);


                //set Parameters to Send
                $methodParameters = $this->K_COMMON->getVarArray($parameters, 0);
                if (!is_array($methodParameters))
                    $methodParameters = [];
                $methodParameters = array_merge($methodParameters, $cstParametersArr); //cstParameters overwrite methodParameters
                $arrayToSend = []; //array with all customization data
                $arrayToSend["customization"] = [];
                $arrayToSend["customization"]["objToCustomize"] = $objRet;
                $arrayToSend["customization"]["objToCustomizeResult"] = $objToCustomizeResult;
                $arrayToSend["customization"]["target"]["plugin"] = $targetPlugin;
                $arrayToSend["customization"]["target"]["pluginObject"] = $targetObject;
                $arrayToSend["customization"]["target"]["method"] = $targetMethod;
                $arrayToSend["customization"]["target"]["methodParameters"] = $methodParameters;



                //object that customize
                $obj = $this->K_COMMON->getPluginObject($sourcePlugin, $sourceObject);


                //echo "<br> **** START ESEGUO CUSTOMIZZAZIONE $targetPlugin / $targetObject --> $targetMethod *** <br>";

                //get Object
                $objRetTmp = $obj->getWOCustomBuilder($arrayToSend);

                if (!empty($objRetTmp))
                    $objRet = $objRetTmp;

                //$argumentsTmp = $objRetTmp;

                //$this->K_COMMON->printArray($argumentsTmp);
                //echo "<br> **** END ESEGUO CUSTOMIZZAZIONE $targetPlugin / $targetObject --> $targetMethod *** <br>";






            }

            //echo "<br> **** END FOREACH $targetPlugin / $targetObject --> $targetMethod *** <br>";

            //echo "<br> **** END CHECK CUSTOM $targetPlugin / $targetObject --> $targetMethod *** <br>";
        }

        //return
        return $objRet;
    }


}

?>