<?
namespace k_custommanager\objects\general\prepared;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    var $ver = "1.0";
    var $objToCustomize;

    public function get($ArrayVariable = [])
    {

        //Return
        return $this;

    }

    protected function customBuilder($ArrayVariable)
    {

        //set variables
        $targetMethodParameters = $ArrayVariable["customization"]["target"]["methodParameters"];

        $this->objToCustomize = $ArrayVariable["customization"]["objToCustomize"];

        $targetMode = $this->K_COMMON->getVarArray($targetMethodParameters, "target_mode");

        $objRet = $this->objToCustomize;

        //search if there is an auto-custom mode
        switch ($targetMode) {
            case "tab":

                //get objects
                $objTab = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tab");

                //get variables from parameters
                $targetID = $this->K_COMMON->getVarArray($targetMethodParameters, "target_id");
                $targetItemTabID = $this->K_COMMON->getVarArray($targetMethodParameters, "target_new_itemtab_id");
                $targetItemTabLabel = $this->K_COMMON->getVarArray($targetMethodParameters, "target_new_itemtab_label");
                $target_plugin_method = $this->K_COMMON->getVarArray($targetMethodParameters, "target_plugin_method");
                $getPluginName = $this->K_COMMON->getVarArray($targetMethodParameters, "getPluginName");
                $getPluginObject = $this->K_COMMON->getVarArray($targetMethodParameters, "getPluginObject");
                $auth = $this->K_COMMON->getVarArray($targetMethodParameters, "auth");

                $targetPluginMethod = $this->K_COMMON->getVarArray($targetMethodParameters, "pluginMethod");

                if ($target_plugin_method == $targetPluginMethod)
                {
                    $obj = $this->K_COMMON->getPluginObject($getPluginName, $getPluginObject);

                    //Add TAB
                    if ($obj->isAuthorizedPlugin($auth) || $auth == "") {

                        //get Object
                        $objRetTmp = $obj->getWO();
                        if (is_object($objRetTmp))
                        {
                            $objRet = $objRetTmp;

                            $targetItemTabLabel = $obj->dictionary[$targetItemTabLabel];
                            $objRet = $objTab->addTabToExistingTabs($this->objToCustomize, $targetID, $targetItemTabID, $targetItemTabLabel, $objRet);
                        }


                    }
                }


                break;

            case "tableListButton":
                //get variables from customization parameters
                $target_button_title = $this->K_COMMON->getVarArray($targetMethodParameters, "target_button_title");
                $target_button_before = $this->K_COMMON->getVarArray($targetMethodParameters, "target_button_before");
                $target_button_url = $this->K_COMMON->getVarArray($targetMethodParameters, "target_button_url");
                $target_button_class = $this->K_COMMON->getVarArray($targetMethodParameters, "target_button_class");
                $target_button_get_attribute = $this->K_COMMON->getVarArray($targetMethodParameters, "target_button_get_attribute");
                $target_button_get_column_value = $this->K_COMMON->getVarArray($targetMethodParameters, "target_button_get_column_value");
                $target_plugin_method = $this->K_COMMON->getVarArray($targetMethodParameters, "target_plugin_method");
                $getPluginName = $this->K_COMMON->getVarArray($targetMethodParameters, "getPluginName");
                $getPluginObject = $this->K_COMMON->getVarArray($targetMethodParameters, "getPluginObject");
                $auth = $this->K_COMMON->getVarArray($targetMethodParameters, "auth");

                //get general variables
                $targetPluginMethod = $this->K_COMMON->getVarArray($targetMethodParameters, "pluginMethod");
                $panelUrlPluginBase = $this->K_COMMON->getVarArray($targetMethodParameters, "panelUrlPluginBase");

                if ($target_plugin_method == $targetPluginMethod)
                {
                    $obj = $this->K_COMMON->getPluginObject($getPluginName, $getPluginObject);

                    //check authorization
                    if ($obj->isAuthorizedPlugin($auth) || $auth == "") {

                        $link = $panelUrlPluginBase . $target_button_url;
                        if ($target_button_title != "")
                            $btnTitle = $obj->dictionary[$target_button_title];
                        else
                            $btnTitle = "";

                        $this->objToCustomize = $this->addButtonTableList(
                            [
                                "link" => $link,
                                "btnTitle" => $btnTitle,
                                "btnBefore" => $target_button_before,
                                "btnClass" => $target_button_class,
                                "getAttribute" => $target_button_get_attribute,
                                "getColumnValue" => $target_button_get_column_value
                            ],
                            $this->objToCustomize
                        );
                    }
                }





                break;



        }

        return $objRet;
    }


    function addButtonTableList($arrayVariable, $objToCustomize)
    {


        $link = $this->K_COMMON->getVarArray($arrayVariable, "link");
        $btnTitle = $this->K_COMMON->getVarArray($arrayVariable, "btnTitle");
        $btnBefore = $this->K_COMMON->getVarArray($arrayVariable, "btnBefore");
        $btnClass = $this->K_COMMON->getVarArray($arrayVariable, "btnClass");
        $getAttribute = $this->K_COMMON->getVarArray($arrayVariable, "getAttribute");
        $getColumnValue = $this->K_COMMON->getVarArray($arrayVariable, "getColumnValue");

        $objInput = $this->K_COMMON->getPluginObject("k_htmlcomponents", "input");
        $objDom = $objToCustomize->getDOMObject();


        // get element and create new button
        $arrayElement = $objDom->getElementsByClass($btnBefore);


        foreach ($arrayElement as $node) {


            if ($getAttribute != "")
                $attributeValue = trim($node->getAttribute($getAttribute));
            elseif ($getColumnValue != "") {
                $attributeValue = trim($node->parentNode->parentNode->parentNode->childNodes->item($getColumnValue)->nodeValue);
            }
            else
                $attributeValue = "";


            $objInput->getWO(
                [
                    "type" => "btn"
                    , "title" => $btnTitle
                    , "link" => $link . $attributeValue
                    , "class" => $btnClass
                    , "size" => "sm"
                ]
            );


            //Append Button
            $fragment = $objDom->getNodeToImport($objInput->getHTML());
            $objToCustomize->mergeObjectVariables($objInput);
            $node = $node->parentNode;
            $node->insertBefore($fragment, $node->firstChild);

        }

        return $objToCustomize;

    }


}

?>