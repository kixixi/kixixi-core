<?
/**
 * KIXIXI k_updmanager (update)
 * Create a panel web object for update single plugin|object
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_updmanager
 * @subpackage objects\p_update
 * @since 1.00
 */

namespace k_updmanager\objects\general\update;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";


    public function updatePlugin($ArrayVariable)
    {

        //set time limit connection
        set_time_limit(5000);

        //set variable
        $pluginName = $this->K_COMMON->getVarArray($ArrayVariable, "pluginName");
        $pluginName = strtolower($pluginName);
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $typeUpdate = $this->K_COMMON->getVarArray($ArrayVariable, "typeUpdate");
        $firstInstall = $this->K_COMMON->getVarArray($ArrayVariable, "firstInstall");
        $getPluginsByFiles = $this->K_COMMON->getVarArray($ArrayVariable, "getPluginsByFiles");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $success = true;
        $errorMsg = "";
        $successMsg = "";
        $errorMsgTmp = "";
        $successMsgTmp = "";

        //include obj
        $objPlugins = $this->K_COMMON->getPluginObject("k_common", "plugins");

        //load definition of plugins
        if ($pluginName != "") {
            $resImport = $objPlugins -> importPlugin(
                [
                    "plgID" => $pluginName
                    ,"pluginObject" => $pluginObject
                    , "typeUpdate" => $typeUpdate
                    , "firstInstall" => $firstInstall
                    , "webCode" => $webCode
                ]
            );
            $errorMsgTmp .= $resImport["ErrorMsg"];
            $successMsgTmp .= $resImport["GoodMsg"];
        } else //execute on all plugins
        {
            $objUsefulData = $this->K_COMMON->getPluginObject("k_usefuldata", "api");
            $pluginList = $objUsefulData->listPluginsAndObjects(
                [
                    "OM" => "array"
                    , "withKCore" => "1"
                    , "byFiles" => $getPluginsByFiles
                ]
            );

            foreach ($pluginList as $pluginArr) {

                $pluginName = $pluginArr["value"];

                //echo "<br><br><br> <strong>PN ===> $pluginName </strong>";



                $resImport = $objPlugins -> importPlugin(
                    [
                        "plgID" => $pluginName
                        ,"pluginObject" => ""
                        , "typeUpdate" => $typeUpdate
                        , "firstInstall" => $firstInstall
                        , "webCode" => $webCode
                    ]
                );
                $errorMsgTmp .= $resImport["ErrorMsg"];
                $successMsgTmp .= $resImport["GoodMsg"];




            }

        }


        //set variable response
        if ($successMsgTmp != "") {
            $successMsg .= "******************************<br>";
            $successMsg .= "* PLUGIN: " . $pluginName . "<br>";
            $successMsg .= "******************************<br>";
            $successMsg .= $successMsgTmp;
        }
        if ($errorMsgTmp != "") {
            $errorMsg .= "******************************<br>";
            $errorMsg .= "* PLUGIN: " . $pluginName . "<br>";
            $errorMsg .= "******************************<br>";
            $errorMsg .= $errorMsgTmp;
        }

        $successMsg = str_replace("	", "", $successMsg);
        $successMsg = str_replace(chr(13) . chr(10), "", $successMsg);
        $successMsg = str_replace(chr(13), "", $successMsg);

        $errorMsg = str_replace("	", "", $errorMsg);
        $errorMsg = str_replace(chr(13) . chr(10), "", $errorMsg);
        $errorMsg = str_replace(chr(13), "", $errorMsg);


        //check if success
        if ($errorMsg != "")
            $success = false;

        //return
        if ($success) return array("success" => "true", "modUpdated" => $pluginName, "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "modUpdated" => $pluginName, "errormsg" => $errorMsg, "successmsg" => $successMsg);


    }

    

}

?>