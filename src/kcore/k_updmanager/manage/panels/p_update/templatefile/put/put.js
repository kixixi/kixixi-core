// create namespace
KXJS.namespace("k_updmanager.objects.p_update");

// create method of plugin
(function(NS) {
    var t = this;
    t.pluginName = "k_updmanager";
    t.pluginObject= "manage\\panels\\p_update";
    t.submitForm = function(){
        waitingDialog.show("Updating...", {dialogSize: "sm", progressType: "warning"});
        $.post(KXJS.buildAPICallUrl("KXCJS",t.pluginName,t.pluginObject,"update"), $("#updateForm").serialize(),
            function(data) {
                var r = $.parseJSON(data);
                KXJS.showSavedMessageJSON("#updateForm .savedMessage",r);
                waitingDialog.hide();
                $("#divMessages").html(r.successmsg + "<br><br>" + r.errormsg);
            });
    };
    t.pluginChange = function(){
        KXJS.loadDataCombo({plugin:t.pluginName,pluginObject:t.pluginObject,pluginAction:"listModuleObjects",comboID:"#updateForm #pluginObject",optionValue:"value",optionText:"text",parameters:{plugin:$("#updateForm #pluginName").val()}});
    }


    t.ready = function() {
        $("#updateForm #pluginName").change(t.pluginChange);


        $("#updateForm").validator().on("submit", function (e) {
            if (e.isDefaultPrevented()) {
            } else {
                e.preventDefault();
                t.submitForm();
            }
        });

    }

    this.init = function(){
        $(document).ready(t.ready);
    };

    this.init();

}).apply(KXNS.k_updmanager.objects.p_update);

