<?
/**
 * KIXIXI k_updmanager (p_update)
 * Create a panel web object for update single plugin|object
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_updmanager
 * @subpackage objects\p_update
 * @since 1.00
 */
namespace k_updmanager\manage\panels\p_update;
class obj extends \k_webmanager\objects\general\base_panel\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
    }

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        if (!$this->isDeveloper())
            return json_encode(["success" => "false", "reason" => "securityError"]);

        //switch request
        switch ($KCA) {
            case "update" :

                //set web code to update
                $ArrayVariable["webCode"] = $this->K_COMMON->getEditingWeb();

                $objUPD = $this->K_COMMON->getPluginObject("k_updmanager","update");
                return json_encode($objUPD->updatePlugin($ArrayVariable));
                break;
        }
    }

    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("get", "put", "")
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {

        //set variables
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        //Assign Page Title
        $this->addSEOMetaTag(["title" => "Update Manager"]);

        switch ($pluginMethod) {
            case "":
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }

        //Return
        return $this;
    }

    protected function getPagePut($ArrayVariable = array())
    {

        //add files to include
        $this->addHeadFile([$this->getPathFileJS("put")]);

        //create form
        $this->appendForm("",
            [
                "id" => "updateForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "update"

            ]
        );


        //MAIN ROW
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow, ["id" => "colSX", "nCol" => 4]);
        $this->appendColumn($this->lastRow, ["id" => "colDX", "nCol" => 7]);

        //NEW ROW COL DX
        $this->appendRow("colDX");
        $this->appendColumn($this->lastRow, ["id" => "divMessages", "nCol" => 12]);

        //NEW ROW COL SX
        $this->appendRow("colSX");

        //plugin
        $label = $this->dictionary["labelFormPlugin"];
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_comboplugin", ["labelInput" => $label, "withKCore" => "1"]);

        //NEW ROW COL SX
        $this->appendRow("colSX");

        //object
        $label = $this->dictionary["labelFormObj"];
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_combopluginobjects", ["labelInput" => $label]);

        //NEW ROW
        $this->appendRow("colSX");

        //type
        $label = $this->dictionary["labelFormType"];
        $name = "typeUpdate";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "id" => $name
                , "name" => $name
                , "value" => ""
                , "typeLoad" => "array"
                , "options" =>
                [
                    "table" => "table"
                    , "trigger" => "trigger"
                    , "sp" => "sp"

                    , "authorization" => "authorization"
                    , "parameters" => "parameters"

                    , "dictionary" => "dictionary"

                    , "messages" => "messages"
                    , "actions" => "actions"

                    , "event" => "event"
                    , "panel" => "panel"
                    , "custom" => "custom"

                    , "webobject" => "webobject"
                    , "paneltemplate" => "paneltemplate"

                    , "webtemplate" => "webtemplate"
                    , "webpage" => "webpage"

                    , "customfields" => "custom fields"
                ]
            ]
        );

        //NEW ROW
        $this->appendRow("colSX");

        //button Save
        $label = $this->dictionary["labelFormBtnUpdate"];
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);


    }




}

?>