<?
/**
 * KIXIXI k_secauth (dologin)
 * functions for manage login
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_secauth
 * @subpackage objects\dologin
 * @since 1.00
 */
namespace k_secauth\objects\general\dologin;
class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();

    }



    /**
     * Manage login with FB
     * Use "appID" and "secret" parameters
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    fbAccessToken: Facebook Access Token
     *
     * @return array ["success" => "true"] | ["success" => "false"]
     *
     */
    public function loginFB($arrayVariable = array())
    {

        //get variables
        $FB_appId = $this->getParamValue("FB_appId");
        $FB_secret = $this->getParamValue("FB_secret");
        $fbAccessToken = $this->K_COMMON->getVarRequest("fbAccessToken");
        if ($fbAccessToken == "")
            $fbAccessToken = $this->K_COMMON->getVarArray($arrayVariable, "fbAccessToken");

        //include
        $pathSDKFB = $this->K_COMMON->getPathSDK(["name" => "facebook"]);
        include_once($pathSDKFB . '/fbbase/facebook.php');

        //facebook object
        $facebook = new \Facebook(array(
            'appId' => $FB_appId,
            'secret' => $FB_secret
        ));

        //set facebook token
        $facebook->setAccessToken($fbAccessToken);

        //get user connected to facebook
        $fbuser = $facebook->getUser();

        if ($fbuser) {

            try {
                //get facebook profile
                $fb_profile = $facebook->api('/me?fields=id,name,email');

                if ($fb_profile['email'] != "") {

                    $resLogin = $this->login(array("snlogin" => "1", "fbid" => $fb_profile['id'], "user" => $fb_profile['email'], "sessionNotExpire" => "1"));


                    $ana_code = $this->K_COMMON->getCurrentUser();

                    if ($ana_code != "") {
                        $this->setFacebookID($ana_code, $fb_profile['id']);
                    }

                    return $resLogin;
                }
            } catch (FacebookApiException $e) {
                return ["success" => "false", "reason" => "fberror", "messagge" => "Error connecting to Facebook"];
            }
        }
        else
            return ["success" => "false", "reason" => "nofbuser", "messagge" => "No Facebook User"];


    }

    /**
     * Set facebook ID on registry
     *
     * @param string $anaCode user code
     * @param string $fbid user facebook ID
     *
     *
     */
    protected function setFacebookID($anaCode, $fbid)
    {

        $table = $this->K_COMMON->getPluginDataObject("k_registry", "registry");
        $table->put(["ana_facebook_id" => $fbid],["ana_code" => $anaCode]);

    }

    /**
     * try login by nickname|email and password
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    user: email | nickname
     *    *    pwd: password
     *    *    snlogin: 1 or 0 (if 1 is login from social network)
     *    *    fbid: Facebook ID (if snlogin == 1)
     *    *    sessionNotExpire: 0 or 1 (if want set session not expiry to 1)
     *
     * @return array ["success" => "true"] | ["success" => "false"]
     *
     */
    public function login($arrayVariable)
    {

        //Include Module
        $tableUSR = $this->getTable("login");
        $tableLGL = $this->getTable("log_logon");
        $objSessions = $this->K_COMMON->getPluginObject("k_sessions","handler");

        //set variables
        $User = htmlspecialchars($this->K_COMMON->getVarArray($arrayVariable, "user"), ENT_QUOTES);
        $Pwd = $this->K_COMMON->getVarArray($arrayVariable, "pwd");
        $sessionNotExpire = $this->K_COMMON->getVarRequest("sessionNotExpire");
        if ($sessionNotExpire == "")
            $sessionNotExpire = $this->K_COMMON->getVarArray($arrayVariable, "sessionNotExpire");
        $snlogin = $this->K_COMMON->getVarArray($arrayVariable, "snlogin");
        $byUserCodeLogin = $this->K_COMMON->getVarArray($arrayVariable, "byUserCodeLogin");
        $userCodeLogin = $this->K_COMMON->getVarArray($arrayVariable, "userCodeLogin");
        $fbid = $this->K_COMMON->getVarArray($arrayVariable, "fbid");
        $loginWebCodeParam = $this->getParamValue("loginWebCode", "", "k_secauth");

        if ($loginWebCodeParam != "")
            $webToLogin = $loginWebCodeParam;
        else
            $webToLogin = $this->K_COMMON->getCurrentWeb();

        //Check that snlogin is not received by request url
        if ($this->K_COMMON->getVarRequest("snlogin") != "")
            return array("success" => "false", "reason" => "security");

        //Check that byUserCodeLogin is not received by request url
        if ($this->K_COMMON->getVarRequest("byUserCodeLogin") != "")
            return array("success" => "false", "reason" => "security");

        //check variables
        if ( (($User == "") || ($Pwd == "")) && ($snlogin != "1" && $byUserCodeLogin != "1") )
            return array("success" => "false", "reason" => "security");

        //Verify which web site manage user
        $CondAdd = "lgn_web_code='" . $webToLogin . "'";

        //Check if verify ana_confirmed
        $Par_STYChkConf = $this->getParamValue("STYChkConf");
        if ($Par_STYChkConf == "1") $CondAdd .= " AND ana_confirmed=1";


        //Verify if Facebook Login or other social
        if ($snlogin == "1")
            $CondAdd .= " AND (REG.ana_facebook_id ='" . $fbid . "')";
        elseif ($byUserCodeLogin == "1")
            $CondAdd .= "REG.ana_code = '".$userCodeLogin."'";
        else
            $CondAdd .= " AND ((contacts_type='EM' AND contacts_value='$User') OR lgn_nickname='$User' OR (contacts_type='CEL' AND concat(contacts_prefix,contacts_value)='$User') ) AND lgn_pwd='" . sha1($Pwd) . "'";


        //try login
        $tableUSR->getByQuery("login_check", "", $CondAdd);
        $dataUSR = $tableUSR->fetch();


        if ($dataUSR) {


            //Insert in Log Logon Table
            $tableLGL->put(
                [
                    "lgl_web_code" => $webToLogin
                    ,"lgl_user_code" => $dataUSR->ana_code
                    , "lgl_date_logon" => ["kx_sql" => "NOW()"]
                    , "lgl_time_logon" => ["kx_sql" => "NOW()"]
                ]
            );
            $logID = $tableLGL->getNewID();

            //Set Variable Session
            $_SESSION["KSESSION_User_FirstName"] = $dataUSR->ana_first_name;
            $_SESSION["KSESSION_User_LastName"] = $dataUSR->ana_last_name;
            $_SESSION["KSESSION_Logon_User"] = $dataUSR->ana_code;
            $_SESSION["KSESSION_Level_Typ_Code"] = $dataUSR->lgn_level_code;
            $_SESSION["KSESSION_Log_Logon_ID"] = $logID;

            //Set Language
            $Language = $this->K_COMMON->getUserLanguage($dataUSR->ana_code);
            if ($Language == "") {
                $Language = $this->K_COMMON->getVarSession("KSESSION_language");
                $this->K_COMMON->setUserLanguage($dataUSR->ana_code, $Language);
            }
            $this->K_COMMON->language = strtoupper($Language);
            $_SESSION["KSESSION_language"] = strtoupper($this->K_COMMON->language);

            if ($sessionNotExpire == "1")
            {
                $objSessions->setSessionNotExpire($sessionNotExpire);
            }
            return array("success" => "true",  "token" => session_id());

        }
        else
            return array("success" => "false", "reason" => "loginerror", "message" => "Check Login and Password");


    }



    /**
     * Execute session logout for actual user
     *
     * @return array ["success" => "true"] 
     *
     */
    function logout()
    {

        //Set session to Expire
        $objSessions = $this->K_COMMON->getPluginObject("k_sessions","handler");
        $objSessions->setSessionNotExpire(0);

        //update log logon
        if (isset($_SESSION["KSESSION_Log_Logon_ID"]))
        {
            $table = $this->getTable("log_logon");
            $table->put(
                [
                    "lgl_date_logout" => ["kx_sql" => "NOW()"]
                    , "lgl_time_logout" => ["kx_sql" => "NOW()"]
                ]
                ,
                ["id_log_logon" => $_SESSION["KSESSION_Log_Logon_ID"]
                ]
            );
        }

        //destroy session
        session_destroy();

        return ["success" => "true"];


    }


    /**
     * Create a new password for a user, and send email to user
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    email: email of user to create new password
     *
     * @return array ["success" => "true"] | ["success" => "false"]
     *
     */
    function pwdLost($arrayVariable)
    {

        //get variables
        $email = $this->K_COMMON->getVarArray($arrayVariable, "email");
        $loginWebCodeParam = $this->getParamValue("loginWebCode", "", "k_secauth");
        if ($loginWebCodeParam != "")
            $webMngUser = $loginWebCodeParam;
        else
            $webMngUser = $this->K_COMMON->getCurrentWeb();

        //check email
        if ($email == "")
            return ["success" => "false", "reason" => "insertEmail", "message" => $this->dictionary["msgInsertEmail"]];
        

        //Include tables
        $tableCNT = $this->K_COMMON->getPluginDataObject("k_registry", "contacts");
        $tableLGN = $this->getTable("login");



        $tableCNT->get("*", ["contacts_value" => $email, "contacts_type" => "EM"]);
        $dataCNT = $tableCNT->fetch();

        if ($dataCNT) {
            $userCode = $dataCNT->company_code;

            //Generate Password
            $newPwd = $this->K_COMMON->generateRandomString(10);

            $tableLGN->get("*", ["lgn_web_code" => $webMngUser, "lgn_user_code" => $userCode]);
            $dataLGN = $tableLGN->fetch();

            if ($dataLGN) {
                $loginID = $dataLGN->id_login;
                $tableLGN->put(["lgn_pwd" => sha1($newPwd)], ["id_login" => $loginID]);


                $objMailing = $this->K_COMMON->getPluginObject("k_mailing","api");
                $objMailing->doAction($this->namePlugin, $this->namePluginObject, $webMngUser, "ANAGPWDLST", ["useremail" => $email], ["password" => $newPwd]);

                return ["success" => "true", "message" => $this->dictionary["msgPwdSent"]];
            } else
                return ["success" => "false", "reason" => "notFound", "message" => $this->dictionary["msgEmailNotFound"]];
        } else
            return ["success" => "false", "reason" => "notFound", "message" => $this->dictionary["msgEmailNotFound"]];


    }
}

?>