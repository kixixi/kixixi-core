<?
namespace k_secauth\objects\general\webservice;
class obj extends \k_common\objects\general\base\obj
{
    var $dictionary;
    var $ver = "1.0";
    var $curAPIVer = "1.0";
    var $curAPPVer = "1.0";

    public function __construct($AV)
    {
        parent::__construct();

    }


    /**
     * Manage Actions from ajax
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($arrayVariable)
    {
        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");
        $APIVER = $this->K_COMMON->getVarRequest("APIVER");
        $APPVER = $this->K_COMMON->getVarRequest("APPVER");

        //check version
        if ($APIVER != "") {
            $APIVER = (float)$APIVER;

            if ($APIVER < $this->curAPIVer)
                return json_encode(["success" => "false", "reason" => "apiver"]);


            if ($APPVER != "") {
                $APPVER = (float)$APPVER;

                if ($APPVER < $this->curAPPVer)
                    return json_encode(["success" => "false", "reason" => "appver"]);
            }

        }


        //switch request
        switch ($KCA) {
            case "logout":
                $obj = $this->K_COMMON->getPluginObject("k_secauth", "dologin");
                return json_encode($obj->logout());
                break;
            case "loginByToken" :
                $objLogin = $this->K_COMMON->getPluginObject("k_secauth", "dologin");
                $arrayVariable["snlogin"] = "";

                $resLogin = $objLogin->login($arrayVariable);


                return json_encode($resLogin);
                break;
            case "login" :
                $objLogin = $this->K_COMMON->getPluginObject("k_secauth", "dologin");
                $arrayVariable["snlogin"] = "";

                $resLogin = $objLogin->login($arrayVariable);

                return json_encode($resLogin);
                break;
            case "loginFB" :
                $objLogin = $this->K_COMMON->getPluginObject("k_secauth", "dologin");
                $resLogin = $objLogin->loginFB($arrayVariable);
                return json_encode($resLogin);
                break;
            case "pwdLost" :
                $objLogin = $this->K_COMMON->getPluginObject("k_secauth", "dologin");
                echo json_encode($objLogin->pwdLost($arrayVariable));
                break;
            case "changePwd":
                $objPassword = $this->K_COMMON->getPluginObject("k_secauth", "password");
                return json_encode($objPassword->change($arrayVariable));
                break;
        }
    }




}

?>