<?
/**
 * KIXIXI k_secauth (api)
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_secauth
 * @subpackage objects\api
 * @since 1.00
 */
namespace k_secauth\objects\general\api;

class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";
    /**
     * Check if user logged, if not logged opern login page
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    nexturl: optional url after login (if empty get actual url)
     *
     * @return boolean
     *
     */
    public function verifyUserLoggedGoLogin($arrayVariable = [])
    {


        $nexturl = $this->K_COMMON->getVarArray($arrayVariable, "nexturl");

        if ($nexturl == "")
            $nexturl = $_SERVER['REQUEST_URI'];

        if (!$this->verifyUserLogged()) {
            $loginURL = $this->getLoginURL();
            echo '<script>location.href = "' . $loginURL . '?nexturl=' . urlencode($nexturl) . '";</script>';
            return false;
        } else {
            return true;
        }
    }


    /**
     * Check if user have a login for a web site
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    anaCode: user code to check
     *    *    webCode: web code to check
     *
     * @return boolean
     *
     */
    public function checkIfUserHaveLogin($arrayVariable = [])
    {

        //set variables
        $anaCode = $this->K_COMMON->getVarArray($arrayVariable, "anaCode");
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");

        $loginWebCodeParam = $this->getParamValue("loginWebCode", $webCode, "k_secauth");
        if ($loginWebCodeParam != "")
            $webMngUser = $loginWebCodeParam;
        else
            $webMngUser = $webCode;

        $table = $this->K_COMMON->getPluginDataObject("k_secauth","login");

        $table->getByQuery("login_with_name","",["lgn_web_code"=>$webMngUser, "lgn_user_code"=>$anaCode]);

        $data = $table->fetch();

        if ($data)
            return true;
        else
            return false;
    }

    /**
     * Check if user is authorized
     *
     * @param string $authCode code of authorization
     * @param string $pluginName code of plugin to check auth
     * @param string $webCodeTarget webCode Target to check auth
     * @param string $customField optional code of custom Field
     *
     * @return boolean
     *
     */
    public function checkAuthorization($authCode, $pluginName, $webCodeTarget = "", $customField = "")
    {


        //Load Table
        $table = $this->getTable("permission");

        //Check if user logged
        if (!$this->verifyUserLogged())
            return 0;

        //set variables
        $userLevel = $this->K_COMMON->getCurrentUserLevelType();
        $userCode = $this->K_COMMON->getCurrentUser();
        $webCode = $this->K_COMMON->getCurrentWeb();

        //Verify Permission
        $where = " AND web_code = '".$webCode."'";
        $where .= " AND code_form='$authCode' ";

        if ($pluginName != "")
            $where .= " AND plugin_name = '" . $pluginName . "'";

        if ($customField != "")
            $where .= " AND custom_field_code_value = '" . $customField . "'";

        if ($webCodeTarget != "")
            $where .= " AND web_code_target = '" . $webCodeTarget . "'";

        //$table->showSQL();
        $table->get("*", [
            "(( "
            . " level_type_code='$userLevel' "
            . $where . " ) "
            . " OR (employee_person_code='$userCode' "
            . $where . " ) "
            . " )"
        ]);

        If ($table->fetch())
            return true;
        else
            return false;


    }

    /**
     * Check if user logged
     *
     *
     * @return boolean
     *
     */
    public function verifyUserLogged()
    {
        if (isset($_SESSION["KSESSION_Logon_User"]))
            if ($_SESSION["KSESSION_Logon_User"] != "") return true;
            else return false;
        else return false;
    }

    /**
     * get array with plugins that have authorization
     *
     *
     * @return array
     *
     */
    public function getPluginsWithAuth()
    {
        //get Table
        $table = $this->K_COMMON->getPluginDataObject("k_secauth", "authtype");
        $table->getByQuery("authtype_with_plugin_name",
            ["value" => "plugin_name", "text" => 'concat(UPPER(plg_type)," - ", plg_name)'], "", "", ["plugin_name"], ["plg_type", "plg_name"]);
        return $table->getResults("array");
    }

    /**
     * get plugin authorizations
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    plgID: name of plugin
     *
     * @return array
     *
     */
    public function getListAuth($arrayVariable)
    {
        //set variable
        $OM = $this->K_COMMON->getVarArray($arrayVariable,"OM");
        $plgID = $this->K_COMMON->getVarArray($arrayVariable, "plgID");

        //get Table
        $table = $this->K_COMMON->getPluginDataObject("k_secauth", "authtype");

        //get all websites for combo
        $table->get(["value" => "code_form", "text" => "code_form"], ["plugin_name" => $plgID]);
        return $table->getResults($OM);

    }

    /**
     * get name of authorization
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginName: name of plugin
     *    *    pluginObject: name of plugin object
     *    *    authCode: auth code to get name
     *
     * @return string
     *
     */
    public function getAuthName($arrayVariable)
    {
        $plugin = $this->K_COMMON->getVarArray($arrayVariable, "pluginName");
        $auth = $this->K_COMMON->getVarArray($arrayVariable, "authCode");
        $arrayWhere = [];
        $arrayWhere["plugin_name"] = $plugin;
        $arrayWhere["code_form"] = $auth;

        //get Table
        $table = $this->K_COMMON->getPluginDataObject("k_secauth", "authtype");
        $table->get(["description"], $arrayWhere);
        $row = $table->fetch();
        if ($row)
            return $row->description;
        else
            return "";
    }

   
    /**
     * get array of auth level
     *
     * @return array
     *
     */
    public function getArrayAuthLvl($arrayVariable)
    {
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");
        if ($webCode == "")
            $webCode = $this->K_COMMON->getEditingWeb();

        //get Table
        $tableAuthLvl = $this->getTable("authlevels");

        //get all websites for combo
        $tableAuthLvl->get(["value" => "level_type_code", "text" => "level_type_description"], ["web_code" => $webCode]);
        return $tableAuthLvl->getResults("array");
    }

    /**
     * get Auth Level Name
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    webCode: web code
     *    *    levelCode: level code to get name
     *
     * @return string
     *
     */
    public function getAuthLevelName($arrayVariable)
    {
        $levelCode = $this->K_COMMON->getVarArray($arrayVariable, "levelCode");
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");
        if ($webCode == "")
            $webCode = $this->K_COMMON->getEditingWeb();

        //get Table
        $tableAuthLvl = $this->getTable("authlevels");

        //get all websites for combo
        $tableAuthLvl->get(["level_type_description"], ["level_type_code" => $levelCode, "web_code" => $webCode]);

        $data = $tableAuthLvl->fetch();
        if ($data)
            return $data->level_type_description;
        else
            return "";
    }


    /**
     * Add User Authorization
     *
     * @param string $anaCode user ana code
     * @param string $pluginName code of plugin auth
     * @param string $authCode code of authorization
     * @param string $valueCustomField optional custom field value
     *
     * @return array
     *
     */
    public function addUserAuth($anaCode, $pluginName, $authCode, $valueCustomField = "")
    {
        //get Table
        $table = $this->K_COMMON->getPluginDataObject("k_secauth","permission");

        if($anaCode != "")
            $table->put(
                [
                    "employee_person_code"=>$anaCode
                    ,"plugin_name"=>$pluginName
                    ,"code_form"=>$authCode
                    ,"custom_field_code_value"=>$valueCustomField
                ]
                
            );
    }


    /**
     * Create a new password for a user, without sending email
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    anaCode: ana code of user
     *
     * @return array ["success" => "true", "pwd" => PASSWORD] | ["success" => "false"]
     *
     */
    function resetPwd($arrayVariable)
    {

        //get variables
        $anaCode = $this->K_COMMON->getVarArray($arrayVariable, "anaCode");

        //check email
        if ($anaCode == "")
            return ["success" => "false", "reason" => "noAnaCode", "message" => "No User Code"];

        //Include tables
        $tableLGN = $this->getTable("login");

        
        $loginWebCodeParam = $this->getParamValue("loginWebCode", "", "k_secauth");
        if ($loginWebCodeParam != "")
            $webMngUser = $loginWebCodeParam;
        else
            $webMngUser = $this->K_COMMON->getCurrentWeb();


        //Generate Password
        $newPwd = $this->K_COMMON->generateRandomString(10);

        $tableLGN->get("*", ["lgn_web_code" => $webMngUser, "lgn_user_code" => $anaCode]);
        $dataLGN = $tableLGN->fetch();

        if ($dataLGN) {
            $loginID = $dataLGN->id_login;
            $tableLGN->put(["lgn_pwd" => sha1($newPwd)], ["id_login" => $loginID]);


            return ["success" => "true", "pwd" => $newPwd];
        } else
            return ["success" => "false", "reason" => "notFound", "message" => "User Not Found"];


    }

}

?>