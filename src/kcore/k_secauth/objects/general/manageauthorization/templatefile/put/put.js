// create namespace
KXJS.namespace("k_secauth.objects.authorization");

// create method of plugin
(function(NS) {
	var t = this;


	t.authChange = function(){
		var codeTabRef = $('#addAuthorizationForm #selectAuth option:selected').attr('codeTabRef');
        var authByWCTarget = $('#addAuthorizationForm #selectAuth option:selected').attr('authByWCTarget');



		$("#addAuthorizationForm #cstmFSelectCol").hide();
        $("#addAuthorizationForm #authWCTSelectCol").hide();
		$("#addAuthorizationForm #codeTabRef").val(codeTabRef);


		if(codeTabRef != "" && typeof(codeTabRef) != "undefined"){
			$("#addAuthorizationForm #cstmFSelectCol").show();
			KXJS.loadDataCombo({plugin:"k_secauth",pluginObject:"manageauthorization",pluginAction:"getAuthCustomField",comboID:"#customSelectCode",optionValue:"value",optionText:"text",parameters:{pluginRef:$("#addAuthorizationForm #selectPlugin").val(),codeRef:codeTabRef}});
		}

		if (authByWCTarget == "1" && typeof(authByWCTarget) != "undefined")
            $("#addAuthorizationForm #authWCTSelectCol").show();

	}
	
	t.ready = function() {
		$("#addAuthorizationForm #selectAuth").change(t.authChange);



	}

	this.init = function(){
		$(document).ready(t.ready);
	};

	this.init();

}).apply(KXNS.k_secauth.objects.authorization);

