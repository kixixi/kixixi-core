// create namespace
KXJS.namespace("k_secauth.objects.authorization");
// create method of plugin
(function() {
	var t = this;
	t.askDeleteAuthorization = function(){
		var keyID = $(this).attr("keyID");
		if(keyID == ""){
			alert("error");
			return;
		}
		dialogModal = waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'warning'});
		$.ajax({
			url: KXJS.buildAPICallUrl("KXCJS","k_secauth","manageauthorization","askConfirm"),
			data: {
				type: "delete",
				idModal:"deleteAuthorizationModal"}

		}).done(function(data) {
			r = $.parseJSON(data);

			dialogModal.on('hidden.bs.modal'
				, function () {

					if(r.success == "true"){
						$(r.html).appendTo('body');
						$('#deleteAuthorizationModal').modal("show");
						$('#deleteAuthorizationModal').on('hidden.bs.modal', function (e) {
							$('#deleteAuthorizationModal').remove();
						});

						$('#deleteAuthorizationModal input[name="no"]').click(function(){$('#deleteAuthorizationModal').modal("hide");});
						$('#deleteAuthorizationModal input[name="yes"]').click(function(){$('#deleteAuthorizationModal').modal("hide");t.deleteAuthorization(keyID);});
					}
					else
						alert("Error Security");

				});
            waitingDialog.hide();

		});
	}
	t.deleteAuthorization = function(keyID){
		$.ajax({
			url: KXJS.buildAPICallUrl("KXCJS","k_secauth","manageauthorization","deleteAuthorization"),
			data: {
				keyID: keyID
				}

		}).done(function(data) {
			var r = $.parseJSON(data);
			if(r.success == "true")
				t.loadTableList(r.anaCode, r.webCode, r.authLvlCode);
		});
	}
	t.addAuthorization  = function(){
		var anaCode = $(this).attr("anaCode");
		var webCode = $(this).attr("webCode");
		var authLvlCode = $(this).attr("authLvlCode");
		dialogModal = waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'warning'});
		$.ajax({
			url: KXJS.buildAPICallUrl("KXCJS","k_secauth","manageauthorization","addAuthorization"),
			data: {
				idModal:"modalAddAuthorization",
				anaCode:anaCode,
				webCode: webCode,
				authLvlCode:authLvlCode

				}

		}).done(function(data) {
			var r = $.parseJSON(data);

			dialogModal.on('hidden.bs.modal'
				, function () {

					if(r.success == "true"){
						$(r.html).appendTo('body');
						$('#modalAddAuthorization').modal("show");
						$('#modalAddAuthorization').on('hidden.bs.modal', function (e) {
							$('#modalAddAuthorization').remove();
							t.loadTableList(anaCode, webCode, authLvlCode);

						});

					}
					else
						alert("Error Security");

				});
            waitingDialog.hide();

		});
	};
	t.loadTableList = function(anaCode, webCode, authLvlCode){
		dialogModal = waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'warning'});

		$.ajax({
			url: KXJS.buildAPICallUrl("KXCJS","k_secauth","manageauthorization","getTableList"),
			data: {
				onlyRow: "1",
				anaCode:anaCode,
				webCode: webCode,
				authLvlCode:authLvlCode
				}

		}).done(function(data) {
			var r = $.parseJSON(data);

			dialogModal.on('hidden.bs.modal'
				, function () {

					if(r.success == "true"){
						$('#authorizationTableList tbody').html(r.html);
						t.assignTrigger();
					}
					else
						alert("Error Security");

				});
            waitingDialog.hide();

		});

	}
	t.assignTrigger = function() {
		$(".btnDeleteAuthorization").click(t.askDeleteAuthorization);
	}
	t.ready = function() {
		t.assignTrigger();
		$("#btnAddAuth").click(t.addAuthorization);
	}

	this.init = function(){
		$(document).ready(t.ready);
	};

	this.init();

}).apply(KXNS.k_secauth.objects.authorization);

