<?
/**
 * KIXIXI k_secauth (manage authorization)
 * Create a web object, with list of authorization of a user or of a authorization level
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_secauth
 * @subpackage objects\manageauthorization
 * @since 1.00
 */
namespace k_secauth\objects\general\manageauthorization;
class obj extends \k_webmanager\objects\general\general_object\obj
{
	/**
	 * Version of object
	 *
	 * @since 1.00
	 * @access public
	 * @var string
	 */
	var $ver = "1.0";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authManageSecurity";

    /**
     * anaCode of user to manage auth
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $anaCode = "";

    /**
     * webCode of user to manage auth
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $webCode = "";

	/**
	 * levelCode of authorization to manage auth
	 *
	 * @since 1.00
	 * @access public
	 * @var string
	 */
	var $authLvlCode = "";

	/**
	 * Constructor
	 *
	 */
	public function __construct() {
		parent::__construct();
		$this -> dictionary = $this->getDictionary();

	}

	/**
	 * Manage Actions from ajax
	 *
	 * @param array $ArrayVariable array of variables
	 * The following directives can be used in the array variables:<br>
	 *    *    KCA: Action to execute
	 *
	 * @return string|json|array depend on ajax type call
	 *
	 */
	public function objectRequest($ArrayVariable) {

   		//set variable
		$KCA = $this->K_COMMON->getVarRequest("KCA");

		//switch request
		switch ($KCA) {
			case "saveData" :
				return json_encode($this->saveData($ArrayVariable));
			break;
			case "askConfirm" :
				echo json_encode($this -> getHtmlAskConfirm($ArrayVariable));
			break;
			case "deleteAuthorization" :
				echo json_encode($this ->delete($ArrayVariable));
			break;
			case "addAuthorization":
				echo json_encode($this->getModalEdit($ArrayVariable));
			break;
			case "getTableList":
				$tableList = $this->getTableList($ArrayVariable);
				echo json_encode(["success" => "true", "html" => $tableList->getHTML()]);
			break;
            case "getAuthCustomField":
                return $this->getAuthCustomField($ArrayVariable);
                break;

		}





	}


	/**
	 * get this object
	 *
	 * @param array $arrayVariable array of variables
	 * The following directives can be used in the array variables:<br>
	 *    *    pluginMethod: method of plugin ("list", "")
	 *    *    anaCode: user code to get authorization
	 *    *    authLvlCode: auth code to get authorization
	 *
	 * @return object this object
	 */
	public function get($arrayVariable = array())
	{

		//set variables
		$pluginMethod = $this->K_COMMON->getVarArray($arrayVariable, "pluginMethod");
		$this->anaCode = $this->K_COMMON->getVarArray($arrayVariable,"anaCode");
        $this->webCode = $this->K_COMMON->getVarArray($arrayVariable,"webCode");
		$this->authLvlCode = $this->K_COMMON->getVarArray($arrayVariable,"authLvlCode");
        $loginWebCodeParam = $this->getParamValue("loginWebCode", $this->webCode, "k_secauth");

        if ($loginWebCodeParam != "" && $this->webCode != $loginWebCodeParam){

            //NEW ROW
            $this->appendRow("", ["margin" => "10px 0 20px 0"]);

            //description
            $desc = $this->getDictionaryWord("msgManageAuthFromCorrectWS");
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendHTML($this->lastCol, $desc);

            return $this;
        }
		
		//check security
		if($this->anaCode == "" && $this->authLvlCode == "")
			return $this;

		switch ($pluginMethod) {
			case "list":
			case "":
				$this->getPageList($arrayVariable);
				break;

		}

		//Return
		return $this;
	}

	protected function getPageList($arrayVariable)
	{


		//add files to include
		$this->addHeadFile([$this->getPathFileJS("list")]);


		//NEW ROW
		$this->appendRow("");

		//get Table List
		$objTableList = $this->getTableList($arrayVariable);
		$this->appendColumn($this->lastRow, ["nCol" => 12]);
		$this->appendObject("", $objTableList);

		//NEW ROW
		$this->appendRow("");

		//append hidden variables
		$this->appendInput($this->lastRow, ["type" => "hidden", "id" => "anaCode", "name" => "anaCode", "value" => $this->anaCode]);
        $this->appendInput($this->lastRow, ["type" => "hidden", "id" => "webCode", "name" => "webCode", "value" => $this->webCode]);
		$this->appendInput($this->lastRow, ["type" => "hidden", "id" => "authLvlCode", "name" => "authLvlCode", "value" => $this->authLvlCode]);



		//button
		$label = $this->dictionary["LabelAddAuth"];
		$name = "btnAddAuth";
		$this->appendColumn($this->lastRow, ["nCol" => 3]);
		$this->appendInput($this->lastCol,
            [
                "type" => "btn",
                "value" => $label,
                "id" => $name,
                "name" => $name,
                "semanticColor" => "outline-success",
                "attributes" => ["anaCode" => $this->anaCode, "webCode" => $this->webCode, "authLvlCode" => $this->authLvlCode]
            ]
        );


	}

	protected function getTableList($ArrayVariable = array()){

		//set variables
		$anaCode = $this->anaCode;
		$authLvlCode = $this->authLvlCode;
		if($anaCode == "")
			$anaCode = $this->K_COMMON->getVarArray($ArrayVariable,"anaCode");	
		if($authLvlCode == "")
			$authLvlCode = $this->K_COMMON->getVarArray($ArrayVariable,"authLvlCode");	

		$onlyRow = $this->K_COMMON->getVarArray($ArrayVariable,"onlyRow");


		$OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents","tablelist");

		$OBJTableList->newTable(["id"=>"authorizationTableList"]);
		$OBJTableList->addColumnHead($this->dictionary["LabelPlugin"],"","",$onlyRow);
		//$OBJTableList->addColumnHead($this->dictionary["LabelObj"],"","",$onlyRow);
		$OBJTableList->addColumnHead($this->dictionary["LabelAuthorization"],"","",$onlyRow);
		$OBJTableList->addColumnHead($this->dictionary["LabelCustomFieldCode"],"","",$onlyRow);
        $OBJTableList->addColumnHead($this->dictionary["LabelWebCodeTarget"],"","",$onlyRow);
		$OBJTableList->addColumnHead("","","",$onlyRow);

		if($anaCode == "" && $authLvlCode == "")
			return $OBJTableList->get();


		//Include Tables
        $table = $this->K_COMMON->getPluginDataObject("k_secauth","permission");
        $tableWS = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_sites");
        $tableAuth = $this->K_COMMON->getPluginDataObject("k_secauth", "authtype");


        //load value
        if($anaCode !="")
			$table->get("*",["employee_person_code"=>$anaCode],"","",["plugin_name"]);
		elseif($authLvlCode != "")
			$table->get("*",["level_type_code"=>$authLvlCode],"","",["plugin_name"]);

		while( $row= $table->fetch()){
			$keyID = $row->id_permission;
			$code = $row->code_form;
			$plugin = $row->plugin_name;
			$value = $row->custom_field_code_value;
			$webCodeTarget = $row->web_code_target;
			$tableAuth->get("*",["plugin_name"=>$plugin,"code_form"=>$code]);
        	$descr = $code;

			if($rowAuth = $tableAuth->fetch()){

			    if ($rowAuth->description != "")
				    $descr = $rowAuth->description;

                $label = $rowAuth->code_label;

                if ($label != "")
                    $descr = $this->getDictionaryWord($label, $plugin, "");

				$codeTableRef = $rowAuth->code_table_ref;
				if($codeTableRef != ""){
					$pluginRef = $this->K_COMMON->getPluginObject($plugin,"api");
					$valueArray = $pluginRef->getAuthCustomField(["codeRef"=>$codeTableRef,"code"=>$value,"OM"=>"array"]);
					$valueArray = $this->K_COMMON->getVarArray($valueArray,"0");
					$value = $this->K_COMMON->getVarArray($valueArray,"text");
				}
				
			}
            $webSiteTarget = "";
            if($webCodeTarget != ""){
                $tableWS->get(["web_name"],["web_code" => $webCodeTarget]);
                if ($dataWS = $tableWS -> fetch())
                    $webSiteTarget = $dataWS->web_name;
            }
			$OBJTableList->addRow();
			$OBJTableList->addColumn("");
			$OBJTableList->addValue($plugin,"","","col-md-12","");
			$OBJTableList->addColumn("");
			$OBJTableList->addValue($descr,"","","col-md-12","");
			$OBJTableList->addColumn("");
			$OBJTableList->addValue($value,"","","col-md-12","");
            $OBJTableList->addColumn("");
            $OBJTableList->addValue($webSiteTarget,"","","col-md-12","");
			$OBJTableList->addColumn("","","","","","");
			$OBJTableList->addButton("",$this->dictionary["labelDeleteAuthorization"],"#","btnDeleteAuthorization btn-secondary btn-sm width40"," keyID=\"".$keyID."\"","fa fa-remove");
		}
		
		
		return $OBJTableList->get();
			
	
	}

	protected function getHtmlAskConfirm($ArrayVariable) {

		//Get Variable
		$type = $this->K_COMMON->getVarArray($ArrayVariable, "type");
		$idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

		switch ($type) {
			case "delete" :
				return $this->K_COMMON->getModalAskConfirm(["idModal" => $idModal, "type"=>"general", "body" => $this->dictionary["MsgAskDeleteAuthorization"]]);
				break;
		}

	}

	protected function getModalEdit($ArrayVariable){



		//Get Variable
		$anaCode = $this->K_COMMON->getVarArray($ArrayVariable,"anaCode");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable,"webCode");
		$authLvlCode = $this->K_COMMON->getVarArray($ArrayVariable,"authLvlCode");
		$idModal = "modalAddAuthorization";

		//get Modal Object
		$webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");
		$webModal->newModal(["id" => $idModal, "title" => "", "type" => "general"]);

		//add files to include
		$webModal->addHeadFile([$this->getPathFileJS("put")]);

		//create form
		$webModal->appendForm($webModal->modalBody,
			[
				"id" => "addAuthorizationForm"
				, "setSavedMessage" => "1"
				, "kcp" => $this->namePlugin
				, "kco" => $this->namePluginObject
				, "kca" => "saveData"
                , "closeModal" => $idModal
                , "reloadTableList" => "authorizationTableList"
			]
		);

		//fields hidden
		$webModal->appendInput($webModal->lastForm, ["type" => "hidden", "id" => "anaCode", "name" => "anaCode", "value" => $anaCode]);
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "id" => "webCode", "name" => "webCode", "value" => $webCode]);
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "id" => "authLvlCode", "name" => "authLvlCode", "value" => $authLvlCode]);
		$webModal->appendInput($webModal->lastForm, ["type" => "hidden", "id" => "codeTabRef", "name" => "codeTabRef", "value" => ""]);


		//NEW ROW
		$webModal->appendRow($webModal->lastForm);

		//plugin
		$label = $this->dictionary["LabelPlugin"];
		$name = "selectPlugin";
		$webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
		$webModal->appendInput($webModal->lastCol,
			[
				"type" => "select"
				,"labelInput" => $label
				,"id" => $name
				,"name" => $name
				,"value" => ""
				,"required" => "1"
				,"typeLoad"=>"function"
				,"pluginName"=>"k_secauth"
				,"pluginObject"=>"api"
				,"functionName"=>"getPluginsWithAuth"
			]
		);



		//NEW ROW
		$webModal->appendRow($webModal->lastForm);

		//authorization
		$label = $this->dictionary["LabelAuthorization"];
		$name = "selectAuth";
		$webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
        $webModal->appendInput($webModal->lastCol, [
            "type" => "select",
            "labelInput" => $label,
            "id" => $name,
            "name" => $name,
            "value" => "",
            "required" => "0",
            "loadTriggerRef" => "#selectPlugin",
            "parameterRef" => "plugin",
            "typeLoad" => "ajax",
            "pluginName" => "k_usefuldata",
            "pluginObject" => "api",
            "pluginAction" => "getListAuthType",
            "triggerFunctionAfter" => "KXNS.k_secauth.objects.authorization.authChange",
            "optionAttributes" => ["codeTabRef","authByWCTarget"]

        ]);


        //NEW ROW
        $webModal->appendRow($webModal->lastForm);
        $webModal->appendColumn($webModal->lastRow, ["id"=>"authWCTSelectCol", "class" => "hidden", "nCol" => 12]);
        $label = $this->getDictionaryWord("LabelWebSite");
        $name = "authWebCodeTarget";
        $webModal->appendWO($webModal->lastCol, "k_usefulobjects", "web\objects\w_combowebsites"
            , [
                "labelInput" => $label
                , "id" => $name
                , "name" => $name
                , "noEmptyOption" => "1"
                , "setDefaultValue" => "1"
                , "value" => ""

            ]
        );

        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

		//custom Field SELECT
		$label = $this->dictionary["LabelCustomFieldCode"];
		$name = "customSelectCode";
		$webModal->appendColumn($webModal->lastRow, ["id"=>"cstmFSelectCol", "class" => "hidden", "nCol" => 12]);
		$webModal->appendInput($webModal->lastCol,
			[
				"type" => "select"
				,"labelInput" => $label
				,"id" => $name
				,"name" => $name
				,"value" => ""
			]
		);


		//NEW ROW
		$webModal->appendRow($webModal->lastForm);

		//button Save
		$label = $this->dictionary["LabelSave"];
		$name = "btnSave";
		$webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
		$webModal->appendInput($webModal->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);



		//GET html Modal
		$htmlResult = $webModal->getHTMLWithCSSJS();

		//return
		return ["success" => "true", "html" => $htmlResult];
	
	}

	protected function delete($ArrayVariable){
		$keyID = $this->K_COMMON->getVarArray($ArrayVariable,"keyID");
		$table = $this->K_COMMON->getPluginDataObject("k_secauth","permission");

        $table->get(["*"], ["id_permission" => $keyID]);
        $data = $table->fetch();
        if ($data)
        {
            $anaCode = $data->employee_person_code;
            $webCode = $data->web_code;
            $authLvlCode = $data->level_type_code;

            $table->delete(["id_permission"=>$keyID]);

            return ["success"=>"true", "anaCode" => $anaCode, "webCode" => $webCode, "authLvlCode" => $authLvlCode];
        }
        else
            return ["success" => "false"];


	}

	protected function saveData($ArrayVariable){

	    //set variables
		$auth = $this->K_COMMON->getVarArray($ArrayVariable,"selectAuth");
		$customSelectCode = $this->K_COMMON->getVarArray($ArrayVariable,"customSelectCode");
        $authWebCodeTarget = $this->K_COMMON->getVarArray($ArrayVariable,"authWebCodeTarget");
		$anaCode = $this->K_COMMON->getVarArray($ArrayVariable,"anaCode");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable,"webCode");
		$authLvlCode = $this->K_COMMON->getVarArray($ArrayVariable,"authLvlCode");
        $valueCustomField = "";
        $codeTabRef = "";
        $authByWebCodeTarget = "";
        $authWebCodeTargetValue = "";
        $plugin = "";
        $authCode = "";
        if ($auth != "")
        {
            $r = $this->K_COMMON->getAuthCodeData($auth);
            $plugin = $r["pluginName"];
            $authCode = $r["code"];



        }

        //check for web code and ana code
        if ($webCode == "" && ($anaCode == "" || $authLvlCode ==""))
            return ["success" => "false", "reason" => "errorSecurity", "message" => "Error Security"];


        //get Auth Type
        $arrayWhere = [];
        $arrayWhere["plugin_name"] = $plugin;
        $arrayWhere["code_form"] = $authCode;

        //include Tables
        $tableAuth = $this->K_COMMON->getPluginDataObject("k_secauth", "authtype");
        $tablePermission = $this->K_COMMON->getPluginDataObject("k_secauth","permission");

        //get authorization
        $tableAuth->get(
            [
                "code_form"
                ,"code_label"
                ,"code_table_ref"
                ,"auth_by_web_target"
            ],$arrayWhere);

        if ($dataAuth = $tableAuth->fetch()) {
            $codeTabRef = $dataAuth->code_table_ref;
            $authByWebCodeTarget = $dataAuth->auth_by_web_target;
        }

		if($codeTabRef != "")
			$valueCustomField = $customSelectCode;

        if ($authByWebCodeTarget == "1")
            $authWebCodeTargetValue = $authWebCodeTarget;

		if($anaCode != "")
        {
            $tablePermission->get("id_permission",
                [
                    "web_code" => $webCode,
                    "employee_person_code"=>$anaCode,
                    "plugin_name"=>$plugin,
                    "code_form"=>$authCode,
                    "custom_field_code_value"=>$valueCustomField,
                    "web_code_target" => $authWebCodeTargetValue
                ]
            );

            $data = $tablePermission->fetch();
            if ($data)
                return ["success" => "false", "reason" => "exist", "message" => $this->getDictionaryWord("msgAuthExit")];
            else
                $tablePermission->put(
                    [
                        "web_code" => $webCode,
                        "employee_person_code"=>$anaCode,
                        "plugin_name"=>$plugin,
                        "code_form"=>$authCode,
                        "custom_field_code_value"=>$valueCustomField,
                        "web_code_target" => $authWebCodeTargetValue
                    ]
                );
        }
		elseif($authLvlCode != "")
        {
            $tablePermission->get("id_permission",
                [
                    "web_code" => $webCode,
                    "level_type_code"=>$authLvlCode,
                    "plugin_name"=>$plugin,
                    "code_form"=>$authCode,
                    "custom_field_code_value"=>$valueCustomField,
                    "web_code_target" => $authWebCodeTargetValue
                ]
            );

            $data = $tablePermission->fetch();
            if ($data)
                return ["success" => "false", "reason" => "exist", "message" => $this->getDictionaryWord("msgAuthExit")];
            else
                $tablePermission->put(
                    [
                        "web_code" => $webCode,
                        "level_type_code"=>$authLvlCode,
                        "plugin_name"=>$plugin,
                        "code_form"=>$authCode,
                        "custom_field_code_value"=>$valueCustomField,
                        "web_code_target" => $authWebCodeTargetValue
                    ]
                );
        }

	    return ["success"=>"true","message"=>$this->dictionary["msgSuccessSave"]];
	}



	protected function getAuthCustomField($ArrayVariable)
    {
        $pluginRef = $this->K_COMMON->getVarArray($ArrayVariable,"pluginRef");
        $codeRef = $this->K_COMMON->getVarArray($ArrayVariable,"codeRef");
        $OM = $this->K_COMMON->getVarArray($ArrayVariable,"OM");
        $obj = $this->K_COMMON->getPluginObject($pluginRef,"api");
        return  $obj->getAuthCustomField(["codeRef"=>$codeRef,"code"=>"","OM"=>$OM]);

    }

}
?>