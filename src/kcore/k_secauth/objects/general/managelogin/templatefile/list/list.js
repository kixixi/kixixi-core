// create namespace
KXJS.namespace("k_secauth.objects.login");
// create method of plugin
(function() {
	var t = this;

	t.updateBox = function(r) {
		if (r.success == "false")
			return;

		$.ajax({
			url: KXJS.buildAPICallUrl("KXCJS","k_secauth","managelogin","refreshBox"),
			data: {
				anaCode:r.anaCode,
				webCode:r.webCode

			}

		}).done(function(data) {
			var r = $.parseJSON(data);
			$("#loginboxglobal").replaceWith(r.html);
		});
	};
	t.addLogin  = function(){
		var anaCode = $(this).attr("anaCode");
		var webCode = $(this).attr("webCode");
		var dialogModal = waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'warning'});

		$.ajax({
			url: KXJS.buildAPICallUrl("KXCJS","k_secauth","managelogin","addLogin"),
			data: { 
				idModal:"modalAddLogin",
				anaCode:anaCode,
				webCode:webCode
				
				}

		}).done(function(data) {
			var r = $.parseJSON(data);
			dialogModal.on('hidden.bs.modal'
				, function () {
					if(r.success == "true"){

						$(r.html).appendTo('body');
						$('#modalAddLogin').modal("show");
						$('#modalAddLogin').on('hidden.bs.modal', function (e) {
							$('#modalAddLogin').remove();


						});

					}
					else
						alert("Error Security");
				});

			waitingDialog.hide();

		});
	};

	t.removeLogin  = function(){
		var anaCode = $(this).attr("anaCode");
		var webCode = $(this).attr("webCode");
		var dialogModal = waitingDialog.show('Removing...', {dialogSize: 'sm', progressType: 'warning'});

		$.ajax({
			url: KXJS.buildAPICallUrl("KXCJS","k_secauth","managelogin","getModalDeleteLogin"),
			data: {
				idModal:"modalRemoveLogin",
				anaCode:anaCode,
				webCode:webCode

			}

		}).done(function(data) {
			var r = $.parseJSON(data);
			dialogModal.on('hidden.bs.modal'
				, function () {
					if(r.success == "true"){

						$(r.html).appendTo('body');
						$('#modalRemoveLogin').modal("show");
						$('#modalRemoveLogin').on('hidden.bs.modal', function (e) {
							$('#modalRemoveLogin').remove();


						});

					}
					else
						alert("Error Security");
				});

			waitingDialog.hide();

		});
	};

	t.ready = function() {
		$("#btnAddLogin").click(t.addLogin);
		$("#btnDeleteLogin").click(t.removeLogin);
		$("#btnEditLogin").click(t.addLogin);
	}

}).apply(KXNS.k_secauth.objects.login);

// document ready
$(document).ready(KXNS.k_secauth.objects.login.ready);
