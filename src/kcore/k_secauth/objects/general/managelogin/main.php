<?
/**
 * KIXIXI k_secauth (manage login)
 * Create a web object, with list of login of a user
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_secauth
 * @subpackage objects\managelogin
 * @since 1.00
 */
namespace k_secauth\objects\general\managelogin;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authManageSecurity";

    /**
     * anaCode of user to manage login
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $anaCode = "";

    /**
     * webCode of user to manage login
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $webCode = "";

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();

    }

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //switch request
        switch ($KCA) {
            case "saveData" :
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "delete" :
                echo json_encode($this->delete($ArrayVariable));
                break;
            case "getModalDeleteLogin" :
                echo json_encode($this->getModalDeleteLogin($ArrayVariable));
                break;
            case "addLogin":
                echo json_encode($this->getModalEdit($ArrayVariable));
                break;
            case "refreshBox":
                    $this->get($ArrayVariable);
                    $htmlResult = $this->getHTMLWithCSSJS();

                    return json_encode(["success" => "true", "html" => $htmlResult]);
                    break;

        }
    }

    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "")
     *    *    anaCode: user code to get authorization
     *    *    authLvlCode: auth code to get authorization
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {

        //set variables
        $this->anaCode = $this->K_COMMON->getVarArray($ArrayVariable,"anaCode");
        $this->webCode = $this->K_COMMON->getVarArray($ArrayVariable,"webCode");
        $anaCode = $this->anaCode;
        $loginWebCodeParam = $this->getParamValue("loginWebCode", $this->webCode, "k_secauth");




        //check security
        if ($anaCode == "")
            return $this;

        //load data
        $table = $this->K_COMMON->getPluginDataObject("k_secauth", "login");
        $table->get(["lgn_user_code", "lgn_web_code", "lgn_nickname", "lgn_level_code"], ["lgn_web_code" => $this->webCode, "lgn_user_code" => $anaCode]);

        //create GENERAL ROW
        $this->appendRow("", ["id" => "loginboxglobal"]);
        $this->appendColumn($this->lastRow, ["id" => "loginboxglobalcol", "nCol" => 12, "offset" => "0"]);

        if ($loginWebCodeParam != "" && $this->webCode != $loginWebCodeParam){

            //NEW ROW
            $this->appendRow("loginboxglobalcol", ["margin" => "10px 0 20px 0"]);

            //description
            $desc = $this->getDictionaryWord("msgManageLoginFromCorrectWS");
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendHTML($this->lastCol, $desc);

            return $this;
        }

        //Check if login exist
        if ($row = $table->fetch()) {
            $value_login = $row->lgn_nickname;
            $value_LevelTypCode = $row->lgn_level_code;


            //NEW ROW
            $this->appendRow("loginboxglobalcol");
            $this->appendColumn($this->lastRow, ["id" => "loginbox", "nCol" => 3, "offset" => "0"]);

            //Login BOX
            $labelLogin = $this->dictionary["LabelLogin"];
            $labelLevel = $this->dictionary["LabelAuthLvl"];
            $box = $this->formatBoxLogin($labelLogin, $value_login, $labelLevel, $value_LevelTypCode, $this->webCode);
            $this->appendHTML($this->lastCol,$box);


            //NEW ROW
            $this->appendRow("loginboxglobalcol");

            //button Save
            $label = $this->dictionary["labelEditLogin"];
            $name = "btnEditLogin";
            $this->appendColumn($this->lastRow, [ "nCol" => 3]);
            $this->appendInput($this->lastCol,
                [
                    "type" => "btn",
                    "value" => $label,
                    "id" => $name,
                    "name" => $name,
                    "semanticColor" => "outline-success",
                    "attributes" => ["anaCode" => $this->anaCode, "webCode" => $this->webCode]
                ]
            );

            //button remove
            $label = $this->dictionary["labelDeleteLogin"];
            $name = "btnDeleteLogin";
            $this->appendColumn($this->lastRow, [ "nCol" => 3]);
            $this->appendInput($this->lastCol,
                [
                    "type" => "btn",
                    "value" => $label,
                    "id" => $name,
                    "name" => $name,
                    "semanticColor" => "outline-danger",
                    "attributes" => ["anaCode" => $this->anaCode, "webCode" => $this->webCode]
                ]
            );






        }
        else
        {

            
            //NEW ROW
            $this->appendRow("loginboxglobalcol", ["margin" => "10px 0 20px 0"]);

            //description
            $desc = $this->getDictionaryWord("msgCreateLogin");
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendHTML($this->lastCol, $desc);

            //NEW ROW
            $this->appendRow("loginboxglobalcol");

            //button Save
            $label = $this->dictionary["LabelAddLogin"];
            $name = "btnAddLogin";
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendInput($this->lastCol,
                [
                    "type" => "btn",
                    "value" => $label,
                    "id" => $name,
                    "name" => $name,
                    "semanticColor" => "outline-success",
                    "attributes" => ["anaCode" => $this->anaCode, "webCode" => $this->webCode]
                ]
            );

        }




        //add files to include
        $this->addHeadFile([$this->getPathFileJS("list")]);



        return $this;
    }

    protected function formatBoxLogin($labelLogin, $login, $labelLevel, $level, $webCode){

        //Include Modules
        $objAuthAPI = $this->K_COMMON->getPluginObject("k_secauth", "api");

        //set variables
        $loginDescription = $login;
        $levelDescription = $objAuthAPI->getAuthLevelName(["levelCode" => $level, "webCode" => $webCode]);
        if ($loginDescription == "")
            $loginDescription = $this->K_COMMON->getUserEmail($this->anaCode);

        return '
        <p class="mt-2"></p>
                <div class="alert alert-success" role="alert">
                  
                  <p><strong>'.$labelLogin.'</strong>: '.$loginDescription.'</p>
                  <p><strong>'.$labelLevel.'</strong>: '.$levelDescription.'</p>
                  
                </div>
        ';
    }


    protected function delete($ArrayVariable)
    {
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $anaCode = $this->K_COMMON->getVarArray($ArrayVariable, "anaCode");
        $table = $this->K_COMMON->getPluginDataObject("k_secauth", "login");
        $table->delete(["lgn_web_code" => $webCode, "lgn_user_code" => $anaCode]);

        return [
            "success" => "true",
            "anaCode" => $anaCode, //used from js to replace box
            "webCode" => $webCode //used from js to replace box
        ];
    }

    protected function saveData($ArrayVariable)
    {
        $LevelTypCode = $this->K_COMMON->getVarArray($ArrayVariable, "LevelTypCode");
        $login = $this->K_COMMON->getVarArray($ArrayVariable, "login");
        $password = $this->K_COMMON->getVarArray($ArrayVariable, "password");
        $confirmPassw = $this->K_COMMON->getVarArray($ArrayVariable, "confirmPassw");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $anaCode = $this->K_COMMON->getVarArray($ArrayVariable, "anaCode");

        //check for password
        if ($password != $confirmPassw)
            return ["success" => "false", "passwIsNotEqual", "message" => $this->dictionary["emsgPasswIsNotEqual"]];

        //check for web code and ana code
        if ($webCode == "" && $anaCode == "")
            return ["success" => "false", "reason" => "errorSecurity", "message" => "Error Security"];


        //set array values to be saved
        $arrayValues =
            [
                "lgn_web_code" => $webCode,
                "lgn_nickname" => $login,
                "lgn_user_code" => $anaCode,
                "lgn_level_code" => $LevelTypCode
            ];

        //only if password is not "" save it
        if ($confirmPassw != "")
            $arrayValues["lgn_pwd"] =sha1($password);


        $table = $this->getTable("login");
        $table->putByKey($arrayValues);


        return [
            "success" => "true",
            "message" => $this->dictionary["msgSuccessSave"],
            "anaCode" => $anaCode, //used from js to replace box
            "webCode" => $webCode //used from js to replace box
        ];
    }

    protected function getModalEdit($ArrayVariable)
    {


        //Get Variable
        $anaCode = $this->K_COMMON->getVarArray($ArrayVariable, "anaCode");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");
        $value_login = "";
        $value_LevelTypCode = "";

        //load data
        $table = $this->K_COMMON->getPluginDataObject("k_secauth", "login");

        $table->get(["lgn_user_code", "lgn_web_code", "lgn_nickname", "lgn_level_code"], ["lgn_web_code" => $webCode, "lgn_user_code" => $anaCode]);
        if ($row = $table->fetch()) {
            $anaCode = $row->lgn_user_code;
            $value_login = $row->lgn_nickname;
            $value_LevelTypCode = $row->lgn_level_code;
        }



        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");
        $webModal->newModal(["id" => $idModal, "title" => $this->dictionary["TitleLogin"], "type" => "normal"]);

        //create form
        $webModal->appendForm($webModal->modalBody,
            [
                "id" => "addLoginForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "closeModal" => $idModal
                , "functionPostSubmit" => "KXNS.k_secauth.objects.login.updateBox"
            ]
        );

        //keyID hidden
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "id" => "anaCode", "name" => "anaCode", "value" => $anaCode]);
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "id" => "webCode", "name" => "webCode", "value" => $webCode]);


        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //Login
        $label = $this->dictionary["LabelLogin"];
        $name = "login";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
        $webModal->appendInput($webModal->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_login]);

        //security level
        $label = $this->dictionary["LabelAuthLvl"];
        $name = "LevelTypCode";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
        $webModal->appendInput($webModal->lastCol,
            [
                "type" => "select"
                ,"labelInput" => $label
                ,"id" => $name
                ,"name" => $name
                ,"value" => $value_LevelTypCode
                ,"required" => "1"
                ,"typeLoad"=>"function"
                ,"pluginName"=>"k_secauth"
                ,"pluginObject"=>"api"
                ,"functionName"=>"getArrayAuthLvl"
            ]
        );

        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //password
        $label = $this->dictionary["LabelPassword"];
        $name = "password";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
        $webModal->appendInput($webModal->lastCol, ["type" => "password", "labelInput" => $label, "id" => $name, "name" => $name, "value" => ""]);

        //confirm password
        $label = $this->dictionary["LabelConfirmPassword"];
        $name = "confirmPassw";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
        $webModal->appendInput($webModal->lastCol, ["type" => "password", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "","attributes"=>" data-match=#password"]);

        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //button Save
        $label = $this->dictionary["LabelSave"];
        $name = "btnSave";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
        $webModal->appendInput($webModal->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);


        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];



    }

    /**
     * get modal delete KTemplate
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *      *   type: type of modal
     *      *   idModal: id to use on modal
     *
     * @return array
     */
    protected function getModalDeleteLogin($ArrayVariable)
    {

        //Get Variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");
        $webCode =  $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $anaCode =  $this->K_COMMON->getVarArray($ArrayVariable, "anaCode");

        $body = $this->getDictionaryWord("msgConfirmRemoveLogin");

        return $this->K_COMMON->getModalAskConfirm(
            [
                "idModal" => $idModal
                , "type" => "deleteWithCheck"
                , "title" => $this->getDictionaryWord("labelRemoveLogin")
                , "body" => $body
                , "buttons" => [
                        [
                            "title" => $this->getDictionaryWord("No")
                            , "value" => $this->getDictionaryWord("No")
                            , "name" => "btnNo"
                            , "onClick" => "general"
                            , "closeModalNow" => $idModal
                        ]
                        ,
                        [
                            "title" => $this->getDictionaryWord("Yes")
                            , "value" => $this->getDictionaryWord("Yes")
                            , "disableWithCheck" => "1"
                            , "name" => "btnYes"
                            , "semanticColor" => "danger"
                            , "staticAttributes" => ["anaCode" => $anaCode,"webCode"=>$webCode]
                            , "onClick" => "ajax"
                            , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                            , "functionPostSubmit" => "KXNS.k_secauth.objects.login.updateBox"
                            , "closeModal" => $idModal
                        ]
                ]
            ]);

    }
}

?>