<?
/**
 * KIXIXI k_secauth (password)
 * Manage Password
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_secauth
 * @subpackage objects\password
 * @since 1.00
 */
namespace k_secauth\objects\general\password;
class obj extends \k_webmanager\objects\general\general_object\obj
{
	/**
	 * Version of object
	 *
	 * @since 1.00
	 * @access public
	 * @var string
	 */
	var $ver = "1.0";

	/**
	 * Constructor
	 *
	 * @param array $arrayVariable array of variables
	 * The following directives can be used in the array variables:<br>
	 *    *    anaCode: user code to get addresses
	 */
	public function __construct($AV) {
		parent::__construct();
		$this->dictionary = $this->getDictionary();
		

	}

    /**
     * change password
     *
     * @param string oldPwd
     * @param string newPwd
     * @param string confPwd
     *
     * @return object this object
     */
	public function change($ArrayVariable){

		//get variables
		$oldPwd = $this->K_COMMON->getVarArray($ArrayVariable, "oldPwd");
		$newPwd = $this->K_COMMON->getVarArray($ArrayVariable, "newPwd");
		$confPwd = $this->K_COMMON->getVarArray($ArrayVariable, "confPwd");


		//set variable
        $loginWebCodeParam = $this->getParamValue("loginWebCode", "", "k_secauth");
        if ($loginWebCodeParam != "")
            $webMngUser = $loginWebCodeParam;
        else
            $webMngUser = $this->K_COMMON->getCurrentWeb();
		$anaCode = $this->K_COMMON->getCurrentUser();
		$table = $this->K_COMMON->getPluginDataObject("k_secauth", "login");

		//Check if match new Pwd
		if ($newPwd != $confPwd) {

			return array("success"=>"false","reason" => "pwdnotmatch", "message"=>$this->dictionary["msgErrorPwdNotMatch"]);

		}

		//check if user logged
		if ($this->K_SECAUTH->verifyUserLogged()) {

			//load user data
			$table->get(["lgn_pwd","id_login"],["lgn_user_code" => $anaCode, "lgn_web_code" => $webMngUser]);
			$row = $table->fetch();

			//Check if user exist
			if ($row) //Ha trovato l'utente
			{

				//Check if old pwd match
				if (sha1($oldPwd) == $row->lgn_pwd) {

					//Update Pwd

					$table->put(["lgn_pwd" => sha1($newPwd)],["id_login" => $row->id_login]);
					return array("success"=>"true","message"=>$this->dictionary["msgSuccessSave"]);

				} else {

					//Old Pwd not match

					return array("success"=>"false","reason"=>"oldpwd","message"=>$this->dictionary["msgErrorOldPwd"]);
				}


			} else {
				return array("success"=>"false","reason"=>"nologin","message"=>"Data Error!");
			}


		}
		else
			return array("success"=>"false","reason"=>"nologin");

	}
	
}
?>