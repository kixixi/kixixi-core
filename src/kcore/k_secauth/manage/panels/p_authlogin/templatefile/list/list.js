// create namespace
KXJS.namespace("k_secauth.objects.p_authlogin");
// create method of plugin
(function() {
	var t = this;
	t.searchReg = function(){
		var searchText = $("#searchField").val();
		location.href = KXJS.panelUrlActual + "list/"+searchText;
	}

	this.init = function(){
		$(document).ready(t.ready);
	};

	t.ready = function() {
		$("#btnSearch").click(t.searchReg)
	}

	t.init();


}).apply(KXNS.k_secauth.objects.p_authlogin);
