<?
/**
 * KIXIXI k_secauth (p_authlogin)
 * Create a panel web object for manage login authorization
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_secauth
 * @subpackage objects\p_authlogin
 * @since 1.00
 */
namespace k_secauth\manage\panels\p_authlogin;
class obj extends \k_webmanager\objects\general\base_panel\obj
{
	/**
	 * Version of object
	 *
	 * @since 1.00
	 * @access public
	 * @var string
	 */
	var $ver = "1.0";

	/**
	 * Constructor
	 *
	 */
	public function __construct() {
		parent::__construct();
		$this -> dictionary = $this->getDictionary();
		
	}

	/**
	 * get this object
	 *
	 * @param array $ArrayVariable array of variables
	 * The following directives can be used in the array variables:<br>
	 *    *    pluginMethod: method of plugin ("list", "put", "get")
	 *
	 * @return object this object
	 */
	public function get($ArrayVariable = array())
	{

		//set variables
		$pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

		switch ($pluginMethod) {
			case "list":
			case "":
				$this->getPageList($ArrayVariable);
				break;
			case "put":
			case "get":
				$this->getPagePut($ArrayVariable);
				break;

		}

		//Return
		return $this;
	}

	protected function getPageList($ArrayVariable = array())
	{

	    //Set Variables
        $editingWeb = $this->K_COMMON->getEditingWeb();

	    //Check if we can modify login from this page
        $loginWebCodeParam = $this->getParamValue("loginWebCode", $editingWeb, "k_secauth");

        if ($loginWebCodeParam != "" && $editingWeb != $loginWebCodeParam){

            //NEW ROW
            $this->appendRow("", ["margin" => "10px 0 20px 0"]);

            //description
            $desc = $this->getDictionaryWord("msgManageFromCorrectWS");
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendHTML($this->lastCol, $desc);

            return $this;
        }

		//add files to include
		$this->addHeadFile([$this->getPathFileJS("list")]);

		//set variables
		$pluginParam = $this->K_COMMON->getVarArray($ArrayVariable,"pluginParam");

		//NEW ROW
		$this->appendRow("");

		//Input Search
		$label = $this->dictionary["LabelName"];
		$name = "searchField";
		$this->appendColumn($this->lastRow, ["nCol" => 3]);
		$this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "", "required" => "1"]);

		//button Search
		$label = "&nbsp;";
		$name = "btnSearch";
		$this->appendColumn($this->lastRow, ["nCol" => 1]);
		$this->appendInput($this->lastCol, ["type" => "btn", "value" => '<i class="fa fa-search"></i>', "label" => $label, "id" => $name, "name" => $name, "semanticColor" => "primary"]);

		//NEW ROW
		$this->appendRow("");

		//get Table List
		$objTableList = $this->getTableList(["searchText"=>$pluginParam]);
		$this->appendColumn($this->lastRow, ["nCol" => 12]);
		$this->appendObject("", $objTableList);



	}

	protected function getPagePut($ArrayVariable = array())
	{

		//set variables
		$pluginParam = $this->K_COMMON->getVarArray($ArrayVariable,"pluginParam");
		$anaCode = $pluginParam;
		$value_name = "";
        $editingWeb = $this->K_COMMON->getEditingWeb();

		//get user data
		if ($anaCode != "")
		{
			//get ana_full_name
			$table = $this->K_COMMON->getPluginDataObject("k_registry","registry");
			$table->get(["ana_full_name"],["ana_code"=>$anaCode]);
			if($row=$table->fetch()){
				$value_name = $row->ana_full_name;
			}
		}

		//Add name on top
		$this->appendHTML("", "<H2>" . $value_name . "</H2>");

		//create Mandate tab
		$this->appendTab("",
			[
				"id" => "tabs"
				, "tabs" =>
				[
					[
						"label" => $this->dictionary["LabelTitleLogin"]
						, "active" => "1"
						, "id" => "tabLogin"
					]
					,
					[
						"label" => $this->dictionary["LabelTitleAuth"]
						, "active" => "0"
						, "id" => "tabAuth"
					]
				]
			]
		);

		//get Login table
		$objLogin = $this->K_COMMON->getPluginObject("k_secauth","managelogin");
		$objLogin->get(["anaCode" => $anaCode, "webCode" => $editingWeb]);
		$this->appendObject("tabLogin",$objLogin);

		//get authorization table
		$objAuth = $this->K_COMMON->getPluginObject("k_secauth","manageauthorization",array("anaCode"=>$anaCode));
		$objAuth->get(["anaCode" => $anaCode, "webCode" => $editingWeb]);
		$this->appendObject("tabAuth",$objAuth);

	}

	protected function getTableList($ArrayVariable = array()){



		//set variables
		$searchText = $this->K_COMMON->getVarArray($ArrayVariable,"searchText");
		$onlyRow = $this->K_COMMON->getVarArray($ArrayVariable,"onlyRow");
		$OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents","tablelist");
		$editingWeb = $this->K_COMMON->getEditingWeb();

		//load value
        $table = $this->K_COMMON->getPluginDataObject("k_secauth","login");

        if($searchText != ""){
			$table->getByQuery("login_with_name",
			    ["ana_code","ana_full_name"]
                ,["(ana_registered_name IS NULL OR ana_registered_name = '')"," ana_full_name LIKE :searchText", "lgn_web_code" => $editingWeb]
                ,["searchText"=>"%".$searchText."%"]);
		}
		else{
			$table->getByQuery("login_with_name","",["lgn_web_code"=>$editingWeb]);
		}

		$dataTable = $table->getResults("array");




		//create table
		$OBJTableList->newTable(
			[
				"id" => "tableList"
				, "tableID" => "tableList"
				, "hideHead" => $onlyRow
				, "data" => $dataTable
				, "fields" =>
				[
					[
						"label" => $this->dictionary["LabelCode"]
						, "qryField" => "ana_code"
					]
					,
					[
						"label" => $this->dictionary["LabelName"]
						, "qryField" => "ana_full_name"
					]
					,
					[
						"buttons" =>
							[
								[
									"title" => $this->dictionary["labelEdit"]
									, "action" => "edit"
									, "qryAttributes" => ["keyID" => "ana_code"]
								]
							]
					]
				]
			]
		);



		return $OBJTableList->get();



			
	
	}	
	
}
?>