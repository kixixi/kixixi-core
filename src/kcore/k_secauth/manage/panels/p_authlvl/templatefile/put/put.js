// create namespace
KXJS.namespace("k_secauth.objects.p_authLvl");
// create method of plugin
(function() {
	var t = this;
	t.submitForm = function(){
		waitingDialog.show("Saving...", {dialogSize: "sm", progressType: "warning"});
		$.post(KXJS.buildAPICallUrl("KXCJS","k_secauth","manage\\panels\\p_authlvl","saveData"), $("#addAuthLvlForm").serialize(),
			function(data) {
				var r = $.parseJSON(data);
				KXJS.showSavedMessageJSON(".savedMessage",r);
				if(r.success == "true")
					if(r.keyID != ""){
						location.href = KXJS.panelUrlActual + "get/"+r.keyID;
					}
				waitingDialog.hide();
			});
	};

	t.ready = function() {
		$("#addAuthLvlForm").validator().on("submit", function (e) {
			if (e.isDefaultPrevented()) {
			} else {
				e.preventDefault();
				t.submitForm();
			}
		});
		$("#btnNew").click(function(){
			location.href = KXJS.panelUrlActual + "put/";
	
		});
	}

}).apply(KXNS.k_secauth.objects.p_authLvl);

// document ready
$(document).ready(KXNS.k_secauth.objects.p_authLvl.ready);