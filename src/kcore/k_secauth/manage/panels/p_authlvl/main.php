<?
/**
 * KIXIXI k_secauth (p_authlvl)
 * Create a panel web object for manage level authorization
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_secauth
 * @subpackage objects\p_authlvl
 * @since 1.00
 */
namespace k_secauth\manage\panels\p_authlvl;
class obj extends \k_webmanager\objects\general\base_panel\obj
{
	/**
	 * Version of object
	 *
	 * @since 1.00
	 * @access public
	 * @var string
	 */
	var $ver = "1.0";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authManageSecurity";

	/**
	 * Constructor
	 *
	 */
	public function __construct($AV) {
		parent::__construct();
		$this -> dictionary = $this->getDictionary();
		
	}

	/**
	 * Manage Actions from ajax
	 *
	 * @param array $ArrayVariable array of variables
	 * The following directives can be used in the array variables:<br>
	 *    *    KCA: Action to execute
	 *
	 * @return string|json|array depend on ajax type call
	 *
	 */
	public function objectRequest($ArrayVariable) {

	    //set variable
		$KCA = $this->K_COMMON->getVarRequest("KCA");
		
		//switch request
		switch ($KCA) {
			case "saveData" :
				return json_encode($this->saveData($ArrayVariable));
			break;
			case "delete" :
				echo json_encode($this ->delete($ArrayVariable));
			break;
			case "add":
				echo json_encode($this->getModalEdit($ArrayVariable));
			break;
			case "getTableList":
				echo json_encode(["success"=>"true","html"=>$this->getTableList($ArrayVariable)]);
			break;
			
			
		}
	}

	/**
	 * get this object
	 *
	 * @param array $ArrayVariable array of variables
	 * The following directives can be used in the array variables:<br>
	 *    *    pluginMethod: method of plugin ("list", "put", "get")
	 *
	 * @return object this object
	 */
	public function get($ArrayVariable = array())
	{


		//set variables
		$pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");
		$this->urlActual = $this->K_COMMON->getVarArray($ArrayVariable, "panelUrlActual");

		switch ($pluginMethod) {
			case "list":
			case "":
				$this->getPageList();
				break;
			case "put":
			case "get":
				$this->getPagePut($ArrayVariable);
				break;

		}

		//Return
		return $this;
	}

	protected function getPageList()
	{

        //Set Variables
        $editingWeb = $this->K_COMMON->getEditingWeb();

        //Check if we can modify login from this page
        $loginWebCodeParam = $this->getParamValue("loginWebCode", $editingWeb, "k_secauth");

        if ($loginWebCodeParam != "" && $editingWeb != $loginWebCodeParam){

            //NEW ROW
            $this->appendRow("", ["margin" => "10px 0 20px 0"]);

            //description
            $desc = $this->getDictionaryWord("msgManageFromCorrectWS");
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendHTML($this->lastCol, $desc);

            return $this;
        }

		//NEW ROW
		$this->appendRow("");

		//get Table List
		$objTableList = $this->getTableList();
		$this->appendColumn($this->lastRow, ["nCol" => 12]);
		$this->appendObject("", $objTableList);

		//NEW ROW
		$this->appendRow($this->lastForm);

		//button
		$label = $this->dictionary["LabelAddAuthLvl"];
		$name = "btnAddNew";
		$this->appendColumn($this->lastRow, ["nCol" => 3]);
		$this->appendInput($this->lastCol,
            [
                "type" => "btnSubmit",
                "value" => $label,
                "id" => $name,
                "name" => $name,
                "semanticColor" => "outline-success",
                "link" => $this->urlActual . "put/".$editingWeb
            ]
        );


	}

	protected function getTableList($ArrayVariable = array()){

		//set variables
		$onlyRow = $this->K_COMMON->getVarArray($ArrayVariable,"onlyRow");
		$OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents","tablelist",["tableID"=>"tableList"]);
		$editingWeb = $this->K_COMMON->getEditingWeb();

		//load values
		$table = $this->getTable("authlevels");
		$table->get(["keyID" => "id_level_type","level_type_code","level_type_description"],["web_code"=>$editingWeb]);

		$dataTable = $table->getResults("array");

		//create table
		$OBJTableList->newTable(
			[
				"id" => "tableList"
				, "tableID" => "tableList"
				, "hideHead" => $onlyRow
				, "data" => $dataTable
				, "fields" =>
				[
					[
						"label" => $this->dictionary["LabelCode"]
						, "qryField" => "level_type_code"
					]
					,
					[
						"label" => $this->dictionary["LabelAuthLvl"]
						, "qryField" => "level_type_description"
					]
					,
					[
						"buttons" =>
							[
								[
									"title" => $this->dictionary["labelEdit"]
									, "action" => "edit"
									, "qryAttributes" => ["keyID" => "keyID"]
								]
								,
								[
									"title" => $this->dictionary["labelDelete"]
									, "action" => "remove"
									, "qryAttributes" => ["keyID" => "keyID"]
									, "ajax" => ["kcp" => $this->getPluginName(),"kco" => $this->getPluginObjectName(),"kca" => "delete"]
								]
							]
					]
				]
			]
		);

		return $OBJTableList->get();



	}

	protected function getPagePut($ArrayVariable)
	{

		//add files to include
		$this->addHeadFile([$this->getPathFileJS("put")]);

		//set variables
		$pluginParam = $this->K_COMMON->getVarArray($ArrayVariable, "pluginParam");
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");
		$value_code = "";
		$value_description = "";
		$code_disabled = "";
		$webCode = "";
		$keyID = "";
        $editingWeb = $this->K_COMMON->getEditingWeb();

		if ($pluginMethod == "put")
		    $webCode = $pluginParam;
		else
		    $keyID = $pluginParam;



		//load data
		if($keyID != ""){
			$code_disabled = " disabled='disabled'";
			$table = $this->getTable("authlevels");
			$table->get(["*"],["id_level_type"=>$keyID]);
			if($row = $table->fetch()){
				$value_code = $row->level_type_code;
				$value_description = $row->level_type_description;
				$webCode = $row->web_code;
			}
		}


		//create form
		$this->appendForm("",
			[
				"id" => "form"
				, "setSavedMessage" => "1"
				, "kcp" => $this->namePlugin
				, "kco" => $this->namePluginObject
				, "kca" => "saveData"
				, "reloadPage" => "1"
			]
		);

		//keyID hidden
		$this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "keyID", "value" => $keyID]);
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "webCode", "name" => "webCode", "value" => $webCode]);

		//NEW ROW
		$this->appendRow($this->lastForm);

		//code
		$label = $this->dictionary["LabelCode"];
		$name = "code";
		$this->appendColumn($this->lastRow, ["nCol" => 2]);
		$this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_code, "required" => "1", "attributes" => $code_disabled]);

		//description
		$label = $this->dictionary["LabelAuthLvl"];
		$name = "authLvl";
		$this->appendColumn($this->lastRow, ["nCol" => 10]);
		$this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_description, "required" => "1"]);


		//NEW ROW
		$this->appendRow($this->lastForm);

		//button Save
		$label = $this->dictionary["LabelSave"];
		$name = "btnSave";
		$this->appendColumn($this->lastRow, ["nCol" => 3]);
		$this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

		//button New
		$label = $this->dictionary["LabelNew"];
		$name = "btnNew";
		$this->appendInput($this->lastCol, ["type" => "btn", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "primary", "link" => $this->urlActual . "put"]);

		//NEW ROW
		$this->appendRow($this->lastForm);

		if ($keyID != "")
		{
			//list authorization
			$objLogin = $this->K_COMMON->getPluginObject("k_secauth","manageauthorization");
			$objLogin->get(["authLvlCode"=>$value_code, "webCode" => $editingWeb]);
			$this->appendColumn($this->lastRow, ["nCol" => 12]);
			$this->appendObject($this->lastCol, $objLogin);
		}


		
	}

	protected function delete($ArrayVariable){
		$keyID = $this->K_COMMON->getVarArray($ArrayVariable,"keyID");
		$table = $this->getTable("authlevels");
		$table->delete(["id_level_type"=>$keyID]);
		return array("success"=>"true");
	}

	protected function saveData($ArrayVariable){
		$keyID = $this->K_COMMON->getVarArray($ArrayVariable,"keyID");
		$code = $this->K_COMMON->getVarArray($ArrayVariable,"code");
		$authLvl = $this->K_COMMON->getVarArray($ArrayVariable,"authLvl");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable,"webCode");

		$table = $this->getTable("authlevels");
		if($code != ""){
			$table->get(["id_level_type"],["web_code" => $webCode, "level_type_code"=>$code]);
			if($row = $table->fetch()){
				if($row->id_level_type != $keyID)
					return ["success"=>"false","reason"=>"codeExist","message"=>$this->dictionary["emsgCodeAlreadyExist"]];
			}
		}
		elseif($keyID != ""){
			$table->get(["level_type_code"],["id_level_type"=>$keyID]);
			if($row = $table->fetch())
				$code = $row->level_type_code;
		}
		else
			return ["success"=>"false","reason"=>"errorSecurity","message"=>"Error Security"];
		
		$table->putByKey(["web_code" => $webCode, "level_type_code"=>$code,"level_type_description"=>$authLvl]);
		if($keyID == "")
			$keyID = $table->getNewID();
		return array("success"=>"true","message"=>$this->dictionary["msgSuccessSave"],"keyID"=>$keyID);
	}



	
	

	
}
?>