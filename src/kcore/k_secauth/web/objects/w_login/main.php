<?
/**
 * KIXIXI k_secauth (w_login)
 * Create web object for login
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_secauth
 * @subpackage web\objects\w_login
 * @since 1.00
 */
namespace k_secauth\web\objects\w_login;
class obj extends \k_webmanager\objects\general\web_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();

    }


    /**
     * get this object
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    nexturl: url after login
     *
     * @return object this object
     */
    public function get($arrayVariable = [])
    {

        //set variables
        $nextURL = $this->K_COMMON->getVarRequest("nexturl");
        if($nextURL == "")
            $nextURL = $this->K_COMMON->getVarArray($arrayVariable, "nexturl");
        $showFBLogin = $this->K_COMMON->getVarArray($arrayVariable, "showFBLogin");
        $showLogin = $this->K_COMMON->getVarArray($arrayVariable, "showLogin");
        $showPwdLost = $this->K_COMMON->getVarArray($arrayVariable, "showPwdLost");
        $showKeepSession = $this->K_COMMON->getVarArray($arrayVariable, "showKeepSession");
        $showSignup = $this->K_COMMON->getVarArray($arrayVariable, "showSignup");
        $labelLoginOldWay = $this->K_COMMON->getVarArray($arrayVariable, "labelLoginOldWay");


        //add files to include
        $this->addHeadFile([$this->getPathFileJS("login")]);
        $this->addHeadFile([$this->getPathFileCSS("login")]);
        $this->addExternalPlugin("facebook");

        //NEW ROW
        $this->appendRow("");

        //Append CARD
        $this->appendColumn($this->lastRow, ["nCol" => 12, "offset" => ""]);
        

        //Append Label Login
        $label = $this->dictionary["labelLoginWith"];
        $this->appendInput($this->lastCol, ["type" => "label", "value" => $label]);


        //create form
        $this->appendForm($this->lastCol,
            [
                "id" => "loginform"
                ,"setSavedMessage" => "1"
                , "functionSubmit" => "KXNS.k_secauth.objects.w_login.doLogin"
            ]
        );

        //hidden variables
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "nextURL", "name" => "nextURL", "value" => $nextURL]);

        if($showFBLogin == "1"){
            //NEW ROW
            $this->appendRow($this->lastForm);

            //button Login FB
            $label = $this->dictionary["labelLoginWithFB"];
            $name = "loginFB";
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendInput($this->lastCol, ["type" => "btn", "value" => $label, "id" => $name, "name" => $name, "class" => "btnWithFB", "blocked" => "1"]);
        }
        if($showFBLogin == "1" && $showLogin == "1"){
            //NEW ROW
            $this->appendRow($this->lastForm,["id" => "rowOldWayLabel"]);
            if($labelLoginOldWay!= "")
                $label = $labelLoginOldWay;
            else
                $label = $this->dictionary["labelLoginOldWay"];
            //Login with email label
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendHTML($this->lastCol,$label);
        }
        if($showLogin == "1"){

            //NEW ROW
            $this->appendRow($this->lastForm);

            //email
            $label = $this->dictionary["labelLoginEmail"];
            $name = "user";
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendInput($this->lastCol, ["type" => "text", "labelPlaceholder" => $label, "id" => $name, "name" => $name, "noLabel" => "1", "required" => "1"]);

            //NEW ROW
            $this->appendRow($this->lastForm);

            //password
            $label = $this->dictionary["labelLoginPwd"];
            $name = "pwd";
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendInput($this->lastCol, ["type" => "password", "labelPlaceholder" => $label, "id" => $name, "name" => $name, "noLabel" => "1", "required" => "1"]);
        }
        if($showKeepSession == "1"){

            //NEW ROW
            $this->appendRow($this->lastForm,["id" => "rowSessionNotExpire"]);

            //Session Expired
            $label = $this->dictionary["labelLoginSessionOpened"];
            $name = "sessionNotExpire";
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1"]);
        }
        //NEW ROW
        $this->appendRow($this->lastForm,["class" => "m-b-1"]);
        if($showLogin == "1") {

            //button Login
            $label = $this->dictionary["login"];
            $name = "loginBtn";
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "blocked" => "1", "semanticColor" => "success"]);
        }
        if($showSignup == "1"){
            //NEW ROW
            $this->appendRow($this->lastForm,["class" => "m-b-1"]);

            //button Login
            $label = $this->dictionary["signup"];
            $name = "signupBtn";
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendInput($this->lastCol, ["type" => "btn", "value" => $label, "id" => $name, "name" => $name, "blocked" => "1", "semanticColor" => "primary"]);
        }
        if($showPwdLost == "1"){
            //NEW ROW
            $this->appendRow($this->lastForm);

            //button Login
            $label = $this->dictionary["labelLoginRecoveryPwd"];
            $name = "pwdLost";
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendInput($this->lastCol, ["type" => "btn", "value" => $label, "id" => $name, "name" => $name, "blocked" => "1", "semanticColor" => "secondary"]);
        }
        return $this;
    }



    /**
     * get Parameterization Form
     *
     * @param array $objParameters array with variables
     *
     * @return webobject
     */
    protected function appendEditLoginFB($param = [],$newRow = true ,$nCol = 6){

        if($newRow){
            //NEW ROW
            $this->appendRow("");
        }
        //labelInput
        $label = "Show Login FB";
        $name = "showFBLogin";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value"=> "1","checked" => $value]);

    }
    protected function appendEditLogin($param = [],$newRow = true ,$nCol = 6){

        if($newRow){
            //NEW ROW
            $this->appendRow("");
        }
        //labelInput
        $label = "Show Login";
        $name = "showLogin";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value"=> "1","checked" => $value]);

    }
    protected function appendEditPwsLost($param = [],$newRow = true ,$nCol = 6){

        if($newRow){
            //NEW ROW
            $this->appendRow("");
        }
        //labelInput
        $label = "Show Password Lost";
        $name = "showPwdLost";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value"=> "1","checked" => $value]);

    }
    protected function appendEditSession($param = [],$newRow = true ,$nCol = 6){

        if($newRow){
            //NEW ROW
            $this->appendRow("");
        }
        //labelInput
        $label = "Show Keep Session";
        $name = "showKeepSession";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value"=> "1","checked" => $value]);

    }
    protected function appendEditSignup($param = [],$newRow = true ,$nCol = 6){

        if($newRow){
            //NEW ROW
            $this->appendRow("");
        }
        //labelInput
        $label = "Show Signup";
        $name = "showSignup";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value"=> "1","checked" => $value]);

    }
    protected function appendEditNextUrl($param = [],$newRow = true ,$nCol = 6){

        if($newRow){
            //NEW ROW
            $this->appendRow("");
        }
        //labelInput
        $label = "Url After Login";
        $name = "nexturl";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value"=> $value]);

    }
    protected function appendEditLabelOldWay($param = [],$newRow = true ,$nCol = 6){

        if($newRow){
            //NEW ROW
            $this->appendRow("");
        }
        //labelInput
        $label = "Label Login with email";
        $name = "labelLoginOldWay";
        $placeHolder = $this->dictionary["labelLoginOldWay"];
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text","labelPlaceholder"=>$placeHolder, "labelInput" => $label, "id" => $name, "name" => $name, "value"=> $value]);

    }

    public function parameterizationGetForm($objParameters)
    {
        $arrayFormField = $objParameters;
        $this->appendEditNextUrl($arrayFormField,true,12);
        $this->appendEditLabelOldWay($arrayFormField,true,12);

        $this->appendEditLoginFB($arrayFormField,true,3);
        $this->appendEditLogin($arrayFormField,false,3);
        $this->appendEditPwsLost($arrayFormField,false,3);
        $this->appendEditSession($arrayFormField,true,3);
        $this->appendEditSignup($arrayFormField,false,3);
        return $this;

    }

    /**
     * save Parameterization Data
     *
     * @param array $AV array with variables
     *
     * @return webobject
     */
    public function parameterizationSaveData($AV)
    {
        $showFBLogin = $this->K_COMMON->getVarArray($AV, "showFBLogin");
        $showLogin = $this->K_COMMON->getVarArray($AV, "showLogin");
        $showPwdLost = $this->K_COMMON->getVarArray($AV, "showPwdLost");
        $showKeepSession = $this->K_COMMON->getVarArray($AV, "showKeepSession");
        $showSignup = $this->K_COMMON->getVarArray($AV, "showSignup");
        $nexturl = $this->K_COMMON->getVarArray($AV, "nexturl");
        $labelLoginOldWay = $this->K_COMMON->getVarArray($AV, "labelLoginOldWay");


        return ["showFBLogin"=>$showFBLogin
                ,"showLogin"=>$showLogin
                ,"showPwdLost"=>$showPwdLost
                ,"showKeepSession"=>$showKeepSession
                ,"showSignup"=>$showSignup
                ,"nexturl"=>$nexturl
                ,"labelLoginOldWay"=>$labelLoginOldWay
        ];
    }

}

?>