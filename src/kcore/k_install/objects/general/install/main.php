<?
namespace k_install\objects\general\install;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    var $ver = "1.0";
    var $dictionary;


    private function installPhase1($ArrayVariable)
    {

        //set time limit connection
        set_time_limit(600);

        //set variables
        global $K_COMMON;
        global $kxDBManager;
        $dbHost = $this->K_COMMON->getVarArray($ArrayVariable, "dbHost");
        $dbPort = $this->K_COMMON->getVarArray($ArrayVariable, "dbPort");
        $dbName = $this->K_COMMON->getVarArray($ArrayVariable, "dbName");
        $dbUser = $this->K_COMMON->getVarArray($ArrayVariable, "dbUser");
        $dbPwd = $this->K_COMMON->getVarArray($ArrayVariable, "dbPwd");
        $contentLocation = $this->K_COMMON->getVarArray($ArrayVariable, "contentLocation");
        $AWSBucket = $this->K_COMMON->getVarArray($ArrayVariable, "AWSBucket");
        $AWSBucketAccessKey = $this->K_COMMON->getVarArray($ArrayVariable, "AWSBucketAccessKey");
        $AWSBucketSecretKey = $this->K_COMMON->getVarArray($ArrayVariable, "AWSBucketSecretKey");
        $AWSBucketURL = $this->K_COMMON->getVarArray($ArrayVariable, "AWSBucketURL");
        $AWSBucketBaseDir = $this->K_COMMON->getVarArray($ArrayVariable, "AWSBucketBaseDir");
        $host = $_SERVER['HTTP_HOST'];
        $userFirstName = $this->K_COMMON->getVarArray($ArrayVariable, "userFirstName");
        $userLastName = $this->K_COMMON->getVarArray($ArrayVariable, "userLastName");
        $userEmail = $this->K_COMMON->getVarArray($ArrayVariable, "userEmail");

        //check all fields
        if ($dbHost == "" || $dbPort == "" || $dbName == "" || $dbUser == "" || $dbPwd == "" || $contentLocation == "" || $userFirstName == "" || $userLastName == "" || $userEmail == "")
        {
            return ["success" => "false", "message" => "Fill in all Fields"];
        }

        //Try to connect DB
        $kxDBManager = new \k_dbmanager\objects\general\manager\obj($dbUser, $dbPwd, $dbName, $dbHost, $dbPort);
        $K_COMMON->kxdb = &$kxDBManager;
        $this->K_COMMON->kxdb = $kxDBManager;

        $dbCon = $kxDBManager->getDBConnection();

        if (!$kxDBManager->checkDBConnection())
        {
            printf ("Error: %s\n", $dbCon->error);
            return ["success" => "false", "reason" => "noDBConnection", "message" => "Error to connect to DB"];
        }

        //Get Tables WS
        $tableWS = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_sites");

        //content for file setting
        $settingContent = ''
            . '<?' . chr(13)
            . '//Database Constants' . chr(13)
            . 'define("KX_DBHOST","' . $dbHost . '");' . chr(13)
            . 'define("KX_DBPORT","' . $dbPort . '");' . chr(13)
            . 'define("DATABASE_USER","' . $dbUser . '");' . chr(13)
            . 'define("DATABASE_PWD","' . $dbPwd . '");' . chr(13)
            . 'define("DATABASE_NAME","' . $dbName . '");' . chr(13)
            . '' . chr(13)
            . '//Static Content Configuration' . chr(13)
            . 'define("staticContentLocation","' . $contentLocation . '"); //"s3" or "local"' . chr(13)
            . '' . chr(13)
            . '//Static Content Configuration AWS (staticContentLocation = s3)' . chr(13)
            . 'define("AWSBucket","' . $AWSBucket . '");' . chr(13)
            . 'define("AWSBucketAccessKey","' . $AWSBucketAccessKey . '");' . chr(13)
            . 'define("AWSBucketSecretKey","' . $AWSBucketSecretKey . '");' . chr(13)
            . 'define("AWSBucketURL","' . $AWSBucketURL . '"); //url of Bucket (without Base Dir)' . chr(13)
            . 'define("AWSBucketBaseDir","' . $AWSBucketBaseDir . '"); //base directory where kplugin can put files (AWS)' . chr(13)
            . 'define("AWSBucketWebsitesDir","' . $AWSBucketBaseDir . '/websites"); //base directory of kwebsites (AWS)' . chr(13)
            . '' . chr(13)
            . '//Static Content Configuration LOCAL (staticContentLocation = local)' . chr(13)
            . 'define("LOCALStaticBaseDir","kwebsites"); //base directory where kplugin can put files (local)' . chr(13)
            . 'define("LOCALStaticWebsitesDir","kwebsites"); //base directory of kwebsites (local)' . chr(13)
            . '?>' . chr(13);


        //create file setting
        $dir = $_SERVER['DOCUMENT_ROOT'] . "/ksettings";
        $filename = "settings.php";
        $arraySaveContent["dir"] = $dir;
        $arraySaveContent["filename"] = $filename;
        $arraySaveContent["content"] = $settingContent;
        $arraySaveContent["append"] = 0;
        $this->K_COMMON->saveFileContent($arraySaveContent);



        //Create temporary file for install
        $dir = $_SERVER['DOCUMENT_ROOT'] . "/ksettings";
        $filename = "installing.php";
        $arraySaveContent["dir"] = $dir;
        $arraySaveContent["filename"] = $filename;
        $arraySaveContent["content"] = 1;
        $arraySaveContent["append"] = 0;
        $this->K_COMMON->saveFileContent($arraySaveContent);

        //Create Object for install kcore plugins
        $objUpd = new \k_updmanager\objects\general\update\obj();


        //install all table
        $objUpd->updatePlugin(["typeUpdate" => "table", "getPluginsByFiles" => "1", "firstInstall" => "1", "pluginName" => "k_dictionary", "pluginObject" => "data\\tables\\dictionary_definition"]);
        $objUpd->updatePlugin(["typeUpdate" => "table", "getPluginsByFiles" => "1", "firstInstall" => "1", "pluginName" => "k_dictionary", "pluginObject" => "data\\tables\\dictionary"]);
        $objUpd->updatePlugin(["typeUpdate" => "table", "getPluginsByFiles" => "1", "firstInstall" => "1", "pluginName" => "k_dictionary"]);


        $objUpd->updatePlugin(["typeUpdate" => "table", "getPluginsByFiles" => "1", "firstInstall" => "1", "pluginName" => "k_usefuldata", "pluginObject" => "data\\tables\\country"]);
        $objUpd->updatePlugin(["typeUpdate" => "table", "getPluginsByFiles" => "1", "firstInstall" => "1", "pluginName" => "k_usefuldata", "pluginObject" => "data\\tables\\province"]);
        $objUpd->updatePlugin(["typeUpdate" => "table", "getPluginsByFiles" => "1", "firstInstall" => "1", "pluginName" => "k_usefuldata", "pluginObject" => "data\\tables\\city"]);
        $objUpd->updatePlugin(["typeUpdate" => "table", "getPluginsByFiles" => "1", "firstInstall" => "1", "pluginName" => "k_usefuldata"]);

        $objUpd->updatePlugin(["typeUpdate" => "table", "getPluginsByFiles" => "1", "firstInstall" => "1", "pluginName" => "k_registry", "pluginObject" => "data\\tables\\registry"]);
        $objUpd->updatePlugin(["typeUpdate" => "table", "getPluginsByFiles" => "1", "firstInstall" => "1", "pluginName" => "k_registry"]);


        $objUpd->updatePlugin(["typeUpdate" => "table", "getPluginsByFiles" => "1", "firstInstall" => "1"]);


        //install all trigger
        $objUpd->updatePlugin(["typeUpdate" => "trigger", "getPluginsByFiles" => "1", "webCode" => "1"]);

        //install all sp
        $objUpd->updatePlugin(["typeUpdate" => "sp", "getPluginsByFiles" => "1", "webCode" => "1"]);

        //Add Web Record
        $dateNow = date('Y-m-d H:i:s');
        $tableWS->put(["web_code" => "1", "web_name" => $host, "web_from_date" => $dateNow, "web_to_date" => "20991231"]);

        return ["success" => "true", "message" => "Phase 1 Completed"];
    }


    private function installPhase2($ArrayVariable)
    {

        //set variables
        $userFirstName = $this->K_COMMON->getVarArray($ArrayVariable, "userFirstName");
        $userLastName = $this->K_COMMON->getVarArray($ArrayVariable, "userLastName");
        $userEmail = $this->K_COMMON->getVarArray($ArrayVariable, "userEmail");


        //RESTART SESSION for SET Environment Variables and web code
        //$K_SESSIONS_START = new \k_sessions\objects\general\initiate\obj;
        //$K_SESSIONS_START->restart();

        //TODO: Valutare se lo start o il restart della sessione si può evitare o meno.... se serve capire se basta il restart.
        //TODO: Magari si potrebbe riavviare la pagina ? da decidere

        //Add User
        $objREG = $this->K_COMMON->getPluginObject("k_registry", "registration");
        $newPasswordUser = "Password1234!";
        $objREG->registration(
            [
                "firstName" => $userFirstName
                , "lastName" => $userLastName
                , "email" => $userEmail
                , "passw" => $newPasswordUser
                , "userLevel" => "1"

            ]
        );


        //create new instance of update object because we restarted session
        $objUpd = new \k_updmanager\objects\general\update\obj();


        $objUpd->updatePlugin(["typeUpdate" => "webtemplate", "getPluginsByFiles" => "1", "webCode" => "1"]);
        $objUpd->updatePlugin(["typeUpdate" => "webpage", "getPluginsByFiles" => "1", "webCode" => "1"]);

        //Update all kcore plugin
        $objUpd->updatePlugin(["pluginName" => "", "typeUpdate" => "", "getPluginsByFiles" => "1", "webCode" => "1"]);



        //Add Auth Level (Administrator and Web User)
        $tableLVL = $this->K_COMMON->getPluginDataObject("k_secauth", "authlevels");
        $tableLVL->put(["level_type_code" => "1", "level_type_description" => "Administrator","web_code" => "1"]);
        $tableLVL->put(["level_type_code" => "2", "level_type_description" => "Web User","web_code" => "1"]);

        //Add all auth at Administrator Level
        $tableAUTType = $this->K_COMMON->getPluginDataObject("k_secauth", "authtype");
        $tableAUTPerm = $this->K_COMMON->getPluginDataObject("k_secauth", "permission");
        $tableAUTType->get(["plugin_name","code_form","auth_by_web_target"],"","",["plugin_name","code_form"]);
        while ($row = $tableAUTType->fetch())
        {
            $authByWebCodeTarget = $row->auth_by_web_target;
            $values = [
                "level_type_code" => "1"
                ,"web_code" => "1"
                ,"plugin_name" => $row->plugin_name
                ,"code_form" => $row->code_form
            ];

            if ($authByWebCodeTarget == "1")
                $values["web_code_target"] = 1; //1 is the created web site
            $tableAUTPerm->put($values);
        }


        //Remove file Installing.com
        $dir = $_SERVER['DOCUMENT_ROOT'] . "/ksettings";
        unlink($dir . "/installing.php");

        return ["success" => "true", "passw" => $newPasswordUser, "message" => "Installation Completed"];
    }

    public function get($ArrayVariable = array())
    {


        //set variables
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }

        //Return
        return $this;
    }

    private function getPagePut($ArrayVariable)
    {
        //add files to include
        $this->addHeadFile([$this->getPathFileJS("put")]);

        //set variables
        $installStep = $this->K_COMMON->getVarArray($ArrayVariable, "installStep");
        $dbHost = "localhost";
        $dbPort = "3306";
        $dbName = "";
        $dbUser = "";
        $dbPwd = "";
        $contentLocation = "local";
        $AWSBucket = "";
        $AWSBucketAccessKey = "";
        $AWSBucketSecretKey = "";
        $AWSBucketURL = "";
        $AWSBucketBaseDir = "";
        $userFirstName = "";
        $userLastName = "";
        $userEmail = "";


//TODO: Da rimuovere
/*

        $dbHost = "db";
        $dbPort = "3306";
        $dbName = "db";
        $dbUser = "root";
        $dbPwd = "root";
        $contentLocation = "local";
        $userFirstName = "Marco";
        $userLastName = "Del Principe";
        $userEmail = "m.delprincipe73@gmail.com";
*/


        //NEW ROW (For center on screen)
        $this->appendRow("");
        $this->appendColumn($this->lastRow, ["nCol" => 4, "offset" => "4"]);

        //Title
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "KIXIXI INSTALL", "importance" => "1"]);



        //check Step
        switch ($installStep)
        {
            case "1":

                //check if exist DB and configuration
                if ($this->K_COMMON->getConstant("DATABASE_NAME") != "")
                {
                    $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "Configuration Exist, please remove before Install!", "importance" => "4", "semanticColor" => "danger"]);
                    return;
                }

                //get actual variables
                $dbHost = $this->K_COMMON->getVarArray($ArrayVariable, "dbHost");
                $dbPort = $this->K_COMMON->getVarArray($ArrayVariable, "dbPort");
                $dbName = $this->K_COMMON->getVarArray($ArrayVariable, "dbName");
                $dbUser = $this->K_COMMON->getVarArray($ArrayVariable, "dbUser");
                $dbPwd = $this->K_COMMON->getVarArray($ArrayVariable, "dbPwd");
                $contentLocation = $this->K_COMMON->getVarArray($ArrayVariable, "contentLocation");
                $AWSBucket = $this->K_COMMON->getVarArray($ArrayVariable, "AWSBucket");
                $AWSBucketAccessKey = $this->K_COMMON->getVarArray($ArrayVariable, "AWSBucketAccessKey");
                $AWSBucketSecretKey = $this->K_COMMON->getVarArray($ArrayVariable, "AWSBucketSecretKey");
                $AWSBucketURL = $this->K_COMMON->getVarArray($ArrayVariable, "AWSBucketURL");
                $AWSBucketBaseDir = $this->K_COMMON->getVarArray($ArrayVariable, "AWSBucketBaseDir");
                $userFirstName = $this->K_COMMON->getVarArray($ArrayVariable, "userFirstName");
                $userLastName = $this->K_COMMON->getVarArray($ArrayVariable, "userLastName");
                $userEmail = $this->K_COMMON->getVarArray($ArrayVariable, "userEmail");


                //make install
                $res = $this->installPhase1($ArrayVariable);
                $success = $res["success"];
                if ($success == "false") {
                    $message = $res["message"];
                    $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $message, "importance" => "4", "semanticColor" => "danger"]);


                } else {


                    //write success message
                    $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "Phase 1 Completed ...", "importance" => "4", "semanticColor" => "success"]);

                    //create form
                    $this->appendForm($this->lastCol,
                        [
                            "id" => "form"
                            , "formMethod" => "post"
                            , "action" => "install.php?"
                        ]
                    );

                    //hidden variables
                    $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "installStep", "name" => "installStep", "value" => "2"]);
                    $this->appendInput($this->lastForm, ["type" => "hidden", "name" => "userFirstName", "value" => $userFirstName]);
                    $this->appendInput($this->lastForm, ["type" => "hidden", "name" => "userLastName", "value" => $userLastName]);
                    $this->appendInput($this->lastForm, ["type" => "hidden", "name" => "userEmail", "value" => $userEmail]);


                    return;
                }

                break;


            case "2":

                //get actual variables
                $userEmail = $this->K_COMMON->getVarArray($ArrayVariable, "userEmail");

                //make install
                $res = $this->installPhase2($ArrayVariable);
                $success = $res["success"];
                if ($success == "false") {
                    $message = $res["message"];
                    $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $message, "importance" => "4", "semanticColor" => "danger"]);


                } else {

                    $userPwd = $res["passw"];


                    //write success message
                    $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "Environment Created", "importance" => "4", "semanticColor" => "success"]);

                    //User data
                    $this->appendHTML($this->lastCol, "Your Login Data:<br>");
                    $this->appendHTML($this->lastCol, "<strong>User:</strong> " . $userEmail . "<br>");
                    $this->appendHTML($this->lastCol, "<strong>Password:</strong> " . $userPwd . "<br>");
                    $this->appendHTML($this->lastCol, "<br><br>Welcome on KIXIXI World ;)<br><br>");

                    //button Go
                    $label = "Start to Use Now -->";
                    $name = "btnGo";
                    $this->appendRow($this->lastCol);
                    $this->appendColumn($this->lastRow);
                    $this->appendInput($this->lastCol, ["type" => "btn", "blocked" => "1", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

                    return;
                }


                return;
                break;



        }


        //create form
        $this->appendForm($this->lastCol,
            [
                "id" => "form"
                , "formMethod" => "post"
                , "action" => "install.php?"

            ]
        );

        //hidden variables
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "installStep", "name" => "installStep", "value" => "1"]);


        //DB Host
        $label = "DB Host";
        $name = "dbHost";
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $dbHost, "required" => "1"]);

        //DB Port
        $label = "DB Port";
        $name = "dbPort";
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $dbPort, "required" => "1"]);

        //DB Name
        $label = "DB Name";
        $name = "dbName";
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $dbName, "required" => "1"]);

        //DB User
        $label = "DB User";
        $name = "dbUser";
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $dbUser, "required" => "1"]);

        //DB Pwd
        $label = "DB Password";
        $name = "dbPwd";
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $dbPwd, "required" => "1"]);

        //Title AWS
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "AWS S3 Configuration", "importance" => "3"]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "Optional", "importance" => "5"]);

        //AWS Storage Type
        $label = "Static Content Location";
        $name = "contentLocation";
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "id" => $name
                , "name" => $name
                , "value" => $contentLocation
                , "required" => "0"
                , "typeLoad" => "array"
                , "options" => ["local" => "Local", "s3" => "S3"]
            ]
        );


        //AWSBucket
        $label = "AWS Bucket";
        $name = "AWSBucket";
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $AWSBucket, "required" => "0"]);

        //AWS Bucket Access key
        $label = "AWS Bucket Access Key";
        $name = "AWSBucketAccessKey";
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $AWSBucketAccessKey, "required" => "0"]);

        //AWS Bucket Secret key
        $label = "AWS Bucket Secret Key";
        $name = "AWSBucketSecretKey";
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $AWSBucketSecretKey, "required" => "0"]);

        //AWS Bucket URL
        $label = "AWS Bucket URL";
        $name = "AWSBucketURL";
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $AWSBucketURL, "required" => "0"]);

        //AWS Bucket Base Dir
        $label = "AWS Bucket Base Dir";
        $name = "AWSBucketBaseDir";
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $AWSBucketBaseDir, "required" => "0"]);


        //Title Admin User
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "Admin User Configuration", "importance" => "3"]);

        //User Name
        $label = "Name";
        $name = "userFirstName";
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $userFirstName, "required" => "1"]);

        //User Last Name
        $label = "Last Name";
        $name = "userLastName";
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $userLastName, "required" => "1"]);

        //User Email
        $label = "Email";
        $name = "userEmail";
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $userEmail, "required" => "1"]);


        //button Install
        $label = "Install";
        $name = "btnInstall";
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);


    }

}

?>