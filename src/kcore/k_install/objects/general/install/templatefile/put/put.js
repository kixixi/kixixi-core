// create namespace
KXJS.namespace("k_install.objects.install.put");

// create method of plugin
(function() {
    var t = this;
    this.init = function(){
        $(document).ready(t.ready);
    };
    t.submitForm = function(){

        $("#form").submit();
    };

    t.goManage = function(){

        location.href = "/manage";
    };

    t.installePhase2 = function(r) {

        $("#form").submit();
    };

    t.ready = function() {

        var step = $("#installStep").val();


        if (step == 2)
            t.installePhase2();

        $("#btnInstall").click(function () {
                t.submitForm();
            }
        );

        $("#btnGo").click(function () {
                t.goManage();
            }
        );

    }

    this.init();

}).apply(KXNS.k_install.objects.install.put);


