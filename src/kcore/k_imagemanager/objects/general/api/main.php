<?
/**
 * KIXIXI Images (api)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_htmlcomponents
 * @subpackage objects\api
 * @since 1.00
 */
namespace k_imagemanager\objects\general\api;
class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * get Image from BMP
     *
     * * @param array $filename file name
     *
     * @return image
     */
    function imageFromBMP($filename)
    {
        // version 1.00
        if (!($fh = fopen($filename, 'rb'))) {
            trigger_error('imageFromBMP: Can not open ' . $filename, E_USER_WARNING);
            return false;
        }
        // read file header
        $meta = unpack('vtype/Vfilesize/Vreserved/Voffset', fread($fh, 14));
        // check for bitmap
        if ($meta['type'] != 19778) {
            trigger_error('imageFromBMP: ' . $filename . ' is not a bitmap!', E_USER_WARNING);
            return false;
        }
        // read image header
        $meta += unpack('Vheadersize/Vwidth/Vheight/vplanes/vbits/Vcompression/Vimagesize/Vxres/Vyres/Vcolors/Vimportant', fread($fh, 40));
        // read additional 16bit header
        if ($meta['bits'] == 16) {
            $meta += unpack('VrMask/VgMask/VbMask', fread($fh, 12));
        }
        // set bytes and padding
        $meta['bytes'] = $meta['bits'] / 8;
        $meta['decal'] = 4 - (4 * (($meta['width'] * $meta['bytes'] / 4) - floor($meta['width'] * $meta['bytes'] / 4)));
        if ($meta['decal'] == 4) {
            $meta['decal'] = 0;
        }
        // obtain imagesize
        if ($meta['imagesize'] < 1) {
            $meta['imagesize'] = $meta['filesize'] - $meta['offset'];
            // in rare cases filesize is equal to offset so we need to read physical size
            if ($meta['imagesize'] < 1) {
                $meta['imagesize'] = @filesize($filename) - $meta['offset'];
                if ($meta['imagesize'] < 1) {
                    trigger_error('imageFromBMP: Can not obtain filesize of ' . $filename . '!', E_USER_WARNING);
                    return false;
                }
            }
        }
        // calculate colors
        $meta['colors'] = !$meta['colors'] ? pow(2, $meta['bits']) : $meta['colors'];
        // read color palette
        $palette = array();
        if ($meta['bits'] < 16) {
            $palette = unpack('l' . $meta['colors'], fread($fh, $meta['colors'] * 4));
            // in rare cases the color value is signed
            if ($palette[1] < 0) {
                foreach ($palette as $i => $color) {
                    $palette[$i] = $color + 16777216;
                }
            }
        }
        // create gd image
        $im = imagecreatetruecolor($meta['width'], $meta['height']);
        $data = fread($fh, $meta['imagesize']);
        $p = 0;
        $vide = chr(0);
        $y = $meta['height'] - 1;
        $error = 'imageFromBMP: ' . $filename . ' has not enough data!';
        // loop through the image data beginning with the lower left corner
        while ($y >= 0) {
            $x = 0;
            while ($x < $meta['width']) {
                switch ($meta['bits']) {
                    case 32:
                    case 24:
                        if (!($part = substr($data, $p, 3))) {
                            trigger_error($error, E_USER_WARNING);
                            return $im;
                        }
                        $color = unpack('V', $part . $vide);
                        break;
                    case 16:
                        if (!($part = substr($data, $p, 2))) {
                            trigger_error($error, E_USER_WARNING);
                            return $im;
                        }
                        $color = unpack('v', $part);
                        $color[1] = (($color[1] & 0xf800) >> 8) * 65536 + (($color[1] & 0x07e0) >> 3) * 256 + (($color[1] & 0x001f) << 3);
                        break;
                    case 8:
                        $color = unpack('n', $vide . substr($data, $p, 1));
                        $color[1] = $palette[$color[1] + 1];
                        break;
                    case 4:
                        $color = unpack('n', $vide . substr($data, floor($p), 1));
                        $color[1] = ($p * 2) % 2 == 0 ? $color[1] >> 4 : $color[1] & 0x0F;
                        $color[1] = $palette[$color[1] + 1];
                        break;
                    case 1:
                        $color = unpack('n', $vide . substr($data, floor($p), 1));
                        switch (($p * 8) % 8) {
                            case 0:
                                $color[1] = $color[1] >> 7;
                                break;
                            case 1:
                                $color[1] = ($color[1] & 0x40) >> 6;
                                break;
                            case 2:
                                $color[1] = ($color[1] & 0x20) >> 5;
                                break;
                            case 3:
                                $color[1] = ($color[1] & 0x10) >> 4;
                                break;
                            case 4:
                                $color[1] = ($color[1] & 0x8) >> 3;
                                break;
                            case 5:
                                $color[1] = ($color[1] & 0x4) >> 2;
                                break;
                            case 6:
                                $color[1] = ($color[1] & 0x2) >> 1;
                                break;
                            case 7:
                                $color[1] = ($color[1] & 0x1);
                                break;
                        }
                        $color[1] = $palette[$color[1] + 1];
                        break;
                    default:
                        trigger_error('imageFromBMP: ' . $filename . ' has ' . $meta['bits'] . ' bits and this is not supported!', E_USER_WARNING);
                        return false;
                }
                imagesetpixel($im, $x, $y, $color[1]);
                $x++;
                $p += $meta['bytes'];
            }
            $y--;
            $p += $meta['decal'];
        }
        fclose($fh);
        return $im;
    }

    /**
     * Resize Image
     *
     * @since 1.00
     * @access public
     *
     * * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *
     * *    filename    : file of image to resize
     * *    newfilename    : file of image resized (if empty same of original)
     * *    width   : pixel or percentage
     * *    height  : pixel or percentage
     * *    checkWH : if true check if initial image width > $width (same for height) otherwise not resize it
     * *    compression: compression (default 75)
     * *    chmod: chmod file
     *
     * @return array Ex: ["success" => true, "width" => "NEW WIDTH", "height" => "NEW HEIGHT"]
     */
    function imageResize($ArrayVariable)
    {
        //set variable
        $filename = $this->K_COMMON->getVarArray($ArrayVariable, "filename");
        $newfilename = $this->K_COMMON->getVarArray($ArrayVariable, "newfilename");
        $width = $this->K_COMMON->getVarArray($ArrayVariable, "width");
        $height = $this->K_COMMON->getVarArray($ArrayVariable, "height");
        $checkWH = $this->K_COMMON->getVarArray($ArrayVariable, "checkWH");
        $compression = $this->K_COMMON->getVarArray($ArrayVariable, "compression");
        $chmod = $this->K_COMMON->getVarArray($ArrayVariable, "chmod");
        if ($newfilename == "")
            $newfilename= $filename;
        if ($compression == "")
            $compression = "75";

        //Example
        //$width: if in % => scale else set this width
        //$height:  if in % => scale else set this height
        //$checkWH: if is set to true, verify if imageGetWidth > $width and same for height


        //Load Image
        $image_info = getimagesize($filename);
        $image_type = $image_info[2];
        if ($image_type == IMAGETYPE_JPEG) $image = imagecreatefromjpeg($filename);
        elseif ($image_type == IMAGETYPE_GIF) $image = imagecreatefromgif($filename);
        elseif ($image_type == IMAGETYPE_PNG) $image = imagecreatefrompng($filename);
        elseif ($image_type == IMAGETYPE_BMP) $image = $this->imageFromBMP($filename);
        else return array("success" => false, "reason" => "noImage");


        //Get Size
        $imageGetWidth = imagesx($image);
        $imageGetHeight = imagesy($image);



        //Resize
        if (($width != "") || ($height != "")) {
            if (strpos($width, "%") > 0) { // scale to %
                if ($height == "") $height = $width;
                $width = $imageGetWidth * $width / 100;
                $height = $imageGetHeight * $height / 100;
            } else {

                if (($width != "") && ($height == "")) { //resize By Width
                    if ($checkWH) {
                        if ($imageGetWidth > $width) {

                            if ($imageGetWidth > $imageGetHeight) {
                                $ratio = $width / $imageGetWidth;
                                $height = $imageGetHeight * $ratio;
                            } else {
                                $ratio = $width / $imageGetWidth;
                                $height = $imageGetHeight * $ratio;
                            }


                        } else {
                            $width = $imageGetWidth;
                            $height = $imageGetHeight;
                        }
                    } else {
                        $ratio = $width / $imageGetWidth;
                        $height = $imageGetHeight * $ratio;
                    }
                }

                if (($width == "") && ($height != "")) { //resize By Height
                    if ($checkWH) {
                        if ($imageGetHeight > $height) {


                            if ($imageGetHeight > $imageGetWidth) {
                                $ratio = $height / $imageGetHeight;
                                $width = $imageGetWidth * $ratio;
                            } else {
                                $ratio = $height / $imageGetHeight;
                                $width = $imageGetWidth * $ratio;
                            }


                        } else {
                            $width = $imageGetWidth;
                            $height = $imageGetHeight;
                        }
                    } else {
                        $ratio = $height / $imageGetHeight;
                        $width = $imageGetWidth * $ratio;
                    }

                }


            }

            //Create New image with new size
            $newImg = imagecreatetruecolor($width, $height);
            if (($image_type == IMAGETYPE_GIF || $image_type == IMAGETYPE_PNG)) {
                imagealphablending($newImg, false);
                imagesavealpha($newImg, true);
                $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
                imagefilledrectangle($newImg, 0, 0, $width, $height, $transparent);
            }
            imagecopyresampled($newImg, $image, 0, 0, 0, 0, $width, $height, $imageGetWidth, $imageGetHeight);
            $image = $newImg;
        }


        //Save image
        if ($newfilename == "") $newfilename = $filename;
        if ($image_type == IMAGETYPE_JPEG) imagejpeg($image, $newfilename, $compression);
        elseif ($image_type == IMAGETYPE_GIF) imagegif($image, $newfilename);
        elseif ($image_type == IMAGETYPE_PNG) imagepng($image, $newfilename);
        elseif ($image_type == IMAGETYPE_BMP) imagepng($image, $newfilename);

        if ($chmod != "")
            chmod($newfilename, $chmod);
        return array("success" => true, "width" => $width, "height" => $height);


    }

    /**
     * Image rotate
     *
     * @since 1.00
     * @access public
     *
     * * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *
     * *    filename    : file of image to rotate
     * *    newfilename    : new file name after rotation
     * *    rotang    : angle of rotation
     * *    chmod: optional chmod on file
     *
     * @return array
     */
    function imageRotate($ArrayVariable)
    {
        //set variable
        $filename = $this->K_COMMON->getVarArray($ArrayVariable, "filename");
        $newfilename = $this->K_COMMON->getVarArray($ArrayVariable, "newfilename");
        $rotang = $this->K_COMMON->getVarArray($ArrayVariable, "rotang");
        $chmod = $this->K_COMMON->getVarArray($ArrayVariable, "chmod");

        $image_info = getimagesize($filename);
        $image_type = $image_info[2];

        if ($image_type == IMAGETYPE_JPEG) $image = imagecreatefromjpeg($filename);
        elseif ($image_type == IMAGETYPE_GIF) $image = imagecreatefromgif($filename);
        elseif ($image_type == IMAGETYPE_PNG) $image = imagecreatefrompng($filename);
        elseif ($image_type == IMAGETYPE_BMP) $image = $this->imageFromBMP($filename);


        imagealphablending($image, false);
        imagesavealpha($image, true);

        $image = imagerotate($image, $rotang, imageColorAllocateAlpha($image, 0, 0, 0, 127));
        imagealphablending($image, false);
        imagesavealpha($image, true);

        if ($image_type == IMAGETYPE_JPEG) imagejpeg($image, $newfilename, 100);
        elseif ($image_type == IMAGETYPE_GIF) imagegif($image, $newfilename);
        elseif ($image_type == IMAGETYPE_PNG) imagepng($image, $newfilename);
        elseif ($image_type == IMAGETYPE_BMP) imagepng($image, $newfilename);


        if (($chmod != "null") && ($chmod != "")) chmod($newfilename, $chmod);

        return array("success" => "true");
    }



}

?>