<?
/**
 * KIXIXI BASE PANEL
 * This object is used as base of each panel object created
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_webmanager
 * @subpackage objects\web_object
 * @since 1.00
 */
namespace k_webmanager\objects\general\base_panel;
class obj extends \k_webmanager\objects\general\web_object_base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * Type of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $oType = "pt";

    /**
     * Get Object, similar to get but with checks
     *
     * @param array $av array to pass to the get of object
     * @param object $panel object panel to customize
     *
     * @return object.
     */
    public function getPanelObject($av = [])
    {

        $this->resetObject();
        $objRet = $this->get($av);

        return $objRet;
    }


}

?>