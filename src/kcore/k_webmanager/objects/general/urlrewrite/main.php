<?
/**
 * KIXIXI URL REWRITE
 * Manage URL Rewrite Data
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_webmanager
 * @subpackage objects\web_page
 * @since 1.00
 */
namespace k_webmanager\objects\general\urlrewrite;
class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";


    /**
     * get Url Rewrite Data
     *
     * @param strin $URLPath url to get (Ex: "/contacts")
     * @param strin $webCode webCode to get
     * @param boolean $asterisk if true search also page with asterisk if not found the URLPath
     *
     * Examples 1:<br>
     * - getByPath("/manage/k_webmanager",true);
     *   In this case, if not found this urls, search also "/manage/*"
     *
     * @return array
     */

    function getByPath($URLPath, $webCode, $asterisk = false)
    {
        //set variables
        $found = false;

        //get tables
        $tableURL = $this->getTable("urlrewrite");


        //check url
        $tableURL->get("*","(urw_url = '" . $URLPath . "' OR urw_url = '" . $URLPath . "/') AND urw_web_code = '".$webCode."'");
        $dataURL = $tableURL->fetch();

        //If not found, search for multi pages managed (EX: /manage/*)
        if (!$dataURL)
        {
            if ($URLPath != "")
            {
                if ($asterisk) //max 2 level on url
                {

                    $URLLevel = explode('/', $URLPath);

                    $where = [];
                    $where["urw_web_code"] = $webCode;
                    if (isset($URLLevel[2]))
                        $where[] = '(urw_url = "' . "/" . $URLLevel[1] . "/*" . '" OR urw_url = "' . "/" . $URLLevel[1] . "/" . $URLLevel[2] . "/*" . '")';
                    else
                        $where[] = '(urw_url = "' . "/" . $URLLevel[1] . "/*" . '")';

                    $tableURL->get("*", $where);
                    $dataURL = $tableURL->fetch();

                    if ($dataURL)
                        $found = true;

                }
            }



        }
        else
            $found = true;

        if ($found)
            return ["success" => "true", "pageID" => $dataURL->urw_page_id, "urlParam" => $dataURL->urw_url_parameters];
        else
            return ["success" => "false"];



    }

    /**
     * check if exist an URL
     *
     * @param array $AV array of parameters for check if exist url
     *
     *  Mandatory Parameters: <br>
     *
     *   *    pluginName    : plugin of which check url
     *   *    pluginObject  : plugin object of which check url
     *   *    refCode   : referrer code (example article code)
     *   *    webCode   : web code
     *   *    url   : url we check if exist (of a different plugin, object and refCode)

     *
     * @return boolean
     */
    function checkExistURL($AV)
    {

        //get variables
        $pluginName = $this->K_COMMON->getVarArray($AV, "pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($AV, "pluginObject");
        $refCode = $this->K_COMMON->getVarArray($AV, "refCode");
        $webCode = $this->K_COMMON->getVarArray($AV, "webCode");
        $url = $this->K_COMMON->getVarArray($AV, "url");


        //get tables
        $tableURL = $this->getTable("urlrewrite");

        //check variable
        if ($url == "")
            return false;
        if (substr($url, 0, 1) != "/")
            $url = "/" . $url;

        //load data
        $where = ["urw_web_code" => $webCode, "urw_url" => $url];
        $tableURL -> get("*", $where);
        $dataURl = $tableURL -> fetch();

        //check if exist
        if ($dataURl) {
            if ($dataURl->urw_plugin_name != $pluginName)
                return true;
            if ($pluginObject !="" && $dataURl->urw_plugin_object != $pluginObject)
                return true;
            if ($dataURl->urw_refcode != $refCode)
                return true;
        }

        return false;

    }

    /**
     * get the URL
     *
     * @param array $AV array of parameters
     *
     *  Mandatory Parameters: <br>
     *
     *   *    pluginName    : plugin which we want get url
     *   *    pluginObject  : plugin object which we want get url
     *   *    refCode   : referrer code which we want get url (example article code)
     *   *    webCode   : web code
     *
     * @return boolean
     */
    function getURL($AV)
    {

        //get tables
        $tableURL = $this->getTable("urlrewrite");

        //get variables
        $pluginName = $this->K_COMMON->getVarArray($AV, "pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($AV, "pluginObject");
        $webCode = $this->K_COMMON->getVarArray($AV, "webCode");
        $refCode = $this->K_COMMON->getVarArray($AV, "refCode");


        //load data
        $where = ["urw_web_code" => $webCode, "urw_plugin_name" => $pluginName, "urw_plugin_object" => $pluginObject, "urw_refcode" => $refCode];
        $tableURL -> get("*", $where);
        $dataURl = $tableURL -> fetch();

        //check data
        if ($dataURl)
            return $dataURl -> urw_url;
        else
            return "";


    }

    /**
     * remove URL
     *
     * @param array $AV array of parameters
     *
     *  Mandatory Parameters: <br>
     *
     *   *    pluginName    : plugin which we want remove url
     *   *    pluginObject  : plugin object which we want remove url
     *   *    refCode   : referrer code which we want remove url (example article code)
     *   *    webCode   : web code
     *
     * @return boolean
     */
    function removeURL($AV)
    {

        //get tables
        $tableURL = $this->getTable("urlrewrite");

        //get variables
        $pluginName = $this->K_COMMON->getVarArray($AV, "pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($AV, "pluginObject");
        $webCode = $this->K_COMMON->getVarArray($AV, "webCode");
        $refCode = $this->K_COMMON->getVarArray($AV, "refCode");


        //remove data
        $where = ["urw_web_code" => $webCode, "urw_plugin_name" => $pluginName, "urw_plugin_object" => $pluginObject, "urw_refcode" => $refCode];
        $tableURL -> delete($where);

        return true;


    }

    /**
     * remove URL (before check if exist)
     *
     * @param array $AV array of parameters
     *
     *  Mandatory Parameters: <br>
     *
     *   *    pluginName    : plugin which we want create url
     *   *    pluginObject  : plugin object which we want create url
     *   *    refCode   : referrer code which we want create url (example article code)
     *   *    webCode   : web code
     *   *    url  : url
     *   *    pageID   : id of page of url
     *   *    urlParam   : param to set on $_REQUEST when call this url
     *
     * @return boolean
     */
    function createURL($AV)
    {

        //get variables
        $pluginName = $this->K_COMMON->getVarArray($AV, "pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($AV, "pluginObject");
        $refCode = $this->K_COMMON->getVarArray($AV, "refCode");
        $webCode = $this->K_COMMON->getVarArray($AV, "webCode");
        $url = $this->K_COMMON->getVarArray($AV, "url");
        $pageID = $this->K_COMMON->getVarArray($AV, "pageID");
        $urlParam = $this->K_COMMON->getVarArray($AV, "urlParam");


        //get tables
        $tableURL = $this->getTable("urlrewrite");

        //check if already exist
        if ($this->checkExistURL($AV))
            return ["success" => "false", "reason" => "exist"];

        //load data (actual if exist)
        $where = ["urw_web_code" => $webCode, "urw_plugin_name" => $pluginName, "urw_plugin_object" => $pluginObject, "urw_refcode" => $refCode];
        $tableURL -> get("*", $where);
        $dataURl = $tableURL -> fetch();

        //check if exist
        if ($dataURl)
            $whereManage = ["id_web_url_rewrite" => $dataURl->id_web_url_rewrite];
        else
            $whereManage = [];

        //if $url !="" save it
        if ($url != "") {


            //save data
            $tableURL -> put(
                [
                    "urw_web_code" => $webCode
                    ,"urw_plugin_name" => $pluginName
                    ,"urw_plugin_object" => $pluginObject
                    ,"urw_refcode" => $refCode
                    ,"urw_page_id" => $pageID
                    ,"urw_url" => $url
                    ,"urw_url_parameters" => $urlParam
                ]
                ,$whereManage
            );



        }
        else //if $url == "" remove it
        {
            if (!empty($whereManage)) //only if find a record
                $tableURL -> delete($whereManage);
        }

        return ["success" => "true"];
    }

}
?>