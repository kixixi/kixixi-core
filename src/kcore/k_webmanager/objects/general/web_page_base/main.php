<?
/**
 * KIXIXI WEB PAGE
 * This object may be used as base for create web page
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_webmanager
 * @subpackage objects\web_page_base
 * @since 1.00
 */
namespace k_webmanager\objects\general\web_page_base;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * Layout of page
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $layout = "";

    /**
     * Construct
     *
     */
    function __construct()
    {
        parent::__construct();

        //load page layout base
        $this->layout = $this->getTemplateLayout("bootstrap_base","k_webmanager","web_page_base");

        //get template
        $this->addExternalPlugin("jquery");
        $this->addExternalPlugin("kjstart");
        $this->addExternalPlugin("bootstrap");




    }

    /**
     * get html of this page
     *
     */
    function getPage()
    {

        //convert CSS/JS to include
        $headFiles = $this->getHeadCSSJSConverted();

        //Assign HEAD JS AND CSS (before external plugin)
        $this->layout->assign_vars(["HEADCSS"=>$headFiles["CSS"]]);
        $this->layout->assign_vars(["FOOTERJS"=> $headFiles["JSFOOTER"]]);
        $this->layout->assign_vars(["HEADJS"=> $headFiles["JS"]]);
        $this->layout->assign_vars(["CONTENT"=>$this->getHTML()]);

        //Assign SEO META TAG
        $pageTitle = $this->K_COMMON->getVarArray($this->getSEOMetaTag(),"title");
        $pageMetaTag = "";
        foreach($this->getSEOMetaTag() as $key => $value)
        {
            if ($value != "")
                $pageMetaTag .= '<meta name="'.$key.'" content="'.$value.'">' .chr(13);
        }

        //Assign FB Meta Tag
        $pageMetaTag .= '<meta property="og:url" content="'.$this->K_COMMON->getCurrentURL().'" />';
        foreach($this->getFBMetaTag() as $key => $value)
        {
            if ($value != "")
                $pageMetaTag .= '<meta property="og:'.$key.'" content="'.$value.'">' .chr(13);
        }

        //Write TAG on Page
        $this->layout->assign_vars(["PAGETITLE"=>$pageTitle]);
        $this->layout->assign_vars(["PAGEHEADMETATAG"=>$pageMetaTag]);

        //parse template
        $html = $this->layout->pparse("body");

        return $html;

    }






}
?>