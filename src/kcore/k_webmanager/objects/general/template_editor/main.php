<?
/**
 * KIXIXI k_webmanager (template editor)
 * Manage Layout of template
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_webmanager
 * @subpackage objects\template_editor
 * @since 1.00
 */
namespace k_webmanager\objects\general\template_editor;

class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Constructor
     *
     */
    public function __construct($AV)
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
    }

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");
        $webCode = $this->K_COMMON->getEditingWeb();

        //Check Authorization
        if (!$this->isAuthorizedWebSiteLayout($webCode))
            return $this->K_COMMON->resultReturn("JSON", false, ["reason" => "errorSecurity", "message" => "Error Security"]);

        //get template
        switch ($KCA) {
            case "saveNewObject" :
                $webTemplate = $this->getObject("web_templates");
                return json_encode($webTemplate->saveNewObject($ArrayVariable));
                break;
            case "moveWebObject" :
                $webTemplate = $this->getObject("web_templates");
                return json_encode($webTemplate->moveWebObject($ArrayVariable));
                break;
            case "deleteObj" :
                $webTemplate = $this->getObject("web_templates");
                return json_encode($webTemplate->deleteObj($ArrayVariable));
                break;
            case "getModalEdit" :
                return json_encode($this->getModalEdit($ArrayVariable));
                break;
            case "getModalAdd" :
                return json_encode($this->getModalAdd($ArrayVariable));
                break;
            case "templateEditor":

                $webPage = $this->getPageOfTemplateToEdit($ArrayVariable);
                $html = $webPage->getPage();
                return $html;
                break;

        }
    }



    /**
     * get template to Edit
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    keyID: the plugin param (keyID)
     *
     * @return webObject
     */
    protected function getTemplateToEdit($ArrayVariable)
    {
        //include module
        $objApiWebManager = $this->K_COMMON->getPluginObject("k_webmanager", "api");

        // set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");

        //get name template
        if ($keyID != "") {
            //load web template
            $webTemplate = $this->getObject("web_templates");
            $webTemplate->get(["keyID" => $keyID, "useTagKWebObject" => true, "setModifyTemplate" => true]);
            $htmlWebTemplate = $webTemplate->getHtml();
        }

        //Append ID Template
        $name = "webEditorKeyID";
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $keyID]);

        //NEW ROW
        $this->appendRow("",["id" => "webEditorHead"]);

        //Button Add on Page
        $this->appendColumn($this->lastRow, ["nCol" => 2]);
        $label = $this->dictionary["labelBtnAddOnPage"];
        $this->appendInput($this->lastCol,
            [
                "type" => "btn"
                , "id" => "btnAddOnPage"
                , "class" => "fa fa-plus"
                , "semanticColor" => "success"
                , "value" => $label
                , "size" => "sm"
                , "attributes" => ["tplid" => $keyID]
            ]
        );

        //Button Preview
        $label = "Preview";
        $this->appendInput($this->lastCol,
            [
                "type" => "btn"
                , "id" => "btnPreview"
                , "class" => "fa fa-file-text-o"
                , "semanticColor" => "primary"
                , "value" => $label
                , "size" => "sm"
            ]
        );

        //Button Preview
        $label = "Edit";
        $this->appendInput($this->lastCol,
            [
                "type" => "btn"
                , "id" => "btnPageEdit"
                , "class" => "fa fa-edit hidden"
                , "semanticColor" => "warning"
                , "value" => $label
                , "size" => "sm"

            ]
        );

        $this->appendColumn($this->lastRow, ["nCol" => 3]);

        //get web site name
        $webName = $objApiWebManager->getWebName(["webCode" => $webTemplate->tplWebCode]);

        //Write info of template
        $this->appendHTML($this->lastCol,
            '<div class="webEditorWebName">'.$webName.'</div><div class="webEditorWebNameSep">-</div><div class="webEditorWebTplName"> '.$webTemplate->tplName.'</div>'

        );

        //include object
        $objTag = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tag");
        $objTag->get(["tag" => "div", "id" => "hiddenContainerBtnEdit", "class" => "collapse", "content" => ""]);
        $objTag->get(["tag" => "div", "id" => "hiddenContainerBtnAdd", "class" => "collapse", "content" => ""]);
        $objTag->get(["tag" => "div", "id" => "hiddenContainerBtnDelete", "class" => "collapse", "content" => ""]);
        $objTag->get(["tag" => "div", "id" => "hiddenContainerBtnMoveUp", "class" => "collapse", "content" => ""]);
        $objTag->get(["tag" => "div", "id" => "hiddenContainerBtnMoveDown", "class" => "collapse", "content" => ""]);

        $this->appendObject("", $objTag);
        $label = "";
        $this->appendInput("hiddenContainerBtnEdit",
            [
                "type" => "btn"
                , "id" => "btnEditID"
                , "class" => "btnWebObject btnEditWebObject fa fa-edit"
                , "semanticColor" => "warning"
                , "value" => $label
                , "size" => "sm"
            ]);
        $this->appendInput("hiddenContainerBtnAdd",
            [
                "type" => "btn"
                , "id" => "btnAddID"
                , "class" => "btnWebObject btnAddWebObject fa fa-plus"
                , "semanticColor" => "success"
                , "value" => $label
                , "size" => "sm"
            ]);
        $this->appendInput("hiddenContainerBtnDelete",
            [
                "type" => "btn"
                , "id" => "btnDeleteID"
                , "class" => "btnWebObject btnDeleteWebObject fa fa-remove"
                , "semanticColor" => "danger"
                , "value" => $label
                , "size" => "sm"]);
        $this->appendInput("hiddenContainerBtnMoveUp",
            [
                "type" => "btn"
                , "id" => "btnMoveUpID"
                , "class" => "btnWebObject btnMoveUpWebObject fa fa-arrow-up"
                , "semanticColor" => "info"
                , "value" => $label
                , "size" => "sm"
            ]);
        $this->appendInput("hiddenContainerBtnMoveDown",
            [
                "type" => "btn"
                , "id" => "btnMoveDownID"
                , "class" => "btnWebObject btnMoveDownWebObject fa fa-arrow-down"
                , "semanticColor" => "info"
                , "value" => $label
                , "size" => "sm"
            ]);

        //put html web template on page page
        $htmlWebTemplate = '<div class = "webEditorBigContainer">' . $htmlWebTemplate . '</div>';
        $this->appendHTML("", $htmlWebTemplate);

        $this->mergeObjectVariables($webTemplate);

        return $this;
    }

    /**
     * get page of template to Edit
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    keyID: the plugin param (keyID)
     *
     * @return webObject
     *
     */
    protected function getPageOfTemplateToEdit($ArrayVariable)
    {


        //create webPage
        $webPage = new \k_webmanager\objects\general\web_page_base\obj();

        //include external plugin
        $webPage->addExternalPlugin(["validator", "waitingfor"]);


        //add files to include
        $webPage->addHeadFile([$this->getPathFileJS("editor")]);
        $webPage->addHeadFile([$this->getPathFileCSS("editor")]);

        //get Template
        $obj = $this->getTemplateToEdit($ArrayVariable);

        //Append Template
        $webPage->appendObject("", $obj);

        return $webPage;
    }




    /**
     * get modal add new object on template
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *      *   tplID: ID of template where add new object
     *      *   tplCntID: tpl Content ID where append object
     *      *   idModal: id to use on modal
     *
     * @return array
     */
    protected function getModalAdd($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");
        $tplID = $this->K_COMMON->getVarArray($ArrayVariable, "tplID");
        $tplCntIDUP = $this->K_COMMON->getVarArray($ArrayVariable, "tplCntIDUP");

        //include
        $objCollapse = $this->K_COMMON->getPluginObject("k_htmlcomponents", "collapse");

        //load table of web objects
        $webObjectTable = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_objects");
        $webObjectTable->getByQuery("web_objects_with_plugin_name", ["plg_name", "id_web_objects", "wob_plugin_name", "wob_plugin_object", "wob_name", "wob_prototypes"], "", "", "", ["plg_type", "plg_name", "wob_name"]);
        $dataWO = $webObjectTable->getResults("array");

        //Create List of objects
        $collapseData = ["id" => "webEditorListObject", "asCard" => true];
        $wobPNamePrev = "";

        foreach ($dataWO as $key => $wObj) {
            $wobPluginName = $wObj["wob_plugin_name"];

            if ($wobPNamePrev != $wobPluginName) {

                $title = $wObj["plg_name"];
                $collapseData["items"][] = ["id" => "webEditorWO_" . $wobPluginName, "label" => ucfirst($title)];
            }

            $wobPNamePrev = $wobPluginName;


        }

        //create collapse
        $objCollapse->create($collapseData);

        //include modal object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        //create modal
        $webModal->newModal(["id" => $idModal, "title" => "", "type" => "normal"]);

        //addHeadFile
        $webModal->addHeadFile([$this->getPathFileJS("modaladd")]);

        //create form
        $webModal->appendForm($webModal->modalBody,
            [
                "id" => "webEditorAddObjectForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveNewObject"
                , "closeModal" => $idModal
                , "functionPostSubmit" => "KXNS.k_webmanager.objects.template_editor.editor.openModalEdit"
            ]
        );

        //hidden variables
        $name = "tplID";
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $tplID]);
        $name = "tplCntIDUP";
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $tplCntIDUP]);
        $name = "webObjPCode"; //web object prototype code
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => ""]);
        $name = "idWebObj"; //id of web object to add
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => ""]);

        //Append list of object
        $webModal->appendObject($webModal->lastForm, $objCollapse->getWO());

        //add Buttons of web object in the collapse menu
        $wobPluginNamePrev = "";
        foreach ($dataWO as $key => $wObj) {
            $wobPluginName = $wObj["wob_plugin_name"];
            $wobName = $wObj["wob_name"];
            $wobID = $wObj["id_web_objects"];
            $wobPrototypesJSON = $wObj["wob_prototypes"];
            if ($wobPrototypesJSON != "")
                $wobPrototypes = (array)json_decode($wobPrototypesJSON, true);
            else
                $wobPrototypes = [];

            if ($wobPluginName != $wobPluginNamePrev)
                $webModal->appendRow("webEditorWO_" . $wobPluginName . "_B", ["class" => "m-t-1"]);

            //Append Object button
            if (empty($wobPrototypes)) {

                $webModal->appendColumn($webModal->lastRow, ["nCol" => 4, "class" => "m-t-1"]);
                $webModal->appendInput($webModal->lastCol,
                    [
                        "type" => "btn"
                        , "value" => $wobName
                        , "id" => ""
                        , "name" => ""
                        , "class" => "webEditorButtonAddObject"
                        , "blocked" => "1"
                        , "semanticColor" => "success"
                        , "size" => "sm"
                        , "attributes" => ["data-idwebobj" => $wobID, "data-pcode" => ""]
                    ]
                );


            } else {

                foreach ($wobPrototypes as $pCode => $pArray) {


                    $webModal->appendColumn($webModal->lastRow, ["nCol" => 4, "class" => "m-t-1"]);
                    $webModal->appendInput($webModal->lastCol,
                        [
                            "type" => "btn"
                            , "value" => $wobName . " - " . $pArray["name"]
                            , "id" => ""
                            , "name" => ""
                            , "class" => "webEditorButtonAddObject"
                            , "blocked" => "1"
                            , "semanticColor" => "secondary"
                            , "size" => "sm"
                            , "attributes" => ["data-idwebobj" => $wobID, "data-pcode" => $pCode]
                        ]
                    );


                }
            }

            $wobPluginNamePrev = $wobPluginName;

        }


        //GET html Modal
        //$webModal->setExternalPlugin([]); //set to emty external plugin, for no conflit with page plugin
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];
    }




    /**
     * get modal for customize object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    tplID: ID of template
     *    *    tplCntID: ID of template content
     *    *    idModal: id to use on modal
     *
     * @return array
     */
    public function getModalEdit($ArrayVariable)
    {

        //set variable
        $tplID = $this->K_COMMON->getVarArray($ArrayVariable, "tplID");
        $tplCntID = $this->K_COMMON->getVarArray($ArrayVariable, "tplCntID");
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");
        $webCode = "";
        $woIsContainer = 0;
        $tplPluginName = "";
        $tplPluginObject = "";
        $objCustomForm = "";

        //include object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");
        $objTab = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tab");

        //include table
        $tableWebTplContent = $this->getTable("web_templates_content");

        //Load Table
        $tableWebObjects = $this->getTable( "web_objects");

        //load data
        $tableWebTplContent->get(
            [
                "tpo_web_code",
                "tpo_tpl_id_obj",
                "tpo_tpl_id_obj_up",
                "tpo_parameters",
                "tpo_data",
                "tpo_plugin_name",
                "tpo_plugin_object",

                "tpo_wo_plugin_name",
                "tpo_wo_plugin_object",
                "tpo_wo_parameters"
            ]
            ,
            ["id_web_template_content" => $tplCntID]
        );

        $rowObj = $tableWebTplContent->fetch();

        if ($rowObj) {
            //set variable
            $webObjectID = $rowObj->tpo_tpl_id_obj;
            $tplCntParameters = (array)json_decode($rowObj->tpo_parameters, true);
            $tplCntDataParameters = (array)json_decode($rowObj->tpo_data, true);
            $webCode = $rowObj->tpo_web_code;
            $tplPluginName = $rowObj->tpo_plugin_name;
            $tplPluginObject = $rowObj->tpo_plugin_object;

            $woPluginName = $rowObj->tpo_wo_plugin_name;
            $woPluginObject = $rowObj->tpo_wo_plugin_object;
            $woParameters = $rowObj->tpo_wo_parameters;

        }

        //Extract parameters
        if (is_array($tplCntParameters)) {
            $paddingT = $this->K_COMMON->getVarArray($tplCntParameters, "padding-top");
            $paddingL = $this->K_COMMON->getVarArray($tplCntParameters, "padding-left");
            $paddingR = $this->K_COMMON->getVarArray($tplCntParameters, "padding-right");
            $paddingB = $this->K_COMMON->getVarArray($tplCntParameters, "padding-bottom");
            $webObjectClass = $this->K_COMMON->getVarArray($tplCntParameters, "class");
            $webObjectStyle = $this->K_COMMON->getVarArray($tplCntParameters, "style");
            $webObjectBGColor = $this->K_COMMON->getVarArray($tplCntParameters, "background-color");
            $webObjectBGImage = $this->K_COMMON->getVarArray($tplCntParameters, "background-image");
            $webObjectTextColor = $this->K_COMMON->getVarArray($tplCntParameters, "color");
        }
        if (is_array($tplCntDataParameters)) {
            $dataLoad = $this->K_COMMON->getVarArray($tplCntDataParameters, "load");
            $dataGet = $this->K_COMMON->getVarArray($tplCntDataParameters, "get");
            $dataLoadPluginName = $this->K_COMMON->getVarArray($dataLoad, "pluginName");
            $dataLoadPluginData = $this->K_COMMON->getVarArray($dataLoad, "pluginData");
            $dataLoadFilterGET = $this->K_COMMON->getVarArray($dataLoad, "filterGET");
            $dataGetFieldName = $this->K_COMMON->getVarArray($dataGet, "fieldName");
        }

        //Load web object Data
        $tableWebObjects->get(["wob_container", "wob_type", "wob_name"],
            ["wob_plugin_name" => $woPluginName, "wob_plugin_object" => $woPluginObject]);
        $dataObject = $tableWebObjects->fetch();
        if ($dataObject)
            $woIsContainer = $dataObject->wob_container;


        //create modal
        $webModal->newModal(["id" => $idModal, "title" => "", "type" => "normal"]);
        //$parametrizationForm = $objectUsed->getParameterizationForm(["tplCntID" => $tplCntID]);

        $webModal->appendForm($webModal->modalBody,
            [
                "id" => "webObjectForm",
                "kcp" => $this->namePlugin,
                "kco" => $this->namePluginObject,
                "kca" => "saveNewObject",
                "setSavedMessage" => "1"
            ]
        );
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "name" => "tplID", "value" => $tplID]);
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "name" => "tplCntID", "value" => $tplCntID]);
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden",  "name" => "webCode", "value" => $webCode]);

        //create tab
        $webModal->appendTab($webModal->lastForm,
            [
                "id" => "tabsWebObjectForm"
                , "tabs" => []

            ]
        );

        //check if found object to customize
        $objCustomFormFind = false;
        if ($woPluginName != "") {

            $obj = $this->K_COMMON->getPluginObject($woPluginName, $woPluginObject);
            if ($obj->methodExist('parameterizationGetForm')) {

                //get customization Form
                $objExtraParameters = ["webCode" => $webCode];
                $objExtraParameters["tmplPluginName"] = $tplPluginName;
                $objExtraParameters["tmplPluginObject"] = $tplPluginObject;
                $objExtraParameters["tplCntID"] = $tplCntID;

                $objParametersSaved = (array)json_decode($woParameters,true);

                $objCustomForm = $obj->parameterizationGetForm($objParametersSaved, $objExtraParameters);

                $objCustomFormFind = true;


            }

        }


        //Add Tabs
        $webModal = $objTab->addTabToExistingTabs($webModal, "tabsWebObjectForm", "tabGeneral", $this->dictionary["labelTabGeneral"], "",1);
        if ($objCustomFormFind != "")
            $webModal = $objTab->addTabToExistingTabs($webModal, "tabsWebObjectForm", "tabObject", $this->dictionary["labelTabObject"], "");
        $webModal = $objTab->addTabToExistingTabs($webModal, "tabsWebObjectForm", "tabPosition", $this->dictionary["labelTabPosition"], "");
        $webModal = $objTab->addTabToExistingTabs($webModal, "tabsWebObjectForm", "tabLoadData", $this->dictionary["labelTabData"], "");
        $webModal = $objTab->addTabToExistingTabs($webModal, "tabsWebObjectForm", "tabDictionary", $this->dictionary["labelTabDictionary"], "");

        /********************/
        /* TAB OBJECT     */
        /********************/
        if ($objCustomFormFind != "")
            $webModal->appendObject("tabObject", $objCustomForm);

        /********************/
        /* TAB GENERAL     */
        /********************/

        //NEW ROW
        $webModal->appendRow("tabGeneral");

        //object id
        $label = $this->dictionary["LabelWebObjectID"];
        $name = "webObjectID";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
        $webModal->appendInput($webModal->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $webObjectID]);

        //NEW ROW
        $webModal->appendRow("tabGeneral");

        //object id
        $label = "class";
        $name = "webObjectClass";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
        $webModal->appendInput($webModal->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $webObjectClass]);

        //NEW ROW
        $webModal->appendRow("tabGeneral");

        //object id
        $label = "style";
        $name = "webObjectStyle";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
        $webModal->appendInput($webModal->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $webObjectStyle]);

        //NEW ROW
        $webModal->appendRow("tabGeneral");

        //append BG
        $label = "Back Ground Color";
        $name = "webObjectBGColor";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
        $webModal->appendInput($webModal->lastCol, ["type" => "colorPicker", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $webObjectBGColor]);

        // append Txt color
        $label = "Text Color";
        $name = "webObjectTextColor";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
        $webModal->appendInput($webModal->lastCol, ["type" => "colorPicker", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $webObjectTextColor]);

        //NEW ROW
        $webModal->appendRow("tabGeneral");

        //Background Image
        $label = "Background Image";
        $name = "webObjectBGImage";
        $textHelp = "";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
        $webModal->appendInput($webModal->lastCol, ["type" => "serverFile", "getFolderByPlugin" => $tplPluginName, "labelInput" => $label, "id" => $name, "name" => $name, "value" => $webObjectBGImage, "textHelp" => $textHelp, "webCode" => $webCode]);

        /********************/
        /* TAB LOAD DATA     */
        /********************/

        if ($woIsContainer == 1) {

            //Description
            $webModal->appendRow("tabLoadData");
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
            $webModal->appendWO($webModal->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => "Data to Load", "importance" => "5"]);
            $webModal->appendHTML($webModal->lastCol, "<small>" . "Select plugin data where to load the data. " . "</small>");

            //Plugin Name
            $label = "Plugin";
            $name = "dataPluginName";
            $value = $dataLoadPluginName;
            $webModal->appendRow("tabLoadData");
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
            $webModal->appendWO($webModal->lastCol, "k_usefulobjects", "web\objects\w_comboplugin", ["labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);


            //Plugin Data
            $label = "Plugin Data";
            $name = "dataPluginData";
            $value = $dataLoadPluginData;

            $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
            $webModal->appendWO($webModal->lastCol, "k_usefulobjects", "web\objects\w_comboplugindata", ["labelInput" => $label, "id" => $name, "name" => $name, "value" => $value, "loadTriggerRef" => "#dataPluginName"]);

            //DB Field for query using get
            $label = "Filter GET";
            $name = "dataLoadFilterGET";
            $value = $dataLoadFilterGET;
            $webModal->appendRow("tabLoadData");
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
            $webModal->appendInput($webModal->lastCol, ["type" => "text", "textHelp" => "Specify the DB Field to filter if get field is not empty", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);


        } else {
            //Field id
            $label = "Field Name";
            $name = "dataFieldName";
            $value = $dataGetFieldName;
            $webModal->appendRow("tabLoadData");
            $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
            $webModal->appendInput($webModal->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

        }

        /********************/
        /* TAB DICTIONARY   */
        /********************/

        //$webModal->appendRow("tabDictionary");
        $arrayParam = ["plgID" => $tplPluginName, "pluginObject" => $tplPluginObject, "webCode" => $webCode];
        $objDictionary = $this->K_COMMON->getPluginObject("k_pluginmanage", "tab_dictionary")->get($arrayParam);
        $webModal->appendObject("tabDictionary", $objDictionary);

        /********************/
        /* TAB POSITION     */
        /********************/

        //padding Top
        $webModal->appendRow("tabPosition");
        $webModal->appendColumn($webModal->lastRow, ["offset" => 2, "nCol" => 3]);
        $label = "T";
        $name = "paddingT";
        $webModal->appendInput($webModal->lastCol, ["type" => "spinner", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $paddingT]);

        //padding Left
        $webModal->appendRow("tabPosition");
        $webModal->appendColumn($webModal->lastRow, ["offset" => 0, "nCol" => 3]);
        $label = "L";
        $name = "paddingL";
        $webModal->appendInput($webModal->lastCol, ["type" => "spinner", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $paddingL]);

        //padding Right
        $webModal->appendColumn($webModal->lastRow, ["offset" => 2, "nCol" => 3]);
        $label = "R";
        $name = "paddingR";
        $webModal->appendInput($webModal->lastCol, ["type" => "spinner", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $paddingR]);

        //padding Bottom
        $webModal->appendRow("tabPosition");
        $webModal->appendColumn($webModal->lastRow, ["offset" => 2, "nCol" => 3]);
        $label = "B";
        $name = "paddingB";
        $webModal->appendInput($webModal->lastCol, ["type" => "spinner", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $paddingB]);



        //********************
        //*** ADD BTN SAVE ***
        //********************

        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //button
        $label = $this->dictionary["save"];
        $name = "btnSave";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 2]);
        $webModal->appendInput($webModal->lastCol, ["type" => "btnSubmit", "label" => "&nbsp;", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);


        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];
    }


    /**
     * get a prototype Form for parameterize an object
     * may be used from all object that may be parameterized and want a standard form
     *
     * @param array $arrayFormField array of variables
     * The following directives can be used in the array variables:<br>
     *    *     labelPlaceholder: label on placeholder of field
     *    *     descr: description for this field
     *    *     setLabel: 1 if user can change label
     *    *     setVisible: 1 if user can select if visible or not
     *    *     setRequired: 1 if the field is required
     *
     * @param array $arrayFormFieldSaved actual values of fields, stored on object used table
     *
     * @return webobject
     */
    public function prototypeFormGet($arrayFormField = [], $arrayFormFieldSaved = [])
    {


        //include module
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");
        $OBJInput = $this->K_COMMON->getPluginObject("k_htmlcomponents", "input");

        //merge fields parameters with parameters saved
        if (!empty($arrayFormFieldSaved))
        {
            $arrayFormField = array_replace_recursive($arrayFormField, $arrayFormFieldSaved["fields"]);
        }

        //set variable
        $onlyRow = "";

        //create tablelist head
        $OBJTableList->addColumnHead($this->dictionary["LabelLabel"], "", "", $onlyRow);
        $OBJTableList->addColumnHead($this->dictionary["LabelCustomLabel"], "", "", $onlyRow);
        $OBJTableList->addColumnHead($this->dictionary["LabelRequired"], "", "", $onlyRow);
        $OBJTableList->addColumnHead($this->dictionary["LabelVisible"], "", "", $onlyRow);

        foreach ($arrayFormField as $name => $field) {

            //label
            $OBJTableList->addRow();
            $OBJTableList->addColumn("");
            $OBJTableList->addValue($this->K_COMMON->getVarArray($field, "descr"), "", "", "col-md-10", "");

            //customize label column
            $OBJTableList->addColumn("");

            //custom label
            if ($this->K_COMMON->getVarArray($field, "setLabel") == "1") {
                $checkedCustomLabel = $this->K_COMMON->getVarArray($field, "customLabel");
                $checkboxCustomLabel = $OBJInput->getWO(
                    [
                        "type" => "checkbox"
                        , "class" => ""
                        , "id" => $name . "_customLabel"
                        , "name" => $name . "_customLabel"
                        , "size" => "sm"
                        , "checked" => $checkedCustomLabel
                        , "value" => "1"
                        , "attributes" => "onclick='if(!$(\"#" . $name . "_customLabel\").is(\":checked\")){ $(\"#" . $name . "_label\").attr(\"disabled\",\"disabled\");$(\"#" . $name . "_label\").val(\"\");} else $(\"#" . $name . "_label\").removeAttr(\"disabled\");'"
                        , "noFieldSet" => "1"
                        , "noContainer" => "1"
                    ]);

                $OBJTableList->addObject($checkboxCustomLabel, "col-md-1", "");


                $label = $this->K_COMMON->getVarArray($field, "label");
                if ($checkedCustomLabel == "")
                    $disabled = "disabled = 'disabled'";
                else
                    $disabled = "";
                $labelPlaceHolder = $this->K_COMMON->getVarArray($field, "labelPlaceholder");
                $inputLabel = $OBJInput->getWO(
                    [
                        "type" => "text"
                        , "class" => ""
                        , "id" => $name . "_label"
                        , "name" => $name . "_label"
                        , "size" => "sm"
                        , "value" => $label
                        , "noLabel" => 1
                        , "attributes" => $disabled
                        , "noFieldSet" => "1"
                        , "noContainer" => "1"
                        , "labelPlaceholder" => $labelPlaceHolder
                    ]
                );

                $OBJTableList->addObject($inputLabel, "col-md-11");
            }


            //required column
            $OBJTableList->addColumn("");
            if ($this->K_COMMON->getVarArray($field, "setRequired") == "1") {
                $checkedReq = $this->K_COMMON->getVarArray($field, "required");
                $checkboxReq = $OBJInput->getWO(
                    [
                        "type" => "checkbox"
                        , "class" => ""
                        , "name" => $name . "_required"
                        , "size" => "sm"
                        , "checked" => $checkedReq
                        , "value" => "1"
                        , "attributes" => ""
                        , "noFieldSet" => "1"
                        , "noContainer" => "1"
                    ]);

                $OBJTableList->addObject($checkboxReq, "col-md-12", "");
            }


            //visible column
            $OBJTableList->addColumn("");
            if ($this->K_COMMON->getVarArray($field, "setVisible") == "1") {
                $checkedVisible = $this->K_COMMON->getVarArray($field, "visible");
                $checkboxVisible = $OBJInput->getWO(
                    [
                        "type" => "checkbox"
                        , "class" => ""
                        , "name" => $name . "_visible"
                        , "size" => "sm"
                        , "checked" => $checkedVisible
                        , "value" => "1"
                        , "attributes" => ""
                        , "noFieldSet" => "1"
                        , "noContainer" => "1"
                    ]);

                $OBJTableList->addObject($checkboxVisible, "col-md-12", "");
            }

        }

        $objTable = $OBJTableList->get();


        return $objTable;
    }

    /**
     * prototype for get array for saving data of prototype Form
     * may be used from all object that may be parameterized and want a standard form
     *
     * @param array $arrayVariable fields form data
     *
     * @return webobject
     */
    public function prototypeFormSave($arrayVariable)
    {
        $parameters["fields"] = [];
        foreach ($arrayVariable as $key => $value) {
            if (strstr($key, "_label")) {
                $code = str_replace("_label", "", $key);
                if ($value != "") {
                    $parameters["fields"][$code]["code"] = $code;
                    $parameters["fields"][$code]["label"] = $value;
                }
            } elseif (strstr($key, "_required")) {
                $code = str_replace("_required", "", $key);
                if ($value != "") {
                    $parameters["fields"][$code]["code"] = $code;
                    $parameters["fields"][$code]["required"] = "1";
                }
            } elseif (strstr($key, "_visible")) {
                $code = str_replace("_visible", "", $key);
                if ($value != "") {
                    $parameters["fields"][$code]["code"] = $code;
                    $parameters["fields"][$code]["visible"] = "1";
                }
            } elseif (strstr($key, "_customLabel")) {
                $code = str_replace("_customLabel", "", $key);
                if ($value != "") {
                    $parameters["fields"][$code]["code"] = $code;
                    $parameters["fields"][$code]["customLabel"] = "1";
                }
            }

        }


        return $parameters;
    }

}

?>
