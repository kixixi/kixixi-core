// create namespace
KXJS.namespace("k_webmanager.objects.template_editor.put");

// create method of plugin
(function () {
    var t = this;
    this.init = function () {
        $(document).ready(t.ready);
    };

      t.ready = function () {


        //resize iframe
        $("#webEditorIframe").load(function() {
            var h = $(this).contents().find("body").height() + 50;
            if (h < 2700)
                h = 2700;
            $(this).height( h );
        });

    }
    this.init();

}).apply(KXNS.k_webmanager.objects.template_editor.put);

