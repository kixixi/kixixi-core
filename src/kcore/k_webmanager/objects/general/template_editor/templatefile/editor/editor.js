// create namespace
KXJS.namespace("k_webmanager.objects.template_editor.editor");

// create method of plugin
(function () {
    var t = this;
    this.init = function () {
        $(document).ready(t.ready);
    };

    t.loadModalEdit = function (tplID, tplCntID) {

        var idModal = "modalEdit";
        dialogModal = waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'warning'});

        $.ajax({
            url: KXJS.buildAPICallUrl("KXCJS", "k_webmanager", "template_editor", "getModalEdit"),
            data: {
                idModal: idModal,
                tplID: tplID,
                tplCntID: tplCntID,
                webCode: $("#webCode").val()
            }

        }).done(function (data) {
            r = $.parseJSON(data);
            waitingDialog.hide();
            if (r.success == "true") {
                $(r.html).appendTo('body');
                $('#' + idModal).modal("show");
                $('#' + idModal).removeAttr("tabindex");

                $('#' + idModal).on('hidden.bs.modal', function (e) {
                    $('#' + idModal).remove();
                    location.reload();
                });
            }
        });
    }
    t.loadModalEditFromBtn = function () {

        var idModal = "modalAdd";
        var tplID = $(this).parent().attr("data-tplid"); // template ID
        var tplCntID = $(this).parent().attr("data-tplcntid"); // template content ID
        t.loadModalEdit(tplID, tplCntID);
    }
    t.loadModalAdd = function () {

        var tplID = $(this).parent().attr("data-tplid"); // template ID
        var tplCntIDUP = $(this).parent().attr("data-tplcntid"); // template content ID
        var idModal = "modalAdd";

        if (tplID == "" || typeof(tplID) == "undefined")
            tplID = $("#webEditorKeyID").val();

        dialogModal = waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'warning'});
        $.ajax({
            url: KXJS.buildAPICallUrl("KXCJS", "k_webmanager", "template_editor", "getModalAdd"),
            data: {
                idModal: idModal,
                tplID: tplID,
                tplCntIDUP: tplCntIDUP
            }

        }).done(function (data) {
            r = $.parseJSON(data);
            waitingDialog.hide();
            if (r.success == "true") {

                $(r.html).appendTo('body');
                $('#' + idModal).modal("show");

                $('#' + idModal).on('hidden.bs.modal', function (e) {
                    $('#' + idModal).remove();
                });

            }
        });
    }


    t.openModalEdit = function (r) {

        if (r.success == "false")
            return;

        var tplID = r.tplID // template ID
        var tplCntID = r.tplCntID; // template content ID
        t.loadModalEdit(tplID, tplCntID);
    };
    t.askDelete = function () {
        var idModal = "modalRemove";
        var tplCntID = $(this).parent().attr("data-tplcntid"); // template content ID
        dialogModal = waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'warning'});
        $.ajax({
            url: KXJS.buildAPICallUrl("KXCJS", "k_common", "webservice", "askConfirm"),
            data: {
                type: "confirm",
                idModal: idModal,
            }

        }).done(function (data) {
            r = $.parseJSON(data);
            waitingDialog.hide();
            if (r.success == "true") {
                $(r.html).appendTo('body');
                $('#' + idModal).modal("show");

                $('#' + idModal).on('hidden.bs.modal', function (e) {
                    $('#' + idModal).remove();
                });

                $('#' + idModal + ' input[name="no"]').click(function () {
                    $('#' + idModal).modal("hide");
                });
                $('#' + idModal + ' input[name="yes"]').click(function () {
                    t.delete(tplCntID);
                    $('#' + idModal).modal("hide");
                });
            }
        });
    }
    t.delete = function (tplCntID) {
        $.ajax({
            url: KXJS.buildAPICallUrl("KXCJS", "k_webmanager", "template_editor", "deleteObj"),
            data: {
                tplCntID: tplCntID
            }

        }).done(function (data) {
            var r = $.parseJSON(data);
            if (r.success == "true")
                location.reload();
        });
    }

    t.moveUpWebObject = function()
    {
        var tplCntID = $(this).parent().attr("data-tplcntid"); // template content ID
        $.ajax({
            url: KXJS.buildAPICallUrl("KXCJS", "k_webmanager", "template_editor", "moveWebObject"),
            data: {
                tplCntID: tplCntID,
                moveDir: "up"
            }

        }).done(function (data) {
            var r = $.parseJSON(data);
            if (r.success == "true")
                location.reload();
        });
    }

    t.moveDownWebObject = function()
    {
        var tplCntID = $(this).parent().attr("data-tplcntid"); // template content ID
        $.ajax({
            url: KXJS.buildAPICallUrl("KXCJS", "k_webmanager", "template_editor", "moveWebObject"),
            data: {
                tplCntID: tplCntID,
                dir: "down"
            }

        }).done(function (data) {
            var r = $.parseJSON(data);
            if (r.success == "true")
                location.reload();
        });
    }

    t.appendObjName = function (obj, objName) {
        obj.append("<span class='tag tag-default webEditorObjectName'>" + objName + "</span>");
    }
    t.ready = function () {


        var webEditorKeyID = $("#webEditorKeyID").val();

        if (webEditorKeyID) {
            var btnAdd = $("#hiddenContainerBtnAdd").html();
            var btnEdit = $("#hiddenContainerBtnEdit").html();
            var btnDelete = $("#hiddenContainerBtnDelete").html();
            var btnMoveUp = $("#hiddenContainerBtnMoveUp").html();
            var btnMoveDown = $("#hiddenContainerBtnMoveDown").html();


            $("#btnAddOnPage").click(t.loadModalAdd);

            $('.webEditorBigContainer .kwebobject[data-iscontainer="1"]').each(function () {
                var newID = KXJS.getNewDomId();
                var btnAddTemp = btnAdd.replace("btnAddID", "btnAddID_" + newID);
                var btnEditTemp = btnEdit.replace("btnEditID", "btnEditID_" + newID);
                var btnDeleteTemp = btnDelete.replace("btnDeleteID", "btnDeleteID_" + newID);
                var btnMoveUpTemp = btnMoveUp.replace("btnMoveUpID", "btnMoveUpID_" + newID);
                var btnMoveDownTemp = btnMoveDown.replace("btnMoveDownID", "btnMoveDownID_" + newID);

                var istemplatein = $(this).attr("istemplatein");

                $(this).addClass("webEditorIsContainer");
                $(this).prepend('<div class="webEditorContainerButtons"></div>');

                $(this).find(".webEditorContainerButtons").append(btnAddTemp);

                if (istemplatein == 0) {


                    $(this).find(".webEditorContainerButtons").append(btnEditTemp);
                    $(this).find(".webEditorContainerButtons").append(btnMoveUpTemp);
                    $(this).find(".webEditorContainerButtons").append(btnMoveDownTemp);
                    $(this).find(".webEditorContainerButtons").append(btnDeleteTemp);

                    $(this).find(".webEditorContainerButtons").attr("data-tplid", $(this).attr("data-tplid"));
                    $(this).find(".webEditorContainerButtons").attr("data-tplcntid", $(this).attr("data-tplcntid"));


                }
                else
                {
                    $(this).find(".webEditorContainerButtons").attr("data-tplid", $("#webEditorKeyID").val()); //In this case, the tplid is the template id of opened template
                    $(this).find(".webEditorContainerButtons").attr("data-tplcntid", $(this).attr("data-tplcntid"));

                }



                var objName = $(this).attr("data-webobjectname");
                t.appendObjName($(this).find(".webEditorContainerButtons"), objName);
            });

            $('.webEditorBigContainer .kwebobject[data-iscontainer=""],.webEditorBigContainer .kwebobject[data-iscontainer="0"]').each(function () {

                var newID = KXJS.getNewDomId();
                var btnEditTemp = btnEdit.replace("btnEditID", "btnEditID_" + newID);
                var btnDeleteTemp = btnDelete.replace("btnDeleteID", "btnDeleteID_" + newID);
                var btnMoveUpTemp = btnMoveUp.replace("btnMoveUpID", "btnMoveUpID_" + newID);
                var btnMoveDownTemp = btnMoveDown.replace("btnMoveDownID", "btnMoveDownID_" + newID);

                var istemplatein = $(this).attr("istemplatein");


                $(this).prepend('<div class="webEditorObjectButtons"></div>');

                if (istemplatein == 0)
                {

                    $(this).find(".webEditorObjectButtons").append(btnEditTemp);
                    $(this).find(".webEditorObjectButtons").append(btnMoveUpTemp);
                    $(this).find(".webEditorObjectButtons").append(btnMoveDownTemp);
                    $(this).find(".webEditorObjectButtons").append(btnDeleteTemp);


                }

                $(this).find(".webEditorObjectButtons").attr("data-tplid", $(this).attr("data-tplid"));
                $(this).find(".webEditorObjectButtons").attr("data-tplcntid", $(this).attr("data-tplcntid"));


                var objName = $(this).attr("data-webobjectname");
                t.appendObjName($(this).find(".webEditorObjectButtons"), objName);


            });

            $(".webEditorBigContainer .btnEditWebObject").click(t.loadModalEditFromBtn);
            $(".webEditorBigContainer .btnAddWebObject").click(t.loadModalAdd);
            $(".webEditorBigContainer .btnDeleteWebObject").click(t.askDelete);
            $(".webEditorBigContainer .btnMoveUpWebObject").click(t.moveUpWebObject);
            $(".webEditorBigContainer .btnMoveDownWebObject").click(t.moveDownWebObject);


        }
        $(".webEditorBigContainer a").click(function (event) {
            event.preventDefault();
        });

        $("#btnPreview").click(function () {
            $(".webEditorBigContainer").addClass("webEditorBigContainerPreview");
            $(".webEditorBigContainer").removeClass("webEditorBigContainer");
            $(this).hide();
            $("#btnPageEdit").show();
        });

        $("#btnPageEdit").click(function () {
            $(".webEditorBigContainerPreview").addClass("webEditorBigContainer");
            $(".webEditorBigContainerPreview").removeClass("webEditorBigContainerPreview");
            $(this).hide();
            $("#btnPreview").show();
        });
        $("#btnPageEdit").hide();


    }
    this.init();

}).apply(KXNS.k_webmanager.objects.template_editor.editor);

