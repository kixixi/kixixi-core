// create namespace
KXJS.namespace("k_webmanager.objects.template_editor.modaladd");

// create method of plugin
(function () {
    var t = this;
    this.init = function () {
        $(document).ready(t.ready);
    };

    t.clickObjectButton = function()
    {
        $("#idWebObj").val($(this).attr("data-idwebobj"));
        $("#webObjPCode").val($(this).attr('data-pcode'));
        $("#webEditorAddObjectForm").submit();

    }
    t.ready = function () {


       $(".webEditorButtonAddObject").click(t.clickObjectButton);

    }
    this.init();

}).apply(KXNS.k_webmanager.objects.template_editor.modaladd);
