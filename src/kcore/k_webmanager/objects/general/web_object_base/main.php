<?
/**
 * KIXIXI WEB OBJECT BASE
 * This object is used as base of each web object created
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_webmanager
 * @subpackage objects\web_object_base
 * @since 1.00
 */
namespace k_webmanager\objects\general\web_object_base;
class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * Type of object (used from child)
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $type = "";

    /**
     * store all necessary JS Head
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $headJS = "";

    /**
     * store all necessary JS footer
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $footerJS = "";

    /**
     * store all necessary CSS
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $headCSS = "";

    /**
     * store all necessary files (JS + CSS etc)
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $headFiles = [];

    /**
     * store all necessary SEO Meta Tag
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $headSEOMetaTag = [];

    /**
     * store all necessary FB Meta Tag
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $headFBMetaTag = [];

    /**
     * store all plugin name to include
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $headExternalPlugin = [];

    /**
     * store the html of the object
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $html = "";

    /**
     * dom of this object
     *
     * @since 1.00
     * @access protected
     * @var dom object
     */
    protected $dom = "";

    /**
     * ID Of HTML Object
     * ID of object (if it have more tags in, it's the id of complete object)
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $IDObject = "";

    /**
     * ID Of HTML Object created where to append by default (the ID of the content)
     * For example on a Column is the ID where to append others OBJECT
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $IDContent = "";

    /**
     * Last Object Appended on DOM
     *
     * @since 1.00
     * @access public
     * @var web_object
     */
    public $lastObject = "";

    /**
     * Last Row Appended on DOM
     *
     * @since 1.00
     * @access public
     * @var web_object
     */
    public $lastRow = "";

    /**
     * Last Column Appended on DOM
     *
     * @since 1.00
     * @access public
     * @var web_object
     */
    public $lastCol = "";

    /**
     * Last Form Appended on DOM
     *
     * @since 1.00
     * @access public
     * @var web_object
     */
    public $lastForm = "";

    /**
     * Construct
     *
     */
    function __construct()
    {
        parent::__construct();
        $this->resetObject();
        $this->dom = $this->K_COMMON->newOnce($this->dom, '\k_webmanager\objects\general\dom\obj');
    }

    /**
     * Add Files to include in the head of page
     *
     * @param array $vars array of files to include
     * Examples:<br>
     *    *    ["/css/page.css","/js/page.js"]
     */
    public function addHeadFile($vars)
    {
        if (is_array($vars))
            foreach ($vars as $var) {
                $this->headFiles[] = $var;
            }
        else
            $this->headFiles[] = $vars;

    }

    /**
     * Add SEO Meta Tag to include in the head of page
     *
     * @param array $vars array of files to include
     * Examples:<br>
     *    *    [
     *          "title" =>  "The true Story"
     *          ,"keywords" => "kixixi, story"
     *          ,"description" => "Kixixi is the powerful software for web developing"
     *          ,"noindex" => "0"
     *          ,"nofollow" => "0"
     *          ]
     */
    public function addSEOMetaTag($vars)
    {
        if (!empty($vars))
            $this->headSEOMetaTag = $vars;

    }

    /**
     * GET SEO Meta Tag
     *
     * @param string $var var to get, if empty get array
     *
     * @return string!array
     */
    public function getSEOMetaTag($var = "")
    {
        if ($var != "")
            return $this->K_COMMON->getVarArray($this->headSEOMetaTag, $var);
        else
            return $this->headSEOMetaTag;

    }

    /**
     * Add FB Meta Tag to include in the head of page
     *
     * @param array $vars array of files to include
     * Examples:<br>
     *    *    [
     *          "title" =>  "The true Story"
     *          ,"description" => "kixixi, story"
     *          ,"image" => url of the image
     *          ]
     */
    public function addFBMetaTag($vars)
    {
        if (!empty($vars))
            $this->headFBMetaTag = $vars;

    }

    /**
     * Get FB Meta Tag to include in the head of page
     *
     * @param string $var var to get, if empty get array
     *
     *
     * @return string!array
     */
    public function getFBMetaTag($var = "")
    {

        if ($var != "")
            return $this->K_COMMON->getVarArray($this->headFBMetaTag, $var);
        else
            return $this->headFBMetaTag;

    }


    /**
     * Add external plugin name to include (see list)
     *
     * @param array $vars array of files to include
     * Examples:<br>
     *    *    ["validator"]
     */
    public function addExternalPlugin($vars)
    {
        if (is_array($vars))
            foreach ($vars as $var) {
                $this->headExternalPlugin[] = $var;
            }
        else
            $this->headExternalPlugin[] = $vars;

    }

    /**
     * Add JS to include on page (Head)
     *
     * @param string $js string of JS (with <scritp></script> tags)
     * Examples:<br>
     *    *    addJS("<script>alert('Hello World!');</script>")
     */
    public function addHeadJS($js)
    {

        $this->headJS .= $js;
    }

    /**
     * Add JS to include on page (Footer)
     *
     * @param string $js string of JS (with <scritp></script> tags)
     * Examples:<br>
     *    *    addJS("<script>alert('Hello World!');</script>")
     */
    public function addFooterJS($js)
    {

        $this->footerJS .= $js;
    }

    /**
     * Add CSS to include on page
     *
     * @param string $css string of CSS (with <style></style> tags)
     * Examples:<br>
     *    *    addJS("<style>body{background: green;}</style>")
     */
    public function addHeadCSS($css)
    {

        $this->headCSS .= $css;
    }


    /**
     * Merge variables from another web object
     *
     * @param web_object $wo
     */
    public function mergeObjectVariables($wo)
    {

        $this->headJS .= $wo->getHeadJS();
        $this->footerJS .= $wo->getFooterJS();
        $this->headCSS .= $wo->getHeadCSS();
        $this->headFiles = array_merge($this->headFiles, $wo->getHeadFiles());
        $this->headExternalPlugin = array_merge($this->headExternalPlugin, $wo->getExternalPlugin());
        if (!empty($wo->headSEOMetaTag))
            $this->headSEOMetaTag = array_merge($this->headSEOMetaTag,$wo->headSEOMetaTag);

        if (!empty($wo->headFBMetaTag))
            $this->headFBMetaTag = array_merge($this->headFBMetaTag, $wo->headFBMetaTag);

    }

    /**
     * Get HTML of object created
     *
     * @return string with html.
     */
    public function getHTML()
    {
        if (is_object($this->dom))
            return $this->dom->get();
        else
            return "";
    }

    /**
     * Get JS of object created
     *
     * @return string with html.
     */
    public function getHeadJS()
    {
        return $this->headJS;
    }

    /**
     * Get JS of object created
     *
     * @return string with html.
     */
    public function getFooterJS()
    {
        return $this->footerJS;
    }

    /**
     * Get JS of object created (completed and converted with external plugin, head files etc)
     *
     * @return string with html.
     */
    public function getHeadCSSJSConverted($cssByJS = false)
    {
        //convert files to include
        $headFiles = $this->getHeadFiles();
        $headFilesConverted = $this->convertHeadFiles($headFiles, $cssByJS);

        //convert external Plugin
        $externalPlugin = $this->getExternalPlugin();
        $externalPluginConverted = $this->convertExternalPlugin($externalPlugin, $cssByJS);

        //Assign HEAD JS AND CSS (before external plugin)
        return
            [
                "JS" =>  $this->getHeadJS()
                ,"JSFOOTER" => $externalPluginConverted["JS"] . $headFilesConverted["JS"] .$this->getFooterJS()
                , "CSS" => $externalPluginConverted["CSS"] . $headFilesConverted["CSS"] . $this->getHeadCSS()
            ];
    }

    /**
     * get complete object in HTML (with js and css)
     *
     * @return string html with js inclusion
     *
     *
     */
    public function getHTMLWithCSSJS()
    {
        //get HTML of Modal Body
        $html = $this->getHTML();

        //convert CSS/JS to include
        $headFiles = $this->getHeadCSSJSConverted(true);

        //Assign HEAD JS AND CSS (before external plugin)
        $html .= $headFiles["JSFOOTER"] . $headFiles["JS"] . $headFiles["CSS"];

        return $html;
    }

    /**
     * Get CSS of object created
     *
     * @return string with html.
     */
    public function getHeadCSS()
    {
        return $this->headCSS;
    }

    /**
     * Get Files To Include of object created
     *
     * @return array
     */
    public function getHeadFiles()
    {

        return array_unique($this->headFiles);
    }

    /**
     * Get External Plugin array
     *
     * @return array
     */
    public function getExternalPlugin()
    {

        return array_unique($this->headExternalPlugin);
    }

    /**
     * Set External Plugin array
     *
     * @return array
     */
    public function setExternalPlugin($ep = [])
    {

        return $this->headExternalPlugin = $ep;
    }

    /**
     * Reset External Plugin array
     *
     * @return array
     */
    public function resetExternalPlugin()
    {

        return $this->headExternalPlugin = [];
    }

    /**
     * Reset all object variables
     *
     */
    public function resetObject()
    {

        $this->headJS = "";
        $this->footerJS = "";
        $this->headCSS = "";
        $this->headFiles = [];
        $this->headExternalPlugin = [];
        $this->html = "";
        if (is_object($this->dom))
            $this->dom->newDom();
        $this->IDObject = "";
        $this->IDContent = "";
        $this->lastObject = $this;
        $this->lastRow = "";
        $this->lastCol = "";
        $this->lastForm = "";
        $this->headSEOMetaTag = [];
        $this->headFBMetaTag = [];
    }

    /**
     * Get Object, similar to get but with checks
     *
     * @param array $av array to pass to the get of object
     *
     * @return object.
     */
    public function getWO($av = [])
    {

        $this->resetObject();
        $objRet = $this->get($av);

        return $objRet;
    }

    /**
     * Get Object, similar to get but with checks
     *
     * @param array $av array to pass to the get of object
     * @param template $template optional template
     *
     * @return object.
     */
    public function getWOCustomBuilder($av = [])
    {

        $this->resetObject();
        $objRet = $this->customBuilder($av);

        return $objRet;
    }


    /**
     * Set HTML to the actual object HTML
     *
     * @param string $h html to put
     *
     */
    public function setHTML($h)
    {

        $this->dom->newDom(["html" => $h]);

    }

    /**
     * Append HTML to the actual object HTML
     *
     * @param string|web_object $id id where to append object, or object where append the new object
     * @param string $h html to put
     *
     */
    public function appendHTML($id, $h)
    {

        $this->appendObject($id, $h);

    }

    /**
     * Append object to ID
     *
     * @param web_object !string|array $objTo id where to append object, or object where append the new object
     * if array: <br>
     *    *    to: object or id to append
     *    *    column: nColumns or array for column object
     *    *    row: true|false or array for row object
     * @param web_object|string $wo object to append or string to append
     * @param boolean $useTagKWebObject true if use "kwebobject" tag
     * @param array $kwebobjectpar parameters for "kwebobject"
     *
     * @return web_object appended
     */
    public function appendObject($objTo, $wo, $useTagKWebObject = false, $kwebobjectpar = [])
    {


        if (is_object($objTo))
            $id = $objTo->getIDContent();
        elseif (is_array($objTo)) {
            $id = $this->K_COMMON->getVarArray($objTo, "to");
            $column = $this->K_COMMON->getVarArray($objTo, "column");
            $row = $this->K_COMMON->getVarArray($objTo, "row");

            if (is_bool($row)) {
                if ($row)
                {
                    $this->appendRow($id);
                }

            } elseif (is_array($row)) {
                {
                    $this->appendRow($id, $row);
                    $id = $this->lastRow->getIDContent();
                }
            }




            if (is_array($column)) {
                $this->appendColumn($id, $column);
                $id = $this->lastCol->getIDContent();
            } elseif (is_numeric($column)) {
                $this->appendColumn($id, ["nCol" => $column]);
                $id = $this->lastCol->getIDContent();
            }


        } else
            $id = $objTo;


        //append object
        $this->_append($id, $wo, $useTagKWebObject, $kwebobjectpar);


        if (!is_object($wo))
        {
            if ($this->getIDContent() != "")
                $this->lastObject = $this->getIDContent();
            else
                $this->lastObject = $id;

            return "";
        }


        else {

            $this->mergeObjectVariables($wo);

            $this->lastObject = $wo;

            return $wo;
        }
    }

    /**
     * Append FORM
     *
     * @param string|web_object $id id where to append object, or object where append the new object
     * @param array $av array of variables to send to object Form
     *
     * @return web_object appended
     */
    public function appendForm($id = "", $av = [])
    {
        $obj = new \k_htmlcomponents\objects\general\form\obj;

        $this->lastForm = $this->appendObject($id, $obj->getWO($av));

        return $this->lastForm;


    }

    /**
     * Append TAB
     *
     * @param string|web_object $id id where to append object, or object where append the new object
     * @param array $av array of variables to send to object Form
     *
     * @return web_object appended
     */
    public function appendTab($id = "", $av = [])
    {
        $obj = new \k_htmlcomponents\objects\general\tab\obj;

        $this->appendObject($id, $obj->getWO($av));


    }

    /**
     * Append CARD
     *
     * @param string|web_object $id id where to append object, or object where append the new object
     * @param array $av array of variables to send to object Form
     *
     * @return web_object appended
     */
    public function appendCard($id = "", $av = [])
    {
        $obj = new \k_htmlcomponents\objects\general\card\obj;

        $this->appendObject($id, $obj->getWO($av));


    }

    /**
     * Append LIST GROUP
     *
     * @param string|web_object $id id where to append object, or object where append the new object
     * @param array $av array of variables to send to object Form
     *
     * @return web_object appended
     */
    public function appendListGroup($id = "", $av = [])
    {
        $obj = new \k_htmlcomponents\objects\general\listgroup\obj;

        $this->appendObject($id, $obj->getWO($av));


    }

    /**
     * Append ROW
     *
     * @param string|web_object $id id where to append object, or object where append the new object
     * @param array $av array of variables to send to object Form
     *
     * @return web_object appended
     */
    public function appendRow($id = "", $av = [])
    {
        $obj = new \k_htmlcomponents\web\objects\w_row\obj;

        $this->lastRow = $this->appendObject($id, $obj->getWO($av));

        return $this->lastRow;


    }

    /**
     * Append COLUMN
     *
     * @param string|web_object $id id where to append object, or object where append the new object
     * @param array $av array of variables to send to object Form
     *
     * @return web_object appended
     */
    public function appendColumn($id = "", $av = [])
    {
        $obj = new \k_htmlcomponents\objects\general\column\obj;

        $this->lastCol = $this->appendObject($id, $obj->getWO($av));

        return $this->lastCol;


    }

    /**
     * Append INPUT
     *
     * @param string|web_object|array $id id where to append object, or object where append the new object
     *
     * @param array $av array of variables to send to object Form
     *
     * @return web_object appended
     */
    public function appendInput($id = "", $av = [])
    {

        $obj = new \k_htmlcomponents\objects\general\input\obj;

        return $this->appendObject($id, $obj->getWO($av));


    }

    /**
     * Append File Browser
     *
     * @param string|web_object|array $id id where to append object, or object where append the new object
     *
     * @param array $av array of variables to send to object Form
     * @pluginName string plugin used for get folder
     *
     * @return web_object appended
     */
    public function appendFileBrowser($id = "", $av = [], $pluginName = null)
    {

        $obj = new \k_htmlcomponents\objects\general\input\obj;

        $av["type"] = "serverFile";
        if ($pluginName === null)
            $av["getFolderByPlugin"] = $this->getPluginName();

        return $this->appendObject($id, $obj->getWO($av));


    }

    /**
     * Append Text Editor
     *
     * @param string|web_object|array $id id where to append object, or object where append the new object
     *
     * @param array $av array of variables to send to object Form
     * @pluginName string plugin used for get folder
     *
     * @return web_object appended
     */
    public function appendEditor($id = "", $av = [], $pluginName = null)
    {

        $obj = new \k_htmlcomponents\objects\general\input\obj;

        $av["type"] = "textEditor";
        if ($pluginName === null)
            $av["getFolderByPlugin"] = $this->getPluginName();

        return $this->appendObject($id, $obj->getWO($av));


    }

    /**
     * Append Alert
     *
     * @param string|web_object|array $id id where to append object, or object where append the new object
     *
     * @param array $av array of variables to send to object Form
     *
     * @return web_object appended
     */
    public function appendAlert($id = "", $av = [])
    {
        $obj = new \k_htmlcomponents\objects\general\alert\obj;

        return $this->appendObject($id, $obj->getWO($av));


    }

    /**
     * Append GENERAL OBJECT with tag "kwebobject"
     * Append the object  on pluginName and pluginObject parameters
     *
     * @param string|web_object $id id where to append object, or object where append the new object
     * @param string $pluginName name of plugin
     * @param string $pluginObject name of object of plugin
     * @param array $av array of variables to send to object Form
     * @param boolean $useTagKWebObject if true add "kwebobect" tag
     *
     * @return web_object appended
     */
    public function appendKWebObject($id = "", $pluginName, $pluginObject, $av = [], $useTagKWebObject = true)
    {

        $objectTypeArr = $this->K_COMMON->getObjectType($pluginObject);
        
        $pluginObject = $objectTypeArr["pluginObjectWithType"];
        $objName = "\\" . $pluginName ."\\" . $pluginObject . "\\obj";

        $objLoaded = $this->K_COMMON->loadPluginObject($pluginName, $pluginObject);

        if ($objLoaded)
            $obj = new $objName;
        else
            $obj = new \k_webmanager\objects\general\web_object\obj; //create a empty object

        //check if there are "kwebobject" parameters
        if (isset($av["kwebobjectpar"])) {
            $kwebobjectpar = $av["kwebobjectpar"];
            unset($av["kwebobjectpar"]);

        } else
            $kwebobjectpar = [];

        $objApp = $this->appendObject($id, $obj->getWO($av), $useTagKWebObject, $kwebobjectpar);

        if (!empty($obj->type)) {
            switch ($obj->type) {
                case "form":
                    $this->lastForm = $objApp;
                    break;
                case "row":
                    $this->lastRow = $objApp;
                    break;
                case "column":
                    $this->lastCol = $objApp;
                    break;
            }
        }

        $this->lastObject = $objApp;
        return $objApp;


    }

    /**
     * Append GENERAL OBJECT (same of appenWebObject but not use tag "kwebobject")
     * Append the object  on pluginName and pluginObject parameters
     *
     * @param string|web_object $id id where to append object, or object where append the new object
     * @param string $pluginName name of plugin
     * @param string $pluginObject name of object of plugin
     * @param array $av array of variables to send to object Form
     *
     * @return web_object appended
     */
    public function appendWO($id = "", $pluginName, $pluginObject, $av = [])
    {
        return $this->appendKWebObject($id, $pluginName, $pluginObject, $av, false);


    }


    /**
     * GET WEB TEMPLATE from template code
     *
     * @param string|web_object $id id where to append object, or object where append the new object
     * @param string $tmplCode code of template
     * @param array $objectsVariables array of variables to send to template, for each object
     * @param string $pluginName name of plugin of template, if empty get actual plugin name
     * @param string $pluginObject name of plugin object of template, if empty get actual plugin object name
     *
     * @return web_object
     */
    public function getWebTemplate($id = "", $tmplCode, $objectsVariables = [], $pluginName = null, $pluginObject = null, $webCode = "",$useTagKWebObject = true)
    {

        if ($pluginName === null)
            $pluginName = $this->getPluginName();
        if ($pluginObject === null)
            $pluginObject = $this->getPluginObjectName();


        $webTemplate = new \k_webmanager\objects\general\web_templates\obj;

        $webTemplate->get(
            [
                "useTagKWebObject" => $useTagKWebObject
                ,"tmplCode" => $tmplCode
                , "tmplPluginName" => $pluginName
                , "tmplPluginObject" => $pluginObject
                , "tmplWebCode" => $webCode
            ]
            , $objectsVariables
        );

        return $webTemplate;

    }

    /**
     * Append WEB TEMPLATE from template code
     *
     * @param string|web_object $id id where to append object, or object where append the new object
     * @param string $tmplCode code of template
     * @param array $objectsVariables array of variables to send to template, for each object
     * @param string $pluginName name of plugin of template, if empty get actual plugin name
     * @param string $pluginObject name of plugin object of template, if empty get actual plugin object name
     * @param array $templateVariables : see web_templates object
     *
     * @return web_object appended
     */

    public function appendWebTemplate($id = "", $tmplCode, $objectsVariables = [], $templateVariables = [], $pluginName = null, $pluginObject = null)
    {


        if ($pluginName === null)
            $templateVariables["tmplPluginName"] = $this->getPluginName();
        else
            $templateVariables["tmplPluginName"] = $pluginName;
        if ($pluginObject === null)
            $templateVariables["tmplPluginObject"] = $this->getPluginObjectName();
        else
            $templateVariables["tmplPluginObject"] = $pluginObject;
        if ($tmplCode != "")
            $templateVariables["tmplCode"] = $tmplCode;
        if (!isset($templateVariables["useTagKWebObject"]))
            $templateVariables["useTagKWebObject"] = true;


        $webTemplate = new \k_webmanager\objects\general\web_templates\obj;

        $webTemplate->get($templateVariables, $objectsVariables);


        return $this->appendObject($id, $webTemplate);

    }

    /**
     * Append WEB TEMPLATE from id template
     *
     * @param string|web_object $id id where to append object, or object where append the new object
     * @param string $tmplID Id of template
     * @param array $objectsVariables array of variables to send to template, for each object
     * @param boolean $useTagKWebObject : if true add tag "kwebobject"
     * @param boolean $setModifyTemplate : if true set variables on kwebobject for use on modify template
     *
     * @return web_object appended
     */
    public function appendWebTemplateByID($id = "", $tmplID, $objectsVariables = [], $useTagKWebObject = true, $setModifyTemplate = false)
    {

        $webTemplate = new \k_webmanager\objects\general\web_templates\obj;

        $webTemplate->get(["keyID" => $tmplID, "useTagKWebObject" => $useTagKWebObject, "setModifyTemplate" => $setModifyTemplate], $objectsVariables);

        return $this->appendObject($id, $webTemplate);

    }

    /**
     * set HTML ID Default (where to append others objects)
     * This is the ID of Content of Object
     *
     * @param string $id
     *
     */
    public function setIDContent($id)
    {
        $this->IDContent = $id;
    }

    /**
     * get HTML ID Default (where to append others objects)
     * This is the ID of Content of Object
     *
     * @return string $id
     *
     */
    public function getIDContent()
    {
        return $this->IDContent;
    }

    /**
     * set HTML ID of Object
     * This is the ID of Object
     *
     * @param string $id
     *
     */
    public function setIDObject($id)
    {
        $this->IDObject = $id;
    }

    /**
     * get HTML ID Object
     * This is the ID of Object
     *
     * @return string $id
     *
     */
    public function getIDObject()
    {
        return $this->IDObject;
    }

    /**
     * get DOM Object
     *
     * @return dom
     *
     */
    public function getDOMObject()
    {

        return $this->dom;
    }

    /**
     * set DOM Object
     *
     *
     */
    public function setDOMObject($dom)
    {
        $this->dom = $dom;
    }


    /**
     * get include files as tag
     *
     * @param array $arrFiles
     * @param boolean $cssByJS
     *
     * @return array
     */
    protected function convertHeadFiles($arrFiles, $cssByJS = false)
    {

        //set variables
        $echoJS = "";
        $echoCSS = "";
        $arrFiles = array_unique($arrFiles);

        foreach ($arrFiles as $FileInclude) {

            if (strpos($FileInclude, ".css") > 0) {
                if (!$cssByJS)
                    $echoCSS .= '<link href="' . $FileInclude . '" rel="stylesheet" type="text/css"/>' . chr(13);
                else
                    $echoJS .= '<script>KXJS.loadjscss("' . $FileInclude . '","css");</script>' . chr(13);
            }
            if (strpos($FileInclude, ".js") > 0) {

                $echoJS .= "<script src='" . str_replace(chr(13), "", str_replace(chr(13) . chr(10), "", $FileInclude)) . "' type='text/javascript'></script>" . chr(13);

            }

        }


        return ["JS" => $echoJS, "CSS" => $echoCSS];


    }

    /**
     * Reset Head File JS (remove JS Files)
     *
     * @param array $arrFiles
     * @param boolean $cssByJS
     *
     * @return array
     */
    protected function resetFileJS()
    {

        $arrFiles = $this->headFiles;

        foreach ($arrFiles as $k => $FileInclude) {

            if (strpos($FileInclude, ".js") > 0) {

               unset($arrFiles[$k]);

            }

        }



        $this->headFiles = $arrFiles;
        $this->headJS = "";
        $this->footerJS = "";


    }

    /**
     * convert External plugin Name
     *
     * @param array $arrPlugin
     *
     * @return array
     */
    public function convertExternalPlugin($arrPlugin, $cssByJS = false)
    {
        //set variables
        $scriptJS = "";
        $arrayCSS = [];
        $includeCSS = "";

        //Load External Plugin
        if (is_array($arrPlugin)) {

            foreach ($arrPlugin as $plugin) {
                switch ($plugin) {
                    case "jquery":
                        $scriptJS .= '<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>' . chr(13) . chr(10);
                        break;

                    case "kjstart":
                        $scriptJS .= '<script type="text/javascript" src="/kcore/k_webmanager/jscript/kjstart.js"></script>' . chr(13) . chr(10);
                        break;
                    case "bootstrap":
                        $scriptJS .= '<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>' . chr(13) . chr(10);
                        $scriptJS .= '<script type="text/javascript" src="/kmore/sdk/bootstrap/v4-alpha/bootstrap.min.js"></script>' . chr(13) . chr(10);
                        $scriptJS .= '<script type="text/javascript" src="/kmore/sdk/bootstrap/v4-alpha/ie10-viewport-bug-workaround.js"></script>' . chr(13) . chr(10);
                        $arrayCSS[] = "/kmore/sdk/bootstrap/v4-alpha/bootstrap.min.css";
                        break;
                    case "comboAjax":
                        $scriptJS .= '<script type="text/javascript" src="/kmore/sdk/bootstrap_plugin/comboAjax/comboAjax.js"></script>' . chr(13) . chr(10);
                        $arrayCSS[] = "/kmore/sdk/bootstrap_plugin/comboAjax/comboAjax.css";
                        break;
                    case "bootstrap-toggle":
                        $scriptJS .= '<script type="text/javascript" src="/kmore/sdk/bootstrap_plugin/bootstrap-toggle/bootstrap-toggle.js"></script>' . chr(13) . chr(10);
                        $arrayCSS[] = "/kmore/sdk/bootstrap_plugin/bootstrap-toggle/bootstrap-toggle.css";
                        break;
                    case "button-toggle":
                        $arrayCSS[] = "/kmore/sdk/bootstrap_plugin/button-toggle/button-toggle.css";
                        break;
                    case "colorpicker":
                        $scriptJS .= '<script type="text/javascript" src="/kmore/sdk/bootstrap_plugin/colorpicker/js/bootstrap-colorpicker.js"></script>' . chr(13) . chr(10);
                        $arrayCSS[] = "/kmore/sdk/bootstrap_plugin/colorpicker/css/bootstrap-colorpicker-modified.css";
                        break;
                    case "datetimepicker":
                        $scriptJS .= '<script type="text/javascript" src="/kmore/sdk/bootstrap_plugin/datepicker/bootstrap-datetimepicker-modified.js"></script>' . chr(13) . chr(10);
                        $arrayCSS[] = "/kmore/sdk/bootstrap_plugin/datepicker/bootstrap-datetimepicker-modified.css";
                        break;
                    case "datepickermulti":
                        $scriptJS .= '<script type="text/javascript" src="/kmore/sdk/bootstrap_plugin/datepickermulti/js/bootstrap-datepicker-modified.js"></script>' . chr(13) . chr(10);
                        $scriptJS .= '<script type="text/javascript" src="/kmore/sdk/bootstrap_plugin/datepickermulti/locales/bootstrap-datepicker.' . strtolower($this->K_COMMON->language) . '.min.js"></script>' . chr(13) . chr(10);
                        $arrayCSS[] = "/kmore/sdk/bootstrap_plugin/datepickermulti/css/bootstrap-datepicker.min.css";
                        break;
                    case "validator":
                        $scriptJS .= '<script type="text/javascript" src="/kmore/sdk/bootstrap_plugin/validator/validator-modified.js"></script>' . chr(13) . chr(10);
                        break;
                    case "waitingfor":
                        $scriptJS .= '<script type="text/javascript" src="/kmore/sdk/bootstrap_plugin/waitingfor/waitingfor-modified.js"></script>' . chr(13) . chr(10);
                        break;
                    case "typeahead":
                        $scriptJS .= '<script type="text/javascript" src="/kmore/sdk/bootstrap_plugin/typeahead/typeahead.bundle.modified.js"></script>' . chr(13) . chr(10);
                        $arrayCSS[] = "/kmore/sdk/bootstrap_plugin/typeahead/typeahead.css";
                        break;
                    case "tinymce4":
                        $scriptJS .= '<script type="text/javascript" src="/kmore/sdk/tinymce/tinymce4/tinymce.min.js"></script>' . chr(13) . chr(10);
                        $scriptJS .= '<script type="text/javascript" src="/kmore/sdk/tinymce/tinymce4/KTinyMceExtend.js"></script>' . chr(13) . chr(10);
                        break;
                    case "chartjs":
                        $scriptJS .= '<script type="text/javascript" src="/kmore/sdk/chartjs/chart.bundle.js"></script>' . chr(13) . chr(10);
                        break;

                    case "facebook":
                        $fbID = $this->getParamValue("FB_appId", "", "k_secauth", "");
                        $scriptJS .= '
                                <script>
                                
                               $(document).ready(function(){
                                    var eDiv = document.createElement(\'div\');
                                    eDiv.id = "fb-root";
                                    document.body.appendChild(eDiv);
                                    
                                    var fbID = "' . $fbID . '";
                                    var e = document.createElement(\'script\'); 
                                    e.async = true;
                                    
                                    e.src = document.location.protocol + \'//connect.facebook.net/it_IT/all.js\';
                                    document.getElementById(\'fb-root\').appendChild(e);
                                    window.fbAsyncInit = function() {
                                        
                                        FB.init({
                                            appId  : fbID,
                                            status : true, 
                                            cookie : true, 
                                            xfbml  : true, 
                                            authResponse: true,
                                            oauth : true
                                        });		
                                    };
                                 });
                                </script>
                            ';
                        break;
                }
            }
        }

        //manage CSS
        foreach ($arrayCSS as $css) {
            if (!$cssByJS)
                $includeCSS .= '<link href="' . $css . '" rel="stylesheet" type="text/css"/>' . chr(13);
            else
                $scriptJS .= '<script>KXJS.loadjscss("' . $css . '","css");</script>' . chr(13);
        }

        return ["JS" => $scriptJS, "CSS" => $includeCSS];

    }

    /**
     * convert kwebobject par
     *
     * @param array $kwebobjectpar array of parameter for kwebobject
     *
     * @return array
     */
    public function elaborateKWebObjectPar($kwebobjectpar, $webCode = "")
    {

       
        //set variables
        $kwoAttributes = "";
        $kwoStyle = "";
        $kwoClass = "";

        //check kwebobject parameters
        foreach ($kwebobjectpar as $key => $val) {
            if ($val != "") 
                switch ($key) {
                    case "style":
                        $kwoStyle .= $val . ";";
                        break;
                    case "padding-top":
                        $kwoStyle .= "padding-top:" . $val . "px;";
                        break;
                    case "padding-right":
                        $kwoStyle .= "padding-right:" . $val . "px;";
                        break;
                    case "padding-bottom":
                        $kwoStyle .= "padding-bottom:" . $val . "px;";
                        break;
                    case "padding-left":
                        $kwoStyle .= "padding-left:" . $val . "px;";
                        break;
                    case "background-color":
                        $kwoStyle .= "background-color:" . $val . ";";
                        break;
                    case "background-image":
                        $urlBase = $this->K_COMMON->getStaticContentURLWeb($webCode);
                        $kwoStyle .= "background-image:url(" . $urlBase.$val . ");";

                        break;
                    case "color":
                        $kwoStyle .= "color:" . $val . ";";
                        break;
                    case "class":
                        $kwoClass .= " " . $val;
                        break;
                    default:
                        $kwoAttributes .= ' ' . $key . '="' . $val . '"';
                        break;

                }
        }

        return ["style" => $kwoStyle, "class" => $kwoClass, "attributes" => $kwoAttributes];
    }

    /* ***************************************************************************** */
    /* ****************************** PROTECTED FUNCTIONS ************************** */
    /* ***************************************************************************** */

    protected function _append($id, $wo, $useTagKWebObject, $kwebobjectpar)
    {

        //set variable
        $htmlToAppend = "";

        //check variables
        if (is_object($wo))
            $htmlToAppend = $wo->getHTML();
        elseif (is_string($wo))
            $htmlToAppend = $wo;



        //append object to dom
        if ($useTagKWebObject)
        {
            //check kwebobject parameters
            $parElaborated = $this->elaborateKWebObjectPar($kwebobjectpar);
            $kwoStyle = $parElaborated["style"];
            $kwoClass = $parElaborated["class"];
            $kwoAttributes = $parElaborated["attributes"];

            //check style
            if ($kwoStyle != "")
                $kwoAttributes .= ' style="' . $kwoStyle . '"';

            $this->dom->append($id, '<div class="kwebobject ' . $kwoClass . '" ' . $kwoAttributes . '>' . $htmlToAppend . '</div>');
        }
        elseif ($htmlToAppend != "")
            $this->dom->append($id, $htmlToAppend);

    }


}

?>