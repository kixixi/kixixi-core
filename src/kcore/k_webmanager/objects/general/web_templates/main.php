<?
/**
 * KIXIXI WEB TEMPLATES
 * Load and manage web template
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_webmanager
 * @subpackage objects\web_templates
 * @since 1.00
 */
namespace k_webmanager\objects\general\web_templates;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * Name of Template
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $tplName = "";

    /**
     * Web Code of Template
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $tplWebCode = "1.0";

    /**
     * Directory on KTemplate / KPlugin Structure of media files for web template
     * Contains all files that will be used in the web template
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $mediaFilesDir = "mediaFiles";


    /**
     * Constructor
     *
     */
    public function __construct($AV = [])
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
    }

    /**
     * get a template (build web object)
     *
     * @param array $templateVariables variables for template
     * @param array $objectsVariables variables for template's object
     *
     *  Parameters for $templateVariables: <br>
     *
     * *    tmplCode    : code of template to get
     * *    tmplPluginName    : plugin name for get template (if empty get a user template)
     * *    tmplPluginObject    : plugin object name for get template (specify only if tmplPluginName is avalued)
     * *    useTagKWebObject : if true add tag "kwebobject"
     * *    setModifyTemplate : if true set variables on kwebobject for use on modify template
     * *    loadTemplateIN: if true we are loading a template in another template
     *
     *  Parameters for $objectsVariables: <br>
     *
     * *    global    : array of variables to send to all objects (Ex: ["baseURL" => "/manage", "urlParam" => ["get" => "1"]])
     * *    objects    : array of ID objects to pass values (Ex: ["obj1" = ["value" => "Marco"],"obj2" = ["value" => "Edit", "link" => "get"]])
     *
     * @return web_object
     */
    public function get($templateVariables, $objectsVariables = [])
    {
        //get variables from $templateVariables
        $keyID = $this->K_COMMON->getVarArray($templateVariables, "keyID");
        $tmplCode = $this->K_COMMON->getVarArray($templateVariables, "tmplCode");
        $tmplPluginName = $this->K_COMMON->getVarArray($templateVariables, "tmplPluginName");
        $tmplPluginObject = $this->K_COMMON->getVarArray($templateVariables, "tmplPluginObject");
        $tmplWebCode = $this->K_COMMON->getVarArray($templateVariables, "tmplWebCode");
        $useTagKWebObject = $this->K_COMMON->getVarArray($templateVariables, "useTagKWebObject");
        $setModifyTemplate = $this->K_COMMON->getVarArray($templateVariables, "setModifyTemplate");
        $loadTemplateIN = $this->K_COMMON->getVarArray($templateVariables, "loadTemplateIN");
        $dbDataLoaded = []; //loaded data from db
        $dbDataToRef = [];  //object id from to get data

        //check variables
        if ($useTagKWebObject == "")
            $useTagKWebObject = true;
        if ($setModifyTemplate)
            $useTagKWebObject = true;
        else
            $setModifyTemplate = false;

        //get variables from $objectsVariables
        $objGlobalVariables = $this->K_COMMON->getVarArray($objectsVariables, "global");
        $objObjectsVariables = $this->K_COMMON->getVarArray($objectsVariables, "objects");
        $urlParam = $this->K_COMMON->getVarArray($objGlobalVariables, "urlParam");

        //set variables
        $arrayObjToAppend = []; //contain object to append
        $arrayObjInserted = []; //list of object inserted

        //get tables
        $table = $this->getTable("web_templates");

        //load template
        if ($keyID != "") //load user template
        {
            $cond = [];
            $cond["id_web_template"] = $keyID;

        } else //load a plugin template
        {
            $cond = [];
            $cond["tpl_code"] = $tmplCode;
            $cond["tpl_plugin_name"] = $tmplPluginName;
            $cond["tpl_plugin_object"] = $tmplPluginObject;
            $cond["tpl_web_code"] = $tmplWebCode;
        }

        //get Data Template
        $table->get(["*"], $cond);
        if ($data = $table->fetch()) {
            $tplID = $data->id_web_template;
            $tplFile = $data->tpl_file;
            $tplInclude = $data->tpl_include;
            $tplObjPluginName = $data->tpl_plugin_name;
            $tplObjPluginObject = $data->tpl_plugin_object;
            $tplCode = $data->tpl_code;
            $tmplPluginName = $data->tpl_plugin_name;
            $tmplPluginObject = $data->tpl_plugin_object;
            $tmplWebCode = $data->tpl_web_code;
            $tmplName = $data->tpl_name;

            //Search if load tpl file
            if ($tplFile != "") {

                $tplLayout = $this->getTemplateLayout($tplFile, $tplObjPluginName, $tplObjPluginObject);
                $htmlLay = $tplLayout->pparse("body");

                //Return
                $this->appendHTML("", $htmlLay);
            }

            //Search if include files (css + js)
            if ($tplInclude != "") {
                $tplIncludeArr = json_decode($tplInclude, true);

                foreach ($tplIncludeArr as $type => $files) {
                    foreach ($files as $file) {
                        switch ($type) {
                            case "plugin":
                                $this->addExternalPlugin($file);
                                break;
                            case "css":
                                if (substr($file, 0, 1) == "/") //get from templatefile folder
                                    $this->addHeadFile($this->K_COMMON->getPathPluginObject(false, $tplObjPluginName, $tplObjPluginObject) . "/templatefile" . $file);
                                else //get from template code folder
                                    $this->addHeadFile($this->K_COMMON->getPathPluginObject(false, $tplObjPluginName, $tplObjPluginObject) . "/templatefile/" . $tplCode . "/" . $file);
                                break;
                            case "js":
                                if (substr($file, 0, 1) == "/") //get from templatefile folder
                                    $this->addHeadFile($this->K_COMMON->getPathPluginObject(false, $tplObjPluginName, $tplObjPluginObject) . "/templatefile" . $file);
                                else //get from template code folder
                                    $this->addHeadFile($this->K_COMMON->getPathPluginObject(false, $tplObjPluginName, $tplObjPluginObject) . "/templatefile/" . $tplCode . "/" . $file);
                                break;
                        }
                    }
                }

            }

        } else
            return $this;

        //set template Data as globla class variables (May be used from plugin)
        $this->tplName = $tmplName;
        $this->tplWebCode = $tmplWebCode;

        //get data content
        $table->getByQuery("web_templates_template_custom_for_modify", "", $cond, "", "", ["tpo_tpl_id_obj_up", "tpo_order"]);

        //Put in Array all objects, before append
        $webObjectName = "";
        while ($data = $table->fetch()) {

            //get fields
            $tplCNTID = $data->id_web_template_content;
            $tplIDObj = $data->tpo_tpl_id_obj;
            $tplIDObjUp = $data->tpo_tpl_id_obj_up;
            $tplObjPluginName = $data->tpo_wo_plugin_name;
            $tplObjPluginObject = $data->tpo_wo_plugin_object;
            $tplObjParameters = $data->tpo_wo_parameters;
            $tplCntParameters = (array)json_decode($data->tpo_parameters, true);
            $tplCntDataParameters = (array)json_decode($data->tpo_data, true);


            //convert parameters to array
            $tplObjParametersArray = json_decode($tplObjParameters, true);

            //Check if is a container
            $isContainer = $data->wob_container; //we get this value for load db Data

            //check if we a in modify
            if ($setModifyTemplate) {

                $webObjectName = $data->wob_name;
                $webObjectPrototypes = $data->wob_prototypes;
                $webObjectPrototypesArray = (array)json_decode($webObjectPrototypes, true);
                $prototypesCode = $this->K_COMMON->getVarArray($tplObjParametersArray, "webObjPCode");
                $webObjectNameArr = $this->K_COMMON->getVarArray($webObjectPrototypesArray, $prototypesCode);
                if (is_array($webObjectNameArr))
                    $webObjectName = $webObjectNameArr["name"];

            }


            $tplObjParametersArray["id"] = $tplIDObj;
            $tplObjParametersArray["webCode"] = $tmplWebCode;

            //Merge the $tplObjParametersArray with $objGlobalVariables
            if ($objGlobalVariables != "")
                $tplObjParametersArray = array_merge($tplObjParametersArray, $objGlobalVariables);

            //Search also if there are variables for this object
            $objCustomVar = $this->K_COMMON->getVarArray($objObjectsVariables, $tplIDObj);
            if (is_array($objCustomVar))
                $tplObjParametersArray = array_merge($tplObjParametersArray, $objCustomVar);


            //add to array
            $arrayObjToAppend[$tplIDObjUp][] =
                [
                    "tplID" => $tplID
                    , "tplCNTID" => $tplCNTID
                    , "id" => $tplIDObj
                    , "pn" => $tplObjPluginName
                    , "po" => $tplObjPluginObject
                    , "pp" => $tplObjParametersArray
                    , "isContainer" => $isContainer
                    , "webObjectName" => $webObjectName
                    , "dbData" => $tplCntDataParameters
                    , "kwebobjectpar" => $tplCntParameters
                ];


        }



        //get Object Dictionary
        $pluginDictionary = $this->getDictionary($tmplPluginName, $tmplPluginObject,$tmplWebCode);

        //Append Objects
        reset($arrayObjToAppend);
        $exitFromArray = false;

        while (count($arrayObjToAppend) > 0 && !$exitFromArray) {

            $IDUp = key($arrayObjToAppend);
            $checkID = $this->dom->getElementById($IDUp);

            if (is_object($checkID) || $IDUp == "") {

                $arrTmp = current($arrayObjToAppend);

                //Check if not error on record saved on db - if is array is correct
                if (is_array($arrTmp)) {

                    foreach ($arrTmp as $key => $obj) {
                        
                        if (isset($obj["pp"]["dictionary"])) {

                            if (is_array($obj["pp"]["dictionary"]))
                                foreach ($obj["pp"]["dictionary"] as $key => $word) {
                                    $obj["pp"][$key] = $this->K_COMMON->getVarArray($pluginDictionary, $word);
                                }

                            unset($obj["pp"]["dictionary"]);

                        }

                        $obj["pp"]["dictionary"] = $pluginDictionary;

                        $useTagKWebObjectActual = $useTagKWebObject;

                        if ($useTagKWebObject) {
                            if (!isset($obj["kwebobjectpar"]) || !is_array($obj["kwebobjectpar"]))
                                $obj["pp"]["kwebobjectpar"] = [];
                            else
                                $obj["pp"]["kwebobjectpar"] = $obj["kwebobjectpar"];


                            //check if modify template
                            if ($setModifyTemplate) {


                                $pluginObjectName = $this->K_COMMON->getPluginObjectName($obj["po"], "flat");

                                $obj["pp"]["kwebobjectpar"]["data-tplid"] = $obj["tplID"];
                                $obj["pp"]["kwebobjectpar"]["data-tplcntid"] = $obj["tplCNTID"];
                                $obj["pp"]["kwebobjectpar"]["data-iscontainer"] = $obj["isContainer"];
                                $obj["pp"]["kwebobjectpar"]["data-webobjectname"] = $obj["webObjectName"];
                                $obj["pp"]["kwebobjectpar"]["data-pn"] = $obj["pn"];
                                $obj["pp"]["kwebobjectpar"]["data-po"] = $obj["po"];
                                $obj["pp"]["kwebobjectpar"]["data-htmlid"] = $obj["id"];
                                $obj["pp"]["kwebobjectpar"]["data-namespace"] = $obj["pn"]."_".$pluginObjectName;
                                $obj["pp"]["kxEditTemplate"] = "1";


                            }

                            //check if load template IN
                            $obj["pp"]["kwebobjectpar"]["isTemplateIN"] = ($loadTemplateIN == true ? "1" : "0");


                            //Data to Load
                            $DBData = $obj["dbData"];

                            //if container, set kwebobjectpar to "pp"
                            if ($obj["isContainer"] == "1") {
                                $useTagKWebObjectActual = false;

                                $this->K_COMMON->concatVarArray($obj["pp"], "class", " kwebobject");

                                //check kwebobject parameters
                                $parElaborated = $this->elaborateKWebObjectPar($obj["pp"]["kwebobjectpar"], $tmplWebCode);
                                $kwoStyle = $parElaborated["style"];
                                $kwoClass = $parElaborated["class"];
                                $kwoAttributes = $parElaborated["attributes"];

                                $this->K_COMMON->concatVarArray($obj["pp"], "style", " " . $kwoStyle);
                                $this->K_COMMON->concatVarArray($obj["pp"], "class", " " . $kwoClass);
                                $this->K_COMMON->concatVarArray($obj["pp"], "attributes", " " . $kwoAttributes);

                                //Check if we need to load data for this container
                                if (isset($dbDataToRef[$IDUp]))
                                    $dbDataToRef[$obj["id"]] = $dbDataToRef[$IDUp];
                                elseif (!empty($DBData)) {
                                    $dataLoad = (array)$this->K_COMMON->getVarArray($DBData, "load");
                                    if (!empty($dataLoad)) {
                                        $pluginName = $this->K_COMMON->getVarArray($dataLoad, "pluginName");
                                        $pluginData = $this->K_COMMON->getVarArray($dataLoad, "pluginData");
                                        $filterGET = $this->K_COMMON->getVarArray($dataLoad, "filterGET");
                                        if ($filterGET != "")
                                        {
                                            //check if ulr param is not empty
                                            $urlParamGET = $this->K_COMMON->getVarArray($urlParam, "get");
                                            if ($urlParamGET != "")
                                            {
                                                if ($pluginName != "" && $pluginData != "") {
                                                    $tableData = $this->K_COMMON->getPluginDataObject($pluginName, $pluginData);
                                                    $tableData->get("*", [$filterGET => $urlParamGET]);

                                                    $dbDataLoaded[$obj["id"]] = $tableData;
                                                    $dbDataToRef[$obj["id"]] = $obj["id"];

                                                }
                                            }
                                        }

                                    }

                                }

                            } else {
                                //Check if to get value from data loaded
                                if (!empty($DBData)) {

                                    //check if there are data loaded
                                    if (isset($dbDataToRef[$IDUp]))
                                    {
                                        $dataGet = (array)$this->K_COMMON->getVarArray($DBData, "get");
                                        if (!empty($dataGet)) {
                                            $fieldName = $this->K_COMMON->getVarArray($dataGet, "fieldName");
                                            if ($fieldName != "") {
                                                $tableTmp = $dbDataLoaded[$dbDataToRef[$IDUp]];
                                                $dataTmp = $tableTmp->fetch();
                                                $obj["pp"]["value"] = $dataTmp->$fieldName;
                                            }

                                        }
                                    }


                                }
                            }
                        }

                        //set variables of object to send to object
                        $obj["pp"]["objData"]["tplID"] = $obj["tplID"];
                        $obj["pp"]["objData"]["tplCNTID"] = $obj["tplCNTID"];
                        $obj["pp"]["objData"]["tmplPluginName"] = $tmplPluginName;
                        $obj["pp"]["objData"]["tmplPluginObject"] = $tmplPluginObject;
                        $obj["pp"]["objData"]["isModify"] = $setModifyTemplate;

                        $this->appendKWebObject($IDUp, $obj["pn"], $obj["po"], $obj["pp"], $useTagKWebObjectActual);

                        $arrayObjInserted[] = $obj["id"];
                    }
                }

                //remove object inserted on template, from array
                unset($arrayObjToAppend[$IDUp]);

                //restart from begin of array
                reset($arrayObjToAppend);


            } else {
                //next array
                if (next($arrayObjToAppend) === false) {
                    $exitFromArray = true;

                    echo '<font color="" face="arial" size="3"><b>Template Manager</b> (ver ' . $this->ver . ') <b>Debug Mode...</b></font></br>';
                    echo "<b>ERRORS:</b> Objects not found<br>";
                    foreach ($arrayObjToAppend as $HTMLIDUp => $arrObj) {
                        foreach ($arrObj as $key => $objError) {

                            echo '<blockquote>';
                            echo '<font color="000000"><b>' . " - CONTENT HTML ID:</b> " . $HTMLIDUp . " <b>- DBTplContentID:</b> " . $objError["tplCNTID"] . "<br>" . '</b></font>';
                            echo '</blockquote><hr noshade color=dddddd size=1>';
                        }
                    }
                }


            }


        }


        //Replace Plugin [KPlugin:]
        $webObjectsManager = new \k_webmanager\objects\general\web_objects_manager\obj;
        $webObjectsManager->replacePluginObject(["html" => $this->getHTML(), "webCode" => $tmplWebCode]);
        $this->mergeObjectVariables($webObjectsManager);
        $this->setHTML($webObjectsManager->getHTML());


        return $this;

    }


    /**
     * Save Template Web Type
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *      *   keyID: id of template
     *      *   webCode: web code of template
     *      *   name: name of template
     *
     * @return array
     */

    public function saveTplWeb($ArrayVariable)
    {


        $ArrayVariable["tplType"] = "web";
        return $this->saveTpl($ArrayVariable);
    }

    /**
     * Save Template Panel Type
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *      *   keyID: id of template
     *      *   name: name of template
     *
     * @return array
     */
    public function saveTplPanel($ArrayVariable)
    {

        $ArrayVariable["tplType"] = "panel";
        $ArrayVariable["webCode"] = "";
        return $this->saveTpl($ArrayVariable);
    }


    /**
     * Save Template Data
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *      *   keyID: id of template (empty if we copy a template)
     *      *   copyID: id of template to be copied
     *      *   webCode: web code of template
     *      *   code: code of template
     *      *   name: name of template
     *      *   tplType: "web" or "panel"
     *      *   file: optional name of template file to be use
     *      *   include: json of file to be include Ex: {"plugin":["jquery","bootstrap"],"css":["\/theme.css"]}
     *      *   pluginName: pluginName of Template
     *      *   pluginObject: pluginObject of Template
     *
     * @return array
     */
    protected function saveTpl($ArrayVariable)
    {


        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $copyID = $this->K_COMMON->getVarArray($ArrayVariable, "copyID");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $code = $this->K_COMMON->getVarArray($ArrayVariable, "code");
        $name = $this->K_COMMON->getVarArray($ArrayVariable, "name");
        $tplType = $this->K_COMMON->getVarArray($ArrayVariable, "tplType");
        $file = $this->K_COMMON->getVarArray($ArrayVariable, "file");
        $include = $this->K_COMMON->getVarArray($ArrayVariable, "include");
        $pluginName = $this->K_COMMON->getVarArray($ArrayVariable, "pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $isNew = true;

        //check fields
        if ($webCode == "" && $keyID == "" && $tplType == "web")
            return ["success" => "false", "reason" => "webCodeNull", "message" => $this->dictionary["EmsgEmptyField"]];


        //check fields
        if ($name == "")
            return ["success" => "false", "reason" => "emptyFields", "message" => $this->dictionary["EmsgEmptyField"]];

        //convert Include JSON
        if (is_array($include))
        {
            $include = json_encode($include);
        }
        elseif ($include != "" && $copyID == "") {
            $includeArrORI = json_decode($include, true);

            //check if not empty because sometime $include = "[]"
            if (!empty($includeArrORI)) {
                $includeArrJSON = [];

                foreach ($includeArrORI as $k => $v) {
                    $t = $v["value"];
                    $f = $v["text"];
                    $includeArrJSON[$t][] = $f;
                }

                $include = json_encode($includeArrJSON);
            } else
                $include = "";

        }

        //set variable for query
        $arrayWhere = [];
        $arrayPut = [];
        $arrayPut["tpl_name"] = $name;
        $arrayPut["tpl_file"] = $file;


        $arrayPut["tpl_include"] = $include;

        if ($keyID == "") {
            $arrayPut["tpl_type"] = $tplType;
            $arrayPut["tpl_web_code"] = $webCode;
            $arrayPut["tpl_plugin_name"] = $pluginName;
            $arrayPut["tpl_plugin_object"] = $pluginObject;
            if ($code != "")
                $arrayPut["tpl_code"] = $code;

        }


        //check variable
        if ($keyID != "")
        {
            $arrayWhere["id_web_template"] = $keyID;
            $isNew = true;
        }

        //include table
        $tableTPL = $this->getTable("web_templates");

        //save
        $tableTPL->put($arrayPut, $arrayWhere);

        if ($keyID == "")
            $keyID = $tableTPL->getNewID();

        if ($copyID != "" && $keyID != "") {

            //include table
            $tableTPLCnt = $this->getTable("web_templates_content");
            $tableTPLCntDup = $this->getTable("web_templates_content");

            //load data template from
            $tableTPL->get(["*"], ["id_web_template" => $copyID]);
            $dataTPLFrom = $tableTPL->fetch();

            //Load Actual TPL Content
            $tableTPLCnt->get(["*"]
                , [
                    "tpo_web_code" => $dataTPLFrom->tpl_web_code
                    , "tpo_plugin_name" => $dataTPLFrom->tpl_plugin_name
                    , "tpo_plugin_object" => $dataTPLFrom->tpl_plugin_object
                    , "tpo_tpl_code" => $dataTPLFrom->tpl_code
                ]
            );

            //load data template copied (new template)
            $tableTPL->get(["*"], ["id_web_template" => $keyID]);
            $tableTPLNew = $tableTPL->fetch();
            $TplWebCode = $tableTPLNew->tpl_web_code;
            $TplPluginName = $tableTPLNew->tpl_plugin_name;
            $TplPluginObject = $tableTPLNew->tpl_plugin_object;
            $TplCode = $tableTPLNew->tpl_code;

            while ($dataTPLCnt = $tableTPLCnt->fetch()) {

                //Duplicate record template content
                $tableTPLCntDup->copy(
                    ["id_web_template_content" => $dataTPLCnt->id_web_template_content]
                    , []
                    , []
                    , [
                        "tpo_web_code" => $TplWebCode
                        , "tpo_plugin_name" => $TplPluginName
                        , "tpo_plugin_object" => $TplPluginObject
                        , "tpo_tpl_code" => $TplCode
                    ]
                );


            }


        }

        //Create dir on web site for this ktemplate
        if ($isNew)
        {
            $objFileManager = $this->K_COMMON->getPluginObject("k_filemanager","api");
            $objFileManager->mkDirStaticContentWeb(
                [
                    "webCode" => $webCode
                    ,"contentType" => "w"
                    ,"dir" => "ktemplate"
                    ,"newFolderName" => $pluginName
                ]
            );
        }


        return ["success" => "true", "keyID" => $keyID, "message" => $this->dictionary["msgSaveSuccess"]];

    }

    /**
     * Delete K-Template from web site
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *      *   pluginName: id of plugin
     *      *   webCode: web code of template
     *
     * @return array
     */
    public function deleteKTemplate($ArrayVariable)
    {

        //set variable
        $pluginName = $this->K_COMMON->getVarArray($ArrayVariable, "pluginName");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $targetDir = $this->K_COMMON->getPluginType($pluginName) . "/" . $pluginName;

        //include objects
        $apiFM = $this->K_COMMON->getPluginObject("k_filemanager", "api");
        $objDictionary = $this->K_COMMON->getPluginObject("k_dictionary", "objects\\general\\dictionary");

        //include table
        $tableTPL = $this->getTable("web_templates");
        $tablePGS = $this->getTable("web_pages");
        $tableURL = $this->getTable("urlrewrite");

        //remove template
        $tableTPL->delete(["tpl_plugin_name" => $pluginName, "tpl_web_code" => $webCode]);

        //remove pages
        $tablePGS->delete(["wpg_plugin_name" => $pluginName, "wpg_web_code" => $webCode]);

        //remove url
        $tableURL->delete(["urw_plugin_name" => $pluginName, "urw_web_code" => $webCode]);

        //Delete files of KTemplate on web folder
        $apiFM -> removeStaticContentWeb(
            [
                "targetDir" => $targetDir
                , "webCode" => $webCode

            ]
        );

        //remove dictionary of this ktemplate
        $objDictionary->delete(
            [
                "webCode" => $webCode,
                "pluginName" => $pluginName,
                "pluginObject" => "*"
            ]
        );




        return ["success" => "true"];

    }



    /**
     * Install K-Template on web site
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *      *   pluginName: id of plugin
     *      *   webCodeSource: web code of template Source
     *      *   webCodeTarget: web code of template Target
     *
     * @return array
     */
    public function installKTemplate($ArrayVariable)
    {

        $pluginName = $this->K_COMMON->getVarArray($ArrayVariable,"pluginName");
        $webCodeTarget = $this->K_COMMON->getVarArray($ArrayVariable,"webCodeTarget");
        $objPlugins = $this->K_COMMON->getPluginObject("k_common", "plugins");

        $objPlugins->importPlugin(
            [
                "plgID" => $pluginName
                , "typeUpdate" => "webtemplate"
                , "webCode" => $webCodeTarget
            ]
        );

        $objPlugins->importPlugin(
            [
                "plgID" => $pluginName
                , "typeUpdate" => "webpage"
                , "webCode" => $webCodeTarget
            ]
        );

        $objPlugins->importPlugin(
            [
                "plgID" => $pluginName
                , "typeUpdate" => "dictionary"
                , "webCode" => $webCodeTarget
            ]
        );

        $ArrayVariable["webCodeSource"] = "";
        return $this->copyKTemplateMediaFiles($ArrayVariable);
    }

    /**
     * Copy K-Template Files
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *      *   pluginName: id of plugin
     *      *   webCodeSource: web code of template Source
     *      *   webCodeTarget: web code of template Target
     *
     * @return array
     */
    public function copyKTemplateMediaFiles($ArrayVariable)
    {

        //set variable
        $pluginName = $this->K_COMMON->getVarArray($ArrayVariable, "pluginName");
        $webCodeSource = $this->K_COMMON->getVarArray($ArrayVariable, "webCodeSource");
        $webCodeTarget = $this->K_COMMON->getVarArray($ArrayVariable, "webCodeTarget");

        //include objects
        $apiFM = $this->K_COMMON->getPluginObject("k_filemanager", "api");

        //Move File of Template
        $pluginPath = $this->K_COMMON->getPathPluginObject(true,$pluginName);
        if ($webCodeTarget != "")
        {

            if ($webCodeSource == "")
            {
                $sourceDir = $pluginPath . "/" . $this->mediaFilesDir;
                $targetDir = $this->K_COMMON->getPluginType($pluginName) . "/" . $pluginName;


                $apiFM -> copyFolderLocalToStaticContentWeb(
                    [
                        "sourceDir" => $sourceDir
                        , "targetDir" => $targetDir
                        , "webCode" => $webCodeTarget
                        , "removeTargetDir" => "1"
                    ]
                );

            }
            else{
                $sourceDir = $this->K_COMMON->getPluginType($pluginName) . "/" . $pluginName;
                $targetDir = $this->K_COMMON->getPluginType($pluginName) . "/" . $pluginName;

                $apiFM -> duplicateFolderStaticContentWeb(
                    [
                        "sourceDir" => $sourceDir
                        , "targetDir" => $targetDir
                        , "webCode" => $webCodeSource
                        , "webCodeTarget" => $webCodeTarget
                        , "removeTargetDir" => "1"
                    ]
                );
            }


        }
        else
        {

            $sourceDir = $this->K_COMMON->getPluginType($pluginName) . "/" . $pluginName;
            $targetDir = $pluginPath . "/" . $this->mediaFilesDir;

            $apiFM -> copyFolderFromStaticContentWebToLocal(
                [
                    "sourceDir" => $sourceDir
                    , "targetDir" => $targetDir
                    , "webCode" => $webCodeSource
                    , "removeTargetDir" => "1"
                ]
            );

        }

        return ["success" => "true"];

    }

    /**
     * Copy K-Template
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *      *   pluginName: id of plugin
     *      *   webCodeSource: web code of template Source
     *      *   webCodeTarget: web code of template Target
     *
     * @return array
     */
    public function copyKTemplate($ArrayVariable)
    {

        //set variable
        $pluginName = $this->K_COMMON->getVarArray($ArrayVariable, "pluginName");
        $webCodeSource = $this->K_COMMON->getVarArray($ArrayVariable, "webCodeSource");
        $webCodeTarget = $this->K_COMMON->getVarArray($ArrayVariable, "webCodeTarget");

        //include table
        $tableWebTemplate = $this->getTable("web_templates");
        $tableWebPage = $this->getTable("web_pages");
        $tableUrlRewrite = $this->getTable("urlrewrite");


        //Remove all templates of this plugin on Target
        $tableWebTemplate->delete(["tpl_web_code" => $webCodeTarget, "tpl_plugin_name" => $pluginName]);

        //Remove all pages of this plugin on Target
        $tableWebPage->delete(["wpg_web_code" => $webCodeTarget, "wpg_plugin_name" => $pluginName]);

        //Remove all url rewrite of this plugin on Target
        $tableUrlRewrite->delete(["urw_web_code" => $webCodeTarget, "urw_plugin_name" => $pluginName]);

        //load data templates
        $tableWebTemplate->get("*", ["tpl_plugin_name" => $pluginName, "tpl_web_code" => $webCodeSource]);

        while ($data = $tableWebTemplate->fetch()) {
            $keyID = $data->id_web_template;
            $tplPluginName = $data->tpl_plugin_name;
            $tplPluginObject = $data->tpl_plugin_object;
            $tplCode = $data->tpl_code;
            $tplName = $data->tpl_name;
            $tplType = $data->tpl_type;
            $tplFile = $data->tpl_file;
            $tplInclude = $data->tpl_include;

            $res = $this->saveTpl(
                [
                    "copyID" => $keyID,
                    "webCode" => $webCodeTarget,
                    "code" => $tplCode,
                    "name" => $tplName,
                    "tplType" => $tplType,
                    "file" => $tplFile,
                    "include" => $tplInclude,
                    "pluginName" => $tplPluginName,
                    "pluginObject" => $tplPluginObject

                ]
            );

            if ($res["success"] == "true")
            {
                $newKeyID = $res["keyID"];





                //Get Page ID source
                $tableWebPage->get("*", ["wpg_tmpl_id" => $keyID]);
                $dataPage = $tableWebPage->fetch();
                if ($dataPage)
                {
                    //get Page ID
                    $pageSourceID = $dataPage->id_web_page;

                    //copy page of template
                    $tableWebPage->copy(
                        ["wpg_tmpl_id" => $keyID]
                        , []
                        , []
                        , [
                            "wpg_web_code" => $webCodeTarget
                            ,"wpg_tmpl_id" => $newKeyID

                        ]
                    );

                    //Copy URL of Page
                    $pageNewID = $tableWebPage->getNewID();
                    $tableUrlRewrite->copy(
                        ["urw_page_id" => $pageSourceID, "urw_plugin_name" => $tplPluginName]
                        , []
                        , []
                        , [
                            "urw_web_code" => $webCodeTarget
                            ,"urw_refcode" => $pageNewID
                            ,"urw_page_id" => $pageNewID

                        ]
                    );
                }


            }
        }

        return ["success" => "true"];

    }

    /**
     * Delete Template Data
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *      *   keyID: id of template
     *      *   webCode: web code of template
     *      *   name: name of template
     *
     * @return array
     */
    public function deleteTpl($ArrayVariable)
    {

        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");

        //include table
        $table = $this->getTable("web_templates");

        //save
        $table->delete(["id_web_template" => $keyID]);

        return ["success" => "true"];

    }


    /**
     * Save New object on template
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *      *   idWebObj: id of web object to add
     *      *   tplID: id of template
     *      *   tplCntID: id of template content
     *      *   tplCntIDUP: id of template content up (if we need to append)
     *      *   webObjectID: HTML ID of object
     *
     * @return array
     */
    public function saveNewObject($ArrayVariable)
    {

        //set variable
        $idWebObj = $this->K_COMMON->getVarArray($ArrayVariable, "idWebObj");
        $webObjPCode = $this->K_COMMON->getVarArray($ArrayVariable, "webObjPCode");
        $tplID = $this->K_COMMON->getVarArray($ArrayVariable, "tplID");
        $tplCntID = $this->K_COMMON->getVarArray($ArrayVariable, "tplCntID");
        $tplCntIDUP = $this->K_COMMON->getVarArray($ArrayVariable, "tplCntIDUP");
        $webObjectID = $this->K_COMMON->getVarArray($ArrayVariable, "webObjectID");
        $webObjectClass = $this->K_COMMON->getVarArray($ArrayVariable, "webObjectClass");
        $webObjectStyle = $this->K_COMMON->getVarArray($ArrayVariable, "webObjectStyle");
        $paddingT = $this->K_COMMON->getVarArray($ArrayVariable, "paddingT");
        $paddingR = $this->K_COMMON->getVarArray($ArrayVariable, "paddingR");
        $paddingB = $this->K_COMMON->getVarArray($ArrayVariable, "paddingB");
        $paddingL = $this->K_COMMON->getVarArray($ArrayVariable, "paddingL");
        $webObjectBGColor = $this->K_COMMON->getVarArray($ArrayVariable, "webObjectBGColor");
        $webObjectBGImage = $this->K_COMMON->getVarArray($ArrayVariable, "webObjectBGImage");
        $webObjectTextColor = $this->K_COMMON->getVarArray($ArrayVariable, "webObjectTextColor");
        $dataPluginName = $this->K_COMMON->getVarArray($ArrayVariable, "dataPluginName");
        $dataPluginData = $this->K_COMMON->getVarArray($ArrayVariable, "dataPluginData");
        $dataLoadFilterGET = $this->K_COMMON->getVarArray($ArrayVariable, "dataLoadFilterGET");
        $dataFieldName = $this->K_COMMON->getVarArray($ArrayVariable, "dataFieldName");
        $webObjectParameters = [];
        $webObjectPName = "";
        $webObjectPObject = "";
        $tplObjIDUp = "";
        $changedHTMLID = false;
        $tplCntOrder = 0;

        //include table and object
        $objApiWebManager = $this->K_COMMON->getPluginObject("k_webmanager", "api");
        $tableWebTemplate = $this->getTable("web_templates");
        $tableWebTplContent = $this->getTable("web_templates_content");

        //Load tpl id up (if in add object)
        if ($tplCntIDUP != "" && $tplCntID == "") {
            $tableWebTplContent->get("*", ["id_web_template_content" => $tplCntIDUP]);
            $rowObj = $tableWebTplContent->fetch();
            if ($rowObj) {

                //set variable
                $tplObjIDUp = $rowObj->tpo_tpl_id_obj;

            } else
                return ["success" => "false", "reason" => "notFoundUp", "message" => "Object UP Not Found"];
        }


        //Load tpl content (if in modify object)
        if ($tplCntID != "") {
            $tableWebTplContent->get("*", ["id_web_template_content" => $tplCntID]);
            $rowObj = $tableWebTplContent->fetch();
            if ($rowObj) {
                //set variable
                $webObjectIDActual = $rowObj->tpo_tpl_id_obj;
                $tplObjIDUp = $rowObj->tpo_tpl_id_obj_up;
                $tplCntOrder = $rowObj->tpo_order;
                $webObjectPName = $rowObj->tpo_wo_plugin_name;
                $webObjectPObject = $rowObj->tpo_wo_plugin_object;


                //check if is changed HTML Obj ID
                if ($webObjectID != $webObjectIDActual)
                    $changedHTMLID = true;

            }
        }

        //load data
        $tableWebTemplate->get(["tpl_web_code", "tpl_plugin_name", "tpl_plugin_object", "tpl_code"], ["id_web_template" => $tplID]);
        $rowObj = $tableWebTemplate->fetch();
        if ($rowObj) {
            //set variable
            $tplWebCode = $rowObj->tpl_web_code;
            $tplPluginName = $rowObj->tpl_plugin_name;
            $tplPluginObject = $rowObj->tpl_plugin_object;
            $tplCode = $rowObj->tpl_code;


            $dataToSaveTplCnt = [
                "tpo_web_code" => $tplWebCode
                , "tpo_plugin_name" => $tplPluginName
                , "tpo_plugin_object" => $tplPluginObject
                , "tpo_tpl_code" => $tplCode
                , "tpo_tpl_id_obj_up" => $tplObjIDUp
            ];


            //We are in ADD WEB Object (search for order)
            if ($tplCntID == "") {

                $tplCntCondiz =
                    [
                        "tpo_web_code" => $tplWebCode
                        , "tpo_plugin_name" => $tplPluginName
                        , "tpo_plugin_object" => $tplPluginObject
                        , "tpo_tpl_code" => $tplCode
                        , "tpo_tpl_id_obj_up" => $tplObjIDUp
                    ];

                $tableWebTplContent->get(
                    [

                        "objOrder" => "max(tpo_order)"
                    ]
                    ,
                    $tplCntCondiz
                );
                $rowObjOrder = $tableWebTplContent->fetch();
                if ($rowObjOrder)
                    $tplCntOrder = $rowObjOrder->objOrder + 1;

                $dataToSaveTplCnt["tpo_order"] = $tplCntOrder;

                //get Object Data
                $webObjInfo = $objApiWebManager->getWebObject(
                    [
                        "keyID" => $idWebObj,
                        "pCode" => $webObjPCode
                    ]
                );

                $parametersObject = $webObjInfo["parametersPCode"];
                $webObjectPName = $webObjInfo["pluginName"];
                $webObjectPObject = $webObjInfo["pluginObject"];
                if ($webObjPCode != "")
                    $parametersObject["webObjPCode"] = $webObjPCode;
                $webObjectParameters = json_encode($parametersObject);

                $dataToSaveTplCnt["tpo_wo_plugin_name"] = $webObjectPName;
                $dataToSaveTplCnt["tpo_wo_plugin_object"] = $webObjectPObject;
                $dataToSaveTplCnt["tpo_wo_plugin_object_pcode"] = $webObjPCode;

                $dataToSaveTplCnt["tpo_wo_parameters"] = $webObjectParameters;


            }

            //create web object ID
            if ($webObjectID == "")
                $webObjectID = str_replace("\\","_",$webObjectPObject) . "_" . uniqid();

            $dataToSaveTplCnt["tpo_tpl_id_obj"] = $webObjectID;


            //If parameters empty, we are on update settings
            if (empty($webObjectParameters))
            {
                $obj = $this->K_COMMON->getPluginObject($webObjectPName, $webObjectPObject);
                if ($obj->methodExist('parameterizationSaveData'))
                {
                    $webObjectParameters = $obj->parameterizationSaveData($ArrayVariable);
                    $webObjectParameters = json_encode($webObjectParameters);

                    $dataToSaveTplCnt["tpo_wo_parameters"] = $webObjectParameters;
                }

            }

            //save record in web template content
            if ($tplCntID != "")
                $tplCntCondiz = ["id_web_template_content" => $tplCntID];
            else
                $tplCntCondiz = [];

            //create array for parameters of template content
            $tplCntParameters = [
                "style" => $webObjectStyle
                , "class" => $webObjectClass
                , "padding-top" => $paddingT
                , "padding-right" => $paddingR
                , "padding-bottom" => $paddingB
                , "padding-left" => $paddingL
                , "background-color" => $webObjectBGColor
                , "background-image" => $webObjectBGImage
                , "color" => $webObjectTextColor

            ];

            //create array for parameters of template content
            $tplDataParameters =
                [
                    "load" =>
                        [
                            "pluginName" => $dataPluginName
                            , "pluginData" => $dataPluginData
                            , "filterGET" => $dataLoadFilterGET //filter used with url "get" parameter
                        ],
                    "get" => [
                        "fieldName" => $dataFieldName
                    ]
                ];


            $dataToSaveTplCnt["tpo_parameters"] = json_encode($tplCntParameters);
            $dataToSaveTplCnt["tpo_data"] = json_encode($tplDataParameters);
            $tableWebTplContent->put($dataToSaveTplCnt, $tplCntCondiz);

            $keyID = $tableWebTplContent->getNewID();

            //check if is changed HTML Obj ID, and change in other objects
            if ($changedHTMLID) {
                $tplCntCondiz =
                    [
                        "tpo_web_code" => $tplWebCode
                        , "tpo_plugin_name" => $tplPluginName
                        , "tpo_plugin_object" => $tplPluginObject
                        , "tpo_tpl_code" => $tplCode
                        , "tpo_tpl_id_obj_up" => $webObjectIDActual
                    ];
                $tableWebTplContent->put(
                    [

                        "tpo_tpl_id_obj_up" => $webObjectID
                    ]
                    ,
                    $tplCntCondiz
                );
            }

            return ["success" => "true", "message" => "", "tplID" => $tplID, "tplCntID" => $keyID];

        } else
            return ["success" => "false", "reason" => "errorSecurity", "message" => "Error Security"];
    }

    /**
     * Move Web object up or down on template
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *      *   tplCntID: id of template content
     *      *   dir: direction of move "up" or "down"
     *
     * @return array
     */
    public function moveWebObject($ArrayVariable)
    {
        //set variable
        $tplCntID = $this->K_COMMON->getVarArray($ArrayVariable, "tplCntID");
        $moveDir = $this->K_COMMON->getVarArray($ArrayVariable, "moveDir");

        //include table and object
        $tableWebTplContent = $this->getTable("web_templates_content");

        //get Actual Object
        if ($tplCntID != "") {
            $tableWebTplContent->get(["*"], ["id_web_template_content" => $tplCntID]);
            $rowObj = $tableWebTplContent->fetch();
            if ($rowObj) {

                $actulOrder = $rowObj->tpo_order;

                //get up/down object
                $tplCntCondiz =
                    [
                        "tpo_web_code" => $rowObj->tpo_web_code
                        , "tpo_plugin_name" => $rowObj->tpo_plugin_name
                        , "tpo_plugin_object" => $rowObj->tpo_plugin_object
                        , "tpo_tpl_code" => $rowObj->tpo_tpl_code
                    ];

                if ($moveDir == "up") {
                    $tplCntCondiz[] = "tpo_order < " . $rowObj->tpo_order;
                    $qryOrder = ["tpo_order DESC"];
                } else {
                    $tplCntCondiz[] = "tpo_order > " . $rowObj->tpo_order;
                    $qryOrder = ["tpo_order ASC"];
                }

                //load up/down object
                $tableWebTplContent->get("id_web_template_content,tpo_order", $tplCntCondiz, "", "", $qryOrder, "1");
                $rowObjSearched = $tableWebTplContent->fetch();

                //move it
                if ($rowObjSearched) {
                    //Update Record to Move
                    $tableWebTplContent->put(["tpo_order" => $rowObjSearched->tpo_order], ["id_web_template_content" => $tplCntID]);

                    //Update Searched Record
                    $tableWebTplContent->put(["tpo_order" => $actulOrder], ["id_web_template_content" => $rowObjSearched->id_web_template_content]);
                }

            }
        }

        //return
        return ["success" => "true"];

    }

    /**
     * Delete Object from template
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *      *   tplCntID: id of web template content
     *
     * @return array
     */
    public function deleteObj($ArrayVariable)
    {

        //set variable
        $tplCntID = $this->K_COMMON->getVarArray($ArrayVariable, "tplCntID");

        //check variable
        if ($tplCntID == "")
            return ["success" => "false", "reason" => "errorSecurity", "message" => "Error Security"];

        //include table
        $tableContent = $this->getTable("web_templates_content");

        //load tplObjID
        $tableContent->get(["tpo_web_code", "tpo_tpl_code", "tpo_tpl_id_obj", "tpo_plugin_name", "tpo_plugin_object"], ["id_web_template_content" => $tplCntID]);
        $rowObj = $tableContent->fetch();
        if ($rowObj) {
            $webCode = $rowObj->tpo_web_code;
            $tplCode = $rowObj->tpo_tpl_code;
            $tplIDObj = $rowObj->tpo_tpl_id_obj;
            $pluginName = $rowObj->tpo_plugin_name;
            $pluginObj = $rowObj->tpo_plugin_object;


            //delete template content (automatically also web object used)
            $tableContent->delete(["id_web_template_content" => $tplCntID]);


            //delete recursively all child of container
            $tableContent->get(
                [
                    "id_web_template_content"
                ]
                , [
                    "tpo_web_code" => $webCode
                    , "tpo_plugin_name" => $pluginName
                    , "tpo_plugin_object" => $pluginObj
                    , "tpo_tpl_code" => $tplCode
                    , "tpo_tpl_id_obj_up" => $tplIDObj
                ]
            );
            while ($rowObj = $tableContent->fetch()) {
                $tplCntIDChild = $rowObj->id_web_template_content;
                $this->deleteObj(["tplCntID" => $tplCntIDChild]);
            }

            return ["success" => "true"];


        } else
            return ["success" => "false", "reason" => "errorSecurity", "message" => "Error Security"];
    }


}

?>