<?
/**
 * KIXIXI WEB MODAL
 * create a web modal object
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_webmanager
 * @subpackage objects\web_modal
 * @since 1.00
 */
namespace k_webmanager\objects\general\web_modal;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * Type of Modal
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $modalType = "";

    /**
     * Max Modal Width
     * if empty use default value
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $modalWidth = "";
    /**
     * Title of Modal
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $modalTitle = "";

    /**
     * Body of Modal
     *
     * @since 1.00
     * @access public
     * @var web_object
     */
    public $modalBody = "";

    /**
     * Buttons of Modal
     *
     * @since 1.00
     * @access public
     * @var web_object
     */
    public $modalButtons = "";

    /**
     * Footer of Modal
     *
     * @since 1.00
     * @access public
     * @var web_object
     */
    public $modalFooter = "";

    /**
     * ID of Modal
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $idModal = "";

    /**
     * Construct
     *
     */
    public function __construct($AV) {
        parent::__construct();
        $this -> dictionary = $this->getDictionary();

    }

    /**
     * create New Modal
     *
     * @param array $av
     *
     * *  Parameters: <br>
     *
     * *    id    : modal ID
     * *    title    : modal Title
     * *    type    : "normal", "confirm"
     *
     *
     */
    public function newModal($av)
    {

        //get variables
        $this->idModal = $this->K_COMMON->getVarArray($av,"id");
        $this->modalTitle = $this->K_COMMON->getVarArray($av,"title");
        $this->modalType = $this->K_COMMON->getVarArray($av,"type");
        $this->modalWidth = $this->K_COMMON->getVarArray($av,"width");
        $this->modalButtons = $this->K_COMMON->getVarArray($av,"buttons");

        //set modal ID
        if ($this->idModal == "")
            $this->idModal = "modalID";

        //load modal template
        $template = $this->getTemplateLayout("modal");

        //Assign ID Modal
        $arrModal = ["id" => $this->idModal];
        if ($this->modalWidth != "")
            $arrModal["style"] = "max-width: " . $this->modalWidth;
        $template->assign_block_vars("MODAL", $arrModal);

        //Assign Title Modal
        $modalHeader_var = array();
        $modalHeader_var["abort"] = $this->dictionary["close"];
        $modalHeader_var["title"] = $this->modalTitle;
        $template->assign_block_vars("MODAL.HEADER", $modalHeader_var);

        //Get HTML Modal from template and append to object
        $this->appendHTML("",$template->pparse("body"));

        //create modal body
        $this->modalBody = $this->idModal."Body";

        //create modal footer
        $this->appendRow($this->idModal."Footer");
        $this->modalFooter = $this->appendColumn($this->lastRow, ["nCol" => 12]);

        //set default footer
        switch ($this->modalType)
        {
            case "custom":

                //Include modules
                $objInput = $this->K_COMMON->getPluginObject("k_htmlcomponents", "input");

                $btnNum = 0;
                if (is_array($this->modalButtons))
                    foreach ($this->modalButtons as $button)
                    {


                        $btnNum++;

                        //get button data
                        $button["type"] = "btn";

                        //add button
                        $this->K_COMMON->concatVarArray($button, "class", " btnModal" . $btnNum);

                        $this->appendInput($this->modalFooter, $button);


                       /* $btnNum++;

                        //get button data
                        $button["type"] = "btn";

                        //add button
                        $this->K_COMMON->concatVarArray($button, "class", " btnModal" . $btnNum);
                        $objInput->getWO($button);

                        $this->appendObject($this->modalFooter,$objInput);

                        $this->mergeObjectVariables($objInput);
                        */

                    }

                break;
            case "normal":
                    $this->appendHTML($this->modalFooter,'<input class="btn btn-secondary" data-dismiss="modal" type="button" name="close" value="'.$this->dictionary["close"].'"/>');
                    $this->addFooterJS('
                        <script>
                            $("#'.$this->idModal.' input[name=\"close\"]").click(function(){$("#'.$this->idModal.'").modal("hide");});
                        </script>
                    ');
                break;
            case "confirm":
                    $this->modalFooter = $this->appendColumn($this->lastRow, ["nCol" => 12]);
                    $this->appendHTML($this->modalFooter,'<input class="btn btn-secondary" data-dismiss="modal" type="button" name="no" value="'.$this->dictionary["no"].'"/>');
                    $this->appendHTML($this->modalFooter,'<input class="btn btn-primary" type="button" name="yes" value="'.$this->dictionary["yes"].'" />');
                    $this->addFooterJS('
                        <script>
                            $("#'.$this->idModal.' input[name=\"no\"]").click(function(){$("#'.$this->idModal.'").modal("hide");});
                        </script>
                    ');
                break;
            case "confirmDeleteCheck":

                $fieldConfirm= $this->K_COMMON->getVarArray($av,"fieldConfirm");

                $this->modalFooter = $this->appendColumn($this->lastRow, ["nCol" => 12]);
                $this->appendHTML($this->modalFooter,'<input class="btn btn-secondary btnNO" data-dismiss="modal" type="button" name="no" value="'.$this->dictionary["no"].'"/>');
                $this->appendHTML($this->modalFooter,'<input class="btn btn-danger btnYES" type="button" name="yes" value="'.$this->dictionary["yes"].'" disabled="disabled" />');
                $this->addFooterJS('
                        <script>
                            $("#'.$this->idModal.' input[name=\"no\"]").click(function(){$("#'.$this->idModal.'").modal("hide");});
                             $("#'.$this->idModal.' #'.$fieldConfirm.'").keyup(function(){
                                
                                if($(this).val() == "DELETE")
                                    {
                                    
                                    $("#' . $this->idModal . ' .btnYES").removeAttr("disabled");
                                    }            
                                else
                                    $("#'.$this->idModal.' .btnYES").attr("disabled","disabled");
                            })
                        </script>
                    ');
                break;
        }





    }

    /**
     * create New Modal by Javascript
     *
     * @param array $av
     *
     * *  Parameters: <br>
     *
     * *        idModal: id of modal
     * *        kcp: kcp of ajax
     * *        kco: kco of ajax
     * *        kca: kca of ajax
     * *        parameters: parameters for ajax
     * *        parametersRef: parameters for ajax to get from page by ID
     * *        onClose: function to call on close modal
     * *        triggerID: ID of button trigger for open modal
     * *        triggerEvent: event for trigger
     *
     *
     */
    public function newModalJS($av)
    {

        //get variables

        $idModal = $this->K_COMMON->getVarArray($av, "idModal");
        $kcp = $this->K_COMMON->getVarArray($av, "kcp");
        $kco = $this->K_COMMON->getVarArray($av, "kco");
        $kca = $this->K_COMMON->getVarArray($av, "kca");
        $onClose = $this->K_COMMON->getVarArray($av, "onClose");
        $parameters = $this->K_COMMON->getVarArray($av, "parameters");
        $parametersRef = $this->K_COMMON->getVarArray($av, "parametersRef");
        $triggerID = $this->K_COMMON->getVarArray($av, "triggerID");
        $triggerEvent = $this->K_COMMON->getVarArray($av, "triggerEvent");
        $ajaxPar = "";
        $ajaxParRef = "";

        //replace "\" on kco with double "\" for javascript
        $kco = str_replace("\\", "\\\\", $kco);

        if (is_array($parameters))
        {
            foreach ($parameters as $key => $val)
            {
                $ajaxPar .= ($ajaxPar == "" ? "" : "&") . $key . "=" . $val;
            }
        }

        //Check for parameters Ref
        if (is_array($parametersRef))
        {
            foreach ($parametersRef as $key => $val)
            {
                $ajaxParRef .= "+\"" . ($ajaxPar == "" && $ajaxParRef == "" ? "" : "&") .  $key . "=\" + $(\"#".$val."\").val()";
            }
        }

        $scriptModal = '<script>'
            .$idModal.'OpenModal = function(options){
                                                waitingDialog.show("Loading...", {dialogSize: "sm", progressType: "warning"});
                                                
                                                
                                                var options = options || {};
                                                var dataAjax = {};
                                                var extraParam = "";
                                                dataAjax.idModal = "' . $idModal . '";
                                                if (typeof (options.extraParam) == "object")
                                                    {
                                                    Object.assign(dataAjax, options.extraParam);
                                                    }
                                                
                                                
                                                $.ajax({
                                                      url: KXJS.buildAPICallUrl("KXCJS","' . $kcp . '","' . $kco . '","' . $kca . '","' . $ajaxPar . '"'.$ajaxParRef.'),
                                                    data: dataAjax
                                                }).done(function(data) {
                                                    r = $.parseJSON(data);
                                                    waitingDialog.hide();
                                                    if(r.success == "true"){
                                                        $(r.html).appendTo("body");
                                                        $("#' . $idModal . '").modal("show");
                                                        ' .  ($onClose != "" ? $onClose . '();' : '') . '
                                                        $("#' . $idModal . '").on("hidden.bs.modal", function (e) {
                                                            $("#' . $idModal . '").remove();
                                                        });
                                                        }
                                                    else
                                                        alert("Error");
                                                });
                                            }     
                                            
               '
                . ($triggerEvent != "" ?

                '$(document).ready(function(){
            
                                   $("#' . $triggerID . '").'.$triggerEvent.'('.$idModal.'OpenModal);
                        });' : '')
        .'
						</script>';

        $this->addFooterJS($scriptModal);






    }






}

?>