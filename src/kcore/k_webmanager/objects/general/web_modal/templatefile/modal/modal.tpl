
<!-- BEGIN MODAL -->
<div class="modal fade" id="{MODAL.id}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="{MODAL.style}">
        <div class="modal-content">
            <!-- BEGIN HEADER -->
            <div class="modal-header" id="{MODAL.id}Header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                    <span class="sr-only">{MODAL.HEADER.abort}</span>
                </button>
                <h4 class="modal-title" id="{MODAL.id}Title">{MODAL.HEADER.title}</h4>
            </div>
            <!-- END HEADER -->

            <div class="modal-body" id="{MODAL.id}Body">
            </div>

            <div class="modal-footer" id="{MODAL.id}Footer">
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->




