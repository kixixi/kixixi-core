<?
/**
 * KIXIXI WEB OBJECT
 * This object is used as base of each web object created
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_webmanager
 * @subpackage objects\web_object
 * @since 1.00
 */
namespace k_webmanager\objects\general\web_object;
class obj extends \k_webmanager\objects\general\web_object_base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * Type of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $oType = "wo";

    /**
     * get this object (default)
     *
     *
     * @return object this object
     */
    public function get()
    {
        return $this;
    }

}

?>