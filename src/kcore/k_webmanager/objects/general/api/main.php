<?
/**
 * KIXIXI k_usefuldata (api)
 * Create a panel web object for manage level authorization
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_usefuldata
 * @subpackage objects\api
 * @since 1.00
 */
namespace k_webmanager\objects\general\api;
class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Example of get custom field for authorization form
     * This function is used as Example for Authorization Get Custom Field Combo Value
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    webCode: web code of webto get url. if "" get current
     *
     * @return string
     */

    public function getAuthCustomField($arrayVariable = []) {


        // get variables
        $OM = $this->K_COMMON->getVarArray($arrayVariable,"OM");
        $codeRef = $this->K_COMMON->getVarArray($arrayVariable,"codeRef");
        $code = $this->K_COMMON->getVarArray($arrayVariable,"code");
        $where = [];

        switch ($codeRef) {
            case "websites":
                if ($code != "")
                    $where["web_code"] = $code;
                $table = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_sites");
                $table->get(["value" => "web_code", "text" => "web_name"],$where);
                return $table->getResults($OM);
                break;
        }



    }

    /**
     * get the URL Base of current web, or by webCode
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    webCode: web code of webto get url. if "" get current
     *
     * @return string
     */
    public function getWebURL($arrayVariable = []) {

        //set variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariable,"webCode");

        $table = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_sites");

        $table->get(["web_code", "web_name"],["web_code" => $webCode]);

        $data =  $table->fetch();

        if ($data)
        {
            $url = $data->web_name;
            $url = "http://".$url;
        }
        else
            $url = "";

        return $url;
    }

    /**
     * get the Web Name of current web, or by webCode
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    webCode: web code of webto get url. if "" get current
     *
     * @return string
     */
    public function getWebName ($arrayVariable = []) {

        //set variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariable,"webCode");

        $table = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_sites");

        $table->get(["web_code", "web_name"],["web_code" => $webCode]);

        $data =  $table->fetch();

        if ($data)
            $name = $data->web_name;
        else
            $name = "";

        return $name;
    }

    /**
     * get web objects
     * from "web_objects" table
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    OM: Output model ("array", "table", "xml", "json") see getResults function of basetable
     *
     * @return array|xml|json|table
     */
    public function getWebObjects($arrayVariable = [])
    {

        //get variable
        $OM = $this->K_COMMON->getVarArray($arrayVariable, "OM");

        //include table
        $webObjects = $this->getTable("web_objects");

        $webObjects->get(["value" => "id_web_objects", "text" => "wob_name"]);
        return $webObjects->getResults($OM);
    }

    /**
     * get web object
     * from "web_objects" table
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *   *    keyID : id of web object (optional if you use pluginName, and pluginObject)
     *   *    pluginName : name of plugin
     *   *    pluginObject : name of object
     *   *    pCode : optional Prototype Code (if valued return also array of this prototype in "parametersPCode" )
     *
     * @return array
     */
    public function getWebObject($arrayVariable = [])
    {

        //get variable
        $keyID = $this->K_COMMON->getVarArray($arrayVariable, "keyID");
        $pluginName = $this->K_COMMON->getVarArray($arrayVariable, "pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($arrayVariable, "pluginObject");
        $pCode = $this->K_COMMON->getVarArray($arrayVariable, "pCode");

        //include table
        $webObjects = $this->getTable("web_objects");


        //set condition
        if ($keyID != "")
            $where = ["id_web_objects" => $keyID];
        else
            $where = ["wob_plugin_name" => $pluginName, "wob_plugin_object" => $pluginObject];

        $webObjects->get("*", $where);

        $data = $webObjects->fetch();
        if ($data)
            {
                $pluginName = $data->wob_plugin_name;
                $pluginObject = $data->wob_plugin_object;
                $parametersPCode = [];
                if ($pCode != ""){
                    $wobPrototypesJSON = $data->wob_prototypes;
                    if (!empty($wobPrototypesJSON)) {
                        $wobPrototypes = (array)json_decode($wobPrototypesJSON, true);
                        $parameters = $this->K_COMMON->getVarArray($wobPrototypes, $pCode);
                        if (is_array($parameters))
                            $parametersPCode = $parameters["parameters"];
                    }

                }

                $res = [
                    "pluginName" => $pluginName,
                    "pluginObject" => $pluginObject,
                    "isContainer" => $data->wob_container,
                    "type" => $data->wob_type,
                    "prototypes" => $data->wob_prototypes,
                    "parametersPCode" => $parametersPCode

                ];
            }
        else
            $res = [];

        return $res;
    }

    /**
     * get templates list
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    webCode: web Code to get templates
     *    *    OM: Output model ("array", "table", "xml", "json") see getResults function of basetable
     *
     * @return array|xml|json|table
     */
    public function getTemplatesList($arrayVariable = array())
    {

        //set variable
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");
        $pluginName = $this->K_COMMON->getVarArray($arrayVariable, "pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($arrayVariable, "pluginObject");
        $condiz = [];
        $OM = $this->K_COMMON->getVarArray($arrayVariable, "OM");

        //check variable
        $condiz["tpl_web_code"] = ["in (%WHERE%)" => [$webCode]];
        $where["tpl_type"] = "web";
        if ($pluginName != "")
            $condiz["tpl_plugin_name"] = $pluginName;
        if ($pluginObject != "")
            $condiz["tpl_plugin_object"] = $pluginObject;
        $table = $this->getTable("web_templates");
     

        $table->getByQuery("web_templates_combo", [], $condiz);

        return $table->getResults($OM);
    }


    /**
     * get object used
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    webCode: web Code to get templates
     *    *    OM: Output model ("array", "table", "xml", "json") see getResults function of basetable
     *
     * @return array|xml|json|table
     */
    public function getObjUsed($arrayVariable = array())
    {

        //set variable
        $id = $this->K_COMMON->getVarArray($arrayVariable, "keyID");
        $condiz = [];
        $OM = $this->K_COMMON->getVarArray($arrayVariable, "OM");

        //check variable
        if ($id != "")
            $condiz["id_web_template_content"] = $id;
        else
            $condiz["id_web_template_content"] = "0"; //return empty

        $table = $this->getTable("web_templates_content");

        $table->get([], $condiz);

        return $table->getResults($OM);
    }

    /**
     * Format HTML For be saved on DB
     * Replace the URL with dynamic URL (example for images)
     *
     * @param array $AV array of variables
     * The following directives can be used in the array variables:<br>
     *    *    webCode: web Code
     *    *    html: html to be managed
     *
     * @return string
     */
    public function formatHtmlForSave($AV)
    {
        $webCode = $this->K_COMMON->getVarArray($AV, "webCode");
        $html = $this->K_COMMON->getVarArray($AV, "html");

        $urlsite = $this->K_COMMON->getStaticContentURLWeb($webCode);

        $html = str_replace($urlsite, "!KPlugin:k_webmanager.weburl!", $html);
        $html = str_replace("!KP:", "!KPlugin:", $html);

        return $html;
    }

    /**
     * Format HTML Loaded from DB
     * Replace the dynamic URL on static URL (example for images)
     *
     * @param array $AV array of variables
     * The following directives can be used in the array variables:<br>
     *    *    webCode: web Code
     *    *    html: html to be managed
     *
     * @return string
     */
    public function formatHtmlLoaded($AV)
    {
        $webCode = $this->K_COMMON->getVarArray($AV, "webCode");
        $html = $this->K_COMMON->getVarArray($AV, "html");

        $urlsite = $this->K_COMMON->getStaticContentURLWeb($webCode);

        $html = str_replace("!KPlugin:k_webmanager.weburl!", $urlsite, $html);
        $html = str_replace("!KPlugin:", "!KP:", $html);

        return $html;
    }



}
?>

