<?
namespace k_webmanager\objects\general\dom;
/**
 * KIXIXI Object DOM
 *
 * This object create a DOM object and allow to append/insert html.
 *  *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package k_webmanager
 * @subpackage object_dom
 * @since 1.00
 */
class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * DOM we are building
     *
     * @since 1.00
     * @access public
     * @var DOMDocument
     */
    public $dom;

    /**
     * ROOT Node of document
     *
     * @since 1.00
     * @access public
     * @var DOMDocument
     */
    public $root;

    /**
     * Constructor - initialize the DOMDocument
     *
     * @since 1.00
     *
     * @param array $AV array with parameters
     * As parameter accept:
     * * html: the HTML to insert into the DOM
     */
    public function __construct($AV = array())
    {

        //call parent construct
        parent::__construct();

        $this -> newDom($AV);

    }

    /**
     * Create New Dom Document
     *
     * @since 1.00
     *
     * @param array $AV array with parameters
     * As parameter accept:
     * * html: the HTML to insert into the DOM
     */
    public function newDom($AV = array())
    {

        //get html variable
        $html = $this->K_COMMON->getVarArray($AV, "html");

        //create DOM Document
        $this->dom = new \DOMdocument();
        $this->dom->validateOnParse = true;



        //create a first element on the DOM
        $element = $this->dom->createElement("tagToRemove");
        $this->dom->appendChild($element);

        //get Root of DOM
        $this->root = $this->dom->getElementsByTagName("tagToRemove")->item(0);


        //load html from variable
        if ($html != "") {


            //$node = $this->getNodeToImport($html);

            // And then append it to the "<root>" node
            //$this->root->appendChild($node);

            $this->appendHTMLToImport($this->root,$html);

        }



    }

    /**
     * Get an element from the DOM
     * It work best that same function of DOMDocument
     *
     * @since 1.00
     *
     * @param string $id id of DOM Element
     *
     * @return DOM Node
     */
    public function getElementById($id)
    {
        $xpath = new \DOMXPath($this->dom);
        return $xpath->query("//*[@id='$id']")->item(0);
    }

    /**
     * Get elements from the DOM by class
     *
     * @since 1.00
     *
     * @param string $classname class of DOM Element
     * @param boolean $asArray if true return array else return xpath
     *
     * @return DOM Nodes in array format
     */
    public function getElementsByClass($classname, $asArray = true)
    {
        $result = [];
        $xpath = new \DOMXPath($this->dom);
        $res = $xpath->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");

        if (!$asArray)
            return $res;

        for ($i = $res->length - 1; $i > -1; $i--) {
            $result[] = $res->item($i);
        }

        return $result;

    }

    /**
     * Get parsed html
     *
     * @since 1.00
     *
     * @return HTML created
     */
    public function get()
    {
        $html = $this->dom->saveHTML();

        $html = str_replace("<tagToRemove>","", $html);
        $html = str_replace("</tagToRemove>","", $html);

        return $html;

    }

    /**
     * Create and Import a node
     *
     * @since 1.00
     *
     * @param string $html HTML to insert in DOM
     *
     * @return DOM Node
     */
    public function getNodeToImport($html)
    {


        $tmpDom = new \DOMDocument;
        $tmpDom->formatOutput = true;

        // Add some markup
        /*@$tmpDom->loadHTML("<?xml version=\"1.0\" encoding=\"utf-8\"?><temphtml>".$html."</temphtml>");*/
        @$tmpDom->loadHTML("<?xml version=\"1.0\" encoding=\"utf-8\"?><node>".$html."</node>");
        $tmpNode = $tmpDom->getElementsByTagName("node")->item(0);

        // Import the node, and all its children, to the document
        $tmpNode = $this->dom->importNode($tmpNode, true);

        $tmpDom = null;

        return $tmpNode;
    }

    /**
     * Append HTML on a DOM Object identified by ID
     *
     * @since 1.00
     *
     * @param string $domID id of DOM Element where insert the html
     * @param string $html HTML to insert in DOM
     */
    public function append($domID, $html, $asCDATA = false)
    {
        //check values
        if ($html == "")
            return;

        //if not $domID passed, append on the root
        if ($domID == "")
            $node = $this->root;
        else
            $node = $this->getElementById($domID, $this->dom);

        //create node to append
        if ($asCDATA)
        {
            $fragment = $this->dom->createCDATASection($html);

            $node->appendChild($fragment);
        }
        else
        {

            $this->appendHTMLToImport($node,$html);

        }




    }

    /**
     * Append HTML To Node
     *
     * @since 1.00
     *
     * @param DOMElement $parent node where append
     * @param string $html HTML to insert in DOM
     */
    public function appendHTMLToImport($parent, $html) {

        $tmpDoc = new \DOMDocument();
        @$tmpDoc->loadHTML("<?xml version=\"1.0\" encoding=\"utf-8\"?><node>".$html."</node>");

        foreach ($tmpDoc->getElementsByTagName('node')->item(0)->childNodes as $node) {
            $node = $this->dom->importNode($node,true);
            $parent->appendChild($node);
        }

    }

    /**
     * Prepend HTML on a DOM Object identified by ID
     *
     * @since 1.00
     *
     * @param string $domID id of DOM Element where insert the html
     * @param string $html HTML to insert in DOM
     */
    public function prepend($domID, $html, $asCDATA = false)
    {
        //check values
        if ($html == "")
            return;

        //if not $domID passed, append on the root
        if ($domID == "")
            $node = $this->root;
        else
            $node = $this->getElementById($domID, $this->dom);

        //create node to append
        if ($asCDATA)
        {
            $fragment = $this->dom->createCDATASection($html);
        }
        else
        {
            $fragment = $this->getNodeToImport($html);
        }

        //append the HTML as child
        $node->insertBefore($fragment,$node->firstChild);


    }
}

?>
