<?
/**
 * KIXIXI WEB OBJECTS MANAGER
 * This object manage and replace KPlugin on page (Ex: !KPlugin:k_webmanager.weburl!)
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_webmanager
 * @subpackage objects\web_objects_manager
 * @since 1.00
 */
namespace k_webmanager\objects\general\web_objects_manager;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";



    /**
     * search on a string if there are plugin to load
     *
     * @param array matches match of variable on string
     *
     * @return string html to insert
     */
    protected function pagePluginMatch($matches)
    {


        //get variables
        $data_array = explode(".", $matches[1], 2);
        $moduleAndObject = $data_array[0];
        $tmp = explode(":", $moduleAndObject, 2);
        $pluginName = $tmp[0];
        $pluginObject = isset($tmp[1]) ? $tmp[1] : "";

        //get object
        $obj = $this->K_COMMON->getPluginObject($pluginName, $pluginObject);

        $ArraySend = ["matches" => $matches, "data_array" => $data_array];


        $ret = $obj->pluginMatch($ArraySend);

        if (is_object($ret))
        {
            ;
            $this->mergeObjectVariables($ret);

            return $ret->getHTML();
        }
        else
            return $ret;

    }

    /**
     * replace plugin string with the html of object
     *
     * @param array $arrayVariables
     * Parameters accepted: <br>
     *
     * *    html    : the html to manage
     *
     * @return array html to insert
     */
    public function replacePluginObject($arrayVariables)
    {

        //set variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariables, "webCode");
        $html = $arrayVariables["html"];
        $urlsite = $this->K_COMMON->getStaticContentURLWeb($webCode);

        //replace weburl
        $html = str_replace('!KPlugin:k_webmanager.weburl!', $urlsite, $html);

        //replace plugin
        $html=preg_replace_callback("/\!KPlugin:(.*?)\!/si",'\k_webmanager\objects\general\web_objects_manager\obj::pagePluginMatch',$html);

        //append new html
        $this->appendHTML("", $html);
        
    }



}

?>