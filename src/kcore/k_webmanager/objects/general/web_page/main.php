<?
/**
 * KIXIXI WEB PAGE
 * Load and manage web page (from actual url)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_webmanager
 * @subpackage objects\web_page
 * @since 1.00
 */
namespace k_webmanager\objects\general\web_page;
class obj extends \k_webmanager\objects\general\general_object\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * load actual page from actual url
     *
     */
    public function get()
    {



        //set variables
        $URLPath = $this->K_COMMON->getCurrentURlPath();
        $webCode = $this->K_COMMON->getCurrentWeb();

        //include objects
        $urlrewrite = $this->K_COMMON->getPluginObject("k_webmanager", "urlrewrite");
        $webTemplate = $this->K_COMMON->getPluginObject("k_webmanager","web_templates");
        $tablePages = $this->getTable("web_pages");

        //search url
        $dataURL = $urlrewrite->getByPath($URLPath,$webCode,true);
        if ($dataURL["success"] == "false")
            die("Page Not Found!!!");

        //get URL DATA
        $pageID = $dataURL["pageID"];
        $urlParam = $dataURL["urlParam"];

        //Merge URL Param with $_REQUEST
        parse_str($urlParam, $urlParamArray);
        $_REQUEST = array_merge($_REQUEST, $urlParamArray);

        //load data Page
        $tablePages->get("*",["wpg_web_code" => $webCode, "id_web_page" => $pageID]);
        $dataPage = $tablePages->fetch();




        if (!$dataPage)
            die("PAGE LOAD ERROR!");

        //get PAGE DATA
        $wpgTmplID = $dataPage->wpg_tmpl_id;
        $wpgTitle = $dataPage->wpg_title;
        $checkLogged = $dataPage->wpg_check_logged;

        if ($checkLogged == "1")
            if (!$this->K_SECAUTH->verifyUserLoggedGoLogin()) return;
        
        //load page layout base
        $layout = $this->getTemplateLayout("bootstrap_base");

        //get template
        $webTemplate->addExternalPlugin("kjstart");
        //$webTemplate->addExternalPlugin("bootstrap");
        $webTemplate->get(["keyID" => $wpgTmplID, "useTagKWebObject" => true]);

        //convert CSS/JS to include
        $headFiles = $webTemplate->getHeadCSSJSConverted();
        
        //Assign HEAD JS AND CSS (before external plugin)
        $layout->assign_vars(["HEADCSS"=>$headFiles["CSS"]]);
        $layout->assign_vars(["FOOTERJS"=> $headFiles["JSFOOTER"]]);
        $layout->assign_vars(["HEADJS"=> $headFiles["JS"]]);
        $layout->assign_vars(["CONTENT"=>$webTemplate->getHTML()]);

        //Assign SEO META TAG
        $pageTitle = $this->K_COMMON->getVarArray($webTemplate->getSEOMetaTag(),"title");
        if ($pageTitle == "")
            $pageTitle = $wpgTitle;
        $pageMetaTag = "";
        foreach($webTemplate->getSEOMetaTag() as $key => $value)
        {
            if ($value != "")
                $pageMetaTag .= '<meta name="'.$key.'" content="'.$value.'">' .chr(13);
        }

        //Assign FB Meta Tag
        //$pageMetaTag .= '<meta property="og:site_name" content="" />';
        $pageMetaTag .= '<meta property="og:url" content="'.$this->K_COMMON->getCurrentURL().'" />';
        foreach($webTemplate->getFBMetaTag() as $key => $value)
        {
            if ($value != "")
                $pageMetaTag .= '<meta property="og:'.$key.'" content="'.$value.'">' .chr(13);
        }


        //Write TAG on Page
        $layout->assign_vars(["PAGETITLE"=>$pageTitle]);
        $layout->assign_vars(["PAGEHEADMETATAG"=>$pageMetaTag]);

        //parse template
        $html = $layout->pparse("body");

        

         echo $html;

    }






}
?>