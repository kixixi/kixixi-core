<?
/**
 * KIXIXI k_webmanager (jscript)
 * create a text object
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_webmanager
 * @subpackage web\objects\w_jscript
 * @since 1.00
 */
namespace k_webmanager\web\objects\w_jscript;
class obj extends \k_webmanager\objects\general\web_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();

    }

    /**
     * get this object
     *
     * @since 1.00
     * @access public
     *
     * @param array $arrayVariable array with variables
     * The following directives can be used in the array variables:<br>
     *
     * *    text    : text to write
     *
     * @return webobject
     */
    public function get($arrayVariable = [])
    {

        //set Variables
        $headjs = $this->K_COMMON->getVarArray($arrayVariable,"headjs");
        $footerjs = $this->K_COMMON->getVarArray($arrayVariable,"footerjs");


        //Return
        $this->addHeadJS($headjs);
        $this->addFooterJS($footerjs);

        return $this;

    }


    /**
     * get Parameterization Form
     *
     * @param array $objParameters array with variables
     *
     * @return webobject
     */
    public function parameterizationGetForm($objParameters,$objExtraParameters)
    {
        $arrayFormField = $objParameters;

        //set variables
        $headjs = $this->K_COMMON->getVarArray($arrayFormField,"headjs");
        $footerjs = $this->K_COMMON->getVarArray($arrayFormField,"footerjs");

        //NEW ROW
        $this->appendRow("");

        //Head JS
        $label = "Head JS";
        $name = "headjs";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "textarea", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $headjs]);

        //NEW ROW
        $this->appendRow("");

        //Head JS
        $label = "Footer JS";
        $name = "footerjs";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "textarea", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $footerjs]);

        return $this;

    }

    /**
     * save Parameterization Data
     *
     * @param array $AV array with variables
     *
     * @return webobject
     */
    public function parameterizationSaveData($AV)
    {
        //set variables
        $headjs = $this->K_COMMON->getVarArray($AV,"headjs");
        $footerjs = $this->K_COMMON->getVarArray($AV,"footerjs");

        return ["headjs" => $headjs,"footerjs" => $footerjs];

    }








}

?>