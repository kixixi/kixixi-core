<?
/**
 * KIXIXI k_webmanager (banner)
 * Create banner object
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_webmanager
 * @subpackage web\objects\w_banner
 * @since 1.00
 */
namespace k_webmanager\web\objects\w_banner;
class obj extends \k_webmanager\objects\general\web_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();

    }

    /**
     * get this object (banner)
     *
     * @since 1.00
     * @access public
     *
     * @param array $arrayVariable array with variables
     * The following directives can be used in the array variables:<br>
     *
     * *    width    : width of image
     * *    img    : image
     * *    link    : optional link
     *
     * @return webobject
     */
    public function get($arrayVariable = [])
    {

        //set Variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");
        $width = $this->K_COMMON->getVarArray($arrayVariable,"width");
        $img = $this->K_COMMON->getVarArray($arrayVariable,"img");
        $link = $this->K_COMMON->getVarArray($arrayVariable,"link");
        $imgFluid = $this->K_COMMON->getVarArray($arrayVariable,"imgFluid");
        $imgRounded = $this->K_COMMON->getVarArray($arrayVariable,"imgRounded");
        $urlBase = $this->K_COMMON->getStaticContentURLWeb($webCode);

        //class
        $class = "";
        if ($imgFluid == "1")
            $class .= " img-fluid";
        if ($imgRounded == "1")
            $class .= " img-rounded";


        $html = '<img border="0" ' . ($width != "" ? 'width="'.$width.'"' : "") . ' class="'.$class.'" src="' . $urlBase . $img . '">';


        if ($link != "")
            $html = '<a href="'.$link.'">'.$html.'</a>';

        $this->appendHTML("",$html);

        return $this;
    }

    /**
     * get Parameterization Form
     *
     * @param array $objParameters array with variables
     *
     * @return webobject
     */
    public function parameterizationGetForm($objParameters,$objExtraParameters)
    {
        $arrayFormField = $objParameters;

        //set variables
        $width = $this->K_COMMON->getVarArray($arrayFormField, "width");
        $imgFluid = $this->K_COMMON->getVarArray($arrayFormField, "imgFluid");
        $imgRounded = $this->K_COMMON->getVarArray($arrayFormField, "imgRounded");
        $imgBanner = $this->K_COMMON->getVarArray($arrayFormField, "img");
        $imgLink = $this->K_COMMON->getVarArray($arrayFormField, "link");
        $webCode = $this->K_COMMON->getVarArray($objExtraParameters, "webCode");
        $tmplPluginName = $this->K_COMMON->getVarArray($objExtraParameters, "tmplPluginName");


        //NEW ROW
        $this->appendRow("");

        //img Banner
        $label = "Banner";
        $name = "imgBanner";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "serverFile", "getFolderByPlugin" => $tmplPluginName,"labelInput" => $label, "id" => $name, "name" => $name, "value" => $imgBanner, "webCode" => $webCode]);

        //link
        $label = "Link";
        $name = "imgLink";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $imgLink]);

        //NEW ROW
        $this->appendRow("");

        //img Width
        $label = "Width";
        $name = "width";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "spinner", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $width]);

        //NEW ROW
        $this->appendRow("");

        //fluid
        $label = "Fluid";
        $name = "imgFluid";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1","checked"=>$imgFluid]);

        //NEW ROW
        $this->appendRow("");

        //fluid
        $label = "Rounded";
        $name = "imgRounded";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1","checked"=>$imgRounded]);


        return $this;

    }

    /**
     * save Parameterization Data
     *
     * @param array $AV array with variables
     *
     * @return webobject
     */
    public function parameterizationSaveData($AV)
    {
        //set variables
        $imgBanner = $this->K_COMMON->getVarArray($AV, "imgBanner");
        $imgLink = $this->K_COMMON->getVarArray($AV, "imgLink");
        $width = $this->K_COMMON->getVarArray($AV, "width");
        $imgFluid = $this->K_COMMON->getVarArray($AV, "imgFluid");
        $imgRounded = $this->K_COMMON->getVarArray($AV, "imgRounded");


        return ["img" => $imgBanner, "link" => $imgLink, "width" => $width, "imgFluid"=> $imgFluid,"imgRounded" => $imgRounded];

    }








}

?>