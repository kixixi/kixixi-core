<?
/**
 * KIXIXI k_webmanager (text editor)
 * create a text object
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_webmanager
 * @subpackage web\objects\w_text
 * @since 1.00
 */
namespace k_webmanager\web\objects\w_texteditor;
class obj extends \k_webmanager\objects\general\web_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();

    }

    /**
     * get this object
     *
     * @since 1.00
     * @access public
     *
     * @param array $arrayVariable array with variables
     * The following directives can be used in the array variables:<br>
     *
     * *    text    : text to write
     *
     * @return webobject
     */
    public function get($arrayVariable = [])
    {
        //Include Objects
        $kwebmanagerApi = $this->K_COMMON->getPluginObject("k_webmanager", "api");

        //set Variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");
        $text = $this->K_COMMON->getVarArray($arrayVariable,"text");
        $text = $kwebmanagerApi->formatHtmlLoaded(["html" => $text, "webCode" => $webCode]);

        //Return
        $this->appendHTML("",$text);
        return $this;

    }


    /**
     * get Parameterization Form
     *
     * @param array $objParameters array with variables
     *
     * @return webobject
     */
    public function parameterizationGetForm($objParameters,$objExtraParameters)
    {


        //Include Objects
        $kwebmanagerApi = $this->K_COMMON->getPluginObject("k_webmanager", "api");

        //set variables
        $text = $this->K_COMMON->getVarArray($objParameters, "text");
        $webCode = $this->K_COMMON->getVarArray($objExtraParameters, "webCode");
        $text = $kwebmanagerApi->formatHtmlLoaded(["html" => $text, "webCode" => $webCode]);

        //NEW ROW
        $this->appendRow("");

        //img Text
        $label = "Text";
        $name = "text";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "textEditor", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $text, "webCode" => $webCode]);

        return $this;

    }

    /**
     * save Parameterization Data
     *
     * @param array $AV array with variables
     *
     * @return webobject
     */
    public function parameterizationSaveData($AV)
    {
        //Include Objects
        $kwebmanagerApi = $this->K_COMMON->getPluginObject("k_webmanager", "api");

        //set variables
        $text = $this->K_COMMON->getVarArray($AV, "text");
        $webCode = $this->K_COMMON->getVarArray($AV, "webCode");
        $text = $kwebmanagerApi->formatHtmlForSave(["html" => $text, "webCode" => $webCode]);

        return ["text" => $text];

    }








}

?>