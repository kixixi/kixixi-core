<?
/**
 * KIXIXI k_webmanager (logo)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_webmanager
 * @subpackage web\objects\w_template
 * @since 1.00
 */
namespace k_webmanager\web\objects\w_template;
class obj extends \k_webmanager\objects\general\web_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();

    }

    /**
     * get this object (logo from web site parameter)
     *
     * @since 1.00
     * @access public
     *
     * @param array $arrayVariable array with variables
     * The following directives can be used in the array variables:<br>
     *
     * *    width    : width of logo
     *
     * @return webobject
     */
    public function get($arrayVariable = [])
    {

        //set Variables
        $objData = $this->K_COMMON->getVarArray($arrayVariable,"objData");
        $pluginName = $this->K_COMMON->getVarArray($arrayVariable,"pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($arrayVariable,"pluginObject");
        $isModify = $this->K_COMMON->getVarArray($objData,"isModify");
        $tplCode = $this->K_COMMON->getVarArray($arrayVariable,"tplCode");
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");

        if($tplCode == "")
            return $this;

        $templateVariables = [
            "useTagKWebObject" => true
            ,"setModifyTemplate" => $isModify
            , "loadTemplateIN" => true
            , "tmplWebCode" => $webCode

        ];

        $this->appendWebTemplate("", $tplCode, [], $templateVariables, $pluginName , $pluginObject );

        //Return
        return $this;
    }

    /**
     * get Parameterization Form
     *
     * @param array $objParameters array with variables
     *
     * @return webobject
     */
    public function parameterizationGetForm($objParameters,$objExtraParameters)
    {


        //set variables
        $pluginName = $this->K_COMMON->getVarArray($objParameters,"pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($objParameters,"pluginObject");
        $tplCode = $this->K_COMMON->getVarArray($objParameters,"tplCode");
        $webCode = $this->K_COMMON->getVarArray($objExtraParameters, "webCode");
        $templateID = "";

        $table = $this->getTable("web_templates");
        $table->get(["*"],["tpl_plugin_name" => $pluginName,"tpl_plugin_object"=>$pluginObject,"tpl_code"=>$tplCode]);
        $data = $table->fetch();

        if ($data)
            $templateID = $data->id_web_template;

        $this->appendRow("");
        $label = $this->dictionary["labelParamFormTemplate"];
        $name = "templateID";
        $this->appendColumn($this->lastRow, ["nCol" => 7]);
        $this->appendInput($this->lastCol, [
            "type" => "select"
            , "labelInput" => $label
            , "id" => $name
            , "name" => $name
            , "value" => $templateID
            , "required" => "1"
            , "typeLoad" => "function"
            , "value" => $templateID
            , "pluginName" => $this->getPluginName()
            , "pluginObject" => "api"
            , "functionName" => "getTemplatesList"
            , "functionVariables" => ["webCode" => $webCode]
        ]);


        return $this;

    }

    /**
     * save Parameterization Data
     *
     * @param array $AV array with variables
     *
     * @return webobject
     */
    public function parameterizationSaveData($AV)
    {
        //set variables
        $templateID = $this->K_COMMON->getVarArray($AV, "templateID");

        $table = $this->getTable("web_templates");
        $table->get(["*"],["id_web_template" => $templateID]);
        $data = $table->fetch();

        $pluginName = $data->tpl_plugin_name;
        $pluginObject = $data->tpl_plugin_object;
        $tplCode = $data->tpl_code;

        return ["pluginName" => $pluginName, "pluginObject" => $pluginObject, "tplCode" => $tplCode];

    }









}

?>