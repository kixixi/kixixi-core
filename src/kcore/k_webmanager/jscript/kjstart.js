if (typeof (KURL_host) == "undefined") KURL_host = "";
var KXNS = {}
var KXJS = {}
KXJS.creationDate = new Date();
KXJS.namespace = function (ns) {
    var res = ns.split(".");
    var nsTemp = KXNS;

    for (i = 0; i < res.length; i++) {

        if (!nsTemp[res[i]])
            nsTemp = nsTemp[res[i]] = {};
        else
            nsTemp = nsTemp[res[i]];

    }

    nsTemp.NSName = ns;


}
KXJS.progID = 0;
KXJS.PCall = "/kapi/kapi.php";
KXJS.Data = {};
KXJS.Form = {};
KXJS.creationDate = new Date();


KXJS.jscssadded = "";
KXJS.alertTest = function() {alert("Test OK!");}
KXJS.loadjscss = function (filename, filetype, fcallback) {
    if (KXJS.jscssadded.indexOf("[" + filename + "]") == -1) {
        if (filetype == "js") {
            var fileref = document.createElement('script');
            fileref.onload = fcallback;
            fileref.setAttribute("type", "text/javascript");
            fileref.setAttribute("src", filename);
        }
        else if (filetype == "css") {
            ieversion = 0;
            if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
                var ieversion = new Number(RegExp.$1)
            }
            var fileref = document.createElement("link");
            if (ieversion >= 9) fileref.onload = fcallback;
            fileref.setAttribute("rel", "stylesheet");
            fileref.setAttribute("type", "text/css");
            fileref.setAttribute("href", filename);
        }
        if (typeof fileref != "undefined") document.getElementsByTagName("head")[0].appendChild(fileref);
        KXJS.jscssadded += "[" + filename + "]";
    }
}
KXJS.JSON = function () {
    this.decode = function (json) {
        return eval("(" + json + ')');
    }
};
KXJS.dataAjax = function () {
    var to = this;
    this.LoadPage = function (url, param) {
        parameters = param;
        if (typeof param == 'object') {
            var i = 0;
            var s = "";
            for (var a in param) {
                s = "&";
                parameters += s + a + "=" + param[a];
            }
        }
        to.http_request = false;
        if (window.XMLHttpRequest) { // Mozilla, Safari,...
            to.http_request = new XMLHttpRequest();

            if (to.http_request.overrideMimeType) {
                to.http_request.overrideMimeType('text/html');
            }
        }
        else if (window.ActiveXObject) { // IE
            try {
                to.http_request = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    to.http_request = new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch (e) {
                }
            }
        }
        if (!to.http_request) {
            alert('Cannot create XMLHTTP instance');
            return false;
        }

        to.http_request.onreadystatechange = to.callbackFirst;
        to.http_request.open('POST', url, true);
        to.http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        to.http_request.setRequestHeader("Content-length", parameters.length);
        to.http_request.setRequestHeader("Connection", "close");
        to.http_request.send(parameters);


    } // End Load Data Ajax


    this.LoadData = function (kcp, kco, kca, addParam) {
        var poststr = "";
        var url = KXJS.buildAPICallUrl("KXCH", kcp, kco, kca);


        if (typeof(addParam) == "object") {
            if (addParam.addParam != "") poststr += "&" + Param.addParam;
            url = addParam.Url;
        }
        else {
            if (addParam != "") addParam = "&" + addParam;
            poststr += addParam;
        }

        to.LoadPage(url, poststr);
    } //End PostForm


    this.PostForm = function (kcp, kco, kca, addParam) {
        var poststr = "";
        var url = KXJS.buildAPICallUrl("KXCH", kcp, kco, kca);
        poststr = KXJS.getFormData(obj, false);
        if (typeof(addParam) == "object") {
            if (addParam.addParam != "") poststr += "&" + Param.addParam;
            url = addParam.Url;
        }
        else {
            if (addParam != "") addParam = "&" + addParam;
            poststr += addParam;
        }


        to.LoadPage(url, poststr);
    } //End PostForm

    this.callbackFirst = function (res) {
    };
    this.callback = function (res) {
    };

    this.callbackFirst = function (res) {

        if (to.http_request.readyState == 4) {
            if (to.http_request.status == 200) {

                result = to.http_request.responseText;

                to.callback(result);


            } else {/*Error Request*/
            }
        }
    }
}
KXJS.getFormData = function (f, returnobj) {
    var s = ""; //Return String
    var n = f.elements.length; //Length of elements
    var t = ""; //Temporary String
    var o = []; //Return as Object


    for (var i = 0; i < n; i++) {
        TagType = f.elements[i].type;
        objName = f.elements[i].name;
        if (objName != "") {
            if (TagType == "checkbox") {
                if (f.elements[i].checked) {
                    t = f.elements[i].name + "= 1";
                    o[objName] = 1;
                }
                else {
                    t = f.elements[i].name + "= 0";
                    o[objName] = "";
                }

            }
            else if (TagType == "radio") {
                if (f.elements[i].checked) {
                    t = f.elements[i].name + "=" + f.elements[i].value;
                    o[objName] = f.elements[i].value;
                }
            }
            else {
                t = f.elements[i].name + "=" + encodeURIComponent(f.elements[i].value);
                o[objName] = encodeURIComponent(f.elements[i].value);
            }
            if (i < n - 1) {
                s += t + "&";
            } else {
                s += t;
            }
        }
    }
    if (returnobj) return o; else return s;
}

KXJS.getNewDomId = function () {
    KXJS.progID += 1;
    return "wid" + KXJS.progID;
}


KXJS.getUrlVar = function (name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || ""
}


KXJS.getImageSizeAdapted = function (ori_w, ori_h, maxw, maxh) {
    var newsize = {};
    if (ori_w >= ori_h) {
        var alt_new = maxw * ori_h / ori_w;
        newsize.width = maxw;
        newsize.height = alt_new;

    }
    else if (ori_h > ori_w) {
        var largh_new = ori_w * maxh / ori_h;
        newsize.width = largh_new;
        newsize.height = maxh;
    }
    return newsize;
}

KXJS.buildAPICallUrl = function (wcall, kcp, kco, kca, param) {
    var currentTime = new Date();
    var KCTime = currentTime.hhmmss();
    var KCDate = currentTime.yyyymmdd();
    var paramString = "";

    if (typeof(param) == "string")
        paramString = "&" + param;
    else
        if (typeof(param) == "object")
        {
        $.each(param, function(index, value) {
            paramString += "&" + index + "=" + value;
        });
        }

    return KXJS.PCall + "?KXTCALL=" + wcall + "&KCP=" + kcp + "&KCO=" + kco + "&KCA=" + kca + "&KCDate=" + KCDate + "&KCTime=" + KCTime + paramString;
}

KXJS.showFileManager = function (webCode, fieldID, folderToGet, fileType, getWithUrl) {


    var fmURL = KXJS.buildAPICallUrl(
        "KXCJS"
        , "k_filemanager"
        , "filebrowser"
        , "get"
        , "webCode=" + webCode + "&fileType=" + fileType + "&fieldID=" + fieldID + "&folderToGet=" + folderToGet + "&getWithUrl=" + getWithUrl
    );

    var modalHTML = '<div id="fileBrowserModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="myModalLabel" style="z-index:999999;">';
    modalHTML += '<div role="document" class="modal-dialog" style="width: 785px; max-width: 785px;"> ';
    modalHTML += '<div class="modal-content">';
    modalHTML += '<div class="modal-header">';
    modalHTML += '<button type="button" class="close" data-dismiss="modal">×</button>';
    modalHTML += '<h4 class="modal-title">File Browser</h4>';
    modalHTML += '</div>';
    modalHTML += '<div class="modal-body">';
    modalHTML += '<iframe src="" style="zoom:0" frameborder="0" height="500" width="750"></iframe>';
    modalHTML += '</div>';
    modalHTML += '</div>';
    modalHTML += '</div>';
    modalHTML += '</div>';

    $("body").append(modalHTML);

    $('#fileBrowserModal').modal("show");
    $('#fileBrowserModal iframe').attr("src", fmURL);


    $('#fileBrowserModal').on('hidden.bs.modal', function (e) {
        $('#fileBrowserModal').remove();
    });


}
KXJS.xmlToHtml = function (data) {
    // XML to HTML
    data = data.replace(/</gi, "<div class='");
    data = data.replace(/>/gi, "'>");
    data = data.replace(/<div class='\//gi, "</");
    data = data.replace(/\<\/[^\>]+\>/gi, "</div>");
    return data;
}
KXJS.dateFormat = function () {
    var LanguageDate = LanguageDate || "EN";
    var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloZ]|"[^"]*"|'[^']*'/g,
        timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
        timezoneClip = /[^-+\dA-Z]/g,
        pad = function (value, length) {
            value = String(value);
            length = parseInt(length) || 2;
            while (value.length < length)
                value = "0" + value;
            return value;
        };

    // Regexes and supporting functions are cached through closure
    return function (date, mask, ActFormat, LanguageDate) {
        ActFormat = ActFormat || "";
        LanguageDate = LanguageDate || "EN";
        if (LanguageDate == "EN")
            KXJS.dateFormat.i18n = {
                dayNames: [
                    "Sun", "Mon", "Tue", "Wed", "Thr", "Fri", "Sat",
                    "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
                ],
                monthNames: [
                    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
                    "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
                ]
            };
        else if (LanguageDate == "IT")
            KXJS.dateFormat.i18n = {
                dayNames: [
                    "Sun", "Mon", "Tue", "Wed", "Thr", "Fri", "Sat",
                    "Domenica", "Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato"
                ],
                monthNames: [
                    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
                    "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"
                ]
            };

        //Verifica se la data è vuota
        if ((date == "") || (date == "null")) return "";

        //Verifica se è nel formato database
        if (typeof(date) == "string") { //alert(date);
            if (date.substr(4, 1) == "-") {
                if (ActFormat == "I") date = date.substr(8, 2) + "/" + date.substr(5, 2) + "/" + date.substr(0, 4);
                else date = date.substr(5, 2) + "/" + date.substr(8, 2) + "/" + date.substr(0, 4);

            }
            //alert(date);
            if (ActFormat == "I") date = date.substr(3, 2) + "/" + date.substr(0, 2) + "/" + date.substr(6, 4);
        }


        // Treat the first argument as a mask if it doesn't contain any numbers

        if (
            arguments.length == 1 &&
            (typeof date == "string" || date instanceof String) && !/\d/.test(date)
        ) {
            mask = date;
            date = undefined;
        }
        //alert(date);
        date = date ? new Date(date) : new Date();
        //alert(date);
        if (isNaN(date)) return "";
        //throw "invalid date";

        var dF = KXJS.dateFormat;
        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        var d = date.getDate(),
            D = date.getDay(),
            m = date.getMonth(),
            y = date.getFullYear(),
            H = date.getHours(),
            M = date.getMinutes(),
            s = date.getSeconds(),
            L = date.getMilliseconds(),
            o = date.getTimezoneOffset(),
            flags = {
                d: d,
                dd: pad(d),
                ddd: dF.i18n.dayNames[D],
                dddd: dF.i18n.dayNames[D + 7],
                m: m + 1,
                mm: pad(m + 1),
                mmm: dF.i18n.monthNames[m],
                mmmm: dF.i18n.monthNames[m + 12],
                yy: String(y).slice(2),
                yyyy: y,
                h: H % 12 || 12,
                hh: pad(H % 12 || 12),
                H: H,
                HH: pad(H),
                M: M,
                MM: pad(M),
                s: s,
                ss: pad(s),
                l: pad(L, 3),
                L: pad(L > 99 ? Math.round(L / 10) : L),
                t: H < 12 ? "a" : "p",
                tt: H < 12 ? "am" : "pm",
                T: H < 12 ? "A" : "P",
                TT: H < 12 ? "AM" : "PM",
                Z: (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                o: (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4)
            };

        return mask.replace(token, function ($0) {
            return ($0 in flags) ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
}();


KXJS.parseDateTime = function (d) {
    var s = $.trim(d);
    s = s.replace(/\.\d+/, ""); // remove milliseconds
    s = s.replace(/-/, "/").replace(/-/, "/");
    s = s.replace(/T/, " ").replace(/Z/, " UTC");
    s = s.replace(/([\+\-]\d\d)\:?(\d\d)/, " $1$2"); // -04:00 -> -0400
    return new Date(s);
}


KXJS.noRightClick = function (e) {
    if (e.stopPropagation) e.stopPropagation()
    else e.cancelBubble = true;
    return false
}
KXJS.browserVersion = {
    init: function () {
        this.browser = this.searchString(this.dataBrowser) || "unknown";
        this.version = this.searchVersion(navigator.userAgent)
            || this.searchVersion(navigator.appVersion)
            || "an unknown version";
        this.OS = this.searchString(this.dataOS) || "an unknown OS";
    },
    searchString: function (data) {
        for (var i = 0; i < data.length; i++) {
            var dataString = data[i].string;
            var dataProp = data[i].prop;
            this.versionSearchString = data[i].versionSearch || data[i].identity;
            if (dataString) {
                if (dataString.indexOf(data[i].subString) != -1)
                    return data[i].identity;
            }
            else if (dataProp)
                return data[i].identity;
        }
    },
    searchVersion: function (dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index == -1) return;
        return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
    },
    dataBrowser: [
        {
            string: navigator.userAgent,
            subString: "Chrome",
            identity: "Chrome"
        },
        {
            string: navigator.userAgent,
            subString: "OmniWeb",
            versionSearch: "OmniWeb/",
            identity: "OmniWeb"
        },
        {
            string: navigator.vendor,
            subString: "Apple",
            identity: "Safari",
            versionSearch: "Version"
        },
        {
            prop: window.opera,
            identity: "Opera",
            versionSearch: "Version"
        },
        {
            string: navigator.vendor,
            subString: "iCab",
            identity: "iCab"
        },
        {
            string: navigator.vendor,
            subString: "KDE",
            identity: "Konqueror"
        },
        {
            string: navigator.userAgent,
            subString: "Firefox",
            identity: "Firefox"
        },
        {
            string: navigator.vendor,
            subString: "Camino",
            identity: "Camino"
        },
        {		// for newer Netscapes (6+)
            string: navigator.userAgent,
            subString: "Netscape",
            identity: "Netscape"
        },
        {
            string: navigator.userAgent,
            subString: "MSIE",
            identity: "Explorer",
            versionSearch: "MSIE"
        },
        {
            string: navigator.userAgent,
            subString: "Gecko",
            identity: "Mozilla",
            versionSearch: "rv"
        },
        { 		// for older Netscapes (4-)
            string: navigator.userAgent,
            subString: "Mozilla",
            identity: "Netscape",
            versionSearch: "Mozilla"
        }
    ],
    dataOS: [
        {
            string: navigator.platform,
            subString: "Win",
            identity: "Windows"
        },
        {
            string: navigator.platform,
            subString: "Mac",
            identity: "Mac"
        },
        {
            string: navigator.userAgent,
            subString: "iPhone",
            identity: "iPhone/iPod"
        },
        {
            string: navigator.platform,
            subString: "Linux",
            identity: "Linux"
        }
    ]
};
KXJS.colorToHex = function (color) {


    if (color.substr(0, 1) === '#') {
        return color;
    }
    if (color == "transparent") return "";
    if (color == "rgba(0, 0, 0, 0)") return "";
    var digits = /^(.*?)(rgb)(a*)\((\d+),\s*(\d+),\s*(\d+)(,*)\s*(\d*)\)$/.exec(color);

    var red = parseInt(digits[4]);
    var green = parseInt(digits[5]);
    var blue = parseInt(digits[6]);
    var rgb = blue | (green << 8) | (red << 16);
    return '#' + rgb.toString(16);


};


//Web Site Editing
KXJS.setEditingWebSite = function (l, r) {
    if (typeof(r) == "undefined") r = true;
    KXJS.setCookie("kcews", l, 365);

    if (r)
    {
        if (typeof (KXJS.editingWebReloadUrl) != "undefined")
            window.location.href = KXJS.editingWebReloadUrl;
        else
            window.location.reload();
    }



}

//Languages
KXJS.setLanguage = function (l, r, u) {
    if (typeof(r) == "undefined") r = true;
    if (typeof(u) == "undefined") u = "";
    if (u !== "") {
        if (u.search("\\?") > -1)
            window.location.href = u + "&setLanguage=" + l;
        else
            window.location.href = u + "?setLanguage=" + l;
    }
    else {
        KXJS.setCookie("kclng", l, 365);
        if (r) window.location.reload();
    }


}


KXJS.getLanguage = function () {
    return KXJS.getCookie("kclng");
}
//Cookie
KXJS.setCookie = function (n, v, d) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + d);
    var value = escape(v) + ((d == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = n + "=" + value+";path=/";
}
KXJS.getCookie = function (n) {
    var re = new RegExp(n + "=([^;]+)");
    var value = re.exec(document.cookie);
    return (value != null) ? unescape(value[1]) : null;
}

//Extension Date Format
Date.prototype.yyyymmdd = function () {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();

    return [this.getFullYear(), "-", ('0' + mm).slice(-2), "-", ('0' + dd).slice(-2)].join('');
};
Date.prototype.hhmmss = function () {
    var t = ('0' + this.getHours()).slice(-2) + ":" + ('0' + this.getMinutes()).slice(-2) + ":" + ('0' + this.getSeconds()).slice(-2);

    return t;
};

Date.prototype.format = function (mask) {
    return KXJS.dateFormat(this, mask);
}
Date.prototype.formatDate = function (format) {
    var returnStr = '';
    var replace = Date.replaceChars;
    for (var i = 0; i < format.length; i++) {
        var curChar = format.charAt(i);
        if (replace[curChar]) {
            returnStr += replace[curChar].call(this);
        } else {
            returnStr += curChar;
        }
    }
    return returnStr;
};
Date.replaceChars = {
    shortMonths: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    longMonths: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    shortDays: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    longDays: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],

    // Day
    d: function () {
        return (this.getDate() < 10 ? '0' : '') + this.getDate();
    },
    D: function () {
        return Date.replaceChars.shortDays[this.getDay()];
    },
    j: function () {
        return this.getDate();
    },
    l: function () {
        return Date.replaceChars.longDays[this.getDay()];
    },
    N: function () {
        return this.getDay() + 1;
    },
    S: function () {
        return (this.getDate() % 10 == 1 && this.getDate() != 11 ? 'st' : (this.getDate() % 10 == 2 && this.getDate() != 12 ? 'nd' : (this.getDate() % 10 == 3 && this.getDate() != 13 ? 'rd' : 'th')));
    },
    w: function () {
        return this.getDay();
    },
    z: function () {
        return "Not Yet Supported";
    },
    // Week
    W: function () {
        return "Not Yet Supported";
    },
    // Month
    F: function () {
        return Date.replaceChars.longMonths[this.getMonth()];
    },
    m: function () {
        return (this.getMonth() < 11 ? '0' : '') + (this.getMonth() + 1);
    },
    M: function () {
        return Date.replaceChars.shortMonths[this.getMonth()];
    },
    n: function () {
        return this.getMonth() + 1;
    },
    t: function () {
        return "Not Yet Supported";
    },
    // Year
    L: function () {
        return "Not Yet Supported";
    },
    o: function () {
        return "Not Supported";
    },
    Y: function () {
        return this.getFullYear();
    },
    y: function () {
        return ('' + this.getFullYear()).substr(2);
    },
    // Time
    a: function () {
        return this.getHours() < 12 ? 'am' : 'pm';
    },
    A: function () {
        return this.getHours() < 12 ? 'AM' : 'PM';
    },
    B: function () {
        return "Not Yet Supported";
    },
    g: function () {
        return this.getHours() % 12 || 12;
    },
    G: function () {
        return this.getHours();
    },
    h: function () {
        return ((this.getHours() % 12 || 12) < 10 ? '0' : '') + (this.getHours() % 12 || 12);
    },
    H: function () {
        return (this.getHours() < 10 ? '0' : '') + this.getHours();
    },
    i: function () {
        return (this.getMinutes() < 10 ? '0' : '') + this.getMinutes();
    },
    s: function () {
        return (this.getSeconds() < 10 ? '0' : '') + this.getSeconds();
    },
    // Timezone
    e: function () {
        return "Not Yet Supported";
    },
    I: function () {
        return "Not Supported";
    },
    O: function () {
        return (this.getTimezoneOffset() < 0 ? '-' : '+') + (this.getTimezoneOffset() / 60 < 10 ? '0' : '') + (this.getTimezoneOffset() / 60) + '00';
    },
    T: function () {
        return "Not Yet Supported";
    },
    Z: function () {
        return this.getTimezoneOffset() * 60;
    },
    // Full Date/Time
    c: function () {
        return "Not Yet Supported";
    },
    r: function () {
        return this.toString();
    },
    U: function () {
        return this.getTime() / 1000;
    }
};


KXJS.isMobile = function () {
    if (navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/Windows Phone/i)
    )
        return true;

    else
        return false;

}

//Social
KXJS.fbShareWin = function (url, winWidth, winHeight) {


    var winTop = (screen.height / 2) - (winHeight / 2);
    var winLeft = (screen.width / 2) - (winWidth / 2);
    url = encodeURIComponent(url);
    window.open('http://www.facebook.com/sharer.php?s=100&u=' + url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
}
KXJS.fbShareWinApp = function (url, title, caption, descr, image, funcresponse) {

    if (typeof(funcresponse) == "string") {
        if (funcresponse == "") {
            fr = function () {
            };
        }
        else {
            fr = function (response) {
                eval(funcresponse);
            };
        }
    }
    else fr = funcresponse;

    FB.ui(
        {
            method: 'share',
            display: 'popup',
            href: url,
            name: title,
            picture: image,
            caption: caption,
            description: descr,
            access_token: ''
        }
        , fr
    );


}
KXJS.gplusShareWin = function (url, title, descr, image, winWidth, winHeight) {
    var winTop = (screen.height / 2) - (winHeight / 2);
    var winLeft = (screen.width / 2) - (winWidth / 2);
    url = encodeURIComponent(url);
    window.open('https://plus.google.com/share?url=' + url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
}


KXJS.dateCountDown = function (endDate, varLabelDay, varLabelDays) {

    var now = new Date();
    var result = {};
    endDate = KXJS.parseDateTime(endDate);


    var difference = (endDate.getTime() - now.getTime());

    if (difference < 0) {
        result.success = false;
        return result;
    }

    data = new Date(difference);
    differenceH = (data.getHours() - 1) + (data.getDate() - 1) * 24;
    differenceM = data.getMinutes();
    differenceS = data.getSeconds();


    differenceD = (differenceH - (differenceH % 24)) / 24;
    differenceH = differenceH % 24;

    if (differenceD == 1)
        differenceD += " " + varLabelDay + " ";
    else if (differenceD > 1)
        differenceD += " " + varLabelDays + " ";
    else
        differenceD = "";

    if (differenceM <= 9)
        differenceM = "0" + differenceM;

    if (differenceS <= 9)
        differenceS = "0" + differenceS;


    result.success = true;
    result.differenceD = differenceD;
    result.differenceH = differenceH;
    result.differenceM = differenceM;
    result.differenceS = differenceS;

    return result;
}

KXJS.jumpTo = function (id, offset) {
    var e = document.getElementById(id);
    var top = 0;

    do {
        top += e.offsetTop;
    } while (e = e.offsetParent);
    $('body,html').animate({scrollTop: top + offset}, 1500);

}

KXJS.showSavedMessageJSON = function (e, rJSON, alertM) {
    message = "";
    if (typeof(rJSON.message) != "undefined") message = rJSON.message;
    if (rJSON.success == "true") {
        KXJS.showSavedMessage(e, message, true, alertM);
    }
    else {
        KXJS.showSavedMessage(e, message, false, alertM);
    }
}

KXJS.showSavedMessage = function (e, message, success, alertM) {
    if (typeof(message) == "undefined") message = "";
    if (success) {
        $(e).show();
        $(e).removeClass("text-danger");
        $(e).removeClass("alert-danger");
        $(e).addClass("text-success");
        if (alertM == 1) $(e).addClass("alert alert-success");
        $(e).html(message);
        setTimeout(function () {
            $(e).fadeOut(3000);
        }, 10000);

    }
    else {
        $(e).show();
        $(e).removeClass("text-success");
        $(e).removeClass("alert-success");
        $(e).addClass("text-danger");
        if (alertM == 1) $(e).addClass("alert alert-danger");
        $(e).html(message);
    }
}


KXJS.Form.createSingleButtonFileForm = function (options) {
    var options = options || {};

    var divID = options.divID || '';
    var divClass = options.divClass || '';
    var formID = options.formID || KXJS.getNewDomId();
    var iFileName = options.iFileName || 'fileName';
    var text = options.text || '';
    var otherFormElement = options.otherFormElement || '';
    var hiddenName = options.hiddenName || '';
    var hiddenValue = options.hiddenValue || '';
    var buttonValue = options.buttonValue || '';
    var buttonClass = options.buttonClass || '';
    var appendTo = options.appendTo || '';
    var onFileChange = options.onFileChange || '';

    var divIDString = (divID === '') ? '' : ' id="' + divID + '"';
    var divClassString = (divClass === '') ? '' : ' class=" ' + divClass + '"';
    var formIDString = (formID === '') ? '' : ' id="' + formID + '"';
    var iFileNameString = (iFileName === '') ? 'fileName' : ' name="' + iFileName + '"';
    var buttonString = (buttonValue === '') ? '' : '<input type="button" class="' + buttonClass + '" value="' + buttonValue + '" />';
    var hiddenString = (hiddenName === '') ? '' : '<input type="hidden" value="' + hiddenValue + '" name="' + hiddenName + '">';

    var d = '';

    var styleForm = "overflow: hidden;";

    var styleInputFile = "background: white none repeat scroll 0 0;" +
        "cursor: inherit;" +
        "display: block;" +
        "min-height: 100%;" +
        "min-width: 100%;" +
        "opacity: 0;" +
        "outline: medium none;" +
        "position: absolute;" +
        "right: 0;" +
        "text-align: right;" +
        "top: 0;" +
        "width:100%;";

    if (buttonString == "") {
        d += '<div' + divIDString + divClassString + '>' + text;
        d += '	<form' + formIDString + ' style="' + styleForm + '">';
        d += '		<input type="file" ' + iFileNameString + ' style="' + styleInputFile + '">';
        d += buttonString;
        d += hiddenString;
        d += otherFormElement;
        d += '	</form>';
        d += '</div>';
    }
    else {
        d += '<div' + divIDString + divClassString + '>' + text;
        d += '	<form' + formIDString + ' style="' + styleForm + '">';
        d += '		<input type="file" ' + iFileNameString + ' style="' + styleInputFile + '">';
        d += buttonString;
        d += hiddenString;
        d += otherFormElement;
        d += '	</form>';
        d += '</div>';
    }


    if (appendTo != "")
        $(appendTo).append(d);

    if (onFileChange != "")
        $("#" + formID + " input[type='file']").change(onFileChange);

    return {"formID": formID, "d": d};
}

KXJS.getDivManageImg = function (options) {
    var rotate = options.rotate || 0;
    var add = options.add || 0;
    var remove = options.remove || 0;
    var edit = options.edit || 0;
    var plus = options.plus || 0;
    var appendTo = options.appendTo || '';
    var src = options.src || '';
    var fileButton = options.fileButton || {};
    var onRotate = options.onRotate || '';
    var onRemove = options.onRemove || '';
    var hiddenName = options.hiddenName || '';
    var hiddenValue = options.hiddenValue || '';

    var divID = options.formID || KXJS.getNewDomId();
    var divIDPlus = options.formID || KXJS.getNewDomId();
    var d = "";

    //check for src
    if (src == "")
        edit = 0;
    else
        plus = 0;

    if ((rotate === 1 || add === 1 || remove === 1 || edit === 1) && src != "") {
        d = '<div class="manageImg bg-faded" id="' + divID + '">';
        d += '<div class="manageImgHidden">';
        d += (hiddenName === '') ? '' : '<input type="hidden" name="keyRef" value="' + hiddenName + '">';
        d += (hiddenName === '') ? '' : '<input type="hidden" name="keyRefValue" value="' + hiddenValue + '">';
        d += '</div>';
        d += (rotate === 0) ? '' : '<div class="rotateRight "><i class="fa fa-rotate-right"></i></div>';
        d += (rotate === 0) ? '' : '<div class="rotateLeft "><i class="fa fa-rotate-left"></i></div>';
        d += (add === 0) ? '' : '<div class="newImg  btn-file"><i class="fa fa-plus"></i></div>';
        d += (remove === 0) ? '' : '<div class="removeImg text-danger"><i class="fa fa-remove"></i></div>';
        d += (edit === 0) ? '' : '<div class="changeImg  btn-file"><i class="fa fa-edit"></i></div>';
        d += '</div>';
    }

    if (plus === 1) {
        if (!$(appendTo + " .plusImg").length)
        {
            d += '<div class="plusImg" id="' + divIDPlus + '">' + String.fromCharCode(13);
            d += '<a href="#" class="btn btn-lg btn-primary btn-file center-block">' + String.fromCharCode(13);
            d += '<i class="fa fa-plus fa-2x"></i>' + String.fromCharCode(13);
            d += '</a>' + String.fromCharCode(13);
            d += '</div>' + String.fromCharCode(13);
        }
    }



    if (appendTo != "")
    {

        //remove actual
        $(appendTo + " .manageImg").remove();

        //Change src
        if (src != "")
        {
            if ( $(appendTo + " .plusImg").length )
                $(appendTo + " .plusImg").remove();

            $(appendTo + " img").attr("src",src+"?" + new Date());
        }
        else
            $(appendTo + " img").attr("src","");
        // Append Icons
        $(appendTo).append(d);

        if (onRotate != "")
        {
            $(appendTo + " .rotateRight").click(function(){onRotate(270,$(this))})
            $(appendTo + " .rotateLeft").click(function(){onRotate(90,$(this))})
        }
        if (onRemove != "")
            $(appendTo + " .removeImg").click(onRemove);


        if (plus === 1)
        {

            fileButton.appendTo = "#" + divIDPlus + " a";
            fileButton.hiddenName = hiddenName;
            fileButton.hiddenValue = hiddenValue;
            KXJS.Form.createSingleButtonFileForm(fileButton);


        }

        if (edit === 1)
        {
            fileButton.appendTo = "#" + divID + " .changeImg";
            fileButton.hiddenName = hiddenName;
            fileButton.hiddenValue = hiddenValue;
            KXJS.Form.createSingleButtonFileForm(fileButton);
        }
    }

    return {"divID": divID, "divIDPlus": divIDPlus, "d": d};
}

KXJS.loginFB = function (options) {

    var response;
    fnFalse = options.fnFalse || "";
    fnSuccess = options.fnSuccess || "";
    scope = options.scope || "";

    if (scope == "")
        scope = 'email,birthday,hometown';

    FB.login(function (response) {
        var fbatk = FB.getAuthResponse().accessToken;

        if (response.authResponse) {
            FB.api('/me/photos?access_token=' + fbatk
                , function (response) {

                    if (response.error) {
                        if (fnFalse != "") fnFalse();
                        else alert(response.error);

                    }
                    else {
                        FB.api('/me', function (me) {
                            if (me.name)
                                if (fnSuccess != "") fnSuccess({"me": me, "fbatk": fbatk});
                        })
                    }
                });
        }
        else {
            alert("Error!");
        }
    }, {scope: scope});
}

KXJS.centerScreenPopUp = function (popUp) {
    pw = popUp.outerWidth();
    ph = popUp.outerHeight();
    ww = $(window).width();
    wh = $(window).height();
    t = wh / 2;
    t = t - (ph / 2);
    l = ww / 2;
    l = l - (pw / 2);
    popUp.css({"position": "fixed", "top": parseInt(t) + "px", "left": parseInt(l) + "px", "z-index": "99999"});
}


KXJS.downloadFile = function (urlDownload) {
    waitingDialog.show("Please Wait ...", {dialogSize: "sm", progressType: "warning"});

    var iframe = $("<iframe/>").attr({
        src: urlDownload,
        style: "visibility:hidden;display:none",
        id: "KXDownloadFileIFrame"
    }).appendTo("body");

    $('#KXDownloadFileIFrame').on('load', function () {
        waitingDialog.hide();
        setTimeout(function () {
            $('#KXDownloadFileIFrame').remove();
        }, 3000);
    });
}

KXJS.assignDataCombo = function (options) {

    //Set Variable
    var comboID = options.comboID || "";
    var optionText = options.optionText || "text";
    var optionValue = options.optionValue || "value";
    var optionAttributes = options.optionAttributes || [];
    var optionSelected = options.optionSelected || "";
    var filterField = options.filterField || "";
    var filterValue = options.filterValue || "";
    var onChange = options.onChange || "";
    var groupData = options.groupData || 0;
    var xmlDataStore = options.xmlDataStore || "";
    var comboIDOption = comboID.replace(/,/gi, " option,") + " option";
    var arrayOptions = [];
    var attributes = "";
    var T = this;


    //Remove old Values
    $(comboIDOption).remove();

    //Add first Blank Value
    arrayOptions.push("<option value=''></option>");


    if (window.DOMParser) {

        var xmlDoc = $.parseXML(xmlDataStore)
        $xmlDataStore = $(xmlDoc);

        $(xmlDataStore).find("row").each(function () {
            attributes = "";
            if ($(this).find(filterField).text() == filterValue) {

                for (var i = 0; i < optionAttributes.length; i++) {
                    attributes += " " + optionAttributes[i] + "=" + '"' + $(this).find(optionAttributes[i]).text() + '"';
                }
                arrayOptions.push("<option value='" + $(this).find(optionValue).text() + "' " + attributes + ">" + $(this).find(optionText).text() + "</option>");
            }


        });
    }
    else {

        $("<option value=''></option>").appendTo(comboID);
        $(xmlDataStore).find(".row").each(function () {
            attributes = "";
            if ($(this).find("." + filterField).html() == filterValue) {
                for (var i = 0; i < optionAttributes.length; i++) {
                    attributes += " " + optionAttributes[i] + "=" + '"' + $(this).find(optionAttributes[i]).text() + '"';
                }
                arrayOptions.push("<option value='" + $(this).find(optionValue).html() + "' " + attributes + ">" + $(this).find(optionText).html() + "</option>");
            }

        });
    }

    //Group Data
    if (groupData == 1) arrayOptions = $.unique(arrayOptions);

    //Assign Combo Values
    arrayOptions = arrayOptions.toString().replace(/,/gi, "");
    $(comboID).html(arrayOptions);

    //Events
    if (onChange != "")
        $(comboID).change(onChange);

    //auto select value
    if (optionSelected != "") {
        $(comboID).find("[value='" + optionSelected + "']").prop('selected', true);

    }

    //active change trigger
    //$(comboID).change(); //if enabled some problems

}
KXJS.loadDataCombo = function (options, dataStoreReturn) {

    //Set Variable
    var pluginName = options.plugin || "";
    var pluginAction = options.pluginAction || "";
    var pluginObject = options.pluginObject || "";
    var parameters = options.parameters || {};
    var doneFunction = options.done || function () {
        };
    parameters.OM = "XML";

    $.ajax({
        url: KXJS.buildAPICallUrl("KXCXML", pluginName, pluginObject, pluginAction),
        data: parameters
    }).done(function (data) {

        //Assign Data Combo
        options.xmlDataStore = data;
        KXJS.assignDataCombo(options);

        //Return Array Data
        if (typeof (dataStoreReturn) != "undefined")
            dataStoreReturn.store = data;

        //Execute done Function
        doneFunction(data);
    });
}

KXJS.logout = function () {
    $.ajax({
        url: KXJS.buildAPICallUrl("KXCJS", "k_secauth", "webservice", "logout")
    }).done(function (data) {
        var r = $.parseJSON(data);
        if (r.success == "true")
            window.location.reload();
    });
}