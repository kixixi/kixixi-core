<?
/**
 * KIXIXI k_webmanager (p_pages)
 * Create a panel web object for manage web sites pages
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_webmanager
 * @subpackage objects\p_pages
 * @since 1.00
 */
namespace k_webmanager\manage\panels\p_pages;

class obj extends \k_webmanager\objects\general\base_panel\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Constructor
     *
     */
    public function __construct($AV)
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
    }

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //set variable
        global $K_COMMON;
        $KCA = $K_COMMON->getVarRequest("KCA");
        $webCode = $this->K_COMMON->getEditingWeb();

        //Check Authorization
        if (!$this->isAuthorizedWebSiteLayout($webCode))
            return $this->K_COMMON->resultReturn("JSON", false, ["reason" => "errorSecurity", "message" => "Error Security"]);

        //get template
        switch ($KCA) {

            case "getModalAddKTemplate" :
                return json_encode($this->getModalAddKTemplate($ArrayVariable));
                break;
            case "getModalInstallKTemplateOnSite" :
                return json_encode($this->getModalInstallKTemplateOnSite($ArrayVariable));
                break;
            case "getTableListStandard":
                $tableList = $this->getTableListStandard($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
            case "getTableListAdvance":
                $tableList = $this->getTableListAdvance($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
            case "getTableListDeveloper":
                $tableList = $this->getTableListDeveloper($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
            case "save" :
                return json_encode($this->save($ArrayVariable));
                break;
            case "delete" :
                return json_encode($this->delete($ArrayVariable));
                break;
            case "openPage" :

                //set variable
                $urlBase = $this->K_COMMON->getVarArray($ArrayVariable, "urlBase");
                $url = $this->K_COMMON->getVarArray($ArrayVariable, "url");

                //redirect site to page url
                header("location: " . $urlBase . $url);
                break;
            case "getModalDeleteKTemplate" :
                return json_encode($this->getModalDeleteKTemplate($ArrayVariable));
                break;
            case "installKTemplate" :
                return json_encode($this->installKTemplate($ArrayVariable));
                break;
            case "uninstallKTemplate":
                return json_encode($this->uninstallKTemplate($ArrayVariable));
                break;
            case "saveKTemplate" :
                return json_encode($this->saveKTemplate($ArrayVariable));
                break;
            case "saveTpl" :
                $webTemplate = $this->getObject("web_templates");
                return json_encode($webTemplate->saveTplWeb($ArrayVariable));
                break;


        }
    }

    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "get", "put", "")
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {


        //include module
        $objApiWebManager = $this->K_COMMON->getPluginObject("k_webmanager", "api");

        //set variables
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");
        $this->urlActual = $this->K_COMMON->getVarArray($ArrayVariable, "panelUrlActual");
        $webCode = $this->K_COMMON->getEditingWeb();

        //make array crumbs
        $this->pageCrumbs[$objApiWebManager->getWebName(["webCode" => $webCode])] = "/manage/k_webmanager/p_pages/list";


        //Check Authorization
        if (!$this->isAuthorizedWebSiteLayout()) {
            //NEW ROW
            $this->appendRow("");
            $this->appendColumn($this->lastRow, ["nCol" => 12]);
            $this->appendHTML($this->lastCol, "ACCESS DENIED!");
            return $this;
        }

        switch ($pluginMethod) {
            case "list":
            case "":
                $this->getPageList($ArrayVariable);
                break;
            case "put":
            case "get":
                $ArrayVariable["urlExtraParam"] = ["webCode" => $webCode];
                $this->getPagePut($ArrayVariable);
                break;
            case "gettemplate":
                $ArrayVariable["urlExtraParam"] = ["webCode" => $webCode];
                $ArrayVariable["pluginMethod"] = "get";
                $this->getPagePutTemplate($ArrayVariable);
                break;
            case "puttemplate":
                $ArrayVariable["urlExtraParam"] = ["webCode" => $webCode];
                $ArrayVariable["pluginMethod"] = "put";
                $this->getPagePutTemplate($ArrayVariable);
                break;
            case "copytemplate":
                $ArrayVariable["urlExtraParam"] = ["webCode" => $webCode];
                $ArrayVariable["pluginMethod"] = "copy";
                $ArrayVariable["keyID"] = $ArrayVariable["pluginParam"];
                $ArrayVariable["copy"] = 1;
                $this->getPagePutTemplate($ArrayVariable);
                break;
        }

        //Return
        return $this;

    }

    protected function getPageList($ArrayVariable)
    {


        //set variable
        $pluginParam = $this->K_COMMON->getVarArray($ArrayVariable, "pluginParam");
        $webCode = $this->K_COMMON->getEditingWeb();

        //Create Layout
        $this->appendRow("", ["id" => "rowUpButtons", "margin" => "30px 0px 10px 0;"]);
        $this->appendColumn($this->lastRow, ["nCol" => 2]);
        $objHtmlComponents = $this->K_COMMON->getPluginObject("k_htmlcomponents", "dropdown");
        $arrayViewType = [];
        $arrayViewType[] = [
            "label" => $this->dictionary["LabelBasic"],
            "link" => $this->urlActual . "list"
        ];
        $arrayViewType[] = [
            "label" => $this->dictionary["LabelAdvance"],
            "link" => $this->urlActual . "list/advance"
        ];

        if ($this->isDeveloper())
            $arrayViewType[] = [
                "label" => $this->dictionary["labelDeveloperMode"],
                "link" => $this->urlActual . "list/developer"
            ];

        if ($pluginParam == "advance") {

            $labelDropDown = $this->dictionary["LabelAdvance"];
        } elseif ($pluginParam == "developer") {

            $labelDropDown = $this->dictionary["labelDeveloperMode"];
        }
        else {

            $labelDropDown = $this->dictionary["LabelBasic"];
        }
        $this->appendHTML($this->lastCol, $objHtmlComponents->get(["items" => $arrayViewType, "label" => $labelDropDown, "style" => "border:1px solid;"]));


        //get Page List
        $arrayVariableList = [];
        $arrayVariableList["pluginParam"] = $webCode;
        if ($pluginParam == "advance") {
            $this->getPageListAdvance($arrayVariableList);
        } elseif ($pluginParam == "developer") {
            $this->getPageListDeveloper($arrayVariableList);
        }
        else {
            $this->getPageListStandard($arrayVariableList);
        }

    }

    //STANDAR MODE
    protected function getPageListStandard($ArrayVariable)
    {



        //set variable
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "pluginParam");


        //hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "webCode", "name" => "webCode", "value" => $webCode]);

        //NEW ROW
        $this->appendRow("");

        $listMyMList = $this->getTableListStandard(["webCode" => $webCode]);
        $this->appendHTML("", $listMyMList);

        $this->appendColumn("rowUpButtons", ["nCol" => 2]);
        $this->appendInput($this->lastCol, ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "addModal"
            , "value" => $this->dictionary["LabelInstallKTemplate"]
            , "openModal" => [
                "idModal" => "installKTemplate"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalInstallKTemplateOnSite"
                , "parameters" => ["webCodeWhereInstall" => $webCode, "tableListToReload" => "tableList"]
            ]
        ]);

    }

    protected function getTableListStandard($ArrayVariable)
    {


        //include module
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");
        $objApiWebManager = $this->K_COMMON->getPluginObject("k_webmanager", "api");

        //load variable
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");
        $arrayWhere = [];
        $currentWebCode = $this->K_COMMON->getCurrentWeb();


        //check variable
        if ($webCode != "")
            $arrayWhere["wpg_web_code"] = $webCode;
        if ($onlyRow == "1") {
            $table = $this->getTable("web_pages");
            $table->getByQuery("web_pages_with_plugin_name"
                , ""
                , $arrayWhere, "", "", ["plg_name"]);
            $dataTable = $table->getResults("array");
        } else
            $dataTable = "";
        $OBJTableList->newTable(
            [
                "id" => "tableList"
                , "tableID" => "tableList"
                , "fieldsPageToGet" => ["webCode" => "webCode"]
                , "ajax" =>
                [
                    "kcp" => $this->getPluginName()
                    , "kco" => $this->getPluginObjectName()
                    , "kca" => "getTableListStandard"
                ]
                , "hideHead" => $onlyRow
                , "data" => $dataTable
                , "groupBy" =>
                [
                    [
                        "groupField" => "wpg_plugin_name"
                        , "qryField" => "plg_name"
                    ]
                    ,
                    [
                        "buttons" =>
                            [

                                [
                                    "title" => $this->dictionary["LabelAddPage"]
                                    , "action" => "link"
                                    , "class" => "fa fa-file-o"
                                    , "urlBaseType" => "panelActual"
                                    , "qryAttributes" => [
                                    "put" => "wpg_plugin_name"
                                ]
                                    , "dynamicAttributes" => []
                                ]
                                /*,
                                [
                                    "title" => $this->dictionary["LabelEditKTemplate"]
                                    , "action" => "link"
                                    , "class" => "fa fa-cogs"
                                    , "urlBaseType" => "panelActual"
                                    , "qryAttributes" => ["templates" => "wpg_web_code", "list" => "wpg_plugin_name"]
                                    , "dynamicAttributes" => []
                                ]*/

                            ]
                    ]
                ]
                , "fields" =>
                [
                    [
                        "label" => $this->dictionary["LabelName"]
                        , "qryField" => "wpg_name"
                    ]
                    ,
                    [
                        "label" => "URL"
                        , "qryField" => "urw_url"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                /*
                                [
                                    "title" => $this->dictionary["LabelEditWebPage"]
                                    , "action" => "edit"
                                    , "qryAttributes" => ["keyID" => "id_web_page"]
                                ]
                                */
                                [
                                    "title" => $this->dictionary["LabelEditWebPage"]
                                    , "action" => "link"
                                    , "class" => "fa fa-edit"
                                    , "url" => ""
                                    , "urlBaseType" => "panelActual"
                                    , "qryAttributes" => ["get" => "id_web_page"]
                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelEditWebTemplate"]
                                    , "action" => "link"
                                    , "qryAttributes" => ["keyID" => "wpg_tmpl_id"]
                                   // , "staticAttributes" => ["token" => $newIDSession]
                                    , "class" => "fa fa-arrows-alt"
                                    , "urlSeparator" => "&"
                                    , "url" => $objApiWebManager->getWebUrl(["webCode" => $currentWebCode]) . $this->K_COMMON->buildAPICallUrl("KXCH", "k_webmanager", "template_editor", "templateEditor")
                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelGoToPage"]
                                    , "action" => "link"
                                    , "qryAttributes" => ["url" => "urw_url"]
                                    , "staticAttributes" => ["urlBase" => $objApiWebManager->getWebUrl(["webCode" => $webCode])]
                                    , "class" => "fa fa-desktop"
                                    , "urlSeparator" => "&"
                                    , "url" => $this->K_COMMON->buildAPICallUrl("KXCH", "k_webmanager", "manage\\\\panels\\\\p_pages", "openPage")
                                ]
                                , [
                                "title" => $this->dictionary["LabelDelete"]
                                , "action" => "modalConfirm"
                                , "icon" => "fa-remove"
                                , "reloadOnSuccess" => "1"
                                , "qryAttributes" => ["keyID" => "wpg_tmpl_id"]
                                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                            ]
                            ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();
    }

    //ADVANCE MODE
    protected function getPageListAdvance($ArrayVariable)
    {

        //set variable
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "pluginParam");
        
        //NEW ROW
        $this->appendRow("");

        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "webCode", "name" => "webCode", "value" => $webCode]);

        $listMyMList = $this->getTableListAdvance(["webCode" => $webCode]);
        $this->appendHTML("", $listMyMList);


        $this->appendColumn("rowUpButtons", ["nCol" => 2]);
        $this->appendInput($this->lastCol, ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "addModalInstallKTemplate"
            , "value" => $this->dictionary["LabelInstallKTemplate"]
            , "openModal" => [
                "idModal" => "installKTemplate"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalInstallKTemplateOnSite"
                , "parameters" => ["webCodeWhereInstall" => $webCode, "tableListToReload" => "tableListAdvance"]
            ]
        ]);

        $this->appendColumn("rowUpButtons", ["nCol" => 2]);
        $this->appendInput($this->lastCol, ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "addModalCreateKTemplate"
            , "value" => $this->dictionary["LabelCreateKTemplate"]
            , "openModal" => [
                "idModal" => "addKTemplate"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAddKTemplate"
                , "parameters" => ["webCode" => $webCode, "tableListToReload" => "tableListAdvance"]
            ]
        ]);

    }

    protected function getTableListAdvance($ArrayVariable)
    {
        //include module
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");
        $objApiWebManager = $this->K_COMMON->getPluginObject("k_webmanager", "api");

        //set variables
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");
        $currentWebCode = $this->K_COMMON->getCurrentWeb();
        $arrayWhere = [];


        //check variable
        if ($webCode != "")
            $arrayWhere["tpl_web_code"] = $webCode;
        if ($onlyRow == "1") {
            $table = $this->getTable("web_pages");
            $table->getByQuery("web_templates_with_page"
                , ""
                , $arrayWhere, "", "", ["plg_name"]);
            $dataTable = $table->getResults("array");
        } else
            $dataTable = "";
        $OBJTableList->newTable(
            [
                "id" => "tableListAdvance"
                , "tableID" => "tableListAdvance"
                , "fieldsPageToGet" => ["webCode" => "webCode"]
                , "ajax" =>
                [
                    "kcp" => $this->getPluginName()
                    , "kco" => $this->getPluginObjectName()
                    , "kca" => "getTableListAdvance"
                ]
                , "hideHead" => $onlyRow
                , "data" => $dataTable
                , "groupBy" =>
                [
                    [
                        "groupField" => "tpl_plugin_name"
                        , "qryField" => "plg_name"
                    ]
                    ,
                    [
                        "buttons" =>
                            [

                                [
                                    "title" => $this->dictionary["LabelAddWebTemplate"]
                                    , "action" => "link"
                                    , "class" => "fa fa-file-image-o"
                                    , "urlBaseType" => "panelActual"
                                    , "qryAttributes" => ["puttemplate" => "tpl_plugin_name"]
                                    , "dynamicAttributes" => []
                                ],
                                [
                                    "title" => $this->dictionary["LabelAddPage"]
                                    , "action" => "link"
                                    , "class" => "fa fa-file-o"
                                    , "urlBaseType" => "panelActual"
                                    , "qryAttributes" => ["put" => "tpl_plugin_name"]
                                    , "dynamicAttributes" => []
                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelDeleteKTemplate"]
                                    , "action" => "modal"
                                    , "icon" => "fa-remove"
                                    , "qryAttributes" => ["pluginName" => "tpl_plugin_name","webCode"=>"tpl_web_code"]
                                    , "dynamicAttributes" => []
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalDeleteKTemplate"]
                                ]
                                /*
                                ,
                                [
                                    "title" => $this->dictionary["LabelEditKTemplate"]
                                    , "action" => "link"
                                    , "class" => "fa fa-cogs"
                                    , "urlBaseType" => "panelActual"
                                    , "qryAttributes" => ["templates" => "tpl_web_code", "list" => "tpl_plugin_name"]
                                    , "dynamicAttributes" => []
                                ]
                                */

                            ]
                    ]
                ]
                , "fields" =>
                [
                    [
                        "label" => $this->dictionary["LabelWebTemplateName"]
                        , "qryField" => "tpl_name"
                    ]
                    ,
                    [
                        "label" => $this->dictionary["LabelPage"]
                        , "qryField" => "wpg_name"
                    ]
                    ,
                    [
                        "label" => "URL"
                        , "qryField" => "urw_url"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->dictionary["LabelEditWebPage"]
                                    , "action" => "link"
                                    , "class" => "fa fa-edit"
                                    , "showIF" => ["isPage" => "1"]
                                    , "url" => ""
                                    , "urlBaseType" => "panelActual"
                                    , "qryAttributes" => ["get" => "id_web_page"]
                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelEditWebTemplate"]
                                    , "action" => "link"
                                    , "class" => "fa fa-edit"
                                    , "showIF" => ["isPage" => "0"]
                                    , "urlBaseType" => "panelActual"
                                    , "qryAttributes" => ["gettemplate" => "id_web_template"]
                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelEditWebTemplate"]
                                    , "action" => "link"
                                    , "qryAttributes" => ["keyID" => "id_web_template"]
                                    , "staticAttributes" => ["token" => $this->K_COMMON->getCurrentSessionID()]
                                    , "class" => "fa fa-arrows-alt"
                                    , "urlSeparator" => "&"
                                    , "url" => $objApiWebManager->getWebUrl(["webCode" => $currentWebCode]) . $this->K_COMMON->buildAPICallUrl("KXCH", "k_webmanager", "template_editor", "templateEditor")
                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelGoToPage"]
                                    , "action" => "link"
                                    , "showIF" => ["isPage" => "1"]
                                    , "qryAttributes" => ["url" => "urw_url"]
                                    , "staticAttributes" => ["urlBase" => $objApiWebManager->getWebUrl(["webCode" => $webCode])]
                                    , "class" => "fa fa-desktop"
                                    , "urlSeparator" => "&"
                                    , "url" => $this->K_COMMON->buildAPICallUrl("KXCH", "k_webmanager", "manage\\\\panels\\\\p_pages", "openPage")
                                ]
                                , [
                                "title" => $this->dictionary["LabelDelete"]
                                , "action" => "modalConfirm"
                                , "reloadOnSuccess" => "1"
                                , "icon" => "fa-remove"
                                , "showIF" => ["isPage" => "1"]
                                , "qryAttributes" => ["keyID" => "id_web_template"]
                                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                            ]
                                , [
                                "title" => $this->dictionary["LabelDeleteWebTemplate"]
                                , "action" => "modalConfirm"
                                , "reloadOnSuccess" => "1"
                                , "icon" => "fa-remove"
                                , "showIF" => ["isPage" => "0"]
                                , "qryAttributes" => ["keyID" => "id_web_template"]
                                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                            ]
                            ]
                    ]
                ]
            ]
        );
        return $OBJTableList->get();

    }

    //DEVELOPER MODE
    protected function getPageListDeveloper($ArrayVariable)
    {

        //set variable
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "pluginParam");

        //NEW ROW
        $this->appendRow("");

        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "webCode", "name" => "webCode", "value" => $webCode]);

        $listMyMList = $this->getTableListDeveloper(["webCode" => $webCode]);
        $this->appendHTML("", $listMyMList);


        $this->appendColumn("rowUpButtons", ["nCol" => 2]);
        $this->appendInput($this->lastCol, ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "addModalInstallKTemplate"
            , "value" => $this->dictionary["LabelInstallKTemplate"]
            , "openModal" => [
                "idModal" => "installKTemplate"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalInstallKTemplateOnSite"
                , "parameters" => ["webCodeWhereInstall" => $webCode, "tableListToReload" => "tableListAdvance"]
            ]
        ]);

        $this->appendColumn("rowUpButtons", ["nCol" => 2]);
        $this->appendInput($this->lastCol, ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success",
            "id" => "addModalCreateKTemplate"
            , "value" => $this->dictionary["LabelCreateKTemplate"]
            , "openModal" => [
                "idModal" => "addKTemplate"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "getModalAddKTemplate"
                , "parameters" => ["webCode" => $webCode, "tableListToReload" => "tableListAdvance"]
            ]
        ]);

    }

    protected function getTableListDeveloper($ArrayVariable)
    {
        //include module
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");
        $objApiWebManager = $this->K_COMMON->getPluginObject("k_webmanager", "api");

        //set variables
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");
        $currentWebCode = $this->K_COMMON->getCurrentWeb();
        $arrayWhere = [];


        //check variable
        if ($webCode != "")
            {
                $arrayWhere["plg_type"] = "ktemplate";
                $bindWhere["tplWebCode"] = $webCode;
            }
        if ($onlyRow == "1") {
            $table = $this->getTable("web_pages");
            //$table->showSQL();
            $table->getByQuery("web_templates_with_page_developer"
                , ""
                , $arrayWhere, $bindWhere, "");
            $dataTable = $table->getResults("array");
        } else
            $dataTable = "";
        $OBJTableList->newTable(
            [
                "id" => "tableListAdvance"
                , "tableID" => "tableListAdvance"
                , "fieldsPageToGet" => ["webCode" => "webCode"]
                , "ajax" =>
                [
                    "kcp" => $this->getPluginName()
                    , "kco" => $this->getPluginObjectName()
                    , "kca" => "getTableListDeveloper"
                ]
                , "hideHead" => $onlyRow
                , "data" => $dataTable
                , "groupBy" =>
                [
                    [
                        "groupField" => "plg_id"
                        , "qryField" => "plg_name"
                    ]
                    ,
                    [
                        "buttons" =>
                            [

                                [
                                    "title" => $this->dictionary["LabelAddWebTemplate"]
                                    , "action" => "link"
                                    , "class" => "fa fa-file-image-o"
                                    , "urlBaseType" => "panelActual"
                                    , "qryAttributes" => ["puttemplate" => "plg_id"]
                                    , "dynamicAttributes" => []
                                ],
                                [
                                    "title" => $this->dictionary["LabelAddPage"]
                                    , "action" => "link"
                                    , "class" => "fa fa-file-o"
                                    , "urlBaseType" => "panelActual"
                                    , "qryAttributes" => ["put" => "plg_id"]
                                    , "dynamicAttributes" => []
                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelDeleteKTemplate"]
                                    , "action" => "modal"
                                    , "icon" => "fa-remove"
                                    , "qryAttributes" => ["pluginName" => "plg_id","webCode"=>"tpl_web_code"]
                                    , "dynamicAttributes" => []
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalDeleteKTemplate"]
                                ]
                                /*
                                ,
                                [
                                    "title" => $this->dictionary["LabelEditKTemplate"]
                                    , "action" => "link"
                                    , "class" => "fa fa-cogs"
                                    , "urlBaseType" => "panelActual"
                                    , "qryAttributes" => ["templates" => "tpl_web_code", "list" => "tpl_plugin_name"]
                                    , "dynamicAttributes" => []
                                ]
                                */

                            ]
                    ]
                ]
                , "fields" =>
                [
                    [
                        "label" => $this->dictionary["LabelWebTemplateName"]
                        , "qryField" => "tpl_name"
                    ]
                    ,
                    [
                        "label" => $this->dictionary["LabelPage"]
                        , "qryField" => "wpg_name"
                    ]
                    ,
                    [
                        "label" => "URL"
                        , "qryField" => "urw_url"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->dictionary["LabelEditWebPage"]
                                    , "action" => "link"
                                    , "class" => "fa fa-edit"
                                    , "showIF" => ["isPage" => "1"]
                                    , "url" => ""
                                    , "urlBaseType" => "panelActual"
                                    , "qryAttributes" => ["get" => "id_web_page"]
                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelEditWebTemplate"]
                                    , "action" => "link"
                                    , "class" => "fa fa-edit"
                                    , "showIF" => ["isPage" => "0", "isTemplate" => "1"]
                                    , "urlBaseType" => "panelActual"
                                    , "qryAttributes" => ["gettemplate" => "id_web_template"]
                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelEditWebTemplate"]
                                    , "action" => "link"
                                    , "qryAttributes" => ["keyID" => "id_web_template"]
                                    , "staticAttributes" => ["token" => $this->K_COMMON->getCurrentSessionID()]
                                    , "class" => "fa fa-arrows-alt"
                                    , "urlSeparator" => "&"
                                    , "showIF" => ["isTemplate" => "1"]
                                    , "url" => $objApiWebManager->getWebUrl(["webCode" => $currentWebCode]) . $this->K_COMMON->buildAPICallUrl("KXCH", "k_webmanager", "template_editor", "templateEditor")
                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelGoToPage"]
                                    , "action" => "link"
                                    , "showIF" => ["isPage" => "1"]
                                    , "qryAttributes" => ["url" => "urw_url"]
                                    , "staticAttributes" => ["urlBase" => $objApiWebManager->getWebUrl(["webCode" => $webCode])]
                                    , "class" => "fa fa-desktop"
                                    , "urlSeparator" => "&"
                                    , "url" => $this->K_COMMON->buildAPICallUrl("KXCH", "k_webmanager", "manage\\\\panels\\\\p_pages", "openPage")
                                ]
                                ,
                                [
                                "title" => $this->dictionary["LabelDelete"]
                                , "action" => "modalConfirm"
                                , "reloadOnSuccess" => "1"
                                , "icon" => "fa-remove"
                                , "showIF" => ["isPage" => "1"]
                                , "qryAttributes" => ["keyID" => "id_web_template"]
                                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                            ]

                            ]
                    ]
                ]
            ]
        );
        return $OBJTableList->get();

    }

    /**
     * get modal delete KTemplate
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *      *   type: type of modal
     *      *   idModal: id to use on modal
     *
     * @return array
     */
    protected function getModalDeleteKTemplate($ArrayVariable)
    {

        //Get Variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");
        $pluginName =  $this->K_COMMON->getVarArray($ArrayVariable, "pluginName");
        $webCode =  $this->K_COMMON->getVarArray($ArrayVariable, "webCode");

        $body = $this->getDictionaryWord("msgConfirmRemoveKTemplate");

        return $this->K_COMMON->getModalAskConfirm(
            [
                "idModal" => $idModal
                , "type" => "deleteWithCheck"
                , "title" => $this->getDictionaryWord("LabelRemoveKTemplate")
                , "body" => $body
                , "buttons" => [
                [
                    "title" => $this->getDictionaryWord("No")
                    , "value" => $this->getDictionaryWord("No")
                    , "name" => "btnNo"
                    , "onClick" => "general"
                    , "closeModalNow" => $idModal
                ]
                ,
                [
                    "title" => $this->getDictionaryWord("Yes")
                    , "value" => $this->getDictionaryWord("Yes")
                    , "disableWithCheck" => "1"
                    , "name" => "btnYes"
                    , "semanticColor" => "danger"
                    , "staticAttributes" => ["plgID" => $pluginName,"webCodeWhereUninstall"=>$webCode]
                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "uninstallKTemplate"]
                    , "onClick" => "ajax"
                    , "reloadPageActual" => "1"



                ]
            ]
            ]);

    }

    protected function getPagePut($ArrayVariable)
    {

        //include object
        $objURLRewrite = $this->K_COMMON->getPluginObject("k_webmanager", "urlrewrite");
        $objPlugins = $this->K_COMMON->getPluginObject("k_common", "plugins");
        $tablePages = $this->getTable("web_pages");
        $tableWebTemplates = $this->getTable("web_templates");

        // set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");
        $extraParam = $this->K_COMMON->getVarArray($ArrayVariable, "urlExtraParam");
        $pluginParam = $this->K_COMMON->getVarArray($ArrayVariable, "pluginParam");
        $keyID = "";
        $plugin = "";
        if ($pluginMethod == "put") {
            $plugin = $pluginParam;
        } elseif ($pluginMethod == "get") {
            $keyID = $pluginParam;
        }

        $webCode = $this->K_COMMON->getVarArray($extraParam, "webCode");
        $value_webCode = $webCode;
        $value_plugin = $plugin;
        $value_object = "web\\templates\\default";
        $value_name = "";
        $value_url = "";
        $value_title = "";
        $checkUserLogged = "";
        $value_include = [];
        $tplID = "";
        //$disabledCombo = "";
        $pageType = "generic";

        //Load Data
        if ($keyID != "") {
            $tablePages->get("*", ["id_web_page" => $keyID, "wpg_web_code" => $webCode]);
            if ($row = $tablePages->fetch()) {
                $value_webCode = $row->wpg_web_code;
                $value_name = $row->wpg_name;
                $value_title = $row->wpg_title;
                $value_plugin = $row->wpg_plugin_name;
                $value_object = $row->wpg_plugin_object;
                $checkUserLogged = $row->wpg_check_logged;
                $tplID = $row->wpg_tmpl_id;

                $tableWebTemplates->get("*", ["id_web_template" => $tplID]);
                $rowWebTemplate = $tableWebTemplates->fetch();
                if ($rowWebTemplate) {
                    $value_name = $rowWebTemplate->tpl_name;
                    $value_include_tmp = $rowWebTemplate->tpl_include;
                    $value_plugin = $rowWebTemplate->tpl_plugin_name;

                    //convert include
                    if ($value_include_tmp != "") {

                        $value_include_tmp = (array)json_decode($value_include_tmp, true);

                        foreach ($value_include_tmp as $k => $v) {


                            foreach ($v as $file) {

                                $value_include[] = ["value" => $k, "text" => $file];

                            }
                        }
                        $value_include = json_encode($value_include);

                    }
                }

                //make array crumbs
                $plgDisplayName = $objPlugins->getPluginDisplayName($value_plugin);
                $this->pageCrumbs[$plgDisplayName] = "";
                $this->pageCrumbs[$value_name] = "";

                //get url
                $value_url = $objURLRewrite->getURL([
                        "pluginName" => $value_plugin
                        , "pluginObject" => $value_object
                        , "refCode" => $keyID
                        , "webCode" => $value_webCode
                    ]
                );

                $paramURLLogin = $this->getParamValue("loginURL", "", "k_webmanager");

                if ($value_url == $paramURLLogin)
                    $pageType = "login";
                else {
                    $paramURLPanel = $this->getParamValue("panelURL", "",  "k_panelmanage");
                    if ($value_url == $paramURLPanel)
                        $pageType = "manage";
                }
            } else
                return;
        } else {
            //make array crumbs
            $plgDisplayName = $objPlugins->getPluginDisplayName($value_plugin);
            $this->pageCrumbs[$plgDisplayName] = "";
            $this->pageCrumbs["New"] = "";
        }

        //Append Form
        $this->appendForm("",
            [
                "id" => "pageForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "save"
                , "reloadPage" => "1"
                , "reloadPageCheckCode" => "keyID"
                , "reloadPageGetCode" => "keyID"

            ]
        );

        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "keyID", "value" => $keyID]);
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "webCode", "name" => "webCode", "value" => $value_webCode]);
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "pluginName", "name" => "pluginName", "value" => $value_plugin]);
        $this->appendInput($this->lastForm, ["type" => "hidden", "name" => "pluginObject", "value" => $value_object]);

        $this->appendRow($this->lastForm);
        $label = $this->dictionary["LabelName"];
        $name = "name";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name
            , "value" => $value_name, "required" => "1"]);

        //combo for type page
        $label = $this->dictionary["labelPageType"];
        $name = "pageType";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select"
                , "labelInput" => $label
                , "id" => $name
                , "name" => $name
                , "value" => $pageType
                , "typeLoad" => "array"
                , "options" =>
                [
                    "generic" => $this->getDictionaryWord("labelPageTypeGeneric")
                    , "login" => $this->getDictionaryWord("labelPageTypeLogin")
                    , "manage" => $this->getDictionaryWord("labelPageTypeManage")
                ]
            ]
        );

        $this->appendRow($this->lastForm);
        $label = $this->dictionary["LabelPageTitle"];
        $name = "title";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name
            , "value" => $value_title, "required" => "0"]);


        //if ($keyID != "")
        //   $disabledCombo = "1";


        $this->appendRow($this->lastForm);
        $label = $this->dictionary["LabelUrl"];
        $name = "url";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name
            , "value" => $value_url, "required" => "1"]);


        //Checkbox for user logged
        //NEW ROW
        $this->appendRow($this->lastForm);

        //checkUserLogged
        $label = $this->dictionary["labelCheckUserLogged"];
        $name = "checkUserLogged";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => $checkUserLogged]);


        //Heading
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->dictionary["LabelTplHeadAddFile"], "importance" => "5"]);

        //Fields for insert new value
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $name = "includeGroup";
        $this->appendListGroupIncludeFileForm($this, $this->lastCol, $name, $value_include);

        $this->appendRow($this->lastForm, ["margin" => "10px 0"]);
        $label = $this->dictionary["LabelSave"];
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);


        if ($tplID != "") {

            $this->appendColumn($this->lastRow, ["nCol" => 3]);
            $label = $this->dictionary["LabelBtnModifyTemplate"];
            $name = "btnModifyTemplate";
            $this->appendInput($this->lastCol,
                [
                    "type" => "btn"
                    , "value" => $label
                    , "id" => $name
                    , "name" => $name
                    , "semanticColor" => "primary"
                    , "link" => $this->K_COMMON->buildAPICallUrl("KXCH", "k_webmanager", "template_editor", "templateEditor", "keyID=" . $tplID)
                    , "linkTarget" => "_blank"
                ]
            );
        }


    }

    protected function delete($ArrayVariable)
    {
        //set variables
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");


        //include modules
        $objURLRewrite = $this->K_COMMON->getPluginObject("k_webmanager", "urlrewrite");

        //include table
        $tablePage = $this->getTable("web_pages");
        $tableWebTemplate = $this->getTable("web_templates");


        //Load Data
        if ($keyID != "") {
            $tablePage->get(["*"], ["wpg_tmpl_id" => $keyID]);
            if ($row = $tablePage->fetch()) {
                $webCode = $row->wpg_web_code;
                $plugin = $row->wpg_plugin_name;
                $object = $row->wpg_plugin_object;
                $pageID = $row->id_web_page;

                //delete page with web template associated
                $tablePage->delete(["id_web_page" => $pageID]);

                //remove url
                $objURLRewrite->removeURL(
                    [
                        "pluginName" => $plugin
                        , "pluginObject" => $object
                        , "webCode" => $webCode
                        , "refCode" => $pageID
                    ]
                );


            }
        } else
            return array("success" => "false", "reason" => "errorSecurity");

        //delete web template
        $tableWebTemplate->delete(["id_web_template" => $keyID]);

        return ["success" => "true"];

    }

    protected function save($ArrayVariable)
    {

        //include object
        $objURLRewrite = $this->K_COMMON->getPluginObject("k_webmanager", "urlrewrite");
        $objWebTemplate = $this->getObject("web_templates");

        //include table
        $table = $this->getTable("web_pages");

        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $name = $this->K_COMMON->getVarArray($ArrayVariable, "name");
        $title = $this->K_COMMON->getVarArray($ArrayVariable, "title");
        $url = $this->K_COMMON->getVarArray($ArrayVariable, "url");
        $pluginName = $this->K_COMMON->getVarArray($ArrayVariable, "pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $checkUserLogged = $this->K_COMMON->getVarArray($ArrayVariable, "checkUserLogged");
        $pageType = $this->K_COMMON->getVarArray($ArrayVariable, "pageType");
        $include = $this->K_COMMON->getVarArray($ArrayVariable, "include");
        $saveEditFilter = [];

        if ($checkUserLogged == "")
            $checkUserLogged = 0;

        $saveData = [
            "wpg_name" => $name
            , "wpg_title" => $title
            , "wpg_check_logged" => $checkUserLogged
        ];




        //Verify Url Rewrite
        $urlExist = $objURLRewrite->checkExistURL(
            [
                "pluginName" => $pluginName
                , "pluginObject" => $pluginObject
                , "refCode" => $keyID
                , "webCode" => $webCode
                , "url" => $url
            ]);

        if ($urlExist)
            return ["success" => "false", "reason" => "URLDuplicated", "message" => "URL Duplicated"];

        //check security
        if ($keyID != "") {
            $saveEditFilter = ["id_web_page" => $keyID];
            $table->get(["wpg_tmpl_id"], ["id_web_page" => $keyID]);
            $row = $table->fetch();
            if ($row) {
                $tmplID = $row->wpg_tmpl_id;
            } else
                return ["success" => "false", "reason" => "securityError", "message" => "Security Error"];
        } else {

            //include module
            $objWebTemplate = $this->getObject("web_templates");

            //create web template
            $arrayRes = $objWebTemplate->saveTplWeb([
                "pluginName" => $pluginName
                , "pluginObject" => $pluginObject
                , "name" => $name
                , "code" => ""
                , "webCode" => $webCode
                , "tplType" => "web"
                , "include" => ["plugin" => ["jquery", "bootstrap"]]
            ]);
            if ($arrayRes["success"] == "true") {
                $tmplID = $arrayRes["keyID"];
            } else
                return $arrayRes;

            $saveData["wpg_web_code"] = $webCode;
            $saveData["wpg_tmpl_id"] = $tmplID;
            $saveData["wpg_plugin_name"] = $pluginName;
            $saveData["wpg_plugin_object"] = $pluginObject;

        }

        $table->put($saveData, $saveEditFilter);

        if ($keyID == "")
            $keyID = $table->getNewID();

        //Set Url Rewrite
        $objURLRewrite->createURL([
            "pluginName" => $pluginName
            , "pluginObject" => $pluginObject
            , "refCode" => $keyID
            , "webCode" => $webCode
            , "url" => $url
            , "pageID" => $keyID
        ]);

        switch ($pageType) {
            case "login":
                $this->setParamValue("loginURL", $webCode, $url, "k_webmanager", "");
                break;
            case "manage":
                $this->setParamValue("panelURL", $webCode, $url, "k_webmanager", "");
                break;
        }

        $arrayTpl = [
            "keyID" => $tmplID
            , "webCode" => $webCode
            , "name" => $name
            , "include" => $include
        ];

        // save web template
        $objWebTemplate->saveTplWeb($arrayTpl);

        return ["success" => "true", "message" => $this->dictionary["msgSuccessSave"], "keyID" => $keyID];
    }

    /**
     * get modal Install on selected Site The KTemplate chose
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *      *   idModal: id to use on modal
     *      *   webCode: web Code To Copy
     *      *   plgID:  IDKTemplate (real DB and Folder NAME)
     *      *   webCodeWhereInstall:    code of web site where install the ktemplate
     *
     * @return array
     */
    protected function getModalInstallKTemplateOnSite($ArrayVariable)
    {
        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");
        $webCodeToCopy = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $webCodeWhereInstall = $this->K_COMMON->getVarArray($ArrayVariable, "webCodeWhereInstall");

        //include modal object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        //create modal
        $webModal->newModal(["id" => $idModal, "title" => $this->dictionary["LabelTitleInstallKTemplate"], "type" => "normal"]);

        //create form
        $webModal->appendForm($webModal->modalBody,
            [
                "id" => "installKTemplateForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "installKTemplate"
                , "closeModal" => $idModal
                , "reloadPageActual" => "1"
            ]
        );
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "name" => "plgID", "value" => $plgID]);
        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "name" => "webCodeWhereInstall", "value" => $webCodeWhereInstall]);

        //NEW ROW
        $webModal->appendRow($webModal->lastForm);
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
        $label = $this->dictionary["LabelInstallKTemplate"];
        $webModal->appendWO($webModal->lastCol, "k_usefulobjects", "web\objects\w_comboplugin"
            , [
                "labelInput" => $label
                , "name" => "plgID"
                , "setDefaultValue" => ""
                , "noEmptyOption" => "1"
                , "functionVariables" => ["pluginType" => ["KTemplate" => "1"]]
            ]

        );
        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //button
        $label = $this->getDictionaryWord("LabelFormInstall");
        $name = "btnSave";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 3]);
        $webModal->appendInput($webModal->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);


        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];
    }

    /**
     * install new k_template
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *      *   webCodeWhereInstall: code of web site where install
     *      *   webCode: code of web site to copy for install
     *      *   plgID:  IDKTemplate (real DB and Folder NAME)
     *
     * @return array
     */
    protected function installKTemplate($ArrayVariable)
    {
        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $webCodeWhereInstall = $this->K_COMMON->getVarArray($ArrayVariable, "webCodeWhereInstall");
        $webCodeSource = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");

        //include object
        $objWebTemplate = $this->getObject("web_templates");
        if ($webCodeSource != "")
            $objResult = $objWebTemplate->copyKTemplate(["pluginName" => $plgID, "webCodeSource" => $webCodeSource, "webCodeTarget" => $webCodeWhereInstall]);
        else
            $objResult = $objWebTemplate->installKTemplate(["pluginName" => $plgID, "webCodeTarget" => $webCodeWhereInstall]);

        return $objResult;
    }

    /**
     * get modal add new k_template
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *      *   idModal: id to use on modal
     *
     * @return array
     */
    protected function getModalAddKTemplate($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $tableListToReload = $this->K_COMMON->getVarArray($ArrayVariable, "tableListToReload");
        if ($tableListToReload == "")
            $tableListToReload = "tableListKTemplate";
        //include modal object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        //create modal
        $webModal->newModal(["id" => $idModal, "title" => $this->dictionary["LabelAddKTemplate"], "type" => "normal"]);

        //create form
        $webModal->appendForm($webModal->modalBody,
            [
                "id" => "addKTemplateForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->getPluginName()
                , "kco" => $this->getPluginObjectName()
                , "kca" => "saveKTemplate"
                , "closeModal" => $idModal
                , "reloadTableList" => $tableListToReload
            ]
        );

        $webModal->appendInput($webModal->lastForm, ["type" => "hidden", "name" => "webCode", "value" => $webCode]);

        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //KTemplate ID
        $label = $this->getDictionaryWord("LabelFormKTemplateID");
        $name = "KTemplateID";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
        $webModal->appendInput($webModal->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "", "required" => "1"]);

        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //KTemplate Name
        $label = $this->getDictionaryWord("LabelFormKTemplateName");
        $name = "KTemplateName";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 6]);
        $webModal->appendInput($webModal->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "", "required" => "1"]);

        //NEW ROW
        $webModal->appendRow($webModal->lastForm);

        //button
        $label = $this->getDictionaryWord("LabelFormSave");
        $name = "btnSave";
        $webModal->appendColumn($webModal->lastRow, ["nCol" => 3]);
        $webModal->appendInput($webModal->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);


        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];
    }

    /**
     * save new k_template general
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *      *   KTemplateName: k_template name
     *      *   KTemplateID: k_template code
     *
     * @return array
     */
    protected function saveKTemplate($ArrayVariable)
    {
        //set variable
        $KTemplateID = $this->K_COMMON->getVarArray($ArrayVariable, "KTemplateID");
        $KTemplateName = $this->K_COMMON->getVarArray($ArrayVariable, "KTemplateName");
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");

        //include object
        $objTabPagePlugin = $this->K_COMMON->getPluginObject("k_pluginmanage", "tab_packageplugin");


        //save Data
        $resBuildPlugin = $objTabPagePlugin->buildPlugin(
            [
                "plgID" => $KTemplateID,
                "plgName" => $KTemplateName,
                "plgType" => "ktemplate",
                "webCode" => $webCode
            ]
        );
        if ($resBuildPlugin["success"] == "true")
            return ["success" => "true"];
        else
            return $resBuildPlugin;

    }


    /**
     * uninstall k_template
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *      *   webCodeWhereUninstall: code of web site where ar installed and need to uninstall
     *      *   plgID:  IDKTemplate (real DB and Folder NAME)
     *
     * @return array
     */
    protected function uninstallKTemplate($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $webCodeWhereUninstall = $this->K_COMMON->getVarArray($ArrayVariable, "webCodeWhereUninstall");

        //include object
        $objWebTemplate = $this->getObject("web_templates");

        $objResult = $objWebTemplate->deleteKTemplate(["pluginName" => $plgID, "webCode" => $webCodeWhereUninstall]);

        return $objResult;
    }

    protected function appendListGroupIncludeFileForm($objTo, $id, $name, $value)
    {

        //List Group of options
        $objTo->appendListGroup($id,
            ["id" => $name
                , "attributes" => 'style="height:135px; overflow: scroll; overflow-x: hidden;"'
                , "values" => $value
                , "addingValues" =>
                ["valuesHName" => "include"
                    , "fields" => [["type" => "select"
                    , "labelInput" => "Type"
                    , "name" => "value"
                    , "required" => "0"
                    , "typeLoad" => "array"
                    , "noEmptyOption" => "1"
                    , "options" => ["css" => "css", "js" => "js", "plugin" => "plugin"]]
                    , ["type" => "text", "labelInput" => "File", "id" => "fileName", "name" => "text"]]]]
        );
    }

    //TEMPLATE

    /**
     * get page for create/modify template
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    keyID: the plugin param (keyID)
     *
     */
    protected function getPagePutTemplate($ArrayVariable)
    {
        //include module
        $objPlugins = $this->K_COMMON->getPluginObject("k_common", "plugins");

        // set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");
        $extraParam = $this->K_COMMON->getVarArray($ArrayVariable, "urlExtraParam");
        $pluginParam = $this->K_COMMON->getVarArray($ArrayVariable, "pluginParam");
        $keyID = "";
        $plgID = "";
        if ($pluginMethod == "put") {
            $plgID = $pluginParam;
        } elseif ($pluginMethod == "get") {
            $keyID = $pluginParam;
        }

        $webCode = $this->K_COMMON->getVarArray($extraParam, "webCode");
        $copy = $this->K_COMMON->getVarArray($ArrayVariable, "copy");
        $panelUrlActual = $this->K_COMMON->getVarArray($ArrayVariable, "panelUrlActual");
        $disabledFields = "";
        $value_name = "";
        $value_code = "";
        $value_file = "";
        $value_include = "";
        $value_plugin = $plgID;
        $value_object = "web\\templates\\default";
        $value_tplType = "web";

        //get data template
        if ($keyID != "") {
            //load data template
            $table = $this->getTable("web_templates");
            $table->get("*", ["id_web_template" => $keyID]);
            $rowObj = $table->fetch();
            if ($rowObj) {
                $webCode = $rowObj->tpl_web_code;
                $value_name = $rowObj->tpl_name;
                $value_code = $rowObj->tpl_code;
                $value_file = $rowObj->tpl_file;
                $value_include_tmp = $rowObj->tpl_include;
                $value_plugin = $rowObj->tpl_plugin_name;
                $value_object = $rowObj->tpl_plugin_object;
                $value_tplType = $rowObj->tpl_type;

                //convert include
                if ($value_include_tmp != "") {

                    $value_include_tmp = (array)json_decode($value_include_tmp, true);
                    foreach ($value_include_tmp as $k => $v) {
                        foreach ($v as $file) {
                            $value_include[] = ["value" => $k, "text" => $file];
                        }
                    }
                    $value_include = json_encode($value_include);
                }

                //make array crumbs
                $plgDisplayName = $objPlugins->getPluginDisplayName($value_plugin);
                $this->pageCrumbs[$plgDisplayName] = "";
                $this->pageCrumbs[$value_name] = "";

            }
        } else {

            //make array crumbs
            $plgDisplayName = $objPlugins->getPluginDisplayName($value_plugin);
            $this->pageCrumbs[$plgDisplayName] = "aaa";
            $this->pageCrumbs["New"] = "";


        }




        //create page put form
        $arrayForm = [
            "id" => "tplForm"
            , "setSavedMessage" => "1"
            , "kcp" => $this->namePlugin
            , "kco" => $this->namePluginObject
            , "kca" => "saveTpl"
            , "inline" => "1"
        ];
        if ($keyID == "") {
            $arrayForm["reloadPage"] = "1";
            $arrayForm["reloadPageCheckCode"] = "keyID";
            $arrayForm["reloadPageGetCode"] = "keyID";
            $arrayForm["reloadPluginMethod"] = "gettemplate";
        }
        if ($copy == 1) {
            $value_code = "";
            $arrayForm["reloadPage"] = "1";
            $arrayForm["reloadPageCheckCode"] = "keyID";
            $arrayForm["reloadPageGetCode"] = "keyID";
            $arrayForm["reloadPageURL"] = $panelUrlActual;
        }

        //create form
        $this->appendForm("", $arrayForm);

        //check if copy
        if ($copy == 1) {
            $name = "copyID";
            $this->appendInput($this->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $keyID]);
            $keyID = ""; //reset keyID
            $value_name = $value_name . " copy";
        }

        //hidden variables
        $name = "keyID";
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => $name, "name" => $name, "value" => $keyID]);
        $this->appendInput($this->lastForm, ["type" => "hidden", "name" => "tplType", "value" => $value_tplType]);
        $this->appendInput($this->lastForm, ["type" => "hidden", "name" => "pluginName", "value" => $value_plugin]);
        $this->appendInput($this->lastForm, ["type" => "hidden", "name" => "pluginObject", "value" => $value_object]);
        $this->appendInput($this->lastForm, ["type" => "hidden", "name" => "webCode", "value" => $webCode]);

        if ($keyID != "") {
            $disabledFields = "1";
            $this->appendInput($this->lastForm, ["type" => "hidden", "name" => "code", "value" => $value_code]);
        }
        //NEW ROW
        $this->appendRow($this->lastForm);
        /* DEPRECATED, CAN BE RESTORED
                //plugin
                $label = $this->dictionary["labelFormPlugin"];
                $this->appendColumn($this->lastRow, ["nCol" => 3]);
                $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_comboplugin", ["labelInput" => $label, "value" => $value_plugin, "disabled" => "1"]);

                //NEW ROW
                $this->appendRow($this->lastForm);


                //object
                $label = $this->dictionary["labelFormObj"];
                $this->appendColumn($this->lastRow, ["nCol" => 3]);
                $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_combopluginwebtemplate", ["labelInput" => $label, "value" => $value_object, "disabled" => "1"]);

                //NEW ROW
                $this->appendRow($this->lastForm);

                //web site
                $label = $this->dictionary["LabelWebSite"];
                $this->appendColumn($this->lastRow, ["nCol" => 3]);
                $arrayParam = [
                    "labelInput" => $label
                    , "required" => "1"
                    , "value" => $webCode
                    , "triggerEvent" => "change"
                    , "triggerFunction" => "kx_tablelist_script_tableList.loadTableList"
                    , "disabled" => "1"
                ];
                $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_combowebsites"
                    , $arrayParam
                );
        */

        //NEW ROW
        $this->appendRow($this->lastForm);

        //NEW COLUMN
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $label = $this->dictionary["LabelTplCode"];
        $name = "code";
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_code, "required" => "1", "disabled" => $disabledFields]);


        //NEW ROW
        $this->appendRow($this->lastForm);

        //NEW COLUMN
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $label = $this->dictionary["LabelTplName"];
        $name = "name";
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_name, "required" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //NEW COLUMN
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $label = $this->dictionary["LabelTplFile"];
        $textHelp = $this->dictionary["LabelTplFileHelp"];
        $name = "file";
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_file, "textHelp" => $textHelp]);

        //New ROW
        $this->appendRow($this->lastForm);

        //Heading
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_hr");
        $this->appendWO($this->lastCol, "k_htmlcomponents", "web\objects\w_heading", ["title" => $this->dictionary["LabelTplHeadAddFile"], "importance" => "5"]);

        //Fields for insert new value
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow, ["nCol" => 6]);

        $name = "includeGroup";
        $this->appendListGroupIncludeFileForm($this, $this->lastCol, $name, $value_include);


        //NEW ROW
        $this->appendRow($this->lastForm);

        //button Save
        $label = $this->dictionary["LabelSave"];
        $name = "btnSaveMonth";
        $this->appendColumn($this->lastRow, ["nCol" => 1]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "emptyLabel" => "1", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        //button Modify Template
        if ($keyID != "") {

            $label = $this->dictionary["LabelBtnModifyTemplate"];
            $name = "btnModifyTemplate";
            $this->appendColumn($this->lastRow, ["nCol" => 3]);
            $this->appendInput($this->lastCol,
                [
                    "type" => "btn"
                    , "emptyLabel" => "1"
                    , "value" => $label
                    , "id" => $name
                    , "name" => $name
                    , "semanticColor" => "primary"
                    , "link" => $this->K_COMMON->buildAPICallUrl("KXCH", "k_webmanager", "template_editor", "templateEditor", "keyID=" . $keyID)
                    , "linkTarget" => "_blank"
                ]
            );
        }


    }
}

?>
