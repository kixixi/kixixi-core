<?
/**
 * KIXIXI k_webmanager (p_sites)
 * Create a panel web object for manage web site managed
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_webmanager
 * @subpackage objects\p_sites
 * @since 1.00
 */
namespace k_webmanager\manage\panels\p_sites;

class obj extends \k_webmanager\objects\general\base_panel\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authManageWebSites";

    /**
     * Constructor
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->dictionary = $this->getDictionary();
    }

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {

         //set variable
        global $K_COMMON;
        $KCA = $K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "save" :
                echo json_encode($this->save($ArrayVariable));
                break;
            case "delete" :
                echo json_encode($this->delete($ArrayVariable));
                break;
        }
    }

    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "get", "put", "")
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {

        //set variables
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");
        $this->urlActual = $this->K_COMMON->getVarArray($ArrayVariable, "panelUrlActual");

        switch ($pluginMethod) {
            case "list":
            case "":
                $this->getPageList();
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;
        }

        //Return
        return $this;

    }
    
    protected function getPageList()
    {

        // set crumbs
        $this->pageCrumbs ["list"] = "";
        $listMyMList = $this->getTableList([]);
        $this->appendHTML("", $listMyMList);
        $this->appendInput("", ["type" => "btn", "class" => "m-l-1", "semanticColor" => "success", "id" => "", "value" => $this->dictionary["LabelAddWebSite"], "link" => $this->urlActual . "put"]);
    }

    protected function getTableList($ArrayVariable)
    {
        //include module
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");

        //load variable
        $ana_code = $this->K_COMMON->getCurrentUser();
        $OM = $this->K_COMMON->getVarArray($ArrayVariable, "OM");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");

        //load value
        $table = $this->getTable("web_sites");
        $table->get(["id_web_sites", "web_code", "web_name", "web_from_date", "web_to_date"]);
        $dataTable = $table->getResults("array");
        $OBJTableList->newTable(
            [
                "id" => "tableList"
                , "tableID" => "tableList"
                , "hideHead" => $onlyRow
                , "data" => $dataTable
                , "fields" =>
                [
                    [
                        "label" => $this->dictionary["LabelWebCode"]
                        , "qryField" => "web_code"
                    ]
                    ,
                    [
                        "label" => $this->dictionary["LabelUrl"]
                        , "qryField" => "web_name"
                    ]
                    ,
                    [
                        "label" => $this->dictionary["LabelFromDate"]
                        , "qryField" => "web_from_date"
                    ]
                    ,
                    [
                        "label" => $this->dictionary["LabelToDate"]
                        , "qryField" => "web_to_date"
                    ]
                    ,
                    [
                        "buttons" =>
                            [
                                [
                                    "title" => $this->dictionary["LabelEdit"]
                                    , "action" => "edit"
                                    , "qryAttributes" => ["keyID" => "id_web_sites"]
                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelListWebPages"]
                                    , "action" => "link"
                                    , "showIF"=>["web_code"=>$this->K_COMMON->getEditingWeb()]
                                    , "class" => "fa fa-list"
                                    , "qryAttributes" => [ "list" => ""]
                                    , "url" => "p_pages"
                                    , "urlBaseType" => "panelPlugin"
                                ]
                                ,
                                [
                                    "title" => $this->dictionary["LabelDelete"]
                                    , "action" => "remove"
                                    , "qryAttributes" => ["keyID" => "id_web_sites"]
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "delete"]
                                ]
                            ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();

    }

    protected function getPagePut($ArrayVariable)
    {

        //add files to include
        $this->addHeadFile([$this->getPathFileJS("put")]);

        // set variable
        $anaCode = $this->K_COMMON->getCurrentUser();
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "pluginParam");
        $mlCode = "";
        $value_url = "";
        $value_fromDate = "";
        $value_toDate = "";
        if ($keyID != "") {

            $table = $this->getTable("web_sites");
            $table->get(["web_name", "web_from_date", "web_to_date"], ["id_web_sites" => $keyID]);
            if ($row = $table->fetch()) {
                $value_url = $row->web_name;
                $value_fromDate = $row->web_from_date;
                $value_toDate = $row->web_to_date;
                //set crumbs
                $this->pageCrumbs [$value_url] = "";
            } else
                return;
        } else {
            //set crumbs
            $this->pageCrumbs ["New"] = "";
        }
        //Append Form
        $this->appendForm("",
            [
                "id" => "siteForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "save"
                , "reloadPage" => "1"
                , "reloadPageCheckCode" => "keyID"
                , "reloadPageGetCode" => "keyID"
            ]
        );

        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "keyID", "value" => $keyID]);

        $this->appendRow($this->lastForm);
        $label = $this->dictionary["LabelUrl"];
        $name = "webName";
        $this->appendColumn($this->lastRow, ["nCol" => 7]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name
            , "value" => $value_url, "required" => "1"]);

        //From Date
        $this->appendRow($this->lastForm);
        $label = $this->dictionary["LabelFromDate"];
        $name = "fromDate";
        $this->appendRow($this->lastForm);
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "datePicker", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_fromDate, "startView" => "month", "minView" => "month"]);


        //To Date
        $this->appendRow($this->lastForm);
        $label = $this->dictionary["LabelToDate"];
        $name = "toDate";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "datePicker", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_toDate, "startView" => "month", "minView" => "month"]);


        $this->appendRow($this->lastForm);
        $label = $this->dictionary["LabelSave"];
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 6]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

    }

    protected function delete($ArrayVariable)
    {

        $idList = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");

        //verify if id list is null
        if ($idList == "")
            return array("success" => "false", "reason" => "errorSecurity");

        $table = $this->getTable("web_sites");
        $table->delete(["id_web_sites" => $idList]);
        return ["success" => "true"];

    }

    protected function save($ArrayVariable)
    {

        $table = $this->getTable("web_sites");

        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $url = $this->K_COMMON->getVarArray($ArrayVariable, "webName");
        $fromDate = $this->K_COMMON->getVarArray($ArrayVariable, "fromDate");
        $toDate = $this->K_COMMON->getVarArray($ArrayVariable, "toDate");
        $saveEdit = [];

        if ($keyID != "") {
            $saveEdit = ["id_web_sites" => $keyID];
        }

        $table->put([
                "web_name" => $url
                , "web_from_date" => $fromDate
                , "web_to_date" => $toDate
            ]
            , $saveEdit);
        if ($keyID == "")
            $keyID = $table->getNewID();
        return ["success" => "true", "message" => $this->dictionary["msgSuccessSave"], "keyID" => $keyID];
    }

}

?>
