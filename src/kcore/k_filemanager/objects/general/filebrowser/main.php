<?
/**
 * KIXIXI File Manager (file browser)
 * window for manage static content files
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package KIXIXI
 * @subpackage k_dbmanager\manager
 * @since 1.00
 */
namespace k_filemanager\objects\general\filebrowser;
class obj extends \k_webmanager\objects\general\web_page_base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * file manage API
     *
     * @since 1.00
     * @access private
     * @var object
     */
    private $apiFM;

    /**
     * Constructor - initialize object
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->apiFM = $this->K_COMMON->getPluginObject("k_filemanager", "api");

    }

    /**
     * Manage Ajax Request
     *
     * @since 1.00
     * @access public
     */
    public function objectRequest($arrayVariable)
    {

        //check Authorization
        if (!$this->isAuthorizedWebSiteLayout() && !$this->isAuthorizedWebSiteContent())
            return $this->K_COMMON->resultReturn("JSON", false, ["reason" => "errorSecurity", "message" => "Error Security"]);

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //switch request
        switch ($KCA) {
            case "get" :
                $this->get($arrayVariable);
                return $this->getPage();
                break;
            case "getDir" :
                return json_encode($this->getDir($arrayVariable));
                break;
            case "newFolder" :
                return json_encode($this->newFolder($arrayVariable));
                break;
            case "uploadFile":
                return json_encode($this->uploadFile($arrayVariable));
                break;
            case
                "removeFile":
                return json_encode($this->removeFile($arrayVariable));
                break;
            case "imgRotate":
                return json_encode($this->imgRotate($arrayVariable));
                break;

        }
    }

    /**
     * get windows file manager
     *
     * @since 1.00
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    webCode: web Code to get stati content
     *    *    fileType: type of file. "image" for images, "" for all files
     *    *    folderToGet: start folder to open
     *    *    fieldID: ID of field where put the file selected (clicked)
     *    *    getWithUrl: "1" for get the file name with url
     *
     * @return object web object
     */
    public function get($arrayVariable = [])
    {

        //check security
        if (!$this->K_SECAUTH->verifyUserLogged()) {
            $this->appendHTML("", "Not Logged!");
            return $this;
        }

        //set variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");
        $fileType = $this->K_COMMON->getVarArray($arrayVariable, "fileType");
        $fieldID = $this->K_COMMON->getVarArray($arrayVariable, "fieldID");
        $folderToGet = $this->K_COMMON->getVarArray($arrayVariable, "folderToGet");
        $getWithUrl = $this->K_COMMON->getVarArray($arrayVariable, "getWithUrl");

        //check variables
        if ($folderToGet == "")
            $folderToGet = "/";

        //assign js
        $this->addExternalPlugin(["validator", "waitingfor"]);
        $this->addHeadFile([$this->getPathFileJS("template")]);
        $this->addHeadFile([$this->getPathFileCSS("template")]);


        //NEW ROW
        $this->appendRow("", ["id" => "fbToolbar"]);

        //append hidden variables
        $this->appendInput($this->lastRow, ["type" => "hidden", "id" => "webCode", "name" => "webCode", "value" => $webCode]);
        $this->appendInput($this->lastRow, ["type" => "hidden", "id" => "fileType", "name" => "fileType", "value" => $fileType]);
        $this->appendInput($this->lastRow, ["type" => "hidden", "id" => "fbFieldID", "name" => "fbFieldID", "value" => $fieldID]);
        $this->appendInput($this->lastRow, ["type" => "hidden", "id" => "folderToGet", "name" => "folderToGet", "value" => $folderToGet]);
        $this->appendInput($this->lastRow, ["type" => "hidden", "id" => "getWithUrl", "name" => "getWithUrl", "value" => $getWithUrl]);

        //Folder Path
        $this->appendColumn($this->lastRow, ["nCol" => ["xs" => 8, "md" => 8]]);
        $this->appendHTML($this->lastCol, '<div id="fbFolder"></div>');

        //Upload File Button
        $label = "";
        $name = "fbBtnUploadFile";
        $this->appendColumn($this->lastRow, ["id" => "fbToolbarButtons", "nCol" => ["xs" => 1, "md" => 1]]);
        $this->appendInput($this->lastCol,
            [
                "type" => "fileButton"
                , "value" => '<i class="fa fa-upload"></i>'
                , "label" => $label
                , "id" => $name
                , "name" => $name
                , "size" => "sm"
                , "link" => "#"
                , "semanticColor" => "secondary"
                , "iFileName" => "fbupfilename"
                , "functionOnChange" => "KXNS.k_filemanager.objects.filebrowser.uploadFile"
            ]
        );

        //Input for New Folder on Hidden Row
        $label = "New Folder";
        $name = "fbNewFolderName";
        $this->appendColumn($this->lastRow, ["nCol" => ["xs" => 3, "md" => 3]]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelPlaceholder" => $label, "id" => $name, "name" => $name, "value" => "", "noLabel" => "1", "size" => "sm", "addIcon" => "fa-floppy-o"]);

        //NEW ROW (For folders and Files)
        $this->appendRow("");
        $this->appendColumn($this->lastRow, ["id" => "fbColFolder", "nCol" => ["xs" => 3, "md" => 3]]);
        $this->appendColumn($this->lastRow, ["id" => "fbColFiles", "nCol" => ["xs" => 9, "md" => 9]]);

        //Return
        return $this;
    }

    /**
     * get dir content
     *
     * @since 1.00
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    webCode: web Code to get stati content
     *    *    dir: folder to get
     *    *    fileType: type of file. "image" for images, "" for all files
     *
     * @return array
     */
    protected function getDir($arrayVariable)
    {
        //check security
        if (!$this->K_SECAUTH->verifyUserLogged())
            return ["success" => "false", "reason" => "errorSecurity", "message" => "notLogged"];

        //set variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");
        $dir = $this->K_COMMON->getVarArray($arrayVariable, "dir");
        $fileType = $this->K_COMMON->getVarArray($arrayVariable, "fileType");
        $oriDir = $dir;


        //load dir
        $paramReadDir = [];
        $paramReadDir["webCode"] = $webCode;
        $paramReadDir["dir"] = $dir;
        $paramReadDir["fileType"] = $fileType;
        $res = $this->apiFM->readStaticContentWeb($paramReadDir);

        $res["response"]["dir"] = $oriDir;
        return $res;

    }

    /**
     * make dir
     *
     * @since 1.00
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    webCode: web Code to get stati content
     *    *    dir: folder to get
     *    *    newFolderName: folder to create
     *
     * @return array array with success
     */
    protected function newFolder($arrayVariable)
    {

        //check security
        if (!$this->K_SECAUTH->verifyUserLogged())
            return ["success" => "false", "reason" => "errorSecurity", "message" => "notLogged"];

        //set variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");
        $dir = $this->K_COMMON->getVarArray($arrayVariable, "dir");
        $newFolderName = $this->K_COMMON->getVarArray($arrayVariable, "newFolderName");

        //make new folder
        $paramNewFolder = [];
        $paramNewFolder["webCode"] = $webCode;
        $paramNewFolder["dir"] = $dir;
        $paramNewFolder["newFolderName"] = $newFolderName;
        $this->apiFM->mkDirStaticContentWeb($paramNewFolder);

        return ["success" => "true"];

    }

    /**
     * upload file
     * get $_FILES["fbupfilename"] file to upload
     *
     * @since 1.00
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    webCode: web Code to get stati content
     *    *    dir: folder where upload file
     *
     *
     * @return array
     */
    protected function uploadFile($arrayVariable)
    {

        //check security
        if (!$this->K_SECAUTH->verifyUserLogged())
            return ["success" => "false", "reason" => "errorSecurity", "message" => "notLogged"];


        //get variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");
        $dir = $this->K_COMMON->getVarArray($arrayVariable, "dir");
        $fileToUpload = $_FILES["fbupfilename"];
        $newFileName = strtolower($fileToUpload['name']);
        $newFileName = str_replace(" ", "_", $newFileName);
        $imgMaxWidth = $this->getParamValue("imgMaxWidth", "", "", "" );

        //Upload
        $paramFileUpload = [];
        $paramFileUpload["sourceFileOBJ"] = $_FILES["fbupfilename"];
        $paramFileUpload["webCode"] = $webCode;
        $paramFileUpload["targetDir"] = $dir;
        $paramFileUpload["targetFile"] = $newFileName;
        $paramFileUpload["resizeImgWidth"] = $imgMaxWidth;
        $this->apiFM->uploadStaticContentWeb($paramFileUpload);

        return ["success" => "true"];
    }

    /**
     * remove file
     *
     * @since 1.00
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    webCode: web Code to get static content
     *    *    dir: folder where upload file
     *    *    filename: file to remove
     *
     *
     * @return array
     */
    protected function removeFile($arrayVariable)
    {

        //check security
        if (!$this->K_SECAUTH->verifyUserLogged())
            return ["success" => "false", "reason" => "errorSecurity", "message" => "notLogged"];


        //get variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");
        $dir = $this->K_COMMON->getVarArray($arrayVariable, "dir");
        $fileName = $this->K_COMMON->getVarArray($arrayVariable, "fileName");
        $paramFileRemove = "";

        $paramFileRemove["webCode"] = $webCode;
        $paramFileRemove["targetDir"] = $dir;
        $paramFileRemove["targetFile"] = $fileName;
        $this->apiFM->removeStaticContentWeb($paramFileRemove);

        return ["success" => "true"];
    }

    /**
     * rotate Img
     *
     * @since 1.00
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    webCode: web Code to get static content
     *    *    dir: folder where upload file
     *    *    filename: file to remove
     *    *    rotate: ° to rotate img
     *
     * @return array
     */
    protected function imgRotate($arrayVariable)
    {

        //check security
        if (!$this->K_SECAUTH->verifyUserLogged())
            return ["success" => "false", "reason" => "errorSecurity", "message" => "notLogged"];


        //get variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");
        $dir = $this->K_COMMON->getVarArray($arrayVariable, "dir");
        $fileName = $this->K_COMMON->getVarArray($arrayVariable, "fileName");
        $rotang = $this->K_COMMON->getVarArray($arrayVariable, "rotang");


        if ($dir != "/")
            $dir .= "/";
        if ($dir != "") {
            if (substr($dir, 0, 1) == "/")
                $dir = substr($dir, 1);
        }

        $paramImgRotate = "";
        $paramImgRotate["webCode"] = $webCode;
        $paramImgRotate["filename"] = $dir . $fileName;
        $paramImgRotate["rotang"] = $rotang;


        $this->apiFM->imageRotateWeb($paramImgRotate);

        return ["success" => "true"];
    }

}

?>