// create namespace
KXJS.namespace("k_filemanager.objects.filebrowser");

// create method of plugin
(function () {
    var t = this;
    t.urlBase = "";
    t.actualDir = "";
    t.init = function () {
        $(document).ready(t.ready);
    };
    t.loadDir = function (dir) {

        var webCode = $("#webCode").val();
        var fileType = $("#fileType").val();
        t.actualDir = dir;

        waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'warning'});

        $.ajax({
            url: KXJS.buildAPICallUrl("KXCJS", "k_filemanager", "filebrowser", "getDir"),
            data: {
                dir: dir
                , fileType: fileType
                , webCode: webCode
            }

        }).done(function (data) {

            waitingDialog.hide();

            r = $.parseJSON(data);
            if (r.success != "false") {
                t.urlBase = r.response.urlBase;
                t.applyFolder(r.response.items.dir, r.response.dir);
                t.applyFiles(r.response.items.files, r.response.dir, r.response.urlBase);
            }
            else {
                if (r.reason == "errorSecurity")
                    window.location.reload();
            }


        });

    };
    t.applyFolder = function (folderList, folderName) {

        //empty folders column
        document.getElementById("fbColFolder").innerHTML = "";

        //set folder name
        $("#fbFolder").html((folderName == "" ? "/" : folderName));

        //get dir up
        if ((folderName != "" && folderName != "/")) {
            folderName += "/";

            splitdir = folderName.split("/");
            dirup = "";
            for (i = 0; i < splitdir.length - 2; i++) {
                if (splitdir[i] != "") dirup += "/" + splitdir[i];
            }
            document.getElementById("fbColFolder").innerHTML += "<div class=\"fbFolderUp\"  foldername=\"" + dirup + "\"> ..</div>";

        }


        //write folders list
        var i = 0;
        while (i < folderList.length) {
            if (folderList[i].type == "dir")
                document.getElementById("fbColFolder").innerHTML += "<div class=\"fbFolderItem\"  foldername=\"" + folderName + folderList[i].filename + "\">" + folderList[i].filename + "</div>";

            i += 1;

        }

        //Assign Trigger
        $(".fbFolderUp, .fbFolderItem").click(function () {

            var foldername = $(this).attr("foldername");

            t.loadDir(foldername);

        });
    };
    t.applyFiles = function (filesList, folderName, urlBase) {

        //empty folders column
        document.getElementById("fbColFiles").innerHTML = "";


        //write folders list
        var i = 0;
        var cardsToWrite = "";
        var btnFile = "";
        $("#fbColFiles").append('<div class="card-deck-wrapper"></div>');


        while (i < filesList.length) {

            //write contents every 3 files
            if (i % 3 == 0 && cardsToWrite != "") {
                $("#fbColFiles .card-deck-wrapper").append('<div class="card-deck">' + cardsToWrite + '</div>');
                cardsToWrite = "";
            }

            //check files type
            if (filesList[i].filetype == "image") {
                btnFile = "<div class=\"btnFileContainer\">" +
                    "<i class=\"btnfile btnRotateL fa fa-rotate-left\"></i>" +
                    "<i class=\"btnfile btnRotateR fa fa-rotate-right\"></i>" +
                    "<i class=\"btnfile btnRemove fa fa-trash-o\"></i>" +
                    "</div>";
                cardsToWrite +=
                    "<div class=\"card fbFileItem fbFileImage\" foldername = \"" + folderName + "\" filename = \"" + filesList[i].filename + "\">" +
                    btnFile +
                    "   <div>" +
                    "   <a href=\"#\" filename=\"" + filesList[i].filename + "\" title=\"" + filesList[i].filename + "\"  >" +
                    "       <img class=\"fbIcoImage card-img-top\" src=\"" + urlBase + filesList[i].filename + "?" + new Date().getTime() + "\" alt=\"" + filesList[i].filename + "\" style=\"width:100%;\" />" +
                    "   </a>" +
                    "   </div>" +
                    "   <div class=\"card-block fbFileItemFooter\">" +
                    "       <p class=\"card-title\"><a href=\"#\">" + filesList[i].filename + "</a></p>" +
                    "   </div>" +
                    "</div>";
            }
            else if (filesList[i].ext == "pdf") {
                btnFile = "<div class=\"btnFileContainer\">" +
                    "<i class=\"btnfile btnRemove fa fa-trash-o\"></i>" +
                    "</div>";
                cardsToWrite +=
                    "<div class=\"card fbFileItem fbFilePdf\" foldername = \"" + folderName + "\" filename = \"" + filesList[i].filename + "\">" +
                    btnFile +
                    "   <a href=\"#\" filename=\"" + filesList[i].filename + "\" title=\"" + filesList[i].filename + "\"  >" +
                    "   <div class=\"fbIcoPdf\">" +
                    "   </div>" +
                    "   </a>" +
                    "   <div class=\"card-block fbFileItemFooter\">" +
                    "       <p class=\"card-title\"><a href=\"#\">" + filesList[i].filename + "</a></p>" +
                    "   </div>" +
                    "</div>";
            }
            else {
                btnFile = "<div class=\"btnFileContainer\">" +
                    "<i class=\"btnfile btnRemove fa fa-trash-o\"></i>" +
                    "</div>"
                cardsToWrite +=
                    "<div class=\"card fbFileItem fbFilePdf\" foldername = \"" + folderName + "\" filename = \"" + filesList[i].filename + "\">" +
                    btnFile +
                    "   <a href=\"#\" filename=\"" + filesList[i].filename + "\" title=\"" + filesList[i].filename + "\"  >" +
                    "   <div class=\"fbIcoFile\">" +
                    "   </div>" +
                    "   </a>" +
                    "   <div class=\"card-block fbFileItemFooter\">" +
                    "       <p class=\"card-title\"><a href=\"#\">" + filesList[i].filename + "</a></p>" +
                    "   </div>" +
                    "</div>";
            }
            i += 1;
            btnFile = "";
        }

        //write contents every 3 files
        if (cardsToWrite != "")
            $("#fbColFiles .card-deck-wrapper").append('<div class="card-deck">' + cardsToWrite + '</div>');


        //Assign Trigger
        $(".fbFileItem a").click(function () {

            var fbFieldID = $("#fbFieldID").val();
            var getWithUrl = $("#getWithUrl").val();
            var linkFile = $(this).attr("filename");

            if (getWithUrl == 1)
                linkFile = t.urlBase + linkFile;
            else
                linkFile = (t.actualDir != "" && t.actualDir != "/" ? t.actualDir + "/" : "/") + linkFile;

            if (parent.$("#" + fbFieldID)) {
                parent.$("#" + fbFieldID).val(linkFile);
                parent.$("#" + fbFieldID).change();
            }

            parent.$('#fileBrowserModal').modal("hide");

        });
        $("#fbColFiles .btnRemove").click(function () {
            var currentFile = $(this).parent().parent();
            t.removeFile(currentFile);
        });
        $("#fbColFiles .btnRotateL").click(function () {
            var currentFile = $(this).parent().parent();
            t.imgRotateL(currentFile);
        });
        $("#fbColFiles .btnRotateR").click(function () {
            var currentFile = $(this).parent().parent();
            t.imgRotateR(currentFile);
        });

    };
    t.uploadFile = function () {

        // Get data Form
        var webCode = $("#webCode").val();
        var idForm = $(this).parents("form").attr("id");
        var form = new FormData($('#' + idForm)[0]);

        $("#nextBtn").hide();
        dialogModal = waitingDialog.show('Uploading...', {dialogSize: 'sm', progressType: 'warning'});

        // Ajax call
        $.ajax({
            url: KXJS.buildAPICallUrl("KXCJS", "k_filemanager", "filebrowser", "uploadFile", "dir=" + t.actualDir + "&webCode=" + webCode)
            , type: 'POST'
            , xhr: function () {
                var myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            success: function (data) {
                waitingDialog.hide();
                r = $.parseJSON(data);
                if (r.success == "true") {
                    dialogModal.on('hidden.bs.modal', function () {
                        KXNS.k_filemanager.objects.filebrowser.loadDir(t.actualDir);
                    });
                }
                else {
                    if (r.reason == "errorSecurity")
                        window.location.reload();
                    else
                        alert(r.message);

                }
            },
            data: form,
            cache: false,
            contentType: false,
            processData: false
        });


    };
    t.makeFolder = function () {

        var webCode = $("#webCode").val();
        var newFolderName = $("#fbNewFolderName").val();

        if (newFolderName != "") {
            $.ajax({
                url: KXJS.buildAPICallUrl("KXCJS", "k_filemanager", "filebrowser", "newFolder"),
                data: {
                    dir: t.actualDir
                    , newFolderName: newFolderName
                    , webCode: webCode
                }

            }).done(function (data) {

                waitingDialog.hide();

                r = $.parseJSON(data);
                if (r.success == "true") {
                    t.loadDir(t.actualDir);
                }
                else {
                    if (r.reason == "errorSecurity")
                        window.location.reload();
                    else
                        alert(r.message);

                }


            });
        }
    };
    t.imgRotateL = function (currentFile) {
        t.imgRotate(currentFile, 90);
    };
    t.imgRotateR = function (currentFile) {
        t.imgRotate(currentFile, 270);
    };
    t.imgRotate = function (currentFile, rotang) {

        var dir = currentFile.attr("foldername");
        var fileName = currentFile.attr("filename");
        var webCode = $("#webCode").val();
        //waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'warning'});
        $.ajax({
            url: KXJS.buildAPICallUrl("KXCJS", "k_filemanager", "filebrowser", "imgRotate"),
            data: {
                dir: dir
                , fileName: fileName
                , rotang: rotang
                , webCode: webCode
            }

        }).done(function (data) {

            waitingDialog.hide();

            r = $.parseJSON(data);
            if (r.success != "false") {
                t.loadDir(t.actualDir);
            }
            else {
                if (r.reason == "errorSecurity")
                    window.location.reload();
            }


        });

    };
    t.removeFile = function (currentFile) {

        var dir = currentFile.attr("foldername");
        var fileName = currentFile.attr("filename");
        var webCode = $("#webCode").val();

        //waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'warning'});
        $.ajax({
            url: KXJS.buildAPICallUrl("KXCJS", "k_filemanager", "filebrowser", "removeFile"),
            data: {
                dir: dir
                , fileName: fileName
                , webCode: webCode
            }
        }).done(function (data) {
            waitingDialog.hide();
            r = $.parseJSON(data);
            if (r.success != "false") {
                t.loadDir(t.actualDir);
            }
            else {
                if (r.reason == "errorSecurity")
                    window.location.reload();
            }
        });
    };

    t.ready = function () {
        var folderToGet = $("#folderToGet").val();

        KXJS.Form.createSingleButtonFileForm({
            "appendTo": "#fbBtnUploadFile",
            "onFileChange": KXNS.k_filemanager.objects.filebrowser.uploadFile,
            "buttonClass": "btn btn-primary",
            "divClass": "",
            "iFileName": "fbupfilename",
            "buttonValue": ""
        });

        t.loadDir(folderToGet);
        $("#fbNewFolderNameIcon").click(t.makeFolder);

    };

    t.init();

}).apply(KXNS.k_filemanager.objects.filebrowser);

