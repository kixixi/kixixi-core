<?
/**
 * KIXIXI File Manager (api)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package KIXIXI
 * @subpackage k_dbmanager\manager
 * @since 1.00
 */

namespace k_filemanager\objects\general\api;
class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";


    /**
     * Upload File on Static content Dir
     *
     * @since 1.00
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    sourceFile: path and file name (EX: /usr/files/image.png)
     *    *    sourceFileOBJ: file object (EX: $_FILES["image"])
     *    *    targetDir: dir to save file
     *    *    targetFile: file name after saved file
     *    *    expiryMinutes: optional. If valued number of minutes for a temporary link (only for S3)
     *    *    resizeImgWidth: if you want resize image, set the max width
     *
     * @return string url file link
     */
    function uploadStaticContent($arrayVariable)
    {
        //set target type
        $arrayVariable["contentType"] = "";

        //upload content
        return $this->uploadStaticContentGeneral($arrayVariable);
    }

    /**
     * Upload File on Static content Dir (on website dir)
     *
     * @since 1.00
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    webCode: web Code
     *    *    sourceFile: path and file name (EX: /usr/files/image.png)
     *    *    sourceFileOBJ: file object (EX: $_FILES["image"])
     *    *    targetDir: dir to save file
     *    *    targetFile: file name after saved file
     *    *    expiryMinutes: optional. If valued number of minutes for a temporary link (only for S3)
     *
     * @return string url file link
     */
    function uploadStaticContentWeb($arrayVariable)
    {
        //set target type
        $arrayVariable["contentType"] = "w";

        //upload content
        return $this->uploadStaticContentGeneral($arrayVariable);
    }

    /**
     * Remove File on Static content Dir
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    targetDir: dir to remove file
     *    *    targetFile: file to remove (if empty remove dir)
     *
     * @return boolean
     */
    function removeStaticContent($arrayVariable)
    {
        //set target type
        $arrayVariable["contentType"] = "";

        //upload content
        return $this->removeStaticContentGeneral($arrayVariable);
    }

    /**
     * Remove File on Static content Dir (on website dir)
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    targetDir: dir to remove file
     *    *    targetFile: file to remove (if empty remove dir)
     *
     * @return boolean
     */
    function removeStaticContentWeb($arrayVariable)
    {
        //set target type
        $arrayVariable["contentType"] = "w";

        //upload content
        return $this->removeStaticContentGeneral($arrayVariable);
    }

    /**
     * Copy a Folder From Local To Static Content
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    sourceDir: dir From
     *    *    targetDir: dir To
     *
     * @return boolean
     */
    function copyFolderLocalToStaticContent($arrayVariable)
    {

        //set target type
        $arrayVariable["contentType"] = "";
        $arrayVariable["sourceType"] = "local";
        $arrayVariable["targetType"] = "static";

        //copy
        return $this->copyFolderGeneral($arrayVariable);
    }

    /**
     * Copy a Folder From Local To Static Content Web (on website dir)
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    sourceDir: dir From
     *    *    targetDir: dir To
     *
     * @return boolean
     */
    function copyFolderLocalToStaticContentWeb($arrayVariable)
    {

        //set target type
        $arrayVariable["contentType"] = "w";
        $arrayVariable["sourceType"] = "local";
        $arrayVariable["targetType"] = "static";

        //copy
        return $this->copyFolderGeneral($arrayVariable);
    }

    /**
     * Copy a Folder From Static Content To Local
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    sourceDir: dir From
     *    *    targetDir: dir To
     *
     * @return boolean
     */
    function copyFolderFromStaticContentToLocal($arrayVariable)
    {

        //set target type
        $arrayVariable["contentType"] = "";
        $arrayVariable["sourceType"] = "static";
        $arrayVariable["targetType"] = "local";

        //copy
        return $this->copyFolderGeneral($arrayVariable);
    }

    /**
     * Copy a Folder From Static Content To Local
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    sourceDir: dir From
     *    *    targetDir: dir To
     *
     * @return boolean
     */
    function copyFolderFromStaticContentWebToLocal($arrayVariable)
    {

        //set target type
        $arrayVariable["contentType"] = "w";
        $arrayVariable["sourceType"] = "static";
        $arrayVariable["targetType"] = "local";

        //copy
        return $this->copyFolderGeneral($arrayVariable);
    }

    /**
     * Duplicate a Folder on Static Content
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    sourceDir: dir From
     *    *    targetDir: dir To
     *
     * @return boolean
     */
    function duplicateFolderStaticContent($arrayVariable)
    {

        //set target type
        $arrayVariable["contentType"] = "";
        $arrayVariable["sourceType"] = "static";
        $arrayVariable["targetType"] = "static";

        //copy
        return $this->copyFolderGeneral($arrayVariable);
    }

    /**
     * Duplicate a Folder on Static Content Web
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    sourceDir: dir From
     *    *    targetDir: dir To
     *
     * @return boolean
     */
    function duplicateFolderStaticContentWeb($arrayVariable)
    {

        //set target type
        $arrayVariable["contentType"] = "w";
        $arrayVariable["sourceType"] = "static";
        $arrayVariable["targetType"] = "static";

        //copy
        return $this->copyFolderGeneral($arrayVariable);
    }

    /**
     * Duplicate a Folder on Static Content Web
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    sourceDir: dir From
     *    *    targetDir: dir To
     *
     * @return boolean
     */
    function duplicateFolderLocal($arrayVariable)
    {

        //set variable
        $sourceDir = $this->K_COMMON->getVarArray($arrayVariable, "sourceDir"); //without slash on start and end
        $targetDir = $this->K_COMMON->getVarArray($arrayVariable, "targetDir"); //without slash on start and end


        //copy
        return $this->copyFolderLocalToLocal($sourceDir, $targetDir);
    }


    /**********************************************************************************/
    /******************************  GET DIR/FILES   **********************************/
    /**********************************************************************************/

    /**
     * Read Static content Dir
     *
     * @since 1.00
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    dir: path to read
     *    *    listype: "dir" for get dirs, "files" for get files, "" for both
     *    *    page: page to get (18 items for page)
     *
     * @return array
     */
    function readStaticContent($arrayVariable)
    {
        //set target type
        $arrayVariable["contentType"] = "";

        //upload content
        return $this->readStaticContentGeneral($arrayVariable);
    }

    /**
     * Read Static content Dir (on website dir)
     *
     * @since 1.00
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    webCode: web Code
     *    *    dir: path to read
     *    *    listype: "dir" for get dirs, "files" for get files, "" for both
     *    *    page: page to get (18 items for page)
     *
     * @return array
     */
    function readStaticContentWeb($arrayVariable)
    {
        //set target type
        $arrayVariable["contentType"] = "w";

        //upload content
        return $this->readStaticContentGeneral($arrayVariable);
    }

    /**
     * Make Dir (on website dir)
     *
     * @since 1.00
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    dir: path where make dir
     *    *    newFolderName: folder name to make
     *
     * @return boolean
     */
    function mkDirStaticContent($arrayVariable)
    {

        //set target type
        $arrayVariable["contentType"] = "";

        //make dir
        return $this->mkDirStaticContentGeneral($arrayVariable);

    }

    /**
     * Make Dir (on website dir)
     *
     * @since 1.00
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    webCode: web Code
     *    *    dir: path where make dir
     *    *    newFolderName: folder name to make
     *
     * @return boolean
     */
    function mkDirStaticContentWeb($arrayVariable)
    {

        //set target type
        $arrayVariable["contentType"] = "w";

        //make dir
        return $this->mkDirStaticContentGeneral($arrayVariable);

    }


    /**********************************************************************************/
    /******************************  ROTATE/RESIZE   **********************************/
    /**********************************************************************************/

    /**
     * Image Rotate
     *
     * @since 1.00
     *
     * * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *
     * *    filename    : file of image to rotate
     * *    rotang    : angle of rotation
     *
     * @return boolean
     */
    function imageRotate($arrayVariable)
    {

        //set target type
        $arrayVariable["contentType"] = "";

        //make dir
        return $this->imageRotateGeneral($arrayVariable);

    }

    /**
     * Image Rotate (on website dir)
     *
     * @since 1.00
     *
     * * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *
     * *    filename    : file of image to rotate
     * *    rotang    : angle of rotation
     *
     * @return boolean
     */
    function imageRotateWeb($arrayVariable)
    {

        //set target type
        $arrayVariable["contentType"] = "w";

        //make dir
        return $this->imageRotateGeneral($arrayVariable);

    }

    /**
     * Image Resize
     *
     * @since 1.00
     *
     * * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *
     * *    filename    : file of image to rotate
     * *    width   : pixel or percentage
     * *    height  : pixel or percentage
     * *    checkWH : if true check if initial image width > $width (same for height) otherwise not resize it
     * *    compression: compression (default 75)
     *
     * @return boolean
     */
    function imageResize($arrayVariable)
    {

        //set target type
        $arrayVariable["contentType"] = "";

        //make dir
        return $this->imageResizeGeneral($arrayVariable);

    }

    /**
     * Image Resize (on website dir)
     *
     * @since 1.00
     *
     * * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *
     * *    filename    : file of image to rotate
     * *    width   : pixel or percentage
     * *    height  : pixel or percentage
     * *    checkWH : if true check if initial image width > $width (same for height) otherwise not resize it
     * *    compression: compression (default 75)
     *
     * @return boolean
     */
    function imageResizeWeb($arrayVariable)
    {

        //set target type
        $arrayVariable["contentType"] = "w";

        //make dir
        return $this->imageResizeGeneral($arrayVariable);

    }


    /***************************************************************************************************/
    /*********************************** PRIVATE FUNCTION **********************************************/
    /***************************************************************************************************/

    private function fixDir($dir)
    {
        if ($dir == "")
            $dir = "/";
        if (substr($dir, 0, 1) != "/")
            $dir = "/" . $dir;
        if (substr($dir, 0, -1) != "/" & $dir != "/")
            $dir = $dir . "/";

        return $dir;
    }

    private function readStaticContentGeneral($arrayVariable)
    {

        //set variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");
        $contentType = $this->K_COMMON->getVarArray($arrayVariable, "contentType"); //"": bucket base dir, "w": bucket website dir
        $dir = $this->K_COMMON->getVarArray($arrayVariable, "dir");
        $listype = $this->K_COMMON->getVarArray($arrayVariable, "listype"); //"dir" or "files" or "" for both
        $page = $this->K_COMMON->getVarArray($arrayVariable, "page");
        $page = $page != "" ? $page : "1";
        $itemsPageMax = 18;
        $itemStart = $itemsPageMax * ($page - 1);
        $fileNum = $itemStart;
        $location = $this->K_COMMON->getStaticContentLocation();

        //check dir
        $dir = $this->fixDir($dir);


        //check variables
        if ($contentType == "w") {
            $urlBase = $this->K_COMMON->getStaticContentURLWeb($webCode) . $dir;
            $dir = $this->K_COMMON->getStaticContentWebSitesDir($webCode) . $dir;
        } else {
            $urlBase = $this->K_COMMON->getStaticContentURL() . $dir;
            $dir = $this->K_COMMON->getStaticContentBaseDir() . $dir;
        }

        //check location
        if ($location == "s3") {

            //include objects
            $objS3 = $this->K_COMMON->getPluginObject("k_aws", "s3");

            //load content from S3
            $awsCredential = $this->K_COMMON->getAWSCredential();
            $bucketName = $this->K_COMMON->getAWSBucket();
            $awsAccessKey = $awsCredential["AWSAccessKey"];
            $awsSecretKey = $awsCredential["AWSSecretKey"];

            $folderContent = $objS3->loadFolderContent(
                [
                    "bucketName" => $bucketName
                    , "awsAccessKey" => $awsAccessKey
                    , "awsSecretKey" => $awsSecretKey
                    , "dir" => $dir
                ]
            );

            //set data variables
            $filesName = $folderContent["files"];
            $foldersNname = $folderContent["folders"];
            $filesNum = count($filesName);
            $foldersNum = count($foldersNname);

        } else {
            $foldersNname = [];
            $filesName = [];
            if (is_dir($this->K_COMMON->pathCstSystem . "/" . $dir)) {
                if ($dh = opendir($this->K_COMMON->pathCstSystem . "/" . $dir)) {
                    while (($file = readdir($dh)) !== false) {
                        if ($file != "." && $file != "..") {
                            if (filetype($this->K_COMMON->pathCstSystem . "/" . $dir . $file) === 'dir') {
                                $foldersNname[] = $file;
                            } else {
                                $filesName[] = $file;
                            }
                        }
                    }
                }
            }

            //set data variables
            $filesNum = count($filesName);
            $foldersNum = count($foldersNname);
        }


        //set json RESULT
        $resArray = [];
        $resArray["response"]["items"]["dir"] = [];
        $resArray["response"]["items"]["files"] = [];

        //create array of directories
        $item_count = 0;
        if ($listype == "dir" || $listype == "")
            while (($item_count < $foldersNum)) {
                $resArray["response"]["items"]["dir"][$item_count]["filename"] = $foldersNname[$item_count];
                $resArray["response"]["items"]["dir"][$item_count]["type"] = "dir";
                $item_count++;

            }

        //create array of files
        $item_count = 0;
        if ($listype == "file" || $listype == "")
            while (($fileNum < $filesNum) && (($item_count < $itemsPageMax) && ($filesNum > $itemStart))) {


                $ext = $this->K_COMMON->getFileExtension($filesName[$fileNum]);
                $resArray["response"]["items"]["files"][$item_count]["ext"] = $ext;

                //if image get width and height
                if (($ext == "jpg") || ($ext == "jpeg") || ($ext == "png") || ($ext == "gif") || ($ext == "bmp")) {

                    $url_image = $urlBase . $filesName[$fileNum];


                    $resArray["response"]["items"]["files"][$item_count]["filetype"] = "image";
                    $resArray["response"]["items"]["files"][$item_count]["type"] = "file";
                    $resArray["response"]["items"]["files"][$item_count]["filename"] = $filesName[$fileNum];

                } else {
                    $resArray["response"]["items"]["files"][$item_count]["filetype"] = "";
                    $resArray["response"]["items"]["files"][$item_count]["type"] = "file";
                    $resArray["response"]["items"]["files"][$item_count]["filename"] = $filesName[$fileNum];
                }

                $fileNum += 1;
                $item_count++;

            }

        //set total_count
        switch ($listype) {
            case "file":
                $resArray["response"]["total_files"] = $filesNum;
                $resArray["response"]["total_dir"] = 0;
                $resArray["response"]["total_count"] = ($filesNum + $foldersNum);
                break;
            case "dir":
                $resArray["response"]["total_files"] = 0;
                $resArray["response"]["total_dir"] = $foldersNum;
                $resArray["response"]["total_count"] = ($filesNum + $foldersNum);
                break;
            case "":
                $resArray["response"]["total_files"] = $filesNum;
                $resArray["response"]["total_dir"] = $foldersNum;
                $resArray["response"]["total_count"] = ($filesNum + $foldersNum);
                break;
        }


        $resArray["response"]["urlBase"] = $urlBase;
        $resArray["response"]["dir"] = $dir;
        $resArray["response"]["page"] = $page;


        return $resArray;


    }

    private function uploadStaticContentGeneral($arrayVariable)
    {
        //set variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");
        $sourceFile = $this->K_COMMON->getVarArray($arrayVariable, "sourceFile"); //path of file
        $sourceFileOBJ = $this->K_COMMON->getVarArray($arrayVariable, "sourceFileOBJ"); //object of file ($_FILES["file"])
        $contentType = $this->K_COMMON->getVarArray($arrayVariable, "contentType"); //"": bucket base dir, "w": bucket website dir
        $targetDir = $this->K_COMMON->getVarArray($arrayVariable, "targetDir"); //without slash on start and end
        $targetFile = $this->K_COMMON->getVarArray($arrayVariable, "targetFile");
        $expiryMinutes = $this->K_COMMON->getVarArray($arrayVariable, "expiryMinutes"); //minutes to expiry (only for AWS)
        $resizeImgWidth = $this->K_COMMON->getVarArray($arrayVariable, "resizeImgWidth"); //resize image (max width)
        $pathWebTmp = $this->K_COMMON->getPathTmp();
        $tmpFileName = "";


        //check dir
        $targetDir = $this->fixDir($targetDir);
        $targetDirOri = $targetDir;

        //get Source File
        if ($sourceFile == "") {
            $tmpFileName = time();
            $this->K_COMMON->uploadFile($sourceFileOBJ, $pathWebTmp, time());
            $sourceFile = $pathWebTmp . $tmpFileName;
        }

        //check variables
        if ($targetFile == "")
            $targetFile = basename($sourceFile);
        if ($contentType == "w") {
            $urlNewFile = $this->K_COMMON->getStaticContentURLWeb($webCode) . $targetDir;
            $targetDir = $this->K_COMMON->getStaticContentWebSitesDir($webCode) . $targetDir;
        } else {
            $urlNewFile = $this->K_COMMON->getStaticContentURL() . $targetDir;
            $targetDir = $this->K_COMMON->getStaticContentBaseDir() . $targetDir;
        }

        //upload file
        $location = $this->K_COMMON->getStaticContentLocation();
        if ($location == "s3") {
            $objS3 = $this->K_COMMON->getPluginObject("k_aws", "s3");

            $awsCredential = $this->K_COMMON->getAWSCredential();
            $bucketName = $this->K_COMMON->getAWSBucket();
            $awsAccessKey = $awsCredential["AWSAccessKey"];
            $awsSecretKey = $awsCredential["AWSSecretKey"];

            $objS3->uploadFile(
                [
                    "sourceFile" => $sourceFile
                    , "targetFile" => $targetFile
                    , "targetDir" => $targetDir
                    , "bucketName" => $bucketName
                    , "awsAccessKey" => $awsAccessKey
                    , "awsSecretKey" => $awsSecretKey
                    , "deleteSource" => true
                ]
            );

            $fileNameWithPath = $targetDir . $targetFile;

            if ($expiryMinutes != "")
                $linkFile = $objS3->getTemporaryLink($awsAccessKey, $awsSecretKey, $bucketName, $targetDir . $targetFile, $expiryMinutes);
            else
                $linkFile = $urlNewFile . $targetFile;

        } else {

            if ($tmpFileName != "") {
                chmod($pathWebTmp . $tmpFileName, 0777);
                rename($pathWebTmp . $tmpFileName, $this->K_COMMON->pathCstSystem . "/" . $targetDir . $targetFile);
            } else {
                $this->mkDirStaticContentGeneral(["contentType" => $contentType, "webCode" => $webCode, "dir" => "/", "newFolderName" => $targetDirOri]);
                rename($sourceFile, $this->K_COMMON->pathCstSystem . "/" . $targetDir . $targetFile);
            }

            $fileNameWithPath = $this->K_COMMON->pathCstSystem . "/" . $targetDir . $targetFile;
            $linkFile = $urlNewFile . $targetFile;

        }

        // resize image
        if ($resizeImgWidth != "")
            $this->imageResizeGeneral(
                [
                    "filename" => $targetDirOri . $targetFile,
                    "width" => $resizeImgWidth,
                    "checkWH" => true,
                    "contentType" => $contentType,
                    "webCode" => $webCode
                ]
            );

        return $linkFile;
    }

    private function removeStaticContentGeneral($arrayVariable)
    {
        //set variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");
        $contentType = $this->K_COMMON->getVarArray($arrayVariable, "contentType"); //"": bucket base dir, "w": bucket website dir
        $targetDir = $this->K_COMMON->getVarArray($arrayVariable, "targetDir"); //without slash on start and end
        $targetFile = $this->K_COMMON->getVarArray($arrayVariable, "targetFile");

        //check dir
        $targetDir = $this->fixDir($targetDir);

        if ($contentType == "w") {
            $targetDir = $this->K_COMMON->getStaticContentWebSitesDir($webCode) . $targetDir;
        } else {
            $targetDir = $this->K_COMMON->getStaticContentBaseDir() . $targetDir;
        }

        $fileName = $targetDir . $targetFile;

        //remove file
        $location = $this->K_COMMON->getStaticContentLocation();
        if ($location == "s3") {
            $objS3 = $this->K_COMMON->getPluginObject("k_aws", "s3");
            $awsCredential = $this->K_COMMON->getAWSCredential();
            $bucketName = $this->K_COMMON->getAWSBucket();
            $awsAccessKey = $awsCredential["AWSAccessKey"];
            $awsSecretKey = $awsCredential["AWSSecretKey"];

            if ($targetFile != "")
                $objS3->deleteFile(
                    [
                        "fileName" => $fileName
                        , "bucketName" => $bucketName
                        , "awsAccessKey" => $awsAccessKey
                        , "awsSecretKey" => $awsSecretKey
                    ]
                );
            else
                $objS3->deleteFolder(
                    [
                        "folderName" => $targetDir
                        , "bucketName" => $bucketName
                        , "awsAccessKey" => $awsAccessKey
                        , "awsSecretKey" => $awsSecretKey
                    ]
                );
        } else {

            $fileNameWithPath = $this->K_COMMON->pathCstSystem . "/" . $targetDir . $targetFile;

            if ($targetFile != "")
                unlink($fileNameWithPath);
            else
                //remove dir
                $this->K_COMMON->emptyDir(["dir" => $fileNameWithPath, "rmDir" => "1"]);


        }


        return;
    }

    private function mkDirStaticContentGeneral($arrayVariable)
    {

        //include objects
        $objS3 = $this->K_COMMON->getPluginObject("k_aws", "s3");

        //set variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");
        $contentType = $this->K_COMMON->getVarArray($arrayVariable, "contentType"); //"": bucket base dir, "w": bucket website dir
        $newFolderName = $this->K_COMMON->getVarArray($arrayVariable, "newFolderName");
        $dir = $this->K_COMMON->getVarArray($arrayVariable, "dir");
        $location = $this->K_COMMON->getStaticContentLocation();

        //check dir
        $dir = $this->fixDir($dir);

        //check new folder name
        if (substr($newFolderName, 0, 1) == "/")
            $newFolderName = substr($newFolderName, 1);

        //check variables
        if ($contentType == "w") {
            $dir = $this->K_COMMON->getStaticContentWebSitesDir($webCode) . $dir;
        } else {
            $dir = $this->K_COMMON->getStaticContentBaseDir() . $dir;
        }

        //check location
        if ($location == "s3") {
            $awsCredential = $this->K_COMMON->getAWSCredential();
            $bucketName = $this->K_COMMON->getAWSBucket();
            $awsAccessKey = $awsCredential["AWSAccessKey"];
            $awsSecretKey = $awsCredential["AWSSecretKey"];

            $objS3->mkDir(
                [
                    "dir" => $dir
                    , "newFolderName" => $newFolderName
                    , "bucketName" => $bucketName
                    , "awsAccessKey" => $awsAccessKey
                    , "awsSecretKey" => $awsSecretKey
                ]
            );
        } else {

            $dir = ($dir != "" && $dir != "/" ? "/" . $dir : "");


            $this->K_COMMON->mkDir(["dir" => $this->K_COMMON->pathCstSystem . $dir . $newFolderName, "mask" => 0777]);


        }

        return true;

    }

    private function imageRotateGeneral($arrayVariable)
    {


        //set variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");
        $filename = $this->K_COMMON->getVarArray($arrayVariable, "filename"); //path of file
        $rotang = $this->K_COMMON->getVarArray($arrayVariable, "rotang");
        $contentType = $this->K_COMMON->getVarArray($arrayVariable, "contentType"); //"": bucket base dir, "w": bucket website dir

        //check variables
        if ($contentType == "w") {
            $targetDir = $this->K_COMMON->getStaticContentWebSitesDir($webCode) . "/";
        } else {
            $targetDir = $this->K_COMMON->getStaticContentBaseDir() . "/";
        }

        //switch by file location
        $location = $this->K_COMMON->getStaticContentLocation();
        if ($location == "s3") {
            $objS3 = $this->K_COMMON->getPluginObject("k_aws", "s3");

            $awsCredential = $this->K_COMMON->getAWSCredential();
            $bucketName = $this->K_COMMON->getAWSBucket();
            $awsAccessKey = $awsCredential["AWSAccessKey"];
            $awsSecretKey = $awsCredential["AWSSecretKey"];

            $objS3->imageRotate(
                [
                    "filename" => $targetDir . $filename
                    , "bucketName" => $bucketName
                    , "awsAccessKey" => $awsAccessKey
                    , "awsSecretKey" => $awsSecretKey
                    , "rotang" => $rotang
                ]
            );


        } else {

            $targetDir = $this->K_COMMON->pathCstSystem . "/" . $targetDir;
            $apiFM = $this->K_COMMON->getPluginObject("k_imagemanager", "api");
            $apiFM->imageRotate(
                [
                    "filename" => $targetDir . $filename
                    , "newfilename" => $targetDir . $filename
                    , "rotang" => $rotang
                ]
            );

        }


    }

    private function imageResizeGeneral($arrayVariable)
    {
        //set variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");
        $filename = $this->K_COMMON->getVarArray($arrayVariable, "filename");
        $newfilename = $this->K_COMMON->getVarArray($arrayVariable, "newfilename");
        $width = $this->K_COMMON->getVarArray($arrayVariable, "width");
        $height = $this->K_COMMON->getVarArray($arrayVariable, "height");
        $checkWH = $this->K_COMMON->getVarArray($arrayVariable, "checkWH");
        $compression = $this->K_COMMON->getVarArray($arrayVariable, "compression");
        $chmod = $this->K_COMMON->getVarArray($arrayVariable, "chmod");
        $contentType = $this->K_COMMON->getVarArray($arrayVariable, "contentType"); //"": bucket base dir, "w": bucket website dir

        //check filename
        if (substr($filename, 0, 1) == "/")
            $filename = substr($filename, 1);
        if ($newfilename == "")
            $newfilename = $filename;

        //check variables
        if ($contentType == "w") {
            $targetDir = $this->K_COMMON->getStaticContentWebSitesDir($webCode) . "/";
        } else {
            $targetDir = $this->K_COMMON->getStaticContentBaseDir() . "/";

        }

        //switch by file location
        $location = $this->K_COMMON->getStaticContentLocation();
        if ($location == "s3") {
            $objS3 = $this->K_COMMON->getPluginObject("k_aws", "s3");

            $awsCredential = $this->K_COMMON->getAWSCredential();
            $bucketName = $this->K_COMMON->getAWSBucket();
            $awsAccessKey = $awsCredential["AWSAccessKey"];
            $awsSecretKey = $awsCredential["AWSSecretKey"];

            $objS3->imageResize(
                [
                    "filename" => $targetDir . $filename
                    , "newfilename" => $targetDir . $newfilename
                    , "width" => $width
                    , "height" => $height
                    , "checkWH" => $checkWH
                    , "compression" => $compression
                    , "bucketName" => $bucketName
                    , "awsAccessKey" => $awsAccessKey
                    , "awsSecretKey" => $awsSecretKey

                ]
            );


        } else {

            $targetDir = $this->K_COMMON->pathCstSystem . "/" . $targetDir;
            $apiFM = $this->K_COMMON->getPluginObject("k_imagemanager", "api");
            $apiFM->imageResize(
                [
                    "filename" => $targetDir . $filename
                    , "newfilename" => $targetDir . $newfilename
                    , "width" => $width
                    , "height" => $height
                    , "checkWH" => $checkWH
                    , "compression" => $compression
                    , "chmod" => $chmod

                ]
            );

        }


    }

    private function copyFolderGeneral($arrayVariable)
    {


        //set variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode"); //Source web Code
        $webCodeTarget = $this->K_COMMON->getVarArray($arrayVariable, "webCodeTarget"); //Target Web Code
        $contentType = $this->K_COMMON->getVarArray($arrayVariable, "contentType"); //"": bucket base dir, "w": bucket website dir
        $targetType = $this->K_COMMON->getVarArray($arrayVariable, "targetType"); //"local": local dir, "static": kwebsite dir (local o s3)
        $sourceType = $this->K_COMMON->getVarArray($arrayVariable, "sourceType"); //"local": local dir, "static": kwebsite dir (local o s3)
        $sourceDir = $this->K_COMMON->getVarArray($arrayVariable, "sourceDir"); //without slash on start and end
        $targetDir = $this->K_COMMON->getVarArray($arrayVariable, "targetDir"); //without slash on start and end
        $removeTargetDir = $this->K_COMMON->getVarArray($arrayVariable, "removeTargetDir"); //1: remove target Dir before copy
        $location = $this->K_COMMON->getStaticContentLocation();
        $bucketName = $this->K_COMMON->getAWSBucket();

        //Include Object
        $objS3 = $this->K_COMMON->getPluginObject("k_aws", "s3");

        //check variables
        if ($contentType == "w") {

            if ($webCodeTarget == "")
                $webCodeTarget = $webCode;

            if ($targetType == "static")
                $targetDir = $this->K_COMMON->getStaticContentWebSitesDir($webCodeTarget) . $this->fixDir($targetDir);

            if ($sourceType == "static")
                $sourceDir = $this->K_COMMON->getStaticContentWebSitesDir($webCode) . $this->fixDir($sourceDir);


        } else {

            if ($targetType == "static")
                $targetDir = $this->K_COMMON->getStaticContentBaseDir() . $this->fixDir($targetDir);

            if ($sourceType == "static")
                $sourceDir = $this->K_COMMON->getStaticContentBaseDir() . $this->fixDir($sourceDir);
        }


        // If location is in local, change dir
        if ($location == "local"){
            $pathSystem = $this->K_COMMON->getPathSystem();

            if ($targetType == "static" && $location == "local")
                $targetDir = $pathSystem . "/" . $targetDir;

            if ($sourceType == "static" && $location == "local")
                $sourceDir = $pathSystem . "/" . $sourceDir;

        }


        //Check if source dir exist
        if ($sourceType == "local" || ($sourceType == "local" && $location == "local"))
            if (!is_dir($sourceDir))
                return true;



        if ($removeTargetDir == "1")
        {

            if ($location == "s3" && $targetType == "static")
            {
                //Remove File From S3
                $paramFileRemove["webCode"] = $webCodeTarget;
                $paramFileRemove["targetDir"] = $targetDir;
                $paramFileRemove["targetFile"] = "";

                $this->removeStaticContentWeb($paramFileRemove);
            }
            else
            {
                $this->K_COMMON->emptyDir(["dir" => $targetDir, "rmDir" => "1"]);
            }

        }


        //create target dir if local
        if ($location == "local" || $targetType == "local")
        {

            $this->K_COMMON->mkDir(["dir" => $targetDir, "mask" => 0777]);
        }


        if ($sourceType == "local" && $targetType == "static"){
            if ($location == "s3") {

                if (is_dir($sourceDir))
                    $objS3->uploadFolderToS3(["bucketName" => $bucketName, "localDir" => $sourceDir, "bucketDir" => $targetDir]);
            }
            else
                $this->copyFolderLocalToLocal($sourceDir, $targetDir);
        }
        elseif ($sourceType == "static" && $targetType == "local"){
            if ($location == "s3") {

                $objS3->downloadFolderFromS3(["bucketName" => $bucketName, "bucketDir" => $sourceDir, "localDir" => $targetDir]);
            }
            else
            {
                if (is_dir($sourceDir))
                    $this->copyFolderLocalToLocal($sourceDir, $targetDir);
            }

        }
        elseif ($sourceType == "local" && $targetType == "local"){
            if (is_dir($sourceDir))
                $this->copyFolderLocalToLocal($sourceDir, $targetDir);
        }
        elseif ($sourceType == "static" && $targetType == "static"){
            if ($location == "s3") {
                $objS3->duplicateFolderS3(["bucketName" => $bucketName, "sourceDir" => $sourceDir, "targetDir" => $targetDir]);
            }
            else {
                if (is_dir($sourceDir))
                    $this->copyFolderLocalToLocal($sourceDir, $targetDir);
            }
        }



        return true;
    }

    protected function copyFolderLocalToLocal($sourceDir, $targetDir)
    {
        $dir = opendir($sourceDir);
        @mkdir($targetDir);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($sourceDir . '/' . $file) ) {
                    $this->copyFolderLocalToLocal($sourceDir . '/' . $file,$targetDir . '/' . $file);
                }
                else {
                    copy($sourceDir . '/' . $file,$targetDir . '/' . $file);
                }
            }
        }
        closedir($dir);
    }




}

?>