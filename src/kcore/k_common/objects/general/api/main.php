<?
/**
 * KIXIXI k_common (api)
 * Create a panel web object for manage level authorization
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_common
 * @subpackage objects\api
 * @since 1.00
 */
namespace k_common\objects\general\api;
class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";


    /**
     * get Modal Ask Confirm
     *
     * @param array $ArrayVariable array to merge with result
     * The following directives can be used in the array variables:<br>
     *    *    idModal: id of modal
     *    *    type: "general" / "delete"
     *    *    body: body of modal for "general" type modal
     *    *    title: title of modal
     *    *    buttons: array of footer buttons
     *
     * @return array ["success" => "true", "html" => html Of Modal]
     */
    function getModalAskConfirm($ArrayVariable)
    {

        //get Modal Variables
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");
        $type = $this->K_COMMON->getVarArray($ArrayVariable, "type");
        $body = $this->K_COMMON->getVarArray($ArrayVariable, "body");
        $title = $this->K_COMMON->getVarArray($ArrayVariable, "title");
        $buttons = $this->K_COMMON->getVarArray($ArrayVariable, "buttons");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        //check variable
        if ($title == "")
            $title = "Alert";

        switch ($type) {
            case "custom" :
                $webModal->newModal(["id" => $idModal, "title" => $title, "type" => "custom"]);
                if ($body != "")
                    $webModal->appendHTML($webModal->modalBody, $body);
                break;
            case "general" :
                $webModal->newModal(["id" => $idModal, "title" => $title, "type" => "confirm"]);
                $webModal->appendHTML($webModal->modalBody, $body);
                break;
            case "delete" :
                $webModal->newModal(["id" => $idModal, "title" => $title, "type" => "confirm"]);
                if ($body == "")
                    $body = $this->getDictionaryWord("msgAskDelete");
                $webModal->appendHTML($webModal->modalBody, $body);
                break;
            case "deleteWithCheck" :
                $nameFieldConfirm = "deleteConfirmWord";
                if (is_array($buttons))
                    {
                        $typeOfModal = "custom";
                        $btnNum = -1;
                        foreach ($buttons as $button)
                        {
                            $btnNum++;
                            if ($this->K_COMMON->getVarArray($button,"disableWithCheck") == "1")
                                $buttons[$btnNum]["disableIF"] = ["type" => "different", "field" => "deleteConfirmWord", "word" => "DELETE"];
                        }
                    }
                else
                    $typeOfModal = "confirmDeleteCheck";

                $webModal->newModal(["id" => $idModal, "title" => $title, "type" => $typeOfModal, "fieldConfirm" => $nameFieldConfirm, "buttons" => $buttons]);
                if ($body == "")
                    $body = $this->getDictionaryWord("msgAskDelete");

                $webModal->appendRow($webModal->modalBody);
                $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);
                $webModal->appendHTML($webModal->lastCol, $body);

                $webModal->appendRow($webModal->modalBody,["style"=>"margin-top:30px;"]);
                $webModal->appendColumn($webModal->lastRow, ["nCol" => 12]);

                $label = $this->getDictionaryWord("msgAskDeleteWithCheck");


                $webModal->appendInput($webModal->lastCol,
                    [
                        "type" => "text",
                        "labelInput" => $label,
                        "id" => $nameFieldConfirm,
                        "name" => $nameFieldConfirm,
                        "value"=> "",
                        "textHelp" => $this->getDictionaryWord("msgAskDeleteWithCheckTextHelp")
                    ]
                );

                break;
            case "confirm" :
                $webModal->newModal(["id" => $idModal, "title" => $title, "type" => "confirm"]);
                if ($body == "")
                    $body = $this->getDictionaryWord("msgAskConfirm");
                $webModal->appendHTML($webModal->modalBody, $body);
                break;

        }

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }


    /**
     * get new Modal
     *
     * @param array $ArrayVariable array to merge with result
     * The following directives can be used in the array variables:<br>
     *    *    idModal: id of modal
     *    *    type: "general" / "delete"
     *    *    body: body of modal for "general" type modal
     *
     * @return array ["success" => "true", "html" => html Of Modal]
     */
    function getNewModal($ArrayVariable)
    {

        //include image api
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager","web_modal");

        //GET html Modal
        $webModal->newModal($ArrayVariable);

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];




    }

}
?>

