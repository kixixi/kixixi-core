<?
/**
 * KIXIXI INTERCEPTOR
 * This object call a function inner actual object, and search customization before and after execution function called
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_common
 * @subpackage objects\interceptor
 * @since 1.00
 */
namespace k_common\objects\general\interceptor;
class obj
{
    /**
     * Object caller
     *
     * @since 1.00
     * @access public
     */
    var $objCaller;


    /**
     * Construct
     *
     */
    function __construct($objCaller)
    {

        //set object caller
        $this->objCaller = $objCaller;

    }

    /**
 * This Function Execute a method of this object
 *
 * @return string|object|array function called value
 */
    function __call($method, $arguments)
    {
        
        return $this->objCaller->executeMethod($method, $arguments);


    }


    /**
     * This Function Execute a method of this object
     *
     * @return string|object|array function called value
     */
    function __get($name)
    {


        return $this->objCaller->$name;


    }


    /**
     * Check if method Exist in OBJ CALLER
     *
     * @return boolean
     */
    function methodExist($name)
    {


        return method_exists($this->objCaller,$name);


    }



}

?>