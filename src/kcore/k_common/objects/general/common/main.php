<?
/**
 * KIXIXI COMMON OBJECT
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_common
 * @subpackage objects\common
 * @since 1.00
 */
namespace k_common\objects\general\common;
class obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";


    /**
     * actual web code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $webCode = "";


    /**
     * web code of web that we are editing
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $editingWebCode = "";


    /**
     * link to kixixi DB
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $kxdb;

    /**
     * customer directory
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $pathCstSystem = ""; //Path of Customer Ex: /usr/local/wsshared/websites/wskixixi/kixixi_xxxxx

    /**
     * customer web directory
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $pathCstWeb = ""; //Path of web of customer Ex: /usr/local/wsshared/websites/wskixixi/kixixi_xxxxx/kwebsites/1

    /**
     * kixixi directory
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $pathKixixi = "";

    /**
     * temporary directory
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $pathWebTmp = "";

    /**
     * AWS Bucket
     *
     * @since 1.00
     * @access private
     * @var string
     */
    private $AWSBucket = "";

    /**
     * AWS Bucket
     *
     * @since 1.00
     * @access private
     * @var string
     */
    private $AWSAccessKey = "";

    /**
     * AWS Bucket
     *
     * @since 1.00
     * @access private
     * @var string
     */
    private $AWSSecretKey = "";

    /**
     * location of static content (AWS S3 or local)
     *
     * @since 1.00
     * @access private
     * @var string
     */
    private $staticContentLocation = "";

    /**
     * url of media files content (AWS or local) with base directory
     *
     * @since 1.00
     * @access private
     * @var string
     */
    private $staticContentURL = "";

    /**
     * base directory where upload media files
     *
     * @since 1.00
     * @access private
     * @var string
     */
    private $staticContentBaseDir = "";

    /**
     * base directory of websites (AWS or local)
     *
     * @since 1.00
     * @access private
     * @var string
     */
    private $staticContentWebSitesDir = "";

    /**
     * actual URL
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $URLActual = "";

    /**
     * URL Referrer
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $URLReferrer = "";

    /**
     * decimal separator
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $decimalSep = ",";

    /**
     * thousands separator
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $thousandsSep = ".";

    /**
     * language of user
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $language = "";

    /**
     * location of user
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $location = "";

    /**
     * dictionary
     *
     * @since 1.00
     * @access public
     * @var array
     */
    var $dictionary;

    /**
     * dictionary object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $objDictionary;

    /**
     * API Registry
     *
     * @since 1.00
     * @access public
     * @var object
     */
    var $APIRegistry;

    /**
     * API URL Call
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $KAPIURLCall = "/kapi/kapi.php";


    /**
     * Return the code of current user language
     *
     * @return string
     *
     */
    function getCurrentSessionID()
    {
        return session_id();
    }

    /**
     * Return the code of current user language
     *
     * @return string
     *
     */
    function getCurrentLanguage()
    {
        return $this->language;
    }

    /**
     * Return the code of current user language
     *
     * @return string
     *
     */
    function getCurrentUser()
    {
        return $this->getVarSession("KSESSION_Logon_User");
    }

    /**
     * Return the current user first name (if logged)
     *
     * @return string
     *
     */
    function getCurrentUserFirstName()
    {
        return $this->getVarSession("KSESSION_User_FirstName");
    }

    /**
     * Return the current user last name (if logged)
     *
     * @return string
     *
     */
    function getCurrentUserLastName()
    {
        return $this->getVarSession("KSESSION_User_LastName");
    }

    /**
     * Return the current user complete name (if logged)
     *
     * @return string
     *
     */
    function getCurrentUserNameComplete()
    {
        return $this->getVarSession("KSESSION_User_FirstName") . " " . $this->getVarSession("KSESSION_User_LastName");
    }

    /**
     * Return the current user profile Image
     *
     * @return string
     *
     */
    function getProfileImage($anaCode = "")
    {

        //set variables
        $table = $this->getPluginDataObject("k_registry", "registry");

        //check variables
        if ($anaCode == "")
            $anaCode = $this->getCurrentUser();

        //get profile data
        $table->get(["ana_photo"], ["ana_code" => $anaCode]);
        $data = $table->fetch();

        $photo = $data->ana_photo;

        if ($photo == "")
            return "";
        else
            return $this->getStaticContentURL() . "/users/" . $anaCode . "/" . $photo;
    }

    /**
     * Return the current user security level type (if logged)
     *
     * @return string
     *
     */
    function getCurrentUserLevelType()
    {
        return $this->getVarSession("KSESSION_Level_Typ_Code");
    }

    /**
     * Return the current session Friend Code
     * May be different from ref code on table registry
     * This is set by url
     *
     * @return string
     *
     */
    function getSessionFriendCode()
    {
        return $this->getVarSession("KSESSION_friendCode");
    }


    /**
     * Return the user email
     *
     * @param string $anaCode code of user
     *
     * @return string
     *
     */
    function getUserEmail($anaCode = "")
    {
        //check variables
        if ($anaCode == "")
            $anaCode = $this->getCurrentUser();

        $this->APIRegistry = $this->newOnce($this->APIRegistry, '\k_registry\objects\general\api\obj');
        return $this->APIRegistry->getContact($anaCode, "EM");
    }

    /**
     * Return a user contact
     *
     * @param string $anaCode code of user
     * @param string $contactType type of contact (EM: Email, CEL: Cellular, TEL: Telephone)
     * @param array $arrayVariable string to be escaped
     * The following directives can be used in the array variables:<br>
     *    *    qty: number of result (empty for all)
     *    *    OM: output model empty for string, "ARRAY" for array
     *
     * @return string
     *
     */
    function getUserContact($anaCode = "", $contactType, $arrayVariable = [])
    {
        $this->APIRegistry = $this->newOnce($this->APIRegistry, '\k_registry\objects\general\api\obj');
        return $this->APIRegistry->getContact($anaCode, $contactType, $arrayVariable);
    }

    /**
     * Return REQUEST_URI variable
     *
     * @return string
     *
     */
    function getCurrentURI()
    {
        return $_SERVER['REQUEST_URI'];
    }

    /**
     * Return current url path (ex: /manage) without parameters
     *
     * @return string
     *
     */
    function getCurrentURlPath()
    {

        if (strpos($this->URLActual, "?") > 0)
            $this->URLActual = substr($this->URLActual, 0, strpos($this->URLActual, "?"));

        return $this->URLActual;
    }

    /**
     * Return current url complete
     *
     * @return string
     *
     */
    function getCurrentURL()
    {
        $s = $_SERVER;
        $use_forwarded_host = false;
        $ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on');
        $ssl = $ssl || ($this->getVarArray($s,'HTTP_X_FORWARDED_PROTO') == 'https'); //For amazon load balancer redirect
        $sp = strtolower($s['SERVER_PROTOCOL']);
        $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
        $port = $s['SERVER_PORT'];
        $port = ((!$ssl && $port == '80') || ($ssl && $port == '443')) ? '' : ':' . $port;
        $host = ($use_forwarded_host && isset($s['HTTP_X_FORWARDED_HOST'])) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
        $host = isset($host) ? $host : $s['SERVER_NAME'] . $port;
        $url = $s['REQUEST_URI'];
        if (strpos($url, "/?") === false)
            if (substr($url,-1,1) != "/")
                $url .= "/";
        return $protocol . '://' . $host . $url;

    }


    /**
     * Location HREF
     *
     * @param string $url
     *
     *
     * @return string
     *
     */
    function locationHREF($url = ""){
        header("location: ".$url);
    }

    /**
     * Replate url parameter
     *
     * @param array $parameters: parameters of url to replace (or add)
     * @param string $url : url to modify (default current url)
     *
     *
     * @return string
     *
     */
    function modifyParametersURL($parameters, $url = "")
    {

        //set variables
        if ($url == "")
            $url = $this->getCurrentURL();
        $urlParsed = parse_url($url);
        $urlScheme = $this->getVarArray($urlParsed, "scheme");
        $urlHost = $this->getVarArray($urlParsed, "host");
        $urlPath = $this->getVarArray($urlParsed, "path");
        $urlQuery = $this->getVarArray($urlParsed, "query");
        if ($urlScheme != "")
            $urlScheme = $urlScheme . "://";
        if ($urlQuery != "")
            $urlQuery = "?" . $urlQuery;
        if (substr($urlPath,-1,1) == "/")
            $urlPath = chop($urlPath,"/");
        if (substr($urlPath,0,1) == "/")
            $urlPath = substr($urlPath,1);
        $splitURL = explode('/', $urlPath );



        //Check each parameter
        foreach ($parameters as $par => $val)
        {
            $pos = array_search($par, $splitURL);
            if ($pos !== false)
                $splitURL[$pos + 1] = $val;
            else
            {
                $splitURL[] = $par;
                $splitURL[] = $val;
            }

        }

        //create new path url
        $newPath = "/";
        foreach ($splitURL as $key => $val)
        {
            $newPath .= $val . "/";
        }

        $urlNew = $urlScheme . $urlHost . $newPath . $urlQuery;

        return $urlNew;



    }

    /**
     * Return current host
     *
     * @return string
     *
     */
    function getCurrentHost($withProtocol = true)
    {
        $pageURL = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === true ? 'https://' : 'http://';
        $acturl = ($withProtocol ? $pageURL : "") . $_SERVER['HTTP_HOST'];
        $pos = strpos($acturl, "?404;");
        if ($pos > 0) $acturl = substr($acturl, $pos + 5);
        return $acturl;
    }

    /**
     * Return client IP
     *
     * @return string
     *
     */
    function getClientIP()
    {
        $ip = $this->getVarArray($_SERVER, 'HTTP_X_FORWARDED_FOR');
        if ($ip == "")
            $ip = $_SERVER['REMOTE_ADDR'];
        return $ip;
    }


    /**
     * Return current web code
     *
     * @return string
     *
     */
    function getCurrentWeb()
    {
        return $this->webCode;
    }

    /**
     * Set Editing Web
     *
     * @return string
     *
     */
    function setEditingWeb($webCode)
    {
        $_SESSION["KSESSION_Editing_Web_Code"] = $webCode;
        $this->editingWebCode = $webCode;
    }

    /**
     * Return current web code Editing
     *
     * @return string
     *
     */
    function getEditingWeb()
    {
        return $this->getVarSession("KSESSION_Editing_Web_Code");
    }


    /**
     * Return API URL Call (Base)
     *
     * @return string
     *
     */
    function getAPIURLCall()
    {
        return $this->KAPIURLCall;
    }



    /**
     * left of a string
     *
     * @param string $string string to be elaborate
     * @param int $count number of char to get
     *
     * @return string
     *
     */
    function left($string, $count)
    {
        return substr($string, 0, $count);
    }

    /**
     * right of a string
     *
     * @param string $string string to be elaborate
     * @param int $count number of char to get
     *
     * @return string
     *
     */
    function right($string, $count)
    {
        return substr($string, (strlen($string) - $count), strlen($string));
    }

    /**
     * Return if a string contain another string
     *
     * @param string $str my string
     * @param string $find find string
     *
     * @return boolean
     *
     */
    function inString($str, $find)
    {

        if (strpos($str, $find) === false)
            return false;
        else
            return true;
    }

    /**
     * get a value from $_SESSION
     *
     * @param string $var var to get from $_SESSION
     *
     * @return string
     *
     */
    function getVarSession($var)
    {
        if (isset($_SESSION[$var]))
            return $_SESSION[$var];
        else
            return "";
    }

    /**
     * get a value from $_REQUEST
     *
     * @param string $var var to get from $_REQUEST
     * @param int $escape 1 for escape
     * @param int $htmlSpecialChars 1 for htmlspecialchars
     *
     * @return string
     *
     */
    function getVarRequest($var, $htmlSpecialChars = 0)
    {
        $varRet = "";
        if (isset($_REQUEST[$var]))
            $varRet = $_REQUEST[$var];
        if ($htmlSpecialChars == 1)
            $varRet = htmlspecialchars($varRet);
        return $varRet;
    }

    /**
     * get a value from an array
     *
     * @param array $array array from get variable
     * @param string $var var to get from
     *
     * @return string
     *
     */
    function getVarArray($array, $var)
    {


        if (isset($array[$var]))
            return $array[$var];
        else
            return "";
    }

    /**
     * get a value from a constant
     *
     * @param array $array array from get variable
     * @param string $var var to get from
     *
     * @return string
     *
     */
    function getConstant($var)
    {
        if (defined($var))
            return constant($var);
        else
            return "";
    }

    /**
     * get kixixi path
     * @return string
     *
     */
    function getPathKixixi()
    {
        return $this->pathKixixi;
    }

    /**
     * get kixixi KCORE path
     * @return string
     *
     */
    function getPathKCore()
    {
        return $this->pathKixixi . "/kcore";
    }

    /**
     * get System path
     * @return string
     *
     */
    function getPathSystem()
    {
        return $this->pathCstSystem;
    }

    /**
     * get kixixi KPLUGIN path
     * @return string
     *
     */
    function getPathKPlugin()
    {
        return $this->pathCstSystem . "/kplugin";
    }

    /**
     * get kixixi KTEMPLATE path
     * @return string
     *
     */
    function getPathKTemplate()
    {
        return $this->pathCstSystem . "/ktemplate";
    }

    /**
     * get kixixi KDATA path
     * @return string
     *
     */
    function getPathKData()
    {
        return $this->pathCstSystem . "/kdata";
    }

    /**
     * get path of a web
     *
     * @param string $webCode web code (if empty get current web code)
     *
     * @return string
     *
     */
    function getPathWeb($webCode)
    {
        return $this->pathCstSystem . "/kwebsites/" . $webCode;
    }

    /**
     * concat a variable to array
     * Example if i have this array ["a" => "1"] and i want concat at "a" the number 2
     * It check if in array exist index "a", if exist concat, otherwise create it
     *
     * @param array $array array to modify
     * @param string $var index of array to concat
     * @param string $val value to concat
     * @param boolean $append true for append, false for prepend
     *
     *
     */
    function concatVarArray(&$array, $var, $val, $append = true)
    {
        if ($val == "")
            return;
        if (isset($array[$var])) {
            if (is_array($val)) {
                if (is_array($array[$var]))
                    $array[$var] = array_merge($array[$var], $val);
                else
                    $array[$var] = $val;
            } else {
                if ($append) {
                    if (is_array($array[$var]))
                        array_push($array[$var], $val);
                    else
                        $array[$var] .= $val;
                } else {
                    if (is_array($array[$var]))
                        array_push($array[$var], $val);
                    else
                        $array[$var] = $val . $array[$var];
                }
            }
        } else
            $array[$var] = $val;

    }

    /**
     * normalize a string
     *
     * @param string $string string to normalize
     *
     * @return string
     *
     */
    function normalizeString($string)
    {
        $string = trim($string);

        if (ctype_digit($string)) {
            return $string;
        } else {
            // replace accented chars
            $accents = '/&([A-Za-z]{1,2})(grave|acute|circ|cedil|uml|lig);/';
            $string_encoded = htmlentities($string, ENT_NOQUOTES, 'UTF-8');
            $string = preg_replace($accents, '$1', $string_encoded);

        }

        return strtolower($string);
    }

    /**
     * convert an array to associative
     * Example: ["a", "1", "b", "2"] become ["a" => "1", "b" => "2"]
     *
     * @param array $oriArray
     *
     * @return array
     *
     */
    function arrayToAssociative($oriArray)
    {

        $newArray = [];

        while (!empty($oriArray)) {
            $key = current($oriArray);
            $value = next($oriArray);
            $newArray[$key] = $value;
            array_shift($oriArray);
            array_shift($oriArray);
        }

        return $newArray;
    }

    /**
     * convert a recursive array to flat
     * Example: ["a" => ["b" => "2"]] become ["a", "b", "2"]
     *
     * @param array $array
     *
     * @return array
     *
     */
    function arrayToFlat($array)
    {

        $tmp = array();
        foreach ($array as $a) {
            if (is_array($a)) {
                $tmp = array_merge($tmp, $this->arrayToFlat($a));
            } else {
                $tmp[] = $a;
            }
        }

        return $tmp;
    }

    /**
     * convert array to XML (from db data)
     *
     * @param $AV $array
     * The following directives can be used in the array variables:<br>
     *    *    array: array to be converted
     *
     * @return string
     *
     */
    function dbDataToXML($AV)
    {

        $dataSetTag = "dataset";
        $resultTag = "results";
        $rowTag = "row";
        $arrayToBeConverted = $this->getVarArray($AV, "array");
        if (empty($arrayToBeConverted))
            $arrayToBeConverted = [];


        $length = count($arrayToBeConverted);

        $XMLOutput = "";
        $XMLOutput .= "<$dataSetTag>" . chr(13);
        $XMLOutput .= "<$resultTag>" . $length . "</$resultTag>" . chr(13);
        reset($arrayToBeConverted);
        for ($i = 0; $i < $length; $i++) {

            $XMLOutput .= "<$rowTag>" . chr(13);
            $FieldList = current($arrayToBeConverted);
            for ($j = 0; $j < count($FieldList); $j++) {
                $FieldName = key($FieldList);
                $FieldValue = current($FieldList);
                $XMLOutput .= '<' . $FieldName . '>' . $FieldValue . '</' . $FieldName . '>' . chr(13);
                next($FieldList);
            } //End For j

            $XMLOutput .= "</$rowTag>" . chr(13);
            next($arrayToBeConverted);
        } //End For i

        $XMLOutput .= "</$dataSetTag>";
        return $XMLOutput;

    }

    /**
     * load XML File
     *
     * @param $fileName file name
     * @param $convert if true convert to arrat
     *
     *
     * @return xml|array
     */

    function loadXMLFile($fileName, $convert = false)
    {

        $x = simplexml_load_file($fileName , null, LIBXML_NOCDATA);

        if ($convert)
            return $this->xmlToArray($x);
        else
            return $x;

    }


    /**
     * convert XML To Array removing Simple Object
     *
     * @param $xml xml
     *
     *
     * @return array
     */

    function xmlToArray($xml)
    {

        $json = json_encode(new \SimpleXMLElement($xml->asXML(), LIBXML_NOCDATA));
        return json_decode($json, true);



    }

    /**
     * save XML To File
     *
     * @param $xml xml
     * @param $dir dir where save file
     * @param $filename filename with .xml extension
     *
     *
     * @return array
     */

    function saveXMLToFile($xml, $dir, $filename)
    {

        //Create XML File
        $xmlFile = $xml->asXML();

        //format Output
        $dom = new \DOMDocument('1.0');
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($xmlFile);
        $xmlFile = $dom->saveXML();

        //Save File
        $arraySaveContent = [];
        $arraySaveContent["dir"] = $dir;
        $arraySaveContent["filename"] = $filename;
        $arraySaveContent["content"] = $xmlFile;
        $arraySaveContent["append"] = 0;
        $this->saveFileContent($arraySaveContent);



    }


    /**
     * convert array to XML
     *
     * @param $data array to be convert
     * @param $xml to add child
     * @param $tagUp tag up
     *
     *
     *
     */

    function arrayToXML($data, $xml, $tagUp = "", $useCDATA = false)
    {


        foreach ($data as $key => $value) {

            if (is_numeric($key)) {
                $key = ($tagUp != "" ? $tagUp : $key);
            }
            if (is_array($value)) {

                $keyValue = key($value);

                if (count($value) > 0)
                {
                    if (is_numeric($keyValue))
                        $new_object = $xml;
                    else
                        $new_object = $xml->addChild($key);

                    $this->arrayToXML($value, $new_object, $key, $useCDATA);
                }




            } else {
                if ($useCDATA)
                {
                    $c = $xml->addChild($key);
                    $c -> addCData($value);
                }
                else
                    $xml->addChild($key, $value);
            }
        }
    }

    /**
     * append XML to XML
     *
     * @param $AV $array
     * The following directives can be used in the array variables:<br>
     *    *    array: array to be converted
     *
     * @return string
     *
     */

    function appendToXML(&$xml, $xmlToAppen)
    {
        // Create new DOMElements from the two SimpleXMLElements
        $domXML = dom_import_simplexml($xml);
        $domXMLToAppend = dom_import_simplexml($xmlToAppen);

        // Import the <cat> into the dictionary document
        $domXMLToAppend = $domXML->ownerDocument->importNode($domXMLToAppend, TRUE);

        // Append the <cat> to <c> in the dictionary
        $domXML->appendChild($domXMLToAppend);
    }

    /**
     * generate a random string (ex password) of n characters
     *
     * @param int $length length of string to be generate
     * @param string $allow characters allowed, default "abcdefghijklmnopqrstuvwxyz0123456789!"
     *
     * @return string
     *
     */
    function generateRandomString($length, $allow = "abcdefghijklmnopqrstuvwxyz0123456789!")
    {
        $i = 1;
        $ret = "";
        while ($i <= $length) {

            $max = strlen($allow) - 1;
            $num = rand(0, $max);
            $temp = substr($allow, $num, 1);
            $ret = $ret . $temp;
            $i++;
        }
        return $ret;

    }

    /**
     * validate an email
     *
     * @param string $email email to be validated
     *
     * @return boolean
     *
     */
    function validateEmail($email)
    {
        // if empty return
        if (!$email) {
            return false;
        }

        // check if contain @
        $num_at = count(explode('@', $email)) - 1;
        if ($num_at != 1) {
            return false;
        }

        // check if there are some not allowed char
        if (strpos($email, ';') || strpos($email, ',') || strpos($email, ' ')) {
            return false;
        }

        // check the format
        if (!preg_match('/^[\w\.\-\+]+@\w+[\w\.\-]*?\.\w{1,4}$/', $email)) {
            return false;
        }

        return true;
    }


    /**
     * format a DB date
     *
     * @param string $dateDB date DB formatted
     * @param string $format output format, see php date
     *
     * @return date
     *
     */
    function dateDBConvert($dateDB, $format)
    {

        $date = new \DateTime($dateDB);
        return $date->format($format);

    }

    /**
     * format a DB date and get only date, without time
     *
     * @param string $dateDB date DB formatted
     *
     * @return date
     *
     */
    function dateDBNoTime($dateDB)
    {
        return $this->dateDBConvert($dateDB, "Y-m-d");

    }

    /**
     * format a DB date to timestamp
     *
     * @param string $dateDB date DB formatted
     * @param int $dayAdd day to add before get timestamp (default zero)
     *
     * @return date
     *
     */
    function dateDBToTimeStamp($dateDB, $dayAdd = 0)
    {

        $date = new \DateTime($dateDB);
        $date->modify(($dayAdd) . 'day');
        return $date->getTimestamp();
    }

    /**
     * format time in decimal value.
     * Example "10:15" become 10.25
     * Example "10:30" become 10.50
     * Example "10:45" become 10.75
     *
     * @param string $time time to be formatted
     *
     * @return decimal
     *
     */
    function timeToDecimal($time)
    {
        $timeArr = explode(':', $time);
        $decTime = ($timeArr[0] * 60) + ($timeArr[1]) + (isset($timeArr[2]) ? ($timeArr[2] / 60) : 0);

        return $decTime / 60;
    }

    /**
     * get time zone with google api
     *
     * @param string $lat latitude
     * @param string $lng longitude
     *
     * @return array
     *
     */
    function getGeoTimeZone($lat, $lng)
    {
        $jsonObject = file_get_contents("https://maps.googleapis.com/maps/api/timezone/json?timestamp=0&sensor=false&location=" . $lat . "," . $lng);
        $object = (array)json_decode($jsonObject);
        if (isset($object["dstOffset"])) {
            $object["dstOffset"] = $object["dstOffset"] / 3600;
            $object["rawOffset"] = $object["rawOffset"] / 3600;
        } else {
            $object["dstOffset"] = 0;
            $object["rawOffset"] = 0;

        }
        return $object;
    }

    /**
     * get latitude, longitude and country from google api
     *
     * @param array $arrayVariable
     * The following directives can be used in the array variables:<br>
     *    *    address: address which search data
     *
     * @return array
     *
     */
    public function getGeocodeAddress($arrayVariable)
    {


        //set variable
        $address = $this->getVarArray($arrayVariable, "address");
        $base_url = "https://maps.googleapis.com/maps/api/geocode/xml?address=";
        $request_url = $base_url . urlencode($address) . "&sensor=true";
        $xml = simplexml_load_file($request_url);
        $country = "";
        if (isset($xml->result->address_component)) {
            $arr = $xml->result->address_component;
            foreach ($arr as $key) {
                if ($key->type[0] == "country") $country = $key->short_name;
            }
        }

        if ($xml->status != "OK")
            return array("success" => "false", "reason" => "addressError");
        else
            return array("success" => "true", "lat" => $xml->result->geometry->location->lat, "lng" => $xml->result->geometry->location->lng, "country" => $country);
    }

    /**
     * make a post request and get response
     *
     * @param string $url url for post
     * @param string $data parameters to send
     * @param string $optional_headers optional header
     *
     * @return string
     *
     */
    function doPostRequest($url, $data, $optional_headers = null)
    {
        if (!function_exists('curl_init')) {
            $params = array(
                'http' => array(
                    'method' => 'POST',
                    'content' => $data
                )
            );
            if ($optional_headers !== null) {
                $params['http']['header'] = $optional_headers;
            }
            $ctx = stream_context_create($params);
            $fp = @fopen($url, 'rb', false, $ctx);
            if (!$fp) {
                return 'Error';
            }
            $response = @stream_get_contents($fp);
            if ($response === false) {
                return 'Error';
            }
            return $response;
        } else {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Generic Client');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_URL, $url);

            if ($optional_headers !== null) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, $optional_headers);
            }

            $response = curl_exec($ch);

          //  print $response;



            curl_close($ch);
            if (!$response) {
                return 'Error';
            }
            return $response;
        }
    }

    /**
     * curl get content from url
     *
     * @param string $url url
     *
     * @return string
     *
     */
    function curlGetUrlContent($url)
    {
        $curl = curl_init();
        $userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';

        curl_setopt($curl, CURLOPT_URL, $url); //The URL to fetch. This can also be set when initializing a session with curl_init().
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE); //TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5); //The number of seconds to wait while trying to connect.

        curl_setopt($curl, CURLOPT_USERAGENT, $userAgent); //The contents of the "User-Agent: " header to be used in a HTTP request.
        curl_setopt($curl, CURLOPT_FAILONERROR, TRUE); //To fail silently if the HTTP code returned is greater than or equal to 400.
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE); //To follow any "Location: " header that the server sends as part of the HTTP header.
        curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE); //To automatically set the Referer: field in requests where it follows a Location: redirect.
        curl_setopt($curl, CURLOPT_TIMEOUT, 10); //The maximum number of seconds to allow cURL functions to execute.

        $contents = curl_exec($curl);
        curl_close($curl);
        return $contents;
    }


    /**
     * check if the device is mobile
     *
     * @return boolean
     *
     */
    function isMobile()
    {
        $array_mobile = array(
            'iphone',
            'ipod',
            'ipad',
            'android',
            'blackberry',
            'opera mobi',
            'windows ce',
            'windows phone os',
            'symbian'

        );

        $UA = isset($_SERVER['HTTP_USER_AGENT']) ? (string)$_SERVER['HTTP_USER_AGENT'] : '';
        $regex = "/(" . implode("|", $array_mobile) . ")/i";
        return preg_match($regex, $UA);
    }

    /**
     * load content of a file
     *
     * @param array $arrayVariable
     * The following directives can be used in the array variables:<br>
     *    *    dir: directory of file
     *    *    filename: file name
     *
     * @return string
     *
     */
    function loadFileContent($arrayVariable)
    {


        $dir = $this->getVarArray($arrayVariable, "dir");
        $filename = $this->getVarArray($arrayVariable, "filename");

        $filenamedir = $dir . "/" . $filename;
        if (!file_exists($filenamedir)) return false;
        $handle = fopen($filenamedir, "r");
        if ($handle == false) return false;
        $contents = fread($handle, filesize($filenamedir));
        fclose($handle);
        return $contents;


    }

    /**
     * save content on file
     *
     * @param array $arrayVariable
     * The following directives can be used in the array variables:<br>
     *    *    dir: directory of file
     *    *    filename: file name
     *    *    content: content of file
     *    *    append: 1 for append content
     *
     * @return string
     *
     */
    function saveFileContent($arrayVariable)
    {

        $dir = $this->getVarArray($arrayVariable, "dir");
        $filename = $this->getVarArray($arrayVariable, "filename");
        $content = $this->getVarArray($arrayVariable, "content");
        $append = $this->getVarArray($arrayVariable, "append");
        $filenamedir = $dir . (substr($dir, -1, 1) == "/" ? "" : "/") . $filename;

        if (!is_dir($dir)) return false;
        if (!file_exists($filenamedir)) {
            $handle = fopen($filenamedir, "x");
        } elseif ($append == 1) {
            $handle = fopen($filenamedir, "a");
        } else {
            $handle = fopen($filenamedir, "w");
        }
        if ($handle == false) return false;
        $result = fwrite($handle, $content);
        fclose($handle);

        if ($result === false)
            return false;
        else
            return true;
    }

    /**
     * create Directory
     *
     * @param array $arrayVariable
     * The following directives can be used in the array variables:<br>
     *    *    dir: base directory 
     *    *    mask: mask
     *
     * @return string
     *
     */
    function mkDir($arrayVariable)
    {

        $dir = $this->getVarArray($arrayVariable, "dir");
        $mask = $this->getVarArray($arrayVariable, "mask");

        $oldmask = umask(0);

        @mkdir($dir, $mask, true);

        umask($oldmask);

    }

    /**
     * remove Directory
     *
     * @param array $arrayVariable
     * The following directives can be used in the array variables:<br>
     *    *    dir: base directory
     *
     * @return string
     *
     */
    function rmDir($arrayVariable)
    {

        $dir = $this->getVarArray($arrayVariable, "dir");

        if (is_dir($dir))
        {
            $oldMask = umask(0);
            @chmod($dir, 0777);

            rmdir($dir);
            umask($oldMask);
        }




    }

    /**
     * remove all file from Directory
     *
     * @param array $arrayVariable
     * The following directives can be used in the array variables:<br>
     *    *     dir: base directory
     *    *     rmDir: 1 if you want remove also master dir
     *
     * @return string
     *
     */
    function emptyDir($arrayVariable)
    {

        $dir = $this->getVarArray($arrayVariable, "dir");
        $rmDir = $this->getVarArray($arrayVariable, "rmDir");


        if (is_dir($dir) === true)
        {
            $files = array_diff(scandir($dir), array('.', '..'));

            foreach ($files as $file)
            {
                $this->emptyDir(["dir" => realpath($dir) . '/' . $file, "rmDir" => "1"]);
            }

            if ($rmDir == "1")
            {

                $oldMask = umask(0);
                @chmod($dir, 0777);

                rmdir($dir);
                umask($oldMask);
            }
        }

        else if (is_file($dir) === true)
        {
            $oldMask = umask(0);
            @chmod($dir, 0777);


            unlink($dir);
            umask($oldMask);
        }

    }

    /**
     * copy Directory
     *
     * @param string $src source directory
     * @param string $dst destination directory
     *
     * @return string
     *
     */
    function copyDir($src,$dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    $this->copyDir($src . '/' . $file,$dst . '/' . $file);
                }
                else {
                    copy($src . '/' . $file,$dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    /**
     * get list of files in directory
     *
     * @param string $dir directory
     * @param boolean $recursive if true search recursive
     * @param string $extension if valued get only files with this extension
     *
     * @return array with files
     *
     */
    function getFilesFromDir($dir, $recursive = false, $extension = "")
    {

        $files = array();
        if ($handle = opendir($dir)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    if (is_dir($dir . '/' . $file)) {
                        if ($recursive) {
                            $dir2 = $dir . '/' . $file;
                            $files[] = $this->getFilesFromDir($dir2, $recursive, $extension);
                        }
                    } else {
                        if ($extension == "") $files[] = $dir . '/' . $file;
                        else {
                            if (substr($file, strlen($file) - 4) == $extension) $files[] = $dir . '/' . $file;
                        }
                    }
                }
            }
            closedir($handle);
        }

        return $this->arrayToFlat($files);
    }

    /**
     * get list of files in directory
     *
     * @param string $dir directory
     * @param array $dirList return array with dir list
     * @param array $files return array with files
     *
     *
     */
    public function getFilesAndDir($dir, &$dirList, &$files)
    {
        if ($handle = opendir($dir)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    if (is_dir($dir . "/" . $file)) {
                        $dirList[] = $file;
                    } else {
                        $files[] = $file;
                    }
                }
            }
            closedir($handle);
        }
    }

    /**
     * get extension of file
     *
     * @param string $str filename
     *
     * @return string
     *
     */
    function getFileExtension($str)
    {
        $i = strrpos($str, ".");
        if ($i === false) {
            return "";
        }
        $explode = explode('.', $str);

        $ext = end($explode);
        return $ext;
    }

    /**
     * upload a file from $_FILES
     *
     * @param string $fileToUpload (ex: $_FILES["image"])
     * @param string $targetDir filename
     * @param string $fileNameSave filename
     * @param string $fnum optional file number (if multi files the number of file)
     *
     * @return string
     *
     */
    function uploadFile($fileToUpload, $targetDir, $fileNameSave = "", $fnum = "")
    {
        if ($fileNameSave == "") $fileNameSave = $fileToUpload['name'];
        $FileUploaded = 0;

        if ($fnum === "") {//Upload file of single upload
            $FileName = $fileToUpload['name'];
            $FileNameTmp = $fileToUpload['tmp_name'];
        } else { //upload file of multiple upload

            $FileName = $fileToUpload['name'][$fnum];
            $FileNameTmp = $fileToUpload['tmp_name'][$fnum];

        }

        if (isset($FileName)) {
            do {
                if (is_uploaded_file($FileNameTmp)) {


                    if (file_exists($targetDir . $fileNameSave)) {
                        unlink($targetDir . $fileNameSave); //Delete Existing File
                    }
                    // Move file in destination folder
                    if (!move_uploaded_file($FileNameTmp, $targetDir . $fileNameSave)) {
                        $FileUploaded = 0; // Errore Uploading
                        break;
                    } else {
                        $FileUploaded = 1; //Uploaded

                    }
                }
            } while (false);
        }

        return $FileUploaded;
    }


    /**
     * set Static Content Variables
     *
     * @return string
     *
     */
    function setStaticContentVariables()
    {
        $this->staticContentLocation = $this->getConstant("staticContentLocation");
        if ($this->staticContentLocation == "s3") {
            $this->staticContentBaseDir = $this->getConstant("AWSBucketBaseDir"); //base directory of bucket
            $this->staticContentWebSitesDir = $this->getConstant("AWSBucketWebsitesDir"); //base directory of websites
            $this->staticContentURL = $this->getConstant("AWSBucketURL"); //url of bucket
            $this->AWSBucket = $this->getConstant("AWSBucket");
            $this->AWSAccessKey = $this->getConstant("AWSBucketAccessKey");
            $this->AWSSecretKey = $this->getConstant("AWSBucketSecretKey");
        } else {
            $this->staticContentBaseDir = $this->getConstant("LOCALStaticBaseDir"); //base directory of bucket
            $this->staticContentWebSitesDir = $this->getConstant("LOCALStaticWebsitesDir"); //base directory of websites
            $this->staticContentURL = $this->getCurrentHost(); //url of static content
        }
    }

    /**
     * get static content location
     *
     * @return string
     *
     */
    function getStaticContentLocation()
    {
        return $this->staticContentLocation;
    }

    /**
     * get AWS Bucket
     *
     * @return string
     *
     */
    function getAWSBucket()
    {
        return $this->AWSBucket;
    }

    /**
     * get AWS Bucket
     *
     * @return string
     *
     */
    function getAWSCredential()
    {
        return ["AWSAccessKey" => $this->AWSAccessKey, "AWSSecretKey" => $this->AWSSecretKey];
    }

    /**
     * get URL of static content
     *
     * @return string
     *
     */
    function getStaticContentURL()
    {
        return $this->staticContentURL . "/" . $this->staticContentBaseDir;
    }

    /**
     * get URL of static content of web site
     *
     * @param string $webCode web code
     *
     * @return string
     *
     */
    function getStaticContentURLWeb($webCode)
    {

        return $this->staticContentURL . "/" . $this->staticContentWebSitesDir . "/" . $webCode;
    }

    /**
     * get static content base dir
     *
     * @return string
     *
     */
    function getStaticContentBaseDir()
    {
        return $this->staticContentBaseDir;
    }

    /**
     * get static content web sites dir
     *
     * @param string $webCode web code
     * @return string
     *
     */
    function getStaticContentWebSitesDir($webCode)
    {
        return $this->staticContentWebSitesDir . "/" . $webCode;
    }


    /**
     * Return the List of objects type
     *
     * @param string $objectType code of object type you want get
     * @param string $pluginObject plugin object
     *
     * @return array
     *
     */
    function getListObjectType($objectType  = "", $pluginObject = "")
    {


        if ($pluginObject != ""){
            $objectTypeArr = $this->getObjectType($pluginObject);
            $objectType = $objectTypeArr["objectType"];
        }

        $oTypeArr =
            [
                "data\\tables" => "Object Data Table",
                "data\\queries" => "Object Data Query",
                "objects\\general" => "Object General",
                "web\\objects" => "Object Web",
                "web\\templates" => "Object Web Template",
                "manage\\panels" => "Object Panel Template"
            ];

        if ($objectType == "")
            return $oTypeArr;
        else
        {
            return array_intersect_key($oTypeArr,[$objectType => ""]);
        }
    }

    /**
     * Return the object description by name
     *
     * @param string $objectName Ex: object\general\category
     *
     * @return array
     *
     */
    function getObjectDescByName($objectName  = "")
    {

        $p = strrpos($objectName, "\\");

        $res = substr($objectName,0, $p);
        $res = str_replace("\\", " ", $res);
        $res = "[" . strtoupper($res) . "] => " . substr($objectName , $p +1);

        return $res;
    }

    /**
     * Return the dir by object type
     *
     * @param string $objectType code of object type: "d" for object data, "g" for object, "wo" for web object, "wt" for web template, "pt" for panel template
     * @param string $separator path separator, default "/"
     *
     * @return string with object folder name
     *
     */
    function getDirByObjectType($objectType = "objects\\general", $separator = "/")
    {
        $ObjectPath = "";
        switch ($objectType)
        {
            case "objects\\general":
            case"":
                $ObjectPath = "objects".$separator."general";
                break;
            case "data\\tables":
                $ObjectPath = "data".$separator."tables";
                break;
            case "data\\queries":
                $ObjectPath = "data".$separator."queries";
                break;
            case "web\\objects":
                $ObjectPath = "web".$separator."objects";
                break;
            case "web\\templates":
                $ObjectPath = "web".$separator."templates";
                break;
            case "manage\\panels":
                $ObjectPath = "manage".$separator."panels";
                break;

        }

        return $ObjectPath;
    }

    /**
     * Split a Class Name in array
     *
     * Ex of Plugin Class: k_sessions\objects\general\start\obj
     *
     * @param string $classNameComplete name of class with namespace - Ex: k_sessions\objects\general\start\obj
     *
     * @return array with object data
     *
     */
    function getObjectDataByClass($classNameComplete)
    {
        $f = explode("\\", $classNameComplete);

        //set variable
        $pluginName = "";
        $objectName = "";
        $objectNameSimple = "";
        $className = "";
        $objType = "";
        $res = [];

        if (count($f) == 5) { //Ex: k_sessions\objects\general\start\obj
            $pluginName = $f[0];
            $objectName = $f[1]."\\".$f[2]."\\".$f[3];
            $objectNameSimple = $f[3];
            $objType = $f[1]."\\".$f[2];
            $className = $f[4];

        }
        elseif (count($f) == 2) {
                $pluginName = $f[0];
                $objectName = "";
                $objectNameSimple = "";
                $className = $f[1];
                $objType = "";

            }


        $res["pluginName"] = $pluginName;
        $res["pluginObject"] = $objectName;
        $res["pluginObjectSimple"] = $objectNameSimple;
        $res["objType"] = $objType;
        $res["className"] = $className;

        return $res;
    }

    /**
     * Split a Auth Code in array
     *
     * Ex of Plugin Auth: k_common\manage\panels\p_parameters\auth
     *
     * @param string $authCode auth code
     *
     * @return array with object data
     *
     */

    function getAuthCodeData($authCode){
        $r = $this->getObjectDataByClass($authCode);

        $res = [];
        $res["pluginName"] = $r["pluginName"];
        $res["objType"] = $r["objType"];
        $res["code"] = $r["className"];


        return $res;
    }

    /**
     * Return the TABLE NAME on DB by Plugin Name and Data Object
     *
     * @param string $pluginName plugin name
     * @param string $dataObject plugin data object
     *
     * @return string
     *
     */
    function getTableNameByObject($pluginName, $dataObject)
    {

        return $pluginName .  "_" . str_replace("data\\tables\\","",$dataObject) ;
    }

    /**
     * Return the Simple TABLE NAME From Data Object
     * Ex: from data\tables\articles return "articles"
     *
     * @param string $dataObject plugin data object
     *
     * @return string
     *
     */
    function getSimpleTableNameByObject($dataObject)
    {

        return str_replace("data\\tables\\","",$dataObject) ;
    }

    /**
     * Return the Simple OBJECT NAME From object name
     * Ex: if mode = "simple" from objects\general\api return "api"
     * Ex: if mode = "flat" from objects\general\api return "objects_general_api"
     *
     * @param string $pluginObject plugin data object
     * @param string $mode "simle" get only name, "flat" replace "\\" with "_"
     *
     * @return string
     *
     */
    function getPluginObjectName($pluginObject, $mode = "simple")
    {
        $res = "";
        switch ($mode)
        {
            case "simple":
                $objInfo = $this->K_COMMON->getObjectType($pluginObject);
                $res =  $objInfo["pluginObjectName"];
                break;
            case "flat":
                $res = str_replace("\\","_", $pluginObject);
                break;
        }

        return $res;
    }

    /**
     * Return the PLUGIN NAME by Table Name
     *
     * @param string $tableName table name
     *
     * @return string
     *
     */
    function getPluginNameByTable($tableName)
    {


        preg_match_all('/_/', $tableName,$matches, PREG_OFFSET_CAPTURE);
        $pos = $matches[0][1][1];

        return substr($tableName ,0, $pos);
    }

    /**
     * Return the object type, by plugin object name by object type
     *
     * @param string $pluginObject plugin object
     *
     * @return array with object type data
     *
     */
    function getObjectType($pluginObject)
    {
        //set variable
        $res = [];

        //Check plugin object variable
        if ($pluginObject == "")
            return $res;

        //Get Type of Plugin Object
        $pluginObject = str_replace("/","\\", $pluginObject);
        $f = explode("\\", $pluginObject);

        if (count($f) == 1)
        {
            $res["objectType"] = "objects\\general";
            $res["pluginObjectName"] = $pluginObject;
            $res["pluginObjectWithType"] = $res["objectType"] . "\\" . $res["pluginObjectName"];
        }
        else
        { //Ex: objects\general\start

            $res["objectType"] = $f[0] . "\\" . $f[1];
            $res["pluginObjectName"] = $f[2];
            $res["pluginObjectWithType"] = $res["objectType"] . "\\" . $res["pluginObjectName"];


        }

        return $res;
    }

    /**
     * Return the path plugin or plugin object
     *
     * @param boolean $byPathAbsolute true if you want absolute path
     * @param string $pluginName name of plugin to get path (empty for actual)
     * @param string $pluginObject name of plugin object to get path (null for actual)
     *
     * @return string with directory 
     *
     */
    function getPathPluginObject($byPathAbsolute = false, $pluginName = "", $pluginObject = "")
    {
        
        //set variable
        $pathRelative = "";
        $ObjectPath = "";
        $pathAbsolute = "";

        //get Module Type
        $moduleType = $this->getPluginType($pluginName);

        //search type of plugin
        switch ($moduleType) {
            case "kcore":
                if ($byPathAbsolute)
                    $pathAbsolute = $this->pathKixixi;
                break;
            case "kplugin":
                if ($byPathAbsolute)
                    $pathAbsolute = $this->pathCstSystem;
                break;
            case "ktemplate":
                if ($byPathAbsolute)
                    $pathAbsolute = $this->pathCstSystem;
                break;
            case "kdata":
                if ($byPathAbsolute)
                    $pathAbsolute = $this->pathCstSystem;
                break;

        }

        //get object path if object setted
        if ($pluginObject != "")
        {
            $objectTypeArr = $this->getObjectType($pluginObject);
            $objectType = $objectTypeArr["objectType"];
            $pluginObject = $objectTypeArr["pluginObjectName"];
            $ObjectPath = $this->getDirByObjectType($objectType);
            $pluginObject = str_replace("\\","/", $pluginObject);
        }

        //return result
        return ($byPathAbsolute ? $pathAbsolute : $pathRelative) . "/" . $moduleType . "/" . $pluginName . ($pluginObject != "" ? "/" . $ObjectPath . "/" . $pluginObject : "");

    }


    /**
     * Include plugin or plugin object
     *
     * @param string $pluginName name of plugin to get path (empty for actual)
     * @param string $pluginObject name of plugin object to get path (null for actual)

     *
     * @return boolean
     */
    function loadPluginObject($pluginName, $pluginObject = "")
    {



        $root = $this->getPathPluginObject(true, $pluginName, $pluginObject);

        $fileName = $root . "/main.php";

         if (file_exists($fileName))
         {
             include_once($fileName);

         }
        else {
            $this->printError("Can't Load $pluginName " . ($pluginObject != "" ? " - " . $pluginObject : "") . " - OBJECT");
            return false;
        }

        return true;

    }


    /**
     * Instantiate and return plugin or plugin object
     *
     * @param string $pluginName name of plugin to get
     * @param string $pluginObject name of plugin object to get
     * @param array $av array parameters of plugin
     *
     * @return object
     */
    function getPluginObject($pluginName, $pluginObject = "", $av = array())
    {
        $objectTypeArr = $this->getObjectType($pluginObject);
        $pluginObject = $objectTypeArr["pluginObjectName"];
        $objectType = $objectTypeArr["objectType"];

        $ObjectPath = $this->getDirByObjectType($objectType,"\\");
        $pluginObject = str_replace("\\","/", $pluginObject);



        if ($pluginObject != "")
            $classObject = $pluginName . "\\" . $ObjectPath . "\\" . $pluginObject . "\\obj";
        else
            $classObject = "\\" . $pluginName . "\\obj";

        return  new \k_common\objects\general\interceptor\obj(new $classObject($av));



    }

    /**
 * Instantiate and return plugin object Panel
 *
 * @param string $pluginName name of plugin to get
 * @param string $pluginObject name of plugin object to get
 * @param array $av array parameters of plugin
 *
 * @return object
 */
    function getPluginObjectPanel($pluginName, $pluginObject = "", $av = array())
    {
        return $this->getPluginObject($pluginName, $pluginObject, $av);


    }

    /**
     * Instantiate and return plugin web object
     *
     * @param string $pluginName name of plugin to get
     * @param string $pluginObject name of plugin object to get
     * @param array $av array parameters of plugin
     *
     * @return object
     */
    function getPluginObjectWeb($pluginName, $pluginObject = "", $av = array())
    {
        return $this->getPluginObject($pluginName, $pluginObject, $av);


    }

    /**
     * get List Objects of Plugin
     *
     * @param string $pluginName name of plugin to get
     *
     * @return array
     */
    function getListPluginObject($pluginName)
    {
        $res = [];

        $root = $this->getPathPluginObject(true, $pluginName, "");
        $dir = $root."/objects";
        $handle = opendir($dir);
        while(false !== ( $file = readdir($handle)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($dir . '/' . $file) ) {
                    $res[] = $file;
                }

            }
        }
        closedir($handle);

        return $res;


    }

    /**
     * get List Data Tables of Plugin
     *
     * @param string $pluginName name of plugin to get
     *
     * @return array
     */
    function getListPluginDBTables($pluginName, $OM = "")
    {

        /**
         *
         * Name of table start with
         *
         * the "_" is necessary beacause otherwise may exist more that 1 plugin with same "start" name
         * for example may exist a plugin "kplugin_menu" and "kplugin_menuweb"
         *
         */
        $pluginNameTableStart = $pluginName . "\_";

        $res = $this->kxdb->loadTableList($pluginNameTableStart);

        if ($OM == "ARRAY"){
            $tablesList = [];
            while ($dataList = $res->fetch()) {
                $tablesList[] = $dataList->TABLE_NAME;
            }

            return $tablesList;
        }
        else
            return $res;





    }


    /**
     * get List Data Tables of Plugin
     *
     * @param string $pluginName name of plugin to get
     *
     * @return array
     */
    function getListPluginDBTablesWithCustomField($pluginName, $OM = "")
    {

        /**
         *
         * Name of table start with
         *
         * the "_" is necessary beacause otherwise may exist more that 1 plugin with same "start" name
         * for example may exist a plugin "kplugin_menu" and "kplugin_menuweb"
         *
         */
        $pluginNameTableStart = $pluginName . "\_";

        $res = $this->kxdb->loadTablesWithCustomFields($pluginNameTableStart);

        if ($OM == "ARRAY"){
            $tablesList = [];
            while ($dataList = $res->fetch()) {
                $tablesList[] = $dataList->TABLE_NAME;
            }

            return $tablesList;
        }
        else
            return $res;





    }

    /**
     * Instantiate and return data object of plugin
     *
     * @param string $pluginName name of plugin to get
     * @param string $objectData name of plugin object to get
     * @param array $av array parameters of plugin
     *
     * @return object data
     */
    function getPluginDataObject($pluginName, $objectData = "", $av = array())
    {


        $av["pluginName"] = $pluginName;
        $av["dataName"] = $this->getSimpleTableNameByObject($objectData);
        return new \k_dbmanager\objects\general\basetable\obj($av);

    }


    /**
     * print array with "pre" html tag
     *
     * @param array $array array to be printed
     *
     */
    function printArray($array)
    {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
    }

    /**
     * new object if not instantiated
     *
     * @param array $var actual object variable
     * @param array $obj object to instatiate
     *
     * @return object (new or old if exist)
     */
    function newOnce(&$var, $obj)
    {
        if (is_object($var))
            return $var;
        else
            return new $obj;
    }

    /**
     * get json success true or false
     *
     * @param boolean $res authorization code
     * @param array $other array to merge with json result
     *
     * @return json
     */
    function jsonResult($res, $other = [])
    {
        if ($res)
            return json_encode(array_merge(array("success" => "true"), $other));
        else
            return json_encode(array_merge(array("success" => "false"), $other));
    }

    /**
     * get a success true or false result converted to JSON/XML/ARRAY
     *
     * @param boolean $om Output Model : JSON, XML, ARRAY
     * @param boolean $res result type (true or false)
     * @param array $other array to merge with result
     *
     * @return json
     */
    function resultReturn($om, $res, $other = [])
    {
        switch ($om){
            case "JSON":
                return $this->jsonResult($res, $other);
                break;
            case "ARRAY":
                return array_merge(array("success" => ($res == true ? "true" : "false")), $other);
                break;
            case "XML":
                return $this->dbDataToXML(["array" => array(array_merge(array("success" => ($res == true ? "true" : "false")), $other))]);
                break;
            case "HTML":
                $h = "<br><br>";
                foreach ($other as $k => $v)
                {
                    $h .= "<strong>" . $k . "</strong>:" . $v . "<br>";
                }

                return $h;
                break;
            default:
                return $res;
                break;
        }

    }

    /**
     * Return the path of temporary directory
     *
     * @return string with directory
     *
     */
    function getPathTmp()
    {

        return $this->pathTmp;
    }

    /**
     * Return the path of SDK directory (kmore)
     *
     * @param array $arrayVariables
     * The following directives can be used in the array variables:<br>
     *    *    name: name of sdk (Ex: "aws")
     *
     * @return string with directory
     *
     */
    function getPathSDK($arrayVariables)
    {
        $name = $this->getVarArray($arrayVariables, "name");

        return $_SERVER['DOCUMENT_ROOT'] . "/kmore/sdk/" . $name;
    }

    /**
     * Get Language of the user specified
     *
     * @param string $anaCode ana code of user to get language
     *
     * @return string with language code.
     */
    function getUserLanguage($anaCode = "")
    {
        //check variables
        if ($anaCode == "")
            $anaCode = $this->getCurrentUser();

        $this->APIRegistry = $this->newOnce($this->APIRegistry, '\k_registry\objects\general\api\obj');
        return $this->APIRegistry->getUserLanguage($anaCode);
    }

    /**
     * Get Language of the user specified
     *
     * @param string $anaCode ana code of user to get language
     *
     * @return array with name ["firstname", "lastname"]
     */
    function getRegistryName($anaCode = "")
    {
        //check variables
        if ($anaCode == "")
            $anaCode = $this->getCurrentUser();

        $this->APIRegistry = $this->newOnce($this->APIRegistry, '\k_registry\objects\general\api\obj');
        return $this->APIRegistry->getRegistryName($anaCode);
    }

    /**
     * Set Language of the user specified
     *
     * @param string $ana_code ana_code of user to set language
     * @param string $language language code to set
     *
     */
    function setUserLanguage($ana_code, $language)
    {
        $language = strtoupper($language);
        $table = $this->getPluginDataObject("k_registry", "registry");
        $table->put(["ana_language" => $language], ["ana_code" => $ana_code]);

    }

    /**
     * Get plugin Type
     *
     * @param string $pluginName
     *
     * @return string
     *
     */
    function getPluginType($pluginName)
    {

        //search type of plugin
        if (substr($pluginName, 0, 2) == "k_")
            return "kcore";
        elseif (substr($pluginName, 0, 8) == "kplugin_")
            return "kplugin";
        elseif (substr($pluginName, 0, 10) == "ktemplate_")
            return "ktemplate";
        elseif (substr($pluginName, 0, 6) == "kdata_")
            return "kdata";

    }

    /**
     * Get plugin Root by Plugin Type
     *
     * @param string $pluginType
     *
     * @return string
     *
     */
    function getPluginRootByType($pluginType)
    {
        $res = "";

        //search type of plugin
        switch ($pluginType) {
            case "kcore":
                $res = "k_";
                break;
            case "kplugin":
                $res = "kplugin_";
                break;
            case "ktemplate":
                $res = "ktemplate_";
                break;
            case "kdata":
                $res = "kdata_";
                break;

        }

        return $res;

    }


    /**
     * get Modal Ask Confirm
     *
     * @param array $ArrayVariable array to merge with result
     * The following directives can be used in the array variables:<br>
     *    *    idModal: id of modal
     *    *    type: "general" / "delete"
     *    *    body: body of modal for "general" type modal
     *
     * @return array ["success" => "true", "html" => html Of Modal]
     */
    function getModalAskConfirm($ArrayVariable)
    {

        //include image api
        $api = $this->getPluginObject("k_common","api");

        //GET html Modal
        return $api->getModalAskConfirm($ArrayVariable);




    }

    /**
     * get new Modal
     *
     * @param array $ArrayVariable array to merge with result
     * The following directives can be used in the array variables:<br>
     *    *    idModal: id of modal
     *    *    type: "general" / "delete"
     *    *    body: body of modal for "general" type modal
     *
     * @return array ["success" => "true", "html" => html Of Modal]
     */
    function getNewModal($ArrayVariable)
    {

        //include image api
        $api = $this->getPluginObject("k_common","api");

        //GET html Modal
        return $api->getNewModal($ArrayVariable);




    }

    /**
     * get dictionary of plugin
     *
     * @param string $pluginName name of plugin to get (empty for actual)
     * @param string $pluginObject name of plugin object to get (if $pluginName == "" get actual)
     * @param string optional $word word code to load
     * @param string $webCode optional webCode to get (only for dictionary installed on web)
     * @param string $languageCode optional Language Code
     * @return array
     */
    function getPluginDictionary($pluginName = "", $pluginObject = "", $word = "", $webCode = "", $languageCode = "")
    {
        $this->objDictionary = $this->newOnce($this->objDictionary, '\k_dictionary\objects\general\dictionary\obj');

        if ($languageCode  == "")
            $languageCode = $this->language;

        return $this->objDictionary->get(
            [
                "word" => $word
                , "language" => $languageCode
                , "pluginName" => $pluginName
                , "pluginObject" => $pluginObject
                , "webCode" => $webCode
        ]);

    }


    /**
     * create URL for API Call
     *
     * @param string $wcall type of call (see kapi)
     * @param string $kcp plugin name
     * @param string $kco plugin object name
     * @param string $kca action of plugin
     * @param string $param optional parameter for URL
     * @return array
     */
    function buildAPICallUrl($wcall, $kcp, $kco, $kca, $param = "")
    {
        //set variables
        $KCTime = date("His");
        $KCDate = date("Ymd");

        if ($param != "")
            $param = "&" . $param;


        return $this->getAPIURLCall() . "?KXTCALL=" . $wcall . "&KCP=" . $kcp . "&KCO=" . $kco . "&KCA=" . $kca . "&KCDate=" . $KCDate . "&KCTime=" . $KCTime . $param;
    }

    /**
     * Print Error
     *
     * @since 1.00
     *
     */
    public function restartSession()
    {

        $K_SESSIONS_START = new \k_sessions\objects\general\initiate\obj;
        $K_SESSIONS_START->initiate();

    }

    /**
     * Print Error
     *
     * @since 1.00
     *
     */
    public function printError($textError)
    {

        echo '<blockquote>';
        echo '[<b>' . $textError . '</b></font>]<p>';
        echo '</blockquote><hr noshade color=dddddd size=1>';

    }

}

?>