<?
/**
 * KIXIXI BASE OBJECT
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 * @author Andrea Orsini <orsiniandrea@hotmail.it>
 *
 * @package kcore\k_common
 * @subpackage objects\base
 * @since 1.00
 */
namespace k_common\objects\general\base;
class obj extends \ReflectionClass
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * language of user
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $language = "";

    /**
     * location of user
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $location = "";

    /**
     * link to K_COMMON
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $K_COMMON;

    /**
     * link to K_SECAUTH
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $K_SECAUTH;

    /**
     * link to K_CUSTOMMANAGER
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $K_CUSTOMMANAGER;

    /**
     * Crumbs of this page
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $pageCrumbs = [];

    /**
     * link to kixixi DB
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $kxdb;

    /**
     * name of actual plugin
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $namePlugin = "";

    /**
     * name of actual plugin object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $namePluginObject = "";

    /**
     * type or plugin object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $typePluginObject = "";



    /**
     * Array of customization fot this object
     *
     * @since 1.00
     * @access public
     */
    var $customArray;

    /**
     * Auth Plugin - Plugin ID that authorize
     * If empty get actual plugin
     *
     * @since 1.00
     * @access public
     */
    var $authPlugin;

    /**
     * Auth Code - code of authorization
     *
     * @since 1.00
     * @access public
     */
    var $authCode;

    /**
     * Is User Authorized
     * false: user Not Authorized
     * true: user Authorized (is true also if not auth code specified)
     *
     * @since 1.00
     * @access public
     */
    var $isUserAuthorized = true;


    /**
     * Construct
     *
     * @param array $arrayVariable optional array of variables for query statement
     * The following directives can be used in the array variables:<br>
     *    *    pluginName: name of plugin table
     *    *    pluginObject: name of object name of table
     *    *    objType: type of object
     *
     *
     */
    function __construct($arrayVariable = [])
    {

        //call parent
        parent::__construct($this);

        //global variables
        global $K_COMMON;
        global $K_SECAUTH;
        global $K_CUSTOMMANAGER;
        global $kxDBManager;
        global $pageCrumbs;

        //set variables
        $this->K_COMMON = &$K_COMMON;
        $this->K_SECAUTH = $K_SECAUTH;
        $this->K_CUSTOMMANAGER = &$K_CUSTOMMANAGER;
        $this->pageCrumbs = &$pageCrumbs;
        $this->kxdb = &$kxDBManager;
        $this->language = $this->K_COMMON->language;
        $this->location = $this->K_COMMON->location;
        $pluginName = $this->K_COMMON->getVarArray($arrayVariable, "pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($arrayVariable, "pluginObject");
        $objType = $this->K_COMMON->getVarArray($arrayVariable, "objType");

        //get name of plugin
        if ($pluginName == "")
        {
            $dataObject = $this->K_COMMON->getObjectDataByClass(get_class($this));
            $pluginName = $dataObject["pluginName"];
            $pluginObject = $dataObject["pluginObject"];
            $objType = $dataObject["objType"];
        }


        $this->namePlugin = $pluginName;
        $this->namePluginObject = $pluginObject;
        $this->typePluginObject = $objType;

        //Check Authorization
        if ($this->authCode != "")
        {
            if ($this->authPlugin != "")
            {
                if (!$this->isAuthorized($this->authCode,$this->authPlugin))
                    $this->isUserAuthorized = false;
            }
            else
            {
                if (!$this->isAuthorizedPlugin($this->authCode))
                    $this->isUserAuthorized = false;
            }
        }

    }

    /**
     * This Function execute web service method on object, after checked authorization
     *
     * @param $r array of request
     *
     * @return object result
     */
    function manageWebService($r){

        if (!$this->isUserAuthorized)
        {
            $OM = $this->K_COMMON->getVarArray($r, "OM");
            return $this->K_COMMON->resultReturn($OM, false, ["reason" => "errorSecurity", "message" => "Error Security"]);
        }
        else return $this->objectRequest($r);

    }

    /**
     * This Function Execute a method of this object and search customization
     *
     * @return string|object|array function called value
     */
    function executeMethod($method, $arguments)
    {


      //  echo "<br> <b>==============>>>>> START EXECUTE METHOD   ".$this->getPluginName()." - " . $this->getPluginObjectName() . " --> $method *** </b><br>";
/*

        $argumentsTmp = $arguments;
        if (isset($argumentsTmp[0]["customization"]))
            $argumentsTmp[0]["customization"] = "";
        $argumentsTmp["customization"] = [];
        $argumentsTmp["objToCustomize"] = [];
        $argumentsTmp["objToCustomizeRef"] = [];
        $this->K_COMMON->printArray($argumentsTmp);


        echo "<br> **** CUSTOM BEFORE  ".$this->getPluginName()." - " . $this->getPluginObjectName() . " --> $method *** <br>";
*/

        //customize object (BEFORE)
        if (isset($this->K_CUSTOMMANAGER)) {
            $resBefore = $this->K_CUSTOMMANAGER->customizeObject(
                [
                    "targetType" => "function"
                    , "targetWhen" => "before"
                    , "targetPlugin" => $this->getPluginName()
                    , "targetObject" => $this->getPluginObjectName()
                    , "targetCode" => $method
                    , "parameters" => $arguments
                ]
                , null
                , $this
            );

            if ($resBefore != null)
            {

                //echo "<br> TORNATO OK ".$this->getPluginName()." - " . $this->getPluginObjectName() . " --> $method *** <br>";
                $arguments = [];
                $arguments[0] = $resBefore;



            }



        }

        /*
        //TEMP ECHO
        $argumentsTmp = $arguments;
        if (isset($argumentsTmp[0]["customization"]))
            $argumentsTmp[0]["customization"] = "";
        $argumentsTmp["customization"] = [];
        $argumentsTmp["objToCustomize"] = [];
        $argumentsTmp["objToCustomizeRef"] = [];
        $this->K_COMMON->printArray($argumentsTmp);
        echo "<br> **** END CUSTOM BEFORE ".$this->getPluginName()." - " . $this->getPluginObjectName() . " --> $method *** <br>";

*/

        //execute CORE function

        //customize object (REPLACE)
        if (isset($this->K_CUSTOMMANAGER)) {
            $resReplace = $this->K_CUSTOMMANAGER->customizeObject(
                [
                    "targetType" => "function"
                    , "targetWhen" => "replace"
                    , "targetPlugin" => $this->getPluginName()
                    , "targetObject" => $this->getPluginObjectName()
                    , "targetCode" => $method
                    , "parameters" => $arguments
                ]
                , null
                , $this
            );

            if ($resReplace != null)
            {
                $res = $resReplace;
            }
            else //no customization found
            {
                
                $res = call_user_func_array(array($this, $method), $arguments);
            }
        }
        else
            $res = call_user_func_array(array($this, $method), $arguments);






       //echo "<br> **** START CUSTOM AFTER ".$this->getPluginName()." - " . $this->getPluginObjectName() . " --> $method *** <br>";

        //customize object (AFTER)
        if (isset($this->K_CUSTOMMANAGER)) {
            $resAfter = $this->K_CUSTOMMANAGER->customizeObject(
                [
                    "targetType" => "function"
                    , "targetWhen" => "after"
                    , "targetPlugin" => $this->getPluginName()
                    , "targetObject" => $this->getPluginObjectName()
                    , "targetCode" => $method
                    , "parameters" => $arguments
                ]
                , $res
                , $this
            );

            if ($resAfter != null)
                $res = $resAfter;
        }


        //echo "<br> **** END CUSTOM AFTER ".$this->getPluginName()." - " . $this->getPluginObjectName() . " --> $method *** <br>";

        //echo "<br> <b>==============>>>>> END EXECUTE METHOD  ".$this->getPluginName()." - " . $this->getPluginObjectName() . " --> $method *** </b> <br>";

        return $res;
    }


    /**
     * Get current Plugin Name
     *
     * @return string with user code.
     */
    function getPluginName()
    {
        return $this->namePlugin;
    }

    /**
     * Get current Plugin Object Name
     *
     * @return string with user code.
     */
    function getPluginObjectName()
    {
        return $this->namePluginObject;
    }

    /**
     * Return my object path
     *
     * @param boolean $byPathAbsolute true if you want absolute path
     *
     * @return string with directory
     *
     */
    function getMyObjectPath($byPathAbsolute = false)
    {
        $pluginName = $this->namePlugin;
        $pluginObject = (isset($this->namePluginObject) ? $this->namePluginObject : "");

        return $this->K_COMMON->getPathPluginObject($byPathAbsolute, $pluginName, $pluginObject, $this->oType);
    }

    /**
     * Return the root path of my plugin
     *
     * @param boolean $byPathAbsolute true if you want absolute path
     *
     * @return string with directory
     *
     */
    function getMyPluginPath($byPathAbsolute = false)
    {
        $pluginName = $this->namePlugin;
        $pluginObject = "";

        return $this->K_COMMON->getPathPluginObject($byPathAbsolute, $pluginName, $pluginObject, $this->oType);
    }

    /**
     * Return the complete path of ".tpl"
     *
     * @param string $layName name of layout
     * @param string $pluginName name of plugin to get path (empty for actual)
     * @param string $pluginObject name of plugin object to get path (null for actual)
     *
     * @return string with directory (with file name)
     *
     */
    function getPathFileTPL($layName, $pluginName = "", $pluginObject = null)
    {
        if ($pluginName == "")
            $pluginName = $this->namePlugin;
        if ($pluginObject === null)
        {
            $pluginObject = (isset($this->namePluginObject) ? $this->namePluginObject : "");


        }

        return $this->K_COMMON->getPathPluginObject(true, $pluginName, $pluginObject) . "/" . "templatefile/" . $layName . "/" . $layName . ".tpl";
    }

    /**
     * Return the complete path of ".js"
     *
     * @param string $layName name of layout
     * @param string $pluginName name of plugin to get path (empty for actual)
     * @param string $pluginObject name of plugin object to get path (null for actual)
     *
     * @return string with directory (with file name)
     *
     */
    function getPathFileJS($layName, $pluginName = "", $pluginObject = null)
    {

        if ($pluginName == "")
            $pluginName = $this->namePlugin;
        if ($pluginObject === null)
            $pluginObject = (isset($this->namePluginObject) ? $this->namePluginObject : "");

        return $this->K_COMMON->getPathPluginObject(false, $pluginName, $pluginObject, $this->oType) . "/" . "templatefile/" . $layName . "/" . $layName . ".js";

    }

    /**
     * Return the complete path of ".css"
     *
     * @param string $layName name of layout
     * @param string $pluginName name of plugin to get path (empty for actual)
     * @param string $pluginObject name of plugin object to get path (null for actual)
     *
     * @return string with directory (with file name)
     *
     */
    function getPathFileCSS($layName, $pluginName = "", $pluginObject = null)
    {

        if ($pluginName == "")
            $pluginName = $this->namePlugin;
        if ($pluginObject === null)
            $pluginObject = (isset($this->namePluginObject) ? $this->namePluginObject : "");

        return $this->K_COMMON->getPathPluginObject(false, $pluginName, $pluginObject, $this->oType) . "/" . "templatefile/" . $layName . "/" . $layName . ".css";

    }


    /**
     * Instantiate and return  object of plugin
     *
     * @param string $pluginObject name of plugin object to get path (null for actual)
     * @param array $av array parameters of plugin
     *
     * @return object
     */
    function getObject($pluginObject = "", $av = array())
    {
        $pluginName = $this->namePlugin;

        return $this->K_COMMON->getPluginObject($pluginName, $pluginObject, $av);

    }

    /**
     * Instantiate and return panel manage object of plugin
     *
     * @param string $pluginObject name of plugin object to get path (null for actual)
     * @param array $av array parameters of plugin
     *
     * @return object
     */
    function getObjectPanel($pluginObject = "", $av = array())
    {
        $pluginName = $this->namePlugin;

        return $this->K_COMMON->getPluginObject($pluginName, "manage\\panels\\" . $pluginObject, $av);

    }

    /**
     * Instantiate and return data object of this plugin
     *
     * @param string $objectData name of "data" object to get (if null instantiate "basetable" object)
     * @param array $av array parameters of plugin
     *
     * @return object data
     */
    function getTable($objectData = "", $av = array())
    {
        
        $pluginName = $this->namePlugin;

        if ($objectData == "")
            return new \k_dbmanager\objects\general\basetable\obj(
                [
                    "pluginName" => ""
                    ,"dataName" => ""
                    ,"objType" => "tables"
                ]
            );
        else
            return $this->K_COMMON->getPluginDataObject($pluginName, $objectData, $av);
    }

    /**
     * get parameter from table parameters
     *
     * @param string $paramCode parameter code
     * @param string $paramWebCode parameter custom code (if = "" use the website Code)
     * @param string $pluginName name of plugin to get (empty for actual)
     *
     * @return string
     */
    function getParamValue($paramCode, $paramWebCode = "", $pluginName = "")
    {


        if ($pluginName == "")
        {
            $pluginName = $this->namePlugin;

        }

        if ($paramWebCode == "")
            $paramWebCode = $this->K_COMMON->getCurrentWeb();

        //load Table
        $table = $this->K_COMMON->getPluginDataObject("k_common", "parameters");


        //load data
        $table->call("parameters_get_parameter", "'" . $pluginName . "','" . $paramCode . "','" . $paramWebCode . "'");
        $data = $table->fetch();
        if ($data)
            return $data->returnvalue;
        else
            return "";


    }

    /**
     * set parameter in table parameters
     *
     * @param string $paramCode parameter code
     * @param string $paramWebCode parameter web code (if = "" use the website Code)
     * @param string $pluginName name of plugin to get (empty for actual)
     * @param string $pluginObject name of plugin object to get (null for actual)
     * @param string $paramValue param value
     *
     * @return string
     */
    function setParamValue($paramCode, $paramWebCode = "", $paramValue, $pluginName = "")
    {


        if ($pluginName == "")
            $pluginName = $this->namePlugin;

        if ($paramWebCode == "")
            $paramWebCode = $this->K_COMMON->getCurrentWeb();

        //load Table
        $table = $this->K_COMMON->getPluginDataObject("k_common", "parameters");

        //load data
        $table->call("parameters_set_parameter", "'" . $pluginName . "','" . $paramCode . "','" . $paramWebCode . "','" . $paramValue . "'","procedure");
        $data = $table->fetch();
        if ($data)
            return $data->returnvalue;
        else
            return "";


    }

    /**
     * get Panel Manage UURL
     *
     * @return string
     */
    function getPanelManageURL()
    {

        $defaultPanelUrl = $this->getParamValue("panelURL","","k_panelmanage");
        $defaultPanelUrl = str_replace("/*","",$defaultPanelUrl);
        return $defaultPanelUrl;


    }

    /**
     * get Panel Manage UURL
     *
     * @return string
     */
    function getLoginURL()
    {

        return $this->getParamValue("loginURL","","k_webmanager");


    }

    /**
     * get dictionary of plugin
     *
     * @param string $pluginName name of plugin to get (empty for actual)
     * @param string $pluginObject name of plugin object to get (if $pluginName == "" get actual)
     *
     * @return array
     */
    function getDictionary($pluginName = "", $pluginObject = "", $webCode = "")
    {
        if ($pluginName == "") {
            $pluginName = $this->namePlugin;
            $pluginObject = $this->namePluginObject;
        }
        return $this->K_COMMON->getPluginDictionary($pluginName, $pluginObject, "", $webCode);
    }

    /**
     * get dictionary word of plugin
     *
     * @param string $word word to get
     * @param string $pluginName name of plugin to get (empty for actual)
     * @param string $pluginObject name of plugin object to get (if $pluginName == "" get actual)
     * @param string $languageCode optional Language Code
     *
     * @return array
     */
    function getDictionaryWord($word = "", $pluginName = "", $pluginObject = "", $webCode = "", $languageCode = "")
    {
        if ($pluginName == "") {
            $pluginName = $this->namePlugin;
            $pluginObject = $this->namePluginObject;
        }

        return $this->K_COMMON->getPluginDictionary($pluginName, $pluginObject, $word, $webCode, $languageCode);
    }


    /**
     * get dictionary word of plugin (alias of getDictionaryWord)
     *
     * @param string $word word to get
     * @param string $pluginName name of plugin to get (empty for actual)
     * @param string $pluginObject name of plugin object to get (if $pluginName == "" get actual)
     *
     * @return array
     */
    function getWord($word = "", $pluginName = "", $pluginObject = "")
    {
        return $this->getDictionaryWord($word,$pluginName,$pluginObject);
    }

    /**
     * convert label and get value from dictionary
     *
     * @param array $label (ex: ["dictionary" => "labelName"])
     * @param string $dictionary dictionary array
     *
     * @return string
     */
    function convertLabelDictionary($label, $dictionary)
    {
        if (is_array($label))
            return $this->K_COMMON->getVarArray($dictionary, $label["dictionary"]);
        else
            return $label;
    }

    /**
     * check if user is a Developer
     *
     *
     * @return boolean
     */
    function isDeveloper()
    {
        //check authorization
        if (!$this->isAuthorized("authDev", "k_common"))
            return false;
        else
            return true;
    }

    /**
     * check if user authorized to modify Web Site Layout
     *
     * @param string $webCode default actual web code
     *
     * @return string
     */
    function isAuthorizedWebSiteLayout($webCode = "")
    {
        if ($webCode == "")
            $webCode = $this->K_COMMON->getEditingWeb();

        if (!$this->isAuthorized("authModLayWS", "k_webmanager",  $webCode))
            return false;
        else
            return true;



    }

    /**
     * check if user authorized to modify Web Site Content (EX: Articles etc)
     *
     * @param string $webCode default actual Edit web code
     *
     * @return string
     */
    function isAuthorizedWebSiteContent($webCode = "")
    {
        if ($webCode == "")
            $webCode = $this->K_COMMON->getEditingWeb();

        if (!$this->isAuthorized("authModCntWS", "k_webmanager",  $webCode))
            return false;
        else
            return true;



    }

    /**
     * check if user authorized
     *
     * @param string $authCode authorization code
     * @param string $pluginName name of plugin to get (empty for actual)
     * @param string $webCodeTarget code of web code target of auth
     * @param string $customField optional code of custom Field
     *
     * @return string
     */
    function isAuthorized($authCode, $pluginName = "", $webCodeTarget = "", $customField = "")
    {
        if ($pluginName == "")
            $pluginName = $this->namePlugin;

        return $this->K_SECAUTH->checkAuthorization($authCode, $pluginName, $webCodeTarget, $customField);
    }

    /**
     * check if user authorized from this plugin (general plugin authorization)
     *
     * @param string $authCode authorization code
     * @param string $webCodeTarget code of web code target of auth
     * @param string $customField optional code of custom Field
     *
     * @return string
     */
    function isAuthorizedPlugin($authCode, $webCodeTarget = "", $customField = "")
    {
       $pluginName = $this->namePlugin;


        return $this->K_SECAUTH->checkAuthorization($authCode, $pluginName, $webCodeTarget, $customField);
    }



    /**
     * check if user authorized from array
     *
     * @param string $auth array of authorization
     * The following parameters are accepted:<br>
     *	*	pluginName (optional): name of plugin to get (empty for actual)
     *	*	authCode : authorization code
     *  *   customField: optional code of custom Field
     *
     * @return string
     */
    function isAuthorizedCheck($auth)
    {
        $pluginName = $this->K_COMMON->getVarArray($auth, "pluginName");
        $customField = $this->K_COMMON->getVarArray($auth, "customField");

        if ($pluginName == "")
            $pluginName = $this->namePlugin;

        $authCode = $this->K_COMMON->getVarArray($auth, "authCode");

        return $this->K_SECAUTH->checkAuthorization($authCode, $pluginName, "", $customField);
    }

    /**
     * Instantiate and return template layout object (for manage .tpl files)
     *
     * @param string $name name of template to get
     *
     * @return object layout
     */
    function getTemplateLayout($name, $pluginName = "", $pluginObject = null)
    {

        $template = new \k_webmanager\objects\general\web_layout\obj;
        $file = $this->getPathFileTPL($name, $pluginName, $pluginObject);
        $template->set_filenames(array("body" => $file));
        return $template;
    }


}

?>