<?
namespace k_common\objects\general\webservice;
class obj extends \k_common\objects\general\base\obj
{
    var $dictionary;
    var $ver = "1.0";
    var $curAPIVer = "1.0";
    var $curAPPVer = "1.0";

    public function __construct($AV)
    {
        parent::__construct();

    }


    /**
     * Manage Actions from ajax
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($arrayVariable)
    {
        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");
        $APIVER = $this->K_COMMON->getVarRequest("APIVER");
        $APPVER = $this->K_COMMON->getVarRequest("APPVER");

        //check version
        if ($APIVER != "") {
            $APIVER = (float)$APIVER;

            if ($APIVER < $this->curAPIVer)
                return json_encode(["success" => "false", "reason" => "apiver"]);


            if ($APPVER != "") {
                $APPVER = (float)$APPVER;

                if ($APPVER < $this->curAPPVer)
                    return json_encode(["success" => "false", "reason" => "appver"]);
            }

        }


        //switch request
        switch ($KCA) {
            case "askConfirm":
                $obj = $this->K_COMMON->getModalAskConfirm($arrayVariable);
                return json_encode($obj);
                break;

        }
    }




}

?>