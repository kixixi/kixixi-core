<?
/**
 * KIXIXI AUTOLOADER
 * This object load file of called object
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_common
 * @subpackage objects\autoloader
 * @since 1.00
 */
namespace k_common\objects\general\autoloader;
class obj extends \k_common\objects\general\base\obj
{

    /**
     * Construct
     *
     */
    public function __construct() {

        //call parent
        parent::__construct();

        //register autoload function
        spl_autoload_register(array($this, 'loader'));

    }

    /**
     * load file of class
     *
     * @param string $className name of class to load (Ex: "k_sessions\objects\general\start\obj")
     *
     *
     */
    private function loader($className) {



        if (substr($className,0,2) == "k_" || substr($className,0,8) == "kplugin_" || substr($className,0,10) == "ktemplate_") {


            $dataObject = $this->K_COMMON->getObjectDataByClass($className);
            $pluginName = $dataObject["pluginName"];
            $objectName = $dataObject["pluginObject"];

            $this->K_COMMON->loadPluginObject($pluginName, $objectName);



        }
        
    }

}

?>