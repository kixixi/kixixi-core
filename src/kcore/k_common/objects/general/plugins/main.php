<?
namespace k_common\objects\general\plugins;
use function GuzzleHttp\Psr7\str;

class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Type of object of a plugin for kcore, kplugin, ktemplate(no kdata)
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $pluginObjType = [

        //DB Definition
        "table",
        "trigger",
        "sp",
        "event",

        //PLUGIN DEFINITION
        "parameters",
        "authorization",
        "dictionary",
        "messages",
        "actions",
        "panel",
        "custom",
        "customfields",
        "messages",
        "actions",


        //OBJECT DEFINITION (GENERAL)
        //"dictionary",

        //OBJECT DEFINITION (MANAGE\PANEL)
        //"dictionary",
        "paneltemplate",

        //OBJECT DEFINITION (WEB\OBJECTS)
        //"dictionary",
        "webobject",

        //OBJECT DEFINITION (WEB\TEMPLATES)
        //"dictionary",
        "webtemplate",
        "webpage"





    ];


    public function __construct()
    {

        /**
         *
         * In this construct we not use Dictionary, because if we are installing the tables no t exist
         *
         */

        parent::__construct();



    }


    /**
     * list plugins
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    OM: Output model ("array", "xml")
     *    *    KCore: if 1 get KCore
     *    *    KPlugin: if 1 get KPlugin
     *    *    KTemplate: if 1 get KTemplate
     *
     * @return array|xml
     */
    function listPlugins($arrayVariable = [])
    {

        //set Variables
        $OM = $this->K_COMMON->getVarArray($arrayVariable,"OM");
        $KCore = $this->K_COMMON->getVarArray($arrayVariable,"KCore");
        $KPlugin = $this->K_COMMON->getVarArray($arrayVariable,"KPlugin");
        $KTemplate = $this->K_COMMON->getVarArray($arrayVariable,"KTemplate");
        $KData = $this->K_COMMON->getVarArray($arrayVariable,"KData");
        $arrayFile  = [];
        $arrayWhere = [];
        $arrayBind = [];
        $pluginType = [];

        //Select plugin Type
        if ($KCore == "1")
            $pluginType[] = "kcore";
        if ($KPlugin == "1")
            $pluginType[] = "kplugin";
        if ($KTemplate == "1")
            $pluginType[] = "ktemplate";
        if ($KData == "1")
            $pluginType[] = "kdata";

        if (!empty($pluginType))
        {
            $arrayWhere[] = "plg_type IN (:plg_type)";
            $arrayBind["plg_type"] = $pluginType;
        }
        
        //get Table Data
        $table = $this->getTable("plugins");
        $table->get("", $arrayWhere, $arrayBind, [], ["plg_type","plg_name"]);
        while ($data = $table->fetch())
        {
            $arrayFile[] =
                [
                    "type" => $data->plg_type,
                    "value" => $data->plg_id,
                    "text" => strtoupper($data->plg_type) . "=>" . $data->plg_name
                ];
        }


        if (strtoupper($OM) == "XML")
            return $this->K_COMMON->dbDataToXML(["array" => $arrayFile]);
        else
            return $arrayFile;
    }

    /**
     * get Info of plugin
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    keyID: keyID of record
     *    *    plgID: plugin id (EX: kplugin_mailinglist)
     *
     * @return array|xml
     */
    function getPluginInfo($arrayVariable = [])
    {

        //set Variables
        $keyID = $this->K_COMMON->getVarArray($arrayVariable,"keyID");
        $plgID = $this->K_COMMON->getVarArray($arrayVariable,"plgID");
        $arrayWhere = [];
        $arrayInfo = [];

        if ($keyID != "")
            $arrayWhere["id_plugin"] = $keyID;
        else
            $arrayWhere["plg_id"] = $plgID;


        //get Table Data
        $table = $this->getTable("plugins");
        $table->get("", $arrayWhere, [], [], []);

        $arrResult = $table->getResults("array");
        $arrResult = $arrResult[0];

        $p = strpos($arrResult["plg_id"],"_");
        $plgIDRoot = substr($plgID, 0,$p +1);
        $plgIDName = substr($plgID, $p + 1);

        $arrResult["plg_id_root"] = $plgIDRoot;
        $arrResult["plg_id_name"] = $plgIDName;

        return $arrResult;

    }

    /**
     * get Info of plugin
     *
     * @param string $plgID id of plugin
     *
     * @return string
     */
    function getPluginDisplayName($plgID)
    {

        //set Variables
        $arrayWhere = [];
        $arrayWhere["plg_id"] = $plgID;

        //get Table Data
        $table = $this->getTable("plugins");
        $table->get("", $arrayWhere, [], [], []);
        if ($data = $table->fetch())
            return $data->plg_name;
        else
            return "";

    }

    /**
     * Remove plugin
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    plgID: plugin id (EX: kplugin_mailinglist)
     *
     * @return boolean
     */
    function deletePlugin($arrayVariable = [])
    {

        //set Variables
        $plgID = $this->K_COMMON->getVarArray($arrayVariable,"plgID");

        $arrayWhere["plg_id"] = $plgID;

        //get Table Data
        $table = $this->getTable("plugins");
        $table->delete($arrayWhere);

        //Remove Plugin Directory
        $pathPlugin = $this->K_COMMON->getPathPluginObject(true, $plgID, "");

        $this->K_COMMON->emptyDir(["dir" => $pathPlugin]);
        $this->K_COMMON->rmDir(["dir" => $pathPlugin]);

        return true;

    }


    /**
     * Import plugin
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    plgID: plugin id (EX: kplugin_mailinglist)
     *
     * @return boolean
     */
    function importPlugin($arrayVariable = [])
    {

        //set variables
        $pluginName = $this->K_COMMON->getVarArray($arrayVariable, "plgID");
        $pluginObject = $this->K_COMMON->getVarArray($arrayVariable, "pluginObject");
        $typeUpdate = $this->K_COMMON->getVarArray($arrayVariable, "typeUpdate");
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");
        $firstInstall = $this->K_COMMON->getVarArray($arrayVariable, "firstInstall");
        $pluginName = strtolower($pluginName);
        $errorMsg = "";
        $successMsg = "";
        $success = true;
        $moduleType = "";
        $importPlugin = false;


        //import config XML

        if ($firstInstall != "1")
        {
            $Res = $this->importConfigXML(["pluginName" => $pluginName]);

            if ($Res["success"] == "false") {
                $errorMsg = "$pluginName: File config.xml not found!";
            }
            else
            {
                $pluginInfo = $this -> getPluginInfo(["plgID" => $pluginName]);


                $plgContent = $pluginInfo["plg_content"];
                $plgParameters = $pluginInfo["plg_parameters"];
                if ($plgParameters != "")
                    $plgParameters = json_decode($plgParameters, true);
                $moduleType = $pluginInfo["plg_type"];

                $importPlugin = true;
            }


        }
        else
            $importPlugin = true;


        //get dir plugin base
        $dirPluginBase = $this->K_COMMON->getPathPluginObject(true, $pluginName, "");


        if ($importPlugin) {



            //search type of plugin
            switch ($moduleType) {
                case "kdata":

                    switch ($plgContent)
                    {
                        case "language":
                            $dir = $dirPluginBase . "/definition/" . "languages";
                            if (is_dir($dir)) {
                                $filenamedir = $dir . "/language.xml";
                                if (file_exists($filenamedir)) {
                                    $Res = $this->importLanguages($filenamedir);
                                    $errorMsg .= $Res["errormsg"];
                                    $successMsg .= $Res["successmsg"];
                                }
                            }
                            break;
                        case "dictionary":

                            $Res = $this->loadModuleDefinition(
                                [
                                    "plgID" => $pluginName
                                    , "pluginObject" => $pluginObject
                                    , "Type" => "dictionary"
                                    , "plgIDRef" => $plgParameters["pluginref"]
                                ]
                            );
                            $errorMsg .= $Res["errormsg"];
                            $successMsg .= $Res["successmsg"];
                            break;
                        case "plugindata":

                            $Res = $this->importDataTable(
                                [
                                    "pluginName" => $pluginName
                                ]
                            );
                            $errorMsg .= $Res["errormsg"];
                            $successMsg .= $Res["successmsg"];
                            break;
                        case "country":
                            $dir = $dirPluginBase . "/definition/" . "countries";
                            if (is_dir($dir)) {
                                $filenamedir = $dir . "/country.xml";
                                if (file_exists($filenamedir)) {
                                    $Res = $this->importCountries($filenamedir);
                                    $errorMsg .= $Res["errormsg"];
                                    $successMsg .= $Res["successmsg"];
                                }

                                $filenamedir = $dir . "/province.xml";
                                if (file_exists($filenamedir)) {
                                    $Res = $this->importProvince($filenamedir);
                                    $errorMsg .= $Res["errormsg"];
                                    $successMsg .= $Res["successmsg"];
                                }

                                $filenamedir = $dir . "/city.xml";
                                if (file_exists($filenamedir)) {
                                    $Res = $this->importCity($filenamedir);
                                    $errorMsg .= $Res["errormsg"];
                                    $successMsg .= $Res["successmsg"];
                                }
                            }

                            break;

                    }



                    break;
                default:

                    if ($typeUpdate != "") {
                        $Res = $this->loadModuleDefinition(
                            [
                                "plgID" => $pluginName,
                                "pluginObject" => $pluginObject,
                                "Type" => $typeUpdate,
                                "webCode" => $webCode
                            ]
                    );
                        $errorMsg .= $Res["errormsg"];
                        $successMsg .= $Res["successmsg"];
                    } else {
                        foreach ($this->pluginObjType as $type) {
                            $Res = $this->loadModuleDefinition(
                                [
                                    "plgID" => $pluginName,
                                    "pluginObject" => $pluginObject,
                                    "Type" => $type,
                                    "webCode" => $webCode
                                ]
                            );
                            $errorMsg .= $Res["errormsg"];
                            $successMsg .= $Res["successmsg"];
                        }
                    }
                    break;

            }

        }

        //check if success
        if ($errorMsg != "") $success = false;

        //return
        if ($success)
            return array("success" => "true", "ErrorMsg" => $errorMsg, "GoodMsg" => $successMsg);
        else
            return array("success" => "false", "ErrorMsg" => $errorMsg, "GoodMsg" => $successMsg);

    }

    function importConfigXML($ArrayVariable)
    {
        //set variable
        $pluginName = $this->K_COMMON->getVarArray($ArrayVariable, "pluginName");
        $pathPlugin = $this->K_COMMON->getPathPluginObject(true, $pluginName, "");
        $fileConfig = $pathPlugin . "/config.xml";



        //check if file Exist
        if (!file_exists($fileConfig))
            return ["success" => "false"];

        //table
        $tablePLG = $this->K_COMMON->getPluginDataObject("k_common", "plugins");

        //load xml file
        $xml = simplexml_load_file($fileConfig);

        //scan xml file
        $id = (string)$xml->id;
        $type = (string)$xml->type;
        $content = (string)$xml->content;
        $name = (string)$xml->name;
        $ver = (string)$xml->ver;
        $language = (string)$xml->language;
        $description = (string)$xml->description;
        $authors = (array)$xml->authors;
        $related = (array)$xml->related;
        $parameters = (array)$xml->parameters;

        $tablePLG->putByKey(
            [
                "plg_id" => $id
                , "plg_type" => $type
                , "plg_content" => $content
                , "plg_name" => $name
                , "plg_ver" => $ver
                , "plg_language" => $language
                , "plg_description" => $description
                , "plg_authors" => json_encode($authors)
                , "plg_related" => json_encode($related)
                , "plg_parameters" => json_encode($parameters)
            ]
        );


        return ["success" => "true"];

    }

    protected function loadModuleDefinition($ArrayVariable)
    {

        //set variable
        $pluginName = $ArrayVariable["plgID"];
        $pluginObject = $ArrayVariable["pluginObject"];
        $definitionType = $ArrayVariable["Type"];
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable,"webCode");
        $plgIDRef = $this->K_COMMON->getVarArray($ArrayVariable,"plgIDRef"); //This is the plugin Ref, for example for kdata dictionary
        $pluginObjectType = "";
        $pluginObjectName = ""; //short name of plugin object
        $success = true;
        $errorMsg = "";
        $successMsg = "";
        if ($plgIDRef == "")
            $plgIDRef = $pluginName;

        //get dir plugin base
        $dirPluginBase = $this->K_COMMON->getPathPluginObject(true, $pluginName, "");

        //IF UPDATE TABLES remove ALL FKeys and Indexes (Only if we update all tables)
        if ($definitionType == "table" & $pluginObject == "")
        {

            $this->kxdb->setForeignKeyCheck(0);

            //Get list table of this plugin
            $tablesArray = $this->K_COMMON->getListPluginDBTables($pluginName, "ARRAY");


            foreach ($tablesArray as $tablePlugin) {

                $this->kxdb->removeTableAllFKeys($tablePlugin);
            }

            foreach ($tablesArray as $tablePlugin) {
                $this->kxdb->removeTableAllIndexes($tablePlugin);
            }


            //get list tables of plugin customized
            $tablesArray = $this->K_COMMON->getListPluginDBTablesWithCustomField($pluginName, "ARRAY");

            $keyPrefix = "zkx_" . $pluginName . "_";

            foreach ($tablesArray as $tablePlugin) {
                $this->kxdb->removeTableAllFKeys($tablePlugin, $keyPrefix);
            }

            foreach ($tablesArray as $tablePlugin) {
                $this->kxdb->removeTableAllIndexes($tablePlugin, $keyPrefix);
            }


            $this->kxdb->setForeignKeyCheck(1);
        }



        //If plugin object = "" search on definition dir


        if ($pluginObject == "") {

            // Table have a different directory structure
            if ($definitionType != "table"){
                //search on definition folder of Plugin
                $dir = $dirPluginBase . "/definition/" . $definitionType;

                if (is_dir($dir)) {
                    //scan dir
                    $arraydir = scandir($dir);


                    //Import
                    $Res = $this->importModuleDefinition($arraydir, $dir, $plgIDRef, "", $definitionType, "", "", $webCode);
                    $errorMsg .= $Res["errormsg"];
                    $successMsg .= $Res["successmsg"];

                }
            }


        }
        else {

            $objectTypeArr = $this->K_COMMON->getObjectType($pluginObject);

            $pluginObjectName = $objectTypeArr["pluginObjectName"];
            $pluginObjectType = $objectTypeArr["objectType"];
        }


        //Load definition
        $arrayObjectType = $this->K_COMMON->getListObjectType($pluginObjectType);
        foreach ($arrayObjectType as $otCode => $otDesc) {
            $getTypeDir = $this->K_COMMON->getDirByObjectType($otCode);
            $dir = $dirPluginBase . "/" . $getTypeDir;

            //search in OBJECTS folder
            if (is_dir($dir)) {

                if ($pluginObject == "") {
                    $arraydirTmp = scandir($dir);
                    if (!empty($arraydirTmp))
                        sort($arraydirTmp);
                }
                else
                    $arraydirTmp = [$pluginObjectName];

                if ($definitionType != "table"){
                    foreach ($arraydirTmp as $pluginObjectNameDir) {

                        $dirDefinition = $dir . "/" . $pluginObjectNameDir . "/definition/" . $definitionType;
                        $pluginObjectWithType =  $otCode . "\\" . $pluginObjectNameDir;

                        if (($pluginObjectNameDir != ".") && ($pluginObjectNameDir != "..") && (is_dir($dirDefinition))) {

                            //scan dir
                            $arraydir = scandir($dirDefinition);

                            //Import
                            $Res = $this->importModuleDefinition($arraydir, $dirDefinition, $plgIDRef, $pluginObjectWithType, $definitionType, $pluginObjectNameDir, $otCode,$webCode);
                            $errorMsg .= $Res["errormsg"];
                            $successMsg .= $Res["successmsg"];

                        }
                    }
                }
                else{ //Execute only for table

                    // Execute First Time and Create Fields And Index
                    foreach ($arraydirTmp as $pluginObjectNameDir) {

                        $dirDefinition = $dir . "/" . $pluginObjectNameDir . "/definition/" . $definitionType;
                        $pluginObjectWithType =  $otCode . "\\" . $pluginObjectNameDir;

                        if (($pluginObjectNameDir != ".") && ($pluginObjectNameDir != "..") && (is_dir($dirDefinition))) {

                            //scan dir
                            $arraydir = scandir($dirDefinition);

                            // Import Fields + Index
                            $Res = $this->importModuleDefinition($arraydir, $dirDefinition, $plgIDRef, $pluginObjectWithType, $definitionType . "Fields", $pluginObjectNameDir, $otCode,$webCode);
                            $errorMsg .= $Res["errormsg"];
                            $successMsg .= $Res["successmsg"];


                        }
                    }

                    // Execute Another Time and Create Foreign Key
                    foreach ($arraydirTmp as $pluginObjectNameDir) {

                        $dirDefinition = $dir . "/" . $pluginObjectNameDir . "/definition/" . $definitionType;
                        $pluginObjectWithType =  $otCode . "\\" . $pluginObjectNameDir;

                        if (($pluginObjectNameDir != ".") && ($pluginObjectNameDir != "..") && (is_dir($dirDefinition))) {

                            //scan dir
                            $arraydir = scandir($dirDefinition);

                            // Import Foreign Key (import later of created all Indexes)
                            $Res = $this->importModuleDefinition($arraydir, $dirDefinition, $plgIDRef, $pluginObjectWithType, $definitionType . "FK", $pluginObjectNameDir, $otCode,$webCode);
                            $errorMsg .= $Res["errormsg"];
                            $successMsg .= $Res["successmsg"];


                        }
                    }
                }

            }
        }





        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);
    }

    protected function importModuleDefinition($arraydir, $dir, $pluginName, $objectName, $type, $pluginObjectName, $objType = "", $webCode)
    {

        //set variable
        $success = true;
        $errorMsg = "";
        $successMsg = "";


        //scan array with file to elaborate
        sort($arraydir, SORT_STRING);
        foreach ($arraydir as $filename) {


            //get complete path of file to import
            $filenamedir = $dir . "/" . $filename;

            //switch type (table  etc)
            switch ($type) {
                case "tableFields":
                    if (strstr($filename, ".xml")) {

                        if ($objectName != "") {

                            // Import Table Definition (FIELDS)
                            $resUpdateTable = $this->importTableDefinitionFieldsXML($pluginName, $filenamedir);
                            $successMsg .= $resUpdateTable["successmsg"];;
                            if ($resUpdateTable["success"] == "true")
                                $successMsg .= "TABLE " . $objectName . " UPDATED<br>";
                            $errorMsg .= $resUpdateTable["errormsg"];;

                        }

                        break;
                    }
                case "tableFK":
                    if (strstr($filename, ".xml")) {

                        if ($objectName != "") {

                            // Import Table Definition (FK)
                            $resUpdateTable = $this->importTableDefinitionFKXML($pluginName, $filenamedir);
                            $successMsg .= $resUpdateTable["successmsg"];;
                            if ($resUpdateTable["success"] == "true")
                                $successMsg .= "TABLE " . $objectName . " UPDATED<br>";
                            $errorMsg .= $resUpdateTable["errormsg"];;

                        }

                        break;
                    }
                case "panel":
                    if (strstr($filename, ".xml")) {
                        $Res = $this->importPanel($pluginName, $objectName, $filenamedir, $pluginObjectName, $objType);
                        $errorMsg .= $Res["errormsg"];
                        $successMsg .= $Res["successmsg"];
                    }

                    break;
                case "custom":
                    if (strstr($filename, ".xml")) {
                        $Res = $this->importCustom($pluginName, $objectName, $filenamedir);
                        $errorMsg .= $Res["errormsg"];
                        $successMsg .= $Res["successmsg"];
                    }

                    break;
                case "webobject":
                    if (strstr($filename, ".xml")) {
                        $Res = $this->importWebObject($pluginName, $objectName, $filenamedir);
                        $errorMsg .= $Res["errormsg"];
                        $successMsg .= $Res["successmsg"];
                    }

                    break;
                case "webtemplate":

                    if (strstr($filename, ".xml")) {
                        $Res = $this->importWebTemplate($pluginName, $objectName, $filenamedir,$webCode);
                        $errorMsg .= $Res["errormsg"];
                        $successMsg .= $Res["successmsg"];
                    }

                    break;
                case "paneltemplate":
                    if (strstr($filename, ".xml")) {
                        $Res = $this->importWebTemplate($pluginName, $objectName, $filenamedir,"");
                        $errorMsg .= $Res["errormsg"];
                        $successMsg .= $Res["successmsg"];
                    }

                    break;
                case "webpage":
                    if (strstr($filename, ".xml")) {

                        $Res = $this->importWebPage($pluginName, $objectName, $filenamedir,$webCode);
                        $errorMsg .= $Res["errormsg"];
                        $successMsg .= $Res["successmsg"];
                    }

                    break;
                case "dictionary":
                    if (strstr($filename, ".xml")) {
                        $Res = $this->importDictionary($pluginName, $objectName, $filenamedir, $webCode);
                        $errorMsg .= $Res["errormsg"];
                        $successMsg .= $Res["successmsg"];
                    }

                    break;
                case "customfields":
                    if (strstr($filename, ".xml")) {
                        $Res = $this->importCustomFields($pluginName, $filenamedir);
                        $errorMsg .= $Res["errormsg"];
                        $successMsg .= $Res["successmsg"];
                    }

                    break;
                case "authorization":

                    if (strstr($filename, ".xml")) {
                        $Res = $this->importAuthorization($pluginName, $filenamedir);
                        $errorMsg .= $Res["errormsg"];
                        $successMsg .= $Res["successmsg"];
                    }

                    break;
                case "actions":
                    if (strstr($filename, ".xml")) {
                        $Res = $this->importMailingActions($pluginName, $objectName, $filenamedir);
                        $errorMsg .= $Res["errormsg"];
                        $successMsg .= $Res["successmsg"];
                    }
                    break;
                case "messages":
                    if (strstr($filename, ".xml")) {
                        $Res = $this->importMailingMessages($pluginName, $objectName, $filenamedir);
                        $errorMsg .= $Res["errormsg"];
                        $successMsg .= $Res["successmsg"];
                    }
                    break;
                case "trigger":
                    if (strstr($filename, ".xml")) {
                        $Res = $this->importTrigger($pluginName, $objectName, $filenamedir);
                        $errorMsg .= $Res["errormsg"];
                        $successMsg .= $Res["successmsg"];
                    }
                    break;
                case "event":
                    if (strstr($filename, ".xml")) {
                        $Res = $this->importEvent($pluginName, $objectName, $filenamedir);
                        $errorMsg .= $Res["errormsg"];
                        $successMsg .= $Res["successmsg"];
                    }
                    break;
                case "sp":
                    if (strstr($filename, ".xml")) {
                        $Res = $this->importSP($pluginName, $objectName, $filenamedir);
                        $errorMsg .= $Res["errormsg"];
                        $successMsg .= $Res["successmsg"];
                    }
                    break;
                case "parameters":
                    if (strstr($filename, ".xml")) {
                        $Res = $this->importParameters($pluginName, $filenamedir);
                        $errorMsg .= $Res["errormsg"];
                        $successMsg .= $Res["successmsg"];
                    }
                    break;


            }


        }

        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);

    }

    protected function importPanel($pluginName, $objectName, $fileName, $pluginObjectName, $objType)
    {


        //set variable
        $success = true;
        $errorMsg = "";
        $successMsg = "";
        $arrayMenuCode = [];

        //get panel menu data object
        $table = $this->K_COMMON->getPluginDataObject("k_panelmanage", "menu");
        $tableDSB = $this->K_COMMON->getPluginDataObject("k_panelmanage", "dashboard");

        //load xml file
        $xml = simplexml_load_file($fileName);


        //scan xml file for panel menu
        foreach ($xml->menu as $menu) {
            $arrayMenuCode[] = (string)$menu->code;
            $auth = (string)$menu->auth;
            if ($auth != "") {
                $autPName = ((string)$menu->authPName != "" ? (string)$menu->authPName : $pluginName);
            } else {
                $autPName = "";
            }

            $refCode = (string)$menu->refCode;
            $url = (string)$menu->url;
            $displayPluginName = (string)$menu->displayPluginName;
            $displayPluginObject = (string)$menu->displayPluginObject;
            $getPluginName = (string)$menu->getPluginName;
            $getPluginObject = (string)$menu->getPluginObject;
            $getPluginMethod = (string)$menu->getPluginMethod;
            $getPluginTpl = (string)$menu->getPluginTpl;
            if ($url == "")
                $url = $pluginObjectName;
            if ($getPluginName == "")
                $getPluginName = $pluginName;
            if ($getPluginObject == "")
                $getPluginObject = $objectName;
            if ($getPluginMethod == "")
                $getPluginMethod = "list";

            if ($displayPluginName == "")
            {
                $displayPluginName = $pluginName;
                $displayPluginObject = $objectName;
            }


            $dataMenu = [
                "pmn_owner_plugin_name" => $pluginName
                , "pmn_owner_plugin_object" => $objectName
                , "pmn_plugin_name" => $displayPluginName
                , "pmn_plugin_object" => $displayPluginObject
                , "pmn_menu_code" => (string)$menu->code
                , "pmn_menu_ref_code" => $refCode
                , "pmn_menu_up_code" => (string)$menu->menuup
                , "pmn_order" => (string)$menu->order
                , "pmn_label" => (string)$menu->label
                , "pmn_icon" => (string)$menu->icon
                , "pmn_authorization_plugin_name" => $autPName
                , "pmn_authorization" => $auth
                , "pmn_url" => $url
                , "pmn_get_plugin_name" => $getPluginName
                , "pmn_get_plugin_object" => $getPluginObject
                , "pmn_get_plugin_method" => $getPluginMethod
                , "pmn_tpl_code" => $getPluginTpl

            ];

            $table->putByKey($dataMenu);


            $successMsg .= "UPDATED MENU " . (string)$menu->code . "<br>";
        }





        //remove old menu
        $table->delete("pmn_plugin_name = :plugin AND pmn_plugin_object = :object AND pmn_menu_code not in (:codes)"
            , [
                "plugin" => $pluginName,
                "object" => $objectName,
                "codes" => $arrayMenuCode
            ]
        );

        //scan xml file for panel dashboard
        foreach ($xml->dashboard as $menu) {
            $arrayMenuCode[] = (string)$menu->code;
            $auth = (string)$menu->auth;
            $dmnType = (string)$menu->type;
            if ($auth != "") {
                $autPName = ((string)$menu->authPName != "" ? (string)$menu->authPName : $pluginName);
            } else {
                $autPName = "";
            }
            if ($dmnType == "")
                $dmnType = "menu";

            $dataMenu = [
                "dmn_plugin_name" => $pluginName
                , "dmn_plugin_object" => $objectName
                , "dmn_menu_code" => (string)$menu->code
                , "dmn_label" => (string)$menu->label
                , "dmn_icon" => (string)$menu->icon
                , "dmn_type" => (string)$dmnType
                , "dmn_authorization_plugin_name" => $autPName
                , "dmn_authorization" => $auth


            ];
            $tableDSB->putByKey($dataMenu);

            $successMsg .= "UPDATED DASHBOARD " . (string)$menu->code . "<br>";
        }


        //Unset Object
        unset($table);
        unset($tableDSB);

        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);
    }

    protected function importCustom($pluginName, $objectName, $fileName)
    {


        //set variable
        $success = true;
        $errorMsg = "";
        $successMsg = "";


        //get panel menu data object
        $table = $this->K_COMMON->getPluginDataObject("k_custommanager", "customization");

        //load xml file
        $xml = simplexml_load_file($fileName);


        //scan xml file
        foreach ($xml->custom as $customization) {

            $code = (string)$customization->code;
            $description = (string)$customization->description;
            $source_plugin = (string)$customization->source_plugin;
            $source_object = (string)$customization->source_object;
            $target_when = (string)$customization->target_when;
            $target_plugin = (string)$customization->target_plugin;
            $target_object = (string)$customization->target_object;
            $getParameters = (array)$customization->parameters;

            //check variables
            if ($target_when == "")
                $target_when = "after";

            //get parameters
            $arrayParam = [];
            foreach ($getParameters as $param => $parVal) {

                //if empty set to "" otherwise set to {}
                if (empty($parVal) && (!$parVal === 0))
                    $parVal = "";

                $arrayParam[$param] = $parVal;
            }
            $getParameters = json_encode($arrayParam, JSON_PRESERVE_ZERO_FRACTION);

            $source_plugin = ($source_plugin != "" ? $source_plugin : $pluginName);
            $source_object = ($source_object != "" ? $source_object : $objectName);

            $data = [
                "cst_code" => $code
                , "cst_description" => $description
                , "cst_owner_plugin_name" => $pluginName
                , "cst_owner_plugin_object" => $objectName
                , "cst_source_plugin_name" => $source_plugin
                , "cst_source_plugin_object" => $source_object
                , "cst_target_when" => $target_when
                , "cst_target_plugin_name" => $target_plugin
                , "cst_target_plugin_object" => $target_object
                , "cst_parameters" => $getParameters
                , "cst_active" => "1"


            ];
            $table->putByKey($data);

            $successMsg .= "UPDATED CUSTOM " . $source_object . "-" . $target_plugin . "-" . $target_object . "<br>";
        }


        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);
    }

    protected function importWebObject($pluginName, $objectName, $fileName)
    {
        //set variable
        $success = true;
        $errorMsg = "";
        $successMsg = "";


        //get panel menu data object
        $tableOBJ = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_objects");

        //load xml file
        $xml = simplexml_load_file($fileName);


        //scan xml file
        foreach ($xml->webobject as $webObject) {

            $name = (string)$webObject->name;
            $type = (string)$webObject->type;
            $container = (string)$webObject->container;
            $prototypes = (array)$webObject->prototypes;

            if ($container == "")
                $container = 0;


            $prototypesJSON = json_encode($prototypes);

            $data = [
                "wob_plugin_name" => $pluginName
                , "wob_plugin_object" => $objectName
                , "wob_container" => $container
                , "wob_name" => $name
                , "wob_type" => $type
                , "wob_prototypes" => $prototypesJSON

            ];

            $tableOBJ->putByKey($data);

            $successMsg .= "UPDATED WO " . $pluginName . "-" . $objectName . "-" . $name . "<br>";

        }


        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);
    }

    protected function importTrigger($pluginName, $objectName, $fileName)
    {
        //set variable
        $success = true;
        $errorMsg = "";
        $successMsg = "";


        //load xml file
        $xml = simplexml_load_file($fileName);


        //scan xml file
        foreach ($xml->trigger as $trigger) {

            $fires = (string)$trigger->fires;
            $event = (string)$trigger->event;
            $statement = (string)$trigger->statement;
            $tableName = $this->K_COMMON->getTableNameByObject($pluginName, $objectName);
            $triggerName = strtolower($tableName . "_" . $fires . "_" . $event);


            $query = " DROP TRIGGER IF EXISTS  " . $triggerName . ";";
            $this->kxdb->prepare($query);
            $this->kxdb->execute();

            $query = " CREATE TRIGGER " . $triggerName . "  " . $fires . " " . $event . " ON " . $tableName . " FOR EACH ROW " . $statement . ";";
            $this->kxdb->prepare($query);
            $this->kxdb->execute();

            $successMsg .= "UPDATED TRIGGER " . $pluginName . "-" . $objectName . "-" . $fires . "_" . $event . "<br>";

        }


        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);
    }

    protected function importEvent($pluginName, $objectName, $fileName)
    {
        //set variable
        $success = true;
        $errorMsg = "";
        $successMsg = "";


        //load xml file
        $xml = simplexml_load_file($fileName);


        //scan xml file
        foreach ($xml->event as $event) {

            $name = (string)$event->name;
            $interval = (string)$event->interval;
            $starttime = (string)$event->starttime;
            $statement = (string)$event->statement;
            $eventName = $pluginName . ($objectName != "" ? "_" . $objectName : "");
            $eventName = strtolower($eventName . "_" . $name);


            //drop actual event
            $query = " DROP EVENT IF EXISTS  " . $eventName . ";";
            $this->kxdb->prepare($query);
            $this->kxdb->execute();


            //create new event
            $query = " CREATE EVENT " . $eventName . " ON SCHEDULE EVERY " . $interval . ($starttime != "" ? " STARTS '" . $starttime . "'" : "") . " DO " . $statement;
            $this->kxdb->prepare($query);
            $this->kxdb->execute();


            //start scheduler
            $query = " SET GLOBAL event_scheduler = 1;";
            $this->kxdb->prepare($query);
            $this->kxdb->execute();


            $successMsg .= "UPDATED EVENT " . $pluginName . "-" . $objectName . "-" . $eventName . "<br>";

        }


        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);
    }

    protected function importSP($pluginName, $objectName, $fileName)
    {
        //set variable
        $success = true;
        $errorMsg = "";
        $successMsg = "";


        //load xml file
        $xml = simplexml_load_file($fileName);


        //scan xml file
        foreach ($xml->function as $function) {

            $name = (string)$function->name;
            $type = (string)$function->type;
            $parameters = (string)$function->parameters;
            $returntype = (string)$function->returntype;
            $statement = (string)$function->statement;
            $spName = $pluginName . ($objectName != "" ? "_" . $objectName : "");
            $spName = strtolower($spName . "_" . $name);

            if ($type == "FUNCTION")
                $returntype = "RETURNS " . $returntype;

            //drop actual procedure
            $query = " DROP PROCEDURE IF EXISTS  " . $spName . ";";
            $this->kxdb->prepare($query);
            $this->kxdb->execute();

            //drop actual function
            $query = " DROP FUNCTION IF EXISTS  " . $spName . ";";
            $this->kxdb->prepare($query);
            $this->kxdb->execute();

            //create new procedure
            $query = " CREATE " . $type . " " . $spName . " (" . $parameters . ") " . $returntype . " " . $statement . ";";
            $this->kxdb->prepare($query);
            $this->kxdb->execute();

            $successMsg .= "CREATED " . strtoupper($type) . " " . $pluginName . "-" . $objectName . "-" . $spName . "<br>";

        }


        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);
    }

    protected function importWebTemplate($pluginName, $objectName, $fileName,$webCode)
    {
        //set variable
        $success = true;
        $errorMsg = "";
        $successMsg = "";


        //get data object
        $tableTPL = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_templates");
        $tableTPLCNT = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_templates_content");


        //load xml file
        $xml = simplexml_load_file($fileName);
        $firstNode = $xml->getName();

        if ($firstNode == "webtemplates")
            $typeTPL = "web";
        elseif ($firstNode == "paneltemplates")
            $typeTPL = "panel";
        else return;

        //scan xml file
        $xmlData = $this->K_COMMON->xmlToArray($xml);


        //Check if more then 1 template
        $checkKey = key(current($xmlData));
        if (!is_numeric($checkKey))
            $xmlData = [0 => current($xmlData)];
        else
            $xmlData = current($xmlData);


        foreach ($xmlData as $webTemplate) {



            $name = $this->K_COMMON->getVarArray($webTemplate, "name");
            $code = $this->K_COMMON->getVarArray($webTemplate, "code");
            $file = $this->K_COMMON->getVarArray($webTemplate, "file");
            $include = $this->K_COMMON->getVarArray($webTemplate, "include");
            $objects = $this->K_COMMON->getVarArray($webTemplate, "objects");
            $includeFile = "";
            $includeFileArr = [];

            if (is_array($include))
                foreach ($include as $type => $arr) {

                    if (is_array($arr)) {
                        foreach ($arr as $fileToInclude) {
                            $includeFileArr[$type][] = $fileToInclude;
                        }
                    } else
                        $includeFileArr[$type][] = $arr;

                }

            if (!empty($includeFileArr))
                $includeFile = json_encode($includeFileArr);


            $data = [
                "tpl_web_code" => $webCode
                , "tpl_plugin_name" => $pluginName
                , "tpl_plugin_object" => $objectName
                , "tpl_code" => $code
                , "tpl_name" => $name
                , "tpl_file" => $file
                , "tpl_type" => $typeTPL
                , "tpl_include" => $includeFile

            ];

            $tableTPL->putByKey($data);


            //delete data content
            $tableTPLCNT->delete([
                "tpo_web_code" => $webCode
                , "tpo_plugin_name" => $pluginName
                , "tpo_plugin_object" => $objectName
                , "tpo_tpl_code" => $code
            ]);



            //Insert Object Used and Template Content
            if (is_array($objects) && !empty($objects))
            {
                if (isset($objects["object"][0]))
                    $objects=$objects["object"];

                foreach ($objects as $object) {



                    $id = $this->K_COMMON->getVarArray($object, "id");
                    $idup = $this->K_COMMON->getVarArray($object, "idup");
                    $order = $this->K_COMMON->getVarArray($object, "order");
                    $getPluginName = $this->K_COMMON->getVarArray($object, "pluginname");
                    $getPluginObject = $this->K_COMMON->getVarArray($object, "pluginobject");

                    $pCode = $this->K_COMMON->getVarArray($object, "pcode");

                    $getParameters = $this->K_COMMON->getVarArray($object, "parameters");
                    $kwebobjectpar = $this->K_COMMON->getVarArray($object, "kwebobjectpar");
                    $kwebobjectdata = $this->K_COMMON->getVarArray($object, "kwebobjectdata");

                    //get parameters WOU
                    if (is_array($getParameters))
                        $getParameters = json_encode($getParameters);
                    else
                        $getParameters = json_encode([]);

                    $getParameters = str_replace(":[]", ':""', $getParameters);


                    //get parameters CONTAINER
                    if (is_array($kwebobjectpar))
                        $kwebobjectpar = json_encode($kwebobjectpar);
                    else
                        $kwebobjectpar = json_encode([]);

                    $kwebobjectpar = str_replace(":[]", ':""', $kwebobjectpar);

                    //get parameters DATA
                    if (is_array($kwebobjectdata))
                        $kwebobjectdata = json_encode($kwebobjectdata);
                    else
                        $kwebobjectdata = json_encode([]);

                    $kwebobjectdata = str_replace(":[]", ':""', $kwebobjectdata);


                    //Save Template Content
                    $data = [
                        "tpo_web_code" => $webCode
                        , "tpo_plugin_name" => $pluginName
                        , "tpo_plugin_object" => $objectName
                        , "tpo_tpl_code" => $code
                        , "tpo_tpl_id_obj" => $id
                        , "tpo_tpl_id_obj_up" => $idup
                        , "tpo_order" => $order
                        , "tpo_parameters" => $kwebobjectpar
                        , "tpo_data" => $kwebobjectdata
                        , "tpo_wo_plugin_name" => $getPluginName
                        , "tpo_wo_plugin_object" => $getPluginObject
                        , "tpo_wo_plugin_object_pcode" => $pCode
                        , "tpo_wo_parameters" => $getParameters


                    ];
                    $tableTPLCNT->putByKey($data);


                }
            }

            $successMsg .= "UPDATED WT " . $pluginName . "-" . $objectName . "-" . $name . "<br>";

        }


        //Unset
        unset($tableTPL);
        unset($tableTPLCNT);

        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);
    }

    protected function importWebPage($pluginName, $objectName, $fileName,$webCode)
    {
        //set variable
        $success = true;
        $errorMsg = "";
        $successMsg = "";

        //get data object
        $tableOBJ = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_pages");
        $tableWT = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_templates");

        //include modules
        $objURLRewrite = $this->K_COMMON->getPluginObject("k_webmanager", "urlrewrite");

        //load xml file
        $xml = simplexml_load_file($fileName);


        //scan xml file
        foreach ($xml->webpage as $webPage) {

            $code = (string)$webPage->code;
            $name = (string)$webPage->name;
            $title = (string)$webPage->title;
            $url = (string)$webPage->url;
            $tmplCode = (string)$webPage->tmplCode;
            $tmplID = "";


            //search template
            if ($tmplCode != "") {
                $tmplPlugin = $pluginName;
                $tmplPluginObject = $objectName;
                $whereWT = ["tpl_plugin_name" => $tmplPlugin, "tpl_plugin_object" => $tmplPluginObject, "tpl_code" => $tmplCode, "tpl_web_code" => $webCode];
                $tableWT->get("*", $whereWT);
                $dataWT = $tableWT->fetch();
                if ($dataWT)
                    $tmplID = $dataWT->id_web_template;
            }


            //check if page exist
            $tableOBJ->get("*", [
                "wpg_plugin_name" => $pluginName
                , "wpg_plugin_object" => $objectName
                , "wpg_code" => $code
                , "wpg_web_code" => $webCode
            ]);

            $row = $tableOBJ->fetch();
            if ($row)
                $pageExist = true;
            else
                $pageExist = false;



            $data = [
                "wpg_web_code" => $webCode
                , "wpg_plugin_name" => $pluginName
                , "wpg_plugin_object" => $objectName
                , "wpg_code" => $code
                , "wpg_name" => $name
                , "wpg_title" => $title
                , "wpg_tmpl_id" => $tmplID

            ];

            $tableOBJ->putByKey($data);
            $keyID = $tableOBJ->getNewID();


            //create url (if new page
            if (!$pageExist) {

                $urlExist = $objURLRewrite->checkExistURL(
                    [
                        "pluginName" => $pluginName
                        , "pluginObject" => $objectName
                        , "refCode" => $keyID
                        , "webCode" => $webCode
                        , "url" => $url
                    ]);

                if ($urlExist)
                    $url = "/" . time() . $url;


                $res = $objURLRewrite->createURL([
                    "pluginName" => $pluginName
                    , "pluginObject" => $objectName
                    , "refCode" => $keyID
                    , "webCode" => $webCode
                    , "url" => $url
                    , "pageID" => $keyID
                ]);


            }


            $successMsg .= "UPDATED WO " . $pluginName . "-" . $objectName . "-" . $name . "<br>";

        }


        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);
    }

    protected function importAuthorization($pluginName, $fileName)
    {


        //set variable
        $success = true;
        $errorMsg = "";
        $successMsg = "";


        //get panel menu data object
        $table = $this->K_COMMON->getPluginDataObject("k_secauth", "authtype");

        //load xml file
        $xml = simplexml_load_file($fileName);


        //scan xml file
        foreach ($xml->authorization as $object) {

            $code = (string)$object->code;
            $codeLabel = (string)$object->label;
            $dataref = (array)$object->dataref;
            $byWebTarget = (string)$object->byWebTarget;
            $description = (array)$object->description;

            $data = [
                "plugin_name" => $pluginName
                , "code_form" => $code
                , "code_label" => $codeLabel
                , "code_table_ref" => $dataref
                , "auth_by_web_target" => $byWebTarget
                , "description" => $description

            ];
            $table->putByKey($data);


            $successMsg .= "UPDATED AUTH " . $pluginName  . "-" . $code . "<br>";
        }


        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);
    }

    protected function importDictionary($pluginName, $objectName, $fileName,$webCode)
    {


        //set variable
        $success = true;
        $errorMsg = "";
        $successMsg = "";


        //include object
        $objDictionary = $this->K_COMMON->getPluginObject("k_dictionary", "objects\\general\\dictionary");

        //load xml file
        $xml = simplexml_load_file($fileName);


        //scan xml file
        foreach ($xml->word as $object) {

            $code = (string)$object->code;
            $isWeb = (string)$object->isWeb;
            $languages = (array)$object->languages;
            $link = (array)$object->link;

            $linkToPut = [];
            foreach ($link as $key => $value) {

                $linkToPut[$key] = $value;

            }

            foreach ($languages as $key => $value) {


                $objDictionary->put(
                    [
                        "webCode" => ($isWeb == "1" ? $webCode : "")
                        ,"pluginName" => $pluginName
                        , "pluginObject" => $objectName
                        , "word" => $code
                        , "language" => $key
                        , "translation" => (string)$value
                        , "link" => $linkToPut
                    ]
                );

            }

            $successMsg .= "UPDATED DICT " . $pluginName . "-" . $objectName . "-" . $code . "<br>";



        }

        //Unset
        unset($objDictionary);


        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);
    }

    protected function importCustomFields($pluginName, $fileName)
    {

        //set variable
        $success = true;
        $errorMsg = "";
        $successMsg = "";

        //get data object
        $tableCF = $this->K_COMMON->getPluginDataObject("k_usefuldata", "customfield");
        $tableCFV = $this->K_COMMON->getPluginDataObject("k_usefuldata", "customfieldvalues");

        //load xml file
        $xml = simplexml_load_file($fileName);


        //scan xml file
        foreach ($xml->customfield as $object) {

            $code = (string)$object->code;
            $name = (array)$object->name;
            $fields = $object->fields;

            $data = [
                "plugin_name" => $pluginName
                , "custom_field_code" => $code
                , "field_name" => $name

            ];
            $tableCF->putByKey($data);

            foreach ($fields->field as $field) {

                $data = [
                    "plugin_name" => $pluginName
                    , "custom_field_code" => $code
                    , "custom_field_order" => $field->order
                    , "field_code" => $field->code
                    , "field_text" => $field->text

                ];
                $tableCFV->putByKey($data);

            }

            $successMsg .= "UPDATED CUSTOM FIELD " . $pluginName . "-" . $code . "<br>";



        }


        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);
    }

    protected function importMailingActions($pluginName, $objectName, $fileName)
    {


        //set variable
        $success = true;
        $errorMsg = "";
        $successMsg = "";


        //get data object
        $table = $this->K_COMMON->getPluginDataObject("k_mailing", "actions");

        //load xml file
        $xml = simplexml_load_file($fileName);


        //scan xml file
        foreach ($xml->action as $object) {

            $code = (string)$object->code;
            $from = (array)$object->from;
            $to = (array)$object->to;
            $messagecode = (array)$object->messagecode;

            $data = [
                "act_plugin_name" => $pluginName
                , "act_plugin_object" => $objectName
                , "act_code" => $code
                , "act_email_from" => $from
                , "act_email_to" => $to
                , "act_message_code" => $messagecode
                , "act_web_code" => ""

            ];
            $table->putByKey($data);


            $successMsg .= "UPDATED MACTION " . $pluginName . "-" . $objectName . "-" . $code . "<br>";
        }


        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);
    }

    protected function importMailingMessages($pluginName, $objectName, $fileName)
    {


        //set variable
        $success = true;
        $errorMsg = "";
        $successMsg = "";


        //get panel menu data object
        $table = $this->K_COMMON->getPluginDataObject("k_mailing", "messages");

        //load xml file
        $xml = simplexml_load_file($fileName);


        //scan xml file
        foreach ($xml->message as $object) {

            $code = (string)$object->code;
            $languages = (array)$object->languages;


            foreach ($languages as $key => $value) {

                $value = (array)$value;
                $subject = (string)$value["subject"];
                $body = (string)$value["body"];


                $data = [
                    "msg_plugin_name" => $pluginName
                    , "msg_plugin_object" => $objectName
                    , "msg_code" => $code
                    , "msg_language_code" => $key
                    , "msg_subject" => $subject
                    , "msg_message" => $body
                    , "msg_web_code" => ""

                ];
                $table->putByKey($data);
            }


            $successMsg .= "UPDATED MMESSAGE " . $pluginName . "-" . $objectName . "-" . $code . "<br>";
        }


        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);
    }

    protected function importParameters($pluginName, $fileName)
    {


        //set variable
        $success = true;
        $errorMsg = "";
        $successMsg = "";


        //get data tables
        $tablePar = $this->K_COMMON->getPluginDataObject("k_common", "parameters");
        $tableParDef = $this->K_COMMON->getPluginDataObject("k_common", "parameters_definition");

        //load xml file
        $xml = simplexml_load_file($fileName);


        //scan xml file
        foreach ($xml->parameter as $parameter) {

            $code = (string)$parameter->code;
            $value = (string)$parameter->value;
            $description = (string)$parameter->description;
            $label = (string)$parameter->label;

            $tableParDef->get("*", [
                " par_code = '" . $code . "' "
                . " AND par_plugin_name = '" . $pluginName . "' "
            ]);
            $dataParamDef = $tableParDef->fetch();


            $ArrParDefIns = [
                "par_code" => $code
                , "par_plugin_name" => $pluginName
                , "par_description" => $description
                , "par_code_label" => $label
            ];

            $ArrParIns = [
                "par_plugin_name" => $pluginName
                , "par_code" => $code
                , "par_value" => $value
                , "par_web_code" => ""
            ];

            if ($dataParamDef) {

                $tableParDef->put($ArrParDefIns, ["id_parameters_definition" => $dataParamDef->id_parameters_definition]);

                $tablePar->put($ArrParIns, ["par_plugin_name = '" . $pluginName . "' AND par_code='" . $code . "' AND (par_web_code is null OR par_web_code ='') "]);


            } else {

                $tableParDef->put($ArrParDefIns);

                $tablePar->put($ArrParIns);
            }


            $successMsg .= "UPDATED PARAM " . $pluginName . " (" . $code . ") UPDATED<br>";
        }


        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);
    }


    protected function importTableDefinitionFieldsXML($pluginName, $filenamedir)
    {

        // set global variable
        $this->kxdb->setTraceError();

        //SET FOREIGN KEY CHECK TO DIABLED
        $this->kxdb->setForeignKeyCheck(0);

        //set variables
        $success = true;
        $errorMsg = "";
        $successMsg = "";
        $indexes = [];


        //load xml file
        $xml = simplexml_load_file($filenamedir);


        //check if Custom Table
        $customPlugin = $xml->table->custom->plugin;
        $shortTableName = $xml->table->name;
        if ($customPlugin != "")
        {
            $tableName = $customPlugin . "_" . $shortTableName ;
            $isCustomTable = true;
        }
        else
        {
            $tableName = $pluginName . "_" . $shortTableName ;
            $isCustomTable = false;
        }


        $successMsg .= "<br>START TABLE: " . $tableName . "<br>";

        // Remove Indexes & FK
        // Remove only for this table because, if we update only a table, are not removed on previous step
        $this->kxdb->removeTableAllFKeys($tableName);
        $this->kxdb->removeTableAllIndexes($tableName);

        //scan xml file for fields
        $tableDefinition = [];
        foreach ($xml->table->columns->column as $column) {

            $field = (string)$column->field;
            $type = (string)$column->type;
            $null = (string)$column->null;
            $key = (string)$column->key;
            $default = (string)$column->default;
            $extra = (string)$column->extra;
            $comment = (string)$column->comment;

            if ($isCustomTable)
                $field = "zkx_" . $pluginName . "_" . $field;

            $tableDefinition[$field] =
                [
                    "Type" => $type,
                    "Default" => $default,
                    "NULL" => ($null == "YES" ? 1 : 0),
                    "extra" => $extra,
                    "PK" => ($key == "PRI" ? "1" : "0"),
                    "Comment" => $comment
                ];

        }


        // update table
        $this->kxdb->createTable($tableName, $tableDefinition, true, $isCustomTable);


        // export error log
        $traceLog = $this->kxdb->getTraceLog();
        if (count($traceLog) > 0) {
            foreach ($traceLog as $err) {
                $errorMsg .= "TABLE: " . $tableName . "<br><span style=\"color:red;\"><b>ERROR: </b></span>" . $err["errorStr"] . "<br><br>" . $err["query"] . "<br><br>";
            }

        } else
            $successMsg .= "TABLE: " . $tableName . " CREATED" . "<br>";

        // empty trace log
        $this->kxdb->emptyTraceLog();

        $successMsg .= "UPDATED TABLE " . $tableName . "<br>";



        //scan xml file for indexes
        foreach ($xml->table->indexes->index as $index) {

            $name = (string)$index->name;
            $unique = (string)$index->unique;
            $columns = (string)$index->columns;
            $type = (string)$index->type;


            if ($isCustomTable)
            {
                $prefix = "zkx_" . $pluginName . "_";
                if (strpos($name, $prefix) !== 0)
                    $name = $prefix . $name;
            }


            $indexes[] = [
                'table' => $tableName,
                'name' => $name,
                'type' => $type,
                'columns' => $columns,
                'unique' => $unique
            ];


        }

        // update indexes
        if (!empty($indexes))
            $this->kxdb->createTableIndexKey($indexes, "");

        // empty trace log
        $this->kxdb->emptyTraceLog();

        $successMsg .= "END TABLE: " . $tableName . "<br><br>";


        //SET FOREIGN KEY CHECK TO ENABLED
        $this->kxdb->setForeignKeyCheck(1);

        //check if success
        if ($errorMsg != "") $success = false;

        //return
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);




    }

    protected function importTableDefinitionFKXML($pluginName, $filenamedir)
    {

        // set global variable
        $this->kxdb->setTraceError();

        //SET FOREIGN KEY CHECK TO DIABLED
        $this->kxdb->setForeignKeyCheck(0);

        //set variables
        $success = true;
        $errorMsg = "";
        $successMsg = "";
        $fkIndexes = [];


        //load xml file
        $xml = simplexml_load_file($filenamedir);


        //check if Custom Table
        $customPlugin = $xml->table->custom->plugin;
        $shortTableName = $xml->table->name;
        if ($customPlugin != "")
        {
            $tableName = $customPlugin . "_" . $shortTableName ;
            $isCustomTable = true;
        }
        else
        {
            $tableName = $pluginName . "_" . $shortTableName ;
            $isCustomTable = false;
        }


        $successMsg .= "<br>START TABLE FK: " . $tableName . "<br>";


        //scan xml file for FK
        foreach ($xml->table->fkeys->fk as $fk) {

            $name = (string)$fk->name;
            $ref_table_name = (string)$fk->ref_table_name;
            $on_delete = (string)$fk->on_delete;
            $on_update = (string)$fk->on_update;
            $fields = (string)$fk->fields;
            $ref_fields = (string)$fk->ref_fields;

            if ($isCustomTable)
            {
                $prefix = "zkx_" . $pluginName . "_";
                if (strpos($name, $prefix) !== 0)
                    $name = $prefix . $name;
            }
            else{
                $prefix = $tableName . "_";
                if (strpos($name, $prefix) !== 0)
                    $name = $prefix . $name;
            }


            $fkIndexes[] = [
                'name' => $name,
                'table' => $tableName,
                'fields' => $fields,
                'refTable' => $ref_table_name,
                'refFields' => $ref_fields,
                'onDelete' => $on_delete,
                'onUpdate' => $on_update
            ];


        }

        // foreign key
        if (!empty($fkIndexes))
            $this->kxdb->createTableForeignKey($fkIndexes, "");


        // export error log
        $traceLog = $this->kxdb->getTraceLog();
        if (count($traceLog) > 0) {


            foreach ($traceLog as $err) {
                $errorString = $err["errorStr"];
                if (!(strpos($errorString, "Duplicate key") === false))
                    $errorType = 2;
                elseif (!(strpos($errorString, "Cannot add") === false))
                    $errorType = 1;
                elseif (!(strpos($errorString, "Cannot drop") === false))
                    $errorType = 2;
                else
                    $errorType = 1;

                if ($errorType == 1)
                    $errorMsg .= "CONSTRAINT: " . $tableName . "<br><span style=\"color:red;\"><b>ERROR: </b></span>" . $err["errorStr"] . "<br><br>" . $err["query"] . "<br><br>";
                else
                    $errorMsg .= "CONSTRAINT: " . $tableName . "<br><span style=\"color:orange;\"><b>WARNING: </b></span>" . $err["errorStr"] . "<br><br>" . $err["query"] . "<br><br>";

            }

        } else
            $successMsg .= "CONSTRAINT: " . $tableName . " CREATED" . "<br>";

        // empty trace log
        $this->kxdb->emptyTraceLog();

        $successMsg .= "END TABLE FK: " . $tableName . "<br><br>";


        //SET FOREIGN KEY CHECK TO ENABLED
        $this->kxdb->setForeignKeyCheck(1);

        //check if success
        if ($errorMsg != "") $success = false;

        //return
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);




    }

    protected function importLanguages($fileName)
    {


        //set variable
        $success = true;
        $errorMsg = "";
        $successMsg = "";


        //get table
        $table = $this->K_COMMON->getPluginDataObject("k_usefuldata", "languages");

        //load xml file
        $xml = simplexml_load_file($fileName);


        //scan xml file
        foreach ($xml->language as $languageData) {

            $iso = (string)$languageData->iso;
            $name = (string)$languageData->name;
            $language = (string)$languageData->language;

            $table->putByKey(["iso" => $iso, "name" => $name, "language" => $language]);


            $successMsg .= "UPDATED LANGUAGE " . $iso . " - " . $name . "<br>";
        }


        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);
    }

    protected function importCountries($fileName)
    {

        //set variable
        $success = true;
        $errorMsg = "";
        $successMsg = "";


        //get table
        $table = $this->K_COMMON->getPluginDataObject("k_usefuldata", "country");

        //load xml file
        $xml = simplexml_load_file($fileName);


        //scan xml file
        foreach ($xml->country as $data) {

            $iso = (string)$data->iso;
            $iso3 = (string)$data->iso3;
            $numcode = (string)$data->numcode;
            $languages = (array)$data->languages;

            foreach ($languages as $language => $languageData) {


                $table->putByKey(
                    [
                        "iso" => $iso
                        , "name" => $languageData->name
                        , "printable_name" => $languageData->printable_name
                        , "iso3" => $iso3
                        , "numcode" => $numcode
                        , "language" => $language
                    ]
                );

            }




            $successMsg .= "UPDATED COUNTRY " . $iso . " - " . $iso3 . "<br>";
        }


        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);
    }

    protected function importProvince($fileName)
    {

        //set variable
        $success = true;
        $errorMsg = "";
        $successMsg = "";


        //get table
        $table = $this->K_COMMON->getPluginDataObject("k_usefuldata", "province");

        //load xml file
        $xml = simplexml_load_file($fileName);


        //scan xml file
        foreach ($xml->province as $data) {

            $iso = (string)$data->iso;
            $code = (string)$data->code;
            $languages = (array)$data->languages;

            foreach ($languages as $language => $languageData) {

                $table->putByKey(
                    [
                        "iso" => $iso
                        , "code" => $code

                        , "province" => $languageData->province
                        , "region" => $languageData->region

                        , "language" => $language
                    ]
                );

            }




            $successMsg .= "UPDATED PROVINCE " . $iso . " - " . $code . "<br>";
        }


        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);
    }

    protected function importCity($fileName)
    {

        //set variable
        $success = true;
        $errorMsg = "";
        $successMsg = "";


        //get table
        $table = $this->K_COMMON->getPluginDataObject("k_usefuldata", "city");

        //load xml file
        $xml = simplexml_load_file($fileName);


        //scan xml file
        foreach ($xml->city as $data) {

            $iso = (string)$data->iso;
            $name = (string)$data->name;
            $zip_code = (string)$data->zip_code;
            $province = (string)$data->province;
            $region = (string)$data->region;
            $language = (string)$data->language;

            $table->putByKey(
                [
                    "iso" => $iso
                    , "city" => $name
                    , "zip_code" => $zip_code

                    , "province" => $province
                    , "region" => $region

                    , "language" => $language
                ]
            );






            $successMsg .= "UPDATED CITY " . $iso . " - " . $name . "<br>";
        }


        //return
        if ($errorMsg != "")
            $success = false;
        if ($success) return array("success" => "true", "errormsg" => $errorMsg, "successmsg" => $successMsg);
        else return array("success" => "false", "errormsg" => $errorMsg, "successmsg" => $successMsg);
    }

    protected function importDataTable($ArrayVariable){

        $pluginName = $this->K_COMMON->getVarArray($ArrayVariable, "pluginName");
        $filesPlugin = [];
        $dirListPlugin = [];
        $dirToRead = $this->K_COMMON->getPathPluginObject(true, $pluginName, "");
        $dirToRead .= "/definition/datadefault";

        $this->K_COMMON->getFilesAndDir($dirToRead, $dirListPlugin, $filesPlugin);
        foreach($dirListPlugin as $pluginDir){
            $files = [];
            $dirList = [];
            $dirPluginToRead = $dirToRead."/".$pluginDir;
            $this->K_COMMON->getFilesAndDir($dirPluginToRead, $dirList, $files);
            foreach($dirList as $dirTable){
                $dirTableToRead = $dirPluginToRead."/".$dirTable;
                $xmlToLoad = $dirTableToRead."/data.xml";
                $xml = simplexml_load_file($xmlToLoad);

                $plugin = (string)$xml->pluginName;
                $table = (string)$xml->tableName;
                $tableData = $xml->dataTable->dataset;

                //include table
                $table = $this->K_COMMON->getPluginDataObject($plugin,$table);

                //read all record to save
                foreach($tableData->row as $record){
                    //set variable
                    $arrayInsert = [];

                    //read single record and create arrayInsert for the query
                    foreach($record as $key=>$field){
                        $field = (string)$field;
                        $field = htmlspecialchars_decode($field,ENT_QUOTES);
                        $arrayInsert[$key] = $field;
                    }

                    //save new record
                    $table->putByKey($arrayInsert);
                }
            }

        }
    }

}

?>