<?
namespace k_common\manage\panels\p_parameters;
class obj extends \k_webmanager\objects\general\base_panel\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * Authorization Code
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $authCode = "authManagePar";


    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "saveData":
                return json_encode($this->saveData($ArrayVariable));
                break;
            case "getModalAdd":
                return json_encode($this->getModalAdd($ArrayVariable));
                break;
            case "restoreDefault":
                return json_encode($this->restoreDefault($ArrayVariable));
                break;
            case "getTableList":
                $tableList = $this->getTableList($ArrayVariable);
                return json_encode(["success" => "true", "html" => $tableList->getHTML()]);
                break;
        }
    }

    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("list", "put", "get")
     *
     * @return object this object
     */
    public function get($ArrayVariable = array())
    {



        //set variable
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        switch ($pluginMethod) {
            case "":
            case "list":
                $this->getPageList($ArrayVariable);
                break;
            case "put":
            case "get":
                $this->getPagePut($ArrayVariable);
                break;

        }
        return $this;
    }

    protected function getPageList($ArrayVariable)
    {


        //labelInput
        $label = "Plugin";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendWO($this->lastCol, "k_usefulobjects", "web\objects\w_comboplugin"
            , [
                "labelInput" => $label
                , "withKCore" => "1"
                , "triggerEvent" => "change"
                , "triggerFunction" => "kx_tablelist_script_tableListParams.loadTableList"
            ]);


        $listMyMList = $this->getTableList($ArrayVariable);
        $this->appendHTML("", $listMyMList);
    }

    protected function getTableList($ArrayVariable)
    {



        $this->addHeadFile([$this->getPathFileCSS("list")]);

        // set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $onlyRow = $this->K_COMMON->getVarArray($ArrayVariable, "onlyRow");
        $webCode = $this->K_COMMON->getEditingWeb();

        //get object html
        $OBJTableList = $this->K_COMMON->getPluginObject("k_htmlcomponents", "tablelist");
        if ($onlyRow == "1") {


            //Load Table
            $table = $this->K_COMMON->getPluginDataObject("k_common", "parameters");

            $queryWhere = " (PRCustom . par_web_code = " . $webCode . " OR PRCustom . par_web_code is null  OR PRCustom . par_web_code ='') ";
            if ($plgID != "")
                $queryWhere .= " AND PRD.par_plugin_name = '" . $plgID . "'";
            $table->getByQuery("with_param", "", $queryWhere, ["webCode" => $webCode]);

            $dataTable = $table->getResults("array");

        } else
            $dataTable = "";

        //create table
        $OBJTableList->newTable(
            [
                "id" => "tableListParams"
                , "hideHead" => $onlyRow
                , "fieldsPageToGet" => ["webCode" => "webCode", "plgID" => "pluginName"]
                , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getTableList"]
                , "data" => $dataTable
                , "fields" =>
                [

                    [
                        "label" => $this->getDictionaryWord("LabelListParamCode")
                        , "qryField" => "code"
                        , "classIF" => ["isCustomized" => ["isCustomized" => "1"]]
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListParamDescr")
                        , "qryField" => "description"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListParamValueDefault")
                        , "qryField" => "valueDefault"
                    ]
                    ,
                    [
                        "label" => $this->getDictionaryWord("LabelListParamValueCustom")
                        , "qryField" => "valueCustom"
                    ]
                    ,
                    [
                        "width" => "130px"
                        , "buttons" =>
                            [
                                [
                                    "title" => $this->getDictionaryWord("LabelListAdd")
                                    , "action" => "editModal"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                    , "idModal" => "modalAddParam"
                                    , "showIF" => ["isCustomized" => "0"]
                                    , "icon" => "fa-edit"
                                    , "dynamicAttributes" => ["webCode" => $webCode]
                                    , "qryAttributes" => [
                                    "keyID" => "id_parameters_definition"
                                    , "plgID" => "pluginName"
                                    , "parCode" => "code"
                                    , "keyIDCustom" => "keyIDCustom"
                                    , "keyIDDefault" => "keyIDDefault"
                                ]
                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelListEdit")
                                    , "action" => "editModal"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "getModalAdd"]
                                    , "idModal" => "modalAddParam"
                                    , "showIF" => ["isCustomized" => "1"]
                                    , "dynamicAttributes" => ["webCode" => $webCode]
                                    , "qryAttributes" => [
                                    "keyID" => "id_parameters_definition"
                                    , "plgID" => "pluginName"
                                    , "parCode" => "code"
                                    , "isCustomized" => "isCustomized"
                                    , "keyIDCustom" => "keyIDCustom"
                                    , "keyIDDefault" => "keyIDDefault"
                                ]

                                ]
                                ,
                                [
                                    "title" => $this->getDictionaryWord("LabelListRestores")
                                    , "qryAttributes" => [
                                    "keyIDCustom" => "keyIDCustom"
                                ]
                                    , "action" => "remove"
                                    , "ajax" => ["kcp" => $this->getPluginName(), "kco" => $this->getPluginObjectName(), "kca" => "restoreDefault"]
                                    , "reloadOnSuccess" => "1"
                                    , "showIF" => ["isCustomized" => "1"]
                                ]
                            ]
                    ]
                ]
            ]
        );

        return $OBJTableList->get();

    }

    protected function getModalAdd($ArrayVariable)
    {

        //set variable
        $idModal = $this->K_COMMON->getVarArray($ArrayVariable, "idModal");

        //get Modal Object
        $webModal = $this->K_COMMON->getPluginObject("k_webmanager", "web_modal");

        $webModal->newModal(["id" => $idModal, "title" => $this->getDictionaryWord("LabelModalTitleAddParam"), "type" => "general"]);
        $webModal->appendHtml($webModal->modalBody, $this->getPagePut($ArrayVariable));

        //GET html Modal
        $htmlResult = $webModal->getHTMLWithCSSJS();

        //return
        return ["success" => "true", "html" => $htmlResult];


    }

    protected function getPagePut($ArrayVariable)
    {

        //set variable
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $parCode = $this->K_COMMON->getVarArray($ArrayVariable, "parCode");
        $isCustomized = $this->K_COMMON->getVarArray($ArrayVariable, "isCustomized");
        $keyIDCustom = $this->K_COMMON->getVarArray($ArrayVariable, "keyIDCustom");
        $keyIDDefault = $this->K_COMMON->getVarArray($ArrayVariable, "keyIDDefault");
        $value_code = "";
        $value_parValue = "";
        $value_parWebCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $arrayWhere  ["id_parameters"] = $keyIDDefault;
        if ($isCustomized == "1") {
            $arrayWhere  ["id_parameters"] = $keyIDCustom;
        }


        //Load Table
        $table = $this->K_COMMON->getPluginDataObject("k_common", "parameters");
        $table->get("*", $arrayWhere);
        $data = $table->fetch();
        if ($data) {
            $value_code = $data->par_code;
            $value_parValue = $data->par_value;

            if ($isCustomized == "1") $value_parWebCode = $data->par_web_code;

        }
        //create form
        $this->appendForm("",
            [
                "id" => "paramForm"
                , "setSavedMessage" => "1"
                , "kcp" => $this->namePlugin
                , "kco" => $this->namePluginObject
                , "kca" => "saveData"
                , "reloadTableList" => "tableListParams"
                , "closeModal" => "modalAddParam"
            ]
        );

        if ($isCustomized == "1") {
            //keyID hidden
            $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "keyID", "name" => "keyID", "value" => $keyIDCustom]);

        }
        //webCode hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "webCode", "name" => "webCode", "value" => $value_parWebCode]);


        //plugin
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "plugin", "name" => "plgID", "value" => $plgID]);

        //parCode
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "parCode", "name" => "paramCode", "value" => $parCode]);


        //NEW ROW
        $this->appendRow($this->lastForm);

        //param obj code
        $label = $this->getDictionaryWord("LabelFormCode");
        $name = "paramCode";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_code, "disabled" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //param is container
        $label = $this->getDictionaryWord("LabelFormValue");
        $name = "paramValue";
        $this->appendColumn($this->lastRow, ["nCol" => 12]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value_parValue]);



        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->getDictionaryWord("LabelFormSave");
        $name = "btnSave";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);

        return $this;

    }

    protected function restoreDefault($ArrayVariable)
    {

        $keyIDCustom = $this->K_COMMON->getVarArray($ArrayVariable, "keyIDCustom");
        $tableDefinition = $this->K_COMMON->getPluginDataObject("k_common", "parameters");
        $tableDefinition->delete(["id_parameters" => $keyIDCustom]);

        return array("success" => "true");
    }

    protected function saveData($ArrayVariable)
    {

        //set variable
        $keyID = $this->K_COMMON->getVarArray($ArrayVariable, "keyID");
        $plgID = $this->K_COMMON->getVarArray($ArrayVariable, "plgID");
        $paramCode = $this->K_COMMON->getVarArray($ArrayVariable, "paramCode");
        $paramValue = $this->K_COMMON->getVarArray($ArrayVariable, "paramValue");
        $paramWebCode = $this->K_COMMON->getVarArray($ArrayVariable, "webCode");
        $arrayWhere = [];
        if ($keyID != "")
            $arrayWhere["id_parameters"] = $keyID;

        //include tables
        $tableParam = $this->K_COMMON->getPluginDataObject("k_common", "parameters");


        if ($plgID == "" || $paramCode == "")
            return ["success" => "false", "reason" => "inputVoid", "message" => "Please fill in the fields!"];
        $tableParam->put(
            [
                "par_plugin_name" => $plgID
                , "par_code" => $paramCode
                , "par_value" => $paramValue
                , "par_web_code" => $paramWebCode
            ], $arrayWhere
        );
        $tableRes = $tableParam->getLastError();
        if ($tableRes["code"] == "1062") {
            return ["success" => "false", "error" => "customCodeDup", "message" => $this->getDictionaryWord("emsgCustomCodeDup")];
        }

        return ["success" => "true"];
    }


}

?>