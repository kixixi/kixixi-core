<?

/**
 * KIXIXI DB Manager
 *
 * Some parts of the code are based on the Code of Justin Vincent {@link http://php.justinvincent.com Justin Vincent}
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package KIXIXI
 * @subpackage k_dbmanager\manager
 * @since 1.00
 */

namespace k_dbmanager\objects\general\manager;
use function GuzzleHttp\default_ca_bundle;

class obj extends \k_common\objects\general\base\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * Print the query string
     *
     * @since 1.00
     * @access protected
     * @var boolean
     */
    protected $echoSQL = false;

    /**
     * Auto Hide show sql after Print the query string
     *
     * @since 1.00
     * @access protected
     * @var boolean
     */
    protected $autoHideEchoSQL = true;

    /**
     * Trace on Array all query executed
     *
     * @since 1.00
     * @access protected
     * @var boolean
     */
    protected $trace = false;

    /**
     * Trace on Array only error (except id trace = true)
     *
     * @since 1.00
     * @access protected
     * @var boolean
     */
    protected $traceError = false;

    /**
     * Array of traced Log
     *
     * @since 1.00
     * @access protected
     * @var array
     */
    protected $traceLog = [];

    /**
     * Show Errors occurred
     *
     * @since 1.00
     * @access protected
     * @var boolean
     */
    protected $showQryErrors = false;

    /**
     * Store the last error occurred
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $lastError = "";

    /**
     * Store the last error Number occurred
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $lastErrorNumber = "";

    /**
     * Query that we are executing
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $queryString = "";

    /**
     * Last query executed
     *
     * @since 1.00
     * @access protected
     * @var array
     */
    protected $lastQuery = "";


    /**
     * amount of queries executed
     *
     * @since 1.00
     * @access protected
     * @var integer
     */
    protected $numQueries = 0;
    

    /**
     * Bind Param query array
     *
     * @since 1.00
     * @access protected
     * @var array
     */
    protected $bindQueryParam = array();

    /**
     * Database Connection
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $dbCon;

    /**
     * user of DB Connection
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $dbUser = "";

    /**
     * password of DB Connection
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $dbPassword = "";

    /**
     * name of DB Connection
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $dbName = "";

    /**
     * host of DB Connection
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $dbHost = "";

    /**
     * port of DB Connection
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $dbPort = "";

    /**
     * encoding of DB Connection
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $dbEncoding = "";

    /**
     * New ID of query INSERT for each table
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $newIDByTable = [];

    /* Variables for actual environment */

    /**
     * Index of global environment
     *
     * @since 1.00
     * @access protected
     * @var int
     */
    protected $environmentGlobalIndex = 0;

    /**
     * Index of actual environment
     *
     * @since 1.00
     * @access protected
     * @var int
     */
    protected $environmentIndex = 0;

    /**
     * array of MySQL result of query executed
     *
     * @since 1.00
     * @access protected
     * @var mixed
     */
    protected $result = [];

    /**
     * Array of MySQL Actual Row Result fetched
     *
     * @since 1.00
     * @access protected
     * @var array
     */
    protected $rowResult = [];

    /**
     * array of Num Rows affected from the query
     *
     * It is used for both of select operations that insert, update etc
     *
     * @since 1.00
     * @access protected
     * @var integer
     */
    protected $numRows = [];

    /**
     * array of Num Columns affected from the query
     *
     * It is used for both of select operations that insert, update etc
     *
     * @since 1.00
     * @access protected
     * @var integer
     */
    protected $numColumns = [];

    /**
     * array of Query Time affected from the query
     *
     * It is used for both of select operations that insert, update etc
     *
     * @since 1.00
     * @access protected
     * @var integer
     */
    protected $queryTime = [];


    /**
     * ID of last inserted record
     *
     * @since 1.00
     * @access protected
     * @var integer
     */
    protected $newID = [];
    
    /**
     * table name (if we are working in a particular table)
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $workingTableName = [];

    /**
     * db connected
     *
     * @since 1.00
     * @access protected
     * @var boolean
     */
    protected $dbConnected = false;

    /**
     * db statement
     *
     * @since 1.00
     * @access public
     */
    public $dbStatement ;



    /**
     * Constructor - initialize connection variable and try to connect DB
     *
     * @since 1.00
     *
     * @param string $dbUser MySQL database user
     * @param string $dbPassword MySQL database password
     * @param string $dbName MySQL database name
     * @param string $dbHost MySQL database host
     * @param string $dbPort MySQL database Port (if omissed get from dbhost or default)
     * @param string $dbEncoding MySQL database encoding
     */
    function __construct($dbUser = "", $dbPassword = "", $dbName = "", $dbHost = "localhost", $dbPort = "", $dbEncoding = "")
    {

        //call parent
        parent::__construct();

        //Set DB connection variable
        $this->dbUser = $dbUser;
        $this->dbPassword = $dbPassword;
        $this->dbName = $dbName;
        list($this->dbHost, $this->dbPort) = $this->getHostPort($dbHost, $dbPort);
        if ($dbEncoding == "")
            $dbEncoding = "";
        $this->dbEncoding = $dbEncoding;

        //Try to connect to DB
        $this->connect();

    }

    /**
     * Split host and port from a string "host:port" .
     *
     * @param string $host MySQL database user
     * @param string $port MySQL database user
     *
     * @return array host and port.
     */
    function getHostPort($host, $port = "")
    {
        if (false !== strpos($host, ':')) {
            list($host, $port) = explode(':', $host);
            $port = (int)$port;
        } else $port = ($port == "" ? "3306" : $port);
        return array($host, $port);
    }

    /**
     * Connect do MySQL DB, using user, password, host setted before
     *
     * @param string $host MySQL database user
     *
     * @return boolean true/false if connection done or not
     */
    function connect()
    {

        // check if user setted
        if ($this->dbUser == "") {
            $this->lastError = "DB User not defined in" . __FILE__ . "  line " . __LINE__;
            $this->debug();

            return false;
        }


        try {
            $this->dbCon = new \PDO ("mysql:host=".$this->dbHost.";port=".$this->dbPort.";dbname=" . $this->dbName, $this->dbUser, $this->dbPassword);
            $this->dbConnected = true;
        } catch (PDOException $e) {
            $this->debug();
            $this->lastError = $e->getMessage();
            $this->dbConnected = false;
            return false;
        }


        //select DB
        $this->selectDB($this->dbName, $this->dbEncoding);


        //Connection Success
        return true;

    }



    /**
     *    Convert a query parameter to number of parameter specified
     *
     *
     * @since 1.0
     *
     * @param string $v reference parameter of string variable
     * @param string $k key name
     * @param string $prefix prefix to be added at key
     *
     */

    protected function arrayParamConvert(&$v, $k, $prefix){
        $v = $prefix . "_" . $k;
    }

    /**
     *    Prepare a query for safe execution
     *
     *    You can bind parameter or leave empty and bind it later.
     *
     *    The principal difference between php mysql prepare is that you can use mnemonic placeholder
     *
     *    **Example #1**<br>
     *    *in $bindParam pass an array with code of placeholder and the value (code => value)*<br>
     *    *where type is not specified it is considerd as string*<br>
     *
     *    kxdb::prepare( <br>
     *                    "SELECT * FROM `table` WHERE `field1` = :code AND `field2` = :year"<br>
     *                    , array( 'code' => '100', 'year' => '2016' ) <br>
     *                );
     *
     *    **Example #2**<br>
     *    in $bindParam pass an array with code of placeholder and the value as array with type of value (code => array( type => value ) )
     *
     *    kxdb::prepare( <br>
     *                    "SELECT * FROM `table` WHERE `field1` = :code"<br>
     *                    , array( 'code' => array('s' => '100') )<br>
     *                );
     *
     *    **Example #3**<br>
     *    leave blank $bindParam and after use bindParam method for add parameters
     *
     *    kxdb::prepare( "SELECT * FROM `table` WHERE `field1` = :code");
     *
     * @since 1.0
     *
     * @param string $query Query statement
     * @param array $bindParam Array of variables to substitute into the query's placeholders<br>
     * The following directives can be used in the values type parameters:<br>
     *    *    kx_sql for insert sql, for example "NOW()"
     *    *    kx_type_string (string)
     *    *    kx_type_number (number)
     *
     * *where type is not specified it is considered as string*
     *
     * **Use of bind**
     *
     * **Example of bind**
     * <pre>
     *        array(
     *            "code" => array ( "kx_type_string" => "100" )
     *            ,"year" => array ( "kx_type_number" => "2016" )
     *            ,"language" => array ( "kx_type_string" => array ("EN", "IT") )
     *            ,"sex" => "m"
     *        );
     *
     * </pre>
     *
     *
     * **Example of bind with sql function**
     * <pre>
     *        array(
     *            "code" => "100"
     *            ,"creationdate" => ["kx_sql" => "NOW()"]
     *        );
     *
     * </pre>

     *
     * @return string|void query string, if there is a query to prepare.
     */
    public function prepare($query, $bindParam = array())
    {


        //TODO: Devo poter dire che tipo di valore sto inserirendo, numerico o stringa


        $this->bindQueryParam = [];


        //If array replace placeholder of parameter to multi parameters
        foreach ($bindParam as $k => $v){

            if (is_array($v))
            {

                if (!empty($v)){

                    $isArrayofValues = true;

                    //Check if there is "sql" element, in this case is not an array of values
                    if (count($v) == 1)
                    {

                        //$this->K_COMMON->printArray($v);
                        //echo "<br> V[0] = ".key($v)." ***";
                        //echo "<br> V[CURRENT] = ".current($v)." ***";

                        if (!is_numeric(key($v)))
                          switch (key($v)){
                            case "kx_sql":
                                $query = str_replace(":" . $k, $v["kx_sql"], $query);
                                unset($bindParam[$k]);

                               // echo "<br> NEW QUERY = $query ***";

                                break;
                            case "kx_type_string":

                                if (is_array(current($v)))
                                    $v = current($v);
                                $isArrayofValues = true;

                                break;
                            case "kx_type_number":

                                if (is_array(current($v)))
                                    $v = current($v);
                                $isArrayofValues = true;

                                break;
                            default:
                                //echo "<br> V[CURRENT] = ".current($v)." ***";
                                $isArrayofValues = true;
                                break;
                        }




                    }

                    if ($isArrayofValues) {

                        //echo "<br>SONO QUI ********";


                        array_walk($v, array($this, 'arrayParamConvert'), ':' . $k);
                        $inQuery = implode(',', $v);

                        $query = preg_replace('/:\b' . $k . '\b/', $inQuery, $query);

                        //echo "<br> LA QUERY DOPO = $query *** $inQuery ***";

                    }



                }



            }

        }



        //Prepare Query
        $this->dbStatement = $this->dbCon->prepare($query);




        //Create Bind Value for each parameter
        foreach ($bindParam as $k => $v){

//            echo "<br>KEY $k";
            if (is_array($v))
            {

                if (!empty($v)){

                  //echo "<br> QUERY: $query ***";


                    $isArrayofValues = true;

                    //Check if there is "sql" element, in this case is not an array of values
                    if (count($v) == 1)
                    {

                        //$this->K_COMMON->printArray($v);
                        //echo "<br> V[0] = ".key($v)." ***";

                        if (!is_numeric(key($v)))
                            switch (key($v)){
                                 case "kx_type_string":

                                     if (is_array(current($v)))
                                         $v = current($v);
                                    $isArrayofValues = true;

                                    break;
                                case "kx_type_number":

                                    if (is_array(current($v)))
                                        $v = current($v);
                                    $isArrayofValues = true;

                                    break;
                                default:
                                    $isArrayofValues = true;
                            }




                    }

                    if ($isArrayofValues)
                    {
                        foreach ($v as $vKey => $vValue){


                            //echo "<br>VALORE1 $vKey - $vValue *";

                            $this->dbStatement->bindValue(':' . $k . '_' . $vKey, $vValue, \PDO::PARAM_STR);
                            $this->bindQueryParam[$k . '_' . $vKey] = $vValue;
                        }
                    }
                }
                else
                {
                    $v = ''; //it is array but empty
                    //echo "<br>VALORE2 $k - $v *";
                    $this->dbStatement->bindValue(':' . $k, $v, \PDO::PARAM_STR);
                    $this->bindQueryParam[$k] = $v;
                }



            }
            else
            {
                $this->dbStatement->bindValue(':' . $k, $v, \PDO::PARAM_STR);
                $this->bindQueryParam[$k] = $v;

            }



        }



    }


    /**
     * Replaces any parameter placeholders in a query with the value of that
     * parameter. Useful for debugging. Assumes anonymous parameters from
     * $params are are in the same order as specified in $query
     *
     * @param string $query The sql query with parameter placeholders
     * @param array $params The array of substitution parameters
     * @return string The interpolated query
     */
    public function interpolateQuery($query, $params) {
        $keys = array();
        $values = $params;

        # build a regular expression for each parameter
        foreach ($params as $key => $value) {
            if (is_string($key)) {
                $keys[] = '/:'.$key.'/';
            } else {
                $keys[] = '/[?]/';
            }

            if (is_string($value))
                $values[$key] = "'" . $value . "'";

            if (is_array($value))
                $values[$key] = "'" . implode("','", $value) . "'";

            if (is_null($value))
                $values[$key] = 'NULL';
        }

        $query = preg_replace($keys, $values, $query);

        return $query;
    }



    /**
     * Execute a query created with prepare
     *
     * @since 1.00
     *
     * @return false|int false if failure or num rows affected
     *
     */
    public function execute()
    {
        return $this->query();
    }

    /**
     * Returns the current row of a result set as an object
     *
     * @since 1.00
     *
     * @return object Returns an object with string properties that corresponds to the fetched row or NULL if there are no more rows in resultset.
     */
    public function fetch()
    {

        if ($this->lastError == "")
            $this->rowResult[$this->environmentIndex] = $this->result[$this->environmentIndex]->fetch(\PDO::FETCH_LAZY );
        else
        {
            $this->rowResult[$this->environmentIndex] = null;
        }

        return $this->rowResult[$this->environmentIndex];
    }



    /**
     * Returns a value of a column of the row fetched
     *
     * @param string $colName name of column
     * @since 1.00
     *
     * @return string Returns value of column
     */
    public function getValue($colName)
    {


        return $this->rowResult[$this->environmentIndex]->$colName;
    }

    /**
     * flush cached data
     *
     * @since 1.00
     *
     */
    protected function flush()
    {

        $this->lastError = "";
        $this->lastQuery = "";
        $this->newID[$this->environmentIndex] = 0;
        $this->lastQuery = "";
        $this->result[$this->environmentIndex] = null;
        $this->rowResult[$this->environmentIndex] = null;

        //free query result
        if (!$this->result[$this->environmentIndex] === null)
            $this->result[$this->environmentIndex]->free_result();

        $this->dbStatement->closeCursor();
    }

    /**
     * Increase and return queries executed
     *
     * @return int Returns query count
     */

    protected function countQueries()
    {
        $this->numQueries++;
        return $this->numQueries;
    }

    /**
     * Execute a MySQL query, using current database connection.
     *
     * @since 1.00
     *
     * @param string $query Database query
     * @return int|false Number of affected rows or false on error
     */

    public function query($query = "")
    {

        // Flush
        $this->flush();

        //Prepare Query
        if ($query != "")
            $this->dbStatement = $this->dbCon->prepare($query);

        // Keep track of the last query for debug
        $this->lastQuery = $this->dbStatement->queryString;

        // count queries
        $this->countQueries();

        // Check if there is a db active connection
        if (!isset($this->dbCon) || !$this->dbCon)
            return false;


        //echo SQL
        $this->echoQry();

        //Calculate Query Time:  start time
        $queryStartTime = microtime(true);


        //set environment variables
        $this->numColumns[$this->environmentIndex] = 0;

        // Execute the query
        if (preg_match("/^(insert|delete|update|replace)/i", $this->dbStatement->queryString)) {



            try {

                $this->dbCon->beginTransaction();


                $this->dbStatement->execute();


                // store the new id
                if (preg_match("/^(insert|replace)\s+/i", $this->dbStatement->queryString)) {

                    $lid = $this->dbCon->lastInsertId();

                    $this->newID[$this->environmentIndex] = $lid;
                    $this->newIDByTable[$this->workingTableName[$this->environmentIndex]][] = $this->newID[$this->environmentIndex];

                }


                $this->dbCon->commit();


                // get and store number rows affected
                $this->numRows[$this->environmentIndex] = $this->dbStatement->rowCount();




            } catch(PDOExecption $e) {

                $this->dbCon->rollback();
                print "<br>Error!: " . $e->getMessage() . "</br>";

            }




        }
        elseif (preg_match("/^(create|alter|truncate|drop)/i", $this->dbStatement->queryString)) {

            $this->dbStatement->execute();

            // get and store number rows affected
            $this->numRows[$this->environmentIndex] = $this->dbStatement->rowCount();


        } else // Query was a select
        {

            try {
                $this->dbStatement->execute();

                // get and store number rows affected
                $this->numRows[$this->environmentIndex] = $this->dbStatement->rowCount();
                $this->numColumns[$this->environmentIndex] = $this->dbStatement->columnCount();

            } catch(PDOExecption $e) {

                $this->dbCon->rollback();
                print "<br>Error!: " . $e->getMessage() . "</br>";

            }




        }



        //Insert db statement in the array
        $this->result[$this->environmentIndex] = $this->dbStatement;


        //Calculate Query Time:  start time
        $queryEndTime = microtime(true);

        //Calculate Query Time:  Difference
        $this->queryTime[$this->environmentIndex]= $queryEndTime - $queryStartTime;

        if ($this->echoSQL)
        {
            echo '<br><b>Query Time:</b>  ' . number_format($this->queryTime[$this->environmentIndex], 10) . '</br><br>';
            echo '<hr noshade color=dddddd size=1>';
        }

        // Check for error
        if ($this->dbStatement->errorCode() > 0) {

            $this->lastError = $this->dbStatement->errorInfo();
            $this->lastErrorNumber = $this->dbStatement->errorCode();
            $this->debug();
            return false;
        } else
            $this->debug();


        //Check if show SQL is enabled only for 1 time
        if ($this->autoHideEchoSQL)
            $this->hideSQL();

        return $this->numRows[$this->environmentIndex];

    }

    /**
     * Print actual query if echoQry is true
     *
     * @param boolean $echoForce true to force echo
     *
     * @since 1.00
     *
     */
    protected function echoQry($echoForce = false)
    {
        // check if error
        if ($this->echoSQL || $echoForce) {

            $q = $this->interpolateQuery($this->lastQuery, $this->bindQueryParam);

            echo '<br><font color="" face="arial" size="3"><b>dbManager</b> (ver ' . $this->ver . ') - <b>envID</b> ' . $this->environmentIndex . ' - <b>Debug Mode Start</b></font></br>';
            echo '<br><font color="000000"><b>QUERY CON PARAMETRI</b></font><br>';
            echo '[<font color="000000"><b>' . $this->lastQuery . '</b></font>]</font><p>';

            echo '<br><font color="000000"><b>QUERY CON VALORI</b></font><br>';
            echo '[<font color="000000"><b>' . $q . '</b></font>]</font><p>';

            //$this->K_COMMON->printArray($this->bindQueryParam);
            echo '<pre>' ,var_dump($this->bindQueryParam), '</pre>';
            $this->dbStatement->debugDumpParams();

            echo '</blockquote><hr noshade color=dddddd size=1>';
            echo '<br><font color="" face="arial" size="3"><b>dbManager</b> (ver ' . $this->ver . ') - <b>envID</b> ' . $this->environmentIndex . ' - <b>Debug Mode End</b></font></br>';


        }

    }

    /**
     * Show error if showQryErrors = true
     *
     * @since 1.00
     *
     */
    protected function debug()
    {


        // check if error
        if ($this->lastError != "")
            $this->showQryErrors ? trigger_error($this->lastError, E_USER_WARNING) : null;



        //register Log
        $this->registerLog();

    }

    /**
     * Register a log in traceLog array
     *
     * @param string $e optional error description
     * @param string $q optional Database query
     *
     * @since 1.00
     *
     */
    protected function registerLog($e = "", $q = "")
    {
        if ($e == "")
        {
            $e = $this->lastError;
            $q = $this->lastQuery;

        }

        // insert on traceLog array
        if (($this->trace) || (($this->traceError) && (!empty($e))))
            $this->traceLog[] = array
            (
                'errorStr' => $e[2],
                'query' => $q
            );
    }

    /**
     * Empty trace Log
     *
     * @since 1.00
     *
     */
    public function emptyTraceLog()
    {
        $this->traceLog = [];
    }


    /**
     * Select DB
     *
     * @since 1.00
     *
     * @param string $dbName Database name
     * @param string $dbEncoding optional Database encoding
     *
     * @return true|false on successful o failure
     */
    public function selectDB($dbName = '', $dbEncoding = '')
    {

        try
        {

            $this->dbCon->exec("USE " . $dbName );
        }
        catch(PDOException $e)
        {
            $this->lastError = $e->getMessage();
            $this->debug();

            return false;
        }


        $this->dbName = $dbName;

        if ($dbEncoding != '') {
            $this->dbCon->exec("SET NAMES '" . $dbEncoding . "'");
            $this->dbEncoding = $dbEncoding;
        }



    }

    /**
     * Set Foreign Key Check
     *
     * @since 1.00
     * @param string $status 1 or 0
     *
     * @return true|false
     */
    public function setForeignKeyCheck($status)
    {

        $this->prepare("SET foreign_key_checks = ".$status.";");
        $this->execute();


    }

    /**
     * Check if table exist on DB
     *
     * @since 1.00
     * @param string $tableName table name to check
     *
     * @return true|false
     */
    public function checkTableExist($tableName)
    {

        $this->prepare("SHOW TABLES LIKE :tableName", array("tableName" => $tableName));
        $res = $this->execute();

        if ($res > 0)
            return true;
        else
            return false;

    }

    /**
     * Check if table exist on DB
     *
     * @since 1.00
     * @param string $tableName table name - use like to find it (empty for all)
     *
     * @return object
     */
    public function loadTableList($tableName)
    {

        //set variables
        $where = "";
        if ($tableName != "")
            $where .= " AND TABLE_NAME like '".$tableName."%'";

        $strSQL = "select TABLE_NAME from information_schema.TABLES "
            ." WHERE TABLE_SCHEMA = DATABASE() "
            . $where;

        $this->prepare($strSQL);
        $this->execute();

        return $this;

    }

    /**
     * Check if table exist on DB
     *
     * @since 1.00
     * @param string $tableName table name - use like to find it (empty for all)
     *
     * @return object
     */
    public function loadTablesWithCustomFields($prefixName)
    {

        //set variables
        $where = "";

        $strSQL = "SELECT TABLE_NAME FROM information_schema.columns "
            ." WHERE TABLE_SCHEMA = DATABASE() AND COLUMN_NAME like 'zkx_".$prefixName."%'"
            ." GROUP BY TABLE_NAME "
            . $where;

        $this->prepare($strSQL);
        $this->execute();

        return $this;

    }

    /**
     * get Table Defintion
     *
     * @since 1.00
     * @param string $tableName table name to check
     * @param string $fieldName optional field to get
     *
     * @return integer
     */
    public function loadTableDefinition($tableName, $fieldName = "")
    {

        $where = "";
        if ($fieldName != "")
            $where = " like '".$fieldName."'";

        $this->prepare(
            "SHOW FULL COLUMNS FROM " . $tableName . " " . $where
        );

        $res = $this->execute();


        return $res;
    }

    /**
     * get Trigger Defintion
     *
     * @since 1.00
     * @param string $triggerName trigger name to check
     *
     * @return object
     */
    public function loadTriggerDefinition($triggerName)
    {

        //set variables
        $where = "";
        if ($triggerName != "")
            $where .= " AND TRIGGER_NAME = '".$triggerName."'";

        $strSQL = "SELECT TRIGGER_NAME, EVENT_OBJECT_TABLE, ACTION_TIMING , EVENT_MANIPULATION, ACTION_STATEMENT "
                    ."FROM information_schema.triggers"
                    ." WHERE TRIGGER_SCHEMA = DATABASE() " . $where;
        $this->prepare($strSQL);
        $res = $this->execute();


        return $this;
    }

    /**
     * get List of Triggers
     *
     * @since 1.00
     * @param string $tableName table name to get (if empty get all triggers)
     *
     * @return object
     */
    public function loadTriggerList($tableName)
    {

        //set variables
        $where = "";
        if ($tableName != "")
            $where .= " AND EVENT_OBJECT_TABLE like '".$tableName."%'";

        $strSQL = "SELECT TRIGGER_NAME, EVENT_OBJECT_TABLE, ACTION_TIMING , EVENT_MANIPULATION, ACTION_STATEMENT "
            ."FROM information_schema.triggers"
            ." WHERE TRIGGER_SCHEMA = DATABASE() "
            .$where ;
        $this->prepare($strSQL);
        $res = $this->execute();


        return $this;
    }

    /**
     * get List of Triggers
     *
     * @since 1.00
     * @param string $startName start name of event (if empty get all events)
     *
     * @return object
     */
    public function loadEventsList($startName)
    {

        //set variables
        $where = "";
        if ($startName != "")
            $where .= " AND EVENT_NAME like '".$startName."%'";

        $strSQL = "SELECT EVENT_NAME, EVENT_DEFINITION, INTERVAL_VALUE, INTERVAL_FIELD, STARTS "
            ."FROM information_schema.events"
            ." WHERE EVENT_SCHEMA = DATABASE() "
            .$where ;



        $this->prepare($strSQL);
        $res = $this->execute();


        return $this;
    }

    /**
     * get Table Index Defintion
     *
     * @since 1.00
     * @param string $tableName table name to check
     * @param string $indexName table name to check
     *
     * @return integer
     */
    public function loadIndexDefinition($tableName, $indexName = "")
    {
        $strSQL = "SHOW INDEX FROM ".$tableName."";

        if ($indexName != "")
            $strSQL .= " WHERE KEY_NAME = '".$indexName."'";

        $this->prepare($strSQL);
        $res = $this->execute();


        return $res;
    }

    /**
     * get Table FK Defintion
     *
     * @since 1.00
     * @param string $tableName table name to check
     * @param string $constraintName constraint Name to get
     *
     * @return integer
     */
    public function loadForeignKeyDefinition($tableName, $constraintName = "")
    {
        $strSQL = "SELECT KC.*, UPDATE_RULE, DELETE_RULE "
            . " FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE KC"
            . " INNER JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS RC ON RC.CONSTRAINT_SCHEMA = KC.CONSTRAINT_SCHEMA AND RC.CONSTRAINT_NAME = KC.CONSTRAINT_NAME"
            ." WHERE KC. TABLE_NAME = '".$tableName."' and KC.referenced_table_name is not null";

        if ($constraintName != "")
            $strSQL .= " AND KC.CONSTRAINT_NAME='".$constraintName."'";

        $this->prepare($strSQL);
        $res = $this->execute();


        return $res;
    }

    /**
     * get SP Definition
     *
     * @since 1.00
     * @param string $name table name to check
     *
     * @return integer
     */
    public function getSPDefinition($name)
    {

        $strSQL = "SELECT * FROM information_schema.ROUTINES
											 WHERE ROUTINE_SCHEMA = DATABASE()
											 AND ROUTINE_NAME   = '" . $name . "'";

        $this->prepare($strSQL);
        $this->execute();
        $type = "";
        $parameters = "";
        $statement = "";
        $returnData = "";


        if ($data = $this->fetch()) {

            //set values
            $type = $data->ROUTINE_TYPE;
            $statement = $data->ROUTINE_DEFINITION;

            //read data parameters
            $strSQL = "SELECT * FROM information_schema.PARAMETERS
											 WHERE SPECIFIC_SCHEMA = DATABASE()
											 AND SPECIFIC_NAME   = '" . $name . "'
											 ORDER BY ORDINAL_POSITION";

            $this->prepare($strSQL);
            $this->execute();

            while ($dataPar = $this->fetch()) {
                if ($dataPar -> PARAMETER_MODE == "")
                {
                    $returnData = $dataPar->DTD_IDENTIFIER;
                }elseif ($dataPar -> PARAMETER_MODE == "IN")
                {
                    $parValue = $dataPar->PARAMETER_NAME . " " . $dataPar->DTD_IDENTIFIER;
                    if ($parameters == "")
                        $parameters = $parValue;
                    else
                        $parameters .= ", " . $parValue;
                }

            }
        }

        $dataReturn =
            [
                "type" => $type
                ,"statement" => $statement
                ,"parameters" => $parameters
                , "return" => $returnData
            ];


        return $dataReturn;


    }


    /**
     * Create SQL String for create/update a field on table
     *
     * @since 1.00
     *
     * @param string $fieldNameOld name of field old (empty for add field)
     * @param string $fieldNameNew name of field new

     * @param array $fieldParam array
     * See the createTable Function for param rules
     * @param string $afterField name of field after to
     *
     * @return string
     */
    protected function getCreateFieldString($fieldNameOld, $fieldNameNew, $fieldParam, $afterField = "")
    {
        // get Parametes Field
        $type = (isset($fieldParam["Type"])) ? $fieldParam["Type"] : "";
        $len = (isset($fieldParam["Len"])) ? $fieldParam["Len"] : "";
        $default = (isset($fieldParam["Default"])) ? $fieldParam["Default"] : "";
        $comment = (isset($fieldParam["Comment"])) ? $fieldParam["Comment"] : "";
        $null = (isset($fieldParam["NULL"])) ? $fieldParam["NULL"] : "";
        $extra = (isset($fieldParam["extra"])) ? $fieldParam["extra"] : "";
        $autoincrement = (isset($fieldParam["AUTOINCREMENT"])) ? $fieldParam["AUTOINCREMENT"] : "";
        $pk = (isset($fieldParam["PK"])) ? $fieldParam["PK"] : null;

        // check if it may be a primary key
        if ($pk === null) {
            if (substr($fieldNameNew, 0, 3) == "id_")
                if ($autoincrement == 1)
                    $pk = 1;
        }

        // create the Field string
        $fieldUpdateString =
            $fieldNameOld . " "
            . $fieldNameNew . " "
            . $type
            . ($len != "" ? "(" . $len . ") " : "")
            . ($null == 0 || $null == "NO" ? " NOT NULL " : "")
            . ($default == "" ? " DEFAULT NULL " : " default '" . $default . "'")
            . ($autoincrement == "1" ? " AUTO_INCREMENT " : "")
            . $extra
            . ($pk == "1" ? " PRIMARY KEY " : "")
            . ($afterField != "" ? " AFTER " . $afterField : "")
            . ($comment != "" ? " COMMENT '"  . str_replace("'","\'", $comment) . "'": "");

        return $fieldUpdateString;

    }


    /**
     * Add or Modify Field on Table
     *
     * @since 1.00
     *
     * @param array $tableName name of the table
     * @param string $fieldNameOld name of field old (empty for add field)
     * @param string $fieldNameNew name of field new
     * @param array $fieldParam array
     * @param string $afterField name of field after to
     *
     */
    public function tableAddField($tableName, $fieldNameOld, $fieldNameNew, $fieldParam, $afterField = "")
    {

        $fieldUpdateString = $this->getCreateFieldString($fieldNameOld, $fieldNameNew, $fieldParam, $afterField);

        if ($fieldNameOld != "")
        {
            $strSQL = "ALTER TABLE " . $tableName . " CHANGE COLUMN "  . $fieldUpdateString;
        }
        else
        {
            $strSQL = "ALTER TABLE " . $tableName . " ADD COLUMN " . $fieldUpdateString;
        }




        // execute query for create/change field
        $this->prepare($strSQL);
        $this->execute();

    }



    /**
     * Create Table on DB (or update if exist)
     *
     * @since 1.00
     * @param array $tableName name of the table
     * @param array $tableFields array of field definition
     *
     * **Fields array definition**<br><br>
     * <pre>
     * array(
     *        'field_name1' => array(
     *                            "Type"=>"VARCHAR|INT|others DB supported"
     *                            ,"Len"=>"LENGTH"
     *                            ,"Default"=>"DEFAULT VALUE"
     *                            ,"NULL"=>0|1
     *                            ,"AUTOINCREMENT"=>"0|1"
     *                            ,"PK"=>"0|1"
     *                        )
     *        ,'fiedl_name2' => array(
     *                            "Type"=>"VARCHAR|INT|others DB supported"
     *                            ,"Len"=>"LENGTH"
     *                            ,"Default"=>"DEFAULT VALUE"
     *                            ,"NULL"=>0|1
     *                            ,"AUTOINCREMENT"=>"0|1"
     *                            ,"PK"=>"0|1"
     *                        )
     * );
     * </pre>
     *
     * You can specify only valued parameters
     *
     * PRIMARY KEY: if a field begin with "id_" (ex: id_user) and it is AUTO INCREMENT will be set as PRIMARY KEY, if not specified as not PK.
     *
     * The array of each field support this values<br>
     *
     *    * Type    : the type of field supported from DB (INT, VARCHAR etc)
     *    * Len    : the length of field
     *    * Default    : the default value. If empty is set to NULL
     *    * NULL    :    1 if accept NULL value, 0 otherwise
     *    * AUTOINCREMENT    : 1 the field is AUTO INCREMENT, 0 no
     *
     * **Example**<br><br>
     * <pre>
     * array(
     *        'id' => array("Type"=>"INT","Len"=>"11","Default"=>"","NULL"=>0,"AUTOINCREMENT"=>"1")
     *        ,'code' => array("Type"=>"VARCHAR","Len"=>"50","Default"=>"","NULL"=>1,"AUTOINCREMENT"=>"0")
     * );
     * </pre>
     *
     * @param boolean $removeOldFields if true remove from actual db table the fields removed on definition
     * @param boolean $isCustomTable if true put "zkx_" before field
     *
     */
    public function createTable($tableName, $tableFields, $removeOldFields = false, $isCustomTable = false)
    {

        //set variables
        $customTable = $isCustomTable;
        $fieldBeforeCreationDate = "";

        //check if table exist
        $tableExist = $this->checkTableExist($tableName);
        $actualTableFields = array();

        if ($tableExist) //table exist
        {
            // get actual Fields on Table
            $this->prepare("SHOW COLUMNS FROM " . $tableName);
            $this->execute();

            //prepare array with actual fields
            $beforeField = "";
            while ($row = $this->fetch()) {
                //set field on array
                $actualTableFields[$row->Field] = array("name" => $row->Field, "key" => $row->Key);

                //search the field before "creationdate"
                if ($row->Field == "creationdate")
                    $fieldBeforeCreationDate = $beforeField;
                $beforeField = $row->Field;
            }

            // remove field deleted from actual table
            if (($removeOldFields) && (!$customTable)) {
                //scan actual table fields
                foreach ($actualTableFields as $fieldName => $actFieldParam) {
                    // if field on actual db table not exist on table definition delete it (except for customized field)
                    if ((!isset($tableFields[$fieldName])) && (substr($fieldName, 0, 4) != "zkx_")) {

                        //Drop Field on Table
                        $this->prepare("ALTER TABLE " . $tableName . " DROP  " . $fieldName);
                        $this->execute();

                    }

                }
            }

            // add/update fields
            $previousField = "";
            foreach ($tableFields as $fieldName => $fieldParam) {

                //check if field exist on table
                if (isset($actualTableFields[$fieldName])) { //the field exist on table

                    // create field update sql string
                    if ($actualTableFields[$fieldName]["key"] == "PRI")
                        $fieldParam["PK"] = 0;


                    $fieldUpdateString = $this->getCreateFieldString($fieldName, $fieldName, $fieldParam);

                    //create SQL
                    $strSQL = "ALTER TABLE " . $tableName . " CHANGE COLUMN " . $fieldUpdateString;

                } else { //the field not exist on table

                    // create field update sql string
                    $fieldUpdateString = $this->getCreateFieldString("", $fieldName, $fieldParam);

                    //create SQL
                    $strSQL = "ALTER TABLE " . $tableName . " ADD COLUMN " . $fieldUpdateString;

                    if (($previousField != "") && (!(substr($fieldName, 0, 4) == "zkx_")))
                        $strSQL .= " AFTER " . $previousField;
                    elseif ($customTable)
                        $strSQL .= " AFTER " . $fieldBeforeCreationDate;
                }


                // execute query for create/change field
                $this->prepare($strSQL);
                $this->execute();


                // set previous field
                $previousField = $fieldName;

            }


        } else { //table not exist

            //if custom table can't customize before exist table
            if ($customTable) {
                $this->lastError = "Can't Customize Table";
                $this->lastQuery = "";
                $this->registerLog();
            }

            //set variable
            $fieldUpdateString = "";


            //prepare string of fields
            foreach ($tableFields as $fieldName => $fieldParam) {

                // create field update sql string
                $fieldUpdateString .= $this->getCreateFieldString("", $fieldName, $fieldParam) . ",";

            }

            //remove last comma
            $fieldUpdateString = substr($fieldUpdateString, 0, -1);

            //execute query for create table
            $this->prepare("CREATE TABLE " . $tableName . " ( " . $fieldUpdateString . ")");
            $this->execute();
        }


    }


    /**
     * Create TABLE INDEX
     *
     * @since 1.00
     * @param array $tableKeys array of field definition
     * The following directives can be used in the tableKeys variable:<br>
     *    *    table: name of table
     *    *    name: name of Index - "PRIMARY" for Primary Key
     *    *    type: BTREE or FULLTEXT
     *    *    columns: string of columns of index
     *    *    unique: 1: unique, 0: normal
     * @param string $prefix if you want a prefix an all keys
     *
     */
    public function createTableIndexKey($tableKeys, $prefix = "")
    {

        /**
             * Example of Index Creation
             *
             *
             * CREATE  UNIQUE INDEX index_name ON table_name(columns)
             * CREATE  INDEX index_name ON table_name(columns)
             * CREATE  FULLTEXT INDEX index_name ON table_name(columns)
             * ALTER TABLE table_name ADD PRIMARY KEY(columns);
             *
             *
         */

        // scan array to find key
        foreach ($tableKeys as $data) {


            $table = $this->K_COMMON->getVarArray($data, "table");
            $name = $this->K_COMMON->getVarArray($data, "name");
            $type = $this->K_COMMON->getVarArray($data, "type");
            $columns = $this->K_COMMON->getVarArray($data, "columns");
            $unique = $this->K_COMMON->getVarArray($data, "unique");
            $columns = str_replace(",","`,`", $columns);

            //add Prefix
            $name = $prefix . $name;


            // check key type
            if ($unique == 1){ //unique key
                $strSQL = "CREATE  UNIQUE INDEX `" . $name . "` ON " . $table . "(`" . $columns . "`)";
            }
             else //generic key
            {

                if ($name == "PRIMARY")
                    $strSQL = "ALTER TABLE " . $table . " ADD  PRIMARY KEY(`" . $columns . "`)";
                else {

                    if ($type == "FULLTEXT")
                        $strSQL = "CREATE  FULLTEXT INDEX `" . $name . "` ON " . $table . "(`" . $columns . "`)";
                    else
                        $strSQL = "CREATE  INDEX `" . $name . "` ON " . $table . "(`" . $columns . "`)";

                }



            }

            // execute query for create/change field
            $this->prepare($strSQL);
            $this->execute();



        }



    }

    /**
     * Create TABLE FOREIGN KEY
     *
     * @since 1.00
     * @param array $tableKeys array of field definition
     * The following directives can be used in the tableKeys variable:<br>
     *    *    name: name of Foreign Key
     *    *    table: name of table
     *    *    fields: string of columns of foreign key
     *    *    refTable: name of table reference
     *    *    refFields: string of columns on table reference
     *    *    onDelete: "CASCADE", "NO ACTION", "RESTRICT, "SET NULL"
     *    *    onUpdate: "CASCADE", "NO ACTION", "RESTRICT, "SET NULL"
     * @param string $prefix if you want a prefix an all keys
     *
     */
    public function createTableForeignKey($tableKeys, $prefix = "")
    {


        // scan array to find key
        foreach ($tableKeys as $data) {


            $name = $this->K_COMMON->getVarArray($data, "name");
            $table = $this->K_COMMON->getVarArray($data, "table");
            $fields = $this->K_COMMON->getVarArray($data, "fields");
            $refTable = $this->K_COMMON->getVarArray($data, "refTable");
            $refFields = $this->K_COMMON->getVarArray($data, "refFields");
            $onDelete = $this->K_COMMON->getVarArray($data, "onDelete");
            $onUpdate = $this->K_COMMON->getVarArray($data, "onUpdate");
            $fields = str_replace(",","`,`", $fields);
            $refFields = str_replace(",","`,`", $refFields);

            //add Prefix
            $name = $prefix . $name;

            //create SQL Query
            $strSQL = "ALTER TABLE " . $table . " ADD CONSTRAINT `" . $name . "` FOREIGN KEY (`" . $fields . "`) REFERENCES " . $refTable . " (`" . $refFields . "`) ON DELETE " . $onDelete . " ON UPDATE " . $onUpdate ;

            // execute query for create/change field
            $this->prepare($strSQL);
            $this->execute();



        }



    }


    /**
     * Remove All Foreign Keys From a Table
     *
     * @since 1.00
     *
     * @param array $tableName name of the table
     * @param array $keyPrefix prefix of key to remove
     *
     */
    public function removeTableAllFKeys($tableName, $keyPrefix = "")
    {


        $this->loadForeignKeyDefinition($tableName);
        $names = [];
        while ($data = $this->fetch()){
                $name = $data->CONSTRAINT_NAME;
                if ($keyPrefix != "")
                {
                    if (substr($name, 0, strlen($keyPrefix)) == $keyPrefix)
                        $names[] = $name;
                }
                else
                    $names[] = $name;
        }


        foreach ($names as $name)
        {
            $strSQL = "ALTER TABLE `".$tableName."` DROP FOREIGN KEY `".$name."`;";
            $this->prepare($strSQL);
            $this->execute();
        }


    }

    /**
     * Remove All Foreign Indexes From a Table
     *
     * @since 1.00
     *
     * @param array $tableName name of the table
     * @param array $keyPrefix prefix of key to remove
     *
     */
    public function removeTableAllIndexes($tableName, $keyPrefix = "")
    {


        $this->loadIndexDefinition($tableName);
        $names = [];
        while ($data = $this->fetch()){

            if ($data->Key_name != "PRIMARY")
            {
                $name = $data->Key_name;
                if ($keyPrefix != "")
                {
                    if (substr($name, 0, strlen($keyPrefix)) == $keyPrefix)
                        $names[] = $name;
                }
                else
                    $names[] = $name;
            }

        }

        foreach ($names as $name)
        {
            $strSQL = "ALTER TABLE `".$tableName."` DROP INDEX `".$name."`;";
            $this->prepare($strSQL);
            $this->execute();
        }





    }

    /**
     * Remove Foreign Keys From a Table
     *
     * @since 1.00
     *
     * @param array $tableName name of the table
     * @param array $key string|array of key to be romoved
     *
     */
    public function removeTableFKey($tableName, $key)
    {

        if ($key == "")
            return;

        //set variables
        $names = [];

        if (is_array($key))
        {
            foreach ($key as $name){
                    $names[] = $name;
            }
        }
        else
            $names[] = $key;



        foreach ($names as $name)
        {

            $this->loadForeignKeyDefinition($tableName, $name);
            if ($data = $this->fetch()){
                $strSQL = "ALTER TABLE `".$tableName."` DROP FOREIGN KEY `".$name."`;";
                $this->prepare($strSQL);
                $this->execute();
            }


        }


    }

    /**
     * Remove All Foreign Indexes From a Table
     *
     * @since 1.00
     *
     * @param array $tableName name of the table
     * @param array $key string|array of key to be romoved
     *
     */
    public function removeTableIndex($tableName, $key)
    {

        if ($key == "")
            return;

        //set variables
        $names = [];

        if (is_array($key))
        {
            foreach ($key as $name){
                $names[] = $name;
            }
        }
        else
            $names[] = $key;

        foreach ($names as $name)
        {
            $this->loadIndexDefinition($tableName, $name);
            if ($data = $this->fetch()){
                $strSQL = "ALTER TABLE `".$tableName."` DROP INDEX `".$name."`;";

                $this->prepare($strSQL);
                $this->execute();
            }

        }





    }



    /**
     * Get DB Conecction
     *
     * @since 1.00
     *
     */
    function getDBConnection()
    {
        return $this->dbCon;
    }

    /**
     * Check DB Conecction
     *
     * @since 1.00
     *
     */
    function checkDBConnection()
    {
        return $this->dbConnected;

    }


    /**
     * Disconnect From DB
     *
     * @since 1.00
     *
     */
    function disconnect()
    {
        $this->flush();
        @$this->dbCon->close();
    }

    /**
     * Set the id of environment to use
     *
     * @param int $eid id of environment to use
     *
     * @since 1.00
     *
     */
    public function setEnvironment($eid)
    {

        $this->environmentIndex = $eid;

    }

    /**
     * Get the id of environment t
     *
     * @return int
     *
     * @since 1.00
     *
     */
    public function getEnvironment()
    {

        return $this->environmentIndex;

    }

    /**
     * Set the id of environment to use
     *
     * @param int $eid id of environment to use
     *
     * @since 1.00
     *
     */
    public function setTableName($tName)
    {

        $this->workingTableName[$this->environmentIndex] = $tName;

    }

    /**
     * Create a new environment
     *
     * @return  int id of environment created
     *
     * @since 1.00
     *
     */
    public function createEnvironment()
    {


        $this->environmentGlobalIndex += 1;

        return $this->environmentGlobalIndex;
    }

    /**
     * Show sql executed
     *
     * @param boolean $autoHide auto hide show sql after first show true or false (default true)
     *
     * @since 1.00
     *
     */
    public function showSQL($autoHide = true)
    {
        $this->autoHideEchoSQL = $autoHide;
        $this->echoSQL = true;
    }

    /**
     * Hide sql executed
     *
     *
     * @since 1.00
     *
     */
    public function hideSQL()
    {
        $this->echoSQL = false;
    }

    /**
     * Set if want trace or not error
     *
     * @param boolean $trace true or false for trace or not
     *
     * @since 1.00
     *
     */
    public function setTraceError($trace = true)
    {
        $this->traceError = $trace;
    }

    /**
     * Get trace log array
     *
     * @return array trace log
     *
     * @since 1.00
     *
     */
    public function getTraceLog()
    {
        return $this->traceLog;
    }

    /**
     * Show query errors
     *
     * @param boolean $show true or false (default true)
     *
     * @since 1.00
     *
     */
    public function showErrors($show = true)
    {
        $this->showQryErrors = $show;
    }

    /**
     * get Last Error
     *
     * @return string
     *
     * @since 1.00
     *
     */
    public function getLastError()
    {
        return ["code" => $this->lastErrorNumber, "description" => $this->lastError];
    }

    /**
     * get last Query executed
     *
     * @return string
     *
     * @since 1.00
     *
     */
    public function getLastQuery()
    {
        return $this->lastQuery;
    }


    /**
     * get result of last execcuted query of actual environment
     *
     * @return array
     *
     * @since 1.00
     *
     */
    public function getResult()
    {
        return $this->result[$this->environmentIndex];
    }

    /**
     * get total of query executed
     *
     * @return int
     *
     * @since 1.00
     *
     */
    public function getNumQueries()
    {
        return $this->numQueries;
    }

    /**
     * get num rows of last executed query of actual environment
     *
     * @return int
     *
     * @since 1.00
     *
     */
    public function getNumRows()
    {
        return $this->numRows[$this->environmentIndex];
    }

    /**
     * get num columns of last executed query of actual environment
     *
     * @return int
     *
     * @since 1.00
     *
     */
    public function getNumColumns()
    {
        return $this->numColumns[$this->environmentIndex];
    }

    /**
     * get last ID of last executed query of actual environment (insert | update)
     *
     * @return int
     *
     * @since 1.00
     *
     */
    public function getNewID()
    {
        return $this->newID[$this->environmentIndex];
    }

    /**
     * get last IDs by Table Name
     *
     * @return int
     *
     * @since 1.00
     *
     */
    public function getNewIDByTable($tName = "")
    {
        if ($tName != "")
            return $this->K_COMMON->getVarArray($this->newIDByTable, $tName);
        else
            return $this->K_COMMON->getVarArray($this->newIDByTable, $this->workingTableName[$this->environmentIndex]);
    }

    /**
     * set result
     *
     * @return int
     *
     * @since 1.00
     *
     */
    public function setResult($res)
    {
        $this->result[$this->environmentIndex] = $res;
    }



}




?>