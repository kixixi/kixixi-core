<?


//TODO: DB: Sistemare le Unione, perchè le prepare query non restituiscono più la query
//TODO: DB: Sostituire ovunque nelle query i % con i :
//TODO: DB: Eliminare ovunque i ["z" => "NOW()"] e anche ["s" => etc
//TODO: nei kplugin verificare dove serve mettere "kx_sql" o "kx_type_string" o "kx_type_number"
//TODO: gestire i tipi sul kdbmanager  "kx_type_string" o "kx_type_number"


/**
 * KIXIXI Base Table
 *
 * This plugin is used as base class of table object
 * It help, with some methods, to get, put and delete ercord from DB.
 * It extends the dbManager, so you can use also dbManager methods.
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package KIXIXI
 * @subpackage k_dbmanage\basetable
 * @since 1.00
 */

namespace  k_dbmanager\objects\general\basetable;

class obj extends \k_common\objects\general\base\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * output model
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $OM = "table";

    /**
     * Contain Last Query With No Limit
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $lastQueryNoLimit = "";

    /**
     * plugin name
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $currentPluginName = "";

    /**
     * plugin data name
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $currentDataName = "";

    /**
     * table name
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $currentTableName = "";

    /**
     * if custom table is set to true
     *
     * @since 1.00
     * @access public
     * @var string
     */
    protected $isCustomTable = false;

    /**
     * environment id on db manager
     *
     * @since 1.00
     * @access public
     * @var int
     */
    protected $dbEnvironmentID;

    /**
     * Constructor - initialize connection variable
     *
     * @param array $arrayVariable optional array of variables for query statement
     * The following directives can be used in the array variables:<br>
     *    *    pluginName: name of plugin table
     *    *    dataName: name of object name of table
     *
     * @since 1.00
     *
     */
    public function __construct($arrayVariable = [])
    {

        //Start Variables
        if (isset($arrayVariable["pluginName"]))
            $this->currentPluginName = $arrayVariable["pluginName"];
        if (isset($arrayVariable["dataName"]))
            $this->currentDataName = $arrayVariable["dataName"];

        //call parent
        parent::__construct(
            [
                "pluginName" => $this->currentPluginName
                ,"pluginObject" => $this->currentDataName
                ,"objType" => "tables"
            ]
        );

        //set variable
        global $kxDBManager;
        $this->dbEnvironmentID = $kxDBManager->createEnvironment();

        $this->currentTableName = $this->buildTableName($this->currentPluginName, $this->currentDataName);

        $this->setTableName($this->currentTableName);


    }

    /**
     * return table name
     *
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->currentTableName;

    }

    /**
     * build table name from namespace
     *
     * @param string $ns string namespace
     *
     * @return string
     */
    protected function buildTableName($pn, $dn)
    {
        return $pn."_".$dn;
    }

    /**
     * get if is custom table
     *
     * @return boolean
     */
    public function isCustomTable()
    {
        return $this->isCustomTable;
    }

    /**
     * Format Query String Condition
     *
     * @param array|string $where string of where, or array of fields to filter. For this array see dbManager documentation
     * @param array $bind optional array for bind if $where is a string
     * @param string $startWith optional how to start the query ("WHERE" or "AND")
     *
     * @return string
     */
    protected function createQueryCondition($where = [], &$bind, $startWith = "WHERE")
    {
        $queryCond = "";
        if (is_array($where)) {
            if (empty($bind))
                $bind = [];

            foreach ($where as $field => $value) {
                if (!is_numeric($field)) {


                    //Search if is a field with dot, for example "k_usefuldata_customfield.plugin_name"
                    $dotPos = strpos($field,".");
                    if ($dotPos > 0)
                            $field_where = "where_" . str_replace(".","_", $field) . "_endwhere";
                    else
                        $field_where = "where_" . $field . "_endwhere";


                    // Check if $value is array
                    // For Example: $cond["tpl_web_code"] = ["in (%WHERE%)" => [$webCode]];
                    if (is_array($value)) {
                        
                        if (!is_numeric(key($value))) {
                            $queryCond .= ($queryCond == "" ? "" : " AND ") . $field . " " . str_replace("%WHERE%", " :" . $field_where , key($value));
                            $bind[$field_where] = current($value);
                        } else
                            $queryCond .= ($queryCond == "" ? "" : " AND ") . $field . " " . current($value);

                    } else {
                        $queryCond .= ($queryCond == "" ? "" : " AND ") . $field . " = :" . $field_where;
                        $bind[$field_where] = $value;
                    }
                } else {
                    $queryCond .= ($queryCond == "" ? "" : " AND ") . $value;
                }
            }
        } else {
            $queryCond = $where;
        }

        if ($queryCond != "")
            $queryCond = " " . $startWith . " " . $queryCond;

        return $queryCond;
    }


    /**
     * count record
     *
     * @param array|string $where string of where, or array of fields to filter. For this array see dbManager documentation
     * @param array $bind optional array for bind if $where is a string
     * @param array $group optional array of fields to be group
     *
     * **Example #1**<br>
     * The "c" of "code" is the alias
     * <pre>
     *      count(array("code" => "100"));
     * </pre>
     *
     *
     * @return int count of recor
     */
    public function count($where = [], $bind = [], $group = [])
    {
        $c = 0;
        $this->get("sum(1) c",$where,$bind,$group);

        if ($row = $this->fetch())
            $c = $row->c;

        return $c;
    }

    /**
     * get Data from table
     *
     * @param array $fields array of Fields to get (may be possible specify an Alias)
     * @param array|string $where string of where, or array of fields to filter. For this array see dbManager documentation
     * @param array $bind optional array for bind if $where is a string
     * @param array $group optional array of fields to be group
     * @param array $order optional array of fields to be ordered
     * @param string $limit optional string limit variable (Ex: 0,3)
     * @param boolean $getWithNoLimit optional get total also with no limit
     *
     * **Example #1**<br>
     * The "c" of "code" is the alias
     * <pre>
     *      get(array("code" => "c", "name"),array("code" => "100"));
     * </pre>
     *
     * **Example #2**<br>
     * <pre>
     *      get(array("code", "name"),"code = :code", array("code", "100"));
     * </pre>
     *
     * **Example #3**<br>
     * <pre>
     *      get(array("code", "name"),array("code" => "100"),array(),array("code"));
     * </pre>
     *
     * **Example #4**<br>
     * <pre>
     *      get(["code", "name"],["code" => ["> %WHERE%" => "100"]]);
     * </pre>
     *
     */
    public function get($fields = [], $where = [], $bind = [], $group = [], $order = [], $limit = "", $getWithNoLimit = false)
    {
        $this->getByQuery("", $fields, $where, $bind, $group, $order, $limit, $getWithNoLimit);

    }

    /**
     * get Data from a query prototype
     *
     * @param string $queryName optional query name  of the query to use, if empty get data from actual table
     * @param array $fields optional array of Fields to get (may be possible specify an Alias), if empty get all fields
     * @param array|string $where string of where, or array of fields to filter. For this array see dbManager documentation. For Having put an array element with this key: "HAVING"
     * @param array $bind optional array for bind if $where is a string
     * @param array $group optional array of fields to be group
     * @param array $order optional array of fields to be ordered
     * @param string $limit optional string limit variable (Ex: 0,3)
     * @param boolean $getWithNoLimit optional get total also with no limit
     *
     * @see get for examples
     *
     * **Example #1**<br>
     * The "c" of "code" is the alias
     * <pre>
     *      get("active", ["code" => "c", "name"],["code" => "100"]);
     * </pre>
     *
     */
    public function getByQuery($queryName = "", $fields = [], $where = [], $bind = [], $group = [], $order = [], $limit = "", $getWithNoLimit = false)
    {





        //set variables
        $union = false; // true if is a union query
        $queryFrom = ""; // from statement, default ""
        $where = (is_array($where) ? $where : $where = [$where]);
        $group = (is_array($group) ? $group : []);
        $having = $this->K_COMMON->getVarArray($where, "HAVING");
        $having = (is_array($having) ? $having : $having = [$having]);
        unset($where["HAVING"]);

        //get query Data
        if ($queryName != "") {
            $PTypeDefinition = $this->loadQuery($queryName);

            //Check if is a UNION query
            if (isset($PTypeDefinition["queryU1"])) {
                $queryU1 = $this->prepareQuery($PTypeDefinition["queryU1"]);
                $queryU2 = $this->prepareQuery($PTypeDefinition["queryU2"]);
                $union = true;
            }

            //check if user defined custom fields
            if (empty($fields))
                if (isset($PTypeDefinition["fields"]))
                    if (!empty($PTypeDefinition["fields"]))
                        $fields = $PTypeDefinition["fields"];

            //if is set WHERE on query definition use it
            if (isset($PTypeDefinition["where"]))
                if (!empty($PTypeDefinition["where"]))
                    $where[] = $this->createQueryCondition($PTypeDefinition["where"], $bind, "");

            //if is set HAVING on query definition use it
            if (isset($PTypeDefinition["having"]))
                if (!empty($PTypeDefinition["having"]))
                    $having[] = $this->createQueryCondition($PTypeDefinition["having"], $bind, "");

            //if is set BIND on  query definition use it
            if (isset($PTypeDefinition["bind"]))
                if (!empty($PTypeDefinition["bind"]))
                    $bind = array_merge($bind, $PTypeDefinition["bind"]);




            //if is set GROUP on  query definition use it
            if (isset($PTypeDefinition["group"]))
                if (!empty($PTypeDefinition["group"]))
                    {
                        $pGroup = $PTypeDefinition["group"];
                        if (!is_array($pGroup))
                            $pGroup = [$pGroup];
                        $group = array_merge($group, $pGroup);

                    }

            //if is set ORDER on  query definition use it
            if (isset($PTypeDefinition["order"]))
                if (!empty($PTypeDefinition["order"]))
                    {
                        $pOrder = $PTypeDefinition["order"];
                        if (!is_array($pOrder))
                            $pOrder = [$pOrder];
                        $order = array_merge($order, $pOrder);
                    }

            //if is set LIMIT on  query definition use it
            if (($limit == "") && (isset($PTypeDefinition["limit"])))
                $limit = $PTypeDefinition["limit"];

            //if is set FROM on  query definition use it
            if (isset($PTypeDefinition["from"]))
                $queryFrom = $PTypeDefinition["from"];
        }


        if ($union) {

            $queryFrom = "(" . $queryU1 . " UNION ALL " . $queryU2 . ") QUERY";

            $this->prepareQuery(
                [
                    "from" => $queryFrom
                    , "fields" => $fields
                    , "where" => $where
                    , "having" => $having
                    , "bind" => $bind
                    , "group" => $group
                    , "order" => $order
                    , "limit" => $limit
                    , "getWithNoLimit" => $getWithNoLimit
                    , "queryName" => $queryName
                ]
            );
            $this->execute();
        } else {


            $this->prepareQuery(
                [
                    "from" => $queryFrom
                    , "fields" => $fields
                    , "where" => $where
                    , "having" => $having
                    , "bind" => $bind
                    , "group" => $group
                    , "order" => $order
                    , "limit" => $limit
                    , "getWithNoLimit" => $getWithNoLimit
                    , "queryName" => $queryName
                ]
            );
            $this->execute();
        }


    }

    /**
     * Prepare a query statement
     *
     * @param array $queryName name of query
     *
     * @return array query
     */
    public function loadQuery($queryName)
    {

        $dirPluginBase = $this->K_COMMON->getPathPluginObject(true, $this->currentPluginName, "");
        $fileName = $dirPluginBase . "/data/queries/" . $queryName . "/definition/query/query.xml";
        $array = $this->K_COMMON->loadXMLFile($fileName, true);

        return $array["query"];

    }


    /**
     * Prepare a query statement
     *
     * @param array $arrayVariables optional array of variables for query statement
     * The following directives can be used in the array variables:<br>
     *    *    from: from statement, if null get actual table name
     *    *    fields: see on get method
     *    *    where: see on get method
     *    *    bind: see on get method
     *    *    group: see on get method
     *    *    order: see on get method
     *    *    limit: see on get method
     *
     * @return string query
     */
    public function prepareQuery($arrayVariables = [])
    {

        //getVariables
        $from = (isset($arrayVariables["from"]) ? $arrayVariables["from"] : "");
        $fields = (isset($arrayVariables["fields"]) ? $arrayVariables["fields"] : []);
        $where = (isset($arrayVariables["where"]) ? $arrayVariables["where"] : []);
        $having = (isset($arrayVariables["having"]) ? $arrayVariables["having"] : []);
        $bind = (isset($arrayVariables["bind"]) ? $arrayVariables["bind"] : []);
        $group = (isset($arrayVariables["group"]) ? $arrayVariables["group"] : []);
        $order = (isset($arrayVariables["order"]) ? $arrayVariables["order"] : []);
        $limit = (isset($arrayVariables["limit"]) ? $arrayVariables["limit"] : "");
        $getWithNoLimit = (isset($arrayVariables["getWithNoLimit"]) ? $arrayVariables["getWithNoLimit"] : false);
        $queryName = (isset($arrayVariables["queryName"]) ? $arrayVariables["queryName"] : "");


        //check fields
        if (empty($fields))
            $fields = "*";

        //set FROM for query (default table)

        $queryFrom = ($from == "" ? $this->currentTableName : $from);

        //Create condition to query
        $queryCond = $this->createQueryCondition($where, $bind);
        $havingCond = $this->createQueryCondition($having, $bind, "HAVING");

        //add fields to query
        $queryFields = "";
        if (is_array($fields)) {
            foreach ($fields as $alias => $field) {
                if (!is_numeric($alias))
                    $field .= " " . $alias;
                $queryFields .= ($queryFields == "" ? "" : " ,") . $field;
            }
        } else {
            $queryFields = $fields;
        }


        //add GROUP BY to query
        $queryGroupBy = "";
        if (is_array($group)) {
            foreach ($group as $field) {
                $queryGroupBy .= ($queryGroupBy == "" ? " GROUP BY " : " ,") . $field;
            }
        }

        //add ORDER BY to query
        $queryOrderBy = "";
        if (is_array($order)) {
            foreach ($order as $field) {
                $queryOrderBy .= ($queryOrderBy == "" ? " ORDER BY " : " ,") . $field;
            }
        } else {
            $queryOrderBy = " ORDER BY " . $order;
        }


        //create query base
        $query = "SELECT " . $queryFields . " FROM " . $queryFrom;


        //create final query
        $queryBASE = $query . $queryCond . $queryGroupBy . $havingCond . $queryOrderBy;

        //check if get With No Limit
        if ($getWithNoLimit)
            $this->lastQueryNoLimit = $this->prepare($queryBASE, $bind);

        //check variables start and limit
        if ($limit != "")
            $queryBASE = $queryBASE . " LIMIT " . $limit;

        //prepare
        $query = $this->prepare($queryBASE, $bind);

        return $query;

    }


    /**
     * put Data on table - INSERT/ON DUPLICATE KEY UPDATE
     *
     * @param array $values array of values to set
     *
     * **Example **<br>
     * Insert New Record or update if exist
     * <pre>
     *      put(array("code" => "100", "name" => "Mark"));
     * </pre>
     *
     */
    public function putByKey($values)
    {
        //set variable
        $bind = [];

        //add fields to query
        $queryFields = "";
        $queryFieldsOnDup = "";
        foreach ($values as $field => $value) {
            $queryFields .= " ," . $field . " = :field_" . $field . "_endfield";
            $queryFieldsOnDup .= " ," . $field . " = :fieldondup_" . $field . "_endfield";
            $bind["field_" . $field . "_endfield"] = $value;
            $bind["fieldondup_" . $field . "_endfield"] = $value;
        }

        //create query base
        $query = "INSERT INTO " . $this->currentTableName
            . " SET creationdate = now(), createdby = '" . $this->K_COMMON->getCurrentUser() . "' "
            . $queryFields
            . " ON DUPLICATE KEY UPDATE "
            . "changedate = NOW(), changedby = '" . $this->K_COMMON->getCurrentUser() . "'"
            . $queryFieldsOnDup;

        //prepare
        $this->prepare($query, $bind);
        $this->execute();

    }

    /**
     * put Data on table
     *
     * @param array $values array of values to set (See example for insert an sql function)
     * @param array|string $where array of values to set
     * @param array $bind optional array for bind if $where is a string
     *
     * **Example #1**<br>
     * Insert New Record
     * <pre>
     *      put(array("code" => "100", "name" => "Mark"));
     * </pre>
     *
     * **Example #2**<br>
     * Update Record
     * <pre>
     *      put(array("name" => "Mark"),array("code" => "100"));
     * </pre>
     *
     * **Example #3**<br>
     * Update Record with where as string
     * <pre>
     *      get(array("name" => "Mark"),"code = :code", array("code", "100"));
     * </pre>
     *
     * **Example #4**<br>
     * Insert Record with Sql Function NOW
     * <pre>
     *     put(array("code" => "100", "name" => "Mark", "creationdate" => ["kx_sql" => "NOW()"]));
     * </pre>
     *
     *
     *
     *
     */
    public function put($values, $where = [], $bind = [])
    {




        //add fields to query
        $queryFields = "";
        foreach ($values as $field => $value) {
            $queryFields .= " ," . $field . " = :field_" . $field . "_endfield";
            $bind["field_" . $field . "_endfield"] = $value;
        }

        //add condition to query
        $queryCond = $this->createQueryCondition($where, $bind);

        //create query base
        if ($queryCond == "")
            $query = "INSERT INTO " . $this->currentTableName . " SET creationdate = now(), createdby = '" . $this->K_COMMON->getCurrentUser() . "'";
        else
            $query = "UPDATE " . $this->currentTableName . " SET changedate = NOW(), changedby = '" . $this->K_COMMON->getCurrentUser() . "'";


        //create final query
        $query = $query . " " . $queryFields . $queryCond;


        //prepare
        $this->prepare($query, $bind);
        $this->execute();
    }

    /**
     * delete Data from table
     *
     * @param array|string $where array of values to set
     * @param array $bind optional array for bind if $where is a string
     *
     * **Example #1**<br>
     * delete a Record
     * <pre>
     *      delete(array("code" => "100"));
     * </pre>
     *
     * **Example #2**<br>
     * Delete Record with where as string and binding
     * <pre>
     *      get("code = :code", array("code" => "100"));
     * </pre>
     *
     */
    public function delete($where = [], $bind = [])
    {
        //add condition to query
        $queryCond = $this->createQueryCondition($where, $bind);

        //create final query
        $query = "DELETE FROM " . $this->currentTableName . $queryCond;

        //prepare
        $this->prepare($query, $bind);
        $this->execute();
    }


    /**
     * copy a record (duplicate)
     *
     * @param array|string $where array of values to set
     * @param array $bind optional array for bind if $where is a string
     * @param array $fieldExclude array of fields to exclude on duplicate (if empty exclude if field have AUTOINCREMENT = 1)
     * @param array $fieldValues array of fields with particular values
     *
     * **Example #1**<br>
     * <pre>
     *      copy(array("id_record" => "100"));
     * </pre>
     *
     */
    public function copy($where = [], $bind = [], $fieldExclude = [], $fieldValues = [])
    {

        //Get table definition
        $tableDef = $this->getTableFieldsDefinition($this->currentTableName);

        //add fields to query
        $queryFields = "";
        $queryValues = "";
        foreach ($tableDef as $field => $options) {

            //check field to exclude
            $addField = false;
            if (empty($fieldExclude)) {
                if (isset($options["AUTOINCREMENT"]))
                    if ($options["AUTOINCREMENT"] != "1")
                    {
                        $addField = true;

                    }

            } else
                if (!in_array($field, $fieldExclude))
                {
                    $addField = true;

                }

            if ($addField)
            {

                if (!isset($fieldValues[$field]))
                {
                    $queryFields .= ($queryFields == "" ? "" : " ,") . $field;
                    $queryValues .= ($queryValues == "" ? "" : " ,") . $field;
                }
                else
                {
                    $queryFields .= ($queryFields == "" ? "" : " ,") . $field;
                    $queryValues .= ($queryValues == "" ? "" : " ,") . ":" . $field ;
                    $bind[$field] = $fieldValues[$field];
                }

            }
        }

        //add condition to query
        $queryCond = $this->createQueryCondition($where, $bind);

        //create query
        $query = "INSERT INTO " . $this->currentTableName
            . " (" . $queryFields . ") "
            . "  SELECT "
            . $queryValues
            . " FROM " . $this->currentTableName . " " . $queryCond;


        //prepare
        $this->prepare($query, $bind);
        $this->execute();
    }


    /**
     * get Results formatted as Output Model variable (OM)
     *
     * @param string $OM "table","array","xml","json"
     * @param boolean $useHtmlSpecialChars for use htmlspecialchars in xmlresult
     * @param boolean $useCData for use CDATA in xmlresult
     *
     * @return table|array|xml|json.
     */
    public function getResults($OM = "",$useHtmlSpecialChars = true,$useCData = false)
    {

        //set variables
        if ($OM == "")
            $OM = $this->OM;
        $OM = strtolower($OM);

        //Switch by type OM
        switch ($OM) {
            case "table" :
                $res = $this;
                break;
            case "array" :
                $res = [];
                while ($row = $this->fetch()) {
                    $res[] = (array)$row;
                }
                $this->setResult($res);
                
                break;
            case "xml" :
            case "XML":

                $DataSetTag = "dataset";
                $ResultTag = "results";
                $RowTag = "row";

                $XMLOutput = "";
                $XMLOutput .= "<$DataSetTag>" . chr(13);
                $XMLOutput .= "<$ResultTag>" . $this->getNumRows() . "</$ResultTag>" . chr(13);

                while ($row = $this->fetch()) {

                    $XMLOutput .= "<$RowTag>" . chr(13);

                    for ($i = 0; $i < $this->getNumColumns(); $i++) {
                        $fieldData = $this->getResult()->getColumnMeta($i);
                        $FieldName = $fieldData["name"];
                        if ($useHtmlSpecialChars)
                            $value = htmlspecialchars($row->$FieldName, ENT_QUOTES, 'UTF-8');
                        else
                            $value = $row->$FieldName;
                        if ($useCData)
                            $value = "<![CDATA[" . $value . "]]>";
                        $XMLOutput .= '<' . $FieldName . '>' . $value . '</' . $FieldName . '>' . chr(13);
                    }


                    $XMLOutput .= "</$RowTag>" . chr(13);
                } //End While

                $XMLOutput .= "</$DataSetTag>";

                return $XMLOutput;


                break;
            case "json" :
            case "JSON" :

                $arrayItems = [];
                while ($row = $this->fetch()) {
                    $arrayItems[] = $row;

                } //End While


                //Create complex JSON results
                $feeds = array('response' => array('value' => array()));
                $feeds['response']['value']['items'] = $arrayItems;
                $feeds['response']['value']['total_count'] = $this->getNumRows();
                $feeds['response']['value']['version']     = 1;
                
                //check if get also total with no limit
                if ($this->lastQueryNoLimit != "")
                {
                    $this->query($this->lastQueryNoLimit);
                    $feeds['response']['value']['total_nolimit_count'] = $this->getNumRows();
                }

                $json = json_encode($feeds);

                //return
                return $json;



                break;
            case "jsonsimple" :
            case "JSONSIMPLE" :

                $arrayItems = [];
                while ($row = $this->fetch()) {
                    $arrayItems[] = $row;

                } //End While

                $json = json_encode($arrayItems);

                //return
                return $json;

                break;



            default:
                $res = $this->getResult();
                break;
        }

        return $res;
    }

    /**
     * get List of Fields of a table
     *
     * @param string $tableName table name to check
     * @param boolean $noID not get ID of table (first field of table with auto increment)
     * @param boolean $noCreationFields not get creationdate, createdby, changedate, changedby
     *
     * @return string
     */
    public function getListOfFields($tableName = "", $noID = false, $noCreationFields = false)
    {

        if ($tableName == "")
            $tableName = $this->currentTableName;

        $this->loadTableDefinition($tableName);

        $listOfField = "";
        $fieldN = 0;
        while ($data = $this->kxdb->fetch()) {
            $fieldN ++;
            $fieldName = $data->Field;
            $fieldExtra = $data->Extra;

            $fieldAutoIncrement = false;
            if ($fieldExtra != "")
                $fieldAutoIncrement = (strpos("auto_increment", $fieldExtra) === false ? false : true);

            $insertField = true;
            if ($noCreationFields)
                if ($fieldName == "creationdate" || $fieldName == "createdby" || $fieldName == "changedate" || $fieldName == "changedby")
                    $insertField = false;

            if ($noID && $fieldN == 1 && $fieldAutoIncrement)
                $insertField = false;

            if ($insertField)
                $listOfField .= ", " . $fieldName;




        }

        if ($listOfField != "")
            $listOfField = substr($listOfField, 2); //remove ", "

        return $listOfField;
    }

    /**
     * get Table Fields Definition
     *
     * @param string $tableName table name to check
     * @param boolean $noID not get ID of table (first field of table with auto increment)
     * @param boolean $noCreationFields not get creationdate, createdby, changedate, changedby
     *
     * @return string
     */
    public function getTableFieldsDefinition($tableName)
    {

        //set variables
        $fieldsResult = [];

        //Load Table Definition
        $this->loadTableDefinition($tableName);
        while ($data = $this->K_COMMON->kxdb->fetch()) {

            $fieldName = $data->Field;
            if (substr($fieldName, 0, 4) != "zkx_"){

                $fieldsResult[$fieldName]["field"] = $fieldName;
                $fieldsResult[$fieldName]["type"] = $data->Type;
                $fieldsResult[$fieldName]["null"] = $data->Null;
                $fieldsResult[$fieldName]["key"] = ($data->Key == "PRI" ? $data->Key : "");
                $fieldsResult[$fieldName]["default"] = $data->Default;
                $fieldsResult[$fieldName]["extra"] = $data->Extra;

                $fieldExtra = $data->Extra;

                $fieldAutoIncrement = false;
                if ($fieldExtra != "")
                    $fieldAutoIncrement = (strpos("auto_increment", $fieldExtra) === false ? false : true);

                $fieldsResult[$fieldName]["AUTOINCREMENT"] = ($fieldAutoIncrement ? "1" : "");


            }

        }



        return $fieldsResult;
    }

    /**
     * call a procedure or get a function result
     *
     * @param string $functionName name of procedure or function to call
     * @param string $functionPar string of procedure parameters
     * @param string $type string of type: "procedure" or "function"
     *
     */
    public function call($functionName, $functionPar = "", $type = "function")
    {

        //create correct name of function
        $functionName = $this->getPluginName() . "_" . $functionName;

        if ($type=="procedure")
            $strSQL='CALL '.$functionName.'('.$functionPar.');';
        if ($type=="function")
            $strSQL='SELECT '.$functionName.'('.$functionPar.') returnvalue';

        $this->prepare($strSQL);
        $this->execute();

    }

    /**
     * This Function Execute a method of this object
     *
     * @return string|object|array function called value
     */
    function __call($method, $arguments)
    {


        global $kxDBManager;
        $kxDBManager->setEnvironment($this->dbEnvironmentID);
        return call_user_func_array(array($kxDBManager, $method), $arguments);


    }


}

?>
