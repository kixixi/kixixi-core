<?
/**
 * KIXIXI k_usefuldata (api)
 * API with data common
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package kcore\k_usefuldata
 * @subpackage objects\api
 * @since 1.00
 */
namespace k_usefuldata\objects\general\api;
class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";


    /**
     * Manage Actions from ajax
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($arrayVariable) {
        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //get template
        switch ($KCA) {
            case "getCountry" :
                return $this->getCountry($arrayVariable);
                break;
            case "getProvince" :
                return $this->getProvince($arrayVariable);
                break;
            case "getCity" :
                return $this->getCity($arrayVariable);
                break;
            case "getLanguages" :
                return $this->getLanguages($arrayVariable);
                break;
            case "listModule" :
                $pluginName = "";
                return $this->listPluginsAndObjects(["pluginName" => $pluginName, "OM" => "XML"]);
                break;
            case "listModuleObjects" :
                $pluginName = $this->K_COMMON->getVarArray($arrayVariable, "pluginName");
                $objectType = $this->K_COMMON->getVarArray($arrayVariable, "objectType");
                return $this->listPluginsAndObjects(["pluginName" => $pluginName, "objectType" => $objectType,"OM" => "XML"]);
                break;
            case "listModuleWebTemplate" :
                $pluginName = $this->K_COMMON->getVarArray($arrayVariable, "pluginName");
                $objectType = "web\\templates";
                return $this->listPluginsAndObjects(["pluginName" => $pluginName, "objectType" => $objectType,"OM" => "XML"]);
                break;
            case "listModuleData" :
                $pluginName = $this->K_COMMON->getVarArray($arrayVariable, "pluginName");
                $objectType = $this->K_COMMON->getVarArray($arrayVariable, "objectType");
                return $this->listPluginsAndObjects(["pluginName" => $pluginName, "objectType" => $objectType, "OM" => "XML"]);
                break;
            case "getWebTemplatesList" :
                return $this->getWebTemplatesList($arrayVariable);
                break;
            case "getPanelTemplatesList" :
                return $this->getPanelTemplatesList($arrayVariable);
                break;
            case "getListAuthType":
                return $this->getListAuthType($arrayVariable);
                break;
            
        }
    }

    /**
     * get countries
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    plugin: plugin to get authorization
     *
     * @return array
     */
    protected function getListAuthType($ArrayVariable){

        //set variables
        $plugin = $this->K_COMMON->getVarArray($ArrayVariable,"plugin");
        $arrayWhere = [];
        if ($plugin != "")
            $arrayWhere["plugin_name"] = $plugin;
        $arrayRes = [];


        //get Table
        $tableAuth = $this->K_COMMON->getPluginDataObject("k_secauth", "authtype");
        $tableAuth->get(
            [
                "plugin_name"
                ,"code_form"
                ,"code_label"
                ,"code_table_ref"
                ,"auth_by_web_target"
            ],$arrayWhere);

        while ($dataAuth = $tableAuth->fetch())
        {

            $codeForm = $dataAuth->code_form;
            $label = $dataAuth->code_label;
            $tabRef = $dataAuth->code_table_ref;
            $plugin = $dataAuth->plugin_name;
            $authByWCTarget = $dataAuth->auth_by_web_target;


            if ($label != "")
                $labelTranslated = $this->getDictionaryWord($label, $plugin);
            else
                $labelTranslated = $this->getDictionaryWord("labelDefaultAuth");

            $arrayRes[] =
                [
                    "value" => $plugin . "\\" . $codeForm,
                    "text" => $labelTranslated,
                    "codeTabRef" => $tabRef,
                    "plugin" => $plugin,
                    "authByWCTarget" => $authByWCTarget
                ];


        }



        $res = $this->K_COMMON->dbDataToXML(["array" => $arrayRes]);


        return $res;



    }

    /**
     * get countries
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    OM: Output model ("array", "table", "xml", "json") see getResults function of basetable
     *
     * @return array|xml|json|table
     */
    public function getCountry($arrayVariable) {
        $lang = $this->language;

        $table = $this->getTable("country");
        $OM = $this->K_COMMON->getVarArray($arrayVariable,"OM");

        $table -> get(["value" => "iso", "text" => "printable_name"],["language" => $lang],"","",["printable_name"]);

        return $table->getResults($OM);
    }

    /**
     * get Provinces
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    country: country code of provinces to get
     *    *    OM: Output model ("array", "table", "xml", "json") see getResults function of basetable
     *
     * @return array|xml|json|table
     */
    public function getProvince($arrayVariable) {
        $lang = $this->language;
        $country = $this -> K_COMMON -> getVarArray($arrayVariable,"country");
        $OM = $this->K_COMMON->getVarArray($arrayVariable,"OM");

        $table = $this->getTable("province");



        $table -> get(["value" => "code", "text" => "province"],["iso" => $country,"language" => $lang]);

        return $table->getResults($OM);
    }

    /**
     * get city
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    province: province code of cities to get
     *    *    OM: Output model ("array", "table", "xml", "json") see getResults function of basetable
     *
     * @return array|xml|json|table
     */
    public function getCity($arrayVariable) {
        $lang = $this->language;
        $province = $this -> K_COMMON -> getVarArray($arrayVariable,"province");
        $OM = $this->K_COMMON->getVarArray($arrayVariable,"OM");

        $table = $this->getTable("city");



        $table -> get(["value" => "city", "text" => "city", "zip_code" ],["province" => $province,"language" => $lang]);

        return $table->getResults($OM);
    }

    /**
     * get iso languages
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    OM: Output model ("array", "table", "xml", "json") see getResults function of basetable
     *
     * @return array|xml|json|table
     */
    public function getLanguages($arrayVariable) {
        $lang = $this->language;
        $OM = $this->K_COMMON->getVarArray($arrayVariable,"OM");

        $table = $this->getTable("languages");
        

        $table -> get(["value" => "iso", "text" => "name"],["language" => $lang]);
        if ($table->getNumRows() == 0)
            $table -> get(["value" => "iso", "text" => "name"],["language" => "EN"]);

        return $table->getResults($OM);
    }

    /**
     * get custom field values (from custom field table)
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginName: code of plugin (if empty get kcore data)
     *    *    cfCode: code of custom field
     *    *    OM: Output model ("array", "table", "xml", "json") see getResults function of basetable
     *
     * @return array|xml|json|table
     */
    public function getCustomFieldsValues($arrayVariable) {
        $cfCode = $this -> K_COMMON -> getVarArray($arrayVariable,"cfCode");
        $pluginName = $this -> K_COMMON -> getVarArray($arrayVariable,"pluginName");
        $OM = $this->K_COMMON->getVarArray($arrayVariable,"OM");

        $table = $this->getTable("customfieldvalues");

        $where = ["custom_field_code" => $cfCode];
        if ($pluginName != "")
            $where[] =  " plugin_name = '" . $pluginName . "'";
        else
            $where[] =  " plugin_name is null ";

        $table -> get(["value" => "field_text", "text" => "field_text"],$where);

        return $table->getResults($OM);
    }

    /**
     * get web sites configured on actual project
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    OM: Output model ("array", "table", "xml", "json") see getResults function of basetable
     *
     * @return array|xml|json|table
     */
    public function getWebSites($arrayVariable) {
        $OM = $this->K_COMMON->getVarArray($arrayVariable,"OM");

        $table = $this->K_COMMON->getPluginDataObject("k_webmanager", "web_sites");

        

        $table->get(["value" => "web_code", "text" => "web_name"]);

        return $table->getResults($OM);
    }

    /**
     * list plugins and plugin object in kplugin directory
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginName: optional. Name of plugin to get Plugin Objects (if empty get list of plugin)
     *    *    OM: Output model ("array", "xml")
     *    *    objectType: type of object of plugin (es: objects\\general)
     *    *    pluginType: array if empty get "kplugin" and "ktemplate". You can also "kcore", "kdata" - Ex: ["KPlugin" => "1", "KTemplate" => "1"]
     *    *    withKCore: if 1 get also KCore
     *    *    byFiles: if 1 get plugin from directory
     *
     * @return array|xml
     */
    function listPluginsAndObjects($arrayVariable = [])
    {


        //set variables
        $OM = $this->K_COMMON->getVarArray($arrayVariable,"OM");
        $pluginName = $this->K_COMMON->getVarArray($arrayVariable,"pluginName");
        $objectType = $this->K_COMMON->getVarArray($arrayVariable,"objectType");
        $pluginType = $this->K_COMMON->getVarArray($arrayVariable,"pluginType");
        $withKCore = $this->K_COMMON->getVarArray($arrayVariable,"withKCore");
        $withKData = $this->K_COMMON->getVarArray($arrayVariable,"withKData");
        $byFiles = $this->K_COMMON->getVarArray($arrayVariable,"byFiles");

        //Load modules
        $ArrayFile = array();


        if ($pluginName == "") //get plugins
        {
            if ($byFiles == "1")
            {
                if (empty($pluginType))
                    $arrayType = ["KPlugin" => "1", "KTemplate" => "1"];
                else
                    $arrayType = $pluginType;
                if ($withKCore == "1")
                    $arrayType["KCore"] = "1";
                if ($withKData == "1")
                    $arrayType["KData"] = "1";

                foreach ($arrayType as $pluginType => $n)
                {

                    $pluginType = strtolower($pluginType);
                    $dir = $this->K_COMMON->pathCstSystem  . "/".$pluginType."/";
                    @$dh = opendir($dir);
                    if ($dh !== false){
                        $arrayFilesTemp = [];
                        while (false !== ($filename = readdir($dh))) {

                            $arrayFilesTemp[]  = $filename;
                        }

                        sort($arrayFilesTemp);

                        foreach ($arrayFilesTemp as $filename){
                            if ((is_dir($dir . $filename)) && ($filename != ".") && ($filename != "..") && ($filename != "_notes")) {
                                $ArrayFile[] = array("type" => $pluginType, "value" => $filename, "text" => ($pluginName != "" ? "" : strtoupper($pluginType)."=>") . $filename);
                            }
                        }



                    }




                }


            }
            else
            {


                if (empty($pluginType))
                    $pluginParams = ["KPlugin" => "1", "KTemplate" => "1"];
                else
                    $pluginParams = $pluginType;
                if ($withKCore == "1")
                    $pluginParams["KCore"] = "1";
                if ($withKData == "1")
                    $pluginParams["KData"] = "1";

                $objPlugins = $this->K_COMMON->getPluginObject("k_common","plugins");
                $ArrayFileTmp = $objPlugins->listPlugins($pluginParams);
                $ArrayFile = array_merge($ArrayFile, $ArrayFileTmp);

            }

        }
        else //get objects
        {

            $pluginType = $this->K_COMMON->getPluginType($pluginName);

            if ($objectType == "data\\tables")
            {
                $tableListData = $this->K_COMMON->getListPluginDBTables($pluginName);

                $count = 0;
                while ($row = $tableListData->fetch()) {

                    $tableName = str_replace($pluginName . "_","",$row->TABLE_NAME);
                    $objType = $objectType;
                    $objTypeDesc = $objType;

                    if ($tableName != "queries")
                    {
                        $ArrayFile[] = [
                            "type" => $pluginType,
                            "objTypeFlat" => str_replace("\\", "_",$objType),
                            "objType" => $objType,
                            "objName" => $tableName,
                            "value" => $objType."\\".$tableName,
                            "text" => "[" . strtoupper($objTypeDesc) . "] => " . ($pluginName != "" ? "" : strtoupper($pluginType)."=>") . $tableName
                        ];
                        $count++;
                    }


                }


            }
            else
            {


                if ($objectType != "")
                    $listObjectType = $this->K_COMMON->getListObjectType($objectType);
                else
                    $listObjectType = $this->K_COMMON->getListObjectType();

                //Search
                foreach ($listObjectType as $objType => $objTypeDesc)
                {

                    $objectTypeDir = $this->K_COMMON->getDirByObjectType($objType);
                    $dir = $this->K_COMMON->pathCstSystem . "/".$pluginType."/" .  $pluginName . "/" . $objectTypeDir . "/";
                    @$dh = opendir($dir);

                    if ($dh !== false)
                        while (false !== ($filename = readdir($dh))) {
                            if ((is_dir($dir . $filename)) && ($filename != ".") && ($filename != "..") && ($filename != "_notes")) {
                                $ArrayFile[] =
                                    [
                                        "type" => $pluginType,
                                        "objTypeFlat" => str_replace("\\", "_",$objType),
                                        "objType" => $objType,
                                        "objName" => $filename,
                                        "value" => $objType."\\".$filename,
                                        "text" => "[" . strtoupper($objTypeDesc) . "] => " . ($pluginName != "" ? "" : strtoupper($pluginType)."=>") . $filename
                                    ];
                            }
                        }
                }
            }






        }


        if (strtoupper($OM) == "XML")
            return $this->K_COMMON->dbDataToXML(["array" => $ArrayFile]);
        else
            return $ArrayFile;
    }

    /**
     * get Web templates list of templates of plugin
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginName: Plugin Name to get templates
     *    *    pluginObject: Plugin Object to get templates
     *    *    OM: Output model ("array", "table", "xml", "json") see getResults function of basetable
     *
     * @return array|xml|json|table
     */
    public function getWebTemplatesList($arrayVariable = array())
    {

        //set variable
        $webCode = $this->K_COMMON->getVarArray($arrayVariable, "webCode");
        $pluginName = $this->K_COMMON->getVarArray($arrayVariable, "pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($arrayVariable, "pluginObject");
        $where = [];
        $OM = $this->K_COMMON->getVarArray($arrayVariable, "OM");

        //check variable
        $where["tpl_type"] = "web";
        $where["tpl_web_code"] = ["in (%WHERE%)" => [$webCode]];
        if ($pluginName != "")
            $where["tpl_plugin_name"] = $pluginName;
        if ($pluginObject != "")
            $where["tpl_plugin_object"] = $pluginObject;
        $table = $this->K_COMMON->getPluginDataObject("k_webmanager","web_templates");

        $table->getByQuery("web_templates_combo_code", [], $where);

        return $table->getResults($OM);
    }

    /**
     * get Panel templates list of templates of plugin
     *
     * @param array $arrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginName: Plugin Name to get templates
     *    *    pluginObject: Plugin Object to get templates
     *    *    OM: Output model ("array", "table", "xml", "json") see getResults function of basetable
     *
     * @return array|xml|json|table
     */
    public function getPanelTemplatesList($arrayVariable = array())
    {

        //set variable
        $pluginName = $this->K_COMMON->getVarArray($arrayVariable, "pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($arrayVariable, "pluginObject");
        $where = [];
        $OM = $this->K_COMMON->getVarArray($arrayVariable, "OM");

        //check variable
        $where["tpl_type"] = "panel";
        if ($pluginName != "")
            $where["tpl_plugin_name"] = $pluginName;
        if ($pluginObject != "")
            $where["tpl_plugin_object"] = $pluginObject;
        $table = $this->K_COMMON->getPluginDataObject("k_webmanager","web_templates");

        $table->getByQuery("web_templates_combo_code", [], $where);

        return $table->getResults($OM);
    }

}
?>