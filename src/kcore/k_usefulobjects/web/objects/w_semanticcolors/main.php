<?
/**
 * KIXIXI USEFUL OBJECTS (combo semantic colors)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package k_usefulobjects\w_semanticcolors
 * @subpackage web\objects\form
 * @since 1.00
 */
namespace k_usefulobjects\web\objects\w_semanticcolors;

class obj extends \k_webmanager\objects\general\web_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";


    /**
     * get
     *
     * @since 1.00
     * @access public
     *
     * @param array $arrayParam array with parameters for create Form
     * @see k_htmlcomponents\input for others parameters
     *
     * @return webobject
     */
    public function get($arrayParam = [])
    {


        //set variables
        $labelInput = $this->K_COMMON->getVarArray($arrayParam, "labelInput");
        $id = $this->K_COMMON->getVarArray($arrayParam, "id");
        $name = $this->K_COMMON->getVarArray($arrayParam, "name");

        //check variables
        if ($id == "")
            $arrayParam["id"] = "semanticColor";
        if ($name == "")
            $arrayParam["name"] = "semanticColor";


        //set default parameter
        $arrayParam["type"] = "select";
        $arrayParam["typeLoad"] = "array";
        $arrayParam["options"] = ["primary" => "primary", "secondary" => "secondary", "success" => "success", "info" => "info", "warning" => "warning", "danger" => "danger", "link" => "link"];

        //check variables
        if ($labelInput == "")
            $arrayParam["labelInput"] = "Semantic Color";

        $this->appendInput("", $arrayParam);

        // return
        return $this;
    }


}

?>

