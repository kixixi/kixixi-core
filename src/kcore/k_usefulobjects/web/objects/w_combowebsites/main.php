<?
/**
 * KIXIXI USEFUL OBJECTS (combo web sites)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package k_usefulobjects\w_combowebsites
 * @subpackage web\objects\form
 * @since 1.00
 */
namespace k_usefulobjects\web\objects\w_combowebsites;

class obj extends \k_webmanager\objects\general\web_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";


    /**
     * get
     *
     * @since 1.00
     * @access public
     *
     * @param array $arrayParam array with parameters for create Form
     * @see k_htmlcomponents\input for others parameters
     *
     *  The array of form support this values <br>
     *
     * *    setDefaultValue    : set this web site code
     *
     * @return webobject
     */
    public function get($arrayParam = [])
    {


        //set variables
        $setDefaultValue = $this->K_COMMON->getVarArray($arrayParam, "setDefaultValue");
        $labelInput = $this->K_COMMON->getVarArray($arrayParam, "labelInput");
        $id = $this->K_COMMON->getVarArray($arrayParam, "id");
        $name = $this->K_COMMON->getVarArray($arrayParam, "name");

        //check variables
        if ($id == "")
            $arrayParam["id"] = "webCode";
        if ($name == "")
            $arrayParam["name"] = "webCode";
        $arrayParam["type"] = "select";
        $arrayParam["typeLoad"] = "function";
        $arrayParam["pluginName"] = "k_usefuldata";
        $arrayParam["pluginObject"] = "api";
        $arrayParam["functionName"] = "getWebSites";

        //check variables
        if ($labelInput == "")
            $arrayParam["labelInput"] = "Web Site";

        if ($setDefaultValue == "1")
            $arrayParam["value"] = $this->K_COMMON->getEditingWeb();

        $this->appendInput("", $arrayParam);

        // return
        return $this;
    }

    /** ****************************************************************************************** **/
    /** ******************************** PARAMETRIZATION FUNCTIONS    **************************** **/
    /** ****************************************************************************************** **/
    protected function appendEditGeneric($param)
    {
        $this->appendEditTriggerEvent($param, true);
        $this->appendEditTriggerFunction($param, false);

    }

    protected function appendEditTriggerEvent($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Trigger Event";
        $name = "triggerEvent";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $eventArray = ["change" => "Change"];
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "select", "typeLoad" => "array", "options" => $eventArray, "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    protected function appendEditTriggerFunction($param = [], $newRow = true, $nCol = 6)
    {

        if ($newRow) {
            //NEW ROW
            $this->appendRow("");
        }

        //labelInput
        $label = "Trigger Function";
        $name = "triggerFunction";
        $value = $this->K_COMMON->getVarArray($param, $name);
        $this->appendColumn($this->lastRow, ["nCol" => $nCol]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "value" => $value]);

    }

    public function parameterizationGetForm($objParametersSaved)
    {
        $arrayFormField = $objParametersSaved;

        $this->appendEditGeneric($arrayFormField);


        return $this;

    }

    public function parameterizationSaveData($AV)
    {
        //include obj
        $triggerEvent = $this->K_COMMON->getVarArray($AV, "triggerEvent");
        $triggerFunction = $this->K_COMMON->getVarArray($AV, "triggerFunction");


        return [
            "triggerEvent" => $triggerEvent
            , "triggerFunction" => $triggerFunction
        ];
    }


}

?>

