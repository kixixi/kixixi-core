<?
/**
 * KIXIXI USEFUL OBJECTS (combo plugin data)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package k_usefulobjects\w_comboplugindata
 * @subpackage web\objects\form
 * @since 1.00
 */
namespace k_usefulobjects\web\objects\w_comboplugindata;

class obj extends \k_webmanager\objects\general\web_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";


    /**
     * get
     *
     * @since 1.00
     * @access public
     *
     * @param array $arrayParam array with parameters for create Form
     * @see k_htmlcomponents\input for others parameters
     *
     * @return webobject
     */
    public function get($arrayParam = [])
    {


        //set variables
        $labelInput = $this->K_COMMON->getVarArray($arrayParam, "labelInput");
        $id = $this->K_COMMON->getVarArray($arrayParam, "id");
        $name = $this->K_COMMON->getVarArray($arrayParam, "name");
        $loadTriggerRef = $this->K_COMMON->getVarArray($arrayParam, "loadTriggerRef");

        //check variables
        if ($id == "")
            $arrayParam["id"] = "pluginData";
        if ($name == "")
            $arrayParam["name"] = "pluginData";
        if ($loadTriggerRef == "")
            $arrayParam["loadTriggerRef"] = "#pluginName";

        //set default parameter
        $arrayParam["type"] = "select";
        $arrayParam["typeLoad"] = "ajax";
        $arrayParam["loadTrigger"] = "change";
        $arrayParam["parameterRef"] = "pluginName";
        $arrayParam["pluginName"] = "k_usefuldata";
        $arrayParam["pluginObject"] = "api";
        $arrayParam["pluginAction"] = "listModuleData";
        $arrayParam["ajaxParameters"] = ["objectType" => "data\\tables"];

        if (isset($arrayParam["value"]))
            $arrayParam["value"] = $value_sourcePluginObject = str_replace("\\", "\\\\\\\\", $arrayParam["value"]);

        //check variables
        if ($labelInput == "")
            $arrayParam["labelInput"] = "Plugin Data";



        $this->appendInput("", $arrayParam);

        // return
        return $this;
    }


}

?>

