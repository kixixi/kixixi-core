<?
/**
 * KIXIXI USEFUL OBJECTS (combo plugin template)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package k_usefulobjects\w_combotemplates
 * @subpackage web\objects\form
 * @since 1.00
 */
namespace k_usefulobjects\web\objects\w_combotemplates;

class obj extends \k_webmanager\objects\general\web_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";


    /**
     * get
     *
     * @since 1.00
     * @access public
     *
     * @param array $arrayParam array with parameters for create Form
     * @see k_htmlcomponents\input for others parameters
     *
     * @return webobject
     */
    public function get($arrayParam = [])
    {


        //set variables
        $webCode = $this->K_COMMON->getVarArray($arrayParam, "webCode");
        $labelInput = $this->K_COMMON->getVarArray($arrayParam, "labelInput");
        $id = $this->K_COMMON->getVarArray($arrayParam, "id");
        $name = $this->K_COMMON->getVarArray($arrayParam, "name");
        $loadTriggerRef = $this->K_COMMON->getVarArray($arrayParam, "loadTriggerRef");
        $parameterRef = $this->K_COMMON->getVarArray($arrayParam, "parameterRef");

        //check variables
        if ($id == "")
            $arrayParam["id"] = "pluginTemplate";
        if ($name == "")
            $arrayParam["name"] = "pluginTemplate";
        if (empty($loadTriggerRef))
            $arrayParam["loadTriggerRef"] = ["#pluginName","#pluginObject"];
        if (empty($parameterRef))
            $parameterRef = ["pluginName","pluginObject"];

        //set default parameter
        $arrayParam["type"] = "select";
        $arrayParam["typeLoad"] = "ajax";
        $arrayParam["loadTrigger"] = "change";
        $arrayParam["parameterRef"] = $parameterRef;
        $arrayParam["pluginName"] = "k_usefuldata";
        $arrayParam["pluginObject"] = "api";
        $arrayParam["pluginAction"] = "getWebTemplatesList";
        $arrayParam["ajaxParameters"] = ["webCode" => $webCode];

        //check variables
        if ($labelInput == "")
            $arrayParam["labelInput"] = "Template";



        $this->appendInput("", $arrayParam);

        // return
        return $this;
    }


}

?>

