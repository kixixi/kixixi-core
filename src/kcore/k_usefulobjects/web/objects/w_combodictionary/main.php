<?
/**
 * KIXIXI USEFUL OBJECTS (combo label dictionary)
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package k_usefulobjects\w_combodictionary
 * @subpackage web\objects\form
 * @since 1.00
 */
namespace k_usefulobjects\web\objects\w_combodictionary;

class obj extends \k_webmanager\objects\general\web_object\obj
{

    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    public $ver = "1.0";

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {

        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");

        //switch request
        switch ($KCA) {
            case "list" :
                return $this->listLabels($ArrayVariable);
                break;
        }
    }

    /**
     * get
     *
     * @since 1.00
     * @access public
     *
     * @param array $arrayParam array with parameters for create select
     * @see k_htmlcomponents\input for others parameters
     *
     * @return webobject
     */
    public function get($arrayParam = [])
    {


        //set variables
        $webCode = $this->K_COMMON->getVarArray($arrayParam, "webCode");
        $labelInput = $this->K_COMMON->getVarArray($arrayParam, "labelInput");
        $id = $this->K_COMMON->getVarArray($arrayParam, "id");
        $name = $this->K_COMMON->getVarArray($arrayParam, "name");
        $pluginName = $this->K_COMMON->getVarArray($arrayParam, "pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($arrayParam, "pluginObject");
        $reloadOnFocus = $this->K_COMMON->getVarArray($arrayParam, "reloadOnFocus");


        //include dictionary
        $dictionary = $this->K_COMMON->getPluginObject("k_dictionary","dictionary");
        $labels = $dictionary->get(["pluginName" => $pluginName,"pluginObject" => $pluginObject]);

        foreach ($labels as $key => $value)
        {
            $labels[$key] = $value . " ($key)";
        }


        //check variables
        if ($id == "")
            $arrayParam["id"] = "labelCode";
        if ($name == "")
            $arrayParam["name"] = "labelCode";
        if ($labelInput == "")
            $arrayParam["labelInput"] = "Label";


        //set default parameter
        $arrayParam["type"] = "select";
        $arrayParam["options"] = $labels;

        if ($reloadOnFocus == "1")
            {
                $arrayParam["typeLoad"] = "ajax";
                $arrayParam["reloadOnFocus"] = "1";
                $arrayParam["pluginName"] = "k_usefulobjects";
                $arrayParam["pluginObject"] = "web\\objects\\w_combodictionary";
                $arrayParam["pluginAction"] = "list";
                $arrayParam["ajaxParameters"] = ["pluginName" => $pluginName,"pluginObject" => $pluginObject, "webCode" => $webCode];
            }
        else
        {
            $arrayParam["typeLoad"] = "array";
        }









        $this->appendInput("", $arrayParam);

        // return
        return $this;
    }


    protected function listLabels($arrayParam)
    {

        //set variables
        $webCode = $this->K_COMMON->getVarArray($arrayParam, "webCode");
        $pluginName = $this->K_COMMON->getVarArray($arrayParam, "pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($arrayParam, "pluginObject");
        $labelsToExport = [];

        //include dictionary
        $dictionary = $this->K_COMMON->getPluginObject("k_dictionary","dictionary");
        $labels = $dictionary->get(["pluginName" => $pluginName,"pluginObject" => $pluginObject, "webCode" => $webCode]);

        foreach ($labels as $key => $value)
        {
            $labelsToExport[] = ["value" => $key, "text" =>  $value . " ($key)"];

        }

        echo $this->K_COMMON->dbDataToXML(["array" => $labelsToExport]);

        return "";
    }
}

?>

