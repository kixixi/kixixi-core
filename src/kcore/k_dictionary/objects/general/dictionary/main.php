<?
/**
 * KIXIXI Dictionary Manager
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @package KIXIXI
 * @subpackage k_dbmanager\dictionary
 * @since 1.00
 */
namespace k_dictionary\objects\general\dictionary;
class obj extends \k_common\objects\general\base\obj
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * get Dictionary
     *
     * @since 1.00
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *  *   word:     word code to search in dictionary
     *  *   language: language to get
     *  *   pluginName:  plugin
     *  *   pluginObject:  object
     *
     * @return array array of dictionary
     */
    function get($ArrayVariable)
    {



        //set variables
        global $kxDictionary;
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable,"webCode");
        $word = $this->K_COMMON->getVarArray($ArrayVariable,"word");
        $language = $this->K_COMMON->getVarArray($ArrayVariable,"language");
        $pluginName  = $this->K_COMMON->getVarArray($ArrayVariable,"pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable,"pluginObject");
        $dictionaryID = $pluginName . ($pluginObject != "" ? "_" . $pluginObject : "");
        $arrCond = [];
        $dataLoaded = false;
        if ($pluginObject != "" && $pluginName == 'k_pluginmanage')
        {
            //$pluginObject = str_replace('\\', "\\", $pluginObject);
          //  echo "<br>PLUGIN OBJECT = " . $pluginObject;
        }


        //Load Table
        $table = $this->getTable("dictionary");

        //check if dictionary exist
        if (!isset($kxDictionary[$dictionaryID])) //not exist, load data
        {

            //initialize array;
            $kxDictionary[$dictionaryID] = [];

            //build query
            if ($language == "")
                $language = $this->language;

            $arrCond["dcd_web_code"] = $webCode;
            $arrCond["dic_language_code"] = $language;
            if ($pluginName != "")
                $arrCond["dic_module"] = $pluginName;
            if ($pluginObject != "")
                $arrCond["dic_module_object"] = $pluginObject;
            else
                $arrCond[] = "(dic_module_object is null OR dic_module_object = '')";


            $table->getByQuery("dictionary_complete","*",$arrCond);


            if ($table->getNumRows() > 0)
                $dataLoaded = true;
            else
            {

                //Search default plugin language
                $tabPlugin = $this->K_COMMON->getPluginDataObject("k_common", "plugins");
                $tabPlugin->get("*",["plg_id"=>$pluginName]);
                $dataPlugin = $tabPlugin->fetch();

                if ($dataPlugin)
                    $language = $dataPlugin->plg_language;


                if ($language == "")
                    $language = "EN";

                $arrCond["dic_language_code"] = $language;


                $table->getByQuery("dictionary_complete","*",$arrCond);

                if ($table->getNumRows() > 0)
                    $dataLoaded = true;


            }

        }

        //create array
        if ($dataLoaded)
            {


                while ($dataDIC = $table->fetch()) {


                    $name = $dataDIC->dic_code;
                    $val = $dataDIC->dic_word;
                    $link = $dataDIC->dcd_link;
                    $lngWord = $dataDIC->dic_language_code;

                    if ($link != "")
                    {
                        $linkArr = json_decode($link,true);
                        foreach ($linkArr as $k => $v)
                        {

                            if (is_array($v))
                            {
                                $target = $this->K_COMMON->getVarArray($v,"target");
                                $link = $this->K_COMMON->getVarArray($v,"link");
                                $class = $this->K_COMMON->getVarArray($v,"class");
                                $title = $this->K_COMMON->getVarArray($v,"title");

                                if (empty($target))
                                    $target = "";
                                if (empty($class))
                                    $class = "";
                                if (empty($title))
                                    $title = "";
                            }
                            else
                                {
                                    $target = "";
                                    $link = $v;
                                    $class = "";
                                    $title = "";
                                }
                            $val = str_replace("[$k]",'<a href="'.$link.'" target="'.$target.'" title="'.$title.'" class="'.$class.'">', $val);
                            $val = str_replace("[/$k]",'</a>', $val);
                        }
                    }

                    $val = str_replace("[language]",$lngWord,$val);
                    $arrDic[$name] = $val;

                }

                $kxDictionary[$dictionaryID] = $arrDic;
            }

        //return data
        if ($word != "")
            return $this->K_COMMON->getVarArray($kxDictionary[$dictionaryID],$word);
        else
            return $kxDictionary[$dictionaryID];


    }

    /**
     * put Dictionary
     *
     * @since 1.00
     *
     * @param array $arrayVariables array of variables
     * The following directives can be used in the array variables:<br>
     *  *   word:     word code to search in dictionary
     *  *   translation:     word code to search in dictionary
     *  *   language: language to get
     *  *   pluginName:  plugin
     *  *   pluginObject:  object
     *
     * @return array array of dictionary
     */
    function put($arrayVariables)
    {
        //set variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariables,"webCode");
        $pluginName  = $this->K_COMMON->getVarArray($arrayVariables,"pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($arrayVariables,"pluginObject");
        $language = $this->K_COMMON->getVarArray($arrayVariables,"language");
        $word = $this->K_COMMON->getVarArray($arrayVariables,"word");
        $translation = $this->K_COMMON->getVarArray($arrayVariables,"translation");
        $link = $this->K_COMMON->getVarArray($arrayVariables,"link");


        //get panel menu data object
        $table = $this->K_COMMON->getPluginDataObject("k_dictionary", "dictionary");
        $tableLink = $this->K_COMMON->getPluginDataObject("k_dictionary", "dictionary_definition");

        //save link
        if (!empty($link)) {

            $data = [
                "dcd_web_code" => $webCode
                ,"dcd_module" => $pluginName
                , "dcd_module_object" => $pluginObject
                , "dcd_code" => $word
                , "dcd_link" => (string)json_encode($link)

            ];

            $tableLink->putByKey($data);
        }
        else{
            
            $data = [
                "dcd_web_code" => $webCode
                ,"dcd_module" => $pluginName
                , "dcd_module_object" => $pluginObject
                , "dcd_code" => $word
                , "dcd_link" => ""

            ];

            $tableLink->putByKey($data);
        }


        $data = [
            "dic_web_code" => $webCode
            ,"dic_module" => $pluginName
            , "dic_module_object" => $pluginObject
            , "dic_code" => $word
            , "dic_language_code" => $language
            , "dic_word" => $translation

        ];
        $table->putByKey($data);

    }

    /**
     * delete Dictionary
     *
     * @since 1.00
     *
     * @param array $arrayVariables array of variables
     * The following directives can be used in the array variables:<br>
     *  *   pluginName:  plugin
     *  *   pluginObject:  object (if = "*" delete all of plugin)
     *  *   word:     word code to search in dictionary (if empty delete all word of dictionary of this plugin object
     *  *   language: language to get - if empty delete all languages of the word
     *
     *
     */
    function delete($arrayVariables)
    {
        //set variables
        $webCode = $this->K_COMMON->getVarArray($arrayVariables,"webCode");
        $pluginName  = $this->K_COMMON->getVarArray($arrayVariables,"pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($arrayVariables,"pluginObject");
        $language = $this->K_COMMON->getVarArray($arrayVariables,"language");
        $word = $this->K_COMMON->getVarArray($arrayVariables,"word");

        //get panel menu data object
        $table = $this->K_COMMON->getPluginDataObject("k_dictionary", "dictionary");
        $tableDef = $this->K_COMMON->getPluginDataObject("k_dictionary", "dictionary_definition");


        if ($language != "")
        {
            //delete dictionary language
            $where = [
                "dic_web_code" => $webCode
                , "dic_module" => $pluginName
                , "dic_language_code" => $language

            ];

            if ($pluginObject != "*")
                $where["dic_module_object"] = $pluginObject;

            if ($word != "")
                $where["dic_code"] = $word;

            $table->delete($where);
        }
        else
        {
            //delete dictionary of all languages
            $where = [
                "dic_web_code" => $webCode
                ,"dic_module" => $pluginName
            ];

            if ($pluginObject != "*")
                $where["dic_module_object"] = $pluginObject;

            if ($word != "")
                $where["dic_code"] = $word;

            $table->delete($where);


            //delete dictionary definition
            $where = [
                "dcd_web_code" => $webCode
                ,"dcd_module" => $pluginName
            ];

            if ($pluginObject != "*")
                $where["dcd_module_object"] = $pluginObject;

            if ($word != "")
                $where["dcd_code"] = $word;

            $tableDef->delete($where);
        }



    }


    /**
     * Check if a word exist in Dictionary
     *
     * @since 1.00
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *  *   word:     word code to search in dictionary
     *  *   pluginName:  plugin
     *  *   pluginObject:  object
     *
     * @return boolean
     */
    function checkWordExist($ArrayVariable)
    {

        //set variables
        $webCode = $this->K_COMMON->getVarArray($ArrayVariable,"webCode");
        $word = $this->K_COMMON->getVarArray($ArrayVariable, "word");
        $pluginName = $this->K_COMMON->getVarArray($ArrayVariable, "pluginName");
        $pluginObject = $this->K_COMMON->getVarArray($ArrayVariable, "pluginObject");
        $arrCond = [];

        //Load Table
        $table = $this->getTable("dictionary_definition");

        //build query
        $arrCond["dcd_code"] = $word;
        $arrCond["dcd_web_code"] = $webCode;
        if ($pluginName != "")
            $arrCond["dcd_module"] = $pluginName;
        if ($pluginObject != "")
            $arrCond["dcd_module_object"] = $pluginObject;
        else
            $arrCond[] = "(dcd_module_object is null OR dcd_module_object = '')";

        $table->get(["id_dictionary_definition"], $arrCond);

        $dataDIC = $table->fetch();

        if ($dataDIC)
            return true;
        else
            return false;

    }


}

?>