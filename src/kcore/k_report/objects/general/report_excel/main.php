<?
namespace k_report\objects\general\report_excel;

class obj extends \k_webmanager\objects\general\base_panel\obj //this class extend panel because it may be used in panel manage
{
    /**
     * Version of object
     *
     * @since 1.00
     * @access public
     * @var string
     */
    var $ver = "1.0";

    /**
     * dictionary of this object
     *
     * @since 1.00
     * @access protected
     * @var string
     */
    protected $dictionaryRep;

    /**
     * array of object that customize report
     *
     * @since 1.00
     * @access protected
     * @var array
     */
    var $customizeExcelObj;

    /**
     * Constructor
     *
     */
    public function __construct($arrayVariable = [])
    {


        parent::__construct();


        //dictionary
        $this->dictionaryRep = $this->getDictionary("k_report","objects\\general\\report_excel");

        //set this object as first custom
        $this->customizeExcelObj[] = ["plugin" => $this->getPluginName(), "object" => $this->getPluginObjectName()];

    }

    /**
     * Manage Actions from ajax
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    KCA: Action to execute
     *
     * @return string|json|array depend on ajax type call
     *
     */
    public function objectRequest($ArrayVariable)
    {
        //set variable
        $KCA = $this->K_COMMON->getVarRequest("KCA");
        
        //get template
        switch ($KCA) {
            case "getListMyTemplate" :
                return $this->getListMyTemplate($ArrayVariable);
                break;
            case "loadDataTemplate" :
                return json_encode($this->loadDataTemplate($ArrayVariable));
                break;
            case "saveDataTemplate" :
                return json_encode($this->saveDataTemplate($ArrayVariable));
                break;
            case "buildReport" :
                return json_encode($this->buildReport($ArrayVariable));
                break;
            case "buildReportOpenFile" :
                return json_encode($this->buildReportOpenFile($ArrayVariable));
                break;

        }
    }

    /**
     * Check for customization
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    reportCustomize: array of plugin that customize object
     *
     * @return object this object
     */
    public function checkCustomization($ArrayVariable = [])
    {

        //set variables
        $reportCustomize = $this->K_COMMON->getVarArray($ArrayVariable, "reportCustomize");

        //check for customization
        if (is_array($reportCustomize))
            foreach ($reportCustomize as $cst)
                $this->customizeExcelObj[] = $cst;
    }

    /**
     * get this object
     *
     * @param array $ArrayVariable array of variables
     * The following directives can be used in the array variables:<br>
     *    *    pluginMethod: method of plugin ("get", "")
     *
     * @return object this object
     */
    public function get($ArrayVariable = [])
    {

        //set variables
        $pluginMethod = $this->K_COMMON->getVarArray($ArrayVariable, "pluginMethod");

        //check for customization
        $this->checkCustomization($ArrayVariable);

        switch ($pluginMethod) {
            case "":
            case "get":

                $this->getPagePut($ArrayVariable);

                //include files
                $this->addHeadFile($this->getPathFileJS("get","k_report","report_excel"));
                $this->addHeadFile($this->getPathFileCSS("get","k_report","report_excel"));
                $this->addExternalPlugin("validator");

                //include files of custom
                $this->getGlobalFileToInclude();


                break;
        }
        //Return
        return $this;
    }

    /**
     * manage customization
     *
     * @param array $ArrayVariable array of variables
     *
     * @return object this object
     */
    protected function customBuilder($ArrayVariable)
    {
        //set variables
        $reportCustomize = $this->K_COMMON->getVarArray($ArrayVariable, "reportCustomize");
        if (empty($reportCustomize))
            $reportCustomize = [];
        $reportCustomize[] = ["plugin" => $this->getPluginName(), "object" => $this->getPluginObjectName()];
        $ret = $ArrayVariable["customization"]["target"]["methodParameters"];
        $ret["reportCustomize"] = $reportCustomize;

        return $ret;
    }

    protected function getPagePut($ArrayVariable)
    {
        //set variables
        $arrayFieldsSetting = $this->getGlobalReportArrayField(["checkPermiss" => true]);
        $arrayField = $this->K_COMMON->getVarArray($arrayFieldsSetting, "fields");
        $arrayFilterComplete = $this->getGlobalReportArrayFilterFields();

        //create form MANDATE
        $this->appendForm("", ["id" => "hiddenForm"]);

        //keyID hidden
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "reportPluginName", "name" => "reportPluginName", "value" => $this->getPluginName()]);
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "reportPluginObject", "name" => "reportPluginObject", "value" => $this->getPluginObjectName()]);

        //create Tabs
        $this->appendTab("",
            [
                "id" => "tabsMain"
                , "tabs" =>
                [
                    ["id" => "tabReport", "label" => $this->dictionaryRep["labelTabsReport"], "active" => "1"]
                    ,["id" => "tabPrint", "label" => $this->dictionaryRep["labelTabsPrint"]]
                    ,["id" => "tabTemplate", "label" => $this->dictionaryRep["labelTabsTemplate"]]
                ]
            ]
        );

        //NEW ROW
        $rowTabReport = $this->appendRow("tabReport");

        //Column Fields
        $itemsField = [];
        $itemsField[] = ["type"=>"link", "class" => "small disabled", "value" => $this->dictionaryRep["labelEditFields"]];
        foreach ($arrayField as $key => $value) {
            $itemsField[] = ["type"=>"button", "class" => "small", "value" => $value["labelField"], "attributes" => 'fieldname="' . $key . '"'];
        }
        $this->appendColumn($rowTabReport, ["nCol" => 2]);
        $this->appendListGroup($this->lastCol,
            [
                "id" => "fieldItemContainer"
                , "attributes" => 'style="height:500px; overflow: scroll; overflow-x: hidden;"'
                , "items" => $itemsField
            ]
        );

        //Column Fields Selected
        $itemsFieldSel = [];
        $itemsFieldSel[] = ["type"=>"link","class" => "small disabled", "value" => $this->dictionaryRep["labelEditFieldsSel"]];
        $this->appendColumn($rowTabReport, ["nCol" => 2]);
        $this->appendListGroup($this->lastCol,
            [
                "id" => "fieldSelectedContainer"
                , "attributes" => 'style="height:500px; overflow: scroll; overflow-x: hidden;"'
                , "items" => $itemsFieldSel
            ]
        );

        //load filter and tab for filter
        $arrayFilter = $this->K_COMMON->getVarArray($arrayFilterComplete,"filters");
        $arrayTabs = $this->K_COMMON->getVarArray($arrayFilterComplete,"tabs");
        $arrayTabsFilter = [];
        $active = 1;
        foreach($arrayTabs as $tab=>$labelTab){
            $arrayTabsFilter[] = ["id" => "tab_".$tab, "label" => $labelTab,"active" => $active];
            $active = "";
        }

        //Create column for Filter
        $this->appendColumn($rowTabReport, ["id" => "tabFilters", "nCol" => 4, "class" => "small"]);

        //create Tabs Filter
        $this->appendTab($this->lastCol,
            [
                "id" => "tabsFilters"
                , "tabs" => $arrayTabsFilter
            ]
        );
        //Insert Filter fields
        foreach ($arrayFilter as $key => $value) {
            $filterSetting = $this->K_COMMON->getVarArray($value, "filterSetting");
            $filterType = $this->K_COMMON->getVarArray($filterSetting, "type");
            $filterTab = $this->K_COMMON->getVarArray($filterSetting, "tab");
            $filterParameters = $this->K_COMMON->getVarArray($filterSetting, "parameters");

            if ($filterTab != ""){
                $this->appendRow("tab_" . $filterTab);
                $this->appendColumn($this->lastRow, ["nCol" => 11]);
                $filterParameters["type"] = $filterType;
                $this->appendInput($this->lastCol, $filterParameters);

            }
        }

        //Column Filter Selected
        $itemsFilterSel = [];
        $itemsFilterSel[] = ["type"=>"link","class" => "small disabled", "value" => $this->dictionaryRep["labelEditFiltersSel"]];
        $this->appendColumn($rowTabReport, ["nCol" => 4]);
        $this->appendListGroup($this->lastCol,
            [
                "id" => "filterSelectedContainer"
                , "attributes" => 'style="height:500px; overflow: scroll; overflow-x: hidden;"'
                , "items" => $itemsFilterSel
            ]
        );

        // *** PRINT TAB ***
        $this->appendForm("tabPrint", ["id" => "printForm"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //Field Group Data
        $label = $this->dictionaryRep["labelEditPrintGroup"];
        $name = "fieldGroup";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "checkbox", "labelInput" => $label, "id" => $name, "name" => $name, "value" => "1", "checked" => ""]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //button
        $label = $this->dictionaryRep["labelEditPrintBtnPrint"];
        $name = "btnPrint";
        $this->appendColumn($this->lastRow, ["nCol" => 3]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success"]);


        // *** TEMPLATE TAB ***
        $this->appendForm("tabTemplate", ["id" => "templateForm", "setSavedMessage" => "1"]);

        //hidden variables
        $this->appendInput($this->lastForm, ["type" => "hidden", "id" => "templateCode", "name" => "templateCode", "value" => ""]);


        //NEW ROW
        $this->appendRow($this->lastForm);

        //template
        $label = $this->dictionaryRep["labelEditTemplateTemplate"];
        $name = "idTmpl";
        $this->appendColumn($this->lastRow, ["nCol" => 2]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select",
                "labelInput" => $label,
                "id" => $name,
                "name" => $name,
                "size" => "sm"
            ]
        );

        //button load
        $label = $this->dictionaryRep["labelEditTemplateLoad"];
        $name = "btnLoad";
        $this->appendColumn($this->lastRow, ["nCol" => 2]);
        $this->appendInput($this->lastCol, ["type" => "btn", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "info", "size" => "sm", "label" => "&nbsp;", "blocked" => "1"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //template name
        $label = $this->dictionaryRep["labelEditTemplateName"];
        $name = "reportName";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol, ["type" => "text", "labelInput" => $label, "id" => $name, "name" => $name, "size" => "sm"]);

        //NEW ROW
        $this->appendRow($this->lastForm);

        //template employee
        $label = $this->dictionaryRep["labelEditTemplateEmployee"];
        $name = "employeeCode";
        $this->appendColumn($this->lastRow, ["nCol" => 4]);
        $this->appendInput($this->lastCol,
            [
                "type" => "select",
                "labelInput" => $label,
                "id" => $name,
                "name" => $name,
                "size" => "sm",
                "typeLoad" => "data",
                "pluginName" => "k_secauth",
                "pluginData" => "login",
                "dataOrder" => "",
                "dataPrototype" => "login_combo"
            ]
        );

        //NEW ROW
        $this->appendRow($this->lastForm);


        //Save Template Button
        $label = $this->dictionaryRep["labelEditTemplateSave"];
        $name = "btnSaveTemplate";
        $this->appendColumn($this->lastRow, ["nCol" => 2]);
        $this->appendInput($this->lastCol, ["type" => "btnSubmit", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "success", "size" => "sm", "label" => "&nbsp;", "blocked" => "1"]);

        //Save Template Button
        $label = $this->dictionaryRep["labelEditTemplateNew"];
        $name = "btnNewTemplate";
        $this->appendColumn($this->lastRow, ["nCol" => 2]);
        $this->appendInput($this->lastCol, ["type" => "btn", "value" => $label, "id" => $name, "name" => $name, "semanticColor" => "secondary", "size" => "sm", "label" => "&nbsp;", "blocked" => "1"]);


    }

    //Template Report Functions
    protected function loadDataTemplate($arrayVariable)
    {
        //include tables
        $tableREP = $this->K_COMMON->getPluginDataObject("k_report","template");
        $tableREPDET = $this->K_COMMON->getPluginDataObject("k_report","template_detail");

        //get variable
        $keyID = $this->K_COMMON->getVarArray($arrayVariable, "keyID");

        //set variable
        $anaCode = $this->K_COMMON->getCurrentUser();

        //Load Report Data
        $tableREP->get(
            ["*"]
            , [
                "id_report_template = " . $keyID . " AND (createdby = :anaCode OR report_employee_code = :anaCode OR (report_employee_code is null) OR (report_employee_code is null))"
                ,"report_template_plugin_name" => $this->getPluginName()
                ,"report_template_plugin_object" => $this->getPluginObjectName()
            ]
            , ["anaCode" => $anaCode]
        );
        $dataREP = $tableREP->fetch();

        if ($dataREP) {
            $reportTemplateCode = $dataREP->report_template_code;
            $reportEmployeeCode = $dataREP->report_employee_code;
            $reportName = $dataREP->report_name;
            $arrayFieldCode = explode(",", $dataREP->report_field_code);
            $reportFieldGroup = $dataREP->report_field_group;
            $arrayFieldComplete = $this->K_COMMON->getVarArray($this->getGlobalReportArrayField(),"fields");

            //Get Fields dictionaryRep
            $arrayField = [];
            foreach ($arrayFieldCode as $value) {
                $arrayFieldData = $this->K_COMMON->getVarArray($arrayFieldComplete,$value);
                $arrayField[$value] = $this->K_COMMON->getVarArray($arrayFieldData, "labelField");
            }

            //Get Details Data
            $tableREPDET->getByQuery("data_converted", "", ["report_template_code" => $reportTemplateCode]);
            $arrayFilterComplete = $this->getGlobalReportArrayFilterFields()["filters"];
            $arrayFilter = [];
            while ($dataREPDET = $tableREPDET->fetch()) {

                $filterCode = $dataREPDET->report_field_name;
                $filterValue = $dataREPDET->report_field_value;
                $thisFilter = $arrayFilterComplete[$filterCode];

                //get description of Filter
                $obj = $this->K_COMMON->getPluginObject($thisFilter["plugin_name"],$thisFilter["plugin_object"]);
                if ($obj->methodExist('getFilterDescription'))
                    $filterDescription = $obj->getFilterDescription($filterCode,$filterValue);
                else
                    $filterDescription = $filterValue;

                //set array filter
                $arrayFilter[] =
                    [
                        "filterCode" => $filterCode
                        ,"filterValue" => $filterValue
                        ,"filterDescription" => $filterDescription
                        ,"filterName" => $thisFilter["filterName"]
                    ] ;
            }
        }
        return array("success" => "true", "templateCode" => $reportTemplateCode, "employeeCode" => $reportEmployeeCode, "reportName" => $reportName, "fieldGroup" => $reportFieldGroup, "arrayField" => $arrayField, "arrayFilter" => $arrayFilter);

    }

    protected function saveDataTemplate($ArrayVariable)
    {

        //include tables
        $tableREP = $this->K_COMMON->getPluginDataObject("k_report","template");
        $tableREPDET = $this->K_COMMON->getPluginDataObject("k_report","template_detail");

        //set variable
        $fieldSelected = $this->K_COMMON->getVarArray($ArrayVariable, "fieldSelected");
        $arrayFilter = $this->K_COMMON->getVarArray($ArrayVariable, "arrayFilter");
        $groupSelected = $this->K_COMMON->getVarArray($ArrayVariable, "groupSelected");
        $report_name = $this->K_COMMON->getVarArray($ArrayVariable, "reportName");
        $reportEmployeeCode = $this->K_COMMON->getVarArray($ArrayVariable, "reportEmployeeCode");
        $reportTemplateCode = $this->K_COMMON->getVarArray($ArrayVariable, "templateCode");

        //check if duplicate
        if ($reportTemplateCode != "")
            $tableREP->get("id_report_template", ["report_name" => $report_name, "report_template_code" => ["<> %WHERE%" => $reportTemplateCode]]);
        else
            $tableREP->get("id_report_template", ["report_name" => $report_name]);
        $dataREP = $tableREP->fetch();
        if ($dataREP)
            return array("success" => "false", "reason" => "duplicate", "message" => $this->dictionaryRep["labelEditTemplateDup"]);

        //create array for save template
        $dataToSave = [];
        $dataToSave["report_template_plugin_name"] = $this->getPluginName();
        $dataToSave["report_template_plugin_object"] = $this->getPluginObjectName();
        $dataToSave["report_template_code"] = $reportTemplateCode;
        $dataToSave["report_name"] = $report_name;
        $dataToSave["report_employee_code"] = $reportEmployeeCode;
        if ($groupSelected != "")
            $dataToSave["report_field_group"] = $groupSelected;
        $dataToSave["report_field_code"] = $fieldSelected;

        if ($reportTemplateCode == "") {
            $tableREP->put($dataToSave);

            $tableREP->get(["report_template_code"], ["id_report_template" => $tableREP->getNewID()]);
            $dataREP = $tableREP->fetch();
            $reportTemplateCode = $dataREP->report_template_code;
        } else {
            $tableREP->put($dataToSave, ["report_template_code" => $reportTemplateCode]);
        }

        //remove Detail Data
        $tableREPDET->delete(["report_template_code" => $reportTemplateCode]);

        //Save Detail Data
        if ($arrayFilter != "")
        {
            reset($arrayFilter);
            $dataToSave = [];
            $dataToSave["report_template_code"] = $reportTemplateCode;
            foreach ($arrayFilter as $key => $value) {

                foreach (explode(",", $value) as $value2) {
                    $dataToSave["report_field_name"] = $key;
                    $dataToSave["report_field_value"] = $value2;
                    $tableREPDET->put($dataToSave);
                }

            }
        }

        return array("success" => "true", "tmplcode" => $reportTemplateCode, "message" => $this->dictionaryRep["labelEditTemplateSaved"]);
    }

    protected function getListMyTemplate($arrayVariable)
    {
        //get variables
        $OM = $this->K_COMMON->getVarArray($arrayVariable, "OM");

        //set variable
        $anaCode = $this->K_COMMON->getCurrentUser();

        //include Table
        $tableREP = $this->K_COMMON->getPluginDataObject("k_report","template");
        
        $tableREP->get(
            ["value" => "id_report_template", "text" => "report_name"]
            , [
                "(createdby = :anaCode OR report_employee_code = :anaCode OR (IFNULL(report_employee_code,'')='') OR (IFNULL(report_employee_code,'')=''))"
                ,"report_template_plugin_name" => $this->getPluginName()
                ,"report_template_plugin_object" => $this->getPluginObjectName()
            ]
            , ["anaCode" => $anaCode]
        );

        return $tableREP->getResults($OM);
    }

    //Build Report Functions
    protected function buildReport($ArrayVariable)
    {

        //get variables
        $groupSelected = $this->K_COMMON->getVarArray($ArrayVariable, "groupSelected");
        $fieldSelected = $this->K_COMMON->getVarArray($ArrayVariable, "fieldSelected");
        $arrayFilter = $this->K_COMMON->getVarArray($ArrayVariable, "arrayFilter");

        //check for customization
        $this->checkCustomization($ArrayVariable);


        $AV = [];
        $AV["groupSelected"] = $groupSelected;
        $AV["Field_Code"] = $fieldSelected;
        $AV["arrayFilter"] = $arrayFilter;
        $AV["fieldSelected"] = $fieldSelected;

        //make query for Report
        $QryCustomArray = $this->buildReportQryCustom($AV);

        //load data and Create report excel
        $resultBuild = $this->buildReportExcelCustom($QryCustomArray);
        return $resultBuild;


    }

    protected function buildReportQryCustom($ArrayVariable)
    {

        //set variables
        $fieldSelected = $this->K_COMMON->getVarArray($ArrayVariable, "fieldSelected");
        $arrayFilter = $this->K_COMMON->getVarArray($ArrayVariable, "arrayFilter");
        $groupSelected = $this->K_COMMON->getVarArray($ArrayVariable, "groupSelected");
        $arrayField = explode(",", $fieldSelected);
        $arrayFieldsSetting = $this->getGlobalReportArrayField($ArrayVariable);
        $arrayFieldsName = $this->K_COMMON->getVarArray($arrayFieldsSetting, "fields");
        $arrayFieldsTables = $this->K_COMMON->getVarArray($arrayFieldsSetting, "tables");
        $arrayFieldsTablesDefault = $this->K_COMMON->getVarArray($arrayFieldsSetting, "tablesDefault");

        //********************************SET FIELD
        reset($arrayField);
        $Field = "";
        $Group = "";
        $Order = "";
        $tables = [];

        foreach ($arrayField as $value) {
            $fieldName = $this->K_COMMON->getVarArray($arrayFieldsName,$value);
            if(is_array($fieldName)){
                $qryField = $fieldName["qryField"];
                if($qryField != ""){
                    $tables[$fieldName["tableCode"]] = $fieldName["tableCode"];
                    $Field .= ",".$qryField." ".$value;
                    if ($groupSelected != 0){
                        $Group .= ",".$qryField;
                    }
                    $Order .= ",".$qryField;
                }
            }
        }
        if ($groupSelected != 0) {
            $Group = "GROUP BY " . substr($Group, "1");
        }
        $Field = substr($Field, 1);
        $Order = substr($Order, 1);
        //**************************END SET FIELD

        //********************************* SET FILTER
        $Cond = [];
        $arrayFilterSetting = $this->getGlobalReportArrayFilterQuery($arrayFilter);

        $arrayFilterSettingConditions = $arrayFilterSetting["conditions"];
        $arrayFilterSettingFilters = $arrayFilterSetting["filters"];

        //gt general filter
        foreach($arrayFilterSettingConditions as $keyFilter => $valueFilter) {
            $this->K_COMMON->concatVarArray($Cond, $keyFilter, $valueFilter);
        }

        //get filter for each field
        foreach ($arrayFilter as $key => $value) {
            $value = str_replace(",", "','", $value);
            $arrayFilterData = $this->K_COMMON->getVarArray($arrayFilterSettingFilters,$key);

            $arrayFilterCond = $this->K_COMMON->getVarArray($arrayFilterData,"condition");
            if(is_array($arrayFilterCond)){
                foreach($arrayFilterCond as $keyFilter=>$valueFilter) {
                    $this->K_COMMON->concatVarArray($Cond, $keyFilter," ".str_replace("%value%", $value, $valueFilter));
                }
            }
        }


        //******************************* END SET FILTER

        //set condition
        $baseCondition = $this->K_COMMON->getVarArray($Cond,"baseCondition");

        // BUILD QUERY DEFAULT
        $TableSelect = "";
        foreach($arrayFieldsTablesDefault as $tableCode => $tablesQuery){
            $table = str_replace("%condition%",$this->K_COMMON->getVarArray($Cond,$tableCode),$tablesQuery);
            $TableSelect .= " ".$table;
        }

        // BUILD QUERY
        foreach($tables as $tableCode){
            $table = $this->K_COMMON->getVarArray($arrayFieldsTables,$tableCode);
            if ($table <> "")
            {
                $table = str_replace("%condition%",$this->K_COMMON->getVarArray($Cond,$tableCode),$table);
                $TableSelect .= " ".$table;
            }
        }


        return array("arrayField" => $arrayField, "field" => $Field, "group" => $Group, "order" => $Order, "cond" => $baseCondition, "tableSelect" => $TableSelect);


    }

    protected function buildReportExcelCustom($ArrayVariable)
    {

        //set variables
        $fieldsName = $this->K_COMMON->getVarArray($ArrayVariable, "arrayField");
        $Field = $this->K_COMMON->getVarArray($ArrayVariable, "field");
        $Group = $this->K_COMMON->getVarArray($ArrayVariable, "group");
        $Order = $this->K_COMMON->getVarArray($ArrayVariable, "order");
        $Cond = $this->K_COMMON->getVarArray($ArrayVariable, "cond");
        $TableSelect = $this->K_COMMON->getVarArray($ArrayVariable, "tableSelect");
        $currentUserCode = $this->K_COMMON->getCurrentUser();
        $reportName = rtrim($currentUserCode) . "_Report_" . time() . ".xlsx";
        $fileName = $this->K_COMMON->getPathTmp() . $reportName;

        //include modules
        $pathSDKExcel = $this->K_COMMON->getPathSDK(["name" => "phpexcel"]);
        $objFM = $this->K_COMMON->getPluginObject("k_filemanager", "api");
        include_once($pathSDKExcel . "/Classes/PHPExcel.php");

        //include tables
        $table = $this->getTable();

        //set Time Limit
        set_time_limit(240);

        //Create Excel Object
        $obj_msexcel = new \PHPExcel();

        //Check if File exist, delete it
        if (file_exists($fileName))
            unlink($fileName);

        //Set Head Report
        $obj_msexcel->setActiveSheetIndex(0);
        $obj_msexcel->getActiveSheet()->setTitle('Report');

        //Write Head Columns
        $i = 1;

        $fieldsComplete = $this->getGlobalReportArrayField()["fields"];

        $pre = "";
        $letter = 64;
        foreach ($fieldsName as $value)
        {
            if ($i > 26)
                $pre = chr(floor($i / 26) + 64);
            if ($letter == 90)
                $letter = 64;
            $letter = $letter + 1;

            $fieldData = $this->K_COMMON->getVarArray($fieldsComplete,$value);
            $fieldNameTrad = $this->K_COMMON->getVarArray($fieldData,"labelFieldExcel");
            $obj_msexcel->getActiveSheet()->setCellValue($pre . chr($letter) . '1', $fieldNameTrad);
            $i++;

        }

        //Load Data Report
        $query = "SELECT  ".$Field
            ." FROM  "
            .$TableSelect
            ." WHERE 1=1 "
            ." ".$Cond
            ." ".$Group
            ." ".($Order != "" ? " ORDER BY ".$Order : "");

        //$table->showSQL();
        $table->prepare($query);
        $table->execute();

        //Write Data
        $RowReport = 2;

        while ($table->fetch()) {
            //Write Value Fields
            $pre = "";
            $letter = 64;
            $i = 1;
            reset($fieldsName);
            foreach ($fieldsName as $value) {

                if ($i > 26)
                    $pre = chr(floor($i / 26) + 64);
                if ($letter == 90)
                    $letter = 64;
                $letter++;

                $fieldSetting = $fieldsComplete[$value];
                $fieldType = $this->K_COMMON->getVarArray($fieldSetting,"type");
                if (($fieldType == "string") || ($fieldType = ""))
                {
                    $ValWrite = str_replace(".", ",", '="' . $table->getValue($value) . '"');
                    $obj_msexcel->getActiveSheet()->setCellValue($pre . chr($letter) . $RowReport, $ValWrite);
                }
                else
                {
                    $obj_msexcel->getActiveSheet()->setCellValue($pre . chr($letter) . $RowReport, rtrim($table->getValue($value)));
                }


                $i++;
            } //End while Field

            //New Row
            $RowReport += 1;

        } //end while


        //Set Font
        $obj_msexcel->getActiveSheet()->getStyle('A1:Z1')->getFont()->setName('Thaoma');
        $obj_msexcel->getActiveSheet()->getStyle('A1:Z1')->getFont()->setBold(true);

        //Fit Columns
        $obj_msexcel->getActiveSheet()->getStyle('B:B')->getAlignment()->setShrinkToFit(true);

        //Save File
        $objWriter = \PHPExcel_IOFactory::createWriter($obj_msexcel, 'Excel2007');
        $objWriter->save($fileName);

        //Save on S3
        $ParamFile = array();
        $ParamFile["file"] = $fileName;
        $ParamFile["newFileName"] = "report/" . $reportName;
        $ParamFile["deleteOri"] = true;
        $ParamFile["authRead"] = "private";


        $linkFile = $objFM->uploadStaticContent(["sourceFile" => $fileName, "targetDir" => "report", "targetFile" => $reportName, "expiryMinutes"=>4320, "authRead" => "private"]);



        return ["success" => "true", "url" => $linkFile];
    }

    protected function getGlobalReportArrayField($arrayVariables = []){

        //set return array
        $returnArray = [
            "fields" => []
            ,"tables" => []
            ,"tablesDefault" => []
        ];


        //get array field from custom
        foreach ($this->customizeExcelObj as $data) {

            $obj = $this->K_COMMON->getPluginObject($data["plugin"],$data["object"]);
            $arrayField = $obj->getReportArrayField($arrayVariables);

            //scan fields
            $fields = $this->K_COMMON->getVarArray($arrayField,"fields");
            foreach ($fields as $key => $parameters)
            {
                $returnArray["fields"][$key] = $parameters;
            }

            //scan tables
            $tables = $this->K_COMMON->getVarArray($arrayField,"tables");
            if ($tables == "")
                $tables = [];
            foreach ($tables as $key => $parameters)
            {
                $returnArray["tables"][$key] = $parameters;
            }

            //scan default table
            $tables = $this->K_COMMON->getVarArray($arrayField,"tablesDefault");
            if ($tables == "")
                $tables = [];
            foreach ($tables as $key => $parameters)
            {
                $returnArray["tablesDefault"][$key] = $parameters;
            }



        }

        return $returnArray;
    }

    protected function getGlobalReportArrayFilterFields($arrayFilter = []){

        //set return array
        $returnArray = [
            "filters" => []
            ,"tabs" => []
        ];


        //get array field from custom
        foreach ($this->customizeExcelObj as $data) {
            $obj = $this->K_COMMON->getPluginObject($data["plugin"],$data["object"]);
            if ($obj->methodExist('getReportArrayFilterFields'))
            {
                $arrayFilters= $obj->getReportArrayFilterFields($arrayFilter);

                //scan filters
                $filters = $this->K_COMMON->getVarArray($arrayFilters,"filters");
                foreach ($filters as $key => $parameters)
                {
                    $parameters["plugin_name"] = $data["plugin"];
                    $parameters["plugin_object"] = $data["object"];
                    $returnArray["filters"][$key] = $parameters;
                    $tab = $this->K_COMMON->getVarArray($returnArray["filters"][$key]["filterSetting"],"tab");
                    if ($tab != "")
                        $returnArray["filters"][$key]["filterSetting"]["tab"] = $data["plugin"]."_".$data["object"]."_".$tab;
                    $parentTab = $this->K_COMMON->getVarArray($returnArray["filters"][$key]["filterSetting"],"parentTab");
                    if ($parentTab != "")
                        $returnArray["filters"][$key]["filterSetting"]["tab"] = $this->getPluginName()."_".$this->getPluginObjectName()."_".$parentTab;

                }

                //scan tabs
                $tabs = $this->K_COMMON->getVarArray($arrayFilters,"tabs");
                if ($tabs == "")
                    $tabs = [];
                foreach ($tabs as $key => $parameters)
                {
                    //$returnArray["tabs"][$key] = $parameters;
                    $returnArray["tabs"][$data["plugin"]."_".$data["object"]."_".$key] = $parameters;
                }
            }


        }

        return $returnArray;
    }

    protected function getGlobalReportArrayFilterQuery($arrayFilter = []){

        //set return array
        $returnArray = [
            "filters" => []
            ,"conditions" => []
        ];


        //get array field from custom
        foreach ($this->customizeExcelObj as $data) {
            $obj = $this->K_COMMON->getPluginObject($data["plugin"],$data["object"]);
            if ($obj->methodExist('getReportArrayFilterQuery')) {
                $arrayFilters = $obj->getReportArrayFilterQuery($arrayFilter);

                //scan filters
                $filters = $this->K_COMMON->getVarArray($arrayFilters, "filters");
                foreach ($filters as $key => $parameters) {
                    $returnArray["filters"][$key] = $parameters;

                }

                //set conditions
                $conditions = $this->K_COMMON->getVarArray($arrayFilters, "conditions");
                $this->K_COMMON->concatVarArray($returnArray, "conditions", $conditions);
            }
        }

        return $returnArray;
    }

    protected function getGlobalFileToInclude()
    {
        //set return array
        $returnArray = [
            "headFiles" => ""

        ];

        foreach ($this->customizeExcelObj as $data) {

            $obj = $this->K_COMMON->getPluginObject($data["plugin"], $data["object"]);

            if ($obj->methodExist('getFileToInclude'))
            {
                $obj->getFileToInclude();

            }


        }

        return $returnArray;
    }

}

?>