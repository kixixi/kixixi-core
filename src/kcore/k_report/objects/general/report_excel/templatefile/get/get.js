
// create namespace
KXJS.namespace("k_report.objects.report_excel");

// create method of plugin
(function() {
    var t = this;
    t.pluginName = "";
    t.pluginObject = "";

    t.ready = function() {
        t.pluginName = $("#hiddenForm #reportPluginName").val();
        t.pluginObject = $("#hiddenForm #reportPluginObject").val();


        t.loadStartValue();

        $("#btnNewTemplate").click(t.resetForm);
        $("#btnLoad").click(t.loadTemplate);

        $("#tabFilters select").change(t.createFilterBySelect);
        $('#tabFilters input:not([type="checkbox"])').keydown(function(e){
            if(e.keyCode == "13"){
                var currentField = $(this);
                t.createFilterByInput(currentField);
            }
        });
        $('#tabFilters div.date input').change(function(e){
            var currentField = $(this);
            t.createFilterByDate(currentField);
        });
        $('#tabFilters input:not([type="checkbox"])').focusout(function(){
            var currentField = $(this);
            t.createFilterByInput(currentField);
        });
        $('#tabFilters [type="checkbox"]').click(t.createFilterByCheckbox);

        $("#fieldItemContainer button").click(function(){
            t.addField(this);
        });


        /*******************SUBMIT FORM PRINT*****************/

        //Validator
        $('#printForm').validator().on('submit', function (e) {
            if (e.isDefaultPrevented()) {
            } else {
                e.preventDefault();
                t.submitPrint();

            }
        })

        //Validator
        $('#templateForm').validator().on('submit', function (e) {
            if (e.isDefaultPrevented()) {
            } else {
                e.preventDefault();
                t.saveTemplate();

            }
        })
    }

    this.init = function(){
        $(document).ready(t.ready);
    };

    t.addFilter = function(value,valueName,filterName,filterDescr){
        var filterText = '<div class="small">'+filterDescr+'</div><div>'+valueName+"</div>";
        var currentFilter = $('#filterSelectedContainer').find("a").first().clone();
        $("#filterSelectedContainer").append(currentFilter);
        currentFilter = $('#filterSelectedContainer a').last();
        currentFilter.removeClass("disabled");
        currentFilter.attr("filterName",filterName);
        currentFilter.attr("filterValue",value);
        currentFilter.html(filterText);
        currentFilter.click(function(){$(this).insertBefore($(this).prev());});
        currentFilter.on("contextmenu", function (e) {e.preventDefault();
            var filterName = $(this).attr("filterName");
            $(this).remove();
        });

    }
    t.createFilterBySelect = function(){
        var value = $(this).val();
        var valueName = $(this).find("[value=\""+value+"\"]").text();
        var filterName = $(this).attr("filterName");
        var filterDescr = $(this).parent().find("label").text();
        if(value == "")
            return;
        t.addFilter(value,valueName,filterName,filterDescr);
    }
    t.createFilterByInput = function(currentField){
        var value = currentField.val();
        if(value == "")
            return;
        var valueName = value;
        var filterName = currentField.attr("filterName");
        var filterDescr = currentField.parent().find("label").text();
        t.addFilter(value,valueName,filterName,filterDescr);
        currentField.val("");
    }
    t.createFilterByDate = function(currentField){
        var value = currentField.val();
        if(value == "")
            return;
        var valueName = value;
        var filterName = currentField.attr("filterName");
        var filterDescr = currentField.parent().parent().find("label").text();
        t.addFilter(value,valueName,filterName,filterDescr);
        currentField.val("");
    }
    t.createFilterByCheckbox = function(){
        currentField = $(this);
        var value = currentField.val();
        var valueName = "";//value;
        var filterName = currentField.attr("filterName");
        var filterDescr = currentField.parent().text();

        if($('#filterSelectedContainer [filtername="'+filterName+'"]').length == 0)
            t.addFilter(value,valueName,filterName,filterDescr);
        else
            $('#filterSelectedContainer [filtername="'+filterName+'"]').remove();
        currentField.prop("checked",false);
    }

    t.addField = function(currentField){//cloneFieldAndSetTrigger

        $("#fieldSelectedContainer").append($(currentField).clone());
        $("#fieldSelectedContainer button").last().click(function(){
            $(this).insertBefore($(this).prev());
        });
        $("#fieldSelectedContainer button").last().on("contextmenu", function (e) {
            e.preventDefault();
            $(this).remove();
        });
    }
    t.resetForm = function(){
        $("#tabReport input,#tabTemplate input").not(":checkbox").val("");
        $('#tabReport [type="checkbox"]').prop("checked",false);
        $('#tabReport select [value=""], #tabTemplate select option[value=""]').prop("selected",true);
        $("#tabReport #fieldSelectedContainer button").remove();
        $("#tabReport #filterSelectedContainer a").first().nextAll().remove();

    }
    t.loadStartValue = function(){
        t.resetForm();
        t.loadReportMyTemplate();
    }

    t.saveTemplate = function(){
        var arrayFieldCode = $('#fieldSelectedContainer button').map(function () {
            return $(this).attr("fieldname");
        }).get();

        var fieldSelected = arrayFieldCode.toString();
        var arrayFilter = {};
        //arrayFilter[""]="";
        $('#filterSelectedContainer a').first().nextAll().map(function () {
            if(typeof(arrayFilter) == "undefined")
                arrayFilter[$(this).attr("filtername")] = $(this).attr("filtervalue");
            else
            {
                if(typeof(arrayFilter[$(this).attr("filtername")]) == "undefined")
                    arrayFilter[$(this).attr("filtername")] = $(this).attr("filtervalue");
                else
                    arrayFilter[$(this).attr("filtername")] += ","+$(this).attr("filtervalue");
            }
        });
        var reportName = $("#reportName").val();
        var reportEmployeeCode = $("#employeeCode").val();
        var templateCode = $("#templateCode").val();
        var groupSelected = "";
        if($("#fieldGroup").prop("checked"))
            groupSelected = 1;
        waitingDialog.show('Saving...', {dialogSize: 'sm', progressType: 'warning'});
        $.ajax({
            url: KXJS.buildAPICallUrl("KXCJS",t.pluginName,t.pluginObject,"saveDataTemplate"),
            data: {
                groupSelected: groupSelected,
                fieldSelected: fieldSelected,
                arrayFilter: arrayFilter,
                reportName: reportName,
                reportEmployeeCode:reportEmployeeCode,
                templateCode: templateCode
            }
        }).done(function(data) {
            var r = $.parseJSON(data);
            KXJS.showSavedMessageJSON("#templateForm .savedMessage",r);
            t.loadReportMyTemplate();
            if(r.success == "true"){
                $("#templateCode").val(r.tmplcode);

            }

            waitingDialog.hide();
        });

    }
    t.submitPrint = function(){
        if($('#fieldSelectedContainer button').length < 1 && $('#filterSelectedContainer button').length <1 )
            return;
        var arrayFieldCode = $('#fieldSelectedContainer button').map(function () {
            return $(this).attr("fieldname");
        }).get();

        var fieldSelected = arrayFieldCode.toString();

        var arrayFilter = {};
        arrayFilter[""]="";
        $('#filterSelectedContainer a').first().nextAll().map(function () {
            if(typeof(arrayFilter[$(this).attr("filtername")]) == "undefined")
                arrayFilter[$(this).attr("filtername")] = $(this).attr("filtervalue");
            else
                arrayFilter[$(this).attr("filtername")] += ","+$(this).attr("filtervalue");

        });
        var groupSelected = "";
        if($("#fieldGroup").prop("checked"))
            groupSelected = 1;
        dialogModal = waitingDialog.show('Printing...', {dialogSize: 'sm', progressType: 'warning'});
        
        $.ajax({
            url: KXJS.buildAPICallUrl("KXCJS",t.pluginName,t.pluginObject,"buildReport"),
            data: {
                groupSelected: groupSelected,
                fieldSelected: fieldSelected,
                arrayFilter: arrayFilter,
            }
        }).done(function(data) {

            var r = $.parseJSON(data);
            dialogModal.on('hidden.bs.modal'
                , function () {
                    var r = $.parseJSON(data);
                    if(r.success == "true"){
                        KXJS.downloadFile(r.url);
                    }
                    else
                        alert("Error");
                }
            );
            waitingDialog.hide();



        });


    }
    t.loadTemplate = function(){
        var idTemplate = $("#idTmpl").val();
        if(idTemplate == "")
            return;
        t.resetForm();
        waitingDialog.show('Loading...', {dialogSize: 'sm', progressType: 'warning'});

        $.ajax({
            url: KXJS.buildAPICallUrl("KXCJS",t.pluginName,t.pluginObject,"loadDataTemplate"),
            data: { keyID: idTemplate}
        }).done(function(data) {


            waitingDialog.hide();

            var r = $.parseJSON(data);
            $("#templateCode").val(r.templateCode);
            $("#reportName").val(r.reportName);

            $('#employeeCode [value="'+r.employeeCode+'"]').prop("selected",true);

            var arrayField = r.arrayField;
            jQuery.each( arrayField, function( i, val ) {
                var currentField = $('#fieldItemContainer').find("button").first();
                t.addField(currentField);
                currentField = $('#fieldSelectedContainer button').last();
                currentField.attr("fieldName",i);
                currentField.text(val);
            });

            var arrayFilter = r.arrayFilter;
            jQuery.each( arrayFilter, function( key, val ) {
                var filterName = val.filterName
                var filterValue = val.filterValue
                var filterDescription = val.filterDescription;
                t.addFilter(filterValue,filterDescription,val.filterCode,filterName);
            });

            if(r.fieldGroup == "1")
                $("#fieldGroup").prop("checked",true);
            else
                $("#fieldGroup").prop("checked",false);

            $("#mainTab .nav-link, #mainContent .tab-pane").removeClass("active");
            $("#mainTab .nav-link").first().addClass("active");
            $("#mainContent .tab-pane").first().addClass("active");


        });

    }
    t.loadReportMyTemplate = function() {
        KXJS.loadDataCombo({plugin:t.pluginName,pluginObject:t.pluginObject,pluginAction:"getListMyTemplate",comboID:"#idTmpl",optionValue:"value",optionText:"text"});
    }





    t.init();

}).apply(KXNS.k_report.objects.report_excel);
