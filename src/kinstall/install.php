<?



//set global variables
global $K_COMMON;

ini_set("display_errors", true);


if (!file_exists('ksettings/installing.php')) //If exist this file we are installing
{
    if (file_exists('ksettings/settings.php')) //If exist this file the system is installed
        die("The system was Installed!");


    include_once("kcore/k_common/objects/general/base/main.php");
    include_once("kcore/k_common/objects/general/common/main.php");


    //set global variable
    $K_COMMON = new \k_common\objects\general\common\obj();
    $K_COMMON->pathCstSystem = $_SERVER['DOCUMENT_ROOT'];
    $K_COMMON->pathKixixi = $K_COMMON->pathCstSystem;

    //include auto loader object
    include_once("kcore/k_common/objects/general/autoloader/main.php");
    new \k_common\objects\general\autoloader\obj();


    //Define Variables for static content
    $K_COMMON->setStaticContentVariables();

    //Load main common plugin
    include_once("kcore/k_sessions/objects/general/handler/main.php");




}
else {

    include_once("kcore/k_sessions/objects/general/initiate/main.php");

    $K_SESSIONS_START = new \k_sessions\objects\general\initiate\obj;

    $K_SESSIONS_START->initiate();

}

//get Page Plugin
$webPage = new \k_webmanager\objects\general\web_page_base\obj();

//get Object Install
$obj = $K_COMMON->getPluginObject("k_install", "install");
$arrObjPar = $_REQUEST;
$arrObjPar["pluginMethod"] = "get";
$webPage->appendObject("", $obj->get($arrObjPar));

//Show Page
echo $webPage->getPage();


?>

