<?

/**
 * KIXIXI INDEX
 * manager virtual Url (must be called from web server)
 *
 * Example of Apache web server configuration
 * <VirtualHost *:80>
 *  DocumentRoot /usr/local/websites/kixixi_websitename
 *  ServerName www.websitename.com
 *  DirectoryIndex index.htm
 *
 *  <Directory /usr/local/websites/kixixi_websitename>
 *      RewriteEngine On
 *      RewriteCond %{REQUEST_METHOD} GET
 *      RewriteRule !\.(css|js|less|jpg|png|gif|php|swf|txt|ttf|otf|eot|woff)$ /index.php [L,NS]
 *      AllowOverride AuthConfig FileInfo Indexes Limit Options=All,MultiViews
 *      Options +FollowSymLinks +SymLinksIfOwnerMatch
 *  </Directory>
 *
 * </VirtualHost>
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @version 1.00
 */






include_once("kcore/k_sessions/objects/general/initiate/main.php");
$K_SESSIONS_START = new \k_sessions\objects\general\initiate\obj;
$K_SESSIONS_START->initiate();

$webPage = new k_webmanager\objects\general\web_page\obj();
$webPage->get();



?>
