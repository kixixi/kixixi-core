KXJS.namespace("tinyMCE");
KXNS.tinyMCE.settingFull = {
    selector:"#content"
    ,skin:"kixixilight"
    ,plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'template paste textcolor colorpicker textpattern imagetools lineheight'
    ]
    ,relative_urls:false
    ,remove_script_host:false
    ,convert_urls:false
    ,browser_spellcheck:true
    ,fix_list_elements:true
    ,entities:"38,amp,60,lt,62,gt"
    ,entity_encoding:"raw"
    ,keep_styles:false
    ,end_container_on_empty_block:true
    ,resize:false
    ,menubar:false
    ,toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | fullscreen | code'
    ,toolbar2: 'print preview media | forecolor backcolor | styleselect formatselect fontselect fontsizeselect lineheightselect'
};
