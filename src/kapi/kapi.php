<?
/**
 *
 * KIXIXI kapi object
 * used for ajax call
 *
 *
 * @author Marco Del Principe <m.delprincipe73@gmail.com>
 *
 * @version 1.00
 *
 */

include_once($_SERVER['DOCUMENT_ROOT'] . "/kcore/k_sessions/objects/general/initiate/main.php");

$K_SESSIONS_START = new \k_sessions\objects\general\initiate\obj;
$K_SESSIONS_START->initiate();



class kapi extends \k_common\objects\general\base\obj
{
	/**
	 * Execute ajax call
	 *
	 * get parameters from $_REQUEST
	 * KXTCALL: Type of call
	 * KCP: Call Plugin
	 * KCO: Call Object
	 *
	 * **Example of XKTCALL**<br>
	 *    *    KXCJS: for JSON result
	 *    *    KXCXML: for XML result
	 *    *    KXCH: for HTML result
	 *    *    KXCDWF: for download file
	 *
	 * @since 1.00
	 *
	 */
	function callManage()
	{

		$KXTCALL = $this->K_COMMON->getVarRequest("KXTCALL");
		$KXPlugin = $this->K_COMMON->getVarRequest("KCP");
		$KXPluginObject = $this->K_COMMON->getVarRequest("KCO");
		$result = "";

		if ($KXTCALL!="")
			switch($KXTCALL)
			{
				case "KXCJS": //Json Request
					header("Access-Control-Allow-Origin: *");

					$r = $_REQUEST;
					$r["OM"] = "JSON";
                    $obj = $this->K_COMMON->getPluginObject($KXPlugin, $KXPluginObject);
					$result = $obj->manageWebService($r);

					break;
				case "KXCXML": //XML Request
					ob_get_clean();
					header('Content-Type: text/xml');

					$r = $_REQUEST;
					$r["OM"] = "XML";

					$obj = $this->K_COMMON->getPluginObject($KXPlugin, $KXPluginObject);
					$result = $obj->manageWebService($r);
					
					break;
				case "KXCH": //Load HTML
					header('Content-Type: text/html');

					$r = $_REQUEST;
					$r["OM"] = "HTML";
					$obj = $this->K_COMMON->getPluginObject($KXPlugin, $KXPluginObject);
					$result = $obj->manageWebService($r);
					
					break;
				case "KXCDWF": //For example for download file
					ob_get_clean();

					$r = $_REQUEST;
					$r["OM"] = "HTML";

					$obj = $this->K_COMMON->getPluginObject($KXPlugin, $KXPluginObject);
					$resultOBJ = $obj->manageWebService($r);
					$result = $this->K_COMMON->getVarArray($resultOBJ,"content");
					$type = strtoupper($this->K_COMMON->getVarArray($resultOBJ,"type"));
					$fileName = $this->K_COMMON->getVarArray($resultOBJ,"fileName");
					$filePath = $this->K_COMMON->getVarArray($resultOBJ,"filePath");

					switch ($type)
					{
						case "XML":
							header('Content-Type: text/xml');
							header("Cache-Control: public");
							header("Content-Description: File Transfer");
							header("Content-Disposition: attachment; filename= " . $fileName);
							header("Content-Transfer-Encoding: binary");
							break;
						case "PDF":
							header('Content-Type: application/pdf');
							header("Cache-Control: public");
							header("Content-Description: File Transfer");
							header("Content-Disposition: attachment; filename= " . $fileName);
							header("Content-Transfer-Encoding: binary");
							break;
						case "JPEG":
							header('Content-Type: image/jpeg');
							header("Cache-Control: public");
							header("Content-Description: File Transfer");
							header("Content-Disposition: attachment; filename= " . $fileName);
							header("Content-Transfer-Encoding: binary");
							break;
						case "GIF":
							header('Content-Type: image/gif');
							header("Cache-Control: public");
							header("Content-Description: File Transfer");
							header("Content-Disposition: attachment; filename= " . $fileName);
							header("Content-Transfer-Encoding: binary");
							break;
						case "PNG":
							header('Content-Type: image/png');
							header("Cache-Control: public");
							header("Content-Description: File Transfer");
							header("Content-Disposition: attachment; filename= " . $fileName);
							header("Content-Transfer-Encoding: binary");
							break;
						case "ZIP":
							/*
							header("Pragma: public");
							header("Expires: 0");
							header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
							header("Cache-Control: public");
							header("Content-Description: File Transfer");
							header("Content-type: application/octet-stream");
							header("Content-Disposition: attachment; filename=\"".$fileName."\"");
							header("Content-Transfer-Encoding: binary");
							header("Content-Length: ".filesize($filePath.$fileName));
							*/

							header("Content-Type: application/zip");
							header("Content-Disposition: attachment; filename=$fileName");
							header("Content-Length: " . filesize($filePath.$fileName));

							//readfile($yourfile);

							//@ob_end_flush();

							@readfile($filePath.$fileName);

							break;

					}


					
					break;


			}

		echo $result;

	}
}




$kapi = new kapi();
$kapi->callManage();


?>




